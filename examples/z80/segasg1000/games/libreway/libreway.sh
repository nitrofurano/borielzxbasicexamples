zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=libreway.sg_
zxb.py libreway.bas --org=$((0x0069))
zxb.py libreway.bas --asm --$((org=0x0069))
cat libreway.bin >> libreway.sg_
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> libreway.sg_
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=libreway.sg_ of=libreway.sg
rm smsboot.bin libreway.sg_ _dummybytes.bin libreway.bin
cp libreway.sg libreway.sms
smshead8k libreway.sms
mame sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 libreway.sg

