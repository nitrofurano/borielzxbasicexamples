zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=xbill.sg_
zxb.py xbill.bas --org=$((0x0069))
zxb.py xbill.bas --asm --org=$((0x0069))
cat xbill.bin >> xbill.sg_
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> xbill.sg_
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=xbill.sg_ of=xbill.sg
rm smsboot.bin xbill.sg_ _dummybytes.bin xbill.bin
cp xbill.sg xbill.sms
smshead16k xbill.sms
mame sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 xbill.sg

