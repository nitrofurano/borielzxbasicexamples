goto chrattr01end:
chrattr01:
asm

;--- attr -------

;0x00..0x0F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1

;0x10..0x1F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $51,$c5,$c5,$c5,$c5,$c5,$c5,$c5
  defb $c5,$c5,$51,$51,$c5,$c5,$c5,$c5
  defb $51,$c5,$c5,$c5,$c5,$51,$c5,$c5
  defb $c5,$c5,$c5,$51,$c5,$c5,$c5,$51
  defb $c5,$c5,$c5,$c5,$c5,$c5,$c5,$c5
  defb $c5,$c5,$c5,$51,$c5,$c5,$c5,$51

;0x20..0x2F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$11,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1

;0x30..0x3F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1

;0x40..0x4F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1

;0x50..0x5F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$11

;0x60..0x6F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$11,$e1,$11,$b1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$fe,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$e1,$11,$e1,$b1
  defb $f1,$f1,$f1,$f1,$fe,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$fe,$fe,$fe,$fe
  defb $f1,$f1,$f1,$f1,$e1,$f1,$e1,$b1
  defb $b1,$e1,$f1,$f1,$e1,$f1,$e1,$f1
  defb $fe,$f4,$f4,$fe,$f4,$f1,$fe,$fe
  defb $fe,$f4,$f4,$fe,$f4,$f1,$fe,$fe
  defb $f1,$f1,$f1,$f1,$fe,$fe,$fe,$fe
  defb $f1,$f1,$f1,$f1,$e1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1

;0x70..0x7F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$11
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$e1,$e8,$fe,$e1,$11,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$fe,$fe,$fe,$f1,$f1,$f1
  defb $f9,$f9,$f9,$f9,$f9,$f9,$f9,$f9
  defb $f1,$f1,$f1,$f1,$f9,$f9,$f9,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f9,$f9,$f9,$f9,$f9,$f9,$f9
  defb $f9,$f9,$f9,$f1,$f1,$f1,$f1,$f1
  defb $e1,$e1,$e1,$e1,$e1,$e1,$e1,$e1
  defb $e1,$b1,$b1,$b1,$91,$51,$c1,$c1

;0x80..0x8F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $b1,$11,$e1,$11,$11,$11,$11,$11
  defb $b1,$11,$e1,$11,$11,$f1,$f1,$11
  defb $11,$f1,$fc,$f1,$f1,$fe,$fe,$f1
  defb $b1,$e1,$c1,$11,$e1,$11,$11,$e1
  defb $b1,$e1,$11,$11,$e1,$fe,$f1,$e1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$e1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$fe
  defb $fe,$f1,$f1,$f1,$e1,$f4,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$f4,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$fe,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
  defb $fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $51,$51,$51,$51,$51,$11,$51,$65

;0x90..0x9F
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $b1,$11,$e1,$e1,$e1,$f1,$11,$f1
  defb $b1,$11,$e8,$e1,$e1,$11,$11,$f1
  defb $b1,$11,$e1,$e1,$e1,$f1,$11,$fe
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$11,$e1,$e1,$e1,$e1,$e1,$e1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $11,$11,$c1,$c1,$c1,$c1,$c1,$c1
  defb $c1,$c1,$11,$11,$c1,$c1,$c1,$c1
  defb $11,$11,$c1,$c1,$c1,$c1,$c1,$c1
  defb $b1,$b1,$b1,$b1,$cb,$cb,$ba,$ba
  defb $cb,$cb,$cb,$cb,$c1,$c1,$a1,$a1
  defb $b1,$b1,$b1,$cb,$cb,$cb,$ba,$b1
  defb $e1,$e1,$e1,$e1,$e5,$e1,$e1,$e1
  defb $e1,$e1,$e1,$e1,$e1,$e1,$e1,$e1

;0xA0..0xAF
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$f1,$fe,$e1,$e1,$11,$f1,$f1
  defb $e1,$fe,$fe,$e1,$e1,$11,$f1,$f1
  defb $fe,$fe,$f1,$f1,$f1,$f1,$f1,$f1
  defb $11,$e1,$e1,$11,$e1,$f1,$f1,$f1
  defb $11,$e1,$e1,$11,$e1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $11,$e1,$11,$e1,$11,$e1,$11,$e1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $11,$f1,$e1,$e1,$fe,$f1,$e1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$fe,$f4,$f4,$fe,$fe,$fe,$fe
  defb $fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $65,$65,$65,$65,$65,$65,$65,$11

;0xB0..0xBF
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$11,$e1,$11,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$e1,$e1,$11,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $e1,$e1,$fe,$e1,$e1,$fe,$e1,$e1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $c1,$c1,$c1,$c1,$c1,$c1,$11,$11
  defb $c1,$c1,$c1,$c1,$11,$11,$c1,$c1
  defb $c1,$c1,$c1,$c1,$c1,$c1,$11,$11
  defb $b9,$b9,$b8,$b8,$b5,$b5,$b1,$b1
  defb $91,$91,$81,$81,$51,$51,$41,$b4
  defb $b1,$b1,$b8,$b8,$b5,$b5,$b4,$b1
  defb $c1,$c1,$e1,$e1,$11,$11,$e1,$e1
  defb $e1,$e1,$e1,$e1,$e1,$e1,$e1,$e1

;0xC0..0xCF
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $b1,$11,$e1,$11,$11,$f1,$f1,$11
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$11,$e1,$e1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$fe,$f1,$f1,$fe
  defb $b1,$e1,$11,$11,$e1,$fe,$f1,$e1
  defb $f1,$f1,$f1,$f1,$f1,$fe,$f1,$fe
  defb $11,$e1,$11,$e1,$11,$e1,$f1,$fe
  defb $f1,$f1,$f1,$f1,$f1,$fe,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$11,$11,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$fe,$f4,$f4,$fe,$f1,$fe,$fe
  defb $b1,$e1,$f4,$f4,$e1,$f1,$e1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $b1,$11,$e1,$11,$fe,$f1,$f1,$11

;0xD0..0xDF
  defb $b1,$11,$e1,$11,$e1,$11,$11,$11
  defb $b1,$11,$e1,$11,$fe,$f1,$f1,$11
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $51,$11,$11,$51,$11,$11,$f1,$f1
  defb $11,$11,$11,$11,$81,$81,$81,$11
  defb $81,$81,$81,$81,$81,$81,$81,$81
  defb $11,$11,$11,$11,$81,$81,$81,$81
  defb $e1,$e1,$e1,$e1,$fe,$ec,$e5,$e1
  defb $e1,$e1,$e1,$e1,$fc,$c5,$c5,$e1
  defb $e1,$e1,$e1,$e1,$e1,$e5,$e5,$e5
  defb $51,$51,$51,$51,$51,$51,$51,$51
  defb $51,$51,$51,$51,$95,$51,$f1,$f1
  defb $51,$51,$51,$51,$51,$51,$51,$51
  defb $b1,$b1,$b1,$b1,$b1,$b5,$b5,$b5
  defb $b5,$b5,$b5,$b5,$b5,$b5,$b5,$b5
  defb $b1,$b1,$b1,$b1,$b5,$b5,$b5,$b5

;0xE0..0xEF
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$fe,$fe,$e1,$e1,$11,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$f1,$f1,$f1
  defb $11,$e1,$e1,$11,$e1,$f1,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$fe,$f1,$f1
  defb $fe,$f1,$fe,$f1,$f1,$fe,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$f1,$11,$e1,$e1,$11,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $fe,$fe,$fe,$fe,$fe,$f4,$f1,$f1
  defb $f1,$f1,$f1,$f1,$e1,$f4,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $e1,$e1,$e1,$e1,$e1,$11,$f1,$f1

;0xF0..0xFF
  defb $e1,$e1,$e1,$e1,$e1,$11,$f1,$f1
  defb $e1,$e1,$e1,$e1,$e1,$11,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$f1,$f1
  defb $f1,$fe,$fe,$fe,$fe,$f1,$f1,$f1
  defb $f1,$f1,$f1,$f1,$f1,$f1,$11,$11
  defb $f8,$f1,$f1,$f1,$f1,$f1,$f1,$11
  defb $81,$11,$11,$11,$f1,$f1,$11,$11
  defb $e1,$e1,$e1,$e1,$e1,$e1,$e1,$e1
  defb $e1,$e5,$e5,$51,$e5,$e1,$e1,$e1
  defb $e5,$e5,$e5,$e5,$e5,$e1,$e1,$e1
  defb $51,$51,$51,$51,$95,$95,$51,$51
  defb $f1,$f1,$f1,$f1,$f9,$f9,$95,$51
  defb $51,$51,$51,$51,$95,$95,$51,$51
  defb $b5,$b5,$b5,$b1,$b1,$b1,$b1,$b1
  defb $b5,$b5,$b5,$b5,$b5,$b5,$b5,$b5
  defb $b5,$b5,$b5,$b5,$b1,$b1,$b1,$b1

;---- assembly ldirvm routines?
;  defb $21,$00,$c8,$11,$00,$00,$01,$00,$08,$cd,$5c,$00
;  defb $21,$00,$d0,$11,$00,$20,$01,$00,$08,$cd,$5c,$00
;  defb $21,$00,$c8,$11,$00,$08,$01,$00,$08,$cd,$5c,$00
;  defb $21,$00,$d0,$11,$00,$28,$01,$00,$08,$cd,$5c,$00
;  defb $21,$00,$c8,$11,$00,$10,$01,$00,$08,$cd,$5c,$00
;  defb $21,$00,$d0,$11,$00,$30,$01,$00,$08,$cd,$5c,$00
;  defb $c9

  end asm
chrattr01end:
