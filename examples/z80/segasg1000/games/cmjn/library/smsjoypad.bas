function fastcall smsjoypad1() as UByte
  asm
    in  a, ($dc)  ; Reads joystick 1
    cpl      ; Inverts all bits
  end asm
end function

function fastcall smsjoypad2() as UByte
  asm
    in  a, ($dc)  ; Reads UP and DOWN
    cpl      ; Inverts all bits
    rla
    rla
    rla      ; Puts them into the 2 lower bits
    and  $03    ; Masks them
    ld  l, a
    in  a, ($dd)  ; Reads the remaining bits
    cpl      ; Inverts all bits
    add  a, a
    add  a, a    ; Shifts them to the correct position
    or  l    ; Groups them with the first two
  end asm
end function
