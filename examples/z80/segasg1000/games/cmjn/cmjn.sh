zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=cmjn.sg_
zxb.py cmjn.bas --org=$((0x0069))
zxb.py cmjn.bas --asm --org=$((0x0069))
cat cmjn.bin >> cmjn.sg_
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> cmjn.sg_
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=cmjn.sg_ of=cmjn.sg
rm smsboot.bin cmjn.sg_ _dummybytes.bin cmjn.bin
cp cmjn.sg cmjn.sms
smshead16k cmjn.sms
mame sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 cmjn.sg

