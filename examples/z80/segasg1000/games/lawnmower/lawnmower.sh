zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=lawnmower.sg_
zxb.py lawnmower.bas --org=$((0x0069))
zxb.py lawnmower.bas --asm --org=$((0x0069))
cat lawnmower.bin >> lawnmower.sg_
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> lawnmower.sg_
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=lawnmower.sg_ of=lawnmower.sg
rm smsboot.bin lawnmower.sg_ _dummybytes.bin lawnmower.bin
cp lawnmower.sg lawnmower.sms
smshead16k lawnmower.sms
mame sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 lawnmower.sg

