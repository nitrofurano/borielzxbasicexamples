sub smsvpoke(tvram as uinteger,tvl as uinteger):
  asm
    ;ld h,(ix+5)
    ;ld l,(ix+4)
    ;ld a,(ix+6)

    ld l,(ix+4)
    ld a,l
    out ($bf),a

    ld h,(ix+5)
    ld a,h
    or $40
    out ($bf),a

    ld a,(ix+6)
    out ($be),a

    end asm
  end sub

'-------------------------------------------------------------------------------

