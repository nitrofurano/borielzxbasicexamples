#include "library/_sms_setz80stackpoint.bas"

#include "library/smsvdp.bas"
#include "library/smsfilvrm.bas"
#include "library/smsldirvm.bas"
#include "library/smsvpoke.bas"
#include "library/sg1000putsprite.bas"
#include "library/smsjoypad.bas"
#include "library/smsrnd.bas"
#include "library/arcade_b.bas"
#include "library/smswaitvbl.bas"
#include "library/cloud01.bas"
#include "library/mountains01.bas"
#include "library/rails01.bas"
#include "library/polepositionrailstripes01b.bas"
#include "library/polepositionrailtilesbg01b.bas"
#include "library/cvdirvmstrp.bas"

'#include "library/cvboot.bas"
'#include "library/cvjoypad.bas"

'-------------------------------------------------------------------------------
'- including binaries... (check redundancy... :S )

goto bininc01end
ptrn01:
asm
  incbin "library/01wholek3r11patterns2.bin"
  end asm
bininc01end:

'-------------------------------------------------------------------------------
'- defining variables

dim eee as uinteger at $C010
dim ee0 as uinteger at $C012
dim ee1 as uinteger at $C014
dim ee2 as uinteger at $C016
dim ee3 as uinteger at $C018
dim xdr as uinteger at $C01A
dim trk as uinteger at $C01C
dim spd as  integer at $C01E
dim xrp as uinteger at $C020
dim trk2 as uinteger at $C022
dim tmr as uinteger at $C024
dim debug as ubyte at $C026
dim gest as ubyte at $C027

debug=0

'-------------------------------------------------------------------------------

sub zprintdecimal(tpos as uinteger,tval as uinteger):
  smsvpoke(6144+tpos+0,48+int(tval/10000) mod 10)
  smsvpoke(6144+tpos+1,48+int(tval/1000) mod 10)
  smsvpoke(6144+tpos+2,48+int(tval/100) mod 10)
  smsvpoke(6144+tpos+3,48+int(tval/10) mod 10)
  smsvpoke(6144+tpos+4,48+tval mod 10)
  end sub

sub zprintdecimal3(tpos as uinteger,tval as uinteger):
  smsvpoke(6144+tpos+0,48+int(tval/100) mod 10)
  smsvpoke(6144+tpos+1,48+int(tval/10) mod 10)
  smsvpoke(6144+tpos+2,48+tval mod 10)
  end sub

sub zprintdecimal2(tpos as uinteger,tval as uinteger):
  smsvpoke(6144+tpos+0,48+int(tval/10) mod 10)
  smsvpoke(6144+tpos+1,48+tval mod 10)
  end sub

sub zprintdecimal5o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  smsvpoke(6144+tpos+0,48+tofs+int(tval/10000) mod 10)
  smsvpoke(6144+tpos+1,48+tofs+int(tval/1000) mod 10)
  smsvpoke(6144+tpos+2,48+tofs+int(tval/100) mod 10)
  smsvpoke(6144+tpos+3,48+tofs+int(tval/10) mod 10)
  smsvpoke(6144+tpos+4,48+tofs+tval mod 10)
  end sub

sub zprintdecimal3o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  smsvpoke(6144+tpos+0,48+tofs+int(tval/100) mod 10)
  smsvpoke(6144+tpos+1,48+tofs+int(tval/10) mod 10)
  smsvpoke(6144+tpos+2,48+tofs+tval mod 10)
  end sub

sub zprintdecimal2o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  smsvpoke(6144+tpos+0,48+tofs+int(tval/10) mod 10)
  smsvpoke(6144+tpos+1,48+tofs+tval mod 10)
  end sub

'-------------------------------------------------------------------------------
'- whatever...
'msxcolor($6,$F,$1)
'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?

'-------------------------------------------------------------------------------
'- whatever...

'msxcolor($6,$F,$1)
smsvdp(7,$61) 'smsvdp(7,F*16+4) '- ink*16+border - color(3,?,1)

'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
smsvdp(0,%00000010):smsvdp(1,%11100010) '- screen2,2 (graphics1)
'- vdp entries for screen2 - base vram addresses
smsvdp(2,$06) '- $1800
smsvdp(3,$FF) '- $2000
smsvdp(4,$03) '- $0000
smsvdp(5,$36) '- $1B00
smsvdp(6,$07) '- $3800

'-------------------------------------------------------------------------------

for ee1=$0000 to $17FF step $0800
  smsfilvrm($0000+ee1,$A5,$0800)
  smsfilvrm($2000+ee1,$A5,$0800)
  next

smsldirvm($0100,@typeface01,512)
smsfilvrm($2100,$F5,512)

smsldirvm($0300,@typeface01+128,80)
smsldirvm($0350,@typeface01+code("T")*8-256,8)
smsldirvm($0358,@typeface01+code("O")*8-256,8)
smsldirvm($0360,@typeface01+code("P")*8-256,8)
smsfilvrm($2300,$95,128)
smsldirvm($0380,@typeface01+128,80)
smsldirvm($03D0,@typeface01+code("L")*8-256,8)
smsldirvm($03D8,@typeface01+code("A")*8-256,8)
smsldirvm($03E0,@typeface01+code("P")*8-256,8)
smsldirvm($03E8,@typeface01+       34*8-256,8)
smsfilvrm($2380,$35,128)
smsldirvm($0400,@typeface01+128,80)
smsldirvm($0450,@typeface01+code("T")*8-256,8)
smsldirvm($0458,@typeface01+code("I")*8-256,8)
smsldirvm($0460,@typeface01+code("M")*8-256,8)
smsldirvm($0468,@typeface01+code("E")*8-256,8)
smsfilvrm($2400,$A5,128)

smsldirvm($1080,@typeface01+code("L")*8-256,8)
smsldirvm($1088,@typeface01+code("O")*8-256,8)
smsldirvm($1090,@typeface01+code("H")*8-256,8)
smsldirvm($1098,@typeface01+code("I")*8-256,8)
smsfilvrm($3080,$FE,32)



smsldirvm($0080,@cloud01,80)

smsldirvm($0F80,@mountains01,128)

smsldirvm($0E00,@rails01,160)
smsfilvrm($1400,$FF,512)
smsldirvm($1400,@rails01+160,448)

smsfilvrm($2E00,$8C,160)
smsfilvrm($2E00,$AE,64)
smsfilvrm($2E00+64,$FE,32)

smsfilvrm($3400,$8C,512)
smsfilvrm($3400,$AE,256)
smsfilvrm($3400+256,$FE,64)

smsfilvrm($2080,$F5,80)
smsfilvrm($2F80,$45,48)
smsfilvrm($2FB0,$64,80)
smsldirvm($2FB0,@mountains01attr,80)

for ee1=$0000 to $17FF step $0800
  for ee0=0 to 15
    smsfilvrm($2000+(ee0*8)+ee1,(ee0*$11) band $FF,8)
    next:next

smsfilvrm($1800,$0C,768)
smsfilvrm($1800,$05,384)

smsldirvm($1800,@dummyscoredisplay01,64)
'smsvpoke($181C,0x22)

smsldirvm($1800+32*10,@mountainstiles01,64)

smsldirvm($1800+5*32+3,@cloudtiles01,5)
smsldirvm($1800+6*32+3,@cloudtiles01+5,5)

'---

for ee0=0 to 31
  smsvpoke($0F00+ee0,$FF)
  smsvpoke($0F20+ee0,$FF)
  smsvpoke($0F40+ee0,$FF)
  next
for ee0=0 to 63
  smsvpoke($1600+ee0,$FF)
  smsvpoke($1640+ee0,$FF)
  smsvpoke($1680+ee0,$FF)
  next

if (debug band 1) <>0 then
'--------------
'!!!!
for ee0=0 to 767
  smsvpoke($1800+ee0,ee0)
  next
'--------------

for ee0=0 to 31
  ee1=$8C
  if (peek(@railstripes01   +ee0)+trk2) band 32<>0 then
      ee1=$FC:end if
  smsvpoke($2F00+ee0,ee1)
  next
for ee0=0 to 63
  ee1=$8C
  if (peek(@railstripes01+32+ee0)+trk2) band 32<>0 then
      ee1=$FC:end if
  smsvpoke($3600+ee0,ee1)
  next

for ee0=0 to 31
  ee1=$AE
  if (peek(@railstripes01   +ee0)+trk2) band 128<>0 then
      ee1=$FE:end if
  smsvpoke($2F20+ee0,ee1)
  next
for ee0=0 to 63
  ee1=$AE
  if (peek(@railstripes01+32+ee0)+trk2) band 128<>0 then
      ee1=$FE:end if
  smsvpoke($3640+ee0,ee1)
  next

for ee0=0 to 31
  ee1=$EE
  if (peek(@railstripes01   +ee0)+trk2) band 64<>0 then
      ee1=$FE:end if
  smsvpoke($2F40+ee0,ee1)
  next
for ee0=0 to 63
  ee1=$EE
  if (peek(@railstripes01+32+ee0)+trk2) band 64<>0 then
      ee1=$FE:end if
  smsvpoke($3680+ee0,ee1)
  next

'for ee0=0 to 3
'  smsldirvm($1800+12+((19+ee0)*32),@cartiles01+(ee0*8),8)
'  next

'--------------
'!!!!
do:loop
'--------------

end if

'---

gest=0
tmr=0
xrp=118
xdr=6:trk=0:spd=0

loopunkn01:
do

'- joystick
'if (cvjoypad1a() band 2)=0 or (cvjoypad2a() band 2)=0 then: '-right
if (smsjoypad1() band 8)<>0 then: '-right
  xdr=xdr+1:end if
'if (cvjoypad1a() band 8)=0 or (cvjoypad2a() band 8)=0 then: '-left
if (smsjoypad1() band 4)<>0 then: '-left
  xdr=xdr-1:end if
'if (cvjoypad1a() band 64)=0 or (cvjoypad2a() band 64)=0 then: '-accel
if (smsjoypad1() band 16)<>0 then:
  if gest=0 then:
    spd=spd+7
  else:
    spd=spd+3
    end if
  end if

'if (cvjoypad1b() band 64)=0 or (cvjoypad2b() band 64)=0 then: '-brake
if (smsjoypad1() band 32)<>0 then:
  spd=spd-16:end if

'if (cvjoypad1a() band 1)=0 or (cvjoypad2a() band 1)=0 then:
if (smsjoypad1() band 1)<>0 then:
  gest=0:end if
'if (cvjoypad1a() band 4)=0 or (cvjoypad2a() band 4)=0 then:
if (smsjoypad1() band 2)<>0 then:
  gest=1:end if

spd=spd-1


'xrp=xrp+(xdr-6)

xrp=xrp+((xdr*spd)/255)-((6*spd)/255)

'xrp=xrp+(((xdr-6)*spd)/255)
if xrp<94 then: xrp=94:end if
if xrp>162 then: xrp=162:end if

'if (msxsnsmat(8) band 1)=0 and shot=0 then:shot=1:end if

if xdr<1 then: xdr=1:end if
if xdr>11 then: xdr=11:end if
if spd<0 then: spd=0:end if
if (spd>255 and gest=1) then: spd=255:end if
if (spd>170 and gest=0) then: spd=170:end if

trk=trk+spd

ee0=((xdr-1)*2)+(int(trk/256)band 1)
ee1=ee0*256
smsldirvm($1700,@ptrn01+ee1,256)
smsldirvm($3700,@ptrn01+6144+ee1,256)

zprintdecimal3( 32+26,spd)

zprintdecimal(32+7,trk)

zprintdecimal2o(26,int(tmr/60) mod 60,64)
zprintdecimal2o(29,tmr mod 60,64)

if (debug band 2)<>0 then
  zprintdecimal3o(160+28,xdr,64)
  zprintdecimal3o(192+28,xrp,64)
  zprintdecimal3o(224+28,(xrp*11)/12,64)
  end if

asm
  ;halt
  halt
  end asm

smsldirvm($1800+12*32,@railtilesbg01+128*00+48                    ,32)
smsldirvm($1800+13*32,@railtilesbg01+128*01+48+(((xrp* 1)/11)- 11),32)
smsldirvm($1800+14*32,@railtilesbg01+128*02+48+(((xrp* 2)/11)- 23),32)
smsldirvm($1800+15*32,@railtilesbg01+128*03+48+(((xrp* 3)/11)- 34),32)
smsldirvm($1800+16*32,@railtilesbg01+128*04+48+(((xrp* 4)/11)- 46),32)
smsldirvm($1800+17*32,@railtilesbg01+128*05+48+(((xrp* 5)/11)- 58),32)
smsldirvm($1800+18*32,@railtilesbg01+128*06+48+(((xrp* 6)/11)- 69),32)
smsldirvm($1800+19*32,@railtilesbg01+128*07+48+(((xrp* 7)/11)- 81),32)
smsldirvm($1800+20*32,@railtilesbg01+128*08+48+(((xrp* 8)/11)- 93),32)
smsldirvm($1800+21*32,@railtilesbg01+128*09+48+(((xrp* 9)/11)-104),32)
smsldirvm($1800+22*32,@railtilesbg01+128*10+48+(((xrp*10)/11)-116),32)
smsldirvm($1800+23*32,@railtilesbg01+128*11+48+(  xrp        -128),32)

for ee0=0 to 3
  smsldirvm($1800+12+((19+ee0)*32),@cartiles01+(ee0*8),8)
  next


smsldirvm($1800+23*32+29,@gearstate01+((gest band 1)*2),2)

trk2=trk/8

''-red/white
cvdirvmstrp($2F00,@railstripes01   ,32,$8C,$FC,32,trk2)
cvdirvmstrp($3600,@railstripes01+32,64,$8C,$FC,32,trk2)

'-yellow/white
cvdirvmstrp($2F20,@railstripes01   ,32,$AE,$FE,32,trk2)
cvdirvmstrp($3640,@railstripes01+32,64,$AE,$FE,32,trk2)

'-grey/white
cvdirvmstrp($2F40,@railstripes01   ,32,$EE,$FE,32,trk2)
cvdirvmstrp($3680,@railstripes01+32,64,$EE,$FE,32,trk2)

tmr=tmr+1

loop

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------

cartiles01:
asm
  defb $E0,$E1,$E2,$E3,$E4,$E5,$E6,$E7
  defb $E8,$E9,$EA,$EB,$EC,$ED,$EE,$EF
  defb $F0,$F1,$F2,$F3,$F4,$F5,$F6,$F7
  defb $F8,$F9,$FA,$FB,$FC,$FD,$FE,$FF
  end asm

dummyscoredisplay01:
asm
  defb "   "
  defb $6A,$6B,$6C         ;"TOP"
  defb " "
  defb $61,$62,$60,$60,$60 ;"12000"
  defb "  "
  defb $8A,$8B,$8C,$8D     ;"TIME"
  defb "    "
  defb $7A,$7B,$7C         ;"LAP"
  defb " "
  defb $72,$74,$7D,$72,$78 ;"24 28"
  defb "  SCORE  3760   "
  defb $89,$85             ;"95"
  defb "   SPEED 189"
  defb $5D,$5E,$20
  end asm

cloudtiles01:
asm
  defb $10,$11,$12,$13,$14
  defb $15,$16,$17,$18,$19
  end asm

mountainstiles01:
asm
  defb  $F0,$F1,$F2,$F0, $F1,$F2,$F1,$F3, $F4,$F0,$F4,$F5, $F4,$F1,$F2,$F0, $F5,$F2,$F0,$F5, $F2,$F4,$F1,$F2, $F0,$F1,$F2,$F1, $F2,$F4,$F0,$F2
  defb  $FF, $FF,$FF,$FF,$FF, $F6,$F7,$F8,$F9, $F8,$FA,$FB,$FB, $F8,$FA,$FB,$FD, $F8,$FD,$FE,$FF, $FF,$FF,$FF,$FF, $FF,$FF,$FF,$FF, $FF,$FF,$FF

  ;defb $F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1
  ;defb $FF,$FF,$FF,$FF,$FF,$F6,$F7,$F8,$F9,$FA,$FB,$FC,$F8,$F9,$FA,$FB,$FC,$F8,$F9,$FD,$FE,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  end asm

gearstate01:
asm
  defb $10,$11,$12,$13
  end asm

'--------------------------------------

