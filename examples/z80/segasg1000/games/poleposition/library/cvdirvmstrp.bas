sub cvdirvmstrp(tvram as uinteger,tram as uinteger,tlen as uinteger,tik as ubyte,tpp as ubyte,tfl as ubyte,tad as ubyte):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)

    ld a,e
    out ($bf),a
    ld a,d
    or $40
    out ($bf),a

    cvdirvmstrploop:
    ld a,(hl)
    add a,(ix+17)
    and (ix+15)
    jp z,cvdirvmstrpik
    ld a,(ix+13)
    jp cvdirvmstrpikend

    cvdirvmstrpik:
    ld a,(ix+11)

    cvdirvmstrpikend:
    out ($be),a
    inc hl
    dec bc
    ld a,b
    or c
    jp nz,cvdirvmstrploop

    end asm
  end sub







'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
