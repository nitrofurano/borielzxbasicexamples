goto mountains01end

mountains01:

asm
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00001110
  defb %00111111
  defb %11111111

  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000001
  defb %10000011
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11000000
  defb %11110000
  defb %11111100
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00001110
  defb %00111111
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00011000
  defb %10111110
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000001
  defb %11000111
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000011
  defb %00111100
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00111111
  defb %11111111
  defb %11000011
  defb %00000000
  defb %00000000
  defb %00000000
  defb %10111100
  defb %11111110
  defb %11011111
  defb %11111111
  defb %00010001
  defb %00000000
  defb %00000011
  defb %00001111
  defb %10111110
  defb %11110011
  defb %11101111
  defb %11111111
  defb %00001110
  defb %00000000
  defb %11000000
  defb %11100000
  defb %11111111
  defb %11111111
  defb %11111111
  defb %11111111
  defb %10011000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11000110
  defb %11111111
  defb %11111111
  defb %11111111
  defb %00010000
  defb %00000000
  defb %00000000
  defb %11000000
  defb %11111000
  defb %11111100
  defb %11111110
  defb %11111111
  defb %00100100
  defb %00000000
  defb %00000000
  defb %00000000
  defb %01111000
  defb %11111101
  defb %11011111
  defb %11111111
  defb %10001000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11000000
  defb %11110000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

mountains01attr:
asm
  defb $64,$64,$64,$64,$64,$64,$6C,$6C
  defb $64,$64,$64,$64,$64,$64,$6C,$6C
  defb $64,$64,$64,$64,$64,$6E,$6C,$6C
  defb $64,$64,$64,$6E,$6E,$6E,$6C,$6C
  defb $64,$64,$64,$64,$64,$64,$6C,$6C
  defb $64,$64,$64,$64,$64,$6E,$6C,$6C
  defb $64,$64,$64,$64,$64,$64,$6C,$6C
  defb $64,$64,$64,$64,$6E,$6E,$6C,$6C
  defb $64,$64,$64,$64,$64,$64,$64,$6C
  defb $64,$64,$64,$64,$64,$64,$6C,$6C
  end asm



mountains01end:

