zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=poleposition.sg_
zxb.py poleposition.bas --org=0x0069
zxb.py poleposition.bas --asm --org=0x0069
cat poleposition.bin >> poleposition.sg_
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> poleposition.sg_
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=poleposition.sg_ of=poleposition.sg
rm smsboot.bin poleposition.sg_ _dummybytes.bin poleposition.bin
cp poleposition.sg poleposition.sms
smshead32k poleposition.sms

#mess sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 poleposition.sg

mess sg1000 -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 poleposition.sg
