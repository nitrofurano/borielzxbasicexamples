sub msxfilvrm(tvl as uinteger, tlen as uinteger, tvram as uinteger):
  asm
    ld a,(ix+4)
    ld b,(ix+7)
    ld c,(ix+6)
    ld h,(ix+9)
    ld l,(ix+8)
    call $016B
    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM
'Address  : #0056
'Function : fill VRAM with value
'Input    : A  - data byte
'           BC - length of the area to be written
'           HL - start address
'Registers: AF, BC
'-------------------------------------------------------------------------------
'BIGFIL
'Address  : #016B
'Function : Same function as FILVRM (total VRAM can be reached).
'Input    : HL - address
'           BC - length
'           A  - data
'Registers: AF,BC
'-------------------------------------------------------------------------------

