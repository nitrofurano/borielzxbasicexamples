#!/usr/bin/python
# -*- coding: latin-1 -*-
import os,sys
def fixhex(va,sz):
  tml_st=""
  for i in range (0,sz,1):tml_st=tml_st+"0"
  tml_st=tml_st+hex(va).lstrip("0x");tml_st=tml_st[len(tml_st)-sz:]
  return tml_st.upper()
finp_st=sys.argv[1];fout_st=finp_st+".hex"
if finp_st.lower()=="--help".lower():
  print "bin2hexdump - 0607021700 - Paulo Silva (GPL licence)"
  print "converts a binary file into a text hexdump file"
  print "usage: python bin2hexdump.py yourfile.bin"
  print "the result will be a neighbour file named yourfile.bin.hex"
  print "(very thanks to python-forum.org guys)"
else:
  finp_fl=open(finp_st,"r");fout_fl=open(fout_st,"w")
  dtof=0
  while True:
    byte_st=finp_fl.read(1);dtof+=1
    if len(byte_st)==0:break
    bytevl=ord(byte_st)
    fout_fl.write(fixhex(bytevl,2))
    if dtof%32==0: # dtof%32 = dtof mod 32
      fout_fl.write("\n")
  finp_fl.close();fout_fl.close()
