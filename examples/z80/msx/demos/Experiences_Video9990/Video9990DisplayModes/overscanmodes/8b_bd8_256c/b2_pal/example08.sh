rm example08.asm
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=example08.tmr
rm msxromheader.bin

zxb.py example08.bas --asm --org=$((0x4010))
zxb.py example08.bas --org=$((0x4010))

#- 8192 for 8kb, 16384 for 16kb, 32768 for 32kb, 131072 for 128kb, 262144 for 256kb
#dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin
dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin

cat example08.bin >> example08.tmr
cat _dummybytes.bin >> example08.tmr
rm _dummybytes.bin example08.bin

#- 0x2000 for 8kb, 0x4000 for 16kb, 0x8000 for 32kb, 0x20000 for 128kb, 0x40000 for 256kb
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example08.tmr of=example08.rom
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example08.tmr of=example08.rom
rm example08.tmr

openmsx -ext video9000 example08.rom
#openmsx -ext gfx9000 -carta example08.rom

#-------------
# on openmsx, why do i need to enter "set videosource gfx9000" on the console (f10) all the time?
# can’t i do it automatically on the command line?
#-------------

#openmsx -ext video9000 -carta example08.rom

# openmsx -ext video9000 example08.rom
# openmsx -ext gfx9000 example08.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# openmsx -machine msx1 -carta example08.rom
# openmsx -machine msx2 example08.rom
# openmsx -machine msx2plus example08.rom
# mess msx -cart1 example08.rom
# mess msx2 -cart1 example08.rom
# mess msx2p -cart1 example08.rom


