sub msxvideo9990vpokeloc(tzad1 as ulong)
  out $64,0
  out $63,(int(tzad1/1) band $FF)
  out $63,(int(tzad1/256) band $FF)
  out $63,(int(tzad1/65536) band $FF) or $80
  end sub


'10000: 'vram write address = p*65536 + a . out &h60 vpokes with autoincrement.
'out $64,0	                        	'vram write mode: 0
'                                   'defusr1 = a		'to get at H,L
'xx=peek($f39c)
'out $63,xx          ': '?xx;
'xx=peek($f39d)
'out $63,xx          ': '?xx;"."
''xx=a
'out $63,p                          'bit 7 address increment inhibit
'return
