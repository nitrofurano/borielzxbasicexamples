#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990setregisters.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxvideo9990vdokeseq.bas"
#include "library/b1r0f0_charmapminivaders01.bas"
#include "library/msxrnd.bas"
#include "library/msxvideo9990command.bas"

msxvideo9990initsuperimpose():msxvideo9990cleanregisters():msxvideo9990vdp(8,$C2)

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim wm as uinteger at $C032
dim fc as uinteger at $C034
dim seed as uinteger at $C036
dim ik as uinteger at $C038
dim pp as uinteger at $C03A
dim e0 as uinteger at $C040
dim e1 as uinteger at $C042
dim e2 as uinteger at $C044
dim e3 as uinteger at $C046
dim ctpi as float at $C080

msxvideo9990vdp(6,$80):msxvideo9990vdp(7,$0E):out $67,1

for i=0 to 3:msxvideo9990palette12bit(i,i*$555):next
msxvideo9990palette12bit(0,$404)
msxvideo9990palette12bit(1,$D02)
msxvideo9990palette12bit(2,$794)
msxvideo9990palette12bit(3,$DE7)

msxvideo9990command(0,0,0,0,192,580,0,$0C,$FFFF,$5555*1,$20)    '-cls beta

msxvideo9990vpokelocp(0,$1504)
for x=0 to 4000:msxvideo9990vpokeseq($00):next
for x=0 to 2000:msxvideo9990vpokeseq($55):next
for x=0 to 1000:msxvideo9990vpokeseq($AA):next
for x=0 to  500:msxvideo9990vpokeseq($FF):next

msxvideo9990command(0,0,8*23,8*3,8,8,0,$0C,$FFFF,0,$10)
ik=3:pp=12:seed=code("A")-32:for x=0 to 7:ikpp2b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next

msxvideo9990command(0,0,8*23,8*5,8,8,0,$0C,$FFFF,0,$10)
ik=3:pp=12:seed=code("A")-32:for x=0 to 7:ikpp2b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next

msxvideo9990command(0,0,8*3,8*5,8,8,0,$0C,$FFFF,0,$10)
ik=3:pp=12:seed=code("?")-32:for x=0 to 7:ikpp2b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next

for e0=0 to 3:for e1=0 to 3
msxvideo9990command(0,0,8*(e0+5),8*(e1+7),8,8,0,$0C,$FFFF,0,$10)
ik=e0:pp=e1:for x=0 to 7:ikpp2b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next
next:next

for e0=0 to 579
seed=msxrnd(seed):e1=seed band 3
msxvideo9990command(0,0,0,e0,192,1,0,$0C,$FFFF,$5555*e1,$20)
next

do
seed=msxrnd(seed):dx=(seed mod 24)
seed=msxrnd(seed):dy=(seed mod 72)
seed=msxrnd(seed):ik=(seed band 3)
seed=msxrnd(seed):pp=(seed band 3)
seed=msxrnd(seed):e1=(seed mod 73)
msxvideo9990command(0,0,8*dx,8*dy,8,8,0,$0C,$FFFF,0,$10)
for x=0 to 7:ikpp2b(peek(@charmap01+x+(e1*8)),ik,pp):next
loop

do:loop

patterntest01a:

sub ikpp8b(tch1 as ubyte,tik1 as ubyte,tpp1 as ubyte)
  dim tlp1 as ubyte at $C000
  dim tmk1 as ubyte at $C001
  tmk1=128
  for tlp1=0 to 7
    if (tmk1 band tch1)<>0 then:
      out $62,tik1
    else
      out $62,tpp1
      end if
    tmk1=tmk1/2
    next
  end sub

sub ikpp2b(tch1 as ubyte,tik1 as ubyte,tpp1 as ubyte)
  dim tlp1 as ubyte at $C000
  dim tmk1 as ubyte at $C001
  dim tou1 as uinteger at $C002
  tou1=0
  tmk1=128
  for tlp1=0 to 7
    tou1=tou1*4
    if (tmk1 band tch1)<>0 then:
      tou1=tou1 bor (tik1 band 3)
    else
      tou1=tou1 bor (tpp1 band 3)
      end if
    tmk1=tmk1/2
    next
  out $62,tou1/256
  out $62,tou1
  end sub


sub ikpp4b(tch1 as ubyte,tik1 as ubyte,tpp1 as ubyte)
  dim tlp1 as ubyte at $C000
  dim tmk1 as ubyte at $C001
  dim tou1 as ulong at $C002
  tou1=0
  tmk1=128
  for tlp1=0 to 7
    tou1=tou1*16
    if (tmk1 band tch1)<>0 then:
      tou1=tou1 bor (tik1 band 15)
    else
      tou1=tou1 bor (tpp1 band 15)
      end if
    tmk1=tmk1/2
    next
  out $62,tou1/$1000000
  out $62,tou1/$10000
  out $62,tou1/$100
  out $62,tou1
  end sub



