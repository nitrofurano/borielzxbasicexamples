sub msxvideo9990command(tsx1 as uinteger,tsy1 as uinteger,tdx1 as uinteger,tdy1 as uinteger,tnx1 as uinteger,tny1 as uinteger,tar1 as ubyte,tlo1 as ubyte,twm1 as uinteger,tfc1 as uinteger,top1 as ubyte):
  while (in($65) band 1)<>0:wend
  'if in($65) band 1 then:goto 11000:end if      'wait for blitter
  out $64,32             'write r32+
  out $63,tsx1 band 255  'r32
  out $63,tsx1/256       'r33
  out $63,tsy1 band 255  'r34
  out $63,tsy1/256       'r35
  out $63,tdx1 band 255  'r36
  out $63,tdx1/256       'r37
  out $63,tdy1 band 255  'r38
  out $63,tdy1/256       'r39
  out $63,tnx1 band 255  'r40
  out $63,tnx1/256       'r41
  out $63,tny1 band 255  'r42
  out $63,tny1/256       'r43
  out $63,tar1           'r44 arg
  out $63,tlo1           'r45 lop
  out $63,twm1 band 255  'r46
  out $63,twm1/256       'r47
  out $63,tfc1 band 255  'r48
  out $63,tfc1/256       'r49
  out $64,52             'write r52+
  out $63,top1           'r52
  end sub
