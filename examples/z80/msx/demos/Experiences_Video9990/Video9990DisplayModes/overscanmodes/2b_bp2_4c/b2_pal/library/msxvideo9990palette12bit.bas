sub msxvideo9990palette12bit(tzid0 as ubyte,tzvl0 as uinteger)
  out $64,128+14
  out $63,tzid0*4
  out $61,(int(tzvl0/256) band 15)*2 '-r ($00..$1F)
  out $61,(int(tzvl0/16) band 15)*2 '-g
  out $61,(tzvl0 band 15)*2 '-b
  end sub
