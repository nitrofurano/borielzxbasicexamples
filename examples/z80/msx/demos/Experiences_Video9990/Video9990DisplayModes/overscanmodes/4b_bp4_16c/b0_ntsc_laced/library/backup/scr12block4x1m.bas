sub scr12block4x1m(vx as uinteger,vy as uinteger,vrcl as ubyte,vgcl as ubyte,vbcl as ubyte,tidfl as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F0) bor ((vkcl band %00011100)/ 4)bor ((tidfl band 1)*8):px1=(vycl band $F0) bor ((vkcl band %11100000)/32)bor ((tidfl band 1)*8)
  px2=(vycl band $F0) bor ((vjcl band %00011100)/ 4)bor ((tidfl band 1)*8):px3=(vycl band $F0) bor ((vjcl band %11100000)/32)bor ((tidfl band 1)*8)
  msx1ldirvm(4,vy*256+vx*4,@px0)
  end sub

sub scr12block4x1ulm(vx as uinteger,vy as uinteger,vrgbcl as ulong,tidfl as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vrcl as ubyte at $E106:dim vgcl as ubyte at $E107:dim vbcl as ubyte at $E108
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vrcl=(vrgbcl band $FF0000)/$10000:vgcl=(vrgbcl band $FF00)/$100:vbcl=vrgbcl band $FF
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F0) bor ((vkcl band %00011100)/ 4)bor ((tidfl band 1)*8):px1=(vycl band $F0) bor ((vkcl band %11100000)/32)bor ((tidfl band 1)*8)
  px2=(vycl band $F0) bor ((vjcl band %00011100)/ 4)bor ((tidfl band 1)*8):px3=(vycl band $F0) bor ((vjcl band %11100000)/32)bor ((tidfl band 1)*8)
  msx1ldirvm(4,vy*256+vx*4,@px0)
  end sub

sub scr12block4x1vhm(vx as uinteger,vy as uinteger,vrcl as ubyte,vgcl as ubyte,vbcl as ubyte, vw as uinteger, vh as uinteger,tidfl as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vxw as uinteger at $E10A:dim vyh as uinteger at $E10C
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F0) bor ((vkcl band %00011100)/ 4)bor ((tidfl band 1)*8):px1=(vycl band $F0) bor ((vkcl band %11100000)/32)bor ((tidfl band 1)*8)
  px2=(vycl band $F0) bor ((vjcl band %00011100)/ 4)bor ((tidfl band 1)*8):px3=(vycl band $F0) bor ((vjcl band %11100000)/32)bor ((tidfl band 1)*8)
  for vxw=vx to vx+vw-1:for vyh=vy to vy+vh-1:msx1ldirvm(4,vyh*256+vxw*4,@px0):next:next
  end sub

sub scr12block4x1ulvhm(vx as uinteger,vy as uinteger,vrgbcl as ulong, vw as uinteger, vh as uinteger,tidfl as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vrcl as ubyte at $E106:dim vgcl as ubyte at $E107:dim vbcl as ubyte at $E108
  dim vxw as uinteger at $E10A:dim vyh as uinteger at $E10C
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vrcl=(vrgbcl band $FF0000)/$10000:vgcl=(vrgbcl band $FF00)/$100:vbcl=vrgbcl band $FF
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F0) bor ((vkcl band %00011100)/ 4)bor ((tidfl band 1)*8):px1=(vycl band $F0) bor ((vkcl band %11100000)/32)bor ((tidfl band 1)*8)
  px2=(vycl band $F0) bor ((vjcl band %00011100)/ 4)bor ((tidfl band 1)*8):px3=(vycl band $F0) bor ((vjcl band %11100000)/32)bor ((tidfl band 1)*8)
  for vxw=vx to vx+vw-1:for vyh=vy to vy+vh-1:msx1ldirvm(4,vyh*256+vxw*4,@px0):next:next
  end sub
