function fastcall msx1vpeek(tad as uinteger) as ubyte
  asm
    call $004A
    end asm
  end function

'-------------------------------------------------------------------------------
'function msx1vpeek(tad as uinteger)
'  asm
'    ld h,(ix+5)
'    ld l,(ix+4)
'    call $004A
'    end asm
'  end function
'-------------------------------------------------------------------------------
'RDVRM
'Address  : #004A
'Function : Reads the content of VRAM
'Input    : HL - address read
'Output   : A  - value which was read
'Registers: AF
'-------------------------------------------------------------------------------
'sub fastcall msxvpoke(tad as uintenger,tva as ubyte)
'  asm
'    pop hl
'    pop af
'    call $004D
'    end asm
'  end sub
'-------------------------------------------------------------------------------
'   vpoke - vpoke v_address,v_value 
'   ld hl,v_address
'   ld a,v_value
'   call 0x004D    #- wrtvrm
'-------------------------------------------------------------------------------
