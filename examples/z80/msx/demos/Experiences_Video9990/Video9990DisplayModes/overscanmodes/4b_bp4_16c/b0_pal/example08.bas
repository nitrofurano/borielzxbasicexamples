#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxrnd.bas"
#include "library/b1r0f0_charmapminivaders01.bas"
#include "library/msxvideo9990command.bas"

msxvideo9990initsuperimpose():msxvideo9990cleanregisters():msxvideo9990vdp(8,$C2)

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim wm as uinteger at $C032
dim fc as uinteger at $C034
dim seed as uinteger at $C036
dim ik as uinteger at $C038
dim pp as uinteger at $C03A
dim e0 as uinteger at $C040
dim e1 as uinteger at $C042
dim e2 as uinteger at $C044
dim e3 as uinteger at $C046

msxvideo9990vdp(6,$81):msxvideo9990vdp(7,$08):out $67,1

for i=0 to 15:msxvideo9990palette12bit(i,i*$111):next
msxvideo9990palette12bit(0,0)

for i=0 to 15:msxvideo9990palette12bit(i,peek(uinteger,@palette01+i*2)):next

msxvideo9990command(0,0,0,0,192,290,0,$0C,$FFFF,$1111*13,$20)    '-cls beta

msxvideo9990vpokelocp(0,$0A04)

for x=0 to 8000:msxvideo9990vpokeseq($55):next
for x=0 to 4000:msxvideo9990vpokeseq($AA):next
for x=0 to 2000:msxvideo9990vpokeseq($FF):next

for x=0 to 15
seed=msxrnd(seed):dx=8*(4+x)
seed=msxrnd(seed):dy=8*23
seed=msxrnd(seed):fc=$1111*(x)
msxvideo9990command(0,0,dx,dy,8,8,0,$0C,$FFFF,fc,$20)
next

for e0=0 to 15:for e1=0 to 15
putchar(7+e0,1+e1,code("1"),e0,e1,0,0,@charmap01-256)
next:next

putchar(5,7,code("Y"),1,5,2,0,@charmap01-256)

writetext(3,21,@text01,@text01end-@text01,9,11,2,@charmap01-256)

msxvideo9990command(0,0,8*1,8*9,8,8,0,$0C,$FFFF,0,$10)
ik=1:pp=14:seed=code("Z")-32:for x=0 to 7:ikpp4b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next

for x=1 to 50
a=300-x
putchar(2,16,48+(int(x/100) mod 10),15,1,0,0,@charmap01-256)
putchar(3,16,48+(int(x/10) mod 10),15,1,0,0,@charmap01-256)
putchar(4,16,48+(x mod 10),15,1,0,0,@charmap01-256)
asm
halt
end asm
next

for x=0 to 289
seed=msxrnd(seed):fc=$1111*(seed mod 16)
msxvideo9990command(0,0,0,x,192,1,0,$0C,$FFFF,fc,$20)
next

do
seed=msxrnd(seed):dx=8*(seed mod 24)
seed=msxrnd(seed):dy=8*(seed mod 36)
seed=msxrnd(seed):fc=$1111*(seed mod 16)
msxvideo9990command(0,0,dx,dy,8,8,0,$0C,$FFFF,fc,$20)

seed=msxrnd(seed):dx=8*(seed mod 24)
seed=msxrnd(seed):dy=8*(seed mod 36)
msxvideo9990command(0,0,dx,dy,8,8,0,$0C,$FFFF,0,$10)
seed=msxrnd(seed):ik=seed mod 16
seed=msxrnd(seed):pp=seed mod 16
seed=msxrnd(seed)
for x=0 to 7:ikpp4b(peek(@charmap01+x+((seed mod 74)*8)),ik,pp):next

seed=msxrnd(seed)
for x=0to 31
  i=peek(@charmap01+x+((seed band $FF)*32) )
  out $62,i
  next

loop

'lmmv
msxvideo9990command(0,0,5*8,7*8,8,8,0,$0C,$FFFF,$1111*7,$20)

do:loop

patterntest01:
palette01:
asm
defw $000,$125,$725,$879
defw $A53,$085,$F04,$554
defw $F7A,$FA0,$CCC,$0E3
defw $FCA,$2AF,$FE2,$FFE
end asm

text01:
asm
defb "HELLO WORLD!"
end asm
text01end:

sub ikpp4b(tch1 as ubyte,tik1 as ubyte,tpp1 as ubyte):
  dim tlp0 as ubyte at $C000
  dim tlp1 as ubyte at $C001
  dim tmk1 as ubyte at $C002
  dim tov1 as uinteger at $C004
  tmk1=128
  for tlp0=0 to 7 step 2
    tov1=0
    for tlp1=0 to 1
      tov1=tov1*16
      if (tmk1 band tch1)<>0 then:
        tov1=tov1 bor (tik1 band 15)
      else
        tov1=tov1 bor (tpp1 band 15)
        end if
      tmk1=tmk1/2
      next
    out $62,tov1
    next
  end sub

sub putchar(txp0 as uinteger,typ0 as uinteger,tch0 as integer,tik0 as ubyte,tpp0 as ubyte,tyof0 as uinteger,tcho0 as uinteger,tbma0 as uinteger)
  dim tln0 as uinteger at $C006
  msxvideo9990command(0,0,8*txp0,(8*typ0)+tyof0,8,8,0,$0C,$FFFF,0,$10)
  for tln0=0 to 7:ikpp4b(peek(tbma0+tln0+((tch0+tcho0)*8)),tik0,tpp0):next
  end sub

sub writetext(txp2 as uinteger,typ2 as uinteger,ttxad2 as uinteger,ttxln2 as uinteger,tik2 as ubyte,tpp2 as ubyte,tyof2 as uinteger,tbma2 as uinteger)
  dim ttlp0 as uinteger at $C008
  for ttlp0=0 to ttxln2-1
    putchar(txp2+ttlp0,typ2,peek(ttxad2+ttlp0),tik2,tpp2,tyof2,0,tbma2)
    next
  end sub


