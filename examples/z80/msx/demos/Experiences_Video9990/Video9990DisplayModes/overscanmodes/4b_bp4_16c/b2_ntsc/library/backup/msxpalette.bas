sub msxpalette(tid as uinteger, thex as uinteger):  '- format GRB $0777
  asm
    di
    ld a,(ix+4)
	out ($99),a
	ld a,$90
	out ($99),a
	ld a,(ix+6)
	out ($9a),a
	ld a,(ix+7)
	out ($9a),a
    ei
    end asm
  end sub

sub msxpalettef(tidq as uinteger, thexq as uinteger):  '- format RGB $0777
  msxpalette(tidq, ((thexq band $0700)/16) bor ((thexq band $0070)*16) bor (thexq band $0007) )
  end sub

sub msxpalettehx(tidq as uinteger, thexq as uinteger):  '- format RGB $0FFF
  msxpalette(tidq, ((thexq band $0E00)/32) bor ((thexq band $00E0)*8) bor ((thexq band $000E)/2) )
  end sub


'-------------------------------------------------------------------------------
'SETPLT
'Address  : #014D
'Function : Sets the color code to the palette
'Input    : D  - Colorcode
'           E  - xxxxGGGG
'           A  - RRRRBBBB
'Registers: AF
'-------------------------------------------------------------------------------
'INIPLT
'Address  : #0141
'Function : Initialises the palette (current palet is save in VRAM)
'Registers: AF, BC, DE
'-------------------------------------------------------------------------------
'- color - color v_ink,v_paper,v_border 
'- ld hl,0xF3E9;ld [hl],v_ink;inc hl  #- forclr
'- ld [hl],v_paper;inc hl             #- bakclr
'- ld [hl],v_border                   #- borclr
'- 
'-------------------------------------------------------------------------------
'- ;-------------------------------------------------------
'- ;- msx2 palette
'- 
'- msx2palt:
'- 
'- ld a,[$2d] ; read MSX version
'- cp 1 ; is it MSX1?
'- ret c ; then there's no need to use it anyway
'- 
'- ld a,[7] ; get first VDP write port
'- ld c,a
'- inc c ; prepare to write register data
'- di ; interrupts could screw things up
'- xor a ; from color 0
'- out [c],a
'- ld a,128+16 ; write R#16
'- out [c],a
'- ei
'- inc c ; prepare to write palette data
'- ld b,32 ; 16 color * 2 bytes for palette data
'- ld hl,palette
'- otir
'- ret
'- ;
'- ; the format of the palette is like $GRB
'- ; and R, G and B must be between 0-7
'- ; currently it's the default MSX2 palette
'- ; but you set up your own in these dw's
'- ;
'- palette:
'- 
'- dw $707,$000,$010,$111,$121,$222,$232,$333
'- dw $343,$444,$454,$555,$565,$666,$676,$777
'- 
'-------------------------------------------------------------------------------

'  asm
'    call $0141
'    ld d,14
'    ld e,$12
'    ld a,$34
'    call $014D
'    end asm

'  asm
'    ld d,(ix+4)
'    ld e,(ix+7)
'    ld a,(ix+6)
'    call $014D
'    end asm



