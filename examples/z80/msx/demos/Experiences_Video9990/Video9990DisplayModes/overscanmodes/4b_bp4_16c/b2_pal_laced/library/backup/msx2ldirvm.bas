sub msx1ldirvm(tlen as uinteger, tvram as uinteger, tram as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)
    ld d,(ix+7)
    ld e,(ix+6)
    ld h,(ix+9)
    ld l,(ix+8)
    call $0059
    end asm
  end sub

'-------------------------------------------------------------------------------
'LDIRMV
'Address  : #0059
'Function : Block transfer to memory from VRAM
'Input    : BC - blocklength
'           DE - Start address of memory
'           HL - Start address of VRAM
'Registers: All
