'- needs msx2vpeek.bas, msx2vpoke.bas
'- doesnt work?

sub msx2pputcharlum8x8(tx as uinteger,ty as uinteger,tch as uinteger,tcl as uinteger,tyof as uinteger,tchmadr as uinteger,tidfl as uinteger):
  dim tyq as ubyte at $E500:dim txq as ubyte at $E502:dim tcq as ubyte at $E504
  dim tuq as uinteger at $E506:dim tuq2 as uinteger at $E508:dim txqe as ubyte at $E50A
  for tyq=0 to 7
    tcq=peek(tchmadr+tyq+(tch*8)):txqe=128
    for txq=0 to 7
      if (tcq band txqe)<>0 then:
        tuq=msx2vpeek((((ty*8)+tyq+tyof)*256)+(tx*8)+txq)
        tuq2=((tuq band 7) bor ((tcl band 15)*16)) bor ((tidfl band 1)*8)
        msx2vpoke((((ty*8)+tyq+tyof)*256)+(tx*8)+txq,tuq2)
        end if
      txqe=txqe >> 1
      next:next
  end sub

sub msx2pputcharlum4x8(tx as uinteger,ty as uinteger,tch as uinteger,tcl as uinteger,tyof as uinteger,tchmadr as uinteger,tidfl as uinteger):
  dim tyq as ubyte at $E500:dim txq as ubyte at $E502:dim tcq as ubyte at $E504
  dim tuq as uinteger at $E506:dim tuq2 as uinteger at $E508:dim txqe as ubyte at $E50A
  for tyq=0 to 7
    tcq=peek(tchmadr+tyq+int(tch/2)*8):txqe=128-((tch band 1)*120)
    for txq=0 to 3
      if (tcq band txqe)<>0 then:
        tuq=msx2vpeek((((ty*8)+tyq+tyof)*256)+(tx*4)+txq)
        tuq2=((tuq band 7) bor ((tcl band 15)*16)) bor ((tidfl band 1)*8)
        msx2vpoke((((ty*8)+tyq+tyof)*256)+(tx*4)+txq,tuq2)
        end if
      txqe=txqe >> 1
      next:next
  end sub

sub msx2pputcharlum4x6(tx as uinteger,ty as uinteger,tch as uinteger,tcl as uinteger,tyof as uinteger,tchmadr as uinteger,tidfl as uinteger):
  dim tyq as ubyte at $E500:dim txq as ubyte at $E502:dim tcq as ubyte at $E504
  dim tuq as uinteger at $E506:dim tuq2 as uinteger at $E508:dim txqe as ubyte at $E50A
  for tyq=0 to 5
    tcq=peek(tchmadr+tyq+int(tch/2)*6):txqe=128-((tch band 1)*120)
    for txq=0 to 3
      if (tcq band txqe)<>0 then:
        tuq=msx2vpeek((((ty*6)+tyq+tyof)*256)+(tx*4)+txq)
        tuq2=((tuq band 7) bor ((tcl band 15)*16)) bor ((tidfl band 1)*8)
        msx2vpoke((((ty*6)+tyq+tyof)*256)+(tx*4)+txq,tuq2)
        end if
      txqe=txqe >> 1
      next:next
  end sub

