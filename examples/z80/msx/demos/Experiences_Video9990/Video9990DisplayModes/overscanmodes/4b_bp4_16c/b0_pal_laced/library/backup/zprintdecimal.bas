sub zprintdecimal(tpos as uinteger,tval as uinteger):
  msx2vpoke(6144+tpos+0,48+int(tval/10000) mod 10)
  msx2vpoke(6144+tpos+1,48+int(tval/1000) mod 10)
  msx2vpoke(6144+tpos+2,48+int(tval/100) mod 10)
  msx2vpoke(6144+tpos+3,48+int(tval/10) mod 10)
  msx2vpoke(6144+tpos+4,48+tval mod 10)
  end sub
