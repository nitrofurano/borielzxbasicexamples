sub msx1vpoke(tad as uinteger,tva as uinteger)
  asm
    ld h,(ix+5)
    ld l,(ix+4)
    ld a,(ix+6)
    call $004D
    end asm
  end sub

'-------------------------------------------------------------------------------
'sub fastcall msxvpoke(tad as uintenger,tva as ubyte)
'  asm
'    pop hl
'    pop af
'    call $004D
'    end asm
'  end sub
'-------------------------------------------------------------------------------
'   vpoke - vpoke v_address,v_value 
'   ld hl,v_address
'   ld a,v_value
'   call 0x004D    #- wrtvrm
'-------------------------------------------------------------------------------
