goto charmap01end:
charmap01:
asm
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00

  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55
  defb $55,$55,$55,$55

  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $F0,$00,$00,$00
  defb $FF,$00,$00,$0F
  defb $FF,$F0,$00,$FF
  defb $FF,$FF,$0F,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$F0
  defb $FF,$FF,$FF,$00
  defb $FF,$FF,$F0,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$F0,$00
  defb $FF,$FF,$FF,$00
  defb $FF,$FF,$FF,$F0

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$F0,$FF,$FF
  defb $FF,$00,$0F,$FF
  defb $F0,$00,$00,$FF
  defb $00,$00,$00,$0F

  defb $0F,$FF,$FF,$FF
  defb $00,$FF,$FF,$FF
  defb $00,$0F,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$0F,$FF,$FF
  defb $00,$FF,$FF,$FF
  defb $0F,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $00,$00,$00,$00
  defb $00,$00,$00,$0F
  defb $00,$00,$00,$FF
  defb $00,$00,$0F,$FF
  defb $00,$00,$FF,$FF
  defb $00,$0F,$FF,$FF
  defb $00,$FF,$FF,$FF
  defb $0F,$FF,$FF,$FF

  defb $00,$00,$00,$00
  defb $F0,$00,$00,$00
  defb $FF,$00,$00,$00
  defb $FF,$F0,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$F0,$00
  defb $FF,$FF,$FF,$00
  defb $FF,$FF,$FF,$F0

  defb $F0,$00,$00,$00
  defb $FF,$00,$00,$0F
  defb $FF,$F0,$00,$FF
  defb $FF,$FF,$0F,$FF
  defb $FF,$FF,$0F,$FF
  defb $FF,$F0,$00,$FF
  defb $FF,$00,$00,$0F
  defb $F0,$00,$00,$00

  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0

  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0
  defb $0F,$0F,$0F,$0F
  defb $F0,$F0,$F0,$F0

  defb $0F,$0F,$FF,$FF
  defb $F0,$F0,$FF,$FF
  defb $0F,$0F,$FF,$FF
  defb $F0,$F0,$FF,$FF
  defb $0F,$0F,$FF,$FF
  defb $F0,$F0,$FF,$FF
  defb $0F,$0F,$FF,$FF
  defb $F0,$F0,$FF,$FF

  defb $FF,$FF,$0F,$0F
  defb $FF,$FF,$F0,$F0
  defb $FF,$FF,$0F,$0F
  defb $FF,$FF,$F0,$F0
  defb $FF,$FF,$0F,$0F
  defb $FF,$FF,$F0,$F0
  defb $FF,$FF,$0F,$0F
  defb $FF,$FF,$F0,$F0

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00

  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00

  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00

  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF

  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF

  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF

  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00

  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00

  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$FF,$FF
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00

  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00
  defb $00,$00,$00,$00

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$55,$FF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$FA,$AA,$AF
  defb $FF,$55,$55,$FF
  defb $FF,$FA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$F5,$FF
  defb $F5,$00,$A5,$AF
  defb $FF,$AA,$AF,$AF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$F5,$FF
  defb $55,$00,$05,$0F
  defb $F0,$00,$A0,$AA
  defb $F5,$00,$A5,$AF
  defb $55,$00,$05,$0F
  defb $F0,$00,$A0,$AA
  defb $FF,$AA,$AF,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$5F,$FF
  defb $55,$50,$00,$5F
  defb $5A,$00,$0A,$AA
  defb $50,$50,$00,$5F
  defb $FA,$00,$0A,$0A
  defb $55,$50,$00,$5A
  defb $FA,$00,$0A,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$F5,$55
  defb $F5,$0A,$55,$0A
  defb $FF,$A0,$50,$AA
  defb $FF,$55,$0A,$AF
  defb $F5,$50,$A0,$5F
  defb $55,$0A,$A5,$0A
  defb $FA,$AA,$FF,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $05,$00,$A0,$FF
  defb $F0,$00,$0F,$AF
  defb $55,$00,$0A,$5F
  defb $50,$00,$A0,$FA
  defb $F0,$00,$0F,$0F
  defb $FF,$AA,$AA,$FA

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$55,$0A,$FF
  defb $FF,$5A,$AA,$FF
  defb $FF,$F0,$FF,$FF
  defb $FF,$FF,$AF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$5F
  defb $FF,$FF,$55,$FA
  defb $FF,$F5,$5A,$AF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$FF,$00,$FF
  defb $FF,$FF,$FA,$0F

  defb $F5,$FF,$FF,$FF
  defb $FF,$05,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$FA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$F5,$FF
  defb $FF,$00,$0F,$AF
  defb $55,$50,$00,$5F
  defb $FA,$00,$0A,$AA
  defb $F5,$50,$A0,$FF
  defb $FF,$AA,$AF,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$FF,$0A,$FF
  defb $FF,$FF,$FA,$FF
  defb $F5,$55,$55,$55
  defb $FF,$AA,$AA,$AA
  defb $FF,$FF,$5F,$FF
  defb $FF,$F5,$50,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $A5,$55,$55,$FF
  defb $FF,$00,$00,$AF
  defb $F5,$50,$0A,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $FF,$AA,$AA,$AF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $F5,$00,$0A,$FF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$55,$55
  defb $FF,$F5,$50,$0A
  defb $FF,$55,$00,$AA
  defb $F5,$50,$0A,$AF
  defb $55,$00,$AA,$FF
  defb $FA,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$5F
  defb $55,$00,$A0,$FA
  defb $00,$0A,$0F,$0F
  defb $50,$A0,$F0,$5A
  defb $5A,$0F,$A5,$0A
  defb $F0,$F0,$55,$AA
  defb $FF,$AF,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $55,$00,$0A,$FF
  defb $F0,$00,$0A,$FF
  defb $F5,$00,$0A,$FF
  defb $F5,$00,$0A,$FF
  defb $55,$00,$00,$FF
  defb $FA,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$00,$0F
  defb $FA,$AA,$00,$0A
  defb $FF,$55,$50,$AA
  defb $55,$50,$AA,$AF
  defb $50,$00,$05,$5F
  defb $FA,$AA,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$AA,$00,$0F
  defb $FA,$A5,$50,$AA
  defb $FF,$F5,$00,$0F
  defb $55,$FF,$00,$0A
  defb $F0,$05,$50,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$55,$FF
  defb $FF,$55,$00,$AF
  defb $F5,$5A,$00,$AF
  defb $55,$AA,$50,$AF
  defb $50,$05,$50,$0F
  defb $FA,$AA,$00,$AA
  defb $FF,$FF,$FA,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$00,$AA,$AA
  defb $50,$00,$05,$FF
  defb $FA,$AA,$00,$0F
  defb $55,$FF,$50,$0A
  defb $F0,$05,$50,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$5F
  defb $55,$00,$AA,$AA
  defb $50,$00,$05,$FF
  defb $50,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $F0,$00,$05,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$00,$00,$0A
  defb $FA,$A0,$00,$AA
  defb $FF,$55,$0A,$AF
  defb $F5,$50,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $F0,$00,$05,$AA
  defb $55,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $F0,$00,$05,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $F0,$00,$05,$0A
  defb $FF,$AA,$00,$0A
  defb $F5,$55,$50,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$F0,$AF,$FF
  defb $FF,$5F,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$55,$5F
  defb $FF,$55,$00,$AA
  defb $F5,$50,$0A,$AF
  defb $FF,$00,$00,$FF
  defb $FF,$F0,$00,$0F
  defb $FF,$FF,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$5F
  defb $FF,$AA,$AA,$AA
  defb $F5,$55,$55,$5F
  defb $FF,$AA,$AA,$AA
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $FF,$00,$00,$FF
  defb $FF,$F0,$00,$0F
  defb $FF,$55,$00,$AA
  defb $F5,$50,$0A,$AF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$A0,$00,$0F
  defb $FA,$A5,$00,$0A
  defb $FF,$55,$00,$AA
  defb $FF,$FA,$AA,$AF
  defb $FF,$55,$5F,$FF
  defb $FF,$FA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$AA,$00,$0F
  defb $50,$A5,$50,$0A
  defb $50,$A5,$00,$0A
  defb $50,$AF,$AA,$AA
  defb $F0,$05,$55,$FF
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$00,$05,$0A
  defb $50,$00,$A0,$0A
  defb $50,$00,$A5,$0A
  defb $FA,$AA,$AF,$AA

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $50,$00,$A0,$0F
  defb $50,$00,$05,$AA
  defb $50,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$00,$05,$AA
  defb $FA,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$0A,$AF,$AA
  defb $50,$0A,$FF,$FF
  defb $50,$00,$F5,$5F
  defb $F0,$00,$05,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FA,$AA,$FF
  defb $55,$55,$5F,$FF
  defb $50,$0A,$00,$FF
  defb $50,$0A,$F0,$0F
  defb $50,$0A,$F5,$0A
  defb $50,$0A,$55,$AA
  defb $50,$00,$5A,$AF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$00,$AA,$AA
  defb $50,$00,$05,$FF
  defb $50,$00,$AA,$AF
  defb $50,$00,$AF,$FF
  defb $50,$00,$05,$5F
  defb $FA,$AA,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$00,$AA,$AA
  defb $50,$00,$05,$FF
  defb $50,$00,$AA,$AF
  defb $50,$00,$AF,$FF
  defb $50,$00,$AF,$FF
  defb $FA,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$0A,$AF,$AA
  defb $50,$0A,$55,$5F
  defb $50,$0A,$F0,$0A
  defb $F0,$00,$55,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FA
  defb $55,$55,$FF,$5F
  defb $50,$00,$AF,$5A
  defb $50,$00,$05,$5A
  defb $50,$00,$AA,$0A
  defb $50,$00,$AF,$5A
  defb $50,$00,$AF,$5A
  defb $FA,$AA,$AF,$FA

  defb $FF,$AF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $F0,$00,$0A,$AF
  defb $F5,$00,$0A,$FF
  defb $F5,$00,$0A,$FF
  defb $F5,$00,$0A,$FF
  defb $55,$00,$00,$FF
  defb $FA,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$55,$5F
  defb $FF,$FF,$50,$0A
  defb $FF,$FF,$50,$0A
  defb $55,$FF,$50,$0A
  defb $50,$0F,$50,$0A
  defb $F0,$00,$50,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$AA,$AF
  defb $55,$5F,$55,$FF
  defb $50,$00,$5A,$AF
  defb $50,$00,$AA,$FF
  defb $50,$00,$0F,$FF
  defb $50,$0A,$00,$FF
  defb $50,$0A,$F0,$0F
  defb $FA,$AA,$FF,$AA

  defb $FF,$AA,$AA,$FF
  defb $55,$55,$FF,$FF
  defb $50,$00,$AF,$FF
  defb $50,$00,$AF,$FF
  defb $50,$00,$AF,$FF
  defb $50,$00,$AF,$FF
  defb $50,$00,$05,$5F
  defb $FA,$AA,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$FF,$5F
  defb $50,$0F,$F5,$5A
  defb $50,$00,$5F,$0A
  defb $50,$0A,$AA,$5A
  defb $50,$0A,$FF,$5A
  defb $50,$0A,$FF,$5A
  defb $FA,$AA,$FF,$FA

  defb $FF,$FF,$FF,$FF
  defb $55,$5F,$FF,$5F
  defb $50,$0A,$FF,$5A
  defb $50,$00,$FF,$5A
  defb $50,$00,$0F,$5A
  defb $50,$00,$A0,$5A
  defb $50,$00,$AF,$0A
  defb $FA,$AA,$AF,$FA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$00,$A5,$0A
  defb $50,$00,$A5,$0A
  defb $F0,$00,$05,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $50,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$00,$05,$AA
  defb $50,$00,$AA,$AF
  defb $50,$00,$AF,$FF
  defb $FA,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$0A,$0F,$0A
  defb $50,$00,$F0,$FA
  defb $F0,$00,$0F,$0F
  defb $FF,$AA,$AA,$FA

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $50,$00,$A0,$0F
  defb $50,$00,$A5,$0A
  defb $50,$00,$05,$AA
  defb $50,$00,$00,$AF
  defb $50,$00,$A0,$0F
  defb $FA,$AA,$AF,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$00,$AA,$AF
  defb $F0,$00,$05,$FF
  defb $FF,$AA,$00,$0F
  defb $55,$5F,$50,$0A
  defb $F0,$00,$50,$AA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $FA,$00,$0A,$AA
  defb $FF,$50,$0A,$FF
  defb $FF,$50,$0A,$FF
  defb $FF,$50,$0A,$FF
  defb $FF,$50,$0A,$FF
  defb $FF,$FA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$FF,$5F
  defb $50,$00,$AF,$5A
  defb $50,$00,$AF,$5A
  defb $50,$00,$AF,$5A
  defb $50,$00,$AF,$5A
  defb $F0,$00,$05,$FA
  defb $FF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$FF,$5F
  defb $50,$00,$AF,$5A
  defb $50,$00,$A5,$FA
  defb $50,$00,$A5,$AF
  defb $50,$00,$0F,$AF
  defb $50,$00,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$5F,$FF,$5F
  defb $50,$0A,$FF,$5A
  defb $50,$0A,$FF,$5A
  defb $50,$0A,$5F,$5A
  defb $50,$00,$50,$5A
  defb $F0,$0A,$A0,$AA
  defb $FF,$AA,$FF,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$5F,$F5,$5F
  defb $F0,$00,$55,$AA
  defb $FF,$00,$0A,$AF
  defb $FF,$50,$0A,$FF
  defb $F5,$50,$00,$FF
  defb $55,$0A,$A0,$0F
  defb $FA,$AA,$FF,$AA

  defb $FF,$FF,$FF,$FF
  defb $55,$5F,$F5,$5F
  defb $F0,$00,$55,$AA
  defb $FF,$00,$0A,$AF
  defb $FF,$50,$0A,$FF
  defb $FF,$50,$0A,$FF
  defb $FF,$50,$0A,$FF
  defb $FF,$FA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$00,$00,$0A
  defb $FA,$A0,$00,$AA
  defb $FF,$55,$0A,$AF
  defb $F5,$50,$00,$5F
  defb $55,$00,$00,$0A
  defb $FA,$AA,$AA,$AA

  defb $FF,$FA,$FF,$FF
  defb $FF,$55,$55,$5F
  defb $FF,$50,$00,$AA
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$0F
  defb $FF,$FA,$AA,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$FF,$FF
  defb $F0,$00,$0F,$FF
  defb $FF,$00,$00,$FF
  defb $FF,$F0,$00,$0F
  defb $FF,$FF,$00,$00
  defb $FF,$FF,$FA,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $FF,$00,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $FF,$50,$00,$AF
  defb $F5,$50,$00,$AF
  defb $AF,$AA,$AA,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$55,$FF
  defb $FF,$55,$00,$0F
  defb $F5,$50,$0A,$A0
  defb $FF,$AA,$AA,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $AF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$55

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$FF,$5F
  defb $FF,$55,$A5,$5A
  defb $FF,$50,$A5,$0A
  defb $FF,$5A,$A5,$AA
  defb $F5,$FA,$5F,$AF
  defb $FF,$AF,$FA,$FF
  defb $F5,$FF,$5F,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$55,$FF
  defb $F5,$0A,$50,$AF
  defb $FF,$0A,$F0,$AF
  defb $F5,$FA,$5F,$AF
  defb $FF,$AF,$FA,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$5F,$F5,$FF
  defb $55,$50,$55,$0F
  defb $FA,$0A,$A0,$AA
  defb $F5,$FA,$F5,$AF
  defb $F5,$AF,$5F,$AF
  defb $55,$05,$50,$5F
  defb $F0,$AA,$0A,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$5F,$FF
  defb $FF,$55,$50,$5F
  defb $FF,$5A,$0A,$AA
  defb $FF,$50,$50,$5F
  defb $FF,$FA,$0A,$0A
  defb $FF,$55,$50,$5A
  defb $FF,$FA,$0A,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$FF,$F5,$5F
  defb $5F,$0F,$55,$AA
  defb $F0,$F0,$5A,$AF
  defb $FF,$A5,$AA,$FF
  defb $FF,$55,$A5,$FF
  defb $F5,$5A,$0F,$0F
  defb $55,$AA,$F0,$FA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $F0,$05,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $55,$A0,$0F,$5F
  defb $50,$AF,$00,$5A
  defb $F0,$05,$50,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$FF,$0A,$FF
  defb $FF,$F5,$FA,$FF
  defb $FF,$FF,$AF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$55,$FF
  defb $FF,$F5,$5A,$AF
  defb $FF,$55,$AA,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$FF,$00,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$FF,$00,$FF
  defb $FF,$FF,$50,$AF
  defb $FF,$FF,$50,$AF
  defb $FF,$F5,$5A,$AF
  defb $FF,$55,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$55,$FF
  defb $FF,$00,$5A,$AF
  defb $55,$50,$00,$5F
  defb $FA,$00,$0A,$AA
  defb $F5,$5A,$00,$FF
  defb $FF,$AA,$FA,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$F5,$0A,$FF
  defb $F5,$55,$00,$5F
  defb $FF,$A0,$0A,$AA
  defb $FF,$F5,$0A,$FF
  defb $FF,$FF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$F0,$AF,$FF
  defb $FF,$5F,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$5F
  defb $FF,$AA,$AA,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $F5,$0A,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$5F
  defb $FF,$FF,$F5,$5A
  defb $FF,$FF,$55,$AA
  defb $FF,$F5,$5A,$AF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $55,$AA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $55,$AA,$00,$FF
  defb $50,$A5,$F0,$0F
  defb $50,$A5,$A5,$0A
  defb $50,$A5,$A5,$0A
  defb $F0,$0F,$A5,$0A
  defb $FF,$00,$55,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$55,$0A,$FF
  defb $FF,$F0,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $F5,$55,$00,$5F

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $55,$AA,$A0,$0F
  defb $FA,$AF,$55,$AA
  defb $FF,$55,$5A,$AF
  defb $F5,$5A,$AA,$FF
  defb $55,$AA,$F5,$5F
  defb $50,$05,$55,$0A

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $FA,$AA,$00,$AA
  defb $FF,$F5,$50,$AF
  defb $FF,$FF,$A0,$0F
  defb $FF,$FF,$F5,$0A
  defb $55,$FF,$55,$AA
  defb $F0,$05,$5A,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $F5,$5A,$05,$FF
  defb $F5,$0A,$50,$AF
  defb $55,$00,$50,$0F
  defb $FA,$AA,$00,$AA
  defb $FF,$FF,$50,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $50,$AA,$AA,$AF
  defb $50,$05,$55,$FF
  defb $FA,$AA,$A0,$0F
  defb $FF,$FF,$F5,$0A
  defb $55,$FF,$55,$AA
  defb $F0,$05,$5A,$AF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$55,$FF
  defb $F5,$5A,$AA,$AF
  defb $55,$00,$FF,$FF
  defb $50,$AA,$05,$FF
  defb $50,$AF,$F0,$0F
  defb $50,$AF,$F5,$0A
  defb $F0,$05,$55,$AA

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$5F
  defb $50,$AA,$A0,$0A
  defb $FA,$AF,$55,$AA
  defb $FF,$F5,$5A,$AF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $55,$AA,$00,$FF
  defb $F0,$05,$50,$AF
  defb $55,$AA,$A0,$0F
  defb $50,$AF,$F5,$0A
  defb $F0,$0F,$F5,$0A
  defb $FF,$00,$55,$AA

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $55,$AA,$00,$FF
  defb $50,$AF,$F0,$0F
  defb $F0,$0F,$F5,$0A
  defb $FF,$00,$55,$0A
  defb $FF,$FA,$A0,$0A
  defb $F5,$55,$55,$AA

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$AA,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $F5,$0A,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$AA,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $FF,$0A,$FF,$FF
  defb $F5,$FA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$55,$FF
  defb $FF,$FF,$5A,$AF
  defb $FF,$55,$FA,$FF
  defb $55,$5A,$AF,$FF
  defb $FA,$00,$FF,$FF
  defb $FF,$FA,$0F,$FF
  defb $FF,$FF,$50,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$5F
  defb $FF,$AA,$AA,$AA
  defb $F5,$55,$55,$5F
  defb $FF,$AA,$AA,$AA
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $FF,$0A,$FF,$FF
  defb $FF,$F0,$5F,$FF
  defb $FF,$FF,$00,$5F
  defb $FF,$F5,$5A,$AA
  defb $FF,$5F,$AA,$FF
  defb $F5,$5A,$FF,$FF

  defb $FA,$AA,$AA,$AA
  defb $F5,$55,$55,$FF
  defb $55,$AA,$A0,$0F
  defb $FA,$AF,$55,$AA
  defb $FF,$F5,$5A,$AF
  defb $FF,$FF,$AA,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$55,$FF
  defb $5F,$AA,$AA,$0F
  defb $5A,$55,$5F,$5A
  defb $5A,$5A,$AA,$5A
  defb $5A,$50,$5F,$5A
  defb $5A,$FA,$AA,$5A
  defb $F0,$55,$55,$FA

  defb $FF,$AF,$FA,$FF
  defb $FF,$F5,$55,$5F
  defb $FF,$55,$A0,$0A
  defb $F5,$5A,$A5,$0A
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$0A
  defb $F0,$0A,$A0,$0A
  defb $55,$0A,$F5,$0A

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $F0,$0A,$00,$FF
  defb $F5,$0A,$50,$AF
  defb $55,$00,$50,$0F
  defb $F0,$0A,$A0,$0A
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$AA

  defb $FF,$AF,$FA,$FF
  defb $F5,$55,$F5,$FF
  defb $55,$AA,$05,$AF
  defb $50,$AF,$FA,$AF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $F0,$0F,$F5,$5F
  defb $FF,$00,$55,$AA

  defb $FF,$FF,$FA,$FF
  defb $55,$55,$5F,$FF
  defb $F0,$0A,$00,$FF
  defb $F5,$0A,$F0,$0F
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$AA

  defb $FA,$AF,$FF,$AF
  defb $55,$55,$55,$FF
  defb $F0,$0A,$00,$AF
  defb $F5,$0A,$FA,$AF
  defb $55,$00,$55,$5F
  defb $F0,$0A,$AA,$AA
  defb $F5,$0A,$F5,$5F
  defb $55,$00,$55,$0A

  defb $FF,$AA,$AA,$AF
  defb $55,$55,$55,$FF
  defb $F0,$0A,$00,$AF
  defb $F5,$0A,$FA,$AF
  defb $55,$00,$55,$5F
  defb $F0,$0A,$AA,$AA
  defb $F5,$0A,$FF,$FF
  defb $55,$0A,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$F5,$FF
  defb $55,$AA,$05,$AF
  defb $50,$AF,$FA,$AF
  defb $50,$A5,$55,$5F
  defb $50,$AF,$A0,$0A
  defb $F0,$0F,$55,$0A
  defb $FF,$00,$5A,$0A

  defb $FF,$FF,$FA,$AF
  defb $55,$5F,$F5,$5F
  defb $F0,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$0A
  defb $F0,$0A,$A0,$0A
  defb $F5,$0A,$F5,$0A
  defb $55,$0A,$F5,$0A

  defb $FF,$FA,$AF,$FF
  defb $FF,$55,$55,$FF
  defb $FF,$F0,$0A,$AF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$00,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$F5,$55,$5F
  defb $F5,$0F,$A0,$0A
  defb $FF,$AA,$F5,$0A
  defb $FF,$FF,$F5,$0A
  defb $FF,$FF,$F5,$0A
  defb $55,$FF,$55,$AA
  defb $F0,$05,$5A,$AF

  defb $FF,$FF,$FF,$FF
  defb $55,$5F,$F5,$FF
  defb $F0,$0A,$55,$AF
  defb $55,$00,$5A,$AF
  defb $F0,$00,$0A,$FF
  defb $F5,$0A,$00,$FF
  defb $F5,$0A,$F0,$0F
  defb $55,$0A,$F5,$0A

  defb $FF,$FA,$FF,$FF
  defb $55,$5F,$FF,$FF
  defb $F0,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$F5,$5F
  defb $55,$00,$55,$0A

  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$F5,$5F
  defb $F0,$0F,$55,$0A
  defb $F5,$00,$50,$0A
  defb $F5,$00,$A0,$0A
  defb $F5,$A0,$A5,$0A
  defb $F5,$AF,$A5,$0A
  defb $55,$AF,$F5,$0A

  defb $FF,$AA,$FF,$FF
  defb $55,$5F,$F5,$5F
  defb $F0,$00,$F5,$0A
  defb $F5,$00,$A5,$0A
  defb $F5,$0A,$05,$0A
  defb $F5,$0A,$50,$0A
  defb $F5,$0A,$F0,$0A
  defb $55,$0A,$F5,$0A

  defb $FA,$AF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $55,$AA,$00,$FF
  defb $50,$AF,$F0,$0F
  defb $50,$AF,$F5,$0A
  defb $50,$AF,$F5,$0A
  defb $F0,$0F,$F5,$0A
  defb $FF,$00,$55,$AA

  defb $FF,$FA,$AA,$AF
  defb $55,$55,$5F,$FF
  defb $F0,$0A,$00,$FF
  defb $F5,$0A,$F0,$0F
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$AA
  defb $F0,$0A,$AA,$AF
  defb $55,$0A,$FF,$FF

  defb $FF,$AA,$AA,$AA
  defb $F5,$55,$5F,$FF
  defb $55,$AA,$00,$FF
  defb $50,$AF,$F0,$0F
  defb $50,$A5,$F5,$0A
  defb $50,$A5,$05,$0A
  defb $F0,$0F,$00,$0A
  defb $FF,$00,$50,$0A

  defb $FA,$AA,$AA,$AA
  defb $55,$55,$5F,$FF
  defb $F0,$0A,$00,$FF
  defb $F5,$0A,$F0,$0F
  defb $F5,$0A,$F5,$0A
  defb $55,$00,$55,$AA
  defb $F0,$0A,$00,$0F
  defb $55,$0A,$F0,$0A

  defb $FF,$AA,$AA,$FF
  defb $F5,$55,$55,$FF
  defb $55,$AA,$AA,$AF
  defb $F0,$05,$5F,$FF
  defb $FF,$AA,$00,$FF
  defb $FF,$FF,$F0,$0F
  defb $55,$FF,$F5,$0A
  defb $F0,$05,$55,$AA

  defb $FF,$FF,$FA,$AF
  defb $55,$55,$55,$5F
  defb $50,$A0,$0A,$AA
  defb $FA,$A5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$0A,$FF

  defb $FF,$AA,$AA,$FF
  defb $55,$5F,$F5,$5F
  defb $F0,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $FF,$00,$F5,$0A
  defb $FF,$F0,$05,$AA

  defb $FF,$AA,$AA,$AF
  defb $55,$5F,$F5,$5F
  defb $F0,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$F5,$0A
  defb $F5,$0A,$55,$AA
  defb $F5,$00,$5A,$AF

  defb $FF,$FF,$AA,$FF
  defb $55,$FF,$F5,$5F
  defb $F0,$AF,$F5,$0A
  defb $F5,$A5,$F5,$0A
  defb $F5,$05,$A5,$0A
  defb $F5,$00,$05,$0A
  defb $F5,$0A,$00,$0A
  defb $55,$AA,$F0,$0A

  defb $FF,$FA,$AA,$AF
  defb $55,$5F,$F5,$5F
  defb $FA,$00,$55,$0A
  defb $FF,$F0,$0A,$AA
  defb $FF,$55,$00,$FF
  defb $F5,$5A,$A0,$0F
  defb $F5,$0A,$F5,$0A
  defb $55,$0A,$F5,$0A

  defb $FF,$AA,$AA,$AF
  defb $55,$5F,$F5,$5F
  defb $F0,$0A,$FF,$0A
  defb $FF,$00,$F5,$5A
  defb $FF,$50,$05,$AA
  defb $FF,$F0,$0A,$AF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$0A,$FF

  defb $FF,$AA,$FF,$FF
  defb $F5,$55,$55,$5F
  defb $55,$AA,$A0,$0A
  defb $FA,$AF,$55,$AA
  defb $FF,$F5,$5A,$AF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$A5,$5F
  defb $F5,$00,$55,$0A

  defb $FF,$A5,$55,$5F
  defb $FF,$55,$AA,$AA
  defb $FF,$FA,$AF,$F5
  defb $FF,$FF,$FF,$5F
  defb $FF,$FF,$55,$F0
  defb $F5,$FF,$FA,$05
  defb $F5,$A5,$F5,$F0
  defb $F5,$A5,$AF,$0F

  defb $FF,$FF,$FA,$AF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $AF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $AF,$FF,$FF,$FF
  defb $AF,$FF,$FF,$FF
  defb $AF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$AA,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FA,$AF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$AA,$AA,$AF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FA,$AA,$FF,$AA
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FA,$AA,$AA,$AF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FA,$AA,$AF
  defb $55,$F5,$5F,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FA,$AA,$AA,$AF
  defb $FF,$5F,$FF,$FF
  defb $55,$50,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $FA,$0A,$AA,$FF

  defb $FA,$AA,$AA,$AA
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$05,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $55,$A0,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FA,$AA,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $F0,$05,$AA,$FF
  defb $F5,$00,$05,$FF
  defb $55,$A0,$0A,$AF
  defb $F0,$05,$00,$FF
  defb $FF,$AA,$AA,$AF

  defb $FF,$FA,$AA,$FA
  defb $FF,$FF,$55,$FF
  defb $FF,$F5,$5A,$AF
  defb $FF,$FF,$AA,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FA,$AA,$FF,$AA
  defb $FF,$55,$FF,$FF
  defb $F5,$5A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$00,$FF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$FA,$AA,$AF
  defb $FF,$55,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$AA,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$AA,$AA,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $F0,$05,$AA,$FF
  defb $55,$00,$0F,$FF
  defb $F0,$00,$AA,$FF
  defb $55,$A0,$0F,$FF
  defb $FA,$AF,$AA,$FF

  defb $FA,$AA,$FF,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $55,$50,$05,$FF
  defb $FA,$00,$AA,$AF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FA,$AA,$AA,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$5A,$AF,$FF

  defb $FA,$AF,$FF,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $FA,$AA,$AA,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FA,$AA,$FF,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$FA,$AA,$AF
  defb $FF,$FF,$5F,$FF
  defb $FF,$F5,$5A,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $55,$AA,$FF,$FF
  defb $5A,$AF,$FF,$FF
  defb $FA,$FF,$FF,$FF

  defb $FA,$AA,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FA,$AA,$AA
  defb $F5,$55,$FF,$FF
  defb $F5,$00,$AF,$FF
  defb $FF,$00,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FA,$AA,$FF,$AA
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $FA,$A5,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$AA,$AA,$AF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $FA,$05,$0A,$FF
  defb $FF,$F0,$0A,$FF
  defb $55,$F5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FA,$AA,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$00,$0A,$FF
  defb $FA,$A0,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$FF,$AA,$FF

  defb $FF,$FF,$AA,$AF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$F5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$AA,$AA,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FA,$AF,$FF,$AA
  defb $55,$55,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$AA,$FF,$FF

  defb $FA,$AA,$FF,$AA
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $F0,$05,$AA,$FF
  defb $55,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FA,$AA,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$AA,$AA,$AA
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$AF,$AF,$FA
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$FA,$AF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$5A,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $FF,$00,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$FF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $FF,$AA,$AA,$FF
  defb $F5,$55,$5F,$FF
  defb $FF,$AA,$AA,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $FF,$00,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $FF,$AA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $FA,$05,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $FF,$AA,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $FF,$AA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $50,$05,$0A,$FF
  defb $50,$00,$0A,$FF
  defb $50,$AA,$AA,$FF
  defb $F0,$05,$FF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$05,$AA,$FF
  defb $50,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FA,$FF,$FF
  defb $55,$55,$FF,$FF
  defb $50,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$AA,$FF
  defb $FA,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$FF,$FF
  defb $50,$AA,$AF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$FF,$FF
  defb $50,$AA,$AF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $FA,$AF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$A5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $FF,$00,$AA,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $F5,$50,$0F,$FF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $55,$F5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$0F,$AA,$FF
  defb $50,$A0,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FA,$FF,$FF
  defb $55,$FF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$AF,$FF,$FF
  defb $FA,$AF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$AA,$FF
  defb $50,$05,$0F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$AA,$FF
  defb $50,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF
  defb $FA,$00,$AA,$AF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $F0,$05,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $F0,$05,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $55,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $F0,$00,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $F5,$00,$AF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $FF,$55,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $55,$AA,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FA,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $F5,$0A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$00,$FF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$FF,$FF
  defb $F0,$0F,$FF,$FF
  defb $FF,$00,$FF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$FF,$00,$FF
  defb $FF,$FF,$FA,$AF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $FF,$00,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $F5,$50,$AF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$5F,$FF,$FF
  defb $F5,$50,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $5A,$AF,$0A,$FF
  defb $FA,$FF,$FA,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$55,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$5F,$FF
  defb $F5,$5A,$AA,$FF
  defb $55,$00,$FF,$FF
  defb $F0,$0A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $55,$00,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $55,$55,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $F5,$0A,$AA,$FF
  defb $F5,$00,$FF,$FF
  defb $F5,$0A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$AA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$5F,$FF
  defb $FF,$00,$AA,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $F5,$50,$0F,$FF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$F5,$5F,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $FF,$F5,$0A,$FF
  defb $55,$F5,$0A,$FF
  defb $50,$05,$0A,$FF

  defb $FF,$FF,$FF,$FF
  defb $55,$FF,$FF,$FF
  defb $50,$A5,$5F,$FF
  defb $50,$05,$AA,$FF
  defb $50,$00,$AF,$FF
  defb $50,$A0,$0F,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$00,$5F,$FF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$05,$0A,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$AF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$A0,$0A,$FF
  defb $FF,$F5,$00,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$A0,$0A,$FF
  defb $50,$AF,$AA,$FF
  defb $50,$AF,$FF,$FF
  defb $50,$AF,$FF,$FF
  defb $FA,$AF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $50,$AA,$AA,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$5F,$FF,$FF
  defb $55,$00,$FF,$FF
  defb $F0,$0A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$00,$5F,$FF
  defb $FF,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $F0,$05,$AA,$FF
  defb $F5,$00,$AF,$FF
  defb $FF,$0A,$AF,$FF
  defb $FF,$FA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$05,$0A,$FF
  defb $50,$00,$0A,$FF
  defb $50,$00,$0A,$FF
  defb $F0,$A0,$AA,$FF
  defb $FF,$AF,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $F0,$05,$AA,$FF
  defb $FF,$0A,$AF,$FF
  defb $F5,$50,$FF,$FF
  defb $55,$A0,$0F,$FF
  defb $FA,$AF,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$F5,$5F,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$A5,$0A,$FF
  defb $50,$05,$0A,$FF
  defb $FA,$A0,$0A,$FF
  defb $55,$55,$0A,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $55,$55,$5F,$FF
  defb $FA,$00,$AA,$FF
  defb $F5,$5A,$AF,$FF
  defb $55,$AA,$FF,$FF
  defb $50,$05,$5F,$FF
  defb $FA,$AA,$AA,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $F5,$0A,$AF,$FF
  defb $55,$AA,$FF,$FF
  defb $F0,$0F,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$00,$FF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$55,$FF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$50,$AF,$FF
  defb $FF,$FA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $F5,$55,$FF,$FF
  defb $FF,$00,$AF,$FF
  defb $FF,$F0,$0F,$FF
  defb $FF,$55,$AA,$FF
  defb $FF,$50,$AF,$FF
  defb $F5,$50,$AF,$FF
  defb $FF,$AA,$AF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $5F,$FF,$5F,$FF
  defb $50,$F5,$5A,$FF
  defb $F0,$05,$AA,$FF
  defb $FF,$0A,$AF,$FF
  defb $FF,$FA,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FA,$AA,$AA,$AF
  defb $F5,$5F,$FF,$FF
  defb $55,$00,$FF,$FF
  defb $50,$00,$AF,$FF
  defb $F0,$0A,$AF,$FF
  defb $F5,$0A,$FF,$FF
  defb $F5,$0A,$FF,$FF
  defb $FF,$AA,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF


end asm
charmap01end:

