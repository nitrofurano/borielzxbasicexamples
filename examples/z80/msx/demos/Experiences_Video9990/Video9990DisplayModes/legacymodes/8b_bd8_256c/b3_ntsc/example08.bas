#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990setregisters.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxvideo9990vdokeseq.bas"
#include "library/msxrnd.bas"
#include "library/b1r0f0_charmapminivaders01.bas"

msxvideo9990initsuperimpose():msxvideo9990cleanregisters():msxvideo9990vdp( 8,$C2)

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim wm as uinteger at $C032
dim fc as uinteger at $C034
dim seed as uinteger at $C036
dim ik as uinteger at $C038
dim pp as uinteger at $C03A
dim ctpi as float at $C080

wm=$FFFF:fc=%0100000100000100

msxvideo9990vdp( 6,$96):msxvideo9990vdp(13,$40)
nx=512:ny=212:sx=0:sy=0:dx=0:dy=0:ar=0:lo=$0C:wm=$FFFF:fc=$1111*2:op=$20:gosub 11000    '-cls beta

msxvideo9990vpokelocp(0,$0A04)
msxvideo9990palette12bit(0,0)

for x=0 to 4000:msxvideo9990vpokeseq(%11100000):next
for x=0 to 2000:msxvideo9990vpokeseq(%00011100):next
for x=0 to 1000:msxvideo9990vpokeseq(%00000011):next

nx=8:ny=8:sx=0:sy=0:dx=8*8:dy=8*10:ar=0:lo=$0C:wm=$FFFF:fc=%0111110000000000:op=$20:gosub 11000

nx=8:ny=8:sx=0:sy=0:dx=8*10:dy=8*12:ar=0:lo=$0C:wm=$FFFF:fc=%0111110000000000:op=$10:gosub 11000
for i=0 to 127:out $62,i:next

do
seed=msxrnd(seed):dx=8*(seed mod 64)
seed=msxrnd(seed):dy=8*(seed mod 28)
seed=msxrnd(seed):fc=(seed band $FF)bor((seed band $FF)*256)
nx=8:ny=8:sx=0:sy=0:ar=0:lo=$0C:wm=$FFFF:op=$20:gosub 11000

seed=msxrnd(seed):dx=8*(seed mod 64)
seed=msxrnd(seed):dy=8*(seed mod 28)
nx=8:ny=8:sx=0:sy=0:ar=0:lo=$0C:wm=$FFFF:fc=0:op=$10:gosub 11000
seed=msxrnd(seed):ik=seed
seed=msxrnd(seed):pp=seed
seed=msxrnd(seed)
for x=0 to 7
ikpp8b(peek(@charmap01+((seed mod 74)*8)+x),ik,pp)
next

loop

do:loop

'-------------------------------------------------------------------------------

11000:
if in($65) band 1 then:goto 11000:end if      'wait for blitter
out $64,32           'write r32+
out $63,sx band 255  'r32
out $63,sx/256       'r33
out $63,sy band 255  'r34
out $63,sy/256       'r35
out $63,dx band 255  'r36
out $63,dx/256       'r37
out $63,dy band 255  'r38
out $63,dy/256       'r39
out $63,nx band 255  'r40
out $63,nx/256       'r41
out $63,ny band 255  'r42
out $63,ny/256       'r43
out $63,ar           'r44 arg
out $63,lo           'r45 lop
out $63,wm and 255   'r46 (?)
out $63,wm/256       'r47 (?)
out $63,fc and 255   'r48
out $63,fc/256       'r49
out $64,52           'write r52+
out $63,op           'r52
return

sub ikpp8b(tch1 as ubyte,tik1 as ubyte,tpp1 as ubyte)
  dim tlp1 as ubyte at $C000
  dim tmk1 as ubyte at $C001
  tmk1=128
  for tlp1=0 to 7
    if (tmk1 band tch1)<>0 then:
      out $62,tik1
    else
      out $62,tpp1
      end if
    tmk1=tmk1/2
    next
  end sub

regvalues01:
10000:
11500:
return

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------

