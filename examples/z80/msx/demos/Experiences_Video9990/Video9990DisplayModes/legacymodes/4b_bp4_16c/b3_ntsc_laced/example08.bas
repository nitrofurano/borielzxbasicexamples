#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxrnd.bas"
#include "library/b4r0f0e0_charmappobynews01.bas"

msxvideo9990initsuperimpose():msxvideo9990cleanregisters():msxvideo9990vdp(8,$C2)

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim wm as uinteger at $C032
dim fc as uinteger at $C034
dim seed as uinteger at $C036
dim ctpi as float at $C080

'msxvideo9990vdp(6,$81)
msxvideo9990vdp(6,$95):msxvideo9990vdp(7,$06)

for i=0 to 15:msxvideo9990palette12bit(i,i*$111):next
msxvideo9990palette12bit(0,0)

for i=0 to 15:msxvideo9990palette12bit(i,peek(uinteger,@palette01+i*2)):next


msxvideo9990palette12bit(0,$000)
msxvideo9990palette12bit(1,$125)
msxvideo9990palette12bit(2,$725)
msxvideo9990palette12bit(3,$879)
msxvideo9990palette12bit(4,$A53)
msxvideo9990palette12bit(5,$085)
msxvideo9990palette12bit(6,$F04)
msxvideo9990palette12bit(7,$554)
msxvideo9990palette12bit(8,$F7A)
msxvideo9990palette12bit(9,$FA0)
msxvideo9990palette12bit(10,$CCC)
msxvideo9990palette12bit(11,$0E3)
msxvideo9990palette12bit(12,$FCA)
msxvideo9990palette12bit(13,$2AF)
msxvideo9990palette12bit(14,$FE2)
msxvideo9990palette12bit(15,$FFE)



nx=512:ny=424:sx=0:sy=0:dx=0:dy=0:ar=0:lo=$0C:wm=$FFFF:fc=$1111*2:op=$20:gosub 11000    '-cls beta

msxvideo9990vpokelocp(0,$0A04)

for x=0 to 8000:msxvideo9990vpokeseq($55):next
for x=0 to 4000:msxvideo9990vpokeseq($AA):next
for x=0 to 2000:msxvideo9990vpokeseq($FF):next

for x=0 to 15
seed=msxrnd(seed):dx=8*(4+x)
seed=msxrnd(seed):dy=8*15
seed=msxrnd(seed):fc=$1111*(x)
nx=8:ny=8:ar=0:lo=$0C:wm=$FFFF:op=$20
gosub 11000
next

for x=0 to 300
asm
halt
end asm
next


'lmmc
dx=5*8:dy=7*8
nx=8:ny=8
fc=$1111*7
ar=0:lo=$0C:wm=$FFFF:op=$10
gosub 11000
for x=0to 63
  i=peek(@patterntest01+x)
  out $62,i
  next

do
seed=msxrnd(seed):dx=8*(seed mod 64)
seed=msxrnd(seed):dy=8*(seed mod 54)
seed=msxrnd(seed):fc=$1111*(seed mod 16)
nx=8:ny=8:ar=0:lo=$0C:wm=$FFFF:op=$20
gosub 11000

seed=msxrnd(seed):dx=8*(seed mod 64)
seed=msxrnd(seed):dy=8*(seed mod 54)
fc=$1111*7:nx=8:ny=8:ar=0:lo=$0C:wm=$FFFF:op=$10
gosub 11000
seed=msxrnd(seed)
for x=0to 31
  i=peek(@charmap01+x+((seed band $FF)*32) )
  out $62,i
  next

loop


'lmmv
dx=5*8:dy=7*8
nx=8:ny=8
ar=0
lo=$0C
wm=$FFFF
fc=$1111*7
op=$20
gosub 11000


do:loop


patterntest01:
asm
defb $55,$AA,$A5,$55
defb $5A,$A5,$AA,$55
defb $AA,$55,$5A,$A5
defb $AA,$55,$5A,$A5
defb $AA,$AA,$AA,$A5
defb $AA,$55,$5A,$A5
defb $AA,$55,$5A,$A5
defb $55,$55,$55,$55
end asm

palette01:
asm
defb $000,$125,$725,$879
defb $A53,$085,$F04,$554
defb $F7A,$FA0,$CCC,$0E3
defb $FCA,$2AF,$FE2,$FFE
end asm


11000:
if in($65) band 1 then:goto 11000:end if      'wait for blitter
out $64,32           'write r32+
out $63,sx band 255  'r32
out $63,sx/256       'r33
out $63,sy band 255  'r34
out $63,sy/256       'r35
out $63,dx band 255  'r36
out $63,dx/256       'r37
out $63,dy band 255  'r38
out $63,dy/256       'r39
out $63,nx band 255  'r40
out $63,nx/256       'r41
out $63,ny band 255  'r42
out $63,ny/256       'r43
out $63,ar           'r44 arg
out $63,lo           'r45 lop
out $63,wm and 255   'r46 (?)
out $63,wm/256       'r47 (?)
out $63,fc and 255   'r48
out $63,fc/256       'r49
out $64,52           'write r52+
out $63,op           'r52
return




