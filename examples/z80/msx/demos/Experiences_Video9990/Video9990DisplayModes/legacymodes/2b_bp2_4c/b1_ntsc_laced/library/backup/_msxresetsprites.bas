sub msxresetsprites():
  '- sprites 16*16
  asm
    ld a,($F3E0)
    or 00000010b
    and 11111110b
    ld b,a
    ld c,1
    call $0047 
    end asm
  '- clear all sprites definitions
  asm
    ld hl,14336
    ld bc,2048
    xor a
    call $0056 
    end asm
  '- sprite definitions
  'asm
  '  ld hl,spr_definition
  '  ld de,14336
  '  ld bc,128 ;(4 sprites * 32 datas)
  '  call $005c
  '  ld hl,spr_attributes
  '  ld de,6912
  '  ld bc,16 ;(4 sprites * 4 attributes)
  '  call $005c
  '  end asm
  end sub
