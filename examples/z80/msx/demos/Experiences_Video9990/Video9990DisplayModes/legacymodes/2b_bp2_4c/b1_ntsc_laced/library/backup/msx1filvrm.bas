sub msx1filvrm(tvl as uinteger, tlen as uinteger, tvram as uinteger):
  asm
    ld a,(ix+4)
    ld b,(ix+7)
    ld c,(ix+6)
    ld h,(ix+9)
    ld l,(ix+8)
    call $0056
    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM
'Address  : #0056
'Function : fill VRAM with value
'Input    : A  - data byte
'           BC - length of the area to be written
'           HL - start address
'Registers: AF, BC
