#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990setregisters.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxvideo9990vdokeseq.bas"

msxvideo9990initsuperimpose():msxvideo9990cleanregisters():msxvideo9990vdp(8,$C2)

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim wm as uinteger at $C032
dim fc as uinteger at $C034
dim seed as uinteger at $C036
dim ctpi as float at $C080


msxvideo9990vdp(6,%10101100)

for i=0 to 3:msxvideo9990palette12bit(i,i*$555):next
msxvideo9990palette12bit(0,$404)
msxvideo9990palette12bit(1,$D02)
msxvideo9990palette12bit(2,$794)
msxvideo9990palette12bit(3,$DE7)

nx=1024:ny=212:sx=0:sy=0:dx=0:dy=0:ar=0:lo=$0C:wm=$FFFF:fc=$5555:op=$20:gosub 11000    '-cls beta

msxvideo9990vpokelocp(0,$1504)
for x=0 to 4000:msxvideo9990vpokeseq($00):next
for x=0 to 2000:msxvideo9990vpokeseq($55):next
for x=0 to 1000:msxvideo9990vpokeseq($AA):next
for x=0 to  500:msxvideo9990vpokeseq($FF):next

nx=8:ny=8:sx=0:sy=0:dx=8*23:dy=8*3:ar=0:lo=$0C:wm=$FFFF:fc=$AAAA:op=$10:gosub 11000
for i=0 to 15:out $62,peek( (i bxor 1)+@patterntest01a):next

nx=8:ny=8:sx=0:sy=0:dx=8*23:dy=8*5:ar=0:lo=$0C:wm=$FFFF:fc=$AAAA:op=$10:gosub 11000
for i=0 to 15:out $62,peek( (i bxor 1)+@patterntest01a):next

do:loop

patterntest01a:
asm
defw %0000111111000000
defw %0011110011110000
defw %1111000000111100
defw %1111000000111100
defw %1111111111111100
defw %1111000000111100
defw %1111000000111100
defw %0000000000000000
end asm



11000:
if in($65) band 1 then:goto 11000:end if      'wait for blitter
out $64,32           'write r32+
out $63,sx band 255  'r32
out $63,sx/256       'r33
out $63,sy band 255  'r34
out $63,sy/256       'r35
out $63,dx band 255  'r36
out $63,dx/256       'r37
out $63,dy band 255  'r38
out $63,dy/256       'r39
out $63,nx band 255  'r40
out $63,nx/256       'r41
out $63,ny band 255  'r42
out $63,ny/256       'r43
out $63,ar           'r44 arg
out $63,lo           'r45 lop
out $63,wm band 255  'r46
out $63,wm/256       'r47
out $63,fc band 255  'r48
out $63,fc/256       'r49
out $64,52           'write r52+
out $63,op           'r52
return


