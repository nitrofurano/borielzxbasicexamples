#include "library/msxvideo9990initsuperimpose.bas"
#include "library/msxvideo9990setregisters.bas"
#include "library/msxvideo9990cleanregisters.bas"
#include "library/msxvideo9990vdp.bas"
#include "library/msxvideo9990palette12bit.bas"
#include "library/msxvideo9990vpokelocp.bas"
#include "library/msxvideo9990vpokeseq.bas"
#include "library/msxvideo9990vdokeseq.bas"

dim i as uinteger at $C010
dim a as uinteger at $C012
dim b as uinteger at $C014
dim p as uinteger at $C016
dim sx as uinteger at $C018
dim sy as uinteger at $C01A
dim x as uinteger at $C01C
dim nx as uinteger at $C01E
dim ny as uinteger at $C020
dim xx as uinteger at $C022
dim xy as uinteger at $C024
dim dx as uinteger at $C026
dim dy as uinteger at $C028
dim ar as uinteger at $C02A
dim lo as uinteger at $C02C
dim op as uinteger at $C02E
dim c as uinteger at $C030
dim ctpi as float at $C080

msxvideo9990initsuperimpose()

'msxvideo9990setregisters(@regvalues01)
msxvideo9990cleanregisters()
msxvideo9990vdp( 6,$96)
msxvideo9990vdp( 7,$00)
msxvideo9990vdp( 8,$C2)
msxvideo9990vdp(13,$00)
out $67,1




for i=0 to 15
  msxvideo9990palette12bit(i,i*$111)
  next

msxvideo9990palette12bit(0,$CDE)

nx=384:ny=240:sx=0:sy=0:dx=0:dy=0:ar=0:lo=0:op=$40:gosub 11000    '-cls beta


msxvideo9990vpokelocp(0,$0A04)

for x=0 to 8000
  msxvideo9990vpokeseq(%11100000)
  next

for x=0 to 4000
  msxvideo9990vpokeseq(%00011100)
  next

for x=0 to 2000
  msxvideo9990vpokeseq(%00000011)
  next

do:loop

'-------------------------------------------------------------------------------

'16bit bitmap
for y=10 to 11
  p=0:a=y*512*2:gosub 10000
  for x=10000 to 61000 '31
    'c=(y band $1f)*32*32+$1F*32+(x band $1f)
    'out $60,(c-int(c/256)*256) band 255
    'out $60,(c/256) band 255
    'out $60,%00011111  '...r,b
    'out $60,%01111100  'g,r...

    msxvideo9990vdokeseq(%0111110000000000)
    next:next

ctpi=3.1415926
for i=0 to 16
  nx = 32 : ny = 32 : sx = 0 : sy = 0
  dx=i 'dx = sin (i/16/2*ctpi)*100
  dy=1 'dy = cos(i/16/2*ctpi)*100
  gosub 11500
  next

do:loop





p=0:a=0:gosub 10000	                       'charset in screen 5 format
for y=0 to 15	                            	'init two row of chars
  for x=0 to 31
    for i=0 to 3
      out $60,(x band 15)*17
      next:next:next

p=4:a=0:gosub 10000                         	'second charset in screen 5 format
for y=0 to 7	                               	'init one row of chars
  for x=0 to 31
    for i=0 to 3
      out $60,((x+y) band 15)*17
      next:next:next

'16bit nametable little endian
for y=0 to 25
  p=7
  a=$C000+64*2*y
  gosub 10000               '64 chars wide, 2 byte per char
  for x=0 to 31
    out $60,x band 15
    out $60,0
    next:next

'lower layer 16bit nametable little endian
for y=0 to 25
  p=7
  a=$E000+64*2*y
  gosub 10000
  for x=0 to 31
    out $60,0 band 15
    out $60,0
    next:next

'sprite attribute table, 4 bytes per sprite: Y, pattern, X, todo(x high?)
p=3
a=$FE00
gosub 10000
for i=0 to 15
  sy=i+100                           'sy=int(sin(i*2*3.1415926/16)*5+100)  : ? sy
  out $60,sy
  out $60,2
  out $60,i*16+3
  out $60,0
  next

'p=$7C:a=0:gosub 10000:out $60,1

do:loop

regvalues01:
asm
defb 0,0,0,0
defb 0,0,$87,0
defb $C2,0,0,0
defb 0,0,0,0
defb 0,0,0,0
defb 0,0,0,0
defb 0
end asm



10000: 'vram write address = p*65536 + a . out &h60 vpokes with autoincrement.
out $64,0	                        	'vram write mode: 0
                                   'defusr1 = a		'to get at H,L
xx=peek($f39c)
out $63,xx          ': '?xx;
xx=peek($f39d)
out $63,xx          ': '?xx;"."
'xx=a
out $63,p                          'bit 7 address increment inhibit
return




11000:
if in($65) band 1 then:goto 11000:end if      'wait for blitter
out $64,32           'write r32+
out $63,sx band 255  'r32
out $63,sx/256       'r33
out $63,sy band 255  'r34
out $63,sy/256       'r35
out $63,dx band 255  'r36
out $63,dx/256       'r37
out $63,dy band 255  'r38
out $63,dy/256       'r39
out $63,nx band 255  'r40
out $63,nx/256       'r41
out $63,ny band 255  'r42
out $63,ny/256       'r43
out $63,ar           'r44 arg
out $63,lo           'r45 lop
out $63,255          'r46
out $63,255          'r47
out $64,52           'write r52+
out $63,op           'r52
return

11500:               'blit (sx,sy)-(sx+nx,sy+ny) to (dx,dy)
op=$40               'lmmm
ar=0:lo=$0C
gosub 11000
return






