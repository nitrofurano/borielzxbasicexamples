	org 16400
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
	call _msxvideo9990initsuperimpose
	call _msxvideo9990cleanregisters
	ld a, 130
	push af
	ld a, 6
	push af
	call _msxvideo9990vdp
	xor a
	push af
	ld a, 7
	push af
	call _msxvideo9990vdp
	ld a, 194
	push af
	ld a, 8
	push af
	call _msxvideo9990vdp
	xor a
	push af
	ld a, 13
	push af
	call _msxvideo9990vdp
	ld hl, 0
	ld (_i), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_i)
	ld de, 273
	call __MUL16_FAST
	push hl
	ld hl, (_i)
	ld a, l
	push af
	call _msxvideo9990palette12bit
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL0:
	ld hl, 15
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL3
	ld hl, 3294
	push hl
	xor a
	push af
	call _msxvideo9990palette12bit
	ld hl, 256
	ld (_nx), hl
	ld hl, 212
	ld (_ny), hl
	ld hl, 0
	ld (_sx), hl
	ld hl, 0
	ld (_sy), hl
	ld hl, 0
	ld (_dx), hl
	ld hl, 0
	ld (_dy), hl
	ld hl, 0
	ld (_ar), hl
	ld hl, 0
	ld (_lo), hl
	ld hl, 64
	ld (_op), hl
	call __LABEL__11000
	ld hl, 2564
	push hl
	xor a
	push af
	call _msxvideo9990vpokelocp
	ld hl, 0
	ld (_x), hl
	jp __LABEL5
__LABEL8:
	ld a, 3
	push af
	call _msxvideo9990vpokeseq
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL5:
	ld hl, 4000
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL8
	ld hl, 0
	ld (_x), hl
	jp __LABEL10
__LABEL13:
	ld a, 12
	push af
	call _msxvideo9990vpokeseq
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL10:
	ld hl, 2000
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL13
	ld hl, 0
	ld (_x), hl
	jp __LABEL15
__LABEL18:
	ld a, 48
	push af
	call _msxvideo9990vpokeseq
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL15:
	ld hl, 1000
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL20:
	jp __LABEL20
	ld a, 10
	ld (_y), a
	jp __LABEL22
__LABEL25:
	ld hl, 0
	ld (_p), hl
	call __LABEL__10000
	ld hl, 10000
	ld (_x), hl
	jp __LABEL27
__LABEL30:
	ld hl, 31744
	push hl
	call _msxvideo9990vdokeseq
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL27:
	ld hl, 61000
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL30
	ld a, (_y)
	inc a
	ld (_y), a
__LABEL22:
	ld a, 11
	ld hl, (_y - 1)
	cp h
	jp nc, __LABEL25
	ld hl, 0
	ld (_i), hl
	jp __LABEL32
__LABEL35:
	ld hl, 32
	ld (_nx), hl
	ld hl, 32
	ld (_ny), hl
	ld hl, 0
	ld (_sx), hl
	ld hl, 0
	ld (_sy), hl
	ld hl, (_i)
	ld (_dx), hl
	ld hl, 1
	ld (_dy), hl
	call __LABEL__11500
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL32:
	ld hl, 16
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL35
__LABEL37:
	jp __LABEL37
	ld hl, 0
	ld (_p), hl
	call __LABEL__10000
	xor a
	ld (_y), a
	jp __LABEL39
__LABEL42:
	ld hl, 0
	ld (_x), hl
	jp __LABEL44
__LABEL47:
	ld hl, 0
	ld (_i), hl
	jp __LABEL49
__LABEL52:
	ld de, 15
	ld hl, (_x)
	call __BAND16
	ld de, 17
	call __MUL16_FAST
	ld a, l
	ld bc, 96
	out (c), a
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL49:
	ld hl, 3
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL52
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL44:
	ld hl, 31
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL47
	ld a, (_y)
	inc a
	ld (_y), a
__LABEL39:
	ld a, 15
	ld hl, (_y - 1)
	cp h
	jp nc, __LABEL42
	ld hl, 4
	ld (_p), hl
	call __LABEL__10000
	xor a
	ld (_y), a
	jp __LABEL54
__LABEL57:
	ld hl, 0
	ld (_x), hl
	jp __LABEL59
__LABEL62:
	ld hl, 0
	ld (_i), hl
	jp __LABEL64
__LABEL67:
	ld a, (_y)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_x)
	add hl, de
	ld de, 15
	call __BAND16
	ld de, 17
	call __MUL16_FAST
	ld a, l
	ld bc, 96
	out (c), a
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL64:
	ld hl, 3
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL67
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL59:
	ld hl, 31
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL62
	ld a, (_y)
	inc a
	ld (_y), a
__LABEL54:
	ld a, 7
	ld hl, (_y - 1)
	cp h
	jp nc, __LABEL57
	xor a
	ld (_y), a
	jp __LABEL69
__LABEL72:
	ld hl, 7
	ld (_p), hl
	call __LABEL__10000
	ld hl, 0
	ld (_x), hl
	jp __LABEL74
__LABEL77:
	ld de, 15
	ld hl, (_x)
	call __BAND16
	ld a, l
	ld bc, 96
	out (c), a
	ld a, 0
	ld bc, 96
	out (c), a
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL74:
	ld hl, 31
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL77
	ld a, (_y)
	inc a
	ld (_y), a
__LABEL69:
	ld a, 25
	ld hl, (_y - 1)
	cp h
	jp nc, __LABEL72
	xor a
	ld (_y), a
	jp __LABEL79
__LABEL82:
	ld hl, 7
	ld (_p), hl
	call __LABEL__10000
	ld hl, 0
	ld (_x), hl
	jp __LABEL84
__LABEL87:
	ld a, 0
	ld bc, 96
	out (c), a
	ld a, 0
	ld bc, 96
	out (c), a
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL84:
	ld hl, 31
	ld de, (_x)
	or a
	sbc hl, de
	jp nc, __LABEL87
	ld a, (_y)
	inc a
	ld (_y), a
__LABEL79:
	ld a, 25
	ld hl, (_y - 1)
	cp h
	jp nc, __LABEL82
	ld hl, 3
	ld (_p), hl
	call __LABEL__10000
	ld hl, 0
	ld (_i), hl
	jp __LABEL89
__LABEL92:
	ld hl, (_i)
	ld de, 100
	add hl, de
	ld (_sy), hl
	ld a, l
	ld bc, 96
	out (c), a
	ld a, 2
	ld bc, 96
	out (c), a
	ld hl, (_i)
	ld de, 16
	call __MUL16_FAST
	inc hl
	inc hl
	inc hl
	ld a, l
	ld bc, 96
	out (c), a
	ld a, 0
	ld bc, 96
	out (c), a
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL89:
	ld hl, 15
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL92
__LABEL94:
	jp __LABEL94
__LABEL__regvalues01:
#line 142
		defb 0,0,0,0
		defb 0,0,$87,0
		defb $C2,0,0,0
		defb 0,0,0,0
		defb 0,0,0,0
		defb 0,0,0,0
		defb 0
#line 149
__LABEL__10000:
	ld a, 0
	ld bc, 100
	out (c), a
	ld a, (62364)
	ld l, a
	ld h, 0
	ld (_xx), hl
	ld a, l
	ld bc, 99
	out (c), a
	ld a, (62365)
	ld l, a
	ld h, 0
	ld (_xx), hl
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_p)
	ld a, l
	ld bc, 99
	out (c), a
	ret
__LABEL__11000:
	ld bc, 101
	in a, (c)
	ld h, 1
	and h
	or a
	jp nz, __LABEL__11000
	ld a, 32
	ld bc, 100
	out (c), a
	ld de, 255
	ld hl, (_sx)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_sx)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld de, 255
	ld hl, (_sy)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_sy)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld de, 255
	ld hl, (_dx)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_dx)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld de, 255
	ld hl, (_dy)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_dy)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld de, 255
	ld hl, (_nx)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_nx)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld de, 255
	ld hl, (_ny)
	call __BAND16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_ny)
	ld de, 256
	call __DIVU16
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_ar)
	ld a, l
	ld bc, 99
	out (c), a
	ld hl, (_lo)
	ld a, l
	ld bc, 99
	out (c), a
	ld a, 255
	ld bc, 99
	out (c), a
	ld a, 255
	ld bc, 99
	out (c), a
	ld a, 52
	ld bc, 100
	out (c), a
	ld hl, (_op)
	ld a, l
	ld bc, 99
	out (c), a
	ret
__LABEL__11500:
	ld hl, 64
	ld (_op), hl
	ld hl, 0
	ld (_ar), hl
	ld hl, 12
	ld (_lo), hl
	call __LABEL__11000
	ret
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	pop iy
	pop ix
	exx
	ei
	ret
__CALL_BACK__:
	DEFW 0
_msxvideo9990initsuperimpose:
	push ix
	ld ix, 0
	add ix, sp
	ld a, 0
	ld bc, 111
	out (c), a
_msxvideo9990initsuperimpose__leave:
	ld sp, ix
	pop ix
	ret
_msxvideo9990cleanregisters:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_msxvideo9990cleanregisters_tzrg0l), hl
	jp __LABEL98
__LABEL101:
	ld hl, (_msxvideo9990cleanregisters_tzrg0l)
	ld a, l
	ld bc, 100
	out (c), a
	ld a, 0
	ld bc, 99
	out (c), a
	ld hl, (_msxvideo9990cleanregisters_tzrg0l)
	inc hl
	ld (_msxvideo9990cleanregisters_tzrg0l), hl
__LABEL98:
	ld hl, 23
	ld de, (_msxvideo9990cleanregisters_tzrg0l)
	or a
	sbc hl, de
	jp nc, __LABEL101
_msxvideo9990cleanregisters__leave:
	ld sp, ix
	pop ix
	ret
_msxvideo9990vdp:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	ld bc, 100
	out (c), a
	ld a, (ix+7)
	ld bc, 99
	out (c), a
_msxvideo9990vdp__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_msxvideo9990palette12bit:
	push ix
	ld ix, 0
	add ix, sp
	ld a, 142
	ld bc, 100
	out (c), a
	ld a, (ix+5)
	add a, a
	add a, a
	ld bc, 99
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 256
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 15
	call __BAND32
	push de
	push hl
	ld de, 0
	ld hl, 2
	call __MUL32
	ld a, l
	ld bc, 97
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 16
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 15
	call __BAND32
	push de
	push hl
	ld de, 0
	ld hl, 2
	call __MUL32
	ld a, l
	ld bc, 97
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 15
	call __BAND16
	add hl, hl
	ld a, l
	ld bc, 97
	out (c), a
_msxvideo9990palette12bit__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_msxvideo9990vpokelocp:
	push ix
	ld ix, 0
	add ix, sp
	ld a, 0
	ld bc, 100
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld a, l
	ld bc, 99
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 256
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld a, l
	ld bc, 99
	out (c), a
	ld a, (ix+5)
	ld bc, 99
	out (c), a
_msxvideo9990vpokelocp__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_msxvideo9990vpokeseq:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	ld bc, 96
	out (c), a
_msxvideo9990vpokeseq__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_msxvideo9990vdokeseq:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 255
	call __BAND16
	ld a, l
	ld bc, 96
	out (c), a
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 256
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld a, l
	ld bc, 96
	out (c), a
_msxvideo9990vdokeseq__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
#line 1 "and8.asm"
	
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 772 "example08.bas"
#line 1 "band16.asm"
	
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 773 "example08.bas"
#line 1 "band32.asm"
	
	; FASTCALL bitwise and 32 version.
	; Performs 32bit and 32bit and returns the bitwise
	; result in DE,HL
	; First operand in DE,HL 2nd operand into the stack
	
__BAND32:
	    ld b, h
	    ld c, l ; BC <- HL
	
	    pop hl  ; Return address
	    ex (sp), hl ; HL <- Lower part of 2nd Operand
	
		ld a, b
	    and h
	    ld b, a
	
	    ld a, c
	    and l
	    ld c, a ; BC <- BC & HL
	
		pop hl  ; Return dddress
	    ex (sp), hl ; HL <- High part of 2nd Operand
	
	    ld a, d
	    and h
	    ld d, a
	
	    ld a, e
	    and l
	    ld e, a ; DE <- DE & HL
	
	    ld h, b
	    ld l, c ; HL <- BC  ; Always return DE,HL pair regs
	
	    ret
	
#line 774 "example08.bas"
#line 1 "div16.asm"
	
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 775 "example08.bas"
#line 1 "mul16.asm"
	
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 776 "example08.bas"
#line 1 "mul32.asm"
	
#line 1 "_mul32.asm"
	
	
; Ripped from: http://www.andreadrian.de/oldcpu/z80_number_cruncher.html#moztocid784223
	; Used with permission.
	; Multiplies 32x32 bit integer (DEHL x D'E'H'L')
	; 64bit result is returned in H'L'H L B'C'A C
	
	
__MUL32_64START:
			push hl
			exx
			ld b, h
			ld c, l		; BC = Low Part (A)
			pop hl		; HL = Load Part (B)
			ex de, hl	; DE = Low Part (B), HL = HightPart(A) (must be in B'C')
			push hl
	
			exx
			pop bc		; B'C' = HightPart(A)
			exx			; A = B'C'BC , B = D'E'DE
	
				; multiply routine 32 * 32bit = 64bit
				; h'l'hlb'c'ac = b'c'bc * d'e'de
				; needs register a, changes flags
				;
				; this routine was with tiny differences in the
				; sinclair zx81 rom for the mantissa multiply
	
__LMUL:
	        and     a               ; reset carry flag
	        sbc     hl,hl           ; result bits 32..47 = 0
	        exx
	        sbc     hl,hl           ; result bits 48..63 = 0
	        exx
	        ld      a,b             ; mpr is b'c'ac
	        ld      b,33            ; initialize loop counter
	        jp      __LMULSTART  
	
__LMULLOOP:
	        jr      nc,__LMULNOADD  ; JP is 2 cycles faster than JR. Since it's inside a LOOP
	                                ; it can save up to 33 * 2 = 66 cycles
	                                ; But JR if 3 cycles faster if JUMP not taken!
	        add     hl,de           ; result += mpd
	        exx
	        adc     hl,de
	        exx
	
__LMULNOADD:
	        exx
	        rr      h               ; right shift upper
	        rr      l               ; 32bit of result
	        exx
	        rr      h
	        rr      l
	
__LMULSTART:
	        exx
	        rr      b               ; right shift mpr/
	        rr      c               ; lower 32bit of result
	        exx
	        rra                     ; equivalent to rr a
	        rr      c
	        djnz    __LMULLOOP
	
			ret						; result in h'l'hlb'c'ac
	       
#line 2 "mul32.asm"
	
__MUL32:	; multiplies 32 bit un/signed integer.
				; First operand stored in DEHL, and 2nd onto stack
				; Lowest part of 2nd operand on top of the stack
				; returns the result in DE.HL
			exx
			pop hl	; Return ADDRESS
			pop de	; Low part
			ex (sp), hl ; CALLEE -> HL = High part
			ex de, hl
			call __MUL32_64START
	
__TO32BIT:  ; Converts H'L'HLB'C'AC to DEHL (Discards H'L'HL)
			exx
			push bc
			exx
			pop de
			ld h, a
			ld l, c
			ret
	
	
#line 777 "example08.bas"
	
ZXBASIC_USER_DATA:
	_msxvideo9990setregisters_tzrg0l EQU 49152
	_msxvideo9990cleanregisters_tzrg0l EQU 49152
	_i EQU 49168
	_p EQU 49174
	_sx EQU 49176
	_sy EQU 49178
	_x EQU 49180
	_nx EQU 49182
	_ny EQU 49184
	_xx EQU 49186
	_dx EQU 49190
	_dy EQU 49192
	_ar EQU 49194
	_lo EQU 49196
	_op EQU 49198
_y:
	DEFB 00
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
