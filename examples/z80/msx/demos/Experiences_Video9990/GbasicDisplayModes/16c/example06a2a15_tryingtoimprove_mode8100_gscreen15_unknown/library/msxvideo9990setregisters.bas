sub msxvideo9990setregisters(tzrg0 as uinteger)
  dim tzrg0l as uinteger at $C000
  for tzrg0l=0 to 23
    out $64,tzrg0l	            	'- register write mode
    out $63,peek(tzrg0+tzrg0l)  '- register value
    next
  end sub
