rm nothingthing.asm nothingthing.rom
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=nothingthing.tmr
rm msxromheader.bin

echo;echo "1..."
zxb.py nothingthing.bas --asm --org=$((0x4010))

echo;echo "2..."
zxb.py nothingthing.bas --org=$((0x4010))

echo;echo "3..."
cat nothingthing.bin >> nothingthing.tmr
filesize=$(stat -c%s "nothingthing.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> nothingthing.tmr
rm _dummybytes.bin nothingthing.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=nothingthing.tmr of=nothingthing.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=nothingthing.tmr of=nothingthing.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=nothingthing.tmr of=nothingthing.rom ; fi
rm nothingthing.tmr

mame fsa1fx -video soft -ui_active -bp ~/.mame/roms -resolution0 1280x720 -aspect 1000:750 -skip_gameinfo -nowindow -cart1 nothingthing.rom

#openmsx nothingthing.rom

# openmsx -machine msx1 -carta nothingthing.rom
# openmsx -machine msx2 nothingthing.rom
# openmsx -machine msx2plus nothingthing.rom
# mess msx -cart1 nothingthing.rom
# mess msx2 -cart1 nothingthing.rom
# mess msx2p -cart1 nothingthing.rom
# openmsx -ext gfx9000
# openmsx -ext video9000

