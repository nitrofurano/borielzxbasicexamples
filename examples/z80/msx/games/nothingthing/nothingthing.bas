#include "library/msx32kslotcompatibilitytemplate.bas"

#include "library/msxfilvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxscreen.bas"
#include "library/msx1vpoke.bas"
#include "library/msx2vpoke.bas"
#include "library/msx2vpeek.bas"
#include "library/msxpalette.bas"
#include "library/msx1putsprite.bas"
#include "library/msxspritest.bas"
#include "library/_msxresetsprites.bas"
#include "library/msxjoystick.bas"

#include "library/msxwaitvbl.bas"
#include "library/msxsnsmat.bas"
#include "library/msx1sound.bas"
#include "library/eu64x53_defb.bas"
#include "library/pic01_defb.bas"
#include "library/pic02_defb.bas"
#include "library/pic03_defb.bas"
#include "library/pic04_defb.bas"

'#include "library/bacaball_charmap_antigo.bas"
#include "library/jupiterace_charmap.bas"

#include "library/msx2pputchar.bas"
'#include "library/bacaball_charmap_antigo.bas"

dim eee as uinteger at $E000:dim qq2 as uinteger at $E002:dim eez as uinteger at $E00A
dim xpos as uinteger at $E004:dim ypos as uinteger at $E006:dim cpos as uinteger at $E008
dim v as uinteger at $E010:dim t as uinteger at $E012
dim rcl as ubyte at $E020:dim gcl as ubyte at $E021:dim bcl as ubyte at $E022
dim ycl as integer at $E024:dim jcl as integer at $E026:dim kcl as integer at $E028
dim ttp as ubyte at $E02A:dim ttr as ubyte at $E02B:dim ttg as ubyte at $E02C:dim ttb as ubyte at $E02D
dim snsm as ubyte at $E02E
dim jst0 as ubyte at $E04A:dim jst1 as ubyte at $E04C:dim jst2 as ubyte at $E04E

'-------------------------------------------------------------------------------

'sub zprintdecimal(tpos as uinteger,tval as uinteger):
'  msx1vpoke(6144+tpos+0,48+int(tval/10000) mod 10)
'  msx1vpoke(6144+tpos+1,48+int(tval/1000) mod 10)
'  msx1vpoke(6144+tpos+2,48+int(tval/100) mod 10)
'  msx1vpoke(6144+tpos+3,48+int(tval/10) mod 10)
'  msx1vpoke(6144+tpos+4,48+tval mod 10)
'  end sub

sub scr12block4x1(vx as uinteger,vy as uinteger,vrcl as ubyte,vgcl as ubyte,vbcl as ubyte):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F8) bor ((vkcl band %00011100)/ 4):px1=(vycl band $F8) bor ((vkcl band %11100000)/32)
  px2=(vycl band $F8) bor ((vjcl band %00011100)/ 4):px3=(vycl band $F8) bor ((vjcl band %11100000)/32)
  msx1ldirvm(4,vy*256+vx*4,@px0)
  end sub

sub scr12block4x1ul(vx as uinteger,vy as uinteger,vrgbcl as ulong):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vrcl as ubyte at $E106:dim vgcl as ubyte at $E107:dim vbcl as ubyte at $E108
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vrcl=(vrgbcl band $FF0000)/$10000:vgcl=(vrgbcl band $FF00)/$100:vbcl=vrgbcl band $FF
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F8) bor ((vkcl band %00011100)/ 4):px1=(vycl band $F8) bor ((vkcl band %11100000)/32)
  px2=(vycl band $F8) bor ((vjcl band %00011100)/ 4):px3=(vycl band $F8) bor ((vjcl band %11100000)/32)
  msx1ldirvm(4,vy*256+vx*4,@px0)
  end sub

sub scr12block4x1vh(vx as uinteger,vy as uinteger,vrcl as ubyte,vgcl as ubyte,vbcl as ubyte, vw as uinteger, vh as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vxw as uinteger at $E10A:dim vyh as uinteger at $E10C
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F8) bor ((vkcl band %00011100)/ 4):px1=(vycl band $F8) bor ((vkcl band %11100000)/32)
  px2=(vycl band $F8) bor ((vjcl band %00011100)/ 4):px3=(vycl band $F8) bor ((vjcl band %11100000)/32)
  for vxw=vx to vx+vw-1:for vyh=vy to vy+vh-1:msx1ldirvm(4,vyh*256+vxw*4,@px0):next:next
  end sub

sub scr12block4x1ulvh(vx as uinteger,vy as uinteger,vrgbcl as ulong, vw as uinteger, vh as uinteger):
  dim vycl as integer at $E100:dim vjcl as integer at $E102:dim vkcl as integer at $E104
  dim vrcl as ubyte at $E106:dim vgcl as ubyte at $E107:dim vbcl as ubyte at $E108
  dim vxw as uinteger at $E10A:dim vyh as uinteger at $E10C
  dim px0 as ubyte at $E110:dim px1 as ubyte at $E111:dim px2 as ubyte at $E112:dim px3 as ubyte at $E113
  vrcl=(vrgbcl band $FF0000)/$10000:vgcl=(vrgbcl band $FF00)/$100:vbcl=vrgbcl band $FF
  vycl=int(vbcl/2)+int(vrcl/4)+int(vgcl/8):vjcl=(256+int((vrcl-vycl)/2))band 255:vkcl=(256+int((vgcl-vycl)/2))band 255
  px0=(vycl band $F8) bor ((vkcl band %00011100)/ 4):px1=(vycl band $F8) bor ((vkcl band %11100000)/32)
  px2=(vycl band $F8) bor ((vjcl band %00011100)/ 4):px3=(vycl band $F8) bor ((vjcl band %11100000)/32)
  for vxw=vx to vx+vw-1:for vyh=vy to vy+vh-1:msx1ldirvm(4,vyh*256+vxw*4,@px0):next:next
  end sub

sub msx2pwritetext8x8(txpt as ubyte,typt as ubyte,tcpt as ubyte,tlocram as uinteger,tleng as uinteger,tyoffpt as ubyte,tmde as ubyte,tchmapl as uinteger):
  dim tlv as uinteger at $E8F0:dim tcht as ubyte at $E8F2
  for tlv=0 to tleng-1
    tcht=peek(tlocram+tlv)
    msx2pputcharlum8x8(txpt+tlv,typt,tcht,tcpt,tyoffpt,tchmapl,tmde)
    next
  end sub

'-------------------------------------------------------------------------------

msxcolor(64,64,%11100000)
'msxscreen(8,0,0,0)
setscreen12() '- i have to fix this asap

'msxresetsprites() '- i have to see better what this does

msxpalettehx(14,$0C84) '- RGB format (hexcolour 12bit, $0C84 means #CC8844)
msxpalettehx(1,$0402)
msxpalettehx(4,$0228)

loop03:

ttp=0:ttb=0:ttr=0:ttg=0
for eee=0 to 3391
  ttp=peek(@pic01+eee)
  if (ttp band 3)=1 then:ttb=ttp:end if
  if (ttp band 3)=2 then:ttr=ttp:end if
  if (ttp band 3)=3 then:ttg=ttp:end if
  scr12block4x1vh(eee band 63,4*(int(eee/64)),ttr,ttg,ttb,1,4)
  next
msx2pwritetext8x8(1,20,3,@text01r01,@text01r02-@text01r01,4,1,@typeface)
msx2pwritetext8x8(1,22,3,@text01r02,@text01r03-@text01r02,4,1,@typeface)
msx2pwritetext8x8(1,23,3,@text01r03,@text01r04-@text01r03,4,1,@typeface)
'for eee=0 to 500:msxwaitvbl():next
'print at 19,1;ink 0;paper 1;" NOTHING THING "
'print at 21,1;ink 0;paper 1;" PAULO SILVA, MARCH 2013 "
'print at 22,1;ink 0;paper 1;" PUSH ANY KEY "
fr1lp:
jst0=msxjoystick(0):jst1=msxjoystick(1):jst2=jst0 band jst1
snsm=0:for eee=0 to 8:snsm=snsm bor 255-(msxsnsmat(eee)):next
if snsm=0 and (jst2 band 63)=63 then: goto fr1lp:end if
'if snsm=0 then: goto fr1lp:end if



ttp=0:ttb=0:ttr=0:ttg=0
for eee=0 to 3391
  ttp=peek(@pic02+eee)
  if (ttp band 3)=1 then:ttb=ttp:end if
  if (ttp band 3)=2 then:ttr=ttp:end if
  if (ttp band 3)=3 then:ttg=ttp:end if
  scr12block4x1vh(eee band 63,4*(int(eee/64)),ttr,ttg,ttb,1,4)
  next
msx2pwritetext8x8(2,3,0,@text02r01,@text02r02-@text02r01,4,1,@typeface)
msx2pwritetext8x8(0,4,0,@text02r02,@text02r03-@text02r02,4,1,@typeface)
msx2pwritetext8x8(0,5,0,@text02r03,@text02r04-@text02r03,4,1,@typeface)
msx2pwritetext8x8(0,6,0,@text02r04,@text02r05-@text02r04,4,1,@typeface)
msx2pwritetext8x8(0,7,0,@text02r05,@text02r06-@text02r05,4,1,@typeface)
msx2pwritetext8x8(0,8,0,@text02r06,@text02r07-@text02r06,4,1,@typeface)
msx2pwritetext8x8(0,9,0,@text02r07,@text02r08-@text02r07,4,1,@typeface)
msx2pwritetext8x8(0,10,0,@text02r08,@text02r09-@text02r08,4,1,@typeface)
msx2pwritetext8x8(1,23,7,@text01r03,@text01r04-@text01r03,4,1,@typeface)
'for eee=0 to 500:msxwaitvbl():next
'print at 2,3;ink 1;paper 5;" THIS GAME IS ABOUT NOTHING T"
'"HINGS. NOTHING THING IS A TERM V"
'"ERY USED FROM JACQUE FRESCO, FRO"
'"M HIS AUDIO MATERIALS AND ONLINE"
'" SEMINARS, ABOUT THE VENUS PROJE"
'"CT. DEPENDING ON THE PURPOSE, A "
'"GAME LIKE THIS CAN BE A NOTHING "
'"THING. "
fr2lp:
jst0=msxjoystick(0):jst1=msxjoystick(1):jst2=jst0 band jst1
snsm=0:for eee=0 to 8:snsm=snsm bor 255-(msxsnsmat(eee)):next
if snsm=0 and (jst2 band 63)=63 then:goto fr2lp:end if

ttp=0:ttb=0:ttr=0:ttg=0
for eee=0 to 3391
  ttp=peek(@pic03+eee)
  if (ttp band 3)=1 then:ttb=ttp:end if
  if (ttp band 3)=2 then:ttr=ttp:end if
  if (ttp band 3)=3 then:ttg=ttp:end if
  scr12block4x1vh(eee band 63,4*(int(eee/64)),ttr,ttg,ttb,1,4)
  next
msx2pwritetext8x8(2,3,14,@text03r01,@text03r02-@text03r01,4,1,@typeface)
msx2pwritetext8x8(0,4,14,@text03r02,@text03r03-@text03r02,4,1,@typeface)
msx2pwritetext8x8(0,5,14,@text03r03,@text03r04-@text03r03,4,1,@typeface)
msx2pwritetext8x8(0,6,14,@text03r04,@text03r05-@text03r04,4,1,@typeface)
msx2pwritetext8x8(0,7,14,@text03r05,@text03r06-@text03r05,4,1,@typeface)
msx2pwritetext8x8(0,8,14,@text03r06,@text03r07-@text03r06,4,1,@typeface)
msx2pwritetext8x8(0,9,14,@text03r07,@text03r08-@text03r07,4,1,@typeface)
msx2pwritetext8x8(0,10,14,@text03r08,@text03r09-@text03r08,4,1,@typeface)
msx2pwritetext8x8(1,23,3,@text01r03,@text01r04-@text01r03,4,1,@typeface)
'for eee=0 to 200:msxwaitvbl():next
''print at 2,3;ink 1;paper 5;
'" AND MAYBE THIS GAME IS NOT A"
'" GAME AT ALL. THE MAIN PURPOSE I"
'"S TO CALL PEOPLE FOR THE IMPORTA"
'"NCE OF DEVELOPING GAMES ABOUT RE"
'"SOURCE BASED ECONOMY, WHICH IS V"
'"ERY PROBABLY THE ONLY SOLUTION W"
'"E MAY HAVE FOR A SUSTAINABLE FUT"
'"URE OF THE WHOLE MANKIND. "
fr3lp:
jst0=msxjoystick(0):jst1=msxjoystick(1):jst2=jst0 band jst1
snsm=0:for eee=0 to 8:snsm=snsm bor 255-(msxsnsmat(eee)):next
if snsm=0 and (jst2 band 63)=63 then:goto fr3lp:end if

ttp=0:ttb=0:ttr=0:ttg=0
for eee=0 to 3391
  ttp=peek(@pic04+eee)
  if (ttp band 3)=1 then:ttb=ttp:end if
  if (ttp band 3)=2 then:ttr=ttp:end if
  if (ttp band 3)=3 then:ttg=ttp:end if
  scr12block4x1vh(eee band 63,4*(int(eee/64)),ttr,ttg,ttb,1,4)
  next
msx2pwritetext8x8(2,3,3,@text04r01,@text04r02-@text04r01,4,1,@typeface)
msx2pwritetext8x8(0,4,3,@text04r02,@text04r03-@text04r02,4,1,@typeface)
msx2pwritetext8x8(0,5,3,@text04r03,@text04r04-@text04r03,4,1,@typeface)
msx2pwritetext8x8(0,6,3,@text04r04,@text04r05-@text04r04,4,1,@typeface)
msx2pwritetext8x8(0,7,3,@text04r05,@text04r06-@text04r05,4,1,@typeface)
msx2pwritetext8x8(0,8,3,@text04r06,@text04r07-@text04r06,4,1,@typeface)
msx2pwritetext8x8(0,9,3,@text04r07,@text04r08-@text04r07,4,1,@typeface)
msx2pwritetext8x8(0,10,3,@text04r08,@text04r09-@text04r08,4,1,@typeface)
msx2pwritetext8x8(1,23,1,@text01r03,@text01r04-@text01r03,4,1,@typeface)
'for eee=0 to 200:msxwaitvbl():next
'print at 2,3;ink 1;paper 5;
'" WELL, THAT IS IT. THIS IS MO"
'"STLY AN EXPERIENCE USING ULAPLUS"
'", AND SOME AESTETIC LIMITATION N"
'"OSTALGY FROM THE GOOD OLD ZX81 M"
'"ACHINES. AND ABOUT THE INFORMATI"
'"ON CITED HERE, BE WELCOME LOOKIN"
'"G FROM THEM FROM THE USUAL SEARC"
'"H ENGINES. CHEERS. "
'" WELL, THAT IS IT. THIS IS MO"
'"STLY AN EXPERIENCE USING A MSX2+"
'" YJK SCREENMODE, AND SOME AESTET"
'"IC NOSTALGY FROM THE GOOD OLD ZX"
'"81 MACHINES. AND ABOUT THE INFOR"
'"MATION CITED HERE, BE WELCOME LO"
'"OKING FROM THEM FROM THE USUAL S"
'"EARCH ENGINES. CHEERS. "
fr4lp:
jst0=msxjoystick(0):jst1=msxjoystick(1):jst2=jst0 band jst1
snsm=0:for eee=0 to 8:snsm=snsm bor 255-(msxsnsmat(eee)):next
if snsm=0 and (jst2 band 63)=63 then:goto fr4lp:end if





goto loop03

'------------------------------------------------------------------------------
'-------------------------------------------------------------------------------

'loop1
'print at 19,1;ink 0;paper 1;" NOTHING THING "
'print at 21,1;ink 0;paper 1;" PAULO SILVA, MARCH 2013 "
'print at 22,1;ink 0;paper 1;" PUSH ANY KEY "
'pause 0

'print at 2,3;ink 1;paper 5;" THIS GAME IS ABOUT NOTHING THINGS. NOTHING THING IS A TERM VERY USED FROM JACQUE FRESCO, FROM HIS AUDIO MATERIALS AND ONLINE SEMINARS, ABOUT THE VENUS PROJECT. DEPENDING ON THE PURPOSE, A GAME LIKE THIS CAN BE A NOTHING THING. "
'print at 22,1;ink 1;paper 5;" PUSH ANY KEY "
'pause 0

'print at 2,3;ink 1;paper 5;" AND MAYBE THIS GAME IS NOT A GAME AT ALL. THE MAIN PURPOSE IS TO CALL PEOPLE FOR THE IMPORTANCE OF DEVELOPING GAMES ABOUT RESOURCE BASED ECONOMY, WHICH IS VERY PROBABLY THE ONLY SOLUTION WE MAY HAVE FOR A SUSTAINABLE FUTURE OF THE WHOLE MANKIND. "
'print at 22,1;ink 1;paper 5;" PUSH ANY KEY "
'pause 0

'print at 2,3;ink 1;paper 5;" WELL, THAT IS IT. THIS IS MOSTLY AN EXPERIENCE USING ULAPLUS, AND SOME AESTETIC LIMITATION NOSTALGY FROM THE GOOD OLD ZX81 MACHINES. AND ABOUT THE INFORMATION CITED HERE, BE WELCOME LOOKING FROM THEM FROM THE USUAL SEARCH ENGINES. CHEERS. "
'print at 22,1;ink 1;paper 5;" PUSH ANY KEY "
'pause 0

'goto loop01

text01r01:
asm
  defb " NOTHING THING "
  end asm
text01r02:
asm
  defb " PAULO SILVA, MAR/SEP 2013 "
  end asm
text01r03:
asm
  defb " PUSH ANY KEY "
  end asm
text01r04:
text02r01:
asm
  defb " THIS GAME IS ABOUT NOTHING T"
  end asm
text02r02:
asm
  defb "HINGS. NOTHING THING IS A TERM V"
  end asm
text02r03:
asm
  defb "ERY USED FROM JACQUE FRESCO, FRO"
  end asm
text02r04:
asm
  defb "M HIS AUDIO MATERIALS AND ONLINE"
  end asm
text02r05:
asm
  defb " SEMINARS, ABOUT THE VENUS PROJE"
  end asm
text02r06:
asm
  defb "CT. DEPENDING ON THE PURPOSE, A "
  end asm
text02r07:
asm
  defb "GAME LIKE THIS CAN BE A NOTHING "
  end asm
text02r08:
asm
  defb "THING. "
  end asm
text02r09:
text03r01:
asm
  defb " AND MAYBE THIS GAME IS NOT A"
  end asm
text03r02:
asm
  defb " GAME AT ALL. THE MAIN PURPOSE I"
  end asm
text03r03:
asm
  defb "S TO CALL PEOPLE FOR THE IMPORTA"
  end asm
text03r04:
asm
  defb "NCE OF DEVELOPING GAMES ABOUT RE"
  end asm
text03r05:
asm
  defb "SOURCE BASED ECONOMY, WHICH IS V"
  end asm
text03r06:
asm
  defb "ERY PROBABLY THE ONLY SOLUTION W"
  end asm
text03r07:
asm
  defb "E MAY HAVE FOR A SUSTAINABLE FUT"
  end asm
text03r08:
asm
  defb "URE OF THE WHOLE MANKIND. "
  end asm
text03r09:
text04r01:
asm
  defb " WELL, THAT IS IT. THIS IS MO"
  end asm
text04r02:
asm
  defb "STLY AN EXPERIENCE USING A MSX2+"
  end asm
text04r03:
asm
  defb " YJK SCREENMODE, AND SOME AESTET"
  end asm
text04r04:
asm
  defb "IC NOSTALGY FROM THE GOOD OLD ZX"
  end asm
text04r05:
asm
  defb "81 MACHINES. AND ABOUT THE INFOR"
  end asm
text04r06:
asm
  defb "MATION CITED HERE, BE WELCOME LO"
  end asm
text04r07:
asm
  defb "OKING FROM THEM FROM THE USUAL S"
  end asm
text04r08:
asm
  defb "EARCH ENGINES. CHEERS. "
  end asm
text04r09:

'-------------------------------------------------------------------------------
eee=0

'- http://www.colourlovers.com/palette/817818/the_color_of_myblood 
scr12block4x1ulvh(3,32*4,$B5002D,1,4)
scr12block4x1ulvh(4,32*4,$E30400,1,4)
scr12block4x1ulvh(5,32*4,$FF4402,1,4)
scr12block4x1ulvh(6,32*4,$D87A00,1,4)
scr12block4x1ulvh(7,32*4,$C65900,1,4)

'- http://www.colourlovers.com/palette/2171081/Morning_Giraffe
scr12block4x1ulvh(3,34*4,$99A39A,1,4)
scr12block4x1ulvh(4,34*4,$FFC396,1,4)
scr12block4x1ulvh(5,34*4,$E78562,1,4)
scr12block4x1ulvh(6,34*4,$F83345,1,4)
scr12block4x1ulvh(7,34*4,$462434,1,4)

'- http://www.colourlovers.com/palette/731215/yellow_tree
scr12block4x1ulvh(3,36*4,$F0DB1D,1,4)
scr12block4x1ulvh(4,36*4,$A6F01D,1,4)
scr12block4x1ulvh(5,36*4,$8DCF13,1,4)
scr12block4x1ulvh(6,36*4,$4C5959,1,4)
scr12block4x1ulvh(7,36*4,$141414,1,4)

'- http://www.colourlovers.com/palette/663521/Signo_Oculto
scr12block4x1ulvh(3,38*4,$545A39,1,4)
scr12block4x1ulvh(4,38*4,$7D6139,1,4)
scr12block4x1ulvh(5,38*4,$BB694B,1,4)
scr12block4x1ulvh(6,38*4,$B3958D,1,4)
scr12block4x1ulvh(7,38*4,$AFB8C4,1,4)

scr12block4x1ulvh(27,37*4,$EEFF22,23,12)

for eez=0 to 63
  scr12block4x1vh(eez,40*4,$FF      ,eez*4    ,$00      ,1,4)
  scr12block4x1vh(eez,41*4,$FF-eez*4,$FF      ,$00      ,1,4)
  scr12block4x1vh(eez,42*4,$00      ,$FF      ,eez*4    ,1,4)
  scr12block4x1vh(eez,43*4,$00      ,$FF-eez*4,$FF      ,1,4)
  scr12block4x1vh(eez,44*4,eez*4    ,$00      ,$FF      ,1,4)
  scr12block4x1vh(eez,45*4,$FF      ,$00      ,$FF-eez*4,1,4)
  scr12block4x1vh(eez,46*4,eez*4    ,eez*4    ,eez*4    ,1,4)
  next

loop02:
goto loop02
