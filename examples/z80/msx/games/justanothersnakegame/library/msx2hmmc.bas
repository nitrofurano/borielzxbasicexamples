sub msx2hmmc(txpos as uinteger,typos as uinteger,txsiz as uinteger,tysiz as uinteger,tadr as uinteger ):
  '$F3B9..$F3C0
  poke uinteger $F3B9,txpos
  poke uinteger $F3BB,typos
  poke uinteger $F3BD,txsiz
  poke uinteger $F3BF,tysiz
  '- tadr= ix+(13,12)
  '- needing to use a "dummy area" from system variables? $F3B9..$F3C0 ?
  asm
   ;based on a code from jrtursan
   ;http://www.msxposse.com/site/forum/viewtopic.php?p=12871&sid=b43b9535707389a59b2e47ae7de647cc
   main1: 
    di 
    ld hl,$F3B9   ; vdp command data - replace with $F3B9 ? -     ld hl,command_hmmc1
    ;ld de,graphic1   ; graphic to dump - replace with d=(ix+13) and e=(ix+12) ?
    ld d,(ix+13)
    ld e,(ix+12)
    ld a,36   ; first register to write 
    out ($99),a 
    ld a,17+128 ; autoincrement mode 
    out ($99),a 
    ld c,$9b 
    ; executes 8 out's, 1 for every register
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    ; executes 3 out's, 1 for every register
    ld a,(de)
    out ($9b),a
    ld a,$00
    out ($9b),a
    ld a,$f0
    out ($9b),a
   waitvdp:
    call readstatus  ; reads s#2 
    bit 0,l 
    jp z,endvdp ; the command is over 
    bit 7,l 
    jp z,waitvdp   ; still not ready to transfer next byte 
    inc de 
    ld a,(de) 
    out ($99),a 
    ld a,128+44 
    out ($99),a  ; write next byte to r#44 
    jp waitvdp  ; loop 
   ;- readstatus - reads s#2 - input : none - output : l=s#2, ei 
   readstatus:
    ld a,2 
    di 
    out ($99),a 
    ld a,15+128 
    out ($99),a 
    in a,($99) 
    ld l,a 
    ld a,0 
    out ($99),a 
    ld a,15+128 
    out ($99),a 
    ret 
   endvdp:
    ei 
   end asm
  end sub

