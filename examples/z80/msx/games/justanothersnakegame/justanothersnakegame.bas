#include "library/msx2filvrm.bas"
#include "library/msx2ldirvm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxrnd.bas"
#include "library/msxscreen.bas"
#include "library/msx2vpoke.bas"
#include "library/msx2hmmc.bas"
#include "library/msxpalette.bas"
#include "library/msxsnsmat.bas"
#include "library_custom/CoastGarterSnake.bas"
#include "library_custom/CoastGarterSnake_palette.bas"

'-------------------------------------------------------------------------------
'----- variables
dim ee1x as uinteger at $E010
dim ee1y as uinteger at $E012
dim ee1q as uinteger at $E014
dim eelp as uinteger at $E016
dim rompos as uinteger at $E018
dim xdir as byte at $E020
dim ydir as byte at $E021
dim xpos as byte at $E022
dim ypos as byte at $E023
dim ccur as ubyte at $E024
dim colisn as ubyte at $E025
dim keyband as ubyte at $E026
dim snkln as uinteger at $E030
dim snklndc as uinteger at $E032
dim eesd as uinteger at $E034
dim xntz as uinteger at $E036
dim yntz as uinteger at $E038
dim keybbf as ubyte at $E03A
dim gameover as ubyte at $E03C


'-------------------------------------------------------------------------------
'----- subroutines
sub put8x8charlaced(txpo as uinteger, typo as uinteger, tchpo as uinteger, tadrpo as uinteger )
  msx2hmmc((txpo*8),(typo*4),8,4,tadrpo+(tchpo*32))
  msx2hmmc((txpo*8),256+(typo*4),8,4,tadrpo+16+(tchpo*32))
  end sub
sub write8x8lacedtext(txpo2 as uinteger, typo2 as uinteger, tadrst as uinteger, tadrln as uinteger, tadrpo2 as uinteger )
  dim tlpx as uinteger at $E000
  for tlpx=0 to tadrln-1
    put8x8charlaced(txpo2+tlpx,typo2,peek(tadrst+tlpx),tadrpo2)
    next
  end sub
sub pokemap(txpo3 as uinteger,typo3 as uinteger,tvl3 as ubyte,tadrpo3 as uinteger)
  poke ((tadrpo3+((typo3 mod 64)*64)+(txpo3 mod 64)),tvl3)
  end sub
function peekmap(txpo4 as uinteger,typo4 as uinteger,tadrpo4 as uinteger) as ubyte
  return peek(tadrpo4+((typo4 mod 64)*64)+(txpo4 mod 64))
  end function

'-------------------------------------------------------------------------------
'----- colour and screenmode

'- apagar - msxpalettehx(2,$222)

'----- transfering palette
for ee1x=0 to 15
  msxpalettehx(ee1x,peek(uinteger,@bin01palette+(ee1x*2) ) )
  next

msxcolor(11,12,2)
msxscreen(7,0,0,0)

'----- lacing the screen
'- vdp(10)=vdp(10) or 12
'- set IL and EO bits at vdp r#9
'- help from BiFi from #msxdev irc, but i have no idea how this code is doing or %00001100 over r#9 at all! :D
'- what are "ld b,63" and "ld c,2"?
asm
  ld a,($ffe8)
  or %00001100  ; $0C
  ld b,a
  ld c,9
  call $0047
  ld b,63
  ld c,2
  call $0047
  end asm

'-------------------------------------------------------------------------------
'----- area for snake parts coordinates - $E400 - $EFFF

for ee1x=$C000 to $D000:poke ee1x,0:next

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'----- main game loop
loopmain:

'----- cls -2 (both even and odd display banks)
for ee1x=0 to $D3FF step 256
  poke $faf6,0:msx2filvrm($CD,256,ee1x)
  poke $faf6,1:msx2filvrm($DC,256,ee1x)
  next

'----- writing title
write8x8lacedtext(3,45,@text01,25,@bin01)
write8x8lacedtext(3,46,@text02,25,@bin01)
write8x8lacedtext(3,48,@text03,14,@bin01)

'-------------------------------------------------------------------------------
'----- keyboard loop
looptitlekbchk:
keyband=255
for ee1x=0 to 8
  keyband=keyband band msxsnsmat(ee1x)
  next
if keyband=255 then:
  goto looptitlekbchk
  end if

gameover=0

'-------------------------------------------------------------------------------
'----- cls -2 (both even and odd display banks)
for ee1x=0 to $d3FF step 256
  poke $faf6,0:msx2filvrm($CD,256,ee1x)
  poke $faf6,1:msx2filvrm($DC,256,ee1x)
  next

'-------------------------------------------------------------------------------
'- variables
colisn=0:snklndc=12
ccur=0:colisn=0
snkln=8:snklndc=12:rompos=0
xpos=12:ypos=14
xdir=1:ydir=0

'-------------------------------------------------------------------------------
'- random
eesd=msxrnd(eesd):xntz=(eesd mod 58)+3
eesd=msxrnd(eesd):yntz=(eesd mod 49)+2

'-------------------------------------------------------------------------------
'- clean map?
for ee1x=$C000 to $E000
  poke ee1x,0
  next
for ee1x=$C000 to $C000+3391
  poke ee1x,code("A")
  'poke ee1x,ee1x
  next



'-------------------------------------------------------------------------------
'----- game loop
loopgame:

keybbf=msxsnsmat(8)
if (keybbf band 128)=0 and (ydir<>0) then:xdir= 1:ydir= 0:end if
if (keybbf band  16)=0 and (ydir<>0) then:xdir=-1:ydir= 0:end if
if (keybbf band  64)=0 and (xdir<>0) then:xdir= 0:ydir= 1:end if
if (keybbf band  32)=0 and (xdir<>0) then:xdir= 0:ydir=-1:end if

eesd=msxrnd(eesd bxor keybbf)
xpos=xpos+xdir
ypos=ypos+ydir

if (xpos=xntz) and (ypos=yntz) then:
  eesd=msxrnd(eesd):xntz=(eesd mod 58)+3
  eesd=msxrnd(eesd):yntz=(eesd mod 49)+2
  snkln=snkln+1
  end if

if ypos<0 then:ypos=52:end if
if xpos<0 then:xpos=63:end if
if ypos>52 then:ypos=0:end if
if xpos>63 then:xpos=0:end if


'----- movimento da cobra
'for eelp=$EFFD-(snkln*2) to $EFFD step 2
for eelp=$EFFD-1024 to $EFFD step 2
  poke uinteger eelp,peek(uinteger,eelp+2)
  next
poke $EFFE,xpos:poke $EFFF,ypos


'- put8x8charlaced(4,2,peekmap(xpos,ypos,$C000),@bin01)  '----- debug

if peekmap(xpos,ypos,$C000)=code("C") then:
  gameover=1
  end if



'----- draw head and tail of the snake, and the pokes are not working?
ccur=(ccur+1)band 15
put8x8charlaced(xpos,ypos,ccur,@bin01)
pokemap(xpos,ypos,code("C"),$C000)
put8x8charlaced(peek($EFFE-(snkln*2)),peek($EFFF-(snkln*2)),$20,@bin01)
pokemap(peek($EFFE-(snkln*2)),peek($EFFF-(snkln*2)),code("B"),$C000)

'- replace with random
put8x8charlaced(xntz,yntz,ccur,@bin01)

  asm
    halt
    end asm

  if gameover=0 then
    goto loopgame
    end if

for ee1x=1 to 50
  asm
    halt
    end asm
  next

  goto loopmain

'-------------------------------------------------------------------------------

text01:
asm
  defb " JUST ANOTHER SNAKE GAME "
  end asm
text01end:
text02:
asm
  defb " PAULO SILVA, OCT-DEC'13 "
  end asm
text02end:
text03:
asm
  defb " PUSH ANY KEY "
  end asm
text03end:

'-------------------------------------------------------------------------------

