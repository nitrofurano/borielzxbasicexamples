rm justanothersnakegame.asm justanothersnakegame.rom
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=justanothersnakegame.tmr
rm msxromheader.bin

echo;echo 1...
zxb.py justanothersnakegame.bas --asm --org=$((0x4010))

echo;echo 2...
zxb.py justanothersnakegame.bas --org=$((0x4010))

echo;echo 3...
cat justanothersnakegame.bin >> justanothersnakegame.tmr
filesize=$(stat -c%s "justanothersnakegame.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> justanothersnakegame.tmr
rm _dummybytes.bin justanothersnakegame.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=justanothersnakegame.tmr of=justanothersnakegame.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=justanothersnakegame.tmr of=justanothersnakegame.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=justanothersnakegame.tmr of=justanothersnakegame.rom ; fi
rm justanothersnakegame.tmr

mame cpc400 -video soft -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 justanothersnakegame.rom

#mame expert20 -video soft -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 justanothersnakegame.rom
#openmsx justanothersnakegame.rom

# ###########################################
# openmsx -machine msx1 -carta justanothersnakegame.rom
# openmsx -machine msx2 justanothersnakegame.rom
# openmsx -machine msx2plus justanothersnakegame.rom
# mess msx -cart1 justanothersnakegame.rom
# mess msx2 -cart1 justanothersnakegame.rom
# mess msx2p -cart1 justanothersnakegame.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################
