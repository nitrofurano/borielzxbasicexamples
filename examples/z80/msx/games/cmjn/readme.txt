CMJN
Paulo Silva, jan-jun’14 - gpl license

An isometric shmup, similar to Viewpoint, Zaxxon, etc.

During boot, the key “a” pressed will disable the alternative palette on msx2, 2+ and turbo-r.
No sound yet (i never tried sound on Boriel’s ZX-Basic Compiler, and i have no idea of how or what to implement).
During this game development Sega-SG1000 (and Master System by retrocompatibility) and ColecoVision are also being released. There are still some bugs to be fixed.

