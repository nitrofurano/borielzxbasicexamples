sub msx1ldirvmdeprecated(tlen as uinteger, tvram as uinteger, tram as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)
    ld d,(ix+7)
    ld e,(ix+6)
    ld h,(ix+9)
    ld l,(ix+8)
    call $005C
    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : BC - blocklength
'-            DE - Start address of VRAM
'-            HL - Start address of memory
'- Registers: All
