rm cmjn.asm cmjn.rom
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=cmjn.tmr
rm msxromheader.bin
zxb.py cmjn.bas --asm --org=$((0x4010))
zxb.py cmjn.bas --org=$((0x4010))
cat cmjn.bin >> cmjn.tmr
filesize=$(stat -c%s "cmjn.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> cmjn.tmr
rm _dummybytes.bin cmjn.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=cmjn.tmr of=cmjn.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=cmjn.tmr of=cmjn.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=cmjn.tmr of=cmjn.rom ; fi
rm cmjn.tmr
openmsx cmjn.rom

# ###########################################
# openmsx -machine msx1 -carta cmjn.rom
# openmsx -machine msx2 cmjn.rom
# openmsx -machine msx2plus cmjn.rom
# mess msx -cart1 cmjn.rom
# mess msx2 -cart1 cmjn.rom
# mess msx2p -cart1 cmjn.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################

