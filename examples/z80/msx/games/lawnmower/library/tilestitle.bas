goto tilestitle01end:

tilestitle01:
asm
  ;(4,0,8,3)
  defb $B5,$B5,$B5,$B5, $B6,$B6,$B7,$B8
  defb $B9,$BA,$BB,$BC, $BD,$BE,$BE,$BF
  defb $C0,$C1,$C2,$C3, $C4,$C5,$C6,$B5
  end asm

tilestitle02:
asm
  ;(8,3,7,5)
  defb $C0,$C7,$C5,$C6, $B5,$B5,$B5
  defb $B5,$C0,$C8,$C5, $C6,$B5,$B5
  defb $B5,$B5,$C9,$CA, $C5,$C6,$B5
  defb $B5,$B5,$B5,$CB, $C5,$CC,$B5    ;<-
  defb $B5,$B5,$B5,$B5, $CB,$CD,$CC    ;<-
  end asm

tilestitle03:
asm
  ;(13,8,6,5)
  defb $CE,$CF,$D0, $B5,$B5,$B5
  defb $B5,$D1,$D2, $D0,$B5,$B5
  defb $B5,$C0,$D3, $D4,$C6,$B5
  defb $B5,$B5,$C9, $D5,$D6,$C6
  defb $B5,$B5,$B5, $D7,$D8,$D6
  end asm

tilestitle04:
asm
  ;(14,13,17,11)
  defb $B5,$B5,$B5,$D9, $DA,$D6,$DB,$DE, $DF,$B8,$B5,$B5, $B5,$B5,$B5,$B5,$B5
  defb $B5,$B5,$BA,$DF, $BE,$BF,$BF,$C1, $C1,$E0,$E1,$C6, $B5,$B5,$B5,$B5,$B5
  defb $B5,$E2,$E3,$C3, $B5,$B5,$B5,$B5, $B5,$B5,$E4,$E5, $BB,$BB,$E6,$e7,$B5
  defb $B5,$CA,$C6,$B5, $B5,$B5,$B5,$B5, $B5,$B5,$B6,$C7, $E9,$CA,$EA,$C5,$B5

  defb $B5,$CB,$E1,$DB, $E6,$B6,$EB,$CF, $EC,$E6,$BD,$CA, $ED,$BC,$EF,$CA,$F0
  defb $B5,$B5,$E9,$F0, $F1,$CD,$F2,$F1, $F3,$BD,$E9,$E9, $CA,$CA,$CA,$ED,$F4

  defb $B9,$BA,$E9,$E9, $E9,$CA,$D1,$CA, $E9,$F5,$F0,$CA, $BC,$E9,$EF,$EA,$F6
  defb $F2,$F3,$E9,$F0, $F7,$EA,$F2,$BD, $ED,$CA,$F3,$F7, $E9,$CA,$F1,$F8,$D4
  defb $CA,$BC,$F5,$EF, $E9,$E9,$CA,$F9, $BC,$E9,$F1,$EE, $CA,$F4,$FA,$D4,$D4
  defb $D9,$E9,$BC,$CA, $CA,$EE,$FC,$FC, $FB,$FC,$FC,$FC, $D4,$D4,$D4,$B5,$B5
  defb $CB,$FC,$FC,$FD, $FA,$FE,$C3,$C3, $C3,$C3,$C3,$C3, $FF,$B5,$B5,$B5,$B5

  end asm

tilestitle05:
tilestitle01end:
