#include "library/msx1filvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxscreen.bas"
#include "library/msx1vpoke.bas"
#include "library/msx1vpeek.bas"
#include "library/msxpalette.bas"
#include "library/msx1putsprite.bas"
#include "library/msxsnsmat.bas"
#include "library/msxjoystick.bas"
#include "library/_msx16x16sprites.bas"
#include "library/msxrnd.bas"
#include "library/lawnmower_charmap10b_msx_bw_8pxw.bas"
#include "library/attributes.bas"
#include "library/tilestitle.bas"
#include "library/msxwaitvbl.bas"

'-------------------------------------------------------------------------------
'- defining variables

dim eee as uinteger at $E010
dim ee0 as uinteger at $E012
dim ee1 as uinteger at $E014
dim ee2 as uinteger at $E016
dim seed as uinteger at $E018

dim a as integer at $E020
dim b as integer at $E022
dim c as integer at $E024
dim d as integer at $E026
dim n as integer at $E028
dim i as integer at $E02A
dim j as integer at $E02C
dim m as integer at $E02E
dim o as integer at $E030
dim u as integer at $E032
dim v as integer at $E034
dim x as integer at $E036
dim y as integer at $E038
dim z as integer at $E03A
dim x2 as integer at $E03C
dim y2 as integer at $E03E
dim ch as integer at $E040
dim alen as integer at $E042
dim iz as integer at $E044
dim x2s as integer at $E046
dim y2s as integer at $E048
dim lev as integer at $E04A
dim gh as integer at $E04C
dim lives as integer at $E04E
dim ntp as integer at $E050
dim oil as integer at $E052
dim sc as integer at $E054
dim li as integer at $E056
dim col as integer at $E058
dim lo as integer at $E05A
dim hi as integer at $E05C
dim keyb as integer at $E05E
dim actu as integer at $E060
dim jk as integer at $E062

dim debug as ubyte at $E064
dim asv as ubyte at $E01A
dim jstz as ubyte at $E01B
dim vpkch as ubyte at $E01C

debug=0 '- 0

'- bugs
'-   - nao sei o que e o ecra vermelho qdo acaba combustivel
'-   - remover click do teclado (como?)


poke $F3DB,0

'dim shared as string a_st,txo_st,c_st
'dim shared as integer attrvm(40,25)

'-------------------------------------------------------------------------------
'- sub routines...

sub zprintdecimal(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/10000) mod 10)
  msx1vpoke(6144+tpos+1,48+int(tval/1000) mod 10)
  msx1vpoke(6144+tpos+2,48+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+3,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+4,48+tval mod 10)
  end sub

sub zprintdecimal4(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/1000) mod 10)
  msx1vpoke(6144+tpos+1,48+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+2,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+3,48+tval mod 10)
  end sub

sub zprintdecimal2(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+1,48+tval mod 10)
  end sub

sub prtsc(tpx1 as uinteger,tpy1 as uinteger,tsl1 as uinteger,tln1 as uinteger,tco1 as ubyte):
  dim tls1 as uinteger at $E000
  dim tch1 as ubyte at $E002
  for tls1=0 to tln1-1:
    tch1=peek(tsl1+tls1)+tco1
    if peek(tsl1+tls1)=32 then:tch1=$0A:end if
    msx1vpoke($1800+tpy1*32+tpx1+tls1,tch1)
    next
  end sub

sub putcharlim(tpx2 as uinteger,tpy2 as uinteger,tch2 as ubyte,tlm2 as ubyte):
  dim tlt2 as ubyte at $E004
  tlt2=tch2
  if tlt2>tlm2 then:tlt2=tlm2:end if
  msx1vpoke($1800+tpy2*32+tpx2,tlt2)
  end sub

sub prtsclim(tpx1 as uinteger,tpy1 as uinteger,tsl1 as uinteger,tln1 as uinteger,tco1 as ubyte,tlm1 as ubyte):
  dim tls1 as uinteger at $E000
  dim tch1 as ubyte at $E002
  dim tlt1 as ubyte at $E003
  for tls1=0 to tln1-1:
    tch1=peek(tsl1+tls1)+tco1
    if tch1>(tlm1+tco1) then:tch1=tlm1+tco1:end if
    if peek(tsl1+tls1)=32 then:tch1=$0A:end if
    msx1vpoke($1800+tpy1*32+tpx1+tls1,tch1)
    next
  end sub

sub inittiles1():
for eee=0 to $17FF step $800
  msx1ldirvm($0100-96-16+eee,@charset01a1,@charset01a1end-@charset01a1)
  msx1ldirvm($0300+eee,@charset01a1+96+16,@charset01a1end-@charset01a1-96-16)
  for ee0=0 to $2FF step $100
    msx1ldirvm($0500+eee+ee0,@charset01a1+96+16+256,@charset01a1end-@charset01a1-96-16-256)
    next
  for ee0=0 to 7
    msx1filvrm($2000+(ee0*$100)+eee,peek(@attributes32ch+ee0),256)
    next
  for ee0=0 to 15
    msx1filvrm($2000+(ee0*8)+eee,ee0*$11,8)
    next
  for ee0=0 to 15
    msx1filvrm($2080+(ee0*8)+eee,peek(@attributes1ch+ee0),8)
    next
  for ee0=0 to 3
    msx1ldirvm($0400+eee+ee0*256,@charset01a1+21*8,8)
    msx1ldirvm($0400+(27*8)+eee+ee0*256,@charset01a1+22*8,8)
    msx1ldirvm($0400+(29*8)+eee+ee0*256,@charset01a1+23*8,8)
    next
  next
  end sub

sub inittiles2():
for eee=0 to $17FF step $800
  msx1filvrm($5A8+eee,$F0,$258)
  for ee0=0 to 149
    msx1filvrm($25A8+(ee0*4)+eee,peek(@attributeshch+ee0),4)
    next
  next
  end sub

'-------------------------------------------------------------------------------
'- startup
'-------------------------------------------------------------------------------
'- display
'-------------------------------------------------------------------------------
msxcolor($6,$F,$1)
msxscreen(2,0,0,0)
msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
'-------------------------------------------------------------------------------
'- showing tiles...
'-------------------------------------------------------------------------------
msx1filvrm(0,0,$4000)
msx1filvrm($2000,$1F,$1800)
for eee=0 to 767:msx1vpoke($1800+eee,eee):next
inittiles1():
msxwaitvbl(10)  '20
inittiles2():
msxwaitvbl(10)
'-------------------------------------------------------------------------------
'- game
'-------------------------------------------------------------------------------
10:
zmbstart:
hi=0
gosub 330 '- zmbdefineudg
20:
zmbstarttitle:
gosub 280 '-zmbtitle
gh=0:lives=3:oil=5000:sc=0
30:
if oil<=0 then
  goto 170
  end if
40:
z=5:j=-2:v=4:m=8:li=10:col=15
'a$="\a"
if lives<=0 then
  goto 170
  end if
50:
d=1:lo=lev*10
msx1filvrm($1800,$02,768) 'border 4:paper 4:cls
for x=0 to 2 'for x=149 to 151
'  j=j+2
'  if j=4 then:j=j+1:end if
60:
   for n=1 to 10 'for n=1 to lo
     seed=msxrnd(seed):a=(seed mod 22)+1 'a=int(rnd*16)+2
     seed=msxrnd(seed):b=(seed mod 32)+0 'b=int(rnd*23)+4
     msx1vpoke($1800+a*32+b,25+x) 'print at a,b;ink j;chr$ x
     next
  next

gosub printhud1

asv=$14 '(apagar)

zmbgameloop:
70:
msx1vpoke($1800+li*32+col,asv) 'print at li,col;paper v; ink 0;a$
if oil<=0 then
  prtsc(10,0,@textgameplay02,10,32*0) 'print at 0,4;ink 2;"0  ";ink 2;paper 7;at 0,10;"OUT OF OIL"
  goto 170
  end if
80:
if oil<100 then
  prtsc(12,0,@textgameplay02+7,7,32*0)   'print at 0,12;ink 7;paper 2;flash 1;"OIL LOW"
  end if

if debug<>0 then
  msx1vpoke($1800+23*32+12,48+(gh mod 10))
  end if

'bug here?
90:
if gh=0 then
  seed=msxrnd(seed):jk=(seed mod 210)+1 'jk=int(rnd*70)+1

  if debug<>0 then
    msx1vpoke($1800+23*32+14,48+(int(jk/100) mod 10))  
    msx1vpoke($1800+23*32+15,48+(int(jk/10) mod 10))
    msx1vpoke($1800+23*32+16,48+(jk mod 10))
    end if
  
  if jk<10 then   '- <3  (/210)
    seed=msxrnd(seed):b=(seed mod 16)+2  'b=rnd*16+2
    seed=msxrnd(seed):c=(seed mod 23)+4  'c=int(rnd*23)+4
    if msx1vpeek($1800+b*32+c)=2 then 'if attr(b,c)=32 then: '-- <--------------
      msx1vpoke ($1800+b*32+c,$18) 'print at b,c;ink 6;bright 1;chr$ 148
      'beep .05,5:beep .05,10
      gh=gh+1
      end if
    end if
  end if

100:
msxwaitvbl(8)
jstz=msxjoystick(0) band msxjoystick(1) 'b$=inkey$
seed=seed+jstz

if (jstz band 15)<>15 or (msxsnsmat(8) band 240)<>240 then 'if b$="5" or b$="6" or b$="7" or b$="8" then:
'  beep .005,3
  'sc=sc+1  '- bug: increment sc only when over grass
  'gosub printscore1  - descomentar???????
  'msx1vpoke($1800+28,48+(int(sc/100) mod 10))  '  print at 0,29;ink 1;sc
  'msx1vpoke($1800+29,48+(int(sc/10) mod 10))
  'msx1vpoke($1800+30,48+(sc mod 10))
  end if
110: 'down
if ((jstz band 2)=0 or (msxsnsmat(8) band 64)=0) and li<22 then 'if b$="6" and li<18 then:
  asv=$16 '  let a$="\c"
  li=li+1
  if msx1vpeek($1800+li*32+col)=2 then:sc=sc+1:gosub printscore1:end if
  msx1vpoke($1800+(li-1)*32+col,$13) 'print at li-1,col;ink 7;"\i"
  gosub 210
  end if
120: 'up
if ((jstz band 1)=0 or (msxsnsmat(8) band 32)=0) and li>1 then 'if b$="7" and li>2 then:
  asv=$14  '  let a$="\a"
  li=li-1
  if msx1vpeek($1800+li*32+col)=2 then:sc=sc+1:gosub printscore1:end if
  msx1vpoke($1800+(li+1)*32+col,$13) '  print at li+1,col;ink 7;"\i"
  gosub 210
  end if
130: 'left
if ((jstz band 4)=0 or (msxsnsmat(8) band 16)=0) and col>0 then  'if b$="5" and col>4 then:
  asv=$15 '  let a$="\b"
  col=col-1
  if msx1vpeek($1800+li*32+col)=2 then:sc=sc+1:gosub printscore1:end if
  msx1vpoke($1800+li*32+col+1,$13) '  print at li,col+1;ink 7;"\i"
  gosub 210
  end if
140: 'right
if ((jstz band 8)=0 or (msxsnsmat(8) band 128)=0) and col<31 then 'if b$="8" and col<27 then:
  asv=$17 '  let a$="\d"
  col=col+1
  if msx1vpeek($1800+li*32+col)=2 then:sc=sc+1:gosub printscore1:end if
  msx1vpoke($1800+li*32+col-1,$13) '  print at li,col-1;ink 7;"\i"
  gosub 210
  end if
150:
oil=oil-1
gosub printoil

goto zmbgameloop 'goto 70

160:
for n=1 to 20:
'  zx=rnd*50:beep .1,zx
  next
return

''---------------- bug!!! -------------------------------------
170: 'goto
zmbgameover:   '- gameover - bug?
'msx1filvrm($1800,$08,768)

'print at 10,12;paper 7;ink 1;flash 1;"GAME OVER"
msx1ldirvm($1800+12*32+12,@textgameplay08,@textgameplay09-@textgameplay08)

'push any key
msx1ldirvm($1800+21*32+10,@textintro09,@textintro10-@textintro09)

'pause 10
if sc>hi then
' print at 2,9;ink 1;flash 1;"NEW HIGH SCORE!"
msx1ldirvm($1800+2*32+9,@textgameplay09,@textgameplay10-@textgameplay09)
  for n=1 to 5:
'    beep .5,5:beep .05,3:beep .05,10:beep .05,15:beep .05,20
    next
  hi=sc
  end if
180:
190:
200:

if debug<>0 then
msxwaitvbl(200):goto zmbstart
end if


for n=1 to 250
asm
halt
end asm
next


gosub keychecking1f
goto zmbstarttitle

'do:loop
goto 170

210:
vpkch=msx1vpeek($1800+li*32+col)

if debug<>0 then
msx1vpoke($1800+16,48+(int(vpkch/100) mod 10))
msx1vpoke($1800+17,48+(int(vpkch/10) mod 10))
msx1vpoke($1800+18,48+(vpkch mod 10))
end if

if debug<>0 then
msx1vpoke($1800+20,vpkch)
end if

if vpkch<=$1B then 'if screen$(li,col)<>" " then:    '$10?
  goto 230
  end if
220:
return
230:
if vpkch=$1A then 'if attr(li,col)=34 then:
  msx1ldirvm($1800+19*32+7,@textgameplay03,18) 'print at 21,7;paper 2;ink 7;flash 1;"YOU HAVE EXPLODED!"
  gosub 160
  msxwaitvbl(50) 'pause 50
  lives=lives-1
  'msx1filvrm($1800,$08,768) 'cls
  goto 30
  end if
240:
if vpkch=$19 then 'if attr(li,col)=32 then:
  msx1ldirvm($1800+19*32+7,@textgameplay04,18) 'print at 21,7;paper 0;ink 7;flash 1;"YOU'VE HIT A ROCK!"
  gosub 160
  msxwaitvbl(50) 'pause 50
  lives=lives-1
  'msx1filvrm($1800,$08,768) 'cls
  goto 30
  end if
250:
if vpkch=$1B then 'if attr(li,col)=37 then:
  msx1ldirvm($1800+19*32+2,@textgameplay05,28) 'print at 21,2;paper 1;ink 7;flash 1;"SLICED A ROSE!OIL SPILLAGE!"
  gosub 160
  msxwaitvbl(50) 'pause 50
  oil=oil-100
  gosub printoil

  'msx1filvrm($1800,$08,768) 'cls
  goto 30
  end if
260:
if vpkch=$18 then 'if attr (li,col)=102 then:
  
  msx1filvrm($1800+0*32+5,$01,27)
  msx1filvrm($1800+23*32+0,$01,32)

  msx1ldirvm($1800+23*32+4,@textgameplay06,24) 'print at 21,4;ink 7;flash 1;"50 GALLONS OF EXTRA OIL!"
  msx1ldirvm($1800+0*32+22,@textgameplay07,9) 'print at 0,10;flash 1;ink 7;paper 2;"REFUELING":
  for n=1 to 50:
    oil=oil+1
    gosub printoil

    msxwaitvbl(1)
'   beep .05,n
    next

  gh=0
  
  msx1filvrm($1800+0*32+5,$01,27)
  msx1filvrm($1800+23*32+0,$01,32)

  gosub printhud1

  goto 70
  end if

270:
oil=oil-25 '-  -50
gosub printoil

goto 70
280:
zmbtitle:
gosub title1f
gosub keychecking1f

290:
zmbinstructions:
gosub instructions1f
gosub keychecking1f
300:
310:
320:
return
330:
zmbdefineudg:
return
340:
350:
360:
10000:
zmbend:

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'- whatever...

do

gosub title1f
gosub keychecking1f
gosub instructions1f
gosub keychecking1f

loop
do:loop

title1f:
msx1filvrm($1800,$03,768)
inittiles1():
inittiles2():
for ee0=0 to 2:msx1ldirvm($1800+4+(0+ee0)*32,@tilestitle01+ee0*8,8):next
for ee0=0 to 4:msx1ldirvm($1800+8+(3+ee0)*32,@tilestitle02+ee0*7,7):next
for ee0=0 to 4:msx1ldirvm($1800+13+(8+ee0)*32,@tilestitle03+ee0*6,6):next
for ee0=0 to 10:msx1ldirvm($1800+14+(13+ee0)*32,@tilestitle04+ee0*17,17):next
msx1ldirvm($1800+1+15*32,@texttitle01,@texttitle02-@texttitle01)
msx1ldirvm($1800+1+17*32,@texttitle02,@texttitle03-@texttitle02)
msx1ldirvm($1800+1+18*32,@texttitle03,@texttitle04-@texttitle03)
msx1ldirvm($1800+1+20*32,@texttitle04,@texttitle05-@texttitle04)
'msxwaitvbl(200)
return

keychecking1f:
ee0=msxjoystick(0) band msxjoystick(1)
eee=$FF
for ee1=0 to 8
  eee=eee band msxsnsmat(ee1)
  next
if eee=$FF and (ee0 band 48)=48 then
  goto keychecking1f:end if
return

instructions1f:
''msx1filvrm($1800,$03,768)
msx1filvrm($1800,$0A,768)
inittiles1():
prtsc(11,2,@textintro01,@textintro02-@textintro01,32*4)
prtsc(1,5,@textintro02,@textintro03-@textintro02,32*3)
prtsc(1,10,@textintro03,@textintro04-@textintro03,32*2)
prtsc(4,11,@textintro04,@textintro05-@textintro04,32*2)
prtsc(4,12,@textintro05,@textintro06-@textintro05,32*2)
prtsc(4,13,@textintro06,@textintro07-@textintro06,32*2)
prtsc(1,15,@textintro07,@textintro08-@textintro07,32*4)
prtsc(1,19,@textintro08,@textintro09-@textintro08,32*5)
prtsc(1,22,@textintro09,@textintro10-@textintro09,32*2)
''msxwaitvbl(200)
''msx1filvrm($1800,$0A,768)
return

printoil:
msx1vpoke($1800+5,48+(int(oil/1000) mod 10))  'print at 0,4;ink 1;oil;" "
msx1vpoke($1800+6,48+(int(oil/100) mod 10))
msx1vpoke($1800+7,48+(int(oil/10) mod 10))
msx1vpoke($1800+8,48+(oil mod 10))
return

printhud1:
msx1filvrm($1800,$01,32)
msx1filvrm($1800+23*32,$01,32)
prtsc(1,0,@textgameplay01,4,32*0)
prtsc(22,0,@textgameplay01+4,6,32*0)
prtsc(1,23,@textgameplay01+10,6,32*0)
prtsc(19,23,@textgameplay01+16,9,32*0)
msx1vpoke($1800+23*32+7,48+(lives mod 10))
msx1vpoke($1800+23*32+28,48+(int(hi/100) mod 10))  '  print at 23,29;ink 1;hi
msx1vpoke($1800+23*32+29,48+(int(hi/10) mod 10))
msx1vpoke($1800+23*32+30,48+(hi mod 10))
msx1vpoke($1800+28,48+(int(sc/100) mod 10))  '  print at 0,29;ink 1;sc
msx1vpoke($1800+29,48+(int(sc/10) mod 10))
msx1vpoke($1800+30,48+(sc mod 10))
return

printscore1:
msx1vpoke($1800+28,48+(int(sc/100) mod 10))  '  print at 0,29;ink 1;sc
msx1vpoke($1800+29,48+(int(sc/10) mod 10))
msx1vpoke($1800+30,48+(sc mod 10))
return


'-------------------------------------------------------------------------------
'- text

texttitle01:
asm
  defb $0A,$8C,$81,$97,$8E,$0A,$8D,$8F,$97,$85,$92,$0A  ;defb " LAWN MOWER "
  end asm
texttitle02:
asm
  defb " MATTHEW EDWARDS, 1983 "
  end asm
texttitle03:
asm
  defb " PAULO SILVA, 2012-2020 "
  end asm
texttitle04:
asm
  defb " PUSH ANY BUTTON "
  end asm
texttitle05:

textintro01:
asm
defb "LAWN MOWER"
end asm
textintro02:
asm
defb "THE OBJECT OF THIS GAME IS TO   CUT AS MUCH GREEN GRASS AS      POSSIBLE BEFORE YOUR OIL RUNS   OUT"
end asm
textintro03:
asm
defb "AVOIDING"
end asm
textintro04:
asm
defb 30-64
defb " [EXPLODING CRICKET BALLS]"
end asm
textintro05:
asm
defb 29-64
defb " [ROCKS]"
end asm
textintro06:
asm
defb 31-64
defb " [FLOWERS]"
end asm
textintro07:
asm
defb "OIL CAN BE GAINED BY GOING TO   A BARREL WHICH WILL APPEAR ON   THE SCREEN OCCASIONALLY"
end asm
textintro08:
asm
defb "USE THE NORMAL CURSOR CONTROLS"
end asm
textintro09:
asm
defb "PUSH ANY KEY"
end asm
textintro10:

textgameplay01:
asm
defb "OIL:SCORE:LIVES:HI-SCORE:"
end asm
textgameplay02:
asm
defb "OUT"
defb $01
defb "OF"
defb $01
defb "OIL"
defb $01
defb "LOW"
end asm
textgameplay03:
asm
defb "YOU HAVE EXPLODED!"
end asm
textgameplay04:
asm
defb "YOU'VE HIT A ROCK!"
end asm
textgameplay05:
asm
defb "SLICED A ROSE! OIL SPILLAGE!"
end asm
textgameplay06:
asm
defb "50 GALLONS OF EXTRA OIL!"
end asm
textgameplay07:
asm
defb "REFUELING"
end asm
textgameplay08:
asm
defb "GAME OVER"
end asm
textgameplay09:
asm
defb "NEW HIGH SCORE!"
end asm
textgameplay10:
asm
defb "ANOTHER GAME(Y/N)?"
end asm
textgameplay11:





