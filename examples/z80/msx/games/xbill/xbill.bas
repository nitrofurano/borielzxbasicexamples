#include "library/msx1filvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxscreen.bas"
#include "library/msx1vpoke.bas"
#include "library/msx1putsprite.bas"
#include "library/msxsnsmat.bas"
#include "library/msxjoystick.bas"
#include "library/_msx16x16sprites.bas"
#include "library/msxrnd.bas"
#include "library/3c1b_chr_bmap.bas"
#include "library/3c1b_chr_attr.bas"
#include "library/xbill_sprites.bas"
#include "library/titletiles.bas"
#include "library/titletileschr.bas"
#include "library/3c1b_chr_bmap_lowercase.bas"

dim xq as uinteger at $E010
dim yq as uinteger at $E012
dim sq as uinteger at $E014
dim ep as uinteger at $E016
dim cp as uinteger at $E018
dim xl as uinteger at $E01A
dim yl as uinteger at $E01C
dim a as uinteger at $E01E
dim b as uinteger at $E020
dim c as uinteger at $E022
dim i as uinteger at $E024
dim x as uinteger at $E026
dim y as uinteger at $E028
dim d as uinteger at $E02A
dim mv as uinteger at $E02C
dim ym as uinteger at $E02E
dim lc as uinteger at $E030
dim ad as uinteger at $E032
dim jst0 as ubyte at $E034
dim xdv as uinteger at $E036
dim seed as uinteger at $E038
dim eee as uinteger at $E03A
dim ee0 as uinteger at $E03C
dim ee1 as uinteger at $E03E

dim xpbt as integer at $E040
dim ypbt as integer at $E042
dim xdbt as integer at $E044
dim ydbt as integer at $E046
dim cbt as ubyte at $E048
dim debug as ubyte at $E049

dim xpb0 as integer at $E050
dim ypb0 as integer at $E052
dim xpb1 as integer at $E054
dim ypb1 as integer at $E056
dim xpb2 as integer at $E058
dim ypb2 as integer at $E05A
dim xpb3 as integer at $E05C
dim ypb3 as integer at $E05E
dim xdb0 as integer at $E060
dim ydb0 as integer at $E062
dim xdb1 as integer at $E064
dim ydb1 as integer at $E066
dim xdb2 as integer at $E068
dim ydb2 as integer at $E06A
dim xdb3 as integer at $E06C
dim ydb3 as integer at $E06E
dim cb0 as ubyte at $E070
dim cb1 as ubyte at $E071
dim cb2 as ubyte at $E072
dim cb3 as ubyte at $E073

'dim zv1(5,5) as ubyte at $E080

debug=%10000011
debug=0

seed=51

seed=msxrnd(seed):xpb0=seed:seed=msxrnd(seed):ypb0=seed mod 384
seed=msxrnd(seed):xpb1=seed:seed=msxrnd(seed):ypb1=seed mod 384
seed=msxrnd(seed):xpb2=seed:seed=msxrnd(seed):ypb2=seed mod 384
seed=msxrnd(seed):xpb3=seed:seed=msxrnd(seed):ypb3=seed mod 384

seed=msxrnd(seed):xdb0=seed mod 5
seed=msxrnd(seed):ydb0=seed mod 5
seed=msxrnd(seed):xdb1=seed mod 5
seed=msxrnd(seed):ydb1=seed mod 5
seed=msxrnd(seed):xdb2=seed mod 5
seed=msxrnd(seed):ydb2=seed mod 5
seed=msxrnd(seed):xdb3=seed mod 5
seed=msxrnd(seed):ydb3=seed mod 5

'20 screen1:width32:keyoff:color 1,14,1:cls
'30 screen2,2:color1,14,1:poke&hfcaf,1
msxcolor($1,$E,$1)
msxscreen(2,0,0,0)
msx16x16sprites()
msx1filvrm($1800,$20,768)

'- corrigir
msx1ldirvm($0000,@chrbmap01,$800)
msx1ldirvm($0800,@chrbmap01,$800)
msx1ldirvm($1000,@chrbmap01,$800)
msx1ldirvm($2000,@chrattr01,$800)
msx1ldirvm($2800,@chrattr01,$800)
msx1ldirvm($3000,@chrattr01,$800)
msx1ldirvm($3800,@spr1,$800)


'-------------------------------------------------------------------------------
'title
'if debug band 4<>0 then
  msx1ldirvm($0300,@lowercase01,$100)
  msx1ldirvm($0B00,@lowercase01,$100)
  msx1ldirvm($1300,@lowercase01,$100)
  msx1filvrm($2300,$1F,$100)
  msx1filvrm($2B00,$1F,$100)
  msx1filvrm($3300,$1F,$100)
  msx1ldirvm($0400,@titletiles01,$400)
  msx1ldirvm($0C00,@titletiles01,$400)
  msx1ldirvm($1400,@titletiles01,$400)
  msx1filvrm($2400,$1F,$400)
  msx1filvrm($2C00,$1F,$400)
  msx1filvrm($3400,$1F,$400)
  msx1filvrm($2528,$5F,$2D8)
  msx1filvrm($2D28,$5F,$2D8)
  msx1filvrm($3528,$5F,$2D8)
  msx1filvrm($27D8,$1F,3)
  msx1filvrm($27E0,$1F,3)
  msx1filvrm($2FD8,$1F,3)
  msx1filvrm($2FE0,$1F,3)
  for eee=0 to 7
    msx1ldirvm($1845+eee*32,@titletileschr01+eee*22,22)
    next
  msx1ldirvm($1800+14*32+2,@texttitle01,@texttitle02-@texttitle01)
  msx1ldirvm($1800+15*32+4,@texttitle02,@texttitle03-@texttitle02)
  msx1ldirvm($1800+16*32+4,@texttitle03,@texttitle04-@texttitle03)
  msx1ldirvm($1800+17*32+2,@texttitle04,@texttitle05-@texttitle04)
  msx1ldirvm($1800+18*32+4,@texttitle05,@texttitle06-@texttitle05)
  msx1ldirvm($1800+22*32+2,@texttitle06,@texttitle07-@texttitle06)

titleloop01:
jst0=(msxjoystick(0) band msxjoystick(1))bxor $3F
if (jst0 band 48)=0 or (msxsnsmat(5) band 128)<>0 then:goto titleloop01:end if '- buttons
'-------------------------------------------------------------------------------


'cls
msx1filvrm($1800,$20,768)

'- corrigir
msx1ldirvm($0000,@chrbmap01,$800)
msx1ldirvm($0800,@chrbmap01,$800)
msx1ldirvm($1000,@chrbmap01,$800)
msx1ldirvm($2000,@chrattr01,$800)
msx1ldirvm($2800,@chrattr01,$800)
msx1ldirvm($3000,@chrattr01,$800)
msx1ldirvm($3800,@spr1,$800)

if debug band 128<>0 then
for yl=0 to 63
  for xl=0 to 127
    msx1vpoke($1800,48+int(yl/10) mod 10)
    msx1vpoke($1801,48+yl mod 10)
    msx1putsprite(17,xl,96,yl*4,1)
    asm
      halt
      end asm
   next:next
end if

gosub 8000
'msx1filvrm($1800,$20,768)

ep=0:cp=0:xl=2:yl=7:gosub 9000
ep=1:cp=1:xl=10:yl=1:gosub 9000
ep=2:cp=2:xl=24:yl=5:gosub 9000
ep=6:cp=3:xl=12:yl=10:gosub 9000
ep=4:cp=4:xl=20:yl=14:gosub 9000
ep=5:cp=5:xl=7:yl=16:gosub 9000

xq=160:yq=96:sq=32

150:
x=0
do
  x=x+1
  xdv=int(x/4)
  jst0=(msxjoystick(0) band msxjoystick(1))bxor $3F

if debug band 2<>0 then:
  msx1vpoke($1840,48+(int(jst0/100) mod 10))
  msx1vpoke($1841,48+(int(jst0/10) mod 10))
  msx1vpoke($1842,48+(jst0 mod 10))
end if

  if (jst0 band 8)<>0 or (msxsnsmat(8) band 128)=0 then:xq=xq+2:end if '-right
  if (jst0 band 4)<>0 or (msxsnsmat(8) band 16)=0 then:xq=xq-2:end if '-left
  if (jst0 band 2)<>0 or (msxsnsmat(8) band 64)=0 then:yq=yq+2:end if '- down
  if (jst0 band 1)<>0 or (msxsnsmat(8) band 32)=0 then:yq=yq-2:end if '- up
  xq=xq band $FF:yq=yq band $FF

  sq=32:if (jst0 band 48)<>0 or (msxsnsmat(5) band 128)=0 then:sq=33:end if '- buttons
  c=xdv mod 4

  if debug band 4<>0 then:
  msx1vpoke($1800,48+c)  'locate 0,0:print c
  end if

  ym=128
  if c mod 2<>1 then:ym=127:end if
  d=c
  if c=0 then:d=2:end if

if debug band 1<>0 then:
msx1vpoke($1820,48-(xdb0>2))
msx1vpoke($1822,48-(xdb1>2))
msx1vpoke($1824,48-(xdb2>2))
msx1vpoke($1826,48-(xdb3>2))
end if

msx1putsprite(0,xq,yq,sq*4,1)

'nao funciona - tentar melhorar
'for eee=0 to 3
'  poke @cb0+eee,1
'  xpbt=peek (integer,@xpb0+eee*2):ypbt=peek (integer,@ypb0+eee*2):xdbt=peek (integer,@xdb0+eee*2):ydbt=peek (integer,@ydb0+eee*2)
'  if (abs(xq-((xpbt/2)band $FF))<16) and (abs(yq-4-((ypbt/2)band $FF))<20) then:poke @cb0+eee,8:end if
'  msx1putsprite(20,xpbt/2,ypbt/2,((xdbt>2)*-8)*4,peek(@cb0+eee) )
'  msx1putsprite(21,xpbt/2,ypbt/2+16,(d+((xdbt>2)*-8))*4,peek(@cb0+eee) )
'  xpbt=xpbt+xdbt+510:ypbt=ypbt+ydbt+510
'  poke integer(@xpb0+eee*2),xpbt
'  poke integer(@ypb0+eee*2),ypbt
'  next

  cb0=1:if (abs(xq-((xpb0/2)band $FF))<16) and (abs(yq-4-((ypb0/2)band $FF))<20) then:
    cb0=8
    if (jst0 band 48)<>0 or (msxsnsmat(5) band 128)=0 then:
      seed=msxrnd(seed):xpb0=seed:seed=msxrnd(seed):ypb0=seed mod 384
      seed=msxrnd(seed):xdb0=seed mod 5:seed=msxrnd(seed):ydb0=seed mod 5
      end if
    end if
  msx1putsprite(20,xpb0/2,ypb0/2,((xdb0>2)*-8)*4,cb0)
  msx1putsprite(21,xpb0/2,ypb0/2+16,(d+((xdb0>2)*-8))*4,cb0)
  xpb0=xpb0+xdb0+510:ypb0=ypb0+ydb0+510

  cb1=1:if (abs(xq-((xpb1/2)band $FF))<16) and (abs(yq-4-((ypb1/2)band $FF))<20) then:
    cb1=8
    if (jst0 band 48)<>0 or (msxsnsmat(5) band 128)=0 then:
      seed=msxrnd(seed):xpb1=seed:seed=msxrnd(seed):ypb1=seed mod 384
      seed=msxrnd(seed):xdb1=seed mod 5:seed=msxrnd(seed):ydb1=seed mod 5
      end if
    end if
  msx1putsprite(22,xpb1/2,ypb1/2,((xdb1>2)*-8)*4,cb1)
  msx1putsprite(23,xpb1/2,ypb1/2+16,(d+((xdb1>2)*-8))*4,cb1)
  xpb1=xpb1+xdb1+510:ypb1=ypb1+ydb1+510

  cb2=1:if (abs(xq-((xpb2/2)band $FF))<16) and (abs(yq-4-((ypb2/2)band $FF))<20) then:
    cb2=8
    if (jst0 band 48)<>0 or (msxsnsmat(5) band 128)=0 then:
      seed=msxrnd(seed):xpb2=seed:seed=msxrnd(seed):ypb2=seed mod 384
      seed=msxrnd(seed):xdb2=seed mod 5:seed=msxrnd(seed):ydb2=seed mod 5
      end if
    end if
  msx1putsprite(24,xpb2/2,ypb2/2,((xdb2>2)*-8)*4,cb2)
  msx1putsprite(25,xpb2/2,ypb2/2+16,(d+((xdb2>2)*-8))*4,cb2)
  xpb2=xpb2+xdb2+510:ypb2=ypb2+ydb2+510

  cb3=1:if (abs(xq-((xpb3/2)band $FF))<16) and (abs(yq-4-((ypb3/2)band $FF))<20) then:
    cb3=8
    if (jst0 band 48)<>0 or (msxsnsmat(5) band 128)=0 then:
      seed=msxrnd(seed):xpb3=seed:seed=msxrnd(seed):ypb3=seed mod 384
      seed=msxrnd(seed):xdb3=seed mod 5:seed=msxrnd(seed):ydb3=seed mod 5
      end if
    end if
  msx1putsprite(26,xpb3/2,ypb3/2,((xdb3>2)*-8)*4,cb3)
  msx1putsprite(27,xpb3/2,ypb3/2+16,(d+((xdb3>2)*-8))*4,cb3)
  xpb3=xpb3+xdb3+510:ypb3=ypb3+ydb3+510

  asm
    halt
    end asm
  loop

8000:
'locate 0,22:print"   bill: 2/12   system: 9/1/0      level:  2    score:    153";:return
msx1ldirvm($1800+22*32,@text01,@text02-@text01)
return

for i=0 to 255
  msx1vpoke($1800+i,i)
  next

do:loop

9000:
for y=0 to 4
  ad=xl+(32*(yl+y))
  msx1ldirvm($1800+ad,@computers01+cp*35+y*7,7)
  next

9040:
for y=0 to 1
  ad=33+xl+(32*(yl+y))
  msx1ldirvm($1800+ad,@operatingsystems01+ep*6+y*3,3)
  next
return

do:loop

9100:
return

do:loop

computers01:
asm
defb $6e,$61,$61,$61,$6f,$70,$71,$80,$20,$20,$20,$8e,$8f,$90,$80,$20,$20,$20,$ae,$af,$b0,$ce,$cf,$d0,$d1,$d2,$d3,$e8,$ee,$ef,$f0,$f1,$f2,$f3,$20
defb $64,$65,$65,$65,$66,$20,$20,$a0,$20,$20,$20,$86,$87,$88,$a0,$20,$20,$20,$a6,$a7,$a8,$c4,$c5,$84,$85,$c6,$c7,$c8,$e4,$e5,$a4,$a5,$e6,$e7,$20
defb $75,$61,$61,$61,$76,$20,$20,$80,$20,$20,$20,$95,$96,$97,$80,$20,$20,$20,$b5,$b6,$b7,$91,$92,$93,$94,$72,$73,$74,$b1,$b2,$b2,$b4,$77,$78,$20
defb $60,$61,$61,$61,$62,$20,$20,$80,$20,$20,$20,$a9,$aa,$ab,$80,$20,$20,$20,$c9,$ca,$cb,$c0,$c1,$81,$82,$e9,$ea,$eb,$e0,$e1,$a1,$a2,$e2,$e3,$20
defb $67,$68,$68,$68,$6c,$20,$20,$8d,$20,$20,$20,$8c,$20,$20,$8d,$20,$20,$20,$ac,$ad,$20,$cc,$cd,$69,$69,$6a,$6b,$20,$ec,$ed,$6d,$89,$8a,$8b,$20
defb $60,$61,$61,$61,$62,$20,$20,$80,$20,$20,$20,$63,$83,$20,$80,$20,$20,$20,$63,$a3,$20,$c0,$c1,$81,$82,$c2,$c3,$20,$e0,$e1,$a1,$a2,$e2,$e3,$20
end asm

operatingsystems01:
asm
defb $1a,$1b,$1c,$1d,$1e,$1f
defb $7e,$7f,$9e,$9f,$be,$bf
defb $9b,$9c,$9d,$bb,$bc,$bd
defb $98,$99,$9a,$b8,$b9,$ba
defb $dd,$de,$df,$fd,$fe,$ff
defb $7b,$7c,$7d,$20,$79,$7a
defb $d4,$d5,$d6,$f4,$f5,$f6
defb $da,$db,$dc,$fa,$fb,$fc
defb $d7,$d8,$d9,$f7,$f8,$f9
end asm

text01:
asm
defb "   BILL: 2/12   SYSTEM: 9/1/0      LEVEL:  2    SCORE:    153"
end asm
text02:

textabout01:
asm
defb "XBill v2.1"
end asm
textabout02:
asm
defb "The Story:"
end asm
textabout03:
asm
defb "Yet aqain, the fate of the world rests in your hands!"
end asm
textabout04:
asm
defb "An evil computer cracker, known only by his handle 'Bill', has created the ultimate computer virus."
end asm
textabout05:
asm
defb "A virus so powerful that it has the power to transmute an ordinary computer into a toaster oven."
end asm
textabout06:
asm
defb "(oooh!)"
end asm
textabout07:
asm
defb "'Bill' has cloned himself into a billion-jillion micro Bills."
end asm
textabout08:
asm
defb "Their sole purpose is to deliver the nefarious virus, which has been cleverly diguised as a popular operating system."
end asm
textabout09:
asm
defb "As System Administrator/Exterminator, your job is to keep Bill from succeeding at his task."
end asm
textabout10:
asm
defb "[OK]"
end asm
textabout11:
asm
defb "XBill was created by Brian Wellington & Matias Duarte."
end asm
textabout12:

'01234567890123456789012345678901

texttitle01:
asm
defb "Original Version, 1994"
end asm
texttitle02:
asm
defb "Brian Wellington"
end asm
texttitle03:
asm
defb "Matias Duarte"
end asm
texttitle04:
asm
defb "MSX1 Version, 2018"
end asm
texttitle05:
asm
defb "Paulo Silva"
end asm
texttitle06:
asm
defb "Push any button"
end asm
texttitle07:
