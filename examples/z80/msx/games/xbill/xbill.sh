rm xbill.asm xbill.rom
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=xbill.tmr
rm msxromheader.bin
zxb.py xbill.bas --asm --org=$((0x4010))
zxb.py xbill.bas --org=$((0x4010))
cat xbill.bin >> xbill.tmr
filesize=$(stat -c%s "xbill.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> xbill.tmr
rm _dummybytes.bin xbill.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=xbill.tmr of=xbill.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=xbill.tmr of=xbill.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=xbill.tmr of=xbill.rom ; fi
rm xbill.tmr

mame expert20 -video soft -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 xbill.rom

#mame expert20 -video opengl -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 %ROM%</command>

#openmsx xbill.rom

# ###########################################
# openmsx -machine msx1 -carta xbill.rom
# openmsx -machine msx2 xbill.rom
# openmsx -machine msx2plus xbill.rom
# mess msx -cart1 xbill.rom
# mess msx2 -cart1 xbill.rom
# mess msx2p -cart1 xbill.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################

