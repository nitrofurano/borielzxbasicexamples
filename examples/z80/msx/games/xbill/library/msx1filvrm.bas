sub msx1filvrm(tvram as uinteger,tvl as uinteger,tlen as uinteger):
  asm
    ld h,(ix+5)
    ld l,(ix+4)

    ;????(ix+7)
    ld a,(ix+6)

    ld b,(ix+9)
    ld c,(ix+8)

    call $0056
    end asm
  end sub

'-------------------------------------------------------------------------------
'- FILVRM
'- Address  : #0056
'- Function : fill VRAM with value
'- Input    : A  - data byte
'-            BC - length of the area to be written
'-            HL - start address
'- Registers: AF, BC
