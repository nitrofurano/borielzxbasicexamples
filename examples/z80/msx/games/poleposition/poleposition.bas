#include "library/msx32kslotcompatibility.bas"
#include "library/msx1filvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msx1vpoke.bas"
#include "library/msxrnd.bas"
#include "library/msxwaitvbl.bas"
#include "library/msxsnsmat.bas"
#include "library/msxjoystick.bas"
#include "library/msxcolor.bas"
#include "library/msxscreen.bas"
#include "library/msxpalette.bas"
#include "library/_msx16x16sprites.bas"
#include "library/arcade_b.bas"
#include "library/polepositionrailstripes01b.bas"
#include "library/polepositionrailtilesbg01c.bas"
#include "library/clouds02.bas"
#include "library/mountains02.bas"
#include "library/circuit01.bas"
#include "library/msx1ldirvmstrp.bas"

'-------------------------------------------------------------------------------
'- including binaries... (check redundancy... :S )

goto bininc01end
ptrn01:
asm
  incbin "library/01wholek3r11patterns2.bin"
  end asm

mountainsandcloudstiles01:
asm
  incbin "library/mountainsandclouds_defw_ubyte.bin"
  end asm

bininc01end:

'-------------------------------------------------------------------------------
'- defining variables

dim eee as uinteger at $E010
dim ee0 as uinteger at $E012
dim ee1 as uinteger at $E014
dim ee2 as uinteger at $E016
dim ee3 as uinteger at $E018
dim xdr as uinteger at $E01A
dim trk as uinteger at $E01C
dim spd as  integer at $E01E
dim xrp as uinteger at $E020
dim trk2 as uinteger at $E022
dim tmr as uinteger at $E024
dim trk3 as uinteger at $E026
dim debug as ubyte at $E028
dim gest as ubyte at $E029
dim jst0 as ubyte at $E02A
dim jst1 as ubyte at $E02B
dim jst2 as ubyte at $E02C
dim jst3 as ubyte at $E02D
dim trkofx as ubyte at $E02E

debug=%00000000
'debug=%11111111

'-------------------------------------------------------------------------------

sub zprintdecimal(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/10000) mod 10)
  msx1vpoke(6144+tpos+1,48+int(tval/1000) mod 10)
  msx1vpoke(6144+tpos+2,48+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+3,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+4,48+tval mod 10)
  end sub

sub zprintdecimal3(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+1,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+2,48+tval mod 10)
  end sub

sub zprintdecimal2(tpos as uinteger,tval as uinteger):
  msx1vpoke(6144+tpos+0,48+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+1,48+tval mod 10)
  end sub

sub zprintdecimal5o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  msx1vpoke(6144+tpos+0,48+tofs+int(tval/10000) mod 10)
  msx1vpoke(6144+tpos+1,48+tofs+int(tval/1000) mod 10)
  msx1vpoke(6144+tpos+2,48+tofs+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+3,48+tofs+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+4,48+tofs+tval mod 10)
  end sub

sub zprintdecimal3o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  msx1vpoke(6144+tpos+0,48+tofs+int(tval/100) mod 10)
  msx1vpoke(6144+tpos+1,48+tofs+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+2,48+tofs+tval mod 10)
  end sub

sub zprintdecimal2o(tpos as uinteger,tval as uinteger, tofs as ubyte):
  msx1vpoke(6144+tpos+0,48+tofs+int(tval/10) mod 10)
  msx1vpoke(6144+tpos+1,48+tofs+tval mod 10)
  end sub

'-------------------------------------------------------------------------------
'- whatever...
msxcolor($6,$F,$1)
msxscreen(2,0,0,0)
msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?

'-------------------------------------------------------------------------------















'-------------------------------------------------------------------------------

for ee1=$0000 to $17FF step $0800
  msx1filvrm($0000+ee1,$F0,$0800)
  msx1filvrm($2000+ee1,$F0,$0800)
  next

msx1ldirvm($0100,@typeface01,512)
msx1filvrm($2100,$F5,512)

msx1ldirvm($0300,@typeface01+128,80)
msx1ldirvm($0350,@typeface01+code("T")*8-256,8)
msx1ldirvm($0358,@typeface01+code("O")*8-256,8)
msx1ldirvm($0360,@typeface01+code("P")*8-256,8)
msx1filvrm($2300,$95,128)
msx1ldirvm($0380,@typeface01+128,80)
msx1ldirvm($03D0,@typeface01+code("L")*8-256,8)
msx1ldirvm($03D8,@typeface01+code("A")*8-256,8)
msx1ldirvm($03E0,@typeface01+code("P")*8-256,8)
msx1ldirvm($03E8,@typeface01+       34*8-256,8)
msx1filvrm($2380,$35,128)
msx1ldirvm($0400,@typeface01+128,80)
msx1ldirvm($0450,@typeface01+code("T")*8-256,8)
msx1ldirvm($0458,@typeface01+code("I")*8-256,8)
msx1ldirvm($0460,@typeface01+code("M")*8-256,8)
msx1ldirvm($0468,@typeface01+code("E")*8-256,8)
msx1filvrm($2400,$A5,128)

msx1ldirvm($1080,@typeface01+code("L")*8-256,8)
msx1ldirvm($1088,@typeface01+code("O")*8-256,8)
msx1ldirvm($1090,@typeface01+code("H")*8-256,8)
msx1ldirvm($1098,@typeface01+code("I")*8-256,8)
msx1filvrm($3080,$FE,32)


'- clouds02, mountains02
msx1ldirvm($0700,@clouds02,256)
msx1ldirvm($0C00,@mountains02,768)
msx1ldirvm($2700,@clouds02attr,256)
msx1ldirvm($2C00,@mountains02attr,768)





'----------

for ee1=$0000 to $17FF step $0800
  for ee0=0 to 15
    msx1filvrm($2000+(ee0*8)+ee1,(ee0*$11) band $FF,8)
    next:next

msx1filvrm($1800,$0C,768)
msx1filvrm($1800,$05,384)

'----------

msx1ldirvm($1800,@dummyscoredisplay01,64)
'msx1vpoke($181C,0x22)

msx1ldirvm($1800+32*10,@mountainstiles01,64)

msx1ldirvm($1800+5*32+3,@cloudtiles01,5)
msx1ldirvm($1800+6*32+3,@cloudtiles01+5,5)

'----------

for ee0=0 to 31
  msx1vpoke($0F00+ee0,$FF)
  msx1vpoke($0F20+ee0,$FF)
  msx1vpoke($0F40+ee0,$FF)
  next
for ee0=0 to 63
  msx1vpoke($1600+ee0,$FF)
  msx1vpoke($1640+ee0,$FF)
  msx1vpoke($1680+ee0,$FF)
  next

'----------

if (debug band 1) <>0 then
'--------------
'!!!!
for ee0=0 to 767
  msx1vpoke($1800+ee0,ee0)
  next
'--------------

''-red/white
msx1ldirvmstrp($2F00,@railstripes01   ,32,$8C,$FC,32,trk2)
msx1ldirvmstrp($3600,@railstripes01+32,64,$8C,$FC,32,trk2)

'-yellow/white
msx1ldirvmstrp($2F20,@railstripes01   ,32,$AE,$FE,32,trk2)
msx1ldirvmstrp($3640,@railstripes01+32,64,$AE,$FE,32,trk2)

'-grey/white
msx1ldirvmstrp($2F40,@railstripes01   ,32,$EE,$FE,32,trk2)
msx1ldirvmstrp($3680,@railstripes01+32,64,$EE,$FE,32,trk2)

'--------------
'!!!!
do:loop
'--------------

end if

'---



msx1ldirvm($1800+32* 4,@mountainsandcloudstiles01+   0,32)
msx1ldirvm($1800+32* 5,@mountainsandcloudstiles01+ 256,32)
msx1ldirvm($1800+32* 6,@mountainsandcloudstiles01+ 512,32)

msx1ldirvm($1800+32* 9,@mountainsandcloudstiles01+ 768,32)
msx1ldirvm($1800+32*10,@mountainsandcloudstiles01+1024,32)
msx1ldirvm($1800+32*11,@mountainsandcloudstiles01+1280,32)



gest=0
tmr=0
xrp=118
xdr=6:trk=0:spd=0
trk3=0

loopunkn01:
do

'- 

jst0=msxjoystick(0)
jst1=msxjoystick(1)
jst3=jst0 band jst1

'- joystick
if (jst3 band 8)=0 or (msxsnsmat(8) band 128)=0 then: '-right
  xdr=xdr+1:end if
if (jst3 band 4)=0 or (msxsnsmat(8) band 16)=0 then: '-left
  xdr=xdr-1:end if

if (jst3 band 1)=0 or (msxsnsmat(8) band 32)=0 then: '- gear down
  gest=0:end if
if (jst3 band 2)=0 or (msxsnsmat(8) band 64)=0 then: '- gear up
  gest=1:end if

if (jst3 band 16)=0 or (msxsnsmat(8) band 1)=0 then: '-accel - space
  if gest=0 then:
    spd=spd+7
  else:
    spd=spd+3
    end if
  end if
if (jst3 band 32)=0 or (msxsnsmat(4) band 4)=0 then: '-brake - 'M'
  spd=spd-16:end if


spd=spd-1



'--------------

'xrp=xrp+(xdr-6)
xrp=xrp+((xdr*spd)/255)-((6*spd)/255)

'xrp=xrp+(((xdr-6)*spd)/255)
if xrp<94 then: xrp=94:end if
if xrp>162 then: xrp=162:end if

'if (msxsnsmat(8) band 1)=0 and shot=0 then:shot=1:end if

if xdr<1 then: xdr=1:end if
if xdr>11 then: xdr=11:end if
if spd<0 then: spd=0:end if
if (spd>255 and gest=1) then: spd=255:end if
if (spd>170 and gest=0) then: spd=170:end if

trk=trk+spd
trk3=trk3+(spd/3)

ee0=((xdr-1)*2)+(int(trk/256)band 1)
ee1=ee0*256
msx1ldirvm($1700,@ptrn01+ee1,256)
msx1ldirvm($3700,@ptrn01+6144+ee1,256)

zprintdecimal3( 32+26,spd)

'--------------

zprintdecimal(32+7,trk3)

zprintdecimal2o(26,int(tmr/60) mod 60,64)
zprintdecimal2o(29,tmr mod 60,64)

if (debug band 2)<>0 then
  zprintdecimal3o(160+28,xdr,64)
  zprintdecimal3o(192+28,xrp,64)
  zprintdecimal3o(224+28,(xrp*11)/12,64)
  end if

asm
  ;halt
  halt
  end asm


trkofx=peek(@circuit01+int(trk3/64) )

msx1ldirvm($1800+32* 4,@mountainsandcloudstiles01+   0+trkofx,32)
msx1ldirvm($1800+32* 5,@mountainsandcloudstiles01+ 256+trkofx,32)
msx1ldirvm($1800+32* 6,@mountainsandcloudstiles01+ 512+trkofx,32)

msx1ldirvm($1800+32* 9,@mountainsandcloudstiles01+ 768+trkofx,32)
msx1ldirvm($1800+32*10,@mountainsandcloudstiles01+1024+trkofx,32)
msx1ldirvm($1800+32*11,@mountainsandcloudstiles01+1280+trkofx,32)




msx1ldirvm($1800+12*32,@railtilesbg01+128*00+48                    ,32)
msx1ldirvm($1800+13*32,@railtilesbg01+128*01+48+(((xrp* 1)/11)- 11),32)
msx1ldirvm($1800+14*32,@railtilesbg01+128*02+48+(((xrp* 2)/11)- 23),32)
msx1ldirvm($1800+15*32,@railtilesbg01+128*03+48+(((xrp* 3)/11)- 34),32)
msx1ldirvm($1800+16*32,@railtilesbg01+128*04+48+(((xrp* 4)/11)- 46),32)
msx1ldirvm($1800+17*32,@railtilesbg01+128*05+48+(((xrp* 5)/11)- 58),32)
msx1ldirvm($1800+18*32,@railtilesbg01+128*06+48+(((xrp* 6)/11)- 69),32)
msx1ldirvm($1800+19*32,@railtilesbg01+128*07+48+(((xrp* 7)/11)- 81),32)
msx1ldirvm($1800+20*32,@railtilesbg01+128*08+48+(((xrp* 8)/11)- 93),32)
msx1ldirvm($1800+21*32,@railtilesbg01+128*09+48+(((xrp* 9)/11)-104),32)
msx1ldirvm($1800+22*32,@railtilesbg01+128*10+48+(((xrp*10)/11)-116),32)
msx1ldirvm($1800+23*32,@railtilesbg01+128*11+48+(  xrp        -128),32)

for ee0=0 to 3
  msx1ldirvm($1800+12+((19+ee0)*32),@cartiles01+(ee0*8),8)
  next

'--------------

msx1ldirvm($1800+23*32+29,@gearstate01+((gest band 1)*2),2)
trk2=trk/8

'--------------

''-red/white
msx1ldirvmstrp($2F00,@railstripes01   ,32,$8C,$FC,32,trk2)
msx1ldirvmstrp($3600,@railstripes01+32,64,$8C,$FC,32,trk2)

'-yellow/white
msx1ldirvmstrp($2F20,@railstripes01   ,32,$AE,$FE,32,trk2)
msx1ldirvmstrp($3640,@railstripes01+32,64,$AE,$FE,32,trk2)

'-grey/white
msx1ldirvmstrp($2F40,@railstripes01   ,32,$EE,$FE,32,trk2)
msx1ldirvmstrp($3680,@railstripes01+32,64,$EE,$FE,32,trk2)

tmr=tmr+1
loop

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------

cartiles01:
asm
  defb $E0,$E1,$E2,$E3,$E4,$E5,$E6,$E7
  defb $E8,$E9,$EA,$EB,$EC,$ED,$EE,$EF
  defb $F0,$F1,$F2,$F3,$F4,$F5,$F6,$F7
  defb $F8,$F9,$FA,$FB,$FC,$FD,$FE,$FF
  end asm

dummyscoredisplay01:
asm
  defb "   "
  defb $6A,$6B,$6C         ;"TOP"
  defb " "
  defb $61,$62,$60,$60,$60 ;"12000"
  defb "  "
  defb $8A,$8B,$8C,$8D     ;"TIME"
  defb "    "
  defb $7A,$7B,$7C         ;"LAP"
  defb " "
  defb $72,$74,$7D,$72,$78 ;"24 28"
  defb "  SCORE  3760   "
  defb $89,$85             ;"95"
  defb "   SPEED 189"
  defb $5D,$5E,$20
  end asm

'- apagar?
cloudtiles01:
asm
  ;defb $10,$11,$12,$13,$14
  ;defb $15,$16,$17,$18,$19
  end asm

'- apagar?
mountainstiles01:
asm
  ;defb  $F0,$F1,$F2,$F0, $F1,$F2,$F1,$F3, $F4,$F0,$F4,$F5, $F4,$F1,$F2,$F0, $F5,$F2,$F0,$F5, $F2,$F4,$F1,$F2, $F0,$F1,$F2,$F1, $F2,$F4,$F0,$F2
  ;defb  $FF, $FF,$FF,$FF,$FF, $F6,$F7,$F8,$F9, $F8,$FA,$FB,$FB, $F8,$FA,$FB,$FD, $F8,$FD,$FE,$FF, $FF,$FF,$FF,$FF, $FF,$FF,$FF,$FF, $FF,$FF,$FF
  ;defb $F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1,$F2,$F3,$F4,$F5,$F0,$F1
  ;defb $FF,$FF,$FF,$FF,$FF,$F6,$F7,$F8,$F9,$FA,$FB,$FC,$F8,$F9,$FA,$FB,$FC,$F8,$F9,$FD,$FE,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  end asm

gearstate01:
asm
  defb $10,$11,$12,$13
  end asm

'--------------------------------------





