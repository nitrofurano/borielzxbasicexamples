rm poleposition.asm poleposition.rom
zxb.py library/msxromheader.bas --org=0x0000
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=poleposition.tmr
rm msxromheader.bin
zxb.py poleposition.bas --asm --org=0x4010
zxb.py poleposition.bas --org=0x4010
cat poleposition.bin >> poleposition.tmr
filesize=$(stat -c%s "poleposition.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> poleposition.tmr
rm _dummybytes.bin poleposition.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=poleposition.tmr of=poleposition.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=poleposition.tmr of=poleposition.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=poleposition.tmr of=poleposition.rom ; fi
rm poleposition.tmr

mame fsa1wsx -video soft -w -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 poleposition.rom
#mame fsa1wsx -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 poleposition.rom

#openmsx poleposition.rom

# ###########################################
# openmsx -machine msx1 -carta poleposition.rom
# openmsx -machine msx2 poleposition.rom
# openmsx -machine msx2plus poleposition.rom
# mess msx -cart1 poleposition.rom
# mess msx2 -cart1 poleposition.rom
# mess msx2p -cart1 poleposition.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################

