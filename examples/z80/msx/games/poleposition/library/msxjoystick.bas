function fastcall msxjoystick(vjo1 as ubyte) as ubyte:
  asm
    and 1
    rl a
    rl a
    rl a
    rl a
    rl a
    rl a
    ld b,a
    ld a,15
    out ($a0),a
    ld a,b
    out ($a1),a
    ld a,14
    out ($a0),a
    in a,($a2)
    and $3F
    ;xor $3F
    end asm
  end function




'function msxjoystick(vjo1 as ubyte) as ubyte:
'  out $a0,15
'  out $a1,(vjo1 band $01)*64
'  out $a0,14
'  return (in($a2) band 63)bxor 63
'  end function
