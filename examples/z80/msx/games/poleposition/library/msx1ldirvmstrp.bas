sub msx1ldirvmstrp(tvram as uinteger,tram as uinteger,tlen as uinteger,tik as ubyte,tpp as ubyte,tfl as ubyte,tad as ubyte):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)

    ld a,e
    out ($99),a
    ld a,d
    or $40
    out ($99),a

    msx1ldirvmstrploop:
    ld a,(hl)
    add a,(ix+17)
    and (ix+15)
    jp z,msx1ldirvmstrpik
    ld a,(ix+13)
    jp msx1ldirvmstrpikend

    msx1ldirvmstrpik:
    ld a,(ix+11)

    msx1ldirvmstrpikend:
    out ($98),a
    inc hl
    dec bc
    ld a,b
    or c
    jp nz,msx1ldirvmstrploop

    end asm
  end sub







'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
