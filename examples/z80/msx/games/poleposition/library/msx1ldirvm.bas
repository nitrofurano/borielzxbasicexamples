sub msx1ldirvm(tvram as uinteger,tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)

    ld h,(ix+7)
    ld l,(ix+6)

    ld b,(ix+9)
    ld c,(ix+8)

    call $005C
    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : BC - blocklength
'-            DE - Start address of VRAM
'-            HL - Start address of memory
'- Registers: All
