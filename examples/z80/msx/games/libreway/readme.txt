Libreway
Paulo Silva, jan-jun’14 - gpl license

It’s just a Freeway clone, nothing more.

Keys are cursors and "wsad" keys. Joystick not supported yet. (i’m struggling on emulation issues)
During boot, the key “a” pressed will disable the alternative palette on msx2, 2+ and turbo-r. (the palette were “stolen” from Mojón Twins’ Subacuatic, ULAPlus palette)
No sound yet (i never tried sound on Boriel’s ZX-Basic Compiler, and i have no idea of how or what to implement).
There are still bugs related to colisions, and some code polishment.
During this game development Sega-SG1000 (and Master System by retrocompatibility) and ColecoVision are also being released. There are still some bugs to be fixed.


