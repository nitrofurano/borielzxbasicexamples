rm libreway.asm libreway.rom
zxb.py library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=libreway.tmr
rm msxromheader.bin
zxb.py libreway.bas --asm --org=$((0x4010))
zxb.py libreway.bas --org=$((0x4010))
cat libreway.bin >> libreway.tmr
filesize=$(stat -c%s "libreway.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> libreway.tmr
rm _dummybytes.bin libreway.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=libreway.tmr of=libreway.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=libreway.tmr of=libreway.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=libreway.tmr of=libreway.rom ; fi
rm libreway.tmr

mame expert20 -video soft -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -cart1 libreway.rom
#mame expert20 -video soft -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 libreway.rom
#openmsx libreway.rom

# ###########################################
# openmsx -machine msx1 -carta libreway.rom
# openmsx -machine msx2 libreway.rom
# openmsx -machine msx2plus libreway.rom
# mess msx -cart1 libreway.rom
# mess msx2 -cart1 libreway.rom
# mess msx2p -cart1 libreway.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################
