#include "library/msx1filvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxscreen.bas"
#include "library/msx1vpoke.bas"
#include "library/msx1putsprite.bas"
#include "library/msx1spritest.bas"
#include "library/msxwaitvbl.bas"
#include "library/msxsnsmat.bas"
#include "library/msxjoystick.bas"
#include "library/msxrnd.bas"
#include "library/librewayconvxwindows8x16font_charsetsprites.bas"
#include "library/msxpalette.bas"

'- check which variables are not being used
dim eee as uinteger at $E010
dim qq2 as uinteger at $E012
dim cpos as uinteger at $E014
dim eeseed as uinteger at $E016
dim ee1 as uinteger at $E018
dim ee2 as uinteger at $E01A
dim xpos1 as uinteger at $E020
dim ypos1 as uinteger at $E022
dim xsid1 as uinteger at $E024
dim xfrm1 as uinteger at $E026
dim xpos2 as uinteger at $E028
dim ypos2 as uinteger at $E02A
dim xsid2 as uinteger at $E02C
dim xfrm2 as uinteger at $E02E
dim spq1 as uinteger at $E030
dim spq2 as uinteger at $E032
dim gct1 as uinteger at $E034
dim gct2 as uinteger at $E036
dim ernd as uinteger at $E038
dim erndm as uinteger at $E03A
dim frmpg as uinteger at $E03C
dim edlim as uinteger at $E03E
dim frspl as uinteger at $E040
dim jst0 as ubyte at $E04A
dim jst1 as ubyte at $E04C
dim jst2 as ubyte at $E04E
dim jst3 as ubyte at $E048


edlim=16  '- goal limit
frspl=0   '- first play for title

'- area 10*4 (xpos,xspd, xdis) - $E080?

msxcolor(3,5,1)

msxscreen(1,0,0,0)
asm
  ld bc,$E201
  call $0047 ;WRTVDP - setting 16x16 sprites
  end asm

poke $f3db,0  '(disable keyboard click)

'-----
'- palette - "stealing" from Subacuatic (can i, Mojón Twins? ;) )
'- i need to simplify this!!!
'- key "a" pressed on booting for default palette, and checking msx version 

eee=0
'if (peek($002D)<>0) then:eee=1:end if
if ((msxsnsmat(2) band 64)<>0) then
  eee=1:end if
eee=0

if eee=1 then
  msxpalettehx( 0,$000):msxpalettehx( 1,$000):msxpalettehx( 2,$6D0):msxpalettehx( 3,$0BF)
  msxpalettehx( 4,$40F):msxpalettehx( 5,$04F):msxpalettehx( 6,$B20):msxpalettehx( 7,$26F)
  msxpalettehx( 8,$D40):msxpalettehx( 9,$B6A):msxpalettehx(10,$B90):msxpalettehx(11,$DB5)
  msxpalettehx(12,$460):msxpalettehx(13,$94A):msxpalettehx(14,$BBF):msxpalettehx(15,$FFF)
  end if

'- defw $000,$000,$6D0,$0BF,$40F,$04F,$B20,$26F
'- defw $D40,$B6A,$B90,$DB5,$460,$94A,$BBF,$FFF
'- k ,k ,g ,c ,bk,b ,rk,ck - 000,000,6D0,0BF,40F,04F,B20,26F
'- r ,m ,yk,y ,gk,mk,wk,w  - D40,B6A,B90,DB5,460,94A,BBF,FFF
'- k ,bk,rk,mk,gk,ck,yk,wk - 000,40F,B20,94A,460,26F,B90,BBF 
'- k ,b ,r ,m ,g ,c ,y ,w  - 000,04F,D40,B6A,6D0,0BF,DB5,FFF

'----
'- vpokes

for eee=0 to 2047
  qq2=peek(@charset01+eee)
  msx1vpoke(eee+$0000,qq2)
  msx1vpoke(eee+$2000,$1F)
  next

msx1vpoke($2000,$9E):msx1vpoke($2001,$FE):msx1vpoke($2002,$FE):msx1vpoke($2003,$FE)
msx1vpoke($2010,$6E):msx1vpoke($2011,$4E):msx1vpoke($2012,$1E):msx1vpoke($2013,$1E)
msx1vpoke($0009,$FF):msx1vpoke($0010,$FF):msx1vpoke($0012,$FF):msx1vpoke($0049,$FF)

for eee=6144 to 6144+767
  msx1vpoke(eee,0)
  next

for ee1=0 to 3
  for ee2=0 to 31
    msx1vpoke($1800+ee2+(ee1*64),ee2+(ee1*32))
    msx1vpoke($1820+ee2+(ee1*64),128+ee2+(ee1*32))
    next:next

for ee1=0 to 15
  for ee2=0 to 15
    msx1vpoke($3800+ee2+(ee1*32),peek(@sprites01+ee2+(ee1*16)))
    next:next

'----
'- subs

sub drawbg():
  '-- draw backgorund
  for eee=0 to 767
    msx1vpoke($1800+eee,0)
    next
  for ee2=0 to 31 step 2
    for ee1=2 to 10
      msx1vpoke($1801+ee2+(ee1*64),9)
      next:next
  for ee2=0 to 31 step 2
    msx1vpoke($1800+ee2+(1*64),1)
    msx1vpoke($1801+ee2+(1*64),1)
    msx1vpoke($1800+ee2+(11*64),1)
    msx1vpoke($1801+ee2+(11*64),1)
    msx1vpoke($1801+ee2+(6*64),2)
    next
  end sub

sub print2x1(txlc as uinteger, tylc as uinteger, tlad as uinteger, tlen as uinteger):
  dim tllp as uinteger at $E00E
  for tllp=0 to tlen-1
    msx1vpoke($1800+txlc+(tylc*32)+tllp,peek(tlad+tllp) )
    msx1vpoke($1820+txlc+(tylc*32)+tllp,peek(tlad+tllp)+128 )
    next
  end sub

'-------------------------------------------------------------------------------

looptitle01:

if frspl=0 then
  drawbg()
  frspl=1
  end if

print2x1(2,12,@text01,8)
print2x1(2,17,@text02,23)
print2x1(2,20,@text03,12)

loopkbd111a:

jst0=msxjoystick(0)
jst1=msxjoystick(1)
jst3=jst0 band jst1

eee=$FF
for ee1=0 to 8
  eee=eee band msxsnsmat(ee1)
  next
if eee=$FF and (jst3 band 63)=63 then
'if eee=$FF then:
  goto loopkbd111a:end if

'-------------------------------------------------------------------------------

drawbg()

'-------------------------------------------------------------------------------

xpos1=64:ypos1=174:xsid1=0:xfrm1=0
xpos2=192:ypos2=174:xsid2=4:xfrm2=0
cpos=12:ernd=0
gct1=0:gct2=0
frmpg=0

'poke $e080,128:poke $e081,1:poke $e082,45
'poke $e084, 28:poke $e085,2:poke $e086,76

for eee=0 to 39 step 4
  poke $e080+eee,ernd:ernd=msxrnd(ernd)
  poke $e081+eee,ernd band 7:ernd=msxrnd(ernd)
  poke $e082+eee,(ernd band 63)+16:ernd=msxrnd(ernd)
  poke $e083+eee,1+ernd mod 12:ernd=msxrnd(ernd)
  next

'-------------------------------------------------------------------------------

loopgameplay01:

'----
'- control

jst0=msxjoystick(0)
jst1=msxjoystick(1)

if (jst0 band 8)=0 or (msxsnsmat(8) band 128)=0 then '-right
  xpos1=xpos1+2:xfrm1=xfrm1+1:xsid1=0
  end if

if (jst0 band 4)=0 or (msxsnsmat(8) band 16)=0 then '-left
  xpos1=xpos1-2:xfrm1=xfrm1+1:xsid1=4
  end if

if (jst0 band 2)=0 or (msxsnsmat(8) band 64)=0 then '- down
  ypos1=ypos1+2:xfrm1=xfrm1+1
  end if

if (jst0 band 1)=0 or (msxsnsmat(8) band 32)=0 then '- up
  ypos1=ypos1-2:xfrm1=xfrm1+1
  end if

if (jst1 band 8)=0 or (msxsnsmat(3) band 2)=0 then '-right
'if (msxsnsmat(3) band 2)=0 then:  'd
  xpos2=xpos2+2:xfrm2=xfrm2+1:xsid2=0
  end if

if (jst1 band 4)=0 or (msxsnsmat(2) band 64)=0 then '-left
'if (msxsnsmat(2) band 64)=0 then:  'a
  xpos2=xpos2-2:xfrm2=xfrm2+1:xsid2=4
  end if

if (jst1 band 2)=0 or (msxsnsmat(5) band 1)=0 then '- down
'if (msxsnsmat(5) band 1)=0 then:  's
  ypos2=ypos2+2:xfrm2=xfrm2+1
  end if

if (jst1 band 1)=0 or (msxsnsmat(5) band 16)=0 then '- up
'if (msxsnsmat(5) band 16)=0 then:  'w
  ypos2=ypos2-2:xfrm2=xfrm2+1
  end if

'----
'- ???

if (msxsnsmat(8) band 1)=0 then
  cpos=(cpos+1)band 15
  end if

'----
'- ???

xfrm1=xfrm1 mod 4:xfrm2=xfrm2 mod 4
spq1=(xfrm1+(xsid1)):spq2=(xfrm2+(xsid2))

'----
'- sprites
msx1putsprite(0,xpos1,ypos1,spq1*4,6)
msx1putsprite(1,xpos2,ypos2,spq2*4,4)

for eee=0 to 4
  msx1putsprite(2+(eee*2),peek($e080+(eee*4)),15+(eee*16),(8+((frmpg/2) band 3))*4,peek($e083+(eee*4)))
  msx1putsprite(3+(eee*2),peek($e080+(eee*4))+peek($e082+(eee*4)),15+(eee*16),(8+((frmpg/2) band 3))*4,peek($e083+(eee*4)))
  poke $e080+(eee*4),(peek($e080+(eee*4))+1+peek($e081+(eee*4)))
  next

for eee=5 to 9
  msx1putsprite(2+(eee*2),peek($e080+(eee*4)),15+(eee*16),(12+((frmpg/2) band 3))*4,peek($e083+(eee*4)))
  msx1putsprite(3+(eee*2),peek($e080+(eee*4))+peek($e082+(eee*4)),15+(eee*16),(12+((frmpg/2) band 3))*4,peek($e083+(eee*4)))
  poke $e080+(eee*4),(peek($e080+(eee*4))+254- peek($e081+(eee*4)) )
  next

'----
'- test - collision with "enemies"
eee=int((ypos1+0)/16)
if (abs (peek($e080+(eee*4))-xpos1)<8) or (abs (peek($e080+(eee*4))+peek($e082+(eee*4))-xpos1)<8) then
  ypos1=ypos1+16
  end if

eee=int((ypos2+0)/16)
if (abs (peek($e080+(eee*4))-xpos2)<8) or (abs (peek($e080+(eee*4))+peek($e082+(eee*4))-xpos2)<8) then
  ypos2=ypos2+16
  end if

'----

ernd=msxrnd(ernd) bxor xpos1
ernd=msxrnd(ernd) bxor xpos2
erndm=(ernd band 3)*2

'----
'- goal scoring

if ypos1<1 then
  msx1vpoke($1800+gct1,128+erndm)
  msx1vpoke($1820+gct1,129+erndm)
  gct1=gct1+1
  ypos1=176
  end if

if ypos2<1 then
  msx1vpoke($1800+31-gct2,128+8+erndm)
  msx1vpoke($1820+31-gct2,128+9+erndm)
  gct2=gct2+1
  ypos2=176
  end if

'----
'- hitting other borders

if ypos1>176 then:ypos1=176:end if
if ypos2>176 then:ypos2=176:end if
if xpos1<2 then:xpos1=2:end if
if xpos2<2 then:xpos2=2:end if
if xpos1>246 then:xpos1=246:end if
if xpos2>246 then:xpos2=246:end if

'----
'- ???

ernd=msxrnd(ernd):eee=ernd:ernd=msxrnd(ernd)
if (ernd band 255)<64 then
  ernd=msxrnd(ernd):eee=ernd:ernd=msxrnd(ernd)
  poke $e081+(ernd band $3C),eee band 7
  end if

frmpg=frmpg+1
asm
  halt
  end asm

'----
'- goal limit

if (gct1<edlim) and (gct2<edlim) then
  goto loopgameplay01
  end if

'----
'- game over delay
for ee1=0 to 100
  asm
    halt
    end asm
  next

goto looptitle01

'-------------------------------------------------------------------------------

text01:
asm
  defb "Libreway"
  end asm
text02:
asm
  defb "Paulo Silva, jan-jun'14"
  end asm
text03:
asm
  defb "Push any key"
  end asm
text04:

'-palette01:
'-asm
'-  defw $000,$000,$6D0,$0BF,$40F,$04F,$B20,$26F
'-  defw $D40,$B6A,$B90,$DB5,$460,$94A,$BBF,$FFF
'-  end asm

'-------------------------------------------------------------------------------

