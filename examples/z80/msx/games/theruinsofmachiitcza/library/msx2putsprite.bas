sub msx2putsprite(tid as ubyte,tx as ubyte,ty as ubyte,tlay as ubyte,tcol as ubyte):
  '- needs msx2ldirvm before - uses $F3B9..$F3BC, dummy area from system variable area
  '- unsure if works on screen4, $1E00 instead of $1B00?
  poke $F3B9,ty:poke $F3BA,tx:poke $F3BB,tid:poke $F3BC,tcol
  msx2ldirvm(4,peek(uinteger,$F928)+(tlay*4),$F3B9)
  end sub

  ''- needs msxvpoke before
  'msx1vpoke($1B00+(tid*4)+0,ty)
  'msx1vpoke($1B00+(tid*4)+1,tx)
  'msx1vpoke($1B00+(tid*4)+2,tlay)
  'msx1vpoke($1B00+(tid*4)+3,tcol)


'-------------------------------------------------------------------------------
'- putsprite - putsprite(v_id,v_x,v_y,v_layer,v_colour) - i don't know how to put sprites in screens 5 to 12 
'- vpoke (0x1B00+(v_id*4)+0 ),v_y
'- vpoke (0x1B00+(v_id*4)+1 ),v_x
'- vpoke (0x1B00+(v_id*4)+2 ),v_layer
'- vpoke (0x1B00+(v_id*4)+3 ),v_colour
