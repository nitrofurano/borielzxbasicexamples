sub msx1putsprite(tlayer as ubyte,tx as ubyte,ty as ubyte,tcolour as ubyte,tid as ubyte):
  '- needs msx1vpoke before
  msx1vpoke($1B00+(tlayer*4)+0,ty)
  msx1vpoke($1B00+(tlayer*4)+1,tx)
  msx1vpoke($1B00+(tlayer*4)+2,tid)
  msx1vpoke($1B00+(tlayer*4)+3,tcolour)
  end sub

'-------------------------------------------------------------------------------
'- putsprite - putsprite(v_id,v_x,v_y,v_layer,v_colour) - i don't know how to put sprites in screens 5 to 12 
'- vpoke (0x1B00+(v_id*4)+0 ),v_y
'- vpoke (0x1B00+(v_id*4)+1 ),v_x
'- vpoke (0x1B00+(v_id*4)+2 ),v_layer
'- vpoke (0x1B00+(v_id*4)+3 ),v_colour


'- http://www.konamiman.com/msx/msx2th/th-2.txt
'- * PUT SPRITE <sprite plane number>[,{(X,Y)|STEP(X,Y)}[,<colour>[,<sprite pattern number>]]]
'- Statement: Displays the sprite pattern.
