asm
  call    0138h ;RSLREG 
  rrca 
  rrca 
  and     03h 
; Secondary Slot 
  ld      c,a 
  ld      hl,0FCC1h 
  add     a,l 
  ld      l,a 
  ld      a,(hl)
  and     80h 
  or      c 
  ld      c,a 
  inc     l 
  inc     l 
  inc     l 
  inc     l 
  ld      a,(hl) 
; Define slot ID
  and     0ch 
  or      c 
  ld      h,80h 
; Enable
  call    0024h ;ENASLT
  end asm

