
sub fastcall msxscreen(tsc as ubyte, tsp as ubyte, tcl as ubyte, tpr as ubyte):
  asm
    ;- screen mode with tsc value in a register
    call $005F    ;- CHGMOD

    ;- sprite mode with tsp value in a register
    pop af
    and 00000011b
    ld b,a
    ld a,($F3E0)
    and 11111100b
    or b
    ld b,a
    ld c,1
    call $0047    ;- WRTVDP
    ;- tsp - ld bc,0xE201:call WRTVDP - v_spritemode - ????

    ;pop af
    ;ld ($F3DB),a
    ;pop af
    ;ld ($F416),a
    end asm
  end sub

'- http://www.konamiman.com/msx/msx2th/th-2.txt
'- * SCREEN <screen mode>[,<sprite size>[,<key click switch>[,<cassette baud rate>[,<printer option>[,<interlace mode>]]]]]
'- Statement: Sets the screen mode and so on.


sub setscreen12():
  asm
    ld a,8
    call $005F
    ld b,$08
    ld c,25
    call $0047
    end asm
  end sub

sub setscreen10():
  asm
    ld a,8
    call $005F
    ld b,$18
    ld c,25
    call $0047
    end asm
  end sub




'  if tsc=10 or tsc=11 then:
'    asm
'      ld a,8
'      call $005F
'      ld b,$08
'      ld c,25
'      call $0047
'      end asm
'    end if

'  if tsc=12 then:
'    asm
'      ld a,8
'      call $005F
'      ld b,$18
'      ld c,25
'      call $0047
'      end asm
'    end if




'sub fastcall msxscreen(tsc as ubyte, tsp as ubyte, tcl as ubyte, tpr as ubyte):
'  asm
'    ld ($FCAF),a
'    call $005F
'    pop bc
'    ;- tsp - ld bc,0xE201:call WRTVDP - v_spritemode - ????
'    pop bc
'    ld a,b
'    ld ($F3DB),a
'    pop bc
'    ld a,b
'    ld ($F416),a
'    end asm
'  end sub


'   screen - screen v_screenmode,v_spritemode,v_click,v_printflag,v_?,v_?,v_?,v_?,v_?,v_? 
'   ld a,v_screenmode;call $005F     #- chgmod
'   #- ld bc,0xE201:call WRTVDP - v_spritemode - ????
'   ld ($F3DB),v_click    #- cliksw
'   ld ($F416),v_prtflg    #- printflag

'screen 10,12
'hack from NYYRIKKI at http://www.msx.org/forum/development/msx-development/accessing-msx2-and-gfx9000-screenmodes-assembly
'NCHGMOD:
'  CP 10
'  JP C,#5F
'  LD BC,#899
'  CP 12
'  JR Z,.SC12
'  RET NC
'  LD B,24
'.SC12:  PUSH BC
'  LD A,8
'  CALL #5F
'  POP BC
'  OUT (C),B
'  OUT (C),C
'  RET
