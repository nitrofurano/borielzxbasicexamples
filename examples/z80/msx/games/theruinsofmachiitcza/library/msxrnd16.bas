function msxrnd16() as uinteger:
  '- using RNDX (last random number) at 0xF857
  '- information found at http://www.msx.org/wiki/System_variables,_code_%26_hooks_in_RAM_after_boot from Nyyrikki/AxelF
  asm
    rand16:
    ld BC,($F857)
    ld HL,253
    xor A
    sbc HL,BC
    sbc A,B
    sbc HL,BC
    sbc A,B
    ld C,A
    sbc HL,BC
    jr nc,rand16end
    inc HL
    rand16end:
    ld ($F857),HL
    ret
    end asm
  return peek(uinteger,$F857)
  end function
