sub msx2putspritescr8(tid as ubyte,tx as ubyte,ty as ubyte,tlayer as ubyte,tcolour as ubyte):
  '- needs msx1vpoke before
  msx1vpoke($FA00+(tid*4)+0,ty)
  msx1vpoke($FA00+(tid*4)+1,tx)
  msx1vpoke($FA00+(tid*4)+2,tlayer)
  msx1vpoke($FA00+(tid*4)+3,tcolour)
  end sub

'-------------------------------------------------------------------------------
'- putsprite - putsprite(v_id,v_x,v_y,v_layer,v_colour) - i don't know how to put sprites in screens 5 to 12 
'- vpoke (0x1B00+(v_id*4)+0 ),v_y
'- vpoke (0x1B00+(v_id*4)+1 ),v_x
'- vpoke (0x1B00+(v_id*4)+2 ),v_layer
'- vpoke (0x1B00+(v_id*4)+3 ),v_colour
