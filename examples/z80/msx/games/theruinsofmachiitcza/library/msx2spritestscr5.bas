sub msx2spritestscr5(tdumpad as uinteger,tid as uinteger)
  '- needs msxldirvm
  msx2ldirvm(32,$7800+(tid*32),tdumpad)
  end sub

'-------------------------------------------------------------------------------
'   sprite$ - sprite$(v_spriteindex)=s_spritebitpmap$ 
'   ld hl,(s_spritebitpmap$)
'   ld de,0x3800+(v_spriteindex*32)
'   ld bc,32
'   call 0x005c   #- ldirvm
