#include "library/msx32kslotcompatibilitytemplate.bas"

goto jumpingdefbasmstuff
#include "library/msx1filvrm.bas"
#include "library/msx1ldirvm.bas"
#include "library/msxcolor.bas"
#include "library/msxpalette.bas"

'#include "library/msxscreen_ng.bas"
#include "library/msxscreen.bas"

#include "library/msxwaitvbl_ng.bas"
#include "library/msx1vpoke.bas"
#include "library/msx1vpeek.bas"
#include "library/msx1spritest.bas"
#include "library/msx1putsprite.bas"

#include "library/msxsnsmat.bas"

#include "library_custom/attributes01d.bas"
#include "library_custom/palette01d.bas"
#include "library_custom/TheRuinsOfMachiItcza_screen1e_charset.bas"
#include "library_custom/TheRuinsOfMachiItcza_screen1e_sprites.bas"
#include "library_custom/map_level0.bas"

#include "library_custom/map9x9.bas"

#include "library_custom/paleta9x9.bas"

#include "library_custom/correlation3x3.bas"

'#include "library_custom/correlation.bas"
'#include "library/msxpalette.bas"

'#include "library_custom/dummy16kb.bas"

jumpingdefbasmstuff:

'-------------------------------------------------------------------------------

'- global variables
dim eee as uinteger at $E000

dim ypos as integer at $E002
dim ygra as integer at $E004
dim xpos as ubyte at $E008
dim isjumping as ubyte at $E009
dim sprfram as ubyte at $E00A
dim sprdir as ubyte at $E00B
dim yposub as ubyte at $E00C

dim xmappos as integer at $E010
dim ymappos as integer at $E012


'- original variables, also global (probably):
'- (since the float precision seems 0.00, i will use "integer" between -327.67 and 327.68 ? )
'var vy=0; '- the velocity of the player vertically.
'var vx=0; '- the velocity horizontally
'var x = player._x; '- x is the player's x pos.
'var y = player._y; '- y is the player's y pos.
'var speed = .6; '- player's acceleration
'var fri = .3; '- the friction
'var gra = .12; '- the gravity
'var start_jump = 2.15; '- the max height of the jump
'var jump = start_jump; '- jump is the jump power
'var speed_limit = 1.4; '- if the player starts to fall to fast, this is the li
'var can_jump = false; '- a boolean to check if the player is allowed to jump

dim vy         as integer at $E030: vy=0
dim vx         as integer at $E032: vx=0
dim playerx    as integer at $E034
dim playery    as integer at $E036
dim x          as integer at $E038
dim y          as integer at $E03A
dim speed      as integer at $E03C: speed     =  60 '- 0.60*100
dim fri        as integer at $E03E: fri       =  30 '- 0.30*100
dim gra        as integer at $E040: gra       = 120 '- 1.20*100
dim startjump  as integer at $E042: startjump = 215 '- 2.15*100
dim jump       as integer at $E044:     jump=startjump
dim speedlimit as integer at $E046: speedlimit= 140 '- 1.40*100
dim canjump    as   ubyte at $E048: canjump   =   0 '- false




'- routine variables
dim txl as uinteger at $E020
dim tyl as uinteger at $E022
dim tcn as uinteger at $E024
dim tp1 as uinteger at $E026
dim tp2 as uinteger at $E028
dim txb as byte     at $E02A
dim tyb as byte     at $E02B
dim ttm as byte     at $E02C

'------------------


function palettefrommappos(txmp as byte, tymp as byte) as byte:
  txb=abs(txmp-4):tyb=abs(tymp-4)
  ttm=txb
  if tyb>ttm then:ttm=tyb:end if
  if ttm>3 then:ttm=3:end if
  return abs(ttm)
  end function


function patternpoint(txl as ubyte, tyl as ubyte)
  '-- needs msx1vpeek
  'return msx1vpeek($1800+((tyl>>3)*32)+(txl>>3))
  return msx1vpeek($1800+((int(tyl/8))*32)+(int(txl/8)))
  end function

sub putbytemap(txpos as ubyte,typos as ubyte,txsiz as ubyte,tysiz as ubyte,tmaplm as uinteger)
  tcn=0
  for tyl=typos to typos+tysiz-1
    for txl=txpos to txpos+txsiz-1
      tp1=peek(tmaplm+tcn)
      msx1vpoke($1800+(tyl*32)+txl,tp1)
      tcn=tcn+1
      next:next
  end sub

sub putbytemapvbl(txpos as ubyte,typos as ubyte,txsiz as ubyte,tysiz as ubyte,tmaplm as uinteger)
  tcn=0
  for tyl=typos to typos+tysiz-1
    for txl=txpos to txpos+txsiz-1
      tp1=peek(tmaplm+tcn)
      msx1vpoke($1800+(tyl*32)+txl,tp1)
      tcn=tcn+1
      asm
        halt
        halt
        halt
        end asm
      next:next
  end sub

sub putnibblemap(txpos as ubyte,typos as ubyte,txsiz as ubyte,tysiz as ubyte,tmaplm as uinteger,trel as uinteger)
  tcn=0
  for tyl=typos to typos+tysiz-1
    for txl=txpos to txpos+txsiz-1 step 2
      tp1=peek(trel+ ((peek(tmaplm+tcn)band 240)/16) )
      tp2=peek(trel+ (peek(tmaplm+tcn)band 15)       )
      msx1vpoke($1800+(tyl*32)+txl,tp1)
      msx1vpoke($1801+(tyl*32)+txl,tp2)
      tcn=tcn+1
      next:next
  end sub

'------------------

msxcolor(7,6,0)
msxscreen(1,0,0,0)

'- copy sprite patterns to vram
msx1ldirvm($0800,$3800,@sprites01)

'- copy palette, attributes and background(text) patterns to vram
'for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@palette+(eee*2))):next
msx1ldirvm(32,$2000,@attributes01)
msx1ldirvm(2048,$0000,@charset01)

resetingstart:
xpos=80:ypos=1280:ygra=0:isjumping=0:sprfram=0:sprdir=0:canjump=1:xmappos=4:ymappos=4

for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@paleta9x9+(palettefrommappos(xmappos,ymappos)*32)+(eee*2))):next

'-------------------------------------------------------------------------------

'- main loop

'- cls, and draw a room from the map
msx1filvrm($20,768,$1800):
'putnibblemap(4,3,24,18,@map33,@correlation3x3)
putnibblemap(4,3,24,18,@map9x9sq+( ((ymappos*9)+xmappos)*216 ),@correlation3x3)

'---

loopmain1:

if (msxsnsmat(8) band 128)=0 and ( (patternpoint(xpos+7,yposub+7)band $07)=0 or (patternpoint(xpos+7,yposub+2)band $07)=0 ) then:  '- move right
'if (msxsnsmat(8) band 128)=0 then:  '- move right
  sprdir=1
  if isjumping=0 then: sprfram=(sprfram+1)mod 6:end if
  xpos=xpos+1
  end if

if (msxsnsmat(8) band 16)=0 and ( (patternpoint(xpos+0,yposub+7)band $07)=0 or (patternpoint(xpos+0,yposub+2)band $07)=0 ) then:   '- move left
'if (msxsnsmat(8) band 16)=0 then:   '- move left
  sprdir=0
  xpos=xpos-1
  if isjumping=0 then: sprfram=(sprfram+1)mod 6:end if
  end if

if (msxsnsmat(8) band 32)=0 and canjump=1 then:   '- jump
  canjump=0
  isjumping=1
  ygra=-20
  end if

if (msxsnsmat(8) band 64)=0 then:  '- down - reseting when getting stuck... :D
  goto resetingstart
  end if



ypos=ypos+ygra

if xpos> 216 then:
  xmappos=(xmappos+1)mod 9
  for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@paleta9x9+(palettefrommappos(xmappos,ymappos)*32)+(eee*2))):next
  putnibblemap(4,3,24,18,@map9x9sq+( ((ymappos*9)+xmappos)*216 ),@correlation3x3)
  xpos=32
  end if

if xpos< 32 then:
  xmappos=(xmappos+8)mod 9
  for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@paleta9x9+(palettefrommappos(xmappos,ymappos)*32)+(eee*2))):next
  putnibblemap(4,3,24,18,@map9x9sq+( ((ymappos*9)+xmappos)*216 ),@correlation3x3)
  xpos=216
  end if

if ypos> 1600 then:
  ymappos=(ymappos+1)mod 9
  for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@paleta9x9+(palettefrommappos(xmappos,ymappos)*32)+(eee*2))):next
  putnibblemap(4,3,24,18,@map9x9sq+( ((ymappos*9)+xmappos)*216 ),@correlation3x3)
  ypos=240
  end if

if ypos< 240 then:
  ymappos=(ymappos+8)mod 9
  for eee=0 to 15:msxpalettehx(eee,peek(uinteger,@paleta9x9+(palettefrommappos(xmappos,ymappos)*32)+(eee*2))):next
  putnibblemap(4,3,24,18,@map9x9sq+( ((ymappos*9)+xmappos)*216 ),@correlation3x3)
  ypos=1600
  end if

yposub=int(ypos/10)

if (patternpoint(xpos+1,yposub+10) band $07)=0 and (patternpoint(xpos+6,yposub+10) band $07)=0 then
  ygra=ygra+1
  if ygra>14 then:ygra=14:end if
  isjumping=1
  canjump=0
else
  ygra=0
  isjumping=0
  canjump=1
  end if

'msx1putsprite(3,xpos,yposub,5,4)
msx1putsprite(3,xpos,yposub,5,4+sprfram+(6*sprdir))



'---
msxwaitvbl(2)
loopzz1:
  goto loopmain1

'-------------------------------------------------------------------------------

  goto loopzz1

'---
'for eee=32 to 215::msxwaitvbl(2):next
'if (msxsnsmat(8) band 64)=0 then:ypos=ypos+2:end if   '- move down
'xpos=128:ypos=8:ygra=0:isjumping=0:sprfram=0:sprdir=0

'---

'msx1filvrm($20,768,$1800):
'putnibblemap(4,3,24,18,@map33,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map34,@correlation3x3)
'for eee=32-8 to 215-8:msx1putsprite(3,255-eee,79,5,4+eee mod 6):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map35,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map43,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map44,@correlation3x3)
'for eee=32-8 to 215-8:msx1putsprite(3,255-eee,79,5,4+eee mod 6):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map45,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map53,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map54,@correlation3x3)
'for eee=32-8 to 215-8:msx1putsprite(3,255-eee,79,5,4+eee mod 6):msxwaitvbl(2):next
'msx1filvrm($20,768,$1800)
'putnibblemap(4,3,24,18,@map55,@correlation3x3)
'for eee=32 to 215:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next




''- show all background patterns available
'for eee=0 to 767:msx1vpoke(eee+6144,eee band $FF):next
''- moving sprites
'for eee=0 to 255:msx1putsprite(3,eee,79,5,15-(eee mod 6)):msxwaitvbl(2):next



'---

''- title
'msx1filvrm($20,768,$1800):
'putnibblemap(4,3,24,18,@mapatitle,@correlationtitle)
'putbytemap(7,5,18,3,@titletile01)
'putbytemap(4,20,2,1,@titletile02+3)
'putbytemap(25,20,3,1,@titletile02+0)
'putbytemap(13,3,6,1,@titletile02+5)
'putbytemap(8,12,4,5,@titletile03)
'putbytemap(19,12,4,5,@titletile03)
'for eee=0 to 200:msxwaitvbl(1):next

'---

''- writingtext
'msx1filvrm($20,768,$1800):
'putbytemapvbl(4,5,18,1,@presentationtext01)
'putbytemapvbl(4,7,25,1,@presentationtext01+18)
'putbytemapvbl(4,9,16,1,@presentationtext01+18+25)
'putbytemapvbl(4,11,25,1,@presentationtext01+18+25+16)
'putbytemapvbl(4,13,22,1,@presentationtext01+18+25+16+25)
'putbytemapvbl(4,15,21,1,@presentationtext01+18+25+16+25+22)
'putbytemapvbl(4,17,23,1,@presentationtext01+18+25+16+25+22+21)
'putbytemapvbl(4,19,25,1,@presentationtext01+18+25+16+25+22+21+23)
'for eee=0 to 200:msxwaitvbl(1):next

'---

''- writingtext
'msx1filvrm($20,768,$1800)
'putbytemapvbl(4,5,21,1,@presentationtext02)
'putbytemapvbl(4,7,24,1,@presentationtext02+21)
'putbytemapvbl(4,9,12,1,@presentationtext02+21+24)
'putbytemapvbl(4,13,10,1,@presentationtext02+21+24+12)
'for eee=0 to 200:msxwaitvbl(1):next

'---



'---


'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------




dump01:
asm
  defb 0c0h,080h,080h,082h,086h,082h,082h,007h
  defb 0c0h,080h,080h,082h,085h,081h,082h,007h
  defb 0c0h,080h,080h,086h,081h,082h,081h,006h
  defb 0c0h,080h,080h,081h,083h,085h,087h,001h
  defb 003h,001h,001h,041h,0c1h,041h,041h,0e0h
  defb 003h,001h,001h,041h,0a1h,021h,041h,0e0h
  defb 003h,001h,001h,0c1h,021h,041h,021h,0c0h
  defb 003h,001h,001h,021h,061h,0a1h,0e1h,020h
  end asm
dump01end:


mapatitle:
asm
  defb $0A,$00,$00,$A0,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$0A,$00,$00,$00
  defb $0A,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$FF,$FF,$F0,$00,$00,$AF,$FF,$FF,$00,$00
  defb $0C,$DA,$88,$88,$80,$00,$00,$08,$88,$88,$00,$00
  defb $0E,$00,$FF,$FF,$F0,$00,$00,$0F,$FF,$FF,$00,$00
  defb $0A,$00,$23,$47,$00,$0A,$00,$02,$34,$70,$0B,$00
  defb $00,$00,$FF,$FF,$0A,$00,$00,$0F,$FF,$F0,$00,$00
  defb $00,$00,$FF,$FF,$00,$00,$00,$0F,$FF,$F0,$00,$00
  defb $0B,$0A,$FF,$FF,$00,$00,$00,$0F,$FF,$F0,$00,$00
  defb $00,$00,$FF,$FF,$00,$0B,$00,$AF,$FF,$F0,$00,$06
  defb $60,$00,$23,$45,$60,$00,$A0,$02,$34,$56,$00,$61
  defb $16,$16,$FF,$FF,$F6,$00,$00,$0F,$FF,$FF,$06,$11
  defb $61,$66,$86,$88,$81,$60,$06,$18,$88,$88,$61,$16
  defb $55,$55,$5F,$6F,$F5,$55,$55,$1F,$1F,$F5,$55,$55
  defb $00,$16,$11,$16,$11,$11,$66,$11,$11,$66,$60,$00
  end asm

correlationtitle:
asm
  defb $20,$16,$CC,$CD
  defb $CE,$19,$14,$CF
  defb $BA,$45,$B2,$B1
  defb $03,$04,$05,$C0
  end asm

titletile01:
asm
  defb $60,$61,$62,$63,$64,$65,$66,$67,$68,$20,$69,$6A,$6B,$6C,$6D,$6E,$6F,$70
  defb $71,$72,$73,$74,$75,$76,$77,$78,$79,$20,$79,$7A,$7B,$7C,$7D,$7E,$7F,$80
  defb $81,$82,$83,$84,$85,$86,$87,$88,$89,$8A,$89,$8B,$8C,$8D,$8E,$8D,$81,$8F
  end asm
titletile02:
asm
  defb $A1,$A2,$A3,$A4,$A5,$A9,$AA,$AB,$AC,$AD,$AE
  end asm
titletile03:
asm
  defb $D0,$D1,$D2,$D3
  defb $D0,$D1,$D2,$D3
  defb $D0,$D1,$D2,$D3
  defb $D0,$D1,$D2,$D3
  defb $D4,$D5,$D6,$D7
  end asm

presentationtext01:
asm
  defb "MANY YEARS AGO THE"         ;18
  defb "ANCIENT MAKE CIVILIZATION"  ;25
  defb "BUILT THE TEMPLE"           ;16
  defb "MACHI ITCZA IN WHICH THEY"  ;25
  defb "HID 13 MAGIC KEYS THAT"     ;22
  defb "WHEN BROUGHT TOGETHER"      ;21
  defb "WOULD UNLOCK THE SECRET"    ;23
  defb "TO THE MEANING OF LIFE. "   ;25
  defb $5F
  end asm

presentationtext02:
asm
  defb "USE <ARROW> OR <WASD>"       ;21
  defb "KEYS TO MOVE AND <M> KEY"    ;24
  defb "TO VIEW MAP."                ;12
  defb "GOOD LUCK!"                  ;10
  end asm




correlationtitle2:
asm
  defb $20,$16,$99,$CD
  defb $CE,$19,$14,$B9
  defb $BA,$45,$B2,$B1
  defb $03,$04,$05,$C0
  end asm


