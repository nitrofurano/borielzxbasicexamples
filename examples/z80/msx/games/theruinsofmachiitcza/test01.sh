rm test01.asm test01.rom

echo;echo "rom header..."
zxb.py ./library/msxromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=msxromheader.bin of=test01.tmr
rm msxromheader.bin

echo;echo "converting to asm..."
zxb.py test01.bas --asm --org=$((0x4010))

echo;echo "compiling to binary..."
zxb.py test01.bas --org=$((0x4010))

echo;echo "rom file..."
cat test01.bin >> test01.tmr
filesize=$(stat -c%s "test01.tmr")
if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> test01.tmr
rm _dummybytes.bin test01.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=test01.tmr of=test01.rom ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=test01.tmr of=test01.rom ; fi
if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=test01.tmr of=test01.rom ; fi
rm test01.tmr

echo;echo "running emulator..."
openmsx test01.rom

# ###########################################
# openmsx -machine msx1 -carta test01.rom
# openmsx -machine msx2 test01.rom
# openmsx -machine msx2plus test01.rom
# mess msx -cart1 test01.rom
# mess msx2 -cart1 test01.rom
# mess msx2p -cart1 test01.rom
# openmsx -ext gfx9000
# openmsx -ext video9000
# ###########################################
