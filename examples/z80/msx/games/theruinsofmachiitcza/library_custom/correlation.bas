palette:
asm
  defw $000,$000,$3B0,$3B0
  defw $444,$07F,$630,$0FF
  defw $F00,$F0D,$FD0,$FF0
  defw $3B0,$904,$999,$FFF
  end asm

attributes:
asm
  defb $f0,$40,$d0,$c0,$f0,$f0,$f0,$f0
  defb $f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0
  defb $e0,$e0,$e0,$e0,$e0,$b0,$b0,$b0
  defb $50,$50,$80,$f0,$90,$70,$60,$a0
  end asm

correlation:
'-  0=$20,  1=$10,  2=$F0,  3=$DB
'-  4=$DE,  5=$18,  6=$11,  7=$80
'-  8=$81,  9=$82, 10=$F1, 11=$32
'- 12=$C0, 13=$F8, 14=$80, 15=$D8

asm
  defb $20,$10,$F0,$DB
  defb $DE,$18,$11,$80
  defb $81,$82,$F1,$32
  defb $C0,$F8,$80,$D8
  end asm


