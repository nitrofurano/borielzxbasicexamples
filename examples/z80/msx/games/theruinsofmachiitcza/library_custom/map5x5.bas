
'- correlation01: (used on 3x3) 4=17, 12=20
'- need to fix correlation on the other rooms

map5x5sq:

room22:
asm
  defb $11,$11,$16,$11,$16,$16,$11,$11,$11,$61,$66,$11
  defb $11,$11,$11,$11,$11,$11,$16,$16,$11,$11,$11,$11
  defb $88,$88,$87,$88,$88,$88,$88,$88,$88,$88,$88,$88
  defb $88,$78,$88,$88,$88,$88,$88,$88,$88,$88,$88,$88
  defb $00,$00,$00,$00,$00,$90,$90,$00,$90,$00,$90,$00
  defb $00,$00,$00,$00,$00,$90,$90,$00,$80,$00,$90,$00
  defb $00,$00,$00,$88,$00,$90,$90,$00,$90,$00,$80,$00
  defb $00,$00,$00,$88,$00,$90,$90,$00,$90,$00,$70,$00
  defb $00,$00,$00,$88,$00,$80,$90,$00,$90,$00,$80,$00
  defb $00,$00,$06,$86,$00,$90,$90,$00,$9A,$00,$90,$00
  defb $00,$00,$01,$88,$00,$90,$80,$00,$90,$00,$90,$00
  defb $00,$00,$11,$88,$00,$90,$00,$00,$80,$00,$90,$00
  defb $00,$06,$16,$78,$00,$90,$00,$00,$00,$00,$90,$F0
  defb $88,$81,$18,$88,$00,$88,$33,$33,$33,$38,$86,$88
  defb $88,$68,$78,$88,$00,$88,$88,$88,$88,$88,$71,$68
  defb $66,$11,$16,$98,$00,$88,$61,$11,$11,$11,$11,$55
  defb $11,$16,$11,$88,$00,$76,$11,$16,$11,$11,$16,$16
  defb $11,$16,$16,$88,$00,$88,$11,$11,$11,$16,$16,$16
  end asm

room23:
asm
  defb $61,$11,$61,$11,$16,$11,$16,$11,$11,$61,$11,$11
  defb $11,$16,$11,$11,$11,$11,$11,$16,$16,$11,$61,$11
  defb $88,$88,$88,$88,$87,$88,$88,$87,$88,$88,$88,$78
  defb $88,$87,$78,$88,$88,$88,$88,$88,$78,$88,$88,$88
  defb $00,$00,$00,$00,$00,$90,$09,$00,$99,$00,$00,$00
  defb $00,$00,$00,$00,$00,$90,$09,$00,$99,$00,$00,$00
  defb $00,$00,$00,$00,$00,$90,$09,$00,$90,$00,$00,$00
  defb $00,$00,$00,$00,$00,$90,$09,$00,$80,$00,$00,$00
  defb $00,$00,$00,$88,$00,$90,$09,$00,$70,$00,$00,$00
  defb $00,$00,$00,$88,$00,$80,$09,$00,$80,$00,$00,$00
  defb $00,$05,$55,$86,$00,$00,$09,$00,$00,$00,$00,$00
  defb $00,$06,$11,$88,$00,$00,$08,$00,$00,$00,$00,$00
  defb $00,$51,$61,$87,$00,$00,$00,$00,$00,$F0,$00,$00
  defb $88,$11,$16,$78,$88,$88,$88,$88,$78,$87,$88,$88
  defb $81,$68,$61,$18,$87,$88,$88,$88,$88,$88,$88,$88
  defb $55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55
  defb $16,$11,$11,$11,$16,$61,$16,$61,$11,$61,$66,$11
  defb $61,$11,$11,$16,$11,$11,$11,$11,$11,$11,$11,$61
  end asm

room24:
asm
  defb $11,$11,$11,$16,$16,$11,$16,$00,$00,$01,$11,$11
  defb $11,$61,$11,$11,$61,$11,$61,$00,$00,$06,$16,$11
  defb $88,$88,$88,$11,$11,$11,$10,$00,$00,$00,$11,$16
  defb $88,$87,$88,$16,$11,$61,$00,$00,$00,$00,$01,$11
  defb $09,$90,$08,$80,$09,$90,$00,$00,$00,$00,$01,$11
  defb $09,$90,$07,$80,$09,$90,$00,$00,$00,$00,$01,$16
  defb $09,$90,$08,$80,$09,$90,$00,$00,$00,$00,$01,$11
  defb $00,$90,$00,$00,$08,$80,$00,$00,$00,$00,$00,$16
  defb $00,$80,$00,$00,$00,$00,$00,$00,$00,$00,$00,$11
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$BB
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$55,$55
  defb $00,$00,$08,$80,$00,$00,$00,$00,$00,$00,$11,$61
  defb $00,$F0,$08,$85,$50,$00,$00,$00,$00,$00,$11,$11
  defb $88,$87,$88,$71,$10,$00,$00,$00,$00,$80,$01,$16
  defb $78,$88,$88,$81,$11,$00,$00,$00,$00,$88,$00,$11
  defb $55,$55,$55,$55,$55,$55,$50,$00,$55,$55,$55,$55
  defb $11,$16,$11,$61,$11,$11,$60,$00,$16,$11,$11,$61
  defb $16,$11,$11,$11,$16,$11,$00,$00,$11,$16,$11,$11
  end asm

room25:
asm
  defb $61,$11,$16,$16,$61,$16,$16,$11,$16,$11,$11,$11
  defb $11,$11,$11,$11,$16,$11,$11,$11,$11,$11,$61,$11
  defb $11,$16,$11,$11,$18,$88,$88,$88,$88,$88,$88,$88
  defb $11,$11,$11,$81,$86,$88,$88,$88,$88,$88,$88,$88
  defb $11,$61,$16,$11,$11,$09,$60,$00,$00,$90,$90,$00
  defb $11,$11,$11,$18,$11,$09,$00,$00,$00,$00,$90,$00
  defb $11,$61,$61,$88,$61,$09,$00,$00,$00,$00,$00,$00
  defb $11,$00,$11,$88,$10,$09,$00,$0D,$00,$00,$00,$00
  defb $16,$0C,$06,$88,$10,$00,$00,$00,$00,$00,$00,$00
  defb $BB,$00,$01,$88,$00,$00,$00,$55,$55,$00,$00,$00
  defb $16,$11,$61,$88,$00,$00,$00,$61,$66,$00,$00,$00
  defb $11,$16,$61,$88,$00,$00,$06,$61,$11,$60,$00,$00
  defb $16,$16,$16,$88,$00,$00,$01,$61,$16,$10,$60,$00
  defb $11,$16,$11,$18,$80,$00,$88,$86,$68,$88,$88,$88
  defb $16,$11,$11,$88,$80,$08,$88,$86,$86,$88,$88,$88
  defb $11,$11,$11,$11,$11,$00,$01,$61,$61,$66,$10,$06
  defb $11,$11,$61,$11,$11,$11,$00,$00,$00,$00,$00,$00
  defb $11,$11,$11,$66,$11,$61,$11,$61,$11,$11,$16,$16
  end asm

room26:
asm
  defb $61,$16,$16,$11,$11,$11,$16,$61,$11,$66,$11,$11
  defb $11,$66,$11,$11,$16,$11,$11,$11,$16,$11,$11,$61
  defb $88,$88,$88,$88,$87,$88,$88,$88,$88,$88,$88,$88
  defb $88,$88,$78,$88,$88,$88,$88,$88,$88,$88,$88,$88
  defb $00,$00,$00,$90,$00,$99,$00,$99,$00,$09,$00,$00
  defb $00,$00,$00,$90,$00,$99,$00,$99,$00,$09,$00,$00
  defb $00,$00,$00,$86,$00,$99,$00,$99,$00,$09,$00,$00
  defb $00,$00,$00,$88,$00,$98,$00,$99,$00,$07,$00,$00
  defb $00,$00,$00,$98,$00,$90,$00,$98,$00,$00,$00,$00
  defb $00,$00,$00,$96,$00,$90,$00,$90,$00,$00,$00,$00
  defb $00,$00,$00,$88,$00,$90,$00,$80,$00,$00,$00,$00
  defb $05,$55,$50,$88,$00,$80,$00,$00,$00,$00,$00,$00
  defb $06,$61,$60,$89,$00,$00,$F0,$00,$00,$00,$00,$00
  defb $88,$11,$88,$88,$88,$88,$88,$86,$71,$17,$88,$88
  defb $81,$86,$68,$88,$88,$78,$88,$88,$11,$81,$18,$88
  defb $16,$10,$00,$99,$00,$01,$61,$11,$11,$61,$11,$61
  defb $00,$00,$00,$99,$00,$00,$00,$16,$11,$11,$16,$11
  defb $16,$16,$61,$61,$66,$10,$00,$66,$11,$61,$11,$16
  end asm


'-------------------------------------------------------------------------------


room32:
asm
  defb $61,$11,$16,$88,$00,$88,$11,$11,$66,$11,$61,$11
  defb $11,$16,$11,$88,$00,$88,$61,$16,$11,$11,$11,$61
  defb $61,$11,$16,$88,$00,$88,$11,$11,$19,$00,$00,$00
  defb $11,$61,$61,$88,$00,$86,$16,$19,$09,$00,$00,$00
  defb $00,$00,$01,$10,$00,$11,$10,$09,$09,$00,$55,$55
  defb $0F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$11,$11
  defb $55,$55,$00,$00,$00,$00,$00,$00,$00,$11,$11,$61
  defb $61,$61,$11,$10,$00,$00,$00,$06,$11,$11,$61,$11
  defb $11,$11,$18,$18,$11,$00,$88,$88,$11,$11,$11,$11
  defb $16,$11,$81,$88,$81,$00,$88,$88,$11,$61,$11,$11
  defb $11,$61,$18,$61,$10,$00,$91,$88,$11,$61,$61,$16
  defb $11,$11,$88,$61,$00,$00,$90,$88,$11,$61,$11,$11
  defb $11,$11,$88,$10,$00,$00,$90,$88,$61,$11,$11,$11
  defb $16,$11,$88,$00,$00,$00,$00,$86,$11,$11,$11,$16
  defb $11,$11,$88,$AE,$00,$00,$0A,$81,$11,$16,$11,$11
  defb $11,$61,$88,$00,$00,$00,$00,$88,$11,$11,$11,$61
  defb $11,$11,$88,$A0,$00,$00,$EA,$88,$11,$61,$11,$11
  defb $11,$16,$88,$00,$00,$00,$00,$88,$11,$11,$61,$11
  end asm

room33:
asm
  defb $11,$11,$11,$61,$11,$11,$16,$11,$61,$11,$11,$11
  defb $11,$11,$11,$11,$66,$11,$11,$11,$11,$11,$16,$11
  defb $10,$00,$00,$00,$00,$00,$00,$01,$11,$11,$11,$61
  defb $00,$0F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $55,$55,$50,$00,$00,$00,$00,$00,$00,$66,$11,$16
  defb $11,$61,$10,$00,$00,$00,$00,$00,$01,$11,$11,$61
  defb $16,$11,$60,$00,$00,$00,$00,$00,$00,$01,$61,$11
  defb $11,$16,$00,$00,$00,$00,$00,$00,$00,$00,$09,$90
  defb $11,$10,$00,$00,$00,$00,$00,$00,$00,$00,$09,$90
  defb $11,$10,$00,$00,$00,$00,$00,$00,$00,$00,$09,$00
  defb $61,$00,$00,$00,$00,$00,$00,$00,$00,$00,$09,$00
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$08,$00
  defb $16,$00,$00,$00,$00,$00,$00,$55,$55,$00,$00,$00
  defb $11,$00,$00,$00,$00,$00,$00,$61,$61,$60,$00,$00
  defb $11,$50,$00,$00,$00,$00,$06,$11,$11,$16,$00,$00
  defb $16,$11,$00,$00,$00,$55,$55,$55,$55,$55,$55,$55
  defb $11,$11,$50,$00,$00,$61,$61,$11,$11,$11,$11,$16
  defb $11,$11,$60,$00,$00,$11,$11,$11,$61,$11,$61,$11
  end asm

room34:
asm
  defb $61,$61,$11,$11,$16,$16,$00,$00,$61,$16,$11,$11
  defb $11,$16,$11,$61,$61,$10,$00,$00,$11,$11,$11,$11
  defb $16,$00,$00,$00,$00,$00,$00,$61,$61,$61,$11,$61
  defb $00,$0F,$00,$61,$61,$61,$61,$11,$16,$11,$11,$11
  defb $11,$61,$11,$11,$11,$61,$11,$11,$11,$16,$16,$11
  defb $16,$11,$11,$11,$16,$11,$68,$16,$11,$11,$11,$11
  defb $61,$16,$68,$61,$0C,$06,$86,$61,$11,$00,$16,$16
  defb $00,$90,$78,$10,$0C,$00,$08,$61,$60,$00,$09,$11
  defb $00,$90,$81,$00,$00,$00,$00,$00,$00,$00,$09,$00
  defb $00,$90,$00,$00,$0E,$00,$00,$00,$80,$00,$08,$00
  defb $00,$80,$00,$00,$00,$00,$00,$55,$55,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$08,$66,$16,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$16,$11,$61,$00,$08,$00
  defb $00,$00,$00,$88,$33,$38,$86,$61,$10,$00,$87,$00
  defb $00,$F0,$78,$88,$88,$88,$78,$11,$60,$00,$88,$80
  defb $55,$55,$55,$55,$55,$55,$55,$00,$00,$00,$55,$55
  defb $61,$11,$11,$61,$11,$11,$60,$00,$00,$01,$61,$11
  defb $11,$11,$16,$11,$11,$11,$00,$01,$11,$61,$16,$61
  end asm

room35:
asm
  defb $11,$61,$11,$11,$61,$11,$61,$11,$16,$11,$11,$61
  defb $61,$11,$11,$60,$00,$11,$11,$61,$61,$61,$61,$16
  defb $11,$11,$00,$00,$00,$00,$00,$00,$00,$00,$16,$11
  defb $11,$11,$00,$00,$06,$11,$60,$00,$00,$00,$00,$00
  defb $16,$11,$00,$00,$11,$11,$16,$55,$55,$55,$55,$55
  defb $11,$10,$00,$00,$16,$11,$11,$11,$11,$11,$61,$11
  defb $11,$10,$02,$00,$09,$00,$11,$11,$61,$00,$00,$11
  defb $16,$00,$00,$00,$09,$00,$09,$01,$10,$00,$D0,$06
  defb $00,$00,$20,$00,$09,$00,$09,$00,$00,$00,$00,$11
  defb $00,$00,$00,$00,$09,$00,$09,$00,$00,$00,$61,$11
  defb $00,$00,$02,$00,$09,$00,$09,$00,$00,$00,$16,$11
  defb $00,$00,$00,$00,$08,$00,$08,$00,$00,$01,$11,$16
  defb $00,$00,$08,$80,$00,$00,$00,$00,$00,$86,$11,$11
  defb $00,$00,$78,$83,$33,$33,$33,$33,$88,$61,$11,$11
  defb $00,$07,$88,$78,$88,$88,$88,$88,$86,$86,$11,$11
  defb $55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$11,$61
  defb $11,$11,$61,$11,$11,$11,$11,$66,$11,$11,$11,$11
  defb $16,$11,$11,$11,$16,$11,$11,$11,$11,$61,$11,$16
  end asm

room36:
asm
  defb $61,$16,$11,$61,$16,$10,$00,$11,$61,$11,$16,$16
  defb $11,$66,$11,$11,$16,$10,$00,$11,$61,$61,$16,$16
  defb $00,$00,$00,$00,$00,$00,$00,$00,$06,$11,$61,$11
  defb $00,$00,$00,$00,$00,$00,$F0,$00,$00,$11,$61,$11
  defb $55,$00,$00,$05,$55,$55,$55,$50,$00,$00,$00,$00
  defb $61,$00,$00,$01,$11,$61,$11,$10,$00,$00,$00,$00
  defb $11,$60,$01,$11,$61,$11,$11,$61,$00,$00,$05,$55
  defb $16,$11,$11,$61,$11,$16,$11,$16,$11,$16,$16,$16
  defb $11,$16,$11,$10,$90,$00,$99,$11,$16,$11,$11,$11
  defb $16,$11,$16,$60,$90,$00,$99,$00,$11,$11,$61,$16
  defb $11,$11,$11,$00,$90,$D0,$99,$00,$00,$11,$11,$11
  defb $11,$11,$10,$00,$90,$00,$99,$00,$00,$01,$11,$11
  defb $11,$61,$00,$00,$88,$88,$88,$00,$00,$00,$11,$11
  defb $61,$11,$00,$00,$00,$00,$00,$00,$20,$00,$11,$61
  defb $11,$11,$00,$00,$00,$00,$00,$00,$00,$00,$11,$11
  defb $16,$10,$02,$00,$00,$00,$00,$00,$00,$55,$11,$11
  defb $11,$10,$00,$00,$00,$00,$00,$20,$00,$16,$11,$11
  defb $16,$10,$00,$00,$00,$20,$00,$00,$00,$61,$11,$61
  end asm


'-------------------------------------------------------------------------------


room42:
asm
  defb $16,$16,$88,$AA,$AA,$AA,$AA,$88,$11,$11,$16,$11
  defb $11,$11,$88,$00,$00,$00,$00,$78,$11,$61,$11,$11
  defb $11,$61,$87,$00,$00,$00,$00,$88,$11,$11,$11,$16
  defb $11,$11,$88,$0E,$00,$00,$00,$88,$16,$11,$71,$11
  defb $66,$11,$88,$00,$00,$00,$00,$81,$11,$11,$61,$11
  defb $11,$16,$88,$00,$00,$00,$00,$86,$11,$11,$11,$16
  defb $61,$11,$78,$00,$00,$00,$00,$81,$11,$16,$11,$11
  defb $11,$11,$88,$00,$00,$0E,$00,$88,$11,$11,$11,$11
  defb $11,$11,$88,$00,$00,$00,$00,$88,$11,$11,$11,$16
  defb $16,$11,$88,$00,$00,$00,$00,$81,$11,$11,$11,$16
  defb $11,$16,$88,$00,$00,$00,$00,$88,$11,$81,$11,$61
  defb $11,$61,$78,$00,$00,$00,$00,$81,$11,$18,$11,$11
  defb $11,$11,$88,$E0,$00,$00,$00,$88,$11,$69,$11,$11
  defb $61,$16,$78,$00,$00,$00,$00,$88,$16,$99,$01,$11
  defb $11,$61,$87,$00,$00,$00,$00,$88,$11,$69,$06,$11
  defb $11,$11,$88,$00,$00,$00,$00,$88,$11,$88,$61,$16
  defb $61,$61,$88,$00,$0E,$00,$00,$88,$18,$11,$11,$11
  defb $11,$11,$88,$AA,$AA,$AA,$AA,$88,$11,$11,$11,$16
  end asm

room43:
asm
  defb $11,$11,$10,$00,$00,$11,$61,$11,$11,$11,$11,$66
  defb $66,$11,$60,$00,$00,$11,$11,$16,$11,$11,$11,$11
  defb $11,$11,$00,$00,$00,$01,$16,$11,$11,$61,$61,$11
  defb $11,$11,$00,$00,$00,$06,$11,$11,$11,$16,$11,$11
  defb $16,$11,$00,$00,$00,$00,$11,$16,$11,$11,$16,$01
  defb $11,$10,$00,$00,$00,$00,$11,$11,$16,$10,$90,$00
  defb $61,$10,$CE,$00,$00,$0C,$11,$11,$11,$00,$90,$00
  defb $11,$10,$00,$00,$00,$00,$01,$16,$11,$00,$00,$00
  defb $11,$10,$00,$00,$00,$00,$06,$16,$10,$00,$55,$55
  defb $11,$10,$00,$00,$00,$00,$01,$61,$10,$00,$11,$11
  defb $16,$10,$00,$00,$00,$E0,$C1,$11,$10,$20,$11,$11
  defb $11,$60,$00,$00,$00,$00,$00,$16,$10,$00,$01,$61
  defb $11,$00,$00,$00,$00,$00,$00,$61,$16,$00,$00,$01
  defb $11,$00,$00,$00,$00,$00,$00,$01,$61,$60,$00,$00
  defb $11,$C0,$0E,$00,$00,$00,$00,$C6,$61,$66,$00,$00
  defb $61,$00,$00,$00,$00,$00,$00,$05,$55,$55,$55,$55
  defb $11,$00,$00,$00,$00,$00,$00,$00,$11,$11,$16,$11
  defb $61,$00,$00,$00,$00,$00,$00,$00,$01,$61,$11,$11
  end asm

room44:
asm
  defb $61,$61,$16,$11,$61,$61,$00,$06,$16,$16,$61,$61
  defb $11,$11,$00,$00,$01,$10,$00,$01,$61,$61,$11,$16
  defb $11,$60,$0D,$00,$00,$00,$00,$00,$16,$00,$06,$16
  defb $61,$11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$66
  defb $16,$66,$61,$00,$00,$00,$00,$00,$00,$00,$00,$16
  defb $00,$01,$10,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$20,$00,$00,$00,$00,$00,$80,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$07,$80,$00
  defb $55,$55,$55,$55,$55,$55,$00,$00,$05,$55,$55,$55
  defb $11,$16,$11,$61,$16,$16,$00,$00,$66,$16,$16,$11
  defb $61,$11,$16,$61,$61,$61,$60,$00,$66,$16,$16,$16
  defb $11,$16,$11,$00,$01,$11,$60,$00,$11,$61,$16,$11
  defb $11,$00,$00,$00,$00,$16,$10,$00,$00,$00,$01,$16
  defb $00,$00,$00,$00,$00,$11,$10,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$06,$11,$60,$00,$00,$80,$00,$00
  defb $55,$55,$55,$55,$51,$66,$10,$00,$55,$55,$55,$55
  defb $61,$66,$16,$11,$61,$61,$00,$00,$61,$16,$66,$11
  defb $11,$11,$61,$16,$11,$16,$00,$06,$11,$61,$11,$16
  end asm

room45:
asm
  defb $61,$11,$16,$66,$16,$11,$11,$61,$11,$16,$61,$61
  defb $16,$61,$11,$11,$16,$61,$61,$11,$16,$11,$61,$11
  defb $61,$11,$60,$00,$11,$11,$11,$11,$16,$11,$16,$61
  defb $11,$11,$00,$00,$00,$09,$00,$09,$11,$11,$11,$61
  defb $16,$60,$00,$00,$00,$09,$0D,$09,$01,$16,$11,$66
  defb $00,$00,$00,$80,$00,$09,$00,$09,$00,$01,$11,$11
  defb $00,$00,$00,$88,$00,$08,$88,$88,$00,$01,$16,$11
  defb $00,$00,$00,$78,$00,$09,$00,$09,$00,$06,$16,$11
  defb $55,$55,$55,$55,$50,$09,$00,$09,$00,$00,$61,$61
  defb $61,$61,$61,$11,$60,$00,$00,$08,$00,$00,$86,$81
  defb $11,$11,$66,$61,$66,$00,$00,$09,$00,$00,$88,$61
  defb $16,$11,$11,$11,$16,$10,$00,$09,$00,$00,$48,$11
  defb $11,$61,$11,$66,$11,$60,$00,$08,$00,$00,$81,$81
  defb $00,$00,$01,$11,$11,$10,$00,$00,$00,$00,$88,$11
  defb $00,$00,$00,$01,$11,$61,$60,$00,$00,$00,$86,$81
  defb $55,$55,$55,$00,$55,$55,$55,$50,$00,$00,$01,$11
  defb $16,$11,$66,$00,$11,$66,$11,$10,$00,$00,$01,$61
  defb $61,$61,$10,$06,$66,$11,$66,$10,$02,$20,$06,$16
  end asm

room46:
asm
  defb $61,$10,$00,$00,$00,$00,$00,$00,$00,$11,$61,$11
  defb $11,$10,$00,$00,$00,$02,$00,$00,$00,$61,$11,$11
  defb $16,$10,$00,$00,$00,$00,$00,$02,$00,$01,$11,$61
  defb $16,$10,$00,$00,$00,$00,$00,$00,$00,$01,$11,$11
  defb $61,$10,$00,$00,$00,$00,$00,$00,$20,$06,$16,$11
  defb $11,$60,$00,$00,$20,$00,$00,$00,$00,$00,$11,$16
  defb $16,$00,$00,$00,$00,$00,$00,$20,$00,$00,$11,$11
  defb $10,$00,$00,$02,$00,$00,$00,$00,$00,$00,$01,$11
  defb $60,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$61
  defb $10,$00,$20,$00,$00,$00,$00,$00,$20,$00,$01,$11
  defb $60,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$61
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$55,$55
  defb $61,$00,$00,$00,$02,$00,$00,$00,$00,$00,$16,$16
  defb $11,$00,$02,$00,$00,$00,$00,$00,$02,$00,$11,$11
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$61,$16
  defb $16,$50,$00,$00,$00,$00,$00,$00,$00,$05,$11,$61
  defb $11,$10,$00,$00,$00,$00,$00,$00,$20,$06,$11,$11
  defb $11,$60,$00,$00,$00,$00,$20,$00,$00,$01,$61,$61
  end asm


'-------------------------------------------------------------------------------


room52:
asm
  defb $11,$11,$88,$00,$00,$00,$00,$88,$16,$11,$61,$61
  defb $16,$61,$88,$00,$00,$00,$00,$78,$11,$11,$11,$11
  defb $11,$11,$87,$00,$00,$00,$00,$88,$11,$61,$11,$16
  defb $16,$11,$88,$00,$00,$00,$00,$88,$11,$11,$11,$11
  defb $11,$16,$88,$00,$00,$00,$00,$61,$11,$11,$61,$11
  defb $11,$11,$88,$00,$0D,$00,$00,$11,$61,$11,$11,$61
  defb $16,$11,$78,$00,$00,$00,$00,$86,$11,$11,$11,$11
  defb $11,$16,$88,$00,$22,$20,$00,$88,$11,$11,$16,$11
  defb $11,$11,$88,$00,$0A,$00,$00,$88,$16,$11,$11,$16
  defb $16,$11,$18,$68,$88,$88,$00,$86,$16,$11,$11,$11
  defb $11,$11,$81,$88,$88,$88,$00,$88,$11,$16,$16,$11
  defb $11,$61,$61,$19,$16,$16,$00,$99,$00,$00,$11,$61
  defb $00,$06,$11,$16,$10,$00,$00,$90,$00,$00,$00,$00
  defb $00,$00,$00,$09,$00,$00,$00,$88,$00,$00,$00,$00
  defb $00,$00,$00,$09,$0F,$07,$08,$88,$00,$00,$00,$00
  defb $55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55
  defb $11,$61,$11,$11,$61,$16,$11,$16,$66,$11,$16,$11
  defb $11,$11,$61,$11,$11,$11,$11,$66,$16,$11,$11,$16
  end asm

room53:
asm
  defb $11,$00,$00,$00,$00,$00,$00,$00,$01,$11,$11,$61
  defb $61,$00,$00,$00,$00,$00,$00,$00,$00,$51,$61,$11
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$11,$11
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$16
  defb $16,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$11
  defb $16,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$16
  defb $11,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$16
  defb $66,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$11
  defb $15,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $60,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $60,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $10,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$78,$80,$78,$00,$00,$20,$00,$00,$00
  defb $00,$00,$08,$87,$80,$87,$88,$00,$00,$00,$70,$00
  defb $55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55
  defb $11,$11,$61,$16,$11,$11,$11,$11,$61,$16,$11,$61
  defb $11,$11,$11,$11,$11,$16,$11,$66,$11,$16,$61,$11
  end asm

room54:
asm
  defb $11,$16,$11,$11,$11,$60,$00,$01,$16,$61,$16,$11
  defb $11,$61,$11,$61,$11,$10,$00,$01,$11,$11,$16,$11
  defb $11,$11,$61,$11,$61,$10,$00,$01,$66,$11,$11,$11
  defb $11,$11,$11,$11,$11,$00,$00,$00,$00,$00,$11,$16
  defb $16,$11,$00,$99,$00,$00,$00,$61,$16,$00,$00,$00
  defb $11,$10,$00,$90,$00,$00,$01,$11,$11,$11,$61,$16
  defb $11,$10,$00,$90,$00,$00,$16,$11,$16,$16,$11,$11
  defb $61,$00,$00,$00,$00,$00,$01,$16,$11,$11,$61,$61
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01,$11
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$01
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$0F,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$05,$55,$55,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$01,$61,$11,$00,$02,$00,$00,$00,$00,$00
  defb $00,$07,$00,$16,$16,$60,$00,$00,$00,$00,$00,$00
  defb $55,$55,$55,$55,$55,$55,$00,$05,$55,$55,$55,$55
  defb $11,$11,$61,$11,$11,$11,$00,$01,$16,$11,$11,$61
  defb $11,$61,$11,$61,$11,$16,$00,$00,$11,$11,$61,$11
  end asm

room55:
asm
  defb $11,$61,$10,$01,$61,$11,$16,$10,$00,$00,$01,$61
  defb $16,$11,$10,$01,$16,$16,$11,$10,$00,$20,$01,$11
  defb $11,$11,$00,$01,$11,$11,$10,$00,$00,$00,$01,$16
  defb $11,$00,$00,$61,$16,$11,$10,$00,$02,$00,$01,$11
  defb $00,$08,$01,$11,$11,$61,$00,$00,$00,$00,$00,$11
  defb $16,$11,$11,$61,$61,$16,$00,$00,$00,$20,$00,$11
  defb $61,$16,$11,$11,$10,$90,$00,$00,$00,$00,$00,$61
  defb $11,$61,$11,$16,$00,$90,$00,$00,$20,$00,$00,$11
  defb $11,$11,$61,$00,$00,$80,$00,$00,$00,$00,$00,$61
  defb $16,$10,$00,$00,$00,$00,$00,$20,$00,$00,$00,$11
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$06
  defb $00,$00,$00,$00,$00,$F0,$00,$02,$00,$00,$00,$01
  defb $00,$00,$00,$00,$06,$16,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$16,$18,$80,$00,$00,$78,$00,$00
  defb $00,$00,$00,$06,$11,$76,$80,$00,$08,$78,$87,$00
  defb $55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55,$55
  defb $11,$11,$11,$61,$11,$11,$11,$11,$11,$11,$11,$11
  defb $11,$11,$61,$11,$11,$11,$11,$11,$61,$11,$11,$11
  end asm

room56:
asm
  defb $11,$60,$00,$00,$00,$00,$00,$00,$00,$01,$11,$11
  defb $61,$10,$00,$00,$00,$02,$00,$00,$00,$01,$11,$11
  defb $11,$10,$00,$02,$00,$00,$00,$00,$00,$00,$61,$11
  defb $16,$60,$00,$00,$00,$00,$00,$00,$00,$00,$11,$61
  defb $11,$65,$00,$00,$00,$20,$00,$00,$00,$00,$01,$11
  defb $16,$16,$00,$00,$00,$00,$00,$00,$00,$00,$01,$61
  defb $11,$11,$00,$00,$00,$00,$02,$00,$00,$00,$01,$11
  defb $61,$66,$00,$00,$00,$00,$00,$00,$02,$00,$01,$11
  defb $11,$10,$00,$00,$00,$00,$00,$00,$00,$00,$00,$71
  defb $11,$10,$00,$00,$00,$00,$00,$20,$00,$00,$00,$16
  defb $11,$60,$00,$00,$00,$0F,$00,$00,$00,$00,$00,$00
  defb $60,$00,$00,$00,$00,$08,$80,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$07,$80,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$88,$78,$00,$00,$88,$00,$00
  defb $00,$00,$00,$00,$87,$88,$88,$00,$80,$78,$00,$00
  defb $55,$55,$50,$00,$55,$55,$55,$55,$55,$55,$55,$55
  defb $11,$11,$10,$00,$11,$11,$11,$16,$11,$11,$11,$11
  defb $11,$11,$10,$00,$11,$11,$11,$11,$11,$11,$11,$11
  end asm


'-------------------------------------------------------------------------------


room62:
asm
  defb $11,$61,$11,$16,$16,$11,$11,$61,$11,$11,$11,$61
  defb $11,$16,$11,$61,$11,$11,$00,$01,$61,$61,$11,$11
  defb $11,$11,$11,$11,$11,$10,$00,$06,$11,$11,$11,$11
  defb $11,$16,$61,$11,$11,$16,$00,$61,$11,$11,$11,$16
  defb $11,$16,$11,$11,$61,$11,$11,$61,$11,$16,$11,$11
  defb $11,$11,$11,$11,$11,$11,$11,$11,$11,$11,$11,$16
  defb $87,$88,$87,$86,$11,$61,$87,$88,$88,$88,$78,$88
  defb $88,$88,$87,$88,$88,$78,$88,$88,$87,$88,$88,$88
  defb $09,$90,$00,$00,$00,$00,$E0,$00,$00,$00,$00,$90
  defb $09,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$90
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$AA,$00,$EE,$00,$00,$00,$E0,$EA,$A0,$00
  defb $00,$00,$00,$20,$00,$20,$00,$20,$00,$20,$00,$00
  defb $0F,$00,$00,$00,$A0,$00,$A0,$00,$A0,$00,$00,$00
  defb $88,$88,$33,$33,$33,$33,$33,$33,$33,$33,$38,$88
  defb $88,$88,$88,$88,$88,$88,$78,$88,$88,$88,$88,$88
  defb $16,$11,$16,$11,$11,$16,$11,$11,$66,$11,$16,$11
  defb $11,$16,$11,$11,$11,$61,$11,$11,$11,$11,$11,$11
  end asm

room63:
asm
  defb $11,$11,$11,$11,$11,$61,$11,$11,$11,$11,$11,$11
  defb $11,$11,$11,$11,$11,$11,$11,$16,$11,$61,$66,$11
  defb $11,$11,$11,$11,$11,$11,$11,$11,$11,$11,$11,$11
  defb $11,$11,$81,$11,$11,$11,$11,$16,$11,$16,$11,$00
  defb $61,$11,$11,$16,$68,$11,$11,$11,$11,$11,$10,$00
  defb $16,$11,$11,$11,$81,$11,$61,$11,$61,$11,$00,$00
  defb $88,$88,$88,$88,$87,$88,$11,$11,$80,$00,$00,$00
  defb $88,$88,$88,$78,$88,$88,$88,$18,$80,$00,$00,$00
  defb $00,$00,$00,$0A,$0A,$00,$01,$68,$80,$00,$00,$00
  defb $00,$00,$00,$0A,$0A,$00,$00,$08,$70,$00,$00,$00
  defb $00,$00,$00,$0E,$00,$00,$00,$00,$00,$00,$05,$55
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$20,$01,$11
  defb $00,$00,$00,$00,$00,$00,$00,$08,$80,$00,$00,$11
  defb $00,$0F,$00,$00,$0E,$00,$00,$68,$71,$00,$00,$11
  defb $88,$88,$88,$83,$33,$88,$88,$88,$81,$61,$00,$11
  defb $88,$88,$88,$87,$88,$87,$11,$87,$86,$11,$10,$01
  defb $11,$11,$11,$16,$11,$16,$16,$55,$55,$55,$55,$55
  defb $11,$11,$11,$61,$61,$11,$11,$16,$11,$16,$11,$11
  end asm

room64:
asm
  defb $11,$11,$16,$11,$11,$11,$00,$00,$11,$61,$11,$16
  defb $11,$11,$11,$11,$61,$11,$00,$00,$06,$11,$16,$11
  defb $11,$66,$11,$11,$11,$61,$00,$00,$00,$00,$00,$00
  defb $00,$11,$66,$16,$11,$11,$50,$00,$00,$08,$00,$00
  defb $00,$00,$00,$00,$00,$11,$60,$00,$00,$08,$70,$80
  defb $00,$00,$0D,$00,$00,$61,$60,$00,$00,$01,$11,$11
  defb $00,$00,$00,$00,$00,$11,$15,$00,$00,$01,$16,$16
  defb $00,$00,$55,$50,$00,$61,$61,$00,$00,$06,$11,$61
  defb $00,$00,$61,$60,$00,$01,$16,$10,$00,$00,$16,$11
  defb $00,$80,$16,$10,$00,$00,$01,$11,$00,$00,$00,$01
  defb $55,$55,$55,$50,$00,$00,$00,$11,$00,$00,$00,$00
  defb $11,$16,$61,$60,$00,$00,$00,$16,$00,$00,$00,$00
  defb $16,$11,$11,$16,$00,$F0,$00,$01,$00,$00,$00,$00
  defb $11,$11,$16,$15,$55,$55,$00,$00,$00,$55,$55,$55
  defb $11,$61,$11,$16,$16,$16,$00,$00,$00,$61,$16,$16
  defb $11,$11,$66,$16,$11,$60,$00,$00,$00,$11,$61,$11
  defb $55,$55,$55,$55,$55,$55,$55,$00,$00,$01,$16,$16
  defb $61,$11,$61,$61,$11,$11,$16,$00,$00,$01,$11,$61
  end asm

room65:
asm
  defb $11,$11,$61,$61,$11,$61,$11,$16,$11,$16,$16,$61
  defb $11,$11,$11,$11,$11,$61,$11,$11,$11,$00,$00,$11
  defb $00,$00,$00,$01,$11,$11,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$11,$11,$11,$61
  defb $00,$08,$08,$80,$00,$00,$01,$61,$11,$61,$11,$11
  defb $11,$11,$11,$11,$16,$11,$11,$11,$11,$11,$16,$61
  defb $11,$11,$11,$68,$68,$88,$16,$11,$86,$88,$88,$81
  defb $11,$66,$61,$78,$88,$88,$88,$78,$18,$78,$88,$88
  defb $61,$11,$11,$88,$09,$90,$00,$16,$10,$00,$00,$00
  defb $11,$60,$00,$88,$09,$00,$00,$01,$60,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$E0,$00,$00,$00
  defb $00,$0F,$00,$88,$00,$00,$00,$0E,$00,$00,$00,$00
  defb $55,$55,$55,$88,$00,$00,$00,$00,$00,$00,$00,$00
  defb $11,$61,$11,$78,$87,$88,$88,$83,$38,$88,$88,$88
  defb $11,$11,$61,$88,$88,$87,$87,$68,$88,$88,$18,$78
  defb $16,$11,$55,$55,$51,$61,$11,$16,$11,$11,$11,$16
  defb $11,$16,$11,$61,$11,$11,$11,$11,$11,$61,$11,$11
  end asm

room66:
asm
  defb $11,$11,$60,$00,$11,$61,$11,$11,$11,$11,$11,$11
  defb $11,$16,$00,$00,$11,$11,$61,$16,$11,$61,$66,$11
  defb $00,$00,$00,$01,$11,$11,$11,$11,$11,$11,$16,$11
  defb $11,$60,$00,$01,$11,$11,$11,$16,$11,$16,$11,$11
  defb $61,$11,$16,$16,$61,$11,$81,$11,$11,$16,$16,$61
  defb $16,$11,$11,$11,$61,$11,$11,$61,$61,$71,$16,$11
  defb $11,$18,$88,$18,$86,$88,$68,$11,$68,$88,$88,$78
  defb $88,$88,$88,$76,$88,$68,$81,$71,$88,$88,$88,$88
  defb $00,$00,$00,$16,$11,$11,$16,$11,$61,$00,$00,$00
  defb $00,$00,$00,$0E,$01,$61,$60,$00,$0E,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$0A,$00,$00,$00,$00,$0A,$00,$00,$00
  defb $00,$00,$00,$0E,$00,$00,$00,$00,$0E,$00,$00,$00
  defb $0F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $88,$88,$88,$33,$38,$88,$88,$88,$33,$38,$88,$88
  defb $88,$88,$88,$87,$88,$87,$88,$88,$88,$88,$88,$88
  defb $11,$11,$11,$11,$11,$16,$11,$11,$16,$11,$61,$11
  defb $11,$11,$11,$61,$11,$11,$11,$16,$11,$11,$11,$11
  end asm


