#include "library/rx78header.bas"
#include "library/smsrnd.bas"
#include "library/b1r0f1_charmapdday01.zxi"
#include "library/smsdelay.bas"
#include "library/rx78ldir24b.bas"
#include "library/rx78ldirxor24b.bas"
#include "library/rx78fillram24b.bas"
#include "library/rx78fillramxor24b.bas"

dim eee as uinteger at $B010
dim seed as uinteger at $B012
dim ee0 as uinteger at $B014
dim ee1 as uinteger at $B016
dim ee2 as uinteger at $B018
dim ee3 as uinteger at $B01A
dim ee4 as uinteger at $B020
dim ee5 as uinteger at $B022
dim ee6 as uinteger at $B024
dim ee7 as uinteger at $B026
dim eex as uinteger at $B01C
dim eey as uinteger at $B01E

sub rx78fillchar(txp1 as uinteger,typ1 as uinteger,tvl1 as uinteger):
  rx78fillram24b($EEC0+(typ1*192)+txp1,tvl1,8)
  end sub

sub rx78fillcharxor(txp1 as uinteger,typ1 as uinteger,tvl1 as uinteger):
  rx78fillramxor24b($EEC0+(typ1*192)+txp1,tvl1,8)
  end sub

sub rx78putchar(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,tad1 as uinteger):
  rx78ldir24b($EEC0+(typ1*192)+txp1,tad1+(tch1*8),8)
  end sub

'sub rx78putcharwsp(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,tad1 as uinteger,txv1 as ubyte):
'  rx78ldirxor24b($EEC0+(typ1*192)+txp1,tad1+(tch1*8),8,txv1)
'  end sub

sub rx78putcharwsp(txp2 as uinteger,typ2 as uinteger,tch2 as uinteger,tad2 as uinteger,tma1 as ubyte, tma2 as ubyte):
  out $F2,$FF:rx78fillchar(txp2,typ2,0)
  out $F2,tma1:rx78putchar(txp2,typ2,tch2,tad2)
  out $F2,tma2:rx78fillcharxor(txp2,typ2,$FF)
  end sub

out $FC,%00000000
out $F5,%01000100
out $F6,%00010001
out $F7,%00100010

for ee1=0 to 22
  for ee0=0 to 23
    ee2=seed:seed=smsrnd(seed)
    ee3=seed:seed=smsrnd(seed)
    ee4=seed:seed=smsrnd(seed)
    rx78putcharwsp(ee0,ee1,96+(ee2 mod 9),@charmap01-256,ee3,ee4)
    smsdelay(100)
    next:next

for ee1=2 to 6
  for ee0=0 to 15
    rx78putcharwsp(ee0+7,ee1+15,ee1*16+ee0,@charmap01-256,%100,%011)
    smsdelay(100)
    next:next





eee=0:eex=8:eey=8
do

rx78putcharwsp(1,1,48+(int(eee/10000) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(2,1,48+(int(eee/1000) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(3,1,48+(int(eee/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,1,48+(int(eee/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,1,48+(eee mod 10),@charmap01-256,%111,%111)

out $F4,1
ee0=in($F4)
rx78putcharwsp(3,3,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,3,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,3,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,2
ee0=in($F4)
rx78putcharwsp(3,4,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,4,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,4,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,3
ee0=in($F4)
rx78putcharwsp(3,5,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,5,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,5,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,4
ee0=in($F4)
rx78putcharwsp(3,6,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,6,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,6,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,5
ee0=in($F4)
rx78putcharwsp(3,7,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,7,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,7,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,6
ee0=in($F4)
rx78putcharwsp(3,8,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,8,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,8,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,7
ee0=in($F4)
rx78putcharwsp(3,9,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,9,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,9,48+(ee0 mod 10),@charmap01-256,%111,%111)

eey=eey+((ee0 band 2)/2)-((ee0 band 4)/4)
eex=eex+((ee0 band 8)/8)-((ee0 band 16)/16)

rx78putcharwsp(eex,eey,32+(eee mod 74),@charmap01-256,%111,%111)

out $F4,8
ee0=in($F4)
rx78putcharwsp(3,10,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,10,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,10,48+(ee0 mod 10),@charmap01-256,%111,%111)

out $F4,9
ee0=in($F4)
rx78putcharwsp(3,11,48+(int(ee0/100) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(4,11,48+(int(ee0/10) mod 10),@charmap01-256,%111,%111)
rx78putcharwsp(5,11,48+(ee0 mod 10),@charmap01-256,%111,%111)

'seed=smsrnd(seed):out $F2,seed


if (eee band 15)=0 then

ee1=seed:seed=smsrnd(seed)
out $F5+(seed mod 6),ee1
seed=smsrnd(seed):out $FC,seed


seed=smsrnd(seed)
if (seed band 15)=0 then

out $F5,%01000100
out $F6,%00010001
out $F7,%00100010
out $F8,%00000000
out $F9,%00000000
out $FA,%00000000
out $FC,%00000000
end if

end if

eee=eee+1
loop


do:loop




'do
'ee0=seed:seed=smsrnd(seed)
'ee1=seed:seed=smsrnd(seed)
'ee2=seed:seed=smsrnd(seed)
'rx78putchar(ee0 mod 24,ee1 mod 23,32+(ee2 mod 74),@charmap01-256)
'loop

do:loop

for eee=0 to 7
  poke $EEC0+(eee*24),peek(@tile01+eee)
  next

do:loop

tile01:
asm
end asm



