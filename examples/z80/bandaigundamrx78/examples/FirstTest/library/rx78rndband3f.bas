
function fastcall rx78rnb3f(trds as uinteger) as uinteger:
  asm
    ld  d, h
    ld  e, l
    ld  a, d
    ld  h, e
    ld  l, 253
    or  a
    sbc  hl, de
    sbc  a, 0
    sbc  hl, de
    ld  d, 0
    sbc  a, d
    ld  e, a
    sbc  hl, de
    jr  nc, rx78rnb3floop
    inc  hl
    rx78rnb3floop:
    ld b,$07
    and b
    ret
    end asm
end function


' from haroldoop
' dim randomSeed as UInteger
' Based on code found on Z80 bits
' http://baze.au.com/misc/z80bits.html#4.2

