rm example01.bin _dummy64k.bin
dd bs=65536 count=1 if=/dev/zero of=_dummy64k.bin
zxb.py example01.bas --org=$((0x3000))
zxb.py example01.bas --asm --org=$((0x3000))
cat _dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=example01.bin2
rm example01.bin _dummy64k.bin
mv example01.bin2 example01.bin
mame rx78 -skip_gameinfo -resolution0 754x448 -aspect 425:320 -video soft -w -sound none -cart1 example01.bin

#dd bs=40000 count=1 if=/dev/zero of=_dummy64k.bin
#dd bs=40000 count=1 if=/dev/urandom of=_dummy64k.bin
#zxb.py library/smsboot.bas --org=1024
#dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=example01.bin2_
#cat example01.bin >> example01.bin2_
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin2_ of=example01.bin2

