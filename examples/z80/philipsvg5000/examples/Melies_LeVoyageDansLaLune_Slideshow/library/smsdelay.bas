sub fastcall smsdelay(ttm as uinteger):
  asm
    ld b,h
    ld c,l

    smsdelayloop:
    dec bc
    ld a,b
    or c
    jp nz,smsdelayloop

    end asm
  end sub


