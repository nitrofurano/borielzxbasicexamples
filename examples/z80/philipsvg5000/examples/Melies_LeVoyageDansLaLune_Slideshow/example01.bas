#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/vg5000putcharbiosvarsyspw.bas"
#include "library/vg5000setetgvarsyspw.bas"
#include "library/charmapvg5000v01.bas"

goto jmp01q01a:
picsall:
#include "library_pictures/01.bas"
#include "library_pictures/02.bas"
#include "library_pictures/03.bas"
#include "library_pictures/04.bas"
#include "library_pictures/05.bas"
#include "library_pictures/06.bas"
#include "library_pictures/07.bas"
#include "library_pictures/08.bas"
#include "library_pictures/09.bas"
#include "library_pictures/10.bas"
#include "library_pictures/11.bas"
#include "library_pictures/12.bas"
jmp01q01a:

asm
di
end asm

dim eee as uinteger at $7010
dim seed as uinteger at $7012
dim ee0 as uinteger at $7014
dim ee1 as uinteger at $7016
dim ee2 as uinteger at $7018
dim ee3 as uinteger at $701A
dim eex as uinteger at $701C
dim eey as uinteger at $701E

dim tw0 as uinteger at $4838
dim tw1 as uinteger at $483A
dim tw2 as uinteger at $483C
dim tw3 as uinteger at $483E
dim tb0 as ubyte at $4838
dim tb1 as ubyte at $4839
dim tb3 as ubyte at $483A
dim tb2 as ubyte at $483B
dim tb4 as ubyte at $483C
dim tb5 as ubyte at $483D
dim tb6 as ubyte at $483E
dim tb7 as ubyte at $483F

seed=40

poke $47FD,0:poke $47FB,1
asm
  ei
  halt
  halt
  di
  end asm

for ee0=0 to 95
tw0=32+ee0:tw1=@charmap01+ee0*10:vg5000setetgvarsyspw()
tw0=128+32+ee0:tw1=@charmap01+ee0*10:vg5000setetgvarsyspw()
next

poke $47FD,0:poke $47FB,1
asm
  ei
  halt
  halt
  di
  end asm

poke $47FD,0:poke $47FB,1
asm
  ei
  halt
  halt
  di
  end asm


do
for ee3=0 to 11
ee2=ee3*500
for eey=0 to 1
  for eex=0 to 39 step 2
    ee1=%10000111
    ee0=int(peek(@picsall+ee2)/16):ee0=(ee0 band %00001111) bor %11100000
    tb0=0+eex:tb1=6+eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
    ee0=(peek(@picsall+ee2)mod 16):ee0=(ee0 band %00001111) bor %11100000
    tb0=1+eex:tb1=6+eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
    ee2=ee2+1
    next:next

ee2=ee3*500
for eey=0 to 25
  for eex=0 to 39 step 2
    ee1=%10000111
    ee0=int(peek(@picsall+ee2)/16):ee0=(ee0 band %00001111) bor %11100000
    tb0=0+eex:tb1=7+eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
    ee0=(peek(@picsall+ee2)mod 16):ee0=(ee0 band %00001111) bor %11100000
    tb0=1+eex:tb1=7+eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
    ee2=ee2+1
    next:next

for eey=1 to 20
smsdelay(10000)
next

next
loop

do:loop


'
'
'
'
'for eey=0 to 25
'  for eex=0 to 39
'    'ee0=seed:seed=smsrnd(seed):ee0=(ee0 band %01111111) bor %00000000
'    'ee0=seed:seed=smsrnd(seed):ee0=(ee0 band %00001111) bor %11100000
'    ee0=seed:seed=smsrnd(seed):ee0=(ee0 band %00001111) bor %11110000
'    ee1=seed:seed=smsrnd(seed):ee1=(ee1 band %11110111) bor %10000000
'    tb0=0+eex:tb1=6+eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
'    smsdelay(100)
'    next:next
'
'for eey=0 to 15
'  for eex=0 to 15
'    tb0=23+eex:tb1=15+eey:tb2=eey*16+eex:tb3=%00000000:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
'    smsdelay(100)
'    next:next
'
'for eey=0 to 15
'  for eex=0 to 15
'    tb0=7+eex:tb1=15+eey:tb2=eey*16+eex:tb3=%11110000:vg5000putcharbiosvarsyspw()    '- vg5000putcharbios(3+eex,11+eey,seed,%11100111)
'    smsdelay(100)
'    next:next
'
'
'
'eee=0:eex=8:eey=12
'
'do
'tb0=1:tb1=8:tb2=$B0+(int(eee/10000) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=8:tb2=$B0+(int(eee/1000) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=8:tb2=$B0+(int(eee/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=4:tb1=8:tb2=$B0+(int(eee/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=5:tb1=8:tb2=$B0+(eee mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($80) bxor $FF
'tb0=1:tb1=10:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=10:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=10:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'eex=eex-((ee0 band 8)/8)
'eex=eex+((ee0 band 16)/16)
'eey=eey+((ee0 band 32)/32)
'
'ee0=in($81) bxor $FF
'tb0=1:tb1=11:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=11:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=11:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'eey=eey-((ee0 band 64)/64)
'
'ee0=in($82) bxor $FF
'tb0=1:tb1=12:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=12:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=12:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($83) bxor $FF
'tb0=1:tb1=13:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=13:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=13:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($84) bxor $FF
'tb0=1:tb1=14:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=14:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=14:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($85) bxor $FF
'tb0=1:tb1=15:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=15:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=15:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($86) bxor $FF
'tb0=1:tb1=16:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=16:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=16:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
'ee0=in($87) bxor $FF
'tb0=1:tb1=17:tb2=$B0+(int(ee0/100) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=2:tb1=17:tb2=$B0+(int(ee0/10) mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'tb0=3:tb1=17:tb2=$B0+(ee0 mod 10):tb3=%10000111:vg5000putcharbiosvarsyspw()
'
''ee0=seed:seed=smsrnd(seed):ee0=(ee0 band %01111111) bor %00000000 
''ee1=seed:seed=smsrnd(seed):ee1=(ee1 band %11110111) bor %10000000
'
'ee0=128+32+(eee mod 64)
'ee1=%10000111
'tb0=eex:tb1=eey:tb2=ee0:tb3=ee1:vg5000putcharbiosvarsyspw()
'
'
'poke $47FD,0
'poke $47FB,1
'asm
'  ei
'  halt
'  di
'  end asm
'
'smsdelay(1000)
'eee=eee+1
'loop
'
'do:loop
'
'
'charmaptest01:
'asm
'  end asm
'
'

'
