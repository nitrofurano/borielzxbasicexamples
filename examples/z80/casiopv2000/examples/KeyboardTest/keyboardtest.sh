echo -ne "\xC3\x03\xC0" > header.bin
rm keyboardtest.asm keyboardtest.rom
zxb.py keyboardtest.bas --asm --org=$((0xBFF3))
zxb.py keyboardtest.bas --org=$((0xBFF3))
dd ibs=1 skip=$((0x0010)) if=keyboardtest.bin of=keyboardtest.rom
cat keyboardtest.rom >> header.bin
rm keyboardtest.rom
mv header.bin keyboardtest.rom
dd bs=$((0x4000)) count=1 if=/dev/zero of=dummy16k.bin
cat dummy16k.bin >> keyboardtest.rom
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=keyboardtest.rom of=keyboardtest.rom_
rm keyboardtest.rom keyboardtest.bin dummy16k.bin
mv keyboardtest.rom_ keyboardtest.rom
mame pv2000 -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 keyboardtest.rom

