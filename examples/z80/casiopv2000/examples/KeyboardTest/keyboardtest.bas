asm
  ld sp,$7FFF
  end asm

#include "library/pv2000vdp.bas"
#include "library/pv2000filvrm.bas"
#include "library/pv2000ldirvm.bas"
#include "library/pv2000vpoke.bas"
#include "library/pv2000rnd.bas"
#include "library/arcade_b.bas"

'-------------------------------------------------------------------------------
'- defining variables

dim eee as uinteger at $7010
dim ee0 as uinteger at $7012
dim ee1 as uinteger at $7014
dim ee2 as uinteger at $7016
dim ee3 as uinteger at $7018
dim xdr as uinteger at $701A
dim trk as uinteger at $701C
dim spd as uinteger at $701E
dim cntr as uinteger at $7020

cntr=0

'-------------------------------------------------------------------------------

sub zprintdecimal2(tpos as uinteger,tval as uinteger):
  pv2000vpoke(6144+tpos+0,48+int(tval/10) mod 10)
  pv2000vpoke(6144+tpos+1,48+tval mod 10)
  end sub

sub zprintdecimal5(tpos as uinteger,tval as uinteger):
  pv2000vpoke(6144+tpos+0,48+int(tval/10000) mod 10)
  pv2000vpoke(6144+tpos+1,48+int(tval/1000) mod 10)
  pv2000vpoke(6144+tpos+2,48+int(tval/100) mod 10)
  pv2000vpoke(6144+tpos+3,48+int(tval/10) mod 10)
  pv2000vpoke(6144+tpos+4,48+tval mod 10)
  end sub

sub zprintbinary(tpos as uinteger,tval as uinteger):
  pv2000vpoke(6144+tpos+0,48+int(tval/128) mod 2)
  pv2000vpoke(6144+tpos+1,48+int(tval/64) mod 2)
  pv2000vpoke(6144+tpos+2,48+int(tval/32) mod 2)
  pv2000vpoke(6144+tpos+3,48+int(tval/16) mod 2)
  pv2000vpoke(6144+tpos+4,48+int(tval/8) mod 2)
  pv2000vpoke(6144+tpos+5,48+int(tval/4) mod 2)
  pv2000vpoke(6144+tpos+6,48+int(tval/2) mod 2)
  pv2000vpoke(6144+tpos+7,48+tval mod 2)
  end sub

'-------------------------------------------------------------------------------
'- whatever...
'msxcolor($6,$F,$1)
'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?

'-------------------------------------------------------------------------------
'- whatever...

'msxcolor($6,$F,$1)
pv2000vdp(7,$61) 'pv2000vdp(7,F*16+4) '- ink*16+border - color(3,?,1)

'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
pv2000vdp(0,%00000010):pv2000vdp(1,%11100010) '- screen2,2 (graphics1)
'- vdp entries for screen2 - base vram addresses
pv2000vdp(2,$06) '- $1800
pv2000vdp(3,$FF) '- $2000
pv2000vdp(4,$03) '- $0000
pv2000vdp(5,$36) '- $1B00
pv2000vdp(6,$07) '- $3800

'-------------------------------------------------------------------------------

for ee1=$0000 to $17FF step $0800
  pv2000filvrm($0000+ee1,$F1,$0800)
  pv2000filvrm($2000+ee1,$1F,$0800)
  next

pv2000ldirvm($0100,@typeface01,768)
pv2000ldirvm($0900,@typeface01,768)
pv2000ldirvm($1100,@typeface01,768)

for ee1=$0000 to $17FF step $0800
  for ee0=0 to 15
    pv2000filvrm($2000+(ee0*8)+ee1,(ee0*$11) band $FF,8)
    next:next

pv2000filvrm($1800,$0C,768)

for ee0=0 to 15
  pv2000vpoke($1800+ee0,(ee0 band 255))
  pv2000vpoke($1900+ee0,(ee0 band 255))
  pv2000vpoke($1a00+ee0,(ee0 band 255))
  next

loopunkn01:

zprintdecimal5( (32*2)+1 , cntr )

for eee=0 to 9
  out $20,eee
  zprintbinary( (32*eee)+23 , (( in($20) band 15)*16) bor ( in($10) band 15) )
  next

asm
  halt
  end asm

cntr=cntr+1

goto loopunkn01

'-------------------------------------------------------------------------------

