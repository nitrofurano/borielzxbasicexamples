sub pv2000filvrm(tvram as uinteger,tvl as uinteger, tlen as uinteger):
  asm
    ld l,(ix+4)
    ld a,l
    ld ($4001),a    ;out ($bf),a

    ld h,(ix+5)
    ld a,h
    or $40
    ld ($4001),a    ;out ($bf),a

    ld b,(ix+9)
    ld c,(ix+8)

    pv2000filvrmloop:
    ld a,(ix+6)
    ld ($4000),a    ;out ($be),a
    dec bc
    ld a,b
    or c
    jp nz,pv2000filvrmloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : 
'           HL - start address
'           A  - data byte
'           BC - length of the area to be written
'Registers: AF, BC
