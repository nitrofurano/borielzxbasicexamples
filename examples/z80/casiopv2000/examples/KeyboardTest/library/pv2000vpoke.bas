sub pv2000vpoke(tvram as uinteger,tvl as uinteger):
  asm
    ld l,(ix+4)
    ld a,l
    ld ($4001),a    ;out ($bf),a

    ld h,(ix+5)
    ld a,h
    or $40
    ld ($4001),a    ;out ($bf),a

    ld a,(ix+6)
    ld ($4000),a    ;out ($be),a

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : A  - data byte
'           BC - length of the area to be written
'           HL - start address
'Registers: AF, BC
