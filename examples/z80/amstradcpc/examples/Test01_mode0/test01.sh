zxb.py library/gx4000boot.bas --org=1024
dd ibs=1 count=$((0x0080)) skip=$((0x0000)) if=gx4000boot.bin of=test01.bin_
zxb.py test01.bas --org=$((0x0080))
zxb.py test01.bas --asm --org=$((0x0080))
cat test01.bin >> test01.bin_

dd bs=524288 count=1 if=/dev/zero of=_dummybytes.bin
#dd bs=1048576 count=1 if=/dev/zero of=_dummybytes.bin

cat _dummybytes.bin >> test01.bin_

#16kb
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin

rm gx4000boot.bin test01.bin_ _dummybytes.bin
#rm test01.bin

buildcpr test01.bin test01.cpr
mame gx4000 -video soft -w -bp ~/.mess/roms -resolution0 800x544 -aspect 423:280 -skip_gameinfo -cart1 test01.cpr

# mess gx4000 -video soft -w -bp ~/.mess/roms -skip_gameinfo -cart1 test01.cpr
#mess gx4000 -video soft -w -bp ~/.mess/roms -skip_gameinfo -cart1 test01.bin
#mess sg1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 test01.bin

#32kb
#dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin
#64kb
#dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin
#128kb
#dd ibs=1 count=$((0x20000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin
#256kb
#dd ibs=1 count=$((0x40000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin
#512kb
#dd ibs=1 count=$((0x80000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin
#1024kb
#dd ibs=1 count=$((0x100000)) skip=$((0x0000)) if=test01.bin_ of=test01.bin

