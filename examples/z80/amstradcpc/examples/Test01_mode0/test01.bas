#include "library/cpcpalettergb.bas"
#include "library/cpcrnd.bas"
#include "library/cpcdelay.bas"
#include "library/cpcfillram.bas"
#include "library/cpcscreenmode.bas"
#include "library/cpcscreensize.bas"
#include "library/cpcscreenoffset.bas"
#include "library/cpcpluspalette.bas"
#include "library/cpcpluspalettergb.bas"
#include "library/cpcpalette.bas"
#include "library/cpckeyboard.bas"
#include "library/4x8charmap01.bas"

'-------------------------------

sub ldirputchar(tvram as uinteger, tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
   cpcldir1024bloop:
    ld a,(hl)   
    ld (de),a   
    inc de
    inc hl
    ld a,(hl)   
    ld (de),a   
    inc hl
    ;add de,2047    ; funciona?
    ex de,hl
    push de
    ld de,2047
    add hl,de
    pop de
    ex de,hl
    dec bc
    ld a,b
    or c
    jp nz,cpcldir1024bloop
    end asm
  end sub


sub putchar(qxpos1 as uinteger,qypos1 as uinteger,qch1 as uinteger,qxed1 as uinteger,qyed1 as uinteger,qadr1 as uinteger)
  ldirputchar($C000+((qypos1*qxed1+qxpos1)*2),qadr1+(qch1*16),8)
  end sub

sub writetext(qxp2 as uinteger,qyp2 as uinteger,qtxp as uinteger,qlng as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to qlng-1
    putchar(qxp2+t2,qyp2,peek(qtxp+t2),qxed2,qyed2,qadr2)
    next
  end sub

sub writebin(qxp2 as uinteger,qyp2 as uinteger,qvl as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to 7
    putchar(qxp2+t2,qyp2,48+((qvl band 128)/128),qxed2,qyed2,qadr2)
    qvl=qvl*2
    next
  end sub

'-------------------------------

cpcscreenmode(0)
cpcscreensize(40,25):cpcscreenoffset(48-4,35-5)

cpcfillram($C000,%00001100,$4000)

dim seed as uinteger at $BF80
dim e0 as uinteger at $BF82
dim e1 as uinteger at $BF84
dim ej0 as uinteger at $BF88
dim ej1 as uinteger at $BF8A
dim ejx as uinteger at $BF86
dim eex as uinteger at $BF8C
dim eey as uinteger at $BF8E

for e0=0 to 15
  cpcpaletteseq(e0,peek(@palette01z+e0 ))
  next

for e0=0 to 15
  cpcpluspalettergb(e0,$111*e0)
  next

cpcpluspalettergb(16,$000)
cpcpluspalettergb( 0,$000)
cpcpluspalettergb( 1,$FFF)
cpcpluspalettergb( 2,$944)
cpcpluspalettergb( 3,$7CC)
cpcpluspalettergb( 4,$95A)
cpcpluspalettergb( 5,$6A5)
cpcpluspalettergb( 6,$549)
cpcpluspalettergb( 7,$CD8)
cpcpluspalettergb( 8,$963)
cpcpluspalettergb( 9,$650)
cpcpluspalettergb(10,$C77)
cpcpluspalettergb(11,$666)
cpcpluspalettergb(12,$888)
cpcpluspalettergb(13,$AE9)
cpcpluspalettergb(14,$87C)
cpcpluspalettergb(15,$AAA)

for e1=0 to 24:for e0=0 to 39
  seed=cpcrnd(seed)
  putchar(e0,e1,96+(seed mod 16),40,35,@charmap01-512)
  next:next

for e1=0 to 7:for e0=0 to 15
  putchar(e0+23,e1+16,e0+(e1*16),40,35,@charmap01-512)
  next:next

writetext(23,14,@text01,12,40,35,@charmap01-512)

eex=8:eey=8
e0=0
do
  ej0=cpckeyboard(6) bxor $FF
  ej1=cpckeyboard(9) bxor $FF
  ejx=ej0 bor ej1

  eex=eex+((ejx band 8)/8)-((ejx band 4)/4)
  eey=eey+((ejx band 2)/2)-((ejx band 1)/1)
 
  writebin(31,1,ej0,40,35,@charmap01-512)
  writebin(31,2,ej1,40,35,@charmap01-512)
  writebin(31,3,cpckeyboard(3) bxor $FF,40,35,@charmap01-512)

  writebin(31,5,e0,40,35,@charmap01-512)
  putchar(38,6,48+(e0 mod 10),40,35,@charmap01-512)
  putchar(37,6,48+(int(e0/10) mod 10),40,35,@charmap01-512)
  putchar(36,6,48+(int(e0/100) mod 10),40,35,@charmap01-512)
  putchar(35,6,48+(int(e0/1000) mod 10),40,35,@charmap01-512)
  putchar(34,6,48+(int(e0/10000) mod 10),40,35,@charmap01-512)

  putchar(eex,eey,32+(e0 mod 64),40,35,@charmap01-512)

  cpcdelay(100)


  e0=e0+1

loop

'-------------------------------------------------------------------------------

palette01z:
asm
  defb 0,13,26, 6,   15,24,21,18,   19,20,11, 2,    5, 8, 7,17
  end asm

text01:
asm
  defb "HELLO WORLD!"
  end asm











