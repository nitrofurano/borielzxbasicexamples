sub cpcpalettergb(tad as uinteger,tvl as uinteger):
  '- not working, i wonder why...
  asm
    ld a,(ix+6)

    ld b,a    ;-> backup to register b
    and $0F
    ld c,a    ;-> register c stores blue

    ld a,b    ;<- gets backup back
    and $F0   ;-- gets green*16 from the backup
    rr a
    rr a
    rr a
    rr a
    ld b,a    ;- register b stores green

    ld a,(ix+7)
    and $0F
    ld d,a    ;- register d stores red

    ;--------------------------------------------------
    ;- i need to clean this up, redundant rr and rl

    ld a,b    ;- register a gets green(0..15) 
    rl a
    add a,b
    and $0C
    rr a
    rr a      ;- register a gets green(0..2)
    ld e,a
    rl a
    add a,e
    ld e,a    ;- register e stores green(0..2)*3

    ld a,d    ;- register a gets red(0..15)
    rl a
    add a,d
    and $0C
    rr a
    rr a      ;- register a gets red(0..2)
    add a,e     ;- add red to register a
    ld e,a
    rl a
    add a,e
    ld e,a    ;- register e stores ((green(0..2)*3=)+(red(0..2)))*3

    ld a,c    ;- register a gets blue(0..15) 
    rl a
    add a,c
    and $0C
    rr a
    rr a      ;- register a gets blue(0..2)
    add a,e     ;- add blue to register e

    ld e,a    ;- e gets (green*9)+(red*3)+blue

    ;--------------------------------------------------

    ld b,0
    ld c,e
    ld hl,(tpaletteseq2)
    add hl,bc
    ld d,(hl)

    ld b,$7F
    ld c,(ix+4)
    out (c),c
    out (c),d

    jr tpaletteseq2end

    tpaletteseq2:
    defb $54,$44,$55,$5C,$58,$5D,$4C,$45,$4D ;- g=0%
    defb $56,$46,$57,$5E,$40,$5F,$4E,$47,$4F ;- g=50%
    defb $52,$42,$53,$5A,$59,$5B,$4A,$43,$4B ;- g=100%
    tpaletteseq2end:

    end asm
  end sub
