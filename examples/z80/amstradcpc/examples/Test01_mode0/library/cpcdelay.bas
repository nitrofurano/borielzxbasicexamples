sub cpcdelay(ttm as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)

    cpcdelayloop:
    dec bc
    ld a,b
    or c
    jp nz,cpcdelayloop

    end asm
  end sub


