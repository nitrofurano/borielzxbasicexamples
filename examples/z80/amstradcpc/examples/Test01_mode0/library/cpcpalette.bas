sub cpcpalette(tad as ubyte, tvl as ubyte):
  asm
    ld b,$7F
    ld c,(ix+5)
    out (c),c
    ld a,(ix+7)
    out (c),a
    end asm
  end sub

sub cpcpaletteseq(tad2 as ubyte, tvl2 as ubyte):
  cpcpalette(tad2,peek(@tpaletteseq+tvl2))
  goto tpaletteseqend
  tpaletteseq:
    asm
    defb $54,$44,$55,$5C,$58,$5D,$4C,$45,$4D ;- g=0%
    defb $56,$46,$57,$5E,$40,$5F,$4E,$47,$4F ;- g=50%
    defb $52,$42,$53,$5A,$59,$5B,$4A,$43,$4B ;- g=100%
    end asm
  tpaletteseqend:

  end sub
