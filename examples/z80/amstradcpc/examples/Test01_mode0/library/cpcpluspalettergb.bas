sub cpcpluspalettergb(tad as uinteger,tvl as uinteger):
  '- it pokes the ram area, no idea if it will corrupt ram content accidentaly, and how to bypass this via memory paging
  asm
    ld a,(ix+4)
    and $3F
    rl a
    ld hl,$6400
    ld l,a ;-?

    ld a,(ix+6)
    ld b,a
    and $0F
    ld c,a

    ld a,b
    and $F0
    rr a
    rr a
    rr a
    rr a
    ld b,a

    ld a,(ix+7)
    and $0F
    rl a
    rl a
    rl a
    rl a
    or c
    ld (hl),a

    inc hl
    ld (hl),b

    end asm
  end sub
