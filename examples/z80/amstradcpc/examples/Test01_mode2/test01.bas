#include "library/cpcpalettergb.bas"
#include "library/cpcrnd.bas"
#include "library/cpcdelay.bas"
#include "library/cpcfillram.bas"
#include "library/cpcscreenmode.bas"
#include "library/cpcscreensize.bas"
#include "library/cpcscreenoffset.bas"
#include "library/cpcpluspalette.bas"
#include "library/cpcpluspalettergb.bas"
#include "library/cpcpalette.bas"
#include "library/cpckeyboard.bas"
'#include "library/charmap_bacachase.zxbasic"
#include "library/charmap_andrewremic.zxbasic"

sub putcharv1(qxpos1 as uinteger,qypos1 as uinteger,qch1 as uinteger,qxed1 as uinteger,qyed1 as uinteger,qadr1 as uinteger)
  dim t1 as uinteger at $BF00
  for ti1=0 to 7
    poke $C000+((qypos1*(qxed1*2))+qxpos1)+(ti1*2048),peek(qadr1+(qch1*8)+ti1)
    next
  end sub

sub writetext(qxp2 as uinteger,qyp2 as uinteger,qtxp as uinteger,qlng as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to qlng-1
    putcharv1(qxp2+t2,qyp2,peek(qtxp+t2),qxed2,qyed2,qadr2)
    next
  end sub

sub writebin(qxp2 as uinteger,qyp2 as uinteger,qvl as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to 7
    putcharv1(qxp2+t2,qyp2,48+((qvl band 128)/128),qxed2,qyed2,qadr2)
    qvl=qvl*2
    next
  end sub




cpcscreenmode(2)
'cpcscreensize(40,25):cpcscreenoffset(48-4,35-5)
'cpcscreensize(40,32):cpcscreenoffset(48-4,35-1)
'cpcscreensize(40,28):cpcscreenoffset(48-4,35-3)
'cpcscreensize(32,24):cpcscreenoffset(48-8,35-5)
cpcscreensize(48,21):cpcscreenoffset(48-0,35-6)
'cpcscreensize(48,35):cpcscreenoffset(48-0,35-0)


cpcfillram($C000,%10101010,$4000)

dim seed as uinteger at $BF80
dim e0 as uinteger at $BF82
dim e1 as uinteger at $BF84
dim e2 as uinteger at $BF86
dim e3 as uinteger at $BF88

for e0=0 to 15
  cpcpaletteseq(e0,peek(@palette01z+e0 ))
  next

'cpcpalettergb(1,$FFF)
'cpcpalettergb(0,$777)

for e0=0 to 15
  cpcpluspalettergb(e0,$111*e0)
  next

cpcpluspalettergb(1,$C84)
cpcpluspalettergb(0,$642)
cpcpluspalettergb(16,$A86)


for e1=0 to 7:for e0=0 to 31
  putcharv1(e0+8,10+e1,e0+(e1*32),48,35,@typeface01)
  next:next

'putcharv1(6,3,0,48,35,@charmaptest1)
writetext(9,5,@text01,12,48,35,@typeface01)

loop02:

for e0=0 to 255
  writebin(8,7,e0,48,35,@typeface01)

  writebin(31,4,cpckeyboard(6),48,35,@typeface01)
  writebin(31,5,cpckeyboard(9),48,35,@typeface01)

  'for e1=0 to 9
  '  writebin(31,4+e1, cpckeyboard(e1),48,35,@typeface01)
  '  next

  cpcdelay(1000)
  next


goto loop02

'-------------------------------------------------------------------------------

loop01:

for e0=0 to 2047 step 2
seed=cpcrnd(seed)
'e0=(seed mod 1000)*2
  for e1=0 to 16000 step 2048
    for e2=0 to 1
      seed=cpcrnd(seed)
      poke $C000+e0+e1+e2,seed
      next:next
  cpcdelay(5000)
  next

'next
'e0=(seed mod 1000)*2
'  for e1=0 to 16000 step 2048
'    for e2=0 to 1
'      poke $C000+e0+e1+e2,%00000011
'      next:next

goto loop01

palette01z:
asm
  defb 0,13,26, 6,   15,24,21,18,   19,20,11, 2,    5, 8, 7,17
  end asm

text01:
asm
  defb "hello world!"
  end asm



