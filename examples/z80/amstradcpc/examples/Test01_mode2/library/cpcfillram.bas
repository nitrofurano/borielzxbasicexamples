sub cpcfillram(tram as uinteger,tvl as uinteger, tlen as uinteger):
  asm
    ld b,(ix+6)
    ld d,(ix+9)
    ld e,(ix+8)
    ld h,(ix+5)
    ld l,(ix+4)
   fillram:
    ld (hl),b
    inc hl
    dec de
    ld a,d
    or e
    jr nz,fillram
    end asm
  end sub

'- b  value
'- de count
'- hl ram-address

