sub cpcscreensize(txsz as ubyte,tysz as ubyte):
  asm
    ld bc,$bc01
    out (c),c
    inc b
    ld a,(ix+5)
    out (c),a
    ld bc,$bc06
    out (c),c
    inc b
    ld a,(ix+7)
    out (c),a
    end asm
  end sub

';bootcrtcdata:
';defb 63,40,44,144, 38,0,25,30, 0,7,0,0, 48,0,192,0 ;- original (edited) (40x25 at 44,30 )
';defb 63,32,40,144, 38,0,24,29, 0,7,0,0, 48,0,192,0 ;- 32x24 at 40,29
';defb 63,45,46,144, 38,0,36,35, 0,7,0,0, 48,0,192,0 ;- 45x36 at 46,35

