
function fastcall cpckeyboard(tkl as ubyte) as ubyte:
  asm
    ;ld a,kbdline ;- from $40 to $49 with bdir/bc1=01
    and $0F
    or $40
    ld d,0
    ld bc,$f782 ;- ppi port a out /c out 
    out (c),c 
    ld bc,$f40e ;- select ay reg 14 on ppi port a 
    out (c),c 
    ld bc,$f6c0 ;- this value is an ay index (r14) 
    out (c),c 
    out (c),d ;- validate!! out (c),0
    ld bc,$f792 ;- ppi port a in/c out 
    out (c),c 
    dec b 
    out (c),a ;- send kbdline on reg 14 ay through ppi port a
    ld b,$f4 ;- read ppi port a 
    in a,(c) ;- e.g. ay r14 (ay port a) 
    ld bc,$f782 ;- ppi port a out / c out 
    out (c),c 
    dec b ;- reset ppi write 
    out (c),d ;- out (c),0
    end asm
  end function

'- from http://www.cpcwiki.eu/index.php/Programming:Keyboard_scanning

'-------------------------------------------------------------------------------
'-       7   6   5   4   3   2   1   0
'- $40  FD  EN  F3  F6  F9  cD  cR  cU
'- $41  F0  F2  F1  F5  F8  F7  CP  cL
'- $42  CT   \  SH  F4   ]  RT   [  CL
'- $43   .   /   :   ;   P   @   -   ^
'- $44   ,   M   K   L   I   O   9   0
'- $45  SP   N   J   H   Y   U   7   8
'- $46   V   B   F   G   T   R   5   6
'- $47   X   C   D   S   W   E   3   4
'- $48   Z  CL   A  TB   Q  ES   2   1
'- $49  DL 1F3 1F2 1F1  1R  1L  1D  1U
'-------------------------------------------------------------------------------
'- gx4000 joystick
'- (up) (down) (left) (right) Z X
'- 6    5      R      T       G F
'- P
'-------------------------------------------------------------------------------

