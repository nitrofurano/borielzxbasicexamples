asm

;; This example shows a suggested startup for a cartridge
;; cartridge page 0 exists at $0000-$3fff
;; execution starts at $0000

org $0000

start1:
di					;; disable interrupts
im 1					;; set interrupt mode 1
ld bc,$f782			;; setup initial PPI port directions
out (c),c
ld bc,$f400			;; set initial PPI port A (AY)
out (c),c
ld bc,$f600			;; set initial PPI port C (AY direction)
out (c),c
ld bc,$7fc0			;; set initial RAM configuration
out (c),c

;org $0017
;; unlock ASIC so we can access ASIC registers
ld b,$bc
ld hl,bootasicsequence
ld e,17
seq:
ld a,(hl)
out (c),a
inc hl
dec e
jr nz,seq

;org $0025
;; set initial CRTC settings (screen dimensions etc)
ld hl,end_bootcrtcdata
ld bc,$bc0f

;org $002B
bootcrtcloop:
out (c),c
dec hl
ld a,(hl)
inc b
out (c),a
dec b
dec c
jp bootcrtccont

;----- 38h interrupt!!!! :S -------------------
;- remarked part with vbl for halt stuff? would it work?
org $0038
ei
;waitvbl38h:
;ld a,&F5
;in a,(&DB)
;rra
;jr nc,waitvbl38h
ret

;----------------------------------------------

bootcrtccont:
jp p,bootcrtcloop

ld hl,$c9fb
ld ($0038),hl
ei

;; enable asic ram (will be visible in range $4000-$7fff)
ld bc,$7fb8
out (c),c

ld sp,$C000

jp start

;; your crtc setup values here; these are examples
bootcrtcdata:
defb 63,40,44,144,38,0,25,30,0,7,0,0,48,0,192,0 ;- original (edited) (40x25 at 44,30 )
;defb 63,32,40,144,38,0,24,29,0,7,0,0,48,0,192,0 ;- 32x24 at 40,29
;defb 63,45,46,144,38,0,36,35,0,7,0,0,48,0,192,0 ;- 45x36 at 46,35
;defb $3f,$28,$2e,$8e,$26,$00,$19,$1e,$00,$07,$00,$00,$30,$00,$C0,$00 ;- original
; more info at http://cpcwiki.eu/index.php/CRTC
end_bootcrtcdata:

;; sequence to unlock asic
bootasicsequence:
defb $FF,$00,$FF,$77,$B3,$51,$A8,$D4,$62,$39,$9C,$46,$2B,$15,$8A,$CD,$EE

start:
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

end asm


