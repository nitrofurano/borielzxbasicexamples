sub cpcpluspalette(tad as uinteger,tvl as uinteger):
  '- it pokes the ram area, no idea if it will corrupt ram content accidentaly, and how to bypass this via memory paging
  asm
    ld a,(ix+4)
    and $3F
    rl a
    ld hl,$6400
    ld l,a ;-?
    ld a,(ix+6)
    ld (hl),a
    inc hl
    ld a,(ix+7)
    ld (hl),a
    end asm
  end sub
