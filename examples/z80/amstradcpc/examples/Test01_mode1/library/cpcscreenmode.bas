sub cpcscreenmode(tsmd as uinteger):
  asm
    ld a,(ix+4)
    and $03
    ld bc,$7f80 ;- screen mode
    or c
    ld c,a
    out (c),c
    end asm
  end sub
