#include "library/cpcpalettergb.bas"
#include "library/cpcrnd.bas"
#include "library/cpcdelay.bas"
#include "library/cpcfillram.bas"
#include "library/cpcscreenmode.bas"
#include "library/cpcscreensize.bas"
#include "library/cpcscreenoffset.bas"
#include "library/cpcpluspalette.bas"
#include "library/cpcpluspalettergb.bas"
#include "library/cpcpalette.bas"
#include "library/cpckeyboard.bas"
#include "library/charmap_arcade_cpcmode1.bas"

'-------------------------------

sub ldirputchar(tvram as uinteger, tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
   cpcldir1024bloop:
    ld a,(hl)   
    ld (de),a   
    inc de
    inc hl
    ld a,(hl)   
    ld (de),a   
    inc hl
    ;add de,2047    ; funciona?
    ex de,hl
    push de
    ld de,2047
    add hl,de
    pop de
    ex de,hl
    dec bc
    ld a,b
    or c
    jp nz,cpcldir1024bloop
    end asm
  end sub

sub putchar(qxpos1 as uinteger,qypos1 as uinteger,qch1 as uinteger,qxed1 as uinteger,qyed1 as uinteger,qadr1 as uinteger)
  ldirputchar($C000+((qypos1*qxed1+qxpos1)*2),qadr1+(qch1*16),8)
  'dim ti1 as uinteger at $BF00
  'for ti1=0 to 15 step 2
  '  poke uinteger $C000+((qypos1*qxed1+qxpos1)*2)+(ti1*1024),peek(uinteger,qadr1+(qch1*16)+ti1)
  '  next
  end sub

sub writetext(qxp2 as uinteger,qyp2 as uinteger,qtxp as uinteger,qlng as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to qlng-1
    putchar(qxp2+t2,qyp2,peek(qtxp+t2),qxed2,qyed2,qadr2)
    next
  end sub

sub writebin(qxp2 as uinteger,qyp2 as uinteger,qvl as uinteger,qxed2 as uinteger,qyed2 as uinteger,qadr2 as uinteger):
  dim t2 as uinteger at $BF02
  for t2=0 to 7
    putchar(qxp2+t2,qyp2,48+((qvl band 128)/128),qxed2,qyed2,qadr2)
    qvl=qvl*2
    next
  end sub

'-------------------------------

cpcscreenmode(1)
cpcscreensize(32,24):cpcscreenoffset(48-8,35-5)

cpcfillram($C000,%00001111,$4000)

dim seed as uinteger at $BF90
dim ee0 as uinteger at $BF92
dim ee1 as uinteger at $BF94
dim ej0 as uinteger at $BF96
dim ej1 as uinteger at $BF98
dim eex as uinteger at $BF9A
dim eey as uinteger at $BF9C
dim eeq as uinteger at $BF9E
dim ejz as uinteger at $BFA0

for ee0=0 to 15
  cpcpaletteseq(ee0,peek(@palette01+ee0 ))
  next

for ee0=0 to 15
  cpcpluspalettergb(ee0,$111*ee0)
  next

'- http://www.colourlovers.com/palette/638540
cpcpluspalettergb(16,$222)
cpcpluspalettergb(0,$431)
cpcpluspalettergb(1,$763)
cpcpluspalettergb(2,$DB6)
cpcpluspalettergb(3,$832)

for ee1=0 to 23:for ee0=0 to 31
  seed=cpcrnd(seed)
  putchar(ee0,ee1,96+(seed band 3),32,35,@charmaptest2-512)
  next:next

for ee1=0 to 7:for ee0=0 to 15
  putchar(ee0+15,ee1+15,ee0+(ee1*16),32,35,@charmaptest2-512)
  next:next

writetext(15,13,@text01,12,32,35,@charmaptest2-512)

eex=8:eey=8:eeq=0
do

for ee0=0 to 255
  ej0=cpckeyboard(6) bxor $FF
  ej1=cpckeyboard(9) bxor $FF
  ejz=ej0 bor ej1

  eex=eex+((ejz band 8)/8)-((ejz band 4)/4)
  eey=eey+((ejz band 2)/2)-((ejz band 1)/1)

  writebin(23,1,ej0,32,35,@charmaptest2-512)
  writebin(23,2,ej1,32,35,@charmaptest2-512)
  writebin(23,3,cpckeyboard(3) bxor $FF,32,35,@charmaptest2-512)
  writebin(23,5,ee0,32,35,@charmaptest2-512)

  putchar(eex,eey,32+(eeq mod 64),32,35,@charmaptest2-512)

  cpcdelay(50)
  eeq=eeq+1
  next

loop

'-------------------------------------------------------------------------------

palette01:
asm
  defb 0,13,26, 6,   15,24,21,18,   19,20,11, 2,    5, 8, 7,17
  end asm

text01:
asm
  defb "HELLO WORLD!"
  end asm


