
goto start:
#include "./library/map01.zxi"
#include "./library/vpokechr.zxi"
#include "./library/charmap01.zxi"
#include "./library/sprites01.zxi"
start:

poke uinteger 23606,@charmap01

dim x0,y0,xl,yl as uinteger
dim xp,yp as integer
dim tlb1 as byte
dim tlq1 as ubyte
dim v0 as ubyte
dim vl as integer
dim xs,ys,ds,fs as byte
dim cn0,cn1,cn2 as ubyte
dim kr,kg,kb,er,eg,eb as ubyte
dim ltyq,ltxq as uinteger
dim debug as ubyte

debug=0
randomize 520

dim a$(10)
a$(0)="\A\B\C\D\E\F\G\H\I"
a$(1)="\A\B\C\D\E\F\J\K\L"
a$(2)="         "
a$(3)="\M\N\O\P"

'--------------------------------------

sub puttilesprite1(txp9 as ubyte,typ9 as ubyte,tid9 as ubyte,tdr9 as ubyte ):
  poke uinteger 23675,@sprites01+(tdr9*128)
  print at typ9,txp9;a$(tid9)(TO 2)
  print at typ9+1,txp9;a$(tid9)(3 TO 5)
  print at typ9+2,txp9;a$(tid9)(6 TO 8)
  end sub

sub puttilesprite2(txp9 as ubyte,typ9 as ubyte,tid9 as ubyte,tat9 as ubyte ):
  poke uinteger 23675,@sprites01+(tid9*128)
  print ink tat9; at typ9,txp9;a$(3)(TO 1)
  print ink tat9; at typ9+1,txp9;a$(3)(2 TO 3)
  end sub

sub vpoketiles(txp1 as uinteger,typ1 as uinteger,tvl1 as ubyte):
  tlq1=128
  for tlb1=0 to 7
    if (tlq1 band tvl1)<>0 then:
      'print at typ1*2,(txp1*16)+(tlb1*2);paper 2;".."
      'print at typ1*2+1,(txp1*16)+(tlb1*2);paper 2;".."
      vl= (128+int(rnd*160))band 255
      vpokechr((txp1*16)+(tlb1*2)+(typ1*64),vl,@charmap01)
      vl= (128+int(rnd*160))band 255
      vpokechr((txp1*16)+(tlb1*2)+(typ1*64)+1,vl,@charmap01)
      vl= (128+int(rnd*160))band 255
      vpokechr((txp1*16)+(tlb1*2)+(typ1*64)+32,vl,@charmap01)
      vl= (128+int(rnd*160))band 255
      vpokechr((txp1*16)+(tlb1*2)+(typ1*64)+33,vl,@charmap01)
      end if
    tlq1=tlq1/2
    next
  end sub

'- bug somewhere
sub drawdoor(txq1 as uinteger,tyq1 as uinteger,txq2 as uinteger,tyq2 as uinteger,tatq as ubyte):
  for ltyq=tyq1 to tyq2
    for ltxq=txq1 to txq2
      vl= (128+int(rnd*160))band 255
      'vpokechr((ltyq*32)+ltxq,vl,@charmap01)
      vpokechrattr((ltyq*32)+ltxq,vl,@charmap01,tatq)
      next:next
  end sub

sub drawdoorrdmatt(txq1 as uinteger,tyq1 as uinteger,txq2 as uinteger,tyq2 as uinteger,tadr as uinteger):
  for ltyq=tyq1 to tyq2
    for ltxq=txq1 to txq2
      vl= (128+int(rnd*160))band 255
      vpokechrattr((ltyq*32)+ltxq,vl,@charmap01,peek(tadr+int(rnd*4)))
      next:next
  end sub

function attrpoint(txp2 as uinteger,typ2 as uinteger) as ubyte:
  return peek(22528+(typ2*32+txp2))
  end function

function attrpointb(txp2 as uinteger,typ2 as uinteger) as ubyte:
  return (peek(22528+(typ2*32+txp2))) band %11111000
  end function

'--------------------------------------


do

title:

paper 6:border 0:ink 0:bright 1: cls
gosub displaytitle01
pause 0


gameplay:

paper 6:border 0:ink 0:bright 1: cls

xp=4:yp=4:xs=16:ys=12:ds=0:fs=0
kr=0:kg=0:kb=0:er=0:eg=0:eb=0

gosub displaymap01
puttilesprite1(xs,ys,fs,ds)

'do
while (er+(eg*2)+(eb*4))<>7:


cn1=((255-(in 64510)) band 2)/2 bor ((255-(in 65022)) band 2) bor ((255-(in 65022)) band 1)*4 bor ((255-(in 65022)) band 4)*2 :'- wsad
cn2=((255-(in 61438)) band 8)/8 bor ((255-(in 61438)) band 16)/8 bor ((255-(in 63486)) band 16)/4 bor ((255-(in 61438)) band 4)*2 :'-7658
cn0=cn1 bor cn2

if ((cn0 band 4) <>0) and ((attrpointb(xs-1,ys)=112 and attrpointb(xs-1,ys+2)=112) or xs=0) then:
  puttilesprite1(xs,ys,2,0)
  xs=xs-1
  ds=1
  fs=fs bxor 1
  if xs<0 then:xs=29:xp=xp-1:gosub displaymap01:end if
  puttilesprite1(xs,ys,fs,ds)
  end if

if ((cn0 band 8) <>0) and ((attrpointb(xs+3,ys)=112 and attrpointb(xs+3,ys+2)=112 ) or xs=29) then:
  puttilesprite1(xs,ys,2,0)
  xs=xs+1
  ds=0
  fs=fs bxor 1
  if xs>29 then:xs=0:xp=xp+1:gosub displaymap01:end if
  puttilesprite1(xs,ys,fs,ds)
  end if

if ((cn0 band 1) <>0) and ((attrpointb(xs,ys-1)=112 and attrpointb(xs+2,ys-1)=112) or ys=0) then:
  puttilesprite1(xs,ys,2,0)
  ys=ys-1
  fs=fs bxor 1
  if ys<0 then:ys=21:yp=yp-1:gosub displaymap01:end if
  puttilesprite1(xs,ys,fs,ds)
  end if

if ((cn0 band 2) <>0) and ((attrpointb(xs,ys+3)=112 and attrpointb(xs+2,ys+3)=112 ) or ys=21) then:
  puttilesprite1(xs,ys,2,0)
  ys=ys+1
  fs=fs bxor 1
  if ys>21 then:ys=0:yp=yp+1:gosub displaymap01:end if
  puttilesprite1(xs,ys,fs,ds)
  end if

if int((xs+1)/2)=8 and int((ys+1)/2)=6 then
  if xp=0 and yp=0 then:kg=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- green key
  if xp=7 and yp=0 then:er=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- red egg
  if xp=4 and yp=1 then:kb=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- blue key
  if xp=3 and yp=7 then:eb=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- blue egg
  if xp=4 and yp=6 then:kr=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- red key
  if xp=7 and yp=7 then:eg=1:puttilesprite1(15,11,2,0):puttilesprite1(xs,ys,fs,ds):end if: '- green egg
  end if

asm
  halt
  halt
  halt
  end asm

'print at 3,1;attrpoint(xs-1,ys);"   "

'if (er+(eg*2)+(eb*4))<>7 then:loop:end if

end while

pause 200

loop

do:loop

'--------------------------------------

displaymap01:
cls
for y0=0 to 11:for x0=0 to 1
  v0=peek(@map01+(xp*2)+x0+(y0*16)+(yp*192))
  vpoketiles(x0,y0,v0)
  next:next

if debug=1 then:
  print at 1,1;xp
  print at 2,1;yp
  end if

if (xp=0 and yp=0) and kg=0 then:puttilesprite2(15,11,1,4):end if: '- green key
if (xp=7 and yp=0) and er=0 then:puttilesprite2(15,11,0,2):end if: '- red egg
if (xp=4 and yp=1) and kb=0 then:puttilesprite2(15,11,1,1):end if: '- blue key
if (xp=3 and yp=7) and eb=0 then:puttilesprite2(15,11,0,1):end if: '- blue egg
if (xp=4 and yp=6) and kr=0 then:puttilesprite2(15,11,1,2):end if: '- red key
if (xp=7 and yp=7) and eg=0 then:puttilesprite2(15,11,0,4):end if: '- green egg

if (xp=4 and yp=4) and kb=0 then:drawdoorrdmatt(1,22,30,22,@rnblu):end if: '- blue door
if (xp=6 and yp=1) and kr=0 then:drawdoorrdmatt(30,1,30,22,@rnred):end if: '- red door
if (xp=5 and yp=7) and kg=0 then:drawdoorrdmatt(30,1,30,22,@rngre):end if: '- green door

'if (xp=4 and yp=4) and kb=0 then:drawdoor(1,22,30,22,%01001111):end if: '- blue door
'if (xp=6 and yp=1) and kr=0 then:drawdoor(30,1,30,22,%01010111):end if: '- red door
'if (xp=5 and yp=7) and kg=0 then:drawdoor(30,1,30,22,%01100111):end if: '- green door

'print at 3,1;attrpoint(xs-1,ys)

return

'---

displaytitle01:

cls
for y0=0 to 11:for x0=0 to 1
  vpoketiles(x0,y0,255)
  next:next

print at 19,2;" H7N9 "
print at 20,2;" PAULO SILVA, 2015 "
print at 22,2;" PUSH ANY KEY "

return

'---

do:loop

'--------------------------------------

rnatr:
rnred:
asm
  defb %00010000
  defb %00010111
  defb %01010000
  defb %01010111
  end asm
rngre:
asm
  defb %00100000
  defb %00100111
  defb %01100000
  defb %01100111
  end asm
rnblu:
asm
  defb %00001000
  defb %00001111
  defb %01001000
  defb %01001111
  end asm

'--------------------------------------

