#include "library/vacaball_charmapudgpalette.bas"
#include "library/ulaplus.bas"

'- Bacaball - jun'05,oct'11 - Paulo Silva (copyleft, gpl3) - rev 20111113200239
'- for the csscgc2011 competition, hosted by mojon twins

debug=0

gosub startup

lp00:
gosub title
gosub playstart
gosub playloop
goto lp00

'-----------------------------------------------
startup:

bright 0:flash 0: inverse 0:border 0:ink 0:paper 0:cls

ulaplusswitch(1):cls
for jv=0 to 63 step 16
for iv=0 to 15
  ulapluspalette(iv+jv,peek(@paleta+iv))
  next:next
for jv=8 to 63 step 32
for iv=0 to 15
  ulapluspalette(iv+jv,peek(@paleta+iv))
  next:next

dim sq1,sq2,sq3,sq4 as uinteger
dim x1,y1,x2,y2,x3,y3,x4,y4 as float
dim xbp,ybp,xbd,ybd,spb as float
dim m1,m2,m3,m4 as ubyte
dim sc1,sc2,sc3,sc4,bown,govr,glim,gst as byte

return

'-----------------------------------------------
title:

bright 1:flash 0: inverse 0:border 0:ink 4:paper 3:cls

paper 4
for y=0 to 5:for x=0 to 31 step 4:print at y,x;chr$(32)+chr$(32)+chr$(32)+chr$(32):next:next

paper 3:ink 2
print at 14,3;"\K\M":print at 15,3;"\P\L"
print at 13,19;"\K\M":print at 14,19;"\P\L"
print at 15,27;"\K\M":print at 16,27;"\P\L"
print at 21,21;"\K\M":print at 22,21;"\P\L"

ink 4
print at 18,2;"BACABALL"
print at 19,2;chr$(127);" PAULO SILVA, '11"
print at 20,2;"PUSH ANY KEY"

pause 0

return

'-----------------------------------------------
playstart:

spb=6:glim=5:gst=0
x1=4:y1=11:x2=15:y2=20:x3=15:y3=2:x4=26:y4=11
m1=0:m2=0:m3=1:m4=1:xbp=16:ybp=12
sc1=gst:sc2=gst:sc3=gst:sc4=gst:bown=0:govr=0
xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32

border 0:bright 1:paper 3:ink 0:flash 0:cls

bright 1:paper 4:ink 5
for y=0 to 23:for x=0 to 31 step 4:print at y,x;chr$(157)+chr$(157)+chr$(157)+chr$(157):next:next

drawsprites()
inverse 0:ink 0:bright 1

return

'-----------------------------------------------
playloop:
lp01:
  pause 1

  if debug<>0 then: '- debug: pressed keys
    ink 3:paper 4:bright 1:print at 23,0;sq1;",";sq2;",";sq3;",";sq4;
    bright 1:paper 4:ink 5:print chr$(157)+chr$(157)+chr$(157)+chr$(157):
    end if

  bright 1:paper 4
  ink 3:print at 0,0;"1:";
  ink 2:print sc1;
  ink 5:print chr$(157);
  ink 3:print "2:";
  ink 2:print sc2;
  ink 5:print chr$(157);
  ink 3:print "3:";
  ink 2:print sc3;
  ink 5:print chr$(157);
  ink 3:print "4:";
  ink 2:print sc4;
  ink 5:print chr$(157)+chr$(157)+chr$(157)+chr$(157)

  bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
  xbp=xbp+xbd:ybp=ybp+ybd

  if debug=0 then:bright 1:paper 4:ink 7:print at ybp,xbp;chr$(158):end if
  if debug<>0 then:bright 1:paper 4:ink 7:print at ybp,xbp;chr$(128+16+16+bown):end if


  sq1=((255-(in 64510)) band 2)/2 bor ((255-(in 65022)) band 2) bor ((255-(in 65022)) band 1)*4 bor ((255-(in 65022)) band 4)*2 :'- wsad
  sq2=((255-(in 64510)) band 16)/16 bor ((255-(in 65022)) band 16)/8 bor ((255-(in 65022)) band 8)/2 bor ((255-(in 49150)) band 16)/2 :'- tgfh
  sq3=((255-(in 57342)) band 4)/4 bor ((255-(in 49150)) band 4)/2 bor ((255-(in 49150)) band 8)/2 bor ((255-(in 49150)) band 2)*4 :'- ikjl
  sq4=((255-(in 61438)) band 8)/8 bor ((255-(in 61438)) band 16)/8 bor ((255-(in 63486)) band 16)/4 bor ((255-(in 61438)) band 4)*2 :'-7658

  if sq1<>0 then
    bright 1:paper 4:ink 5:print at y1,x1;chr$(157)+chr$(157):print at y1+1,x1;chr$(157)+chr$(157)
    if ((sq1 band 4) <>0) and x1>0 then:x1=x1-.25:m1=1:end if
    if ((sq1 band 8) <>0) and x1<30 then:x1=x1+.25:m1=0:end if
    if ((sq1 band 1)<>0) and y1>0 then:y1=y1-.25:end if
    if ((sq1 band 2) <>0) and y1<22 then:y1=y1+.25:end if
    drawsprites()
    end if

  if sq2<>0 then
    bright 1:paper 4:ink 5:print at y2,x2;chr$(157)+chr$(157):print at y2+1,x2;chr$(157)+chr$(157)
    if ((sq2 band 4) <>0) and x2>0 then:x2=x2-.25:m2=1:end if
    if ((sq2 band 8) <>0) and x2<30 then:x2=x2+.25:m2=0:end if
    if ((sq2 band 1)<>0) and y2>0 then:y2=y2-.25:end if
    if ((sq2 band 2) <>0) and y2<22 then:y2=y2+.25:end if
    drawsprites()
    end if

  if sq3<>0 then
    bright 1:paper 4:ink 5:print at y3,x3;chr$(157)+chr$(157):print at y3+1,x3;chr$(157)+chr$(157)
    if ((sq3 band 4) <>0) and x3>0 then:x3=x3-.25:m3=1:end if
    if ((sq3 band 8) <>0) and x3<30 then:x3=x3+.25:m3=0:end if
    if ((sq3 band 1)<>0) and y3>0 then:y3=y3-.25:end if
    if ((sq3 band 2) <>0) and y3<22 then:y3=y3+.25:end if
    drawsprites()
    end if

  if sq4<>0 then
    bright 1:paper 4:ink 5:print at y4,x4;chr$(157)+chr$(157):print at y4+1,x4;chr$(157)+chr$(157)
    if ((sq4 band 4) <>0) and x4>0 then:x4=x4-.25:m4=1:end if
    if ((sq4 band 8) <>0) and x4<30 then:x4=x4+.25:m4=0:end if
    if ((sq4 band 1)<>0) and y4>0 then:y4=y4-.25:end if
    if ((sq4 band 2) <>0) and y4<22 then:y4=y4+.25:end if
    drawsprites()
    end if

  if bown<>1 then:
  xt=int(x1)-int(xbp):yt=int(y1)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
      xbd=(rnd*spb)/32
      ybd=((rnd*spb*2)-spb)/32
      bown=1
      end if:end if:end if

  if bown<>4 then:
  xt=int(x4)-int(xbp):yt=int(y4)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
      xbd=((rnd*spb)/32)*-1
      ybd=((rnd*spb*2)-spb)/32
      bown=4
      end if:end if:end if

  if bown<>2 then:
  xt=int(x2)-int(xbp):yt=int(y2)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
      xbd=((rnd*spb*2)-spb)/32
      ybd=((rnd*spb)/32)*-1
      bown=2
      end if:end if:end if

  if bown<>3 then:
  xt=int(x3)-int(xbp):yt=int(y3)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
      xbd=((rnd*spb*2)-spb)/32
      ybd=(rnd*spb)/32
      bown=3
      end if:end if:end if

  if int(xbp)<0 then:
    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    sc1=sc1-1
    if bown=2 then:sc2=sc2+1:end if
    if bown=3 then:sc3=sc3+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12:xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    end if

  if int(ybp)>23 then:
    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    sc2=sc2-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=3 then:sc3=sc3+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12:xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    end if

  if int(ybp)<0 then:
    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    sc3=sc3-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=2 then:sc2=sc2+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12:xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    end if

  if int(xbp)>31 then:
    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    sc4=sc4-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=2 then:sc2=sc2+1:end if
    if bown=3 then:sc3=sc3+1:end if
    bown=0:xbp=16:ybp=12:xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    end if

  if sc1<0 then:sc1=0:end if
  if sc2<0 then:sc2=0:end if
  if sc3<0 then:sc3=0:end if
  if sc4<0 then:sc4=0:end if

  if sc1>=glim then:govr=1:end if
  if sc2>=glim then:govr=1:end if
  if sc3>=glim then:govr=1:end if
  if sc4>=glim then:govr=1:end if

  if govr=0 then goto lp01:end if

  wnr=0
  if sc1>=glim then:wnr=1:end if
  if sc2>=glim then:wnr=2:end if
  if sc3>=glim then:wnr=3:end if
  if sc4>=glim then:wnr=4:end if

  print at 11,10;"PLAYER ";wnr;" WINS"
  pause 500

return

'-----------------------------------------------
sub drawsprites():
  bright 1:paper 4:ink 6:
  if m1=0 then:print at y1,x1;"\M\I":print at y1+1,x1;"\J\A":else:print at y1,x1;"\K\M":print at y1+1,x1;"\E\L":end if
  if m2=0 then:print at y2,x2;"\M\I":print at y2+1,x2;"\J\B":else:print at y2,x2;"\K\M":print at y2+1,x2;"\F\L":end if
  if m3=0 then:print at y3,x3;"\M\I":print at y3+1,x3;"\J\C":else:print at y3,x3;"\K\M":print at y3+1,x3;"\G\L":end if
  if m4=0 then:print at y4,x4;"\M\I":print at y4+1,x4;"\J\D":else:print at y4,x4;"\K\M":print at y4+1,x4;"\H\L":end if
  end sub


