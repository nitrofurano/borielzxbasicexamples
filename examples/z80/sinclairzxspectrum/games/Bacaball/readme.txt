Bacaball
(Paulo Silva, november 2011)

This game is a soccer-like for 4 players simultaneously on one keyboard.
(maybe the first one for zx-spectrum?)

And since cows loves to play with balls, why not coding a game about this? :)

Controls are:
- 'wsad' controls the first player
- 'tgfh' controls the second player
- 'ikjl' controls the third player
- cursors controls the fourth player

The winner should set 5 goals. If some ball reaches player field edge, one point is lost.

Crappy parts on this game: bugs...
- all 'sprites' flickers...;
- sometimes a 'ghost' of the last ball remains on a display edge;
- i really don't know how addictive is this game when played from 4 players;
- some more which are not mentioned here yet

This game is better played on black&white displays ('designed' for that), and also uses ULAplus to make the colour palette being as more greyscale as possible.

Emulators tested: FBZX, Fuse-GTK, and JSpeccy (this last one for cheking ULAplus compatibility)
Real machines tested: none

This game were compiled on Boriel's zxbasic-compiler
