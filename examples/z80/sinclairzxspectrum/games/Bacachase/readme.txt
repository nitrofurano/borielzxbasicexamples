Bacachase
(Paulo Silva, november 2011)

This game is based on Robots (BSD-Robots, or Robots from Gnome Games - http://en.wikipedia.org/wiki/Robots_%28computer_game%29 )

Controls are 'wsad' keys, and 'p' for hyperspace.

The dark cow from this game should runaway from the brighter cows (zombies), and also avoid the sleepy cows (those upside down).

Crappy parts on this game: lots of bugs...
- score table flashes a lot;
- sometimes the hyperspace gots out of display;
- and some more are still not mentioned here! :)

This game is better played on black&white displays ('designed' for that), and also uses ULAplus to make the colour palette being as more greyscale as possible.

Emulators tested: FBZX, Fuse-GTK, and JSpeccy (this last one for cheking ULAplus compatibility)
Real machines tested: none

This game were compiled on Boriel's zxbasic-compiler

(important note: the author of this version of this game is completelly against any kind of animal abuse)

