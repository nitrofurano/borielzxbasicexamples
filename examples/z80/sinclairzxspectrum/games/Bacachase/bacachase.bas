#include "library/bacachase_charmapudgpalette.bas"
#include "library/ulaplus.bas"

'- Bacachase - nov'11 - Paulo Silva (copyleft, gpl3) - rev 20111115175125
'- for the csscgc2011 competition, hosted by mojon twins

debug=0

'-----------------------------------------------
variables:
  dim i,j,enam,cnt,stam,stg,score,hiscore as integer
  dim x,y,d,kprs as byte
  dim txmm(768) as byte:dim xin(768) as byte:dim yin(768) as byte
  dim stin(768) as byte:dim din(768) as byte
  gosub startup

'-----------------------------------------------
lp00:
  gosub title
  gosub gamestart
playloop:
  gosub stagestart
  gosub stageplay
  if stam>0 then: goto playloop:end if
  goto lp00

'-----------------------------------------------
startup:
  bright 0:flash 0: inverse 0:border 0:ink 0:paper 0:cls
  ulaplusswitch(1):cls
  for jv=0 to 63 step 16
  for iv=0 to 15
    ulapluspalette(iv+jv,peek(@paleta+iv))
    next:next
  for jv=8 to 63 step 32
  for iv=0 to 15
    ulapluspalette(iv+jv,peek(@paleta+iv))
    next:next

  hiscore=0
  return

'-----------------------------------------------
title:
  paper 4:bright 1:border 0:cls

  paper 2
  for y=0 to 5:for x=0 to 31 step 4:print at y,x;chr$(32)+chr$(32)+chr$(32)+chr$(32):next:next

  paper 4:ink 3
  for c=0 to 15
    print at int(rnd*8)+15,int(rnd*32);chr$(145)
    next

  ink 1
  print at 18,2;"BACACHASE"
  print at 19,2;chr$(127);" PAULO SILVA, '11"
  print at 20,2;"PUSH ANY KEY"

  pause 0

  return

'-----------------------------------------------
gamestart:
  paper 8
  score=0:stg=1:stam=99
  return

'-----------------------------------------------
stagestart:

paper 4:bright 1:border 0:cls
gosub displayscore
gosub displaystamina

enam=4+(4*stg):cnt=enam

kprs=0
x=int(rnd*32):y=int(rnd*24)

for i=0 to 255
  xin(i)=int(rnd*32):yin(i)=int(rnd*23)+2
  din(i)=0:if xin(i)>x then:din(i)=1:end if
  stin(i)=0
  next

for i=0 to enam-1
  ink 6:print at yin(i),xin(i);chr$(144+din(i))
  next

ink 1:print at y,x;chr$(144+d)

return

'-----------------------------------------------
stageplay:

lp01:
pause 1

if inkey$="w" and kprs=0 then
  print at y,x;" "
  y=y-1:kprs=1:if y<3 then:y=2:end if
  ink 1:print at y,x;chr$(144+d)
  end if
if inkey$="s" and kprs=0 then
  print at y,x;" "
  y=y+1:kprs=1:if y>22 then:y=23:end if
  ink 1:print at y,x;chr$(144+d)
  end if
if inkey$="a" and kprs=0 then
  print at y,x;" "
  x=x-1:kprs=1:d=1:if x<1 then:x=0:end if
  ink 1:print at y,x;chr$(144+d)
  end if
if inkey$="d" and kprs=0 then
  print at y,x;" "
  x=x+1:kprs=1:d=0:if x>30 then:x=31:end if
  ink 1:print at y,x;chr$(144+d)
  end if
if inkey$="p" and kprs=0 then
  print at y,x;" "
  x=int(rnd*32):y=int(rnd*23)+2:kprs=1
  ink 1:print at y,x;chr$(144+d)
  end if

if inkey$="" and kprs=1 then
  kprs=0
  for i=0 to enam-1
    if stin(i)=0 then:
      print at yin(i),xin(i);" "
      if yin(i)>y then:yin(i)=yin(i)-1:end if
      if yin(i)<y then:yin(i)=yin(i)+1:end if
      if xin(i)>x then:xin(i)=xin(i)-1:end if
      if xin(i)<x then:xin(i)=xin(i)+1:end if
      end if
    for j=0 to i-1
      if yin(i)=yin(j) and xin(i)=xin(j) then
        stin(i)=1:stin(j)=1:score=score+10
        end if
      next
    if hiscore<score then:hiscore=score:end if
    gosub displayscore
    if xin(i)>x then:din(i)=1:end if
    if xin(i)<x then:din(i)=0:end if
    ink 6:print at yin(i),xin(i);chr$(144+din(i))
    if stin(i)=1 then
      ink 5:print at yin(i),xin(i);chr$(146)
      end if
    next
  cnt=0
  for i=0 to enam-1
    if stin(i)=0 then:cnt=cnt+1:end if
    if x=xin(i) and y=yin(i) then:stam=stam-1:end if
    next
  ink 1:print at y,x;chr$(144+d)
  gosub displaystamina
  end if

if cnt>0 then: goto lp01:end if

stg=stg+1

gosub displayscore
gosub displaystamina

return

'-------------------------------------------------
displayscore:
  paper 0:print at 0,10;"                      ";
  ink 5:print at 0,0;"SCORE:";:ink 7:print score;
  ink 5:print" HISCORE:";:ink 7:print hiscore;
  paper 8
  return

'-------------------------------------------------
displaystamina:
  paper 0:print at 1,28;"    ";
  ink 5:print at 1,0;"ZOMBIES:";:ink 7:print cnt;
  ink 5:print" STAMINA:";:ink 7:print stam;
  ink 5:print" STAGE:";:ink 7:print stg;" "
  paper 8
  return

'-------------------------------------------------


