
Solitario
(Paulo Silva, november 2011)

This game is merely based on that popular solitaire game.  
This game is based on the msx version for the Karoshi's msxbasic competition at winter'06

On the table there are 32 pieces, which you drop each one from this neighbour to their orthogonal empty space, until lasting the less possible pieces on table.

When select a piece, this one becomes yellow, and will be moved when you select after the empty space in an orthogonal position, jumping over an existing one.

The keys used are cursors (or 'wsad') and space. You have 15 minutes to solve the game.

In the hiscore table, the resting pieces are more significant than the time spent.

Crappy parts of this game: Bugs! :D
- select an unselected piece doesn't unselect the other one;
- sometimes the game over can not be checked, and the game got hanged;
- score name writer is not working as it should
- it's missing some way to quit the game (the msx version you could use 'Esc' key...)

This game is supporting a bit the ULAplus feature (only tested on JSpeccy emulator)
