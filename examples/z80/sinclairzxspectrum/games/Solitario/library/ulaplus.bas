dim tmpv as uinteger
dim fdbkv as ubyte

function ulapluscheck():
  tmpv=in(65339):pause 1:fdbkv=0
  out 48955,0:out 65339,0:pause 1
  if in(65339)<>tmpv then:fdbkv=1:out 48955,0:out 65339,tmpv:pause 1:end if
  return fdbkv
  end function

sub ulapluspalette(clv as ubyte, vlv as ubyte):
  out 48955,(clv band 63):out 65339,vlv:pause 1
  end sub

function ulaplusgetpalette(clv as ubyte)
  out 48955,(clv band 63)
  return in(65339)
  pause 1
  end function

sub ulaplusswitch(flgv as ubyte):
  out 48955,64:out 65339,(flgv band 1):pause 1
  end sub

