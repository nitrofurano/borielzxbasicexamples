'- ULAplus library for Boriel's zxbasic-compiler
'- 20111205164150 - (ↄ) Paulo Silva - http://nitrofurano.altervista.org
'- This file is released under the GPL v3 License

dim tmpv as uinteger
dim fdbkv as ubyte

function ulapluscheck():
  tmpv=in(65339):pause 1:fdbkv=0
  out 48955,0:out 65339,0:pause 1
  if in(65339)<>tmpv then:fdbkv=1:out 48955,0:out 65339,tmpv:pause 1:end if
  return fdbkv
  end function

sub ulapluspalette(clv as ubyte, vlv as ubyte):
  out 48955,(clv band 63):out 65339,vlv:pause 1
  end sub

function ulaplusgetpalette(clv as ubyte)
  out 48955,(clv band 63)
  return in(65339)
  pause 1
  end function

sub ulaplusswitch(flgv as ubyte):
  out 48955,64:out 65339,(flgv band 1):pause 1
  end sub

sub ulaplusattr(ikv as ubyte,pav as ubyte):
  '- usage: ink priority (from 0 to 63), and only using 3 bits lsb from paper
  inverse 0:ink(ikv band 7):paper(pav band 7)
  bright((ikv band 16)/16):flash((ikv band 32)/32)
  if (ikv band 8)<>0 then:
    inverse 1:ink(pav band 7):paper(ikv band 7)
    end if
  end sub

function ulaplushexcolour(hxcv as uinteger):
  tmpv=(hxcv band 12)/4
  tmpv=tmpv bor(hxcv band 224)
  hxcv=int(hxcv/256)
  tmpv=tmpv bor((hxcv band 14)*2)
  return tmpv
  end function
  

