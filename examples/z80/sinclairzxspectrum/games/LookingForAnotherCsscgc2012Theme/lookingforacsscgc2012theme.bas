#include "./lib/postscriptreader.bas"
#include "./lib/print64x32.bas"
#include "./lib/ulaplus.bas"

#include "./lib_drawings/titledrawing.bas"
#include "./lib_drawings/hamletdrawing.bas"
#include "./lib_drawings/cowdrawing.bas"
#include "./lib_drawings/horsedrawing.bas"
#include "./lib_drawings/maredrawing.bas"
#include "./lib_drawings/bulldrawing.bas"
#include "./lib_drawings/elephantdrawing.bas"
#include "./lib_drawings/narwhaldrawing.bas"
#include "./lib_drawings/ponydrawing.bas"
#include "./lib_drawings/turtledrawing.bas"

ulaplusswitch(1):cls

for i=0 to 7
for j=0 to 7
ulapluspalette(i*8+j,ulaplushexcolour(0222h*j))
next:next

paper 7:border 6:bright 0:flash 0:cls




rem ---------------------------------------------------------------------------------------------
titlepage:

cls
ulapluspalette(1,ulaplushexcolour(0B84h))
ulapluspalette(2,ulaplushexcolour(06AAh))
ulapluspalette(3,ulaplushexcolour(0CEDh))
ulapluspalette(14,ulaplushexcolour(0666h))
ulapluspalette(15,ulaplushexcolour(0854h))
rem palette based on http://www.colourlovers.com/palette/294883

ink 1:postscriptread(@titledrawing)
ink 2:postscriptread(@titledrawingp2)
ink 1:postscriptread(@titledrawingp3)

ink 3:
print64x32at(1,27,"Paulo Silva, 2012 - GPL license")
print64x32at(1,28,"features ulaplus, 4x6 characters, and postscript-like drawings")
print64x32at(1,30,"push any key")
'- version 20120301145340

titleloop:
pause 0
a$=inkey$
goto hamletpage
goto titleloop




rem ---------------------------------------------------------------------------------------------
hamletpage:

cls
ulapluspalette(1,ulaplushexcolour(0F54h))
ulapluspalette(2,ulaplushexcolour(0222h))
ulapluspalette(3,ulaplushexcolour(0DE5h))
ulapluspalette(14,ulaplushexcolour(0275h))
ulapluspalette(15,ulaplushexcolour(0576h))
rem palette based on http://www.colourlovers.com/palette/1774403

ink 1
postscriptread(@hamletdrawing)

ink 2:
print64x32at(17,1,"I'm just trying to have ideas for a game,")
print64x32at(17,2,"but csscgc2012 did not propose any theme.")
print64x32at(17,3,"So, i think choosing among these ones:")

ink 3:
print64x32at(17,29,"A: Cow")
print64x32at(17,30,"B: Elephant")

hamletloop:
pause 0
a$=inkey$
if a$="a" then: goto cowpage:end if
if a$="b" then: goto elephantpage:end if
goto hamletloop




rem ---------------------------------------------------------------------------------------------
cowpage:

cls
ulapluspalette(1,ulaplushexcolour(0889h))
ulapluspalette(2,ulaplushexcolour(09E5h))
ulapluspalette(3,ulaplushexcolour(08FEh))
ulapluspalette(14,ulaplushexcolour(08DFh))
ulapluspalette(15,ulaplushexcolour(06BFh))
rem palette based on http://www.colourlovers.com/palette/1579901

ink 1
postscriptread(@cowdrawing)

ink 2:
print64x32at(2,1,"Cow cannot be a theme because it were used on csscgc2011,")
print64x32at(2,2,"by Mojon Twins")

ink 3:
print64x32at(2,29,"A: Horse")
print64x32at(2,30,"B: Bull")

cowloop:
pause 0
a$=inkey$
if a$="a" then: goto horsepage:end if
if a$="b" then: goto bullpage:end if
goto cowloop




rem ---------------------------------------------------------------------------------------------
horsepage:

cls
ulapluspalette(1,ulaplushexcolour(0830h))
ulapluspalette(2,ulaplushexcolour(0158h))
ulapluspalette(3,ulaplushexcolour(0EEDh))
ulapluspalette(14,ulaplushexcolour(0111h))
ulapluspalette(15,ulaplushexcolour(0520h))
rem palette based on http://www.colourlovers.com/palette/2026000

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@horsedrawing)

ink 2:
print64x32at(2,1,"too porn... ")
print64x32at(2,2,"i'm affraid that a game based on horse")
print64x32at(2,3,"wouldn't be accepted...")

ink 3:
print64x32at(56,29,"A: Mare")
print64x32at(56,30,"B: Pony")

horseloop:
pause 0
a$=inkey$
if a$="a" then: goto marepage:end if
if a$="b" then: goto ponypage:end if
goto horseloop:




rem ---------------------------------------------------------------------------------------------
marepage:

cls
ulapluspalette(1,ulaplushexcolour(0253h))
ulapluspalette(2,ulaplushexcolour(0AACh))
ulapluspalette(3,ulaplushexcolour(0FEBh))
ulapluspalette(14,ulaplushexcolour(047Bh))
ulapluspalette(15,ulaplushexcolour(0975h))
rem palette based on http://www.colourlovers.com/palette/1255501

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@maredrawing)

ink 2:
print64x32at(2,1,"The same problem as from the horse - too porny... ")
print64x32at(2,2,"(but, of course, this mare looks much more nicer")
print64x32at(2,3,"than that horse! ;p )")

print64x32at(2,5,"going too far... - game over! :D")

ink 3:
print64x32at(2,30,"push any key")

mareloop:
pause 0
a$=inkey$
goto titlepage
goto mareloop




rem ---------------------------------------------------------------------------------------------
bullpage:

cls
ulapluspalette(1,ulaplushexcolour(02ABh))
ulapluspalette(2,ulaplushexcolour(0A84h))
ulapluspalette(3,ulaplushexcolour(0458h))
ulapluspalette(14,ulaplushexcolour(0844h))
ulapluspalette(15,ulaplushexcolour(0A44h))
rem palette based on http://www.colourlovers.com/palette/117013

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@bulldrawing)

ink 2:
print64x32at(2,2,"Well, we must not forget that, still in the 21th century,")
print64x32at(2,3,"there are people and organizations exploring animal abuse")
print64x32at(2,4,"as culture or tradition, in some few countries")
print64x32at(2,5,"around the world, and shamefully, with some")
print64x32at(2,6,"government support.")

print64x32at(2,27,"This would be a great idea for a game")
print64x32at(2,28,"complaining this situation.")

ink 3:
print64x32at(2,30,"push any key")

bullloop:
pause 0
a$=inkey$
goto titlepage
goto bullloop




rem ---------------------------------------------------------------------------------------------
elephantpage:

cls
ulapluspalette(1,ulaplushexcolour(0000h))
ulapluspalette(2,ulaplushexcolour(0022h))
ulapluspalette(3,ulaplushexcolour(0432h))
ulapluspalette(14,ulaplushexcolour(0854h))
ulapluspalette(15,ulaplushexcolour(0DA7h))
rem palette based on http://www.colourlovers.com/palette/794598

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@elephantdrawing)

ink 2:
print64x32at(2,2,"A game against animals in circus can be an excellent idea anyway...")

ink 3:
print64x32at(53,29,"A: Narwhal")
print64x32at(53,30,"B: Turtle")

elephantloop:
pause 0
a$=inkey$
if a$="a" then: goto narwhalpage:end if
if a$="b" then: goto turtlepage:end if
goto elephantloop




rem ---------------------------------------------------------------------------------------------
narwhalpage:

cls
ulapluspalette(1,ulaplushexcolour(0202h))
ulapluspalette(2,ulaplushexcolour(0222h))
ulapluspalette(3,ulaplushexcolour(0022h))
ulapluspalette(14,ulaplushexcolour(0044h))
ulapluspalette(15,ulaplushexcolour(0499h))
rem palette based on http://www.colourlovers.com/palette/356533

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@narwhaldrawing)

ink 2:
print64x32at(2,2,"Lorem ipsum dolor sit amet...")

ink 3:
print64x32at(2,30,"push any key")

narwhalloop:
pause 0
a$=inkey$
goto titlepage
goto narwhalloop




rem ---------------------------------------------------------------------------------------------
ponypage:

cls
ulapluspalette(1,ulaplushexcolour(08FCh))
ulapluspalette(2,ulaplushexcolour(0FA7h))
ulapluspalette(3,ulaplushexcolour(096Dh))
ulapluspalette(14,ulaplushexcolour(07CEh))
ulapluspalette(15,ulaplushexcolour(0FFBh))
rem palette based on http://www.colourlovers.com/palette/1198396

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@ponydrawing)

ink 2:
print64x32at(2,2,"Too gay...")

ink 3:
print64x32at(52,29,"A: Mare")
print64x32at(52,30,"B: Unicorn")

ponyloop:
pause 0
a$=inkey$
if a$="a" then: goto marepage:end if
if a$="b" then: goto narwhalpage:end if
goto ponyloop




rem ---------------------------------------------------------------------------------------------
turtlepage:

cls
ulapluspalette(1,ulaplushexcolour(0210h))
ulapluspalette(2,ulaplushexcolour(0CDBh))
ulapluspalette(3,ulaplushexcolour(0E32h))
ulapluspalette(14,ulaplushexcolour(0424h))
ulapluspalette(15,ulaplushexcolour(0467h))
rem palette based on http://www.colourlovers.com/palette/145164

paper 7:border 6:bright 0:flash 0:cls

ink 1
postscriptread(@turtledrawing)

ink 2:
print64x32at(2,2,"Turtles are also in risk of extintion... game over again...")

ink 3:
print64x32at(2,30,"push any key")

turtleloop:
pause 0
a$=inkey$
goto titlepage
goto turtleloop




rem ---------------------------------------------------------------------------------------------
endofcode:




