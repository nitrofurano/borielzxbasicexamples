goto hamletdrawingend

rem based on picture from http://en.wikipedia.org/wiki/File:Waldkirch_Oberwil.jpg

hamletdrawing:
asm
  defb "m"
  defb 0FFh
  defb  79,108
  defb "l"
  defb 0FFh
  defb  85,99,91,107,114,107,108,99,84,99

  defb "m"
  defb 0FFh
  defb  135,105
  defb "l"
  defb 0FFh
  defb  124,105,122,99

  defb "m"
  defb 0FFh
  defb  120,104
  defb "l"
  defb 0FFh
  defb  122,101,136,99

  defb "m"
  defb 0FFh
  defb  147,106
  defb "l"
  defb 0FFh
  defb  155,98,162,106

  defb "m"
  defb 0FFh
  defb  179,101
  defb "l"
  defb 0FFh
  defb  176,98,155,98

  defb "m"
  defb 0FFh
  defb  199,107
  defb "l"
  defb 0FFh
  defb  193,101,188,109

  defb "m"
  defb 0FFh
  defb  175,101
  defb "l"
  defb 0FFh
  defb  192,102

  defb "m"
  defb 0FFh
  defb  213,113
  defb "l"
  defb 0FFh
  defb  216,110,209,103,200,106,197,110

  defb "m"
  defb 0FFh
  defb  199,114
  defb "l"
  defb 0FFh
  defb  197,111,193,111,193,114,193,114

  defb "m"
  defb 0FFh
  defb  158,114
  defb "l"
  defb 0FFh
  defb  157,109,162,109,164,105,171,107,174,106,178,108,183,105,190,111

  defb "m"
  defb 0FFh
  defb  148,114
  defb "l"
  defb 0FFh
  defb  149,108,142,98,134,102,134,105,130,107,119,104,116,107,117,113

  defb "m"
  defb 0FFh
  defb  111,111
  defb "l"
  defb 0FFh
  defb  111,107

  defb "m"
  defb 0FFh
  defb  114,115
  defb "l"
  defb 0FFh
  defb  110,110,106,113,101,109,93,109,90,114,90,107

  defb "m"
  defb 0FFh
  defb  82,115
  defb "l"
  defb 0FFh
  defb  80,108,77,109,77,102,70,101,66,104,64,107,60,112,55,113,57,118

  defb "m"
  defb 0FFh
  defb  13,115
  defb "l"
  defb 0FFh
  defb  34,112,52,110,60,110

  defb "m"
  defb 0FFh
  defb  35,111
  defb "l"
  defb 0FFh
  defb  37,105,44,105,48,106,50,99,56,103,64,101,68,102

  defb "m"
  defb 0FFh
  defb  166,97
  defb "l"
  defb 0FFh
  defb  163,93,169,92,172,89,172,94,170,97

  defb "m"
  defb 0FFh
  defb  189,101
  defb "l"
  defb 0FFh
  defb  190,95,184,93,178,94,183,85,193,84,194,88,210,87,226,83,243,85,255,89

  defb "m"
  defb 0FFh
  defb  194,102
  defb "l"
  defb 0FFh
  defb  194,95,200,93,199,102,203,93,206,102,209,99

  defb "m"
  defb 0FFh
  defb  214,96
  defb "l"
  defb 0FFh
  defb  215,91,210,91,214,96

  defb "m"
  defb 0FFh
  defb  195,102
  defb "l"
  defb 0FFh
  defb  221,104,255,106

  defb "m"
  defb 0FFh
  defb  171,116
  defb "l"
  defb 0FFh
  defb  206,116,236,119,252,121

  defb "m"
  defb 0FFh
  defb  70,119
  defb "l"
  defb 0FFh
  defb  94,116,127,115

  defb "m"
  defb 0FFh
  defb  2,130
  defb "l"
  defb 0FFh
  defb  44,130,118,129

  defb "m"
  defb 0FFh
  defb  45,138
  defb "l"
  defb 0FFh
  defb  76,137,139,140,197,142,253,136

  defb "m"
  defb 0FFh
  defb  0,41
  defb "l"
  defb 0FFh
  defb  4,44,4,40,10,45,17,42,19,45,11,48,14,52,24,54,33,43,39,48,35,50,41,54,28,53,24,58,15,57,22,62,27,58,31,64,39,61,41,67,30,66,31,71,21,67,40,77,38,85,50,82,51,88,39,87,32,78,25,71,16,73,28,82,17,83,21,89,27,85,31,90,33,84,35,91,33,92,35,98,37,94,45,101,29,99,29,93,22,95,27,101,20,100,25,105,32,104,35,110,28,115,18,106,18,114,11,110,16,121,15,130,11,127,6,132,0,125,6,122,3,117,1,117

  defb "m"
  defb 0FFh
  defb  16,83
  defb "l"
  defb 0FFh
  defb  20,93,17,95,15,86,11,93,52,87,10,81 

  defb "m"
  defb 0FFh
  defb  15,52
  defb "l"
  defb 0FFh
  defb  21,30,28,14,45,5,33,4,48,0,50,4,57,0

  defb "m"
  defb 0FFh
  defb  31,0
  defb "l"
  defb 0FFh
  defb  30,6,20,0,24,6,14,4,26,11,26,17

  defb "m"
  defb 0FFh
  defb  17,52
  defb "l"
  defb 0FFh
  defb  29,37,42,29,40,27,33,29,34,25,44,19,30,22,21,22,26,29,16,27,25,31,26,35,26,39

  defb "m"
  defb 0FFh
  defb  52,153
  defb "l"
  defb 0FFh
  defb  59,155,132,163

  defb "m"
  defb 0FFh
  defb  145,102
  defb "l"
  defb 0FFh
  defb  145,97,150,98,151,102

  defb "m"
  defb 0FFh
  defb  111,103
  defb "l"
  defb 0FFh
  defb  120,103

  defb "m"
  defb 0FFh
  defb  122,113
  defb "l"
  defb 0FFh
  defb  145,113

  defb "m"
  defb 0FFh
  defb  150,113
  defb "l"
  defb 0FFh
  defb  157,113

  defb "m"
  defb 0FFh
  defb  59,117
  defb "l"
  defb 0FFh
  defb  34,121,14,128

  defb "m"
  defb 0FFh
  defb  63,115
  defb "l"
  defb 0FFh
  defb  75,116

  defb "z"
  defb 0FFh
  
  end asm
  
hamletdrawingend:

