goto titledrawingend

titledrawing:
asm

  ;l
  defb "m"
  defb 0FFh
  defb 26,79
  defb "l"
  defb 0FFh
  defb 18,78,18,76,21,77,21,69,18,69,18,67,22,67,23,77,26,77,26,79

  ;o
  defb "m"
  defb 0FFh
  defb 34,79
  defb "l"
  defb 0FFh
  defb 31,79,28,77,29,72,31,71,35,73,36,75,36,78,34,79
  defb "m"
  defb 0FFh
  defb 32,78
  defb "l"
  defb 0FFh
  defb 30,76,30,73,32,73,34,74,34,76,32,78

  ;o
  defb "m"
  defb 0FFh
  defb 42,80
  defb "l"
  defb 0FFh
  defb 40,78,38,75,39,72,42,71,44,72,46,74,45,78,42,80
  defb "m"
  defb 0FFh
  defb 43,77
  defb "l"
  defb 0FFh
  defb 40,76,40,73,43,72,44,75,43,77

  ;k
  defb "m"
  defb 0FFh
  defb 51,79
  defb "l"
  defb 0FFh
  defb 48,79,48,76,49,76,49,69,48,69,48,67,50,67,51,74,53,72,51,71,52,70,57,70,57,72,55,73,53,74,55,76,57,77,57,79,53,79,53,77,55,77,52,75,51,76,51,77,52,77,51,79

  ;i
  defb "m"
  defb 0FFh
  defb 66,78
  defb "l"
  defb 0FFh
  defb 59,79,59,76,61,77,61,72,60,72,60,70,63,71,63,77,66,77 
  defb "m"
  defb 0FFh
  defb 63,69
  defb "l"
  defb 0FFh
  defb 62,69,61,67,63,67,63,69

  defb "m"
  defb 0FFh
  defb 68,70
  defb "l"
  defb 0FFh
  defb 68,72,70,72,69,77,68,77,68,78,73,78,72,76,71,76,70,73,72,72,74,71,75,76,73,76,73,78,77,78,77,76,75,76,75,72,74,70,73,70,71,71,70,70,68,70

  defb "m"
  defb 0FFh
  defb 86,73
  defb "l"
  defb 0FFh
  defb 85,72,86,71,85,71,85,73,84,75,83,76,81,75,80,76,80,77,83,76,85,77,87,78,87,81,84,81,81,81,79,80,79,78,79,76,80,75,79,74,80,72,82,71,84,71,85,70,87,71,87,72,86,73


  defb "m"
  defb 0FFh
  defb 83,75
  defb "l"
  defb 0FFh
  defb 81,74,81,72,82,71,84,73,84,74,83,75

  defb "m"
  defb 0FFh
  defb 80,79
  defb "l"
  defb 0FFh
  defb 81,78,84,77,86,78,86,80,82,81,80,79

  defb "m"
  defb 0FFh
  defb 106,79
  defb "l"
  defb 0FFh
  defb 98,78,99,76,101,76,101,72,100,72,100,70,102,70,102,68,104,67,107,68,108,70,106,70,106,68,104,68,104,70,105,70,104,72,103,72,103,76,106,76,106,79

  defb "m"
  defb 0FFh
  defb 114,79
  defb "l"
  defb 0FFh
  defb 110,77,110,72,113,71,116,73,117,76,116,79,114,79

  defb "m"
  defb 0FFh
  defb 113,77
  defb "l"
  defb 0FFh
  defb 111,75,112,73,114,73,116,75,113,77

  defb "m"
  defb 0FFh
  defb 120,78
  defb "l"
  defb 0FFh
  defb 126,79,127,77,123,77,124,74,126,72,126,74,128,75,128,71,126,70,123,72,123,71,120,71,120,72,122,72,122,77,120,76,120,78

  defb "m"
  defb 0FFh
  defb 144,79
  defb "l"
  defb 0FFh
  defb 140,77,141,75,144,74,146,73,144,72,143,72,143,73,141,73,141,71,143,70,147,72,148,79,146,78 

  defb "m"
  defb 0FFh
  defb 144,77
  defb "l"
  defb 0FFh
  defb 142,77,144,75,146,75,146,76 

  defb "z"
  defb 0FFh

  end asm

titledrawingp2:
asm


  ;c
  defb "m"
  defb 0FFh
  defb 24,98
  defb "l"
  defb 0FFh
  defb 27,98,24,104,18,107,11,102,8,95,11,88,15,86,22,86,25,91,24,94,20,92,21,89,19,88,15,89,11,94,12,98,14,102,19,102,22,102,24,98

  defb "m"
  defb 0FFh
  defb 34,107
  defb "l"
  defb 0FFh
  defb 34,99,37,99,37,102,42,104,46,104,47,101,43,97,37,97,35,93,35,88,42,86,47,89,47,86,49,87,49,94,46,93,47,90,44,89,39,89,37,91,38,94,44,95,48,97,50,103,47,107,40,107,36,104,34,107

  defb "m"
  defb 0FFh
  defb 58,107
  defb "l"
  defb 0FFh
  defb 58,98,60,99,60,102,64,104,69,105,72,102,70,98,62,97,59,94,59,89,65,86,71,88,71,86,73,87,73,95,70,94,71,90,67,89,62,89,61,93,63,94,71,96,74,99,73,104,69,108,64,106,60,105,58,107

  defb "m"
  defb 0FFh
  defb 94,89
  defb "l"
  defb 0FFh
  defb 94,93,97,94,100,91,95,86,88,86,83,91,82,97,84,103,89,106,95,105,98,102,100,98,97,97,96,101,91,103,87,100,84,95,87,90,91,88,94,89

  defb "m"
  defb 0FFh
  defb 113,87
  defb "l"
  defb 0FFh
  defb 107,89,107,93,108,97,105,99,107,104,105,108,108,113,117,114,124,112,124,105,122,103,117,101,109,103,108,99,111,98,112,101,119,98,120,94,119,90,121,89,122,92,125,92,125,88,121,86,117,89,113,87

  defb "m"
  defb 0FFh
  defb 109,91
  defb "l"
  defb 0FFh
  defb 113,88,116,90,117,93,115,98,111,97,109,94,109,91

  defb "m"
  defb 0FFh
  defb 133,89
  defb "l"
  defb 0FFh
  defb 131,96,133,103,138,104,144,105,148,103,148,98,146,98,144,102,139,103,135,99,133,96,135,91,139,89,143,89,142,92,145,93,148,91,146,88,141,85,136,86,133,89

  ; g
  defb "m"
  defb 0FFh
  defb 107,109
  defb "l"
  defb 0FFh
  defb 109,105,114,104,120,105,123,107,122,111,116,112,110,111,107,109

  defb "m"
  defb 0FFh
  defb 173,106
  defb "l"
  defb 0FFh
  defb 154,106,157,96,166,93,170,89,170,86,167,82,160,81,155,88,153,86,156,82,164,79,170,82,173,87,170,93,163,97,158,101,156,104,169,104,174,103,173,106

  defb "m"
  defb 0FFh
  defb 187,106
  defb "l"
  defb 0FFh
  defb 180,104,178,93,182,82,189,80,196,83,198,93,197,102,193,108,187,106

  defb "m"
  defb 0FFh
  defb 191,104
  defb "l"
  defb 0FFh
  defb 184,103,181,95,182,84,189,83,195,88,194,97,191,104

  defb "m"
  defb 0FFh
  defb 204,107
  defb "l"
  defb 0FFh
  defb 211,106,221,108,222,104,214,103,213,79,204,79,205,83,210,83,210,104,203,104,204,107

  defb "m"
  defb 0FFh
  defb 227,107
  defb "l"
  defb 0FFh
  defb 247,106,247,103,230,104,231,97,237,96,244,92,247,88,243,81,238,78,231,80,227,86,229,87,233,83,238,81,242,84,243,89,239,92,231,96,228,101,227,107


  defb "z"
  defb 0FFh

  end asm

titledrawingp3:
asm






  defb "m"
  defb 0FFh
  defb 145,114
  defb "l"
  defb 0FFh
  defb 146,114,146,117,150,117,150,118,146,118,146,122,147,124,149,123,149,121,150,121,150,123,148,125,146,124,145,123,145,118,144,118,144,116,145,117 

  defb "m"
  defb 0FFh
  defb 152,113
  defb "l"
  defb 0FFh
  defb 152,114,154,115,154,123,152,123,152,124,156,124,156,123,155,123,155,119,157,117,158,118,159,120,159,123,157,123,157,124,162,124,162,123,160,123,160,118,159,116,157,116,155,117,155,113 

  defb "m"
  defb 0FFh
  defb 171,122
  defb "l"
  defb 0FFh
  defb 167,124,164,122,164,118,168,115,171,118,171,121,165,120,166,122,168,123,171,121 

  defb "m"
  defb 0FFh
  defb 166,119
  defb "l"
  defb 0FFh
  defb 170,120,169,117,167,117 

  defb "m"
  defb 0FFh
  defb 173,116
  defb "l"
  defb 0FFh
  defb 172,118,173,118,174,122,173,123,173,124,175,124,176,122,175,121,175,117,176,117,176,122,176,123,176,124,179,124,179,123,178,122,178,119,179,117,180,118,180,122,179,123,179,124,182,124,182,123,181,122,181,117,179,116,177,117,177,116,174,116 

  defb "m"
  defb 0FFh
  defb 190,123
  defb "l"
  defb 0FFh
  defb 188,124,185,124,184,120,186,116,189,116,191,118,192,120,185,120,186,122,188,123,190,122 

  defb "m"
  defb 0FFh
  defb 189,119
  defb "l"
  defb 0FFh
  defb 186,119,187,117,189,117


  defb "z"
  defb 0FFh
  
  end asm
  
titledrawingend:

