goto bulldrawingend

rem based on picture from http://en.wikipedia.org/wiki/File:Taureau_charolais_au_pr%C3%A9.jpg

bulldrawing:
asm

  defb "m"
  defb 0FFh
  defb 51,85
  defb "l"
  defb 0FFh
  defb 52,81,55,82,51,85

  defb "m"
  defb 0FFh
  defb 42,98
  defb "l"
  defb 0FFh
  defb 40,103,35,102,42,98

  defb "m"
  defb 0FFh
  defb 30,98
  defb "l"
  defb 0FFh
  defb 32,102,30,104

  defb "m"
  defb 0FFh
  defb 32,69
  defb "l"
  defb 0FFh
  defb 24,70,19,67,19,66,26,65,32,64,35,64

  defb "m"
  defb 0FFh
  defb 31,93
  defb "l"
  defb 0FFh
  defb 29,83,31,72,35,64,40,62,54,62,62,65,70,68,70,71,63,71,57,70

  defb "m"
  defb 0FFh
  defb 61,83
  defb "l"
  defb 0FFh
  defb 67,83,74,78,74,74,68,70,65,72,60,74

  defb "m"
  defb 0FFh
  defb 70,75
  defb "l"
  defb 0FFh
  defb 68,78,63,79

  defb "m"
  defb 0FFh
  defb 30,73
  defb "l"
  defb 0FFh
  defb 21,73,15,76,14,80,17,84,24,84,29,84

  defb "m"
  defb 0FFh
  defb 24,82
  defb "l"
  defb 0FFh
  defb 29,81,24,77,19,76,17,79

  defb "m"
  defb 0FFh
  defb 32,89
  defb "l"
  defb 0FFh
  defb 28,97,29,108,34,108,38,112,46,110,52,108,57,106,60,99,62,90,62,90,62,90

  defb "m"
  defb 0FFh
  defb 52,61
  defb "l"
  defb 0FFh
  defb 61,56,69,51,80,50,86,54,98,53,114,56,132,57,138,58,178,56,185,53,205,54,214,52,223,55,232,68,238,82,238,104,241,124,243,134,242,151

  defb "m"
  defb 0FFh
  defb 232,75
  defb "l"
  defb 0FFh
  defb 235,85,234,103,229,115,221,123,222,141,215,152

  defb "m"
  defb 0FFh
  defb 121,98
  defb "l"
  defb 0FFh
  defb 121,111,115,122,113,129,111,143

  defb "m"
  defb 0FFh
  defb 235,93
  defb "l"
  defb 0FFh
  defb 235,119,235,137,236,146

  defb "m"
  defb 0FFh
  defb 235,130
  defb "l"
  defb 0FFh
  defb 229,122,232,116,235,114

  defb "m"
  defb 0FFh
  defb 222,136
  defb "l"
  defb 0FFh
  defb 230,153

  defb "m"
  defb 0FFh
  defb 202,152
  defb "l"
  defb 0FFh
  defb 202,139,197,129,197,116,189,110,183,111,172,120,169,127

  defb "m"
  defb 0FFh
  defb 203,143
  defb "l"
  defb 0FFh
  defb 200,149,193,138,196,129,195,123,195,123

  defb "m"
  defb 0FFh
  defb 113,136
  defb "l"
  defb 0FFh
  defb 125,134,130,139,137,137,147,141,154,145,155,150,160,150,164,145,169,141,174,140,183,132,193,124

  defb "m"
  defb 0FFh
  defb 89,110
  defb "l"
  defb 0FFh
  defb 88,123,93,132,92,144,94,151

  defb "m"
  defb 0FFh
  defb 93,142
  defb "l"
  defb 0FFh
  defb 84,145,78,142,72,143,67,137,62,127,51,118,43,117,38,111

  defb "m"
  defb 0FFh
  defb 94,68
  defb "l"
  defb 0FFh
  defb 90,78,87,95,84,101,85,106

  defb "m"
  defb 0FFh
  defb 53,113
  defb "l"
  defb 0FFh
  defb 64,122,66,127

  defb "m"
  defb 0FFh
  defb 121,131
  defb "l"
  defb 0FFh
  defb 138,131,153,136,168,134

  defb "m"
  defb 0FFh
  defb 0,27
  defb "l"
  defb 0FFh
  defb 40,27,115,25,209,23,223,20,234,23,249,21,255,22

  defb "m"
  defb 0FFh
  defb 31,27
  defb "l"
  defb 0FFh
  defb 39,21,47,25,56,19,64,24,67,21,63,17,67,11,65,3,60,4,63,0

  defb "m"
  defb 0FFh
  defb 58,0
  defb "l"
  defb 0FFh
  defb 55,2,50,0

  defb "m"
  defb 0FFh
  defb 48,12
  defb "l"
  defb 0FFh
  defb 44,14,42,8

  defb "m"
  defb 0FFh
  defb 15,11
  defb "l"
  defb 0FFh
  defb 18,6,24,8

  defb "m"
  defb 0FFh
  defb 30,81
  defb "l"
  defb 0FFh
  defb 31,84,29,84

  defb "z"
  defb 0FFh
  
  end asm
  
bulldrawingend:

