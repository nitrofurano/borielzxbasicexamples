goto horsedrawingend

rem based on picture from http://ast.wikipedia.org/wiki/Archivu:Bridon_Belfrey,_RID,_Irish_Draught_Stallion.jpg

horsedrawing:
asm

  defb "m"
  defb 0FFh
  defb  224,14
  defb "l"
  defb 0FFh
  defb  234,3,230,17

  defb "m"
  defb 0FFh
  defb  210,46
  defb "l"
  defb 0FFh
  defb  230,53,237,58,241,63,251,63,255,53,248,41,240,30,238,20,231,13

  defb "m"
  defb 0FFh
  defb  146,103
  defb "l"
  defb 0FFh
  defb  150,130,147,137,149,143,147,168,151,170,154,176,152,180,158,181,167,181,166,177,162,172,157,165,157,145,159,139,161,130,170,104,176,99,183,87,186,74,195,60,206,49,214,44

  defb "m"
  defb 0FFh
  defb  16,124
  defb "l"
  defb 0FFh
  defb  15,95,17,81,18,64,27,50,48,41,64,40,78,43,103,50,119,48,137,42,155,30,179,17,201,12,216,10,225,12

  defb "m"
  defb 0FFh
  defb  41,105
  defb "l"
  defb 0FFh
  defb  71,78,129,145,102,169,41,105

  defb "m"
  defb 0FFh
  defb  149,142
  defb "l"
  defb 0FFh
  defb  146,148,147,170,152,176,152,179,143,181,138,177,141,172,136,164,144,133,145,130,140,107

  defb "m"
  defb 0FFh
  defb  94,105
  defb "l"
  defb 0FFh
  defb  108,108,131,108,144,107

  defb "m"
  defb 0FFh
  defb  55,120
  defb "l"
  defb 0FFh
  defb  50,139,49,155,55,169,60,174,65,182,56,184,48,182,50,175,44,172,42,160,37,141,38,124

  defb "m"
  defb 0FFh
  defb  29,54
  defb "l"
  defb 0FFh
  defb  25,68,25,81,29,98,28,107,16,125,15,137,7,167,10,172,9,180,7,184,12,187,23,187,19,178,18,168,22,144,33,128,48,113,48,113,48,113

  defb "m"
  defb 0FFh
  defb  35,126
  defb "l"
  defb 0FFh
  defb  31,148,33,158,25,153,25,158,21,151

  defb "m"
  defb 0FFh
  defb  233,28
  defb "l"
  defb 0FFh
  defb  238,29,239,32,236,30

  defb "m"
  defb 0FFh
  defb  218,12
  defb "l"
  defb 0FFh
  defb  222,27,227,35,240,47,233,55,236,56,241,49,248,43,247,40,243,44,226,28,226,22,221,15

  defb "m"
  defb 0FFh
  defb  65,98
  defb "l"
  defb 0FFh
  defb  66,97,64,94,60,96,60,100,64,102

  defb "m"
  defb 0FFh
  defb  70,109
  defb "l"
  defb 0FFh
  defb  66,107,66,105,69,103,72,104,72,106,72,107,68,103

  defb "m"
  defb 0FFh
  defb  70,111
  defb "l"
  defb 0FFh
  defb  73,114

  defb "m"
  defb 0FFh
  defb  72,112
  defb "l"
  defb 0FFh
  defb  76,109,76,107

  defb "m"
  defb 0FFh
  defb  76,118
  defb "l"
  defb 0FFh
  defb  74,115

  defb "m"
  defb 0FFh
  defb  75,116
  defb "l"
  defb 0FFh
  defb  78,113,79,111,77,110,74,110

  defb "m"
  defb 0FFh
  defb  77,121
  defb "l"
  defb 0FFh
  defb  79,119,79,122,81,123,83,123,82,120,81,117,82,116,84,118,84,121,86,120

  defb "m"
  defb 0FFh
  defb  86,125
  defb "l"
  defb 0FFh
  defb  88,124,91,126,91,128,89,131,86,131,84,128,86,125

  defb "m"
  defb 0FFh
  defb  90,134
  defb "l"
  defb 0FFh
  defb  94,139

  defb "m"
  defb 0FFh
  defb  92,136
  defb "l"
  defb 0FFh
  defb  96,132,95,130

  defb "m"
  defb 0FFh
  defb  97,136
  defb "l"
  defb 0FFh
  defb  99,135,98,133,95,133

  defb "m"
  defb 0FFh
  defb  102,144
  defb "l"
  defb 0FFh
  defb  99,144,98,142,99,139,103,138,105,140,104,143,99,139

  defb "m"
  defb 0FFh
  defb  109,151
  defb "l"
  defb 0FFh
  defb  106,150,104,148,106,145,109,145,111,146,111,148

  defb "m"
  defb 0FFh
  defb  108,153
  defb "l"
  defb 0FFh
  defb  107,152,114,146,112,144

  defb "m"
  defb 0FFh
  defb  254,53
  defb "l"
  defb 0FFh
  defb  251,52,252,58



  defb "z"
  defb 0FFh
  
  end asm
  
horsedrawingend:

