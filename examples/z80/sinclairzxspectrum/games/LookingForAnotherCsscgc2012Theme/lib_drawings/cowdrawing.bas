goto cowdrawingend

rem based on picture from http://en.wikipedia.org/wiki/File:CH_cow_2.jpg

cowdrawing:
asm
  defb "m"
  defb 0FFh
  defb 192,85
  defb "l"
  defb 0FFh
  defb 193,78,196,73,196,64,189,57,180,44,172,39,166,39,156,48,149,53,139,54,127,53,120,54,120,62,129,69,140,71,149,71,154,68,154,68

  defb "m"
  defb 0FFh
  defb 171,75
  defb "l"
  defb 0FFh
  defb 167,72,166,70,163,66,159,67,163,72,166,73

  defb "m"
  defb 0FFh
  defb 149,76
  defb "l"
  defb 0FFh
  defb 142,81,138,89,133,88,133,93,140,95,150,91

  defb "m"
  defb 0FFh
  defb 149,80
  defb "l"
  defb 0FFh
  defb 148,88,151,96,157,106,168,115,177,116,180,116,201,115,202,111,202,102,197,96,193,90,193,86,200,86,209,80,214,78,215,72,211,65,205,61,194,62

  defb "m"
  defb 0FFh
  defb 81,139
  defb "l"
  defb 0FFh
  defb 71,141,53,139,44,130,37,120,38,105,50,91,66,76,72,72,83,70,96,64,110,60,120,61,121,61

  defb "m"
  defb 0FFh
  defb 144,94
  defb "l"
  defb 0FFh
  defb 148,102,152,99,145,105,151,117,156,130,161,132,162,126,157,106

  defb "m"
  defb 0FFh
  defb 171,117
  defb "l"
  defb 0FFh
  defb 173,127,169,126,176,133,178,141,168,143,157,150,147,155,154,145,158,133

  defb "m"
  defb 0FFh
  defb 99,133
  defb "l"
  defb 0FFh
  defb 111,134,125,142,134,151,138,154,136,161,124,164,110,158,96,155,84,150,81,139,83,128,86,120

  defb "m"
  defb 0FFh
  defb 154,128
  defb "l"
  defb 0FFh
  defb 143,142,141,148,133,148,125,142

  defb "m"
  defb 0FFh
  defb 139,155
  defb "l"
  defb 0FFh
  defb 147,153

  defb "m"
  defb 0FFh
  defb 124,62
  defb "l"
  defb 0FFh
  defb 126,57,132,58,133,65,137,69

  defb "m"
  defb 0FFh
  defb 175,114
  defb "l"
  defb 0FFh
  defb 175,106,185,97,195,96

  defb "m"
  defb 0FFh
  defb 190,113
  defb "l"
  defb 0FFh
  defb 184,106,187,101,194,100,201,102

  defb "m"
  defb 0FFh
  defb 193,110
  defb "l"
  defb 0FFh
  defb 188,108,187,103,192,106,192,106

  defb "m"
  defb 0FFh
  defb 201,111
  defb "l"
  defb 0FFh
  defb 199,108,201,106

  defb "m"
  defb 0FFh
  defb 193,76
  defb "l"
  defb 0FFh
  defb 193,72,196,71

  defb "m"
  defb 0FFh
  defb 0,13
  defb "l"
  defb 0FFh
  defb 13,24,45,48,71,70

  defb "m"
  defb 0FFh
  defb 180,45
  defb "l"
  defb 0FFh
  defb 197,35,208,29,218,17,239,0

  defb "m"
  defb 0FFh
  defb 43,45
  defb "l"
  defb 0FFh
  defb 54,44,65,51,88,40,93,48,103,45,126,28,131,28,135,24,147,35,168,25,178,23,184,16,191,14,193,6,197,0

  defb "m"
  defb 0FFh
  defb 255,89
  defb "l"
  defb 0FFh
  defb 213,92,194,89

  defb "m"
  defb 0FFh
  defb 104,128
  defb "l"
  defb 0FFh
  defb 112,121,125,125

  defb "m"
  defb 0FFh
  defb 139,134
  defb "l"
  defb 0FFh
  defb 139,119,144,113

  defb "m"
  defb 0FFh
  defb 163,120
  defb "l"
  defb 0FFh
  defb 170,127,163,130

  defb "z"
  defb 0FFh
  end asm
  
cowdrawingend:

