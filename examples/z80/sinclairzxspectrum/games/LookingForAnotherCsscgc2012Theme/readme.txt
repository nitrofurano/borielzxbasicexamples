Looking for a csscgc2012 theme
from Paulo Silva, february-march 2012, gpl license

This is a mere 'illustrated adventure' game, like those 'Aventura A o B' (Spectraventura)

It uses ULAplus feature (but it seems to working fine on machines doensn't support it), 4x6 characters and postscript-like drawings

These postscript drawings were hand-traced on Inkscape, mostly from pictures found at wikipedia. The .svg files were converted to .bas code on Gedit by hand, with 'find/replace' (a bit tedious, and coding a converter just for it didn't justify).

The colour palettes were based on some found randomly from colourlovers.com

This game were compiled on zxbasic compiler, from Boriel ( zxb.py -tBa filename.bas )

What makes this game crappy:
- sometimes it is unreadable
- sometimes the colour palette is not the best
- may look ugly on machines doesn't has ulaplus
- the content of the game may feel silly, nonsense, or unadictive
- the text drawing is slow

