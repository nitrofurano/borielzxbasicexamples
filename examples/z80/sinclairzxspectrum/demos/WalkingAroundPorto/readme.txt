Walking Around Porto
Paulo Silva, for CSSCGC2013 - GPL license

A Myst-like ( http://en.wikipedia.org/wiki/Myst ) "game" for ZX-Spectrum 128k, optimized for b&w display, coded on Boriel's ZX-Basic Compiler.

Due on some laziness an some lack of a photo camera, this game uses screenshots from Google Street View, as fair use.
(would be awesome if Google make Maps (see OpenStreetMap...) and Street View as collaborative and CC-NC-SA, so we wouldn't need to worry about this... ( http://www.google.com/permissions/geoguidelines.html ) - but i think there is no problems at all, the pictures are not that many, and they are small and dithered enough)

The first photo used was from Panoramio, and has CC-NC-SA license.

crapness: the game is incomplete, and there are some bugs around! :D

this game was tested only on emulators, FBZX (2.7.0) and Fuse (1.0.0), on Crunchbang GNU/Linux

