#include <print64.bas>
#include <memcopy.bas>
rem zxb.py -t main.bas

sub cpymem(src as uinteger,des as uinteger,lng as uinteger):
  asm
    ld h,(ix+5)
    ld l,(ix+4)
    ld d,(ix+7)
    ld e,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
    ldir
    end asm
  end sub

sub ppoke (tpadr as ulong, tvl as ubyte):
  tpvl=int (tpadr/16384)
  tqadr=tpadr mod 16384
  out 32765,16+(int(tpvl*1.5))
  poke 49152+tqadr,tvl
  end sub

function ppeek(tpadr as ulong):
  tpvl=int (tpadr/16384)
  tqadr=tpadr mod 16384
  out 32765,16+(int(tpvl*1.5))
  return peek(49152+tqadr)
  end function

sub memcpypagedtodisplay(tdest as ulong, tpadr as ulong, tleng as ulong):
  dim tpvl as ulong
  dim tqadr as ulong
  tpvl=int (tpadr/16384)
  tqadr=tpadr mod 16384
  out 32765,16+(int(tpvl*1.5))
  cpymem(49152+tqadr,tdest,tleng)
  end sub

sub transferpicfrompagedmemory(tpic as ubyte):
  tpicadr=tpic*(4096+512)
  'for ti=0 to 4095:poke 16384+ti,ppeek(tpicadr+ti):next
  'for ti=0 to 511:poke 16384+6144+ti,ppeek(tpicadr+4096+ti):next
  '- i am struggling a lot using memcopy library... :S
  for ti=0 to 4095 step 512:memcpypagedtodisplay(16384+ti,tpicadr+ti,512):next
  for ti=0 to 511 step 512:memcpypagedtodisplay(16384+6144+ti,tpicadr+4096+ti,512):next
  end sub

goto patternsend
patterns:
asm
  defb 0FFh,081h,089h,099h,089h,0BDh,081h,0FFh 
  defb 0FFh,081h,099h,085h,099h,0BDh,081h,0FFh
  defb 0FFh,081h,0BDh,099h,085h,0B9h,081h,0FFh
  end asm
patternsend:
poke uinteger 23675,@patterns

border 0:paper 0:ink 7:bright 1:flash 0:over 0:cls

title01:
ink 1:cls:ink 7
out 32765,16+(int(5*1.5))
cpymem(54784,16384,6912)
out 32765,16+(int(0*1.5))
paper 7:ink 0
printat64(15,2):print64(" Walking Around Porto ")
printat64(17,2):print64(" Paulo Silva, Jun 2013, GPL license ")
printat64(18,2):print64(" (using "+chr$(34)+"stolen"+chr$(34)+" (fair use!) screenshots from Google Street    View... ;p ) ")
printat64(20,2):print64(" (better seen on b&w tv sets or monitors) ")
printat64(22,2):print64(" push any key ")
pause 0
paper 0:ink 7:cls
'- 

l01:
ink 1:cls:ink 7:transferpicfrompagedmemory(1-1)
printat64(17,2):print64("Estacao de Sao Bento")
printat64(19,2):print64("1- Avenida dos Aliados")
printat64(20,2):print64("2- Sa da Bandeira")
print at 8,7;paper 7;ink 0;"\A"
print at 8,18;paper 7;ink 0;"\B"
pause 0
if inkey$="1" then goto l02:end if
if inkey$="2" then goto l04:end if
goto l01

l02:
ink 1:cls:ink 7:transferpicfrompagedmemory(2-1)
printat64(17,2):print64("Avenida dos Aliados")
printat64(19,2):print64("1- Praca dos Loios")
printat64(20,2):print64("2- Avenida dos Aliados")
printat64(21,2):print64("3- go back")
print at 6,12;paper 7;ink 0;"\A"
print at 6,22;paper 7;ink 0;"\B"
pause 0
if inkey$="1" then goto l18:end if
if inkey$="2" then goto l03:end if
if inkey$="3" then goto l01:end if
goto l02

l03:
ink 1:cls:ink 7:transferpicfrompagedmemory(3-1)
printat64(17,2):print64("Sa da Bandeira")
printat64(19,2):print64("1- Praca dos Loios")
printat64(20,2):print64("2- Praca da Batalha")
printat64(21,2):print64("3- go back")
print at 10,12;paper 7;ink 0;"\A"
print at 9,24;paper 7;ink 0;"\B"
if inkey$="1" then goto l18:end if
if inkey$="2" then goto l17:end if
if inkey$="3" then goto l01:end if
pause 0
goto l03

l04:
ink 1:cls:ink 7:transferpicfrompagedmemory(4-1)
printat64(17,2):print64("Sa da Bandeira")
printat64(19,2):print64("1- Sa da Bandeira")
printat64(20,2):print64("2- Praca da Batalha")
printat64(21,2):print64("3- go back")
print at 10,12;paper 7;ink 0;"\A"
print at 9,24;paper 7;ink 0;"\B"
pause 0
if inkey$="1" then goto l08:end if
if inkey$="2" then goto l05:end if
if inkey$="3" then goto l01:end if
goto l04

l05:
ink 1:cls:ink 7:transferpicfrompagedmemory(5-1)
printat64(17,2):print64("Santo Idelfonso")
printat64(19,2):print64("1- Santa Catarina")
printat64(20,2):print64("2- Praca da Batalha")
printat64(21,2):print64("3- go back")
print at 11,5;paper 7;ink 0;"\A"
print at 9,26;paper 7;ink 0;"\B"
pause 0
if inkey$="1" then goto l06:end if
if inkey$="2" then goto l09:end if
if inkey$="3" then goto l04:end if
goto l05

l06:
ink 1:cls:ink 7:transferpicfrompagedmemory(6-1)
printat64(17,2):print64("Santa Catarina")
printat64(19,2):print64("1- Rivoli")
printat64(20,2):print64("2- Santa Catarina")
printat64(21,2):print64("3- Coliseu")
printat64(22,2):print64("4- go back")
print at 10,7;paper 7;ink 0;"\A"
print at 8,14;paper 7;ink 0;"\B"
print at 9,23;paper 7;ink 0;"\C"
pause 0
if inkey$="1" then goto l07:end if
if inkey$="3" then goto l05:end if
goto l06

l07:
ink 1:cls:ink 7:transferpicfrompagedmemory(7-1)
printat64(17,2):print64("Rivoli")
printat64(19,2):print64("1- Avenida dos Aliados")
printat64(20,2):print64("2- Sa da Bandeira")
printat64(21,2):print64("3- go back")
print at 8,16;paper 7;ink 0;"\A"
print at 9,26;paper 7;ink 0;"\B"
pause 0
'- 16 or 15
if inkey$="1" then goto l08:end if
if inkey$="2" then goto l05:end if
if inkey$="3" then goto l06:end if
goto l07

l08:
ink 1:cls:ink 7:transferpicfrompagedmemory(8-1)
printat64(17,2):print64("Sa da Bandeira")
printat64(19,2):print64("1- Sa da Bandeira")
printat64(20,2):print64("2- Praca da Batalha")
printat64(21,2):print64("3- go back")
print at 10,12;paper 7;ink 0;"\A"
print at 9,24;paper 7;ink 0;"\B"
pause 0
'- 04 -> 08 -> 14
if inkey$="1" then goto l08:end if
if inkey$="2" then goto l05:end if
if inkey$="3" then goto l01:end if
goto l08

l09:
ink 1:cls:ink 7:transferpicfrompagedmemory(9-1)
printat64(17,2):print64("Praca da Batalha")
printat64(19,2):print64("1- Praca da Batalha")
printat64(20,2):print64("2- go back")
print at 7,15;paper 7;ink 0;"\A"
pause 0
if inkey$="1" then goto l10:end if
if inkey$="2" then goto l05:end if
goto l09

l10:
ink 1:cls:ink 7:transferpicfrompagedmemory(10-1)
printat64(17,2):print64("Praca da Batalha")
printat64(19,2):print64("1- ?")
printat64(20,2):print64("2- go back")
print at 8,21;paper 7;ink 0;"\A"
pause 0
if inkey$="1" then goto l11:end if
if inkey$="2" then goto l09:end if
goto l10

l11:
ink 1:cls:ink 7:transferpicfrompagedmemory(11-1)
printat64(17,2):print64("unknown-11")
printat64(19,2):print64("1- ?")
printat64(20,2):print64("2- go back")
print at 8,25;paper 7;ink 0;"\A"
pause 0
if inkey$="1" then goto l12:end if
if inkey$="2" then goto l10:end if
goto l11

l12:
ink 1:cls:ink 7:transferpicfrompagedmemory(12-1)
printat64(17,2):print64("Catedral")
printat64(19,2):print64("1- ?")
printat64(20,2):print64("2- go back")
print at 9,26;paper 7;ink 0;"\A"
pause 0
if inkey$="1" then goto l13:end if
if inkey$="2" then goto l11:end if
goto l12

l13:
ink 1:cls:ink 7:transferpicfrompagedmemory(13-1)
printat64(17,2):print64("unknown")
printat64(19,2):print64("1- ?")
printat64(20,2):print64("2- go back")
print at 8,20;paper 7;ink 0;"\A"
'- 12 -> 13 -> 01
pause 0
if inkey$="1" then goto l01:end if
if inkey$="2" then goto l12:end if
goto l13

l14:
ink 1:cls:ink 7:transferpicfrompagedmemory(14-1)
printat64(17,2):print64("?14")
printat64(19,2):print64("1- ?")
printat64(20,2):print64("2- ?")
printat64(21,2):print64("3- go back")
print at 10,12;paper 7;ink 0;"\A"
'- 08 -> 14 -> 16 or 15
pause 0
if inkey$="1" then goto l16:end if
if inkey$="2" then goto l15:end if
if inkey$="3" then goto l08:end if
goto l14

l15:
ink 1:cls:ink 7:transferpicfrompagedmemory(15-1)
printat64(17,2):print64("?15")
printat64(19,2):print64("1- go back")
printat64(20,2):print64("2- game over")
pause 0
if inkey$="1" then goto l14:end if
if inkey$="2" then goto title01:end if
goto l15

l16:
ink 1:cls:ink 7:transferpicfrompagedmemory(16-1)
printat64(17,2):print64("?16")
printat64(19,2):print64("1- go back")
printat64(20,2):print64("2- game over")
pause 0
if inkey$="1" then goto l14:end if
if inkey$="2" then goto title01:end if
goto l16

l17:
ink 1:cls:ink 7:transferpicfrompagedmemory(17-1)
printat64(17,2):print64("Avenida dos Aliados")
printat64(19,2):print64("1- go back")
printat64(20,2):print64("2- game over")
pause 0
if inkey$="1" then goto l03:end if
if inkey$="2" then goto title01:end if
goto l17

l18:
ink 1:cls:ink 7:transferpicfrompagedmemory(18-1)
printat64(17,2):print64("Praca dos Loios")
printat64(19,2):print64("1- Torre dos Clerigos")
printat64(20,2):print64("2- go back")
pause 0
if inkey$="1" then goto l19:end if
if inkey$="2" then goto l02:end if
goto l18

l19:
ink 1:cls:ink 7:transferpicfrompagedmemory(19-1)
printat64(17,2):print64("Torre dos Clerigos")
printat64(19,2):print64("1- go back")
printat64(20,2):print64("2- game over")
'- 18 -> 19 -> over
pause 0
if inkey$="1" then goto l18:end if
if inkey$="2" then goto title01:end if
goto l19

l20:
'- 







