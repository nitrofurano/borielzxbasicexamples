#!/bin/bash

rm all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/01.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/01.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/02.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/02.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/03.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/03.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/04.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/04.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/05.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/05.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/06.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/06.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/07.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/07.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/08.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/08.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/09.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/09.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/10.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/10.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/11.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/11.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/12.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/12.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/13.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/13.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/14.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/14.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/15.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/15.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/16.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/16.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/17.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/17.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/18.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/18.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=scr/19.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0200)) skip=$((0x1800)) if=scr/19.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

dd ibs=1 count=$((0x1800)) skip=$((0x0000)) if=scr/20.scr of=_tmp_a.bin
dd ibs=1 count=$((0x0300)) skip=$((0x1800)) if=scr/20.scr of=_tmp_b.bin
cat _tmp_a.bin _tmp_b.bin >> all.bin

#- split all.bin
split -b 16384 all.bin
mv xaa split00.bin
mv xab split01.bin
mv xac split02.bin
mv xad split03.bin
mv xae split04.bin
mv xaf split05.bin

python tools/bin2tap.py split00.bin
python tools/bin2tap.py split01.bin
python tools/bin2tap.py split02.bin
python tools/bin2tap.py split03.bin
python tools/bin2tap.py split04.bin
python tools/bin2tap.py split05.bin

mv split00.bin.tap split00.tap
mv split01.bin.tap split01.tap
mv split02.bin.tap split02.tap
mv split03.bin.tap split03.tap
mv split04.bin.tap split04.tap
mv split05.bin.tap split05.tap

rm _tmp_a.bin _tmp_b.bin

zxb.py -t walkingaroundporto.bas
sleep 5

mv walkingaroundporto.tap main.tap

zmakebas -a 10 library/loader.bas && mv out.tap loader.tap
cat loader.tap split00.tap split01.tap split02.tap split03.tap split04.tap split05.tap main.tap > walkingaroundporto.tap

rm split*.tap loader.tap main.tap *.bin

fbzx walkingaroundporto.tap

