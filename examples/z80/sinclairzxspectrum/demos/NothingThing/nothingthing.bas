#include "library/ulaplus.zxbasic"
#include "library/udgzx81.zxbasic"
#include "library/zx81zxspconv.zxbasic"
#include "library/a01scr.zxbasic"
#include "library/a02scr.zxbasic"
#include "library/a03scr.zxbasic"
#include "library/a04scr.zxbasic"

dim izz as uinteger:ulaplusswitch(1):cls
poke uinteger 23675,@udgzx81 :rem poke uinteger 23606,@typeface: rem zxb.py -tBa nothingthing.zxbasic

loop01:
cls

for izz=0 to 63:ulapluspalette(izz,peek(@a01scr+izz+1536)):next
for izz=0 to 767:poke 22528+izz,(peek(@a01scr+izz+768)):next
for izz=0 to 767:print paper 8;ink 8;bright 8;flash 8;inverse((peek(@a01scr+izz)band 128)/128);chr$(peek(@zx81zxspconv+(peek(@a01scr+izz)band 63)));:next

print at 19,1;ink 0;paper 1;" NOTHING THING "
print at 21,1;ink 0;paper 1;" PAULO SILVA, MARCH 2013 "
print at 22,1;ink 0;paper 1;" PUSH ANY KEY "

pause 0

for izz=0 to 63:ulapluspalette(izz,peek(@a02scr+izz+1536)):next
for izz=0 to 767:poke 22528+izz,(peek(@a02scr+izz+768)):next
for izz=0 to 767:print paper 8;ink 8;bright 8;flash 8;inverse((peek(@a02scr+izz)band 128)/128);chr$(peek(@zx81zxspconv+(peek(@a02scr+izz)band 63)));:next

print at 2,3;ink 1;paper 5;" THIS GAME IS ABOUT NOTHING THINGS. NOTHING THING IS A TERM VERY USED FROM JACQUE FRESCO, FROM HIS AUDIO MATERIALS AND ONLINE SEMINARS, ABOUT THE VENUS PROJECT. DEPENDING ON THE PURPOSE, A GAME LIKE THIS CAN BE A NOTHING THING. "
print at 22,1;ink 1;paper 5;" PUSH ANY KEY "

pause 0

for izz=0 to 63:ulapluspalette(izz,peek(@a03scr+izz+1536)):next
for izz=0 to 767:poke 22528+izz,(peek(@a03scr+izz+768)):next
for izz=0 to 767:print paper 8;ink 8;bright 8;flash 8;inverse((peek(@a03scr+izz)band 128)/128);chr$(peek(@zx81zxspconv+(peek(@a03scr+izz)band 63)));:next

print at 2,3;ink 1;paper 5;" AND MAYBE THIS GAME IS NOT A GAME AT ALL. THE MAIN PURPOSE IS TO CALL PEOPLE FOR THE IMPORTANCE OF DEVELOPING GAMES ABOUT RESOURCE BASED ECONOMY, WHICH IS VERY PROBABLY THE ONLY SOLUTION WE MAY HAVE FOR A SUSTAINABLE FUTURE OF THE WHOLE MANKIND. "
print at 22,1;ink 1;paper 5;" PUSH ANY KEY "

pause 0

for izz=0 to 63:ulapluspalette(izz,peek(@a04scr+izz+1536)):next
for izz=0 to 767:poke 22528+izz,(peek(@a04scr+izz+768)):next
for izz=0 to 767:print paper 8;ink 8;bright 8;flash 8;inverse((peek(@a04scr+izz)band 128)/128);chr$(peek(@zx81zxspconv+(peek(@a04scr+izz)band 63)));:next

print at 2,3;ink 1;paper 5;" WELL, THAT IS IT. THIS IS MOSTLY AN EXPERIENCE USING ULAPLUS, AND SOME AESTETIC LIMITATION NOSTALGY FROM THE GOOD OLD ZX81 MACHINES. AND ABOUT THE INFORMATION CITED HERE, BE WELCOME LOOKING FROM THEM FROM THE USUAL SEARCH ENGINES. CHEERS. "
print at 22,1;ink 1;paper 5;" PUSH ANY KEY "

pause 0

goto loop01




