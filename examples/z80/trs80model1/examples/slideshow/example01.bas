asm
  org $5300
  end asm

#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/pacmanldir.bas"

goto jmp01:
picture01:
asm
incbin "library/pictures/01_05compstrs80hf.bin"
incbin "library/pictures/02_image.bin"
incbin "library/pictures/03_tandytrs80model1complete1.bin"
incbin "library/pictures/04_trs80i.bin"
incbin "library/pictures/05_trs80iii.bin"
incbin "library/pictures/06_trs80model1.bin"
incbin "library/pictures/07_trs80mod1l1_4k.bin"
incbin "library/pictures/08_trs80Model4pCropDelsener.bin"
end asm
jmp01:

dim eee as integer at $5210
dim seed as integer at $5212
dim ee0 as integer at $5214
dim ee1 as integer at $5216
dim ee2 as integer at $5218
dim ee3 as integer at $521A
dim eex as integer at $521C
dim eey as integer at $521E

sub trs80putchar(txp1 as uinteger,typ1 as uinteger,tch1 as ubyte):
  poke $3C00+typ1*64+txp1,tch1
  end sub


do
for eee=0 to 7
pacmanldir($3C00,@picture01+(1024*eee),1024)
smsdelay(60000):smsdelay(60000)
next
loop

do:loop

'out $FF,%01001000

for eee=0 to 1023
  seed=smsrnd(seed)
  poke $3C00+eee,128+(seed band 127)
  smsdelay(10)
  next

for ee0=0 to 15
  for ee1=0 to 15
    trs80putchar(48+ee1,0+ee0,ee0*16+ee1)
    next:next

eee=0:eex=16:eey=8
do

trs80putchar(1,1,48+(int(eee/10000) mod 10) )
trs80putchar(2,1,48+(int(eee/1000) mod 10) )
trs80putchar(3,1,48+(int(eee/100) mod 10) )
trs80putchar(4,1,48+(int(eee/10) mod 10) )
trs80putchar(5,1,48+(eee mod 10) )

ee0=peek($3801)
trs80putchar(3,4,48+(int(ee0/100) mod 10) )
trs80putchar(4,4,48+(int(ee0/10) mod 10) )
trs80putchar(5,4,48+(ee0 mod 10) )

eex=eex+((ee0 band 16)/16)-((ee0 band 2)/2)  '- wasd keys

ee0=peek($3802)
trs80putchar(3,5,48+(int(ee0/100) mod 10) )
trs80putchar(4,5,48+(int(ee0/10) mod 10) )
trs80putchar(5,5,48+(ee0 mod 10) )

ee0=peek($3804)
trs80putchar(3,6,48+(int(ee0/100) mod 10) )
trs80putchar(4,6,48+(int(ee0/10) mod 10) )
trs80putchar(5,6,48+(ee0 mod 10) )

eey=eey+((ee0 band 8)/8)-((ee0 band 128)/128)  '- wasd keys

ee0=peek($3808)
trs80putchar(3,7,48+(int(ee0/100) mod 10) )
trs80putchar(4,7,48+(int(ee0/10) mod 10) )
trs80putchar(5,7,48+(ee0 mod 10) )

trs80putchar(eex,int(eey/2),eee band 127 )

eee=eee+1
smsdelay(1000)
loop

'- video ram: $3C00..$3FFF

