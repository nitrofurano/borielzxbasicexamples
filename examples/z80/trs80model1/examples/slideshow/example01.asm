	org 21232
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		org $5300
#line 1
	jp __LABEL__jmp01
__LABEL__picture01:
#line 10
		incbin "library/pictures/01_05compstrs80hf.bin"
		incbin "library/pictures/02_image.bin"
		incbin "library/pictures/03_tandytrs80model1complete1.bin"
		incbin "library/pictures/04_trs80i.bin"
		incbin "library/pictures/05_trs80iii.bin"
		incbin "library/pictures/06_trs80model1.bin"
		incbin "library/pictures/07_trs80mod1l1_4k.bin"
		incbin "library/pictures/08_trs80Model4pCropDelsener.bin"
#line 18
__LABEL__jmp01:
__LABEL0:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL2
__LABEL5:
	ld hl, 1024
	push hl
	ld hl, (_eee)
	ld de, 1024
	call __MUL16_FAST
	ex de, hl
	ld hl, (__LABEL__picture01) & 0xFFFF
	add hl, de
	push hl
	ld hl, 15360
	push hl
	call _pacmanldir
	ld hl, 60000
	push hl
	call _smsdelay
	ld hl, 60000
	push hl
	call _smsdelay
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL2:
	ld hl, 7
	ld de, (_eee)
	call __LTI16
	or a
	jp z, __LABEL5
	jp __LABEL0
__LABEL7:
	jp __LABEL7
	ld hl, 0
	ld (_eee), hl
	jp __LABEL9
__LABEL12:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 15360
	add hl, de
	push hl
	ld de, 127
	ld hl, (_seed)
	call __BAND16
	ld de, 128
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 10
	push hl
	call _smsdelay
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL9:
	ld hl, 1023
	ld de, (_eee)
	call __LTI16
	or a
	jp z, __LABEL12
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL14
__LABEL17:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL19
__LABEL22:
	ld hl, (_ee0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee0)
	push hl
	ld hl, (_ee1)
	ld de, 48
	add hl, de
	push hl
	call _trs80putchar
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL19:
	ld hl, 15
	ld de, (_ee1)
	call __LTI16
	or a
	jp z, __LABEL22
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL14:
	ld hl, 15
	ld de, (_ee0)
	call __LTI16
	or a
	jp z, __LABEL17
	ld hl, 0
	ld (_eee), hl
	ld hl, 16
	ld (_eex), hl
	ld hl, 8
	ld (_eey), hl
__LABEL24:
	ld hl, (_eee)
	ld de, 10000
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 1
	push hl
	call _trs80putchar
	ld hl, (_eee)
	ld de, 1000
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 2
	push hl
	call _trs80putchar
	ld hl, (_eee)
	ld de, 100
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 3
	push hl
	call _trs80putchar
	ld hl, (_eee)
	ld de, 10
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 4
	push hl
	call _trs80putchar
	ld hl, (_eee)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 5
	push hl
	call _trs80putchar
	ld a, (14337)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 100
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 3
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 4
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 5
	push hl
	call _trs80putchar
	ld de, 16
	ld hl, (_ee0)
	call __BAND16
	ld de, 16
	call __DIVI16
	ex de, hl
	ld hl, (_eex)
	add hl, de
	push hl
	ld de, 2
	ld hl, (_ee0)
	call __BAND16
	sra h
	rr l
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_eex), hl
	ld a, (14338)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 100
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 3
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 4
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 5
	push hl
	call _trs80putchar
	ld a, (14340)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 100
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 3
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 4
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 5
	push hl
	call _trs80putchar
	ld de, 8
	ld hl, (_ee0)
	call __BAND16
	ld de, 8
	call __DIVI16
	ex de, hl
	ld hl, (_eey)
	add hl, de
	push hl
	ld de, 128
	ld hl, (_ee0)
	call __BAND16
	ld de, 128
	call __DIVI16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_eey), hl
	ld a, (14344)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 100
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 3
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 4
	push hl
	call _trs80putchar
	ld hl, (_ee0)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 5
	push hl
	call _trs80putchar
	ld de, 127
	ld hl, (_eee)
	call __BAND16
	ld a, l
	push af
	ld hl, (_eey)
	sra h
	rr l
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push hl
	ld hl, (_eex)
	push hl
	call _trs80putchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	ld hl, 1000
	push hl
	call _smsdelay
	jp __LABEL24
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	pop iy
	pop ix
	exx
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_smsdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b,(ix+5)
		ld c,(ix+4)
smsdelayloop:
		dec bc
		ld a,b
		or c
		jp nz,smsdelayloop
#line 8
_smsdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_pacmanldir:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d,(ix+5)
		ld e,(ix+4)
		ld h,(ix+7)
		ld l,(ix+6)
		ld b,(ix+9)
		ld c,(ix+8)
		ldir
#line 8
_pacmanldir__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_trs80putchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 64
	call __MUL16_FAST
	ld de, 15360
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_trs80putchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 726 "example01.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 727 "example01.bas"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 728 "example01.bas"
#line 1 "lti16.asm"
	
#line 1 "lti8.asm"
	
__LTI8: ; Test 8 bit values A < H
        ; Returns result in A: 0 = False, !0 = True
	        sub h
	
__LTI:  ; Signed CMP
	        PROC
	        LOCAL __PE
	
	        ld a, 0  ; Sets default to false
__LTI2:
	        jp pe, __PE
	        ; Overflow flag NOT set
	        ret p
	        dec a ; TRUE
	
__PE:   ; Overflow set
	        ret m
	        dec a ; TRUE
	        ret
	        
	        ENDP
#line 3 "lti16.asm"
	
__LTI16: ; Test 8 bit values HL < DE
        ; Returns result in A: 0 = False, !0 = True
	        xor a
	        sbc hl, de
	        jp __LTI2
	
#line 729 "example01.bas"
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 730 "example01.bas"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 731 "example01.bas"
	
ZXBASIC_USER_DATA:
	_eee EQU 21008
	_seed EQU 21010
	_ee0 EQU 21012
	_ee1 EQU 21014
	_eex EQU 21020
	_eey EQU 21022
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
