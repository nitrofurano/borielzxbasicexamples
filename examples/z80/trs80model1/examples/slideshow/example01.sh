#!/bin/bash
rm example01.asm example01.cas
zxb.py example01.bas --asm --org=$((0x52F0))
zxb.py example01.bas --org=$((0x52F0))
dd ibs=1 skip=$((0x0010)) if=example01.bin of=example01_b.bin
pasmo --hex library/pasmoincbin.asm example01.hex
ihx2cmd example01.hex example01.cmd
cmd2cas example01.cmd example01.cas
rm example01.hex example01.bin example01_b.bin
#rm example01.cmd
#mame trs80l2 -cass1 example01.cas

mame trs80l2 -quik example01.cmd

# [enter]
# system
# example01
# /

