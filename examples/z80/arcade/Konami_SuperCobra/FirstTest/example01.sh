rm stratgyx.zip
rm example01.asm
rm stratgyx/*
rmdir stratgyx
mkdir stratgyx

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=stratgyx/2c_1.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=stratgyx/2e_2.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=stratgyx/2f_3.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=stratgyx/2h_4.bin
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=stratgyx/2j_5.bin
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=stratgyx/2l_6.bin

#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "2c_1.bin",     0x0000, 0x1000, CRC(eec01237) SHA1(619aaf6379064395a1166575f207d61c90731bb9) )
#- ROM_LOAD( "2e_2.bin",     0x1000, 0x1000, CRC(926cb2d5) SHA1(241e7b0f0d7e20a79299696be28963fd01269e86) )
#- ROM_LOAD( "2f_3.bin",     0x2000, 0x1000, CRC(849e2504) SHA1(0ec00a5c77e9d81d69f8b847a50f36af2dacc8d0) )
#- ROM_LOAD( "2h_4.bin",     0x3000, 0x1000, CRC(8a64069b) SHA1(ccc0e0441360e3ed93f2a0dab70c3a66edad969f) )
#- ROM_LOAD( "2j_5.bin",     0x4000, 0x1000, CRC(78b9b898) SHA1(158e11352d68ed9e4277efbcb7e927e9bdc662f8) )
#- ROM_LOAD( "2l_6.bin",     0x5000, 0x1000, CRC(20bae414) SHA1(cd3c03d3b6a3abb64cb86cffa733d5494cb226f7) )



dd bs=$((0x1000)) count=1 if=/dev/zero of=stratgyx/s1.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=stratgyx/s2.bin

#- ROM_REGION( 0x10000, "audiocpu", 0 )    /* 64k for sound code */
#- ROM_LOAD( "s1.bin",       0x0000, 0x1000, CRC(713a5db8) SHA1(8ec41cb93cfd856dc5aecace6238240a5d114ce1) )
#- ROM_LOAD( "s2.bin",       0x1000, 0x1000, CRC(46079411) SHA1(72bfc39979818309ac5a49654a825f9e4bd0236c) )







#gfx
zxb.py library/b2r0f0_charmapchacknpop01.zxi --org=$((0x0000))
cat dummy64k.bin >> b2r0f0_charmapchacknpop01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b2r0f0_charmapchacknpop01.bin of=b2r0f0_charmapchacknpop01b.bin
rm b2r0f0_charmapchacknpop01.bin
#need to alternate bytes
#dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b2r0f0_charmapchacknpop01b.bin of=stratgyx/5f_c2.bin
#dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=b2r0f0_charmapchacknpop01b.bin of=stratgyx/5h_c1.bin

xxd -p -c 2 -s 0 -l $((0x1000)) b2r0f0_charmapchacknpop01b.bin | cut -b 1-2 | xxd -r -p > stratgyx/5f_c2.bin
xxd -p -c 2 -s 1 -l $((0x1000)) b2r0f0_charmapchacknpop01b.bin | cut -b 1-2 | xxd -r -p > stratgyx/5h_c1.bin

rm b2r0f0_charmapchacknpop01b.bin







#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=stratgyx/pp_e02.rom
#rm spritemap01.bin



zxb.py library/palette01.bas --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=stratgyx/strategy.6e
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=stratgyx/stratgyx.3j


#- clut01.zxi might need to be different files
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
rm clut01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=clut01b.bin of=stratgyx/strategy.10k
rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=stratgyx/82s126.1m


#- ROM_REGION( 0x0020, "proms", 0 )
#- ROM_LOAD( "strategy.6e",  0x0000, 0x0020, CRC(51a629e1) SHA1(f9826202c91a4a3deb8d815cbaa107b29dce5835) )

#- ROM_REGION( 0x0020, "user1", 0 )
#- ROM_LOAD( "strategy.10k", 0x0000, 0x0020, CRC(d95c0318) SHA1(83e5355fdca7b4e1fb5a0e6eeaacfbf9561e2c36) )    /* background color map */






rm smsboot.bin dummy64k.bin example01.bin

zip -r stratgyx stratgyx
rm stratgyx/*
rmdir stratgyx

mame -w -video soft -resolution0 512x512 -rp ./ stratgyx


