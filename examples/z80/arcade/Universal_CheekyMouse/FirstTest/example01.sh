rm example01.asm
rm cheekyms.zip
rm cheekyms/*
rmdir cheekyms
mkdir cheekyms






dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin








#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=cheekyms/cm03.c5
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=cheekyms/cm04.c6
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=cheekyms/cm05.c7
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=cheekyms/cm06.c8
rm example01.bin

#ROM_START( cheekyms )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cm03.c5",       0x0000, 0x0800, CRC(1ad0cb40) SHA1(2a751395ac19a3218c22cfd3597f9a17b8e31527) )
#ROM_LOAD( "cm04.c6",       0x0800, 0x0800, CRC(2238f607) SHA1(35df9eb49f6e3c6351fae220d773442cf8536f90) )
#ROM_LOAD( "cm05.c7",       0x1000, 0x0800, CRC(4169eba8) SHA1(52a059f29c724d087483c7cd733f75d7b8a5b103) )
#ROM_LOAD( "cm06.c8",       0x1800, 0x0800, CRC(7031660c) SHA1(1370702e30897e45ee172609c1d983f8a4fdf157) )








#gfx
zxb.py library/b2r3f0_charmapslapfight01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r3f0_charmapslapfight01.bin of=b2r3f0_charmapslapfight01b.bin
xxd -p -c 2 -s 0 -l $((0x1000)) b2r3f0_charmapslapfight01b.bin | cut -b 1-2 | xxd -r -p > cheekyms/cm01.c1
xxd -p -c 2 -s 1 -l $((0x1000)) b2r3f0_charmapslapfight01b.bin | cut -b 1-2 | xxd -r -p > cheekyms/cm02.c2
xxd -p -c 2 -s 0 -l $((0x1000)) b2r3f0_charmapslapfight01b.bin | cut -b 1-2 | xxd -r -p > cheekyms/cm07.n5
xxd -p -c 2 -s 1 -l $((0x1000)) b2r3f0_charmapslapfight01b.bin | cut -b 1-2 | xxd -r -p > cheekyms/cm08.n6
rm b2r3f0_charmapslapfight01.bin b2r3f0_charmapslapfight01b.bin

#gfx1
#dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b2r3f0_charmapslapfight01b.bin of=cheekyms/cm01.c1
#dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b2r3f0_charmapslapfight01b.bin of=cheekyms/cm02.c2
#dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b2r3f0_charmapslapfight01b.bin of=cheekyms/cm07.n5
#dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b2r3f0_charmapslapfight01b.bin of=cheekyms/cm08.n6
#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD( "cm01.c1",       0x0000, 0x0800, CRC(26f73bd7) SHA1(fa4db5df5be1a5f4531cba86a83f89b7eb7fa3ec) )
#ROM_LOAD( "cm02.c2",       0x0800, 0x0800, CRC(885887c3) SHA1(62ce8e39d27c0cfea9ebd51757ad31b0baf6b3cd) )
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "cm07.n5",       0x0000, 0x0800, CRC(2738c88d) SHA1(3ccd6c1d49bfe2c1b141854ec705e692252e8af8) )
#ROM_LOAD( "cm08.n6",       0x0800, 0x0800, CRC(b3fbd4ac) SHA1(9f45cc6d9e0bf580149e18de5c3e37d4de347b92) )







#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=cheekyms/cm.m9
dd ibs=1 count=$((0x0020)) skip=$((0x0020)) if=palette01b.bin of=cheekyms/cm.m8
dd ibs=1 count=$((0x0020)) skip=$((0x0040)) if=palette01b.bin of=cheekyms/cm.p3
rm palette01b.bin
#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=cheekyms/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=cheekyms/hgb1.h7  #?sp
#rm clut01b.bin
#proms

#dd bs=$((0x20)) count=1 if=/dev/urandom of=cheekyms/cm.m9
#dd bs=$((0x20)) count=1 if=/dev/urandom of=cheekyms/cm.m8
#dd bs=$((0x20)) count=1 if=/dev/urandom of=cheekyms/cm.p3

#ROM_REGION( 0x0060, "proms", 0 )
#ROM_LOAD( "cm.m9",         0x0000, 0x0020, CRC(db9c59a5) SHA1(357ed5ac8e954a4c8b4d78d36e57bf2de36c1d57) )    /* Character colors /                                */
#ROM_LOAD( "cm.m8",         0x0020, 0x0020, CRC(2386bc68) SHA1(6676082860cd8678a71339a352d2c6286e78ba44) )    /* Character colors \ Selected by Bit 6 of Port 0x80 */
#ROM_LOAD( "cm.p3",         0x0040, 0x0020, CRC(6ac41516) SHA1(05bf40790a0de1e859362df892f7f158c183e247) )  /* Sprite colors */
#ROM_END




#-
zip -r cheekyms cheekyms
rm cheekyms/*
rmdir cheekyms
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ cheekyms







#ROM_START( cheekyms )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cm03.c5",       0x0000, 0x0800, CRC(1ad0cb40) SHA1(2a751395ac19a3218c22cfd3597f9a17b8e31527) )
#ROM_LOAD( "cm04.c6",       0x0800, 0x0800, CRC(2238f607) SHA1(35df9eb49f6e3c6351fae220d773442cf8536f90) )
#ROM_LOAD( "cm05.c7",       0x1000, 0x0800, CRC(4169eba8) SHA1(52a059f29c724d087483c7cd733f75d7b8a5b103) )
#ROM_LOAD( "cm06.c8",       0x1800, 0x0800, CRC(7031660c) SHA1(1370702e30897e45ee172609c1d983f8a4fdf157) )

#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD( "cm01.c1",       0x0000, 0x0800, CRC(26f73bd7) SHA1(fa4db5df5be1a5f4531cba86a83f89b7eb7fa3ec) )
#ROM_LOAD( "cm02.c2",       0x0800, 0x0800, CRC(885887c3) SHA1(62ce8e39d27c0cfea9ebd51757ad31b0baf6b3cd) )
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "cm07.n5",       0x0000, 0x0800, CRC(2738c88d) SHA1(3ccd6c1d49bfe2c1b141854ec705e692252e8af8) )
#ROM_LOAD( "cm08.n6",       0x0800, 0x0800, CRC(b3fbd4ac) SHA1(9f45cc6d9e0bf580149e18de5c3e37d4de347b92) )

#ROM_REGION( 0x0060, "proms", 0 )
#ROM_LOAD( "cm.m9",         0x0000, 0x0020, CRC(db9c59a5) SHA1(357ed5ac8e954a4c8b4d78d36e57bf2de36c1d57) )    /* Character colors /                                */
#ROM_LOAD( "cm.m8",         0x0020, 0x0020, CRC(2386bc68) SHA1(6676082860cd8678a71339a352d2c6286e78ba44) )    /* Character colors \ Selected by Bit 6 of Port 0x80 */
#ROM_LOAD( "cm.p3",         0x0040, 0x0020, CRC(6ac41516) SHA1(05bf40790a0de1e859362df892f7f158c183e247) )  /* Sprite colors */
#ROM_END












