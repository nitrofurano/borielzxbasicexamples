palette01:
'----- bbgggrrr
asm

  defb 11111111b ;- #E9F7CB
  defb 00101110b ;- #D4BE2F
  defb 01001001b ;- #303745
  defb 10110001b ;- #2FD4B0
  defb 01010000b ;- #005E46
  defb 00011000b ;- #00713D
  defb 01110100b ;- #89C541
  defb 00110110b ;- #CEDD2A
  defb 00000111b ;- #F00000
  defb 00011111b ;- #F07800
  defb 00101111b ;- #F0A800
  defb 00110100b ;- #90C030
  defb 00010111b ;- #FE4F3E
  defb 00101111b ;- #FBAF26
  defb 01110111b ;- #FAC14E
  defb 01010010b ;- #5A434D


  defb 01001001b ;- #303745
  defb 10110001b ;- #2FD4B0
  defb 00101110b ;- #D4BE2F
  defb 11111111b ;- #E9F7CB
  defb 01010000b ;- #005E46
  defb 00011000b ;- #00713D
  defb 01110100b ;- #89C541
  defb 00110110b ;- #CEDD2A
  defb 00000111b ;- #F00000
  defb 00011111b ;- #F07800
  defb 00101111b ;- #F0A800
  defb 00110100b ;- #90C030
  defb 01010010b ;- #5A434D
  defb 00010111b ;- #FE4F3E
  defb 00101111b ;- #FBAF26
  defb 01110111b ;- #FAC14E

  defb 01001001b ;- #303745
  defb 10110001b ;- #2FD4B0
  defb 00101110b ;- #D4BE2F
  defb 11111111b ;- #E9F7CB
  defb 01010000b ;- #005E46
  defb 00011000b ;- #00713D
  defb 01110100b ;- #89C541
  defb 00110110b ;- #CEDD2A
  defb 00000111b ;- #F00000
  defb 00011111b ;- #F07800
  defb 00101111b ;- #F0A800
  defb 00110100b ;- #90C030
  defb 01010010b ;- #5A434D
  defb 00010111b ;- #FE4F3E
  defb 00101111b ;- #FBAF26
  defb 01110111b ;- #FAC14E
  defb 01001001b ;- #303745
  defb 10110001b ;- #2FD4B0
  defb 00101110b ;- #D4BE2F
  defb 11111111b ;- #E9F7CB
  defb 01010000b ;- #005E46
  defb 00011000b ;- #00713D
  defb 01110100b ;- #89C541
  defb 00110110b ;- #CEDD2A
  defb 00000111b ;- #F00000
  defb 00011111b ;- #F07800
  defb 00101111b ;- #F0A800
  defb 00110100b ;- #90C030
  defb 01010010b ;- #5A434D
  defb 00010111b ;- #FE4F3E
  defb 00101111b ;- #FBAF26
  defb 01110111b ;- #FAC14E
  end asm
