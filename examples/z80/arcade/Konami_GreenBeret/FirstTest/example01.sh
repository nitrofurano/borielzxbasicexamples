rm gberet.zip
rm example01.asm
rm gberet/*
rmdir gberet
mkdir gberet

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=gberet/577l03.10c
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=gberet/577l02.8c
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=gberet/577l01.7c




zxb.py library/charmaptecmo01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmaptecmo01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmaptecmo01.bin of=gberet/577l07.3f
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmaptecmo01.bin of=gberet/577l06.5e
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmaptecmo01.bin of=gberet/577l05.4e
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmaptecmo01.bin of=gberet/577l08.4f
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmaptecmo01.bin of=gberet/577l04.3e
rm charmaptecmo01.bin













#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=gberet/pp_e02.rom
#rm spritemap01.bin

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=gberet/577h09.2f
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=gberet/577h09.2f





#- clut01.zxi might need to be different files
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=gberet/577h11.6f # characters
dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=gberet/577h10.5f # sprites
rm clut01b.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=gberet/577h09.2f # palette
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=gberet/577h11.6f # characters
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=gberet/577h10.5f # sprites








#????
#dd bs=256 count=1 if=/dev/zero of=gberet/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=gberet/82s126.3m

rm smsboot.bin dummy64k.bin example01.bin

zip -r gberet gberet
rm gberet/*
rmdir gberet

mame -w -video soft -resolution0 512x512 -rp ./ gberet


