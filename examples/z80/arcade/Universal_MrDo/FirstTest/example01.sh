rm example01.asm
rm mrdo.zip
rm mrdo/*
rmdir mrdo
mkdir mrdo


#first cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=mrdo/a4-01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=mrdo/c4-02.bin
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=mrdo/e4-03.bin
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=mrdo/f4-04.bin
rm example01.bin

#- ROM_LOAD( "a4-01.bin",    0x0000, 0x2000, CRC(03dcfba2) SHA1(c15e3d0c4225e0ca120bcd28aca39632575f8e11) )
#- ROM_LOAD( "c4-02.bin",    0x2000, 0x2000, CRC(0ecdd39c) SHA1(c64b3363593911a676c647bf3dba8fe063fcb0de) )
#- ROM_LOAD( "e4-03.bin",    0x4000, 0x2000, CRC(358f5dc2) SHA1(9fed1f5d1d04935d1b77687c8b2f3bfce970dc08) )
#- ROM_LOAD( "f4-04.bin",    0x6000, 0x2000, CRC(f4190cfc) SHA1(24f5125d900f944294d4eda068b710c8f1c6d39f) )






#second cpu
#dd bs=$((0x4000)) count=1 if=/dev/zero of=mrdo/07n_a0.bin
#zxb.py library/dorunrunsecondcpu.bas --org=2048
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=dorunrunsecondcpu.bin of=dorunrunsecondcpu2.bin
#cat dummy64k.bin >> dorunrunsecondcpu2.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=dorunrunsecondcpu2.bin of=mrdo/07n_a0.bin
#rm dorunrunsecondcpu.bin dorunrunsecondcpu2.bin









#third cpu
#dd bs=$((0x2000)) count=1 if=/dev/zero of=mrdo/01d.bin




#gfx
zxb.py library/b2r1f1_charmapmrdo01.zxi --org=$((0x0000))
cat dummy64k.bin >> b2r1f1_charmapmrdo01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin
rm b2r1f1_charmapmrdo01.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/s8-09.bin
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/u8-10.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/r8-08.bin
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/n8-07.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/h5-05.bin
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrdo/k5-06.bin
rm b2r1f1_charmapmrdo01b.bin






#palette
#zxb.py library/paletteseq01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=paletteseq01.bin of=mrdo/09c.bin
#rm paletteseq01.bin
#dd bs=256 count=1 if=/dev/urandom of=mrdo/mrdo.clr
zxb.py library/paletteseq01.zxi --org=$((0x0000))
cat dummy64k.bin >> paletteseq01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=paletteseq01.bin of=paletteseq01b.bin
rm paletteseq01.bin
#
#sequence
dd ibs=1 count=$((0x20)) skip=$((0x0000)) if=paletteseq01b.bin of=mrdo/u02--2.bin
dd ibs=1 count=$((0x20)) skip=$((0x0020)) if=paletteseq01b.bin of=mrdo/t02--3.bin
#
#alternate
#xxd -p -c 2 -s 0 -l $((0x40)) paletteseq01b.bin | cut -b 1-2 | xxd -r -p > mrdo/u02--2.bin
#xxd -p -c 2 -s 1 -l $((0x40)) paletteseq01b.bin | cut -b 1-2 | xxd -r -p > mrdo/t02--3.bin
#
#random
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=mrdo/u02--2.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=mrdo/t02--3.bin
#
rm paletteseq01b.bin



#clut
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x20)) skip=$((0x0010)) if=clut01.bin of=mrdo/f10--1.bin
rm clut01.bin




#dd bs=$((0x0020)) count=1 if=/dev/urandom of=mrdo/f10--1.bin

dd bs=$((0x0020)) count=1 if=/dev/zero of=mrdo/j10--4.bin

#- ROM_REGION( 0x0080, "proms", 0 )
#- ROM_LOAD( "u02--2.bin",   0x0000, 0x0020, CRC(238a65d7) SHA1(a5b20184a1989db23544296331462ec4d7be7516) )    /* palette (high bits) */
#- ROM_LOAD( "t02--3.bin",   0x0020, 0x0020, CRC(ae263dc0) SHA1(7072c100b9d692f5bb12b0c9e304425f534481e2) )    /* palette (low bits) */
#- ROM_LOAD( "f10--1.bin",   0x0040, 0x0020, CRC(16ee4ca2) SHA1(fcba4d103708b9711452009cd29c4f88d2f64cd3) )    /* sprite color lookup table */
#- ROM_LOAD( "j10--4.bin",   0x0060, 0x0020, CRC(ff7fe284) SHA1(3ac8e30011c1fcba0ee8f4dc932f82296c3ba143) )    /* timing (not used) */






#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=mrdo/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=mrdo/mrdo.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=mrdo/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=mrdo/82s126.3m

rm smsboot.bin dummy64k.bin

#rm mrdo.zip
#rm mrdo/*
#rmdir mrdo

zip -r mrdo mrdo
rm mrdo/*
rmdir mrdo


mame -w -video soft -resolution0 512x512 -rp ./ mrdo

