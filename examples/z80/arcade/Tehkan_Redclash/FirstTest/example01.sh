rm redclash.zip
rm example01.asm
rm redclash/*
rmdir redclash
mkdir redclash


dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=redclash/11.11c
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=redclash/13.7c
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=redclash/12.9c

#	ROM_REGION(0x10000, "maincpu", 0 )
#	ROM_LOAD( "11.11c",       0x0000, 0x1000, CRC(695e070e) SHA1(8d0451a05572f62e0f282ab96bdd26d08b77a6c9) )
#	ROM_LOAD( "13.7c",        0x1000, 0x1000, CRC(c2090318) SHA1(71725cdf51aedf5f29fa1dd1a41ad5e62c9a580d) )
#	ROM_LOAD( "12.9c",        0x2000, 0x1000, CRC(b60e5ada) SHA1(37440f382c5e8852d804fa9837c36cc1e9d94d1d) )






#audio
#dd bs=$((0x2000)) count=1 if=/dev/zero of=redclash/01_h03t.bin
#	ROM_REGION( 0x10000, "audiocpu", 0 )    /* 64k for sound board */
#	ROM_LOAD( "01_h03t.bin",  0x0000, 0x2000, CRC(8407917d) SHA1(318face9f7a7ab6c7eeac773995040425e780aaf) )





zxb.py library/b2r1f1_charmapredclash01.zxi --org=$((0x0000))
cat dummy64k.bin >> b2r1f1_charmapredclash01.bin
dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b2r1f1_charmapredclash01.bin of=redclash/6.12a
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapredclash01.bin of=redclash/14.3e
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapredclash01.bin of=redclash/15.3d
rm b2r1f1_charmapredclash01.bin

#	ROM_REGION(0x0800, "gfx1", 0 )
#	ROM_LOAD( "6.12a",        0x0000, 0x0800, CRC(da9bbcc2) SHA1(4cbe03c7f5e99cc2f124e0089ea3c392156b5d92) )
#	ROM_REGION(0x2000, "gfx2", 0 )
#	ROM_LOAD( "14.3e",        0x0000, 0x0800, CRC(483a1293) SHA1(e7812475c7509389bcf8fee35598e9894428eb37) )
#	ROM_CONTINUE(             0x1000, 0x0800 )
#	ROM_LOAD( "15.3d",        0x0800, 0x0800, CRC(c45d9601) SHA1(2f156ad61161d65284df0cc55eb1b3b990eb41cb) )
#	ROM_CONTINUE(             0x1800, 0x0800 )
#	ROM_REGION(0x2000, "gfx3", ROMREGION_ERASE00 )
#	/* gfx data will be rearranged here for 8x8 sprites */







#palette
zxb.py library/palette01.bas --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=redclash/1.12f
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=redclash/redclash.3j

#clut
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=clut01.bin of=redclash/2.4a
rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=redclash/redclash.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=redclash/redclash.11j
#rm clut01b.bin

#proms
dd bs=$((0x020)) count=1 if=/dev/urandom of=redclash/3.11e

#	ROM_REGION(0x0060, "proms", 0 )
#	ROM_LOAD( "1.12f",        0x0000, 0x0020, CRC(43989681) SHA1(0d471e6f499294f2f62f27392b8370e2af8e38a3) ) /* palette */
#	ROM_LOAD( "2.4a",         0x0020, 0x0020, CRC(9adabf46) SHA1(f3538fdbc4280b6be46a4d7ebb4c34bd1a1ce2b7) ) /* sprite color lookup table */
#	ROM_LOAD( "3.11e",        0x0040, 0x0020, CRC(27fa3a50) SHA1(7cf59b7a37c156640d6ea91554d1c4276c1780e0) ) /* ?? */






rm smsboot.bin dummy64k.bin example01.bin

zip -r redclash redclash
rm redclash/*
rmdir redclash

mame -w -video soft -sound none -resolution0 512x512 -rp ./ redclash






#ROM_START( redclash )
#	ROM_REGION(0x10000, "maincpu", 0 )
#	ROM_LOAD( "11.11c",       0x0000, 0x1000, CRC(695e070e) SHA1(8d0451a05572f62e0f282ab96bdd26d08b77a6c9) )
#	ROM_LOAD( "13.7c",        0x1000, 0x1000, CRC(c2090318) SHA1(71725cdf51aedf5f29fa1dd1a41ad5e62c9a580d) )
#	ROM_LOAD( "12.9c",        0x2000, 0x1000, CRC(b60e5ada) SHA1(37440f382c5e8852d804fa9837c36cc1e9d94d1d) )
#
#	ROM_REGION(0x0800, "gfx1", 0 )
#	ROM_LOAD( "6.12a",        0x0000, 0x0800, CRC(da9bbcc2) SHA1(4cbe03c7f5e99cc2f124e0089ea3c392156b5d92) )
#	ROM_REGION(0x2000, "gfx2", 0 )
#	ROM_LOAD( "14.3e",        0x0000, 0x0800, CRC(483a1293) SHA1(e7812475c7509389bcf8fee35598e9894428eb37) )
#	ROM_CONTINUE(             0x1000, 0x0800 )
#	ROM_LOAD( "15.3d",        0x0800, 0x0800, CRC(c45d9601) SHA1(2f156ad61161d65284df0cc55eb1b3b990eb41cb) )
#	ROM_CONTINUE(             0x1800, 0x0800 )
#	ROM_REGION(0x2000, "gfx3", ROMREGION_ERASE00 )
#	/* gfx data will be rearranged here for 8x8 sprites */
#
#	ROM_REGION(0x0060, "proms", 0 )
#	ROM_LOAD( "1.12f",        0x0000, 0x0020, CRC(43989681) SHA1(0d471e6f499294f2f62f27392b8370e2af8e38a3) ) /* palette */
#	ROM_LOAD( "2.4a",         0x0020, 0x0020, CRC(9adabf46) SHA1(f3538fdbc4280b6be46a4d7ebb4c34bd1a1ce2b7) ) /* sprite color lookup table */
#	ROM_LOAD( "3.11e",        0x0040, 0x0020, CRC(27fa3a50) SHA1(7cf59b7a37c156640d6ea91554d1c4276c1780e0) ) /* ?? */
#ROM_END


