rm gotya.zip
rm example01.asm
rm gotya/*
rmdir gotya
mkdir gotya


dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin


#cpu
#- zxb.py example01.zxb --asm --org=0x0000
#- zxb.py example01.zxb --org=0x0000
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=0x0069
zxb.py example01.bas --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=gotya/gb-06.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=gotya/gb-05.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=gotya/gb-04.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=gotya/gb-03.bin

#ROM_START( gotya )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "gb-06.bin",  0x0000, 0x1000, CRC(7793985a) SHA1(23aa8bd161e700bea59b92075423cdf55e9a26c3) )
#ROM_LOAD( "gb-05.bin",  0x1000, 0x1000, CRC(683d188b) SHA1(5341c62f5cf384c73be0d7a0a230bb8cebfbe709) )
#ROM_LOAD( "gb-04.bin",  0x2000, 0x1000, CRC(15b72f09) SHA1(bd941722ed1310d5c8ca8a44899368cba3815f3b) )
#ROM_LOAD( "gb-03.bin",  0x3000, 0x1000, CRC(f34d90ab) SHA1(bec5f6a34a273f308083a280f2b425d9c273c69b) )    /* this is the only ROM that passes the ROM test */





#gfx

zxb.py library/charmap01.zxi --org=0x0000
cat dummy32k.bin >> charmap01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=gotya/gb-12.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=gotya/gb-11.bin
rm charmap01.bin

#ROM_REGION( 0x1000,  "gfx1", 0 )    /* characters */
#ROM_LOAD( "gb-12.bin",  0x0000, 0x1000, CRC(4993d735) SHA1(9e47876238a8af3659721191a5f75c33507ed1a5) )
#ROM_REGION( 0x1000,  "gfx2", 0 )    /* sprites */
#ROM_LOAD( "gb-11.bin",  0x0000, 0x1000, CRC(5d5eca1b) SHA1(d7c6b5f4d398d5e33cc411ed593d6f53a9979493) )







#proms

#zxb.py library/palette01.bas --org=0x0000
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=gotya/gotya.3j
#rm palette01.bin

dd bs=$((0x0020)) count=1 if=/dev/urandom of=gotya/prom.1a
dd bs=$((0x0100)) count=1 if=/dev/urandom of=gotya/prom.4c

#ROM_REGION( 0x0120,  "proms", 0 )
#ROM_LOAD( "prom.1a",    0x0000, 0x0020, CRC(4864a5a0) SHA1(5b49f60b085fa026d4e8d4a5ad28ee7037a8ff9c) )    /* color PROM */
#ROM_LOAD( "prom.4c",    0x0020, 0x0100, CRC(4745b5f6) SHA1(02a7f759e9bc8089cbd9213a71bbe671f9641638) )    /* lookup table */




#user
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=0x0000
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=gotya/gotya.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=gotya/gotya.11j
#rm clut01b.bin
#????
dd bs=$((0x0800)) count=1 if=/dev/zero of=gotya/gb-01.bin
dd bs=$((0x0800)) count=1 if=/dev/zero of=gotya/gb-02.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=gotya/gb-10.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=gotya/gb-09.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=gotya/gb-08.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=gotya/gb-07.bin

#ROM_REGION( 0x1000,  "user1", 0 )       /* no idea what these are */
#ROM_LOAD( "gb-01.bin",  0x0000, 0x0800, CRC(c31dba64) SHA1(15ae54b7d475ca3f0a3acc45cd8da2916c5fdef2) )
#ROM_LOAD( "gb-02.bin",  0x0800, 0x0800, CRC(65a7e284) SHA1(91e9c34dcf20608863ad5475dc0c4309971c8eee) )
#ROM_REGION( 0x8000,  "user2", 0 )       /* HD38880 code/samples? */
#ROM_LOAD( "gb-10.bin",  0x4000, 0x1000, CRC(8101915f) SHA1(c4d21b1938ea7e0d47c48e74037f005280ac101b) )
#ROM_LOAD( "gb-09.bin",  0x5000, 0x1000, CRC(619bba76) SHA1(2a2deffe6f058fc840329fbfffbc0c70a0147c14) )
#ROM_LOAD( "gb-08.bin",  0x6000, 0x1000, CRC(82f59528) SHA1(6bfa2329eb291040bfc229c56420865253b0132a) )
#ROM_LOAD( "gb-07.bin",  0x7000, 0x1000, CRC(92a9f8bf) SHA1(9231cd86f24f1e6a585c3a919add50c1f8e42a4c) )
#ROM_END




rm smsboot.bin dummy32k.bin example01.bin

zip -r gotya gotya
rm gotya/*
rmdir gotya

mame -w -video soft -resolution0 512x512 -rp ./ gotya




#ROM_START( gotya )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "gb-06.bin",  0x0000, 0x1000, CRC(7793985a) SHA1(23aa8bd161e700bea59b92075423cdf55e9a26c3) )
#ROM_LOAD( "gb-05.bin",  0x1000, 0x1000, CRC(683d188b) SHA1(5341c62f5cf384c73be0d7a0a230bb8cebfbe709) )
#ROM_LOAD( "gb-04.bin",  0x2000, 0x1000, CRC(15b72f09) SHA1(bd941722ed1310d5c8ca8a44899368cba3815f3b) )
#ROM_LOAD( "gb-03.bin",  0x3000, 0x1000, CRC(f34d90ab) SHA1(bec5f6a34a273f308083a280f2b425d9c273c69b) )    /* this is the only ROM that passes the ROM test */

#ROM_REGION( 0x1000,  "gfx1", 0 )    /* characters */
#ROM_LOAD( "gb-12.bin",  0x0000, 0x1000, CRC(4993d735) SHA1(9e47876238a8af3659721191a5f75c33507ed1a5) )
#ROM_REGION( 0x1000,  "gfx2", 0 )    /* sprites */
#ROM_LOAD( "gb-11.bin",  0x0000, 0x1000, CRC(5d5eca1b) SHA1(d7c6b5f4d398d5e33cc411ed593d6f53a9979493) )

#ROM_REGION( 0x0120,  "proms", 0 )
#ROM_LOAD( "prom.1a",    0x0000, 0x0020, CRC(4864a5a0) SHA1(5b49f60b085fa026d4e8d4a5ad28ee7037a8ff9c) )    /* color PROM */
#ROM_LOAD( "prom.4c",    0x0020, 0x0100, CRC(4745b5f6) SHA1(02a7f759e9bc8089cbd9213a71bbe671f9641638) )    /* lookup table */

#ROM_REGION( 0x1000,  "user1", 0 )       /* no idea what these are */
#ROM_LOAD( "gb-01.bin",  0x0000, 0x0800, CRC(c31dba64) SHA1(15ae54b7d475ca3f0a3acc45cd8da2916c5fdef2) )
#ROM_LOAD( "gb-02.bin",  0x0800, 0x0800, CRC(65a7e284) SHA1(91e9c34dcf20608863ad5475dc0c4309971c8eee) )
#ROM_REGION( 0x8000,  "user2", 0 )       /* HD38880 code/samples? */
#ROM_LOAD( "gb-10.bin",  0x4000, 0x1000, CRC(8101915f) SHA1(c4d21b1938ea7e0d47c48e74037f005280ac101b) )
#ROM_LOAD( "gb-09.bin",  0x5000, 0x1000, CRC(619bba76) SHA1(2a2deffe6f058fc840329fbfffbc0c70a0147c14) )
#ROM_LOAD( "gb-08.bin",  0x6000, 0x1000, CRC(82f59528) SHA1(6bfa2329eb291040bfc229c56420865253b0132a) )
#ROM_LOAD( "gb-07.bin",  0x7000, 0x1000, CRC(92a9f8bf) SHA1(9231cd86f24f1e6a585c3a919add50c1f8e42a4c) )
#ROM_END


