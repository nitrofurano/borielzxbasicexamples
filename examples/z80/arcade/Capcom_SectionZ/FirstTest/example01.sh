# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm lwings.zip
rm lwings/*
rmdir lwings
mkdir lwings

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=lwings/6c_lw01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x8000)) if=example01.bin of=lwings/7c_lw02.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=lwings/9c_lw03.bin
rm dummy64k.bin example01.bin

#sound
dd bs=$((0x8000)) count=1 if=/dev/zero of=lwings/11e_lw04.bin

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/9h_lw05.bin
#gfx2
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3e_lw14.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1e_lw08.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3d_lw13.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1d_lw07.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3b_lw12.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1b_lw06.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3f_lw15.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1f_lw09.bin
#gfx3
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3j_lw17.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1j_lw11.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/3h_lw16.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=lwings/1h_lw10.bin
rm charmap01b.bin

#proms
dd bs=$((0x100)) count=1 if=/dev/urandom of=lwings/63s141.15g

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=lwings/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=lwings/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=lwings/lwings.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=lwings/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=lwings/82s126.3m

zip -r lwings lwings
rm lwings/*
rmdir lwings

rm smsboot.bin

mame -w -video soft -resolution0 512x512 -rp ./ lwings

