	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$DFFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld de, 819
	ld hl, (_seed)
	call __BAND16
	push hl
	ld hl, (_eee)
	push hl
	call _sectionzpalette
	ld de, 819
	ld hl, (_seed)
	call __BAND16
	ld de, 1092
	add hl, de
	push hl
	ld hl, (_eee)
	inc hl
	push hl
	call _sectionzpalette
	ld de, 819
	ld hl, (_seed)
	call __BAND16
	ld de, 2184
	add hl, de
	push hl
	ld hl, (_eee)
	inc hl
	inc hl
	push hl
	call _sectionzpalette
	ld de, 819
	ld hl, (_seed)
	call __BAND16
	ld de, 3276
	add hl, de
	push hl
	ld hl, (_eee)
	inc hl
	inc hl
	inc hl
	push hl
	call _sectionzpalette
	call _sectionzwatchdog
__LABEL4:
	ld hl, (_eee)
	ld de, 4
	add hl, de
	ld (_eee), hl
__LABEL0:
	ld hl, 1023
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 2645
	push hl
	ld hl, 0
	push hl
	call _sectionzpalette
	ld hl, 1445
	push hl
	ld hl, 1
	push hl
	call _sectionzpalette
	ld hl, 1370
	push hl
	ld hl, 2
	push hl
	call _sectionzpalette
	ld hl, 4080
	push hl
	ld hl, 3
	push hl
	call _sectionzpalette
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL10
__LABEL13:
	ld hl, 1
	ld (_eee), hl
	jp __LABEL15
__LABEL18:
__LABEL19:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL15:
	ld hl, 10
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	xor a
	push af
	ld a, 45
	push af
	ld hl, (_ee1)
	push hl
	ld hl, (_ee2)
	push hl
	call _sectionzputchar
	call _sectionzwatchdog
__LABEL14:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL10:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL5:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL20
__LABEL23:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL25
__LABEL28:
	ld hl, 1
	ld (_eee), hl
	jp __LABEL30
__LABEL33:
__LABEL34:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL30:
	ld hl, 100
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
	xor a
	push af
	ld a, 7
	push af
	ld hl, (_ee1)
	push hl
	ld hl, (_ee2)
	push hl
	call _sectionzputbgtile
	call _sectionzwatchdog
__LABEL29:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL25:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
__LABEL24:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL20:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL35
__LABEL38:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL40
__LABEL43:
	xor a
	push af
	ld hl, (_ee1)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee1)
	ld de, 14
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 12
	add hl, de
	push hl
	call _sectionzputchar
	call _sectionzwatchdog
__LABEL44:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL40:
	ld hl, 15
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL43
__LABEL42:
__LABEL39:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL35:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL38
__LABEL37:
	ld hl, 56832
	ld (_eee), hl
	jp __LABEL45
__LABEL48:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	xor a
	ld hl, (_eee)
	ld (hl), a
	call _sectionzwatchdog
__LABEL49:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL45:
	ld hl, 57215
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL48
__LABEL47:
	ld hl, 40
	ld (_eex), hl
	ld hl, 40
	ld (_eey), hl
	ld hl, 0
	ld (_eee), hl
__LABEL50:
	ld a, (63497)
	cpl
	ld l, a
	ld h, 0
	ld (_ej0), hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 8
	push hl
	call _sectionzputchar
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 7
	push hl
	call _sectionzputchar
	xor a
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 6
	push hl
	call _sectionzputchar
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL53
	ld hl, (_eex)
	inc hl
	ld (_eex), hl
__LABEL53:
	ld de, 2
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL55
	ld hl, (_eex)
	dec hl
	ld (_eex), hl
__LABEL55:
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL57
	ld hl, (_eey)
	inc hl
	ld (_eey), hl
__LABEL57:
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL59
	ld hl, (_eey)
	dec hl
	ld (_eey), hl
__LABEL59:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL60
__LABEL63:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL65
__LABEL68:
	xor a
	push af
	xor a
	push af
	ld hl, (_ee2)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_eey)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee1)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_eex)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee1)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	push af
	call _sectionzputsprite
__LABEL69:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL65:
	ld hl, 7
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL68
__LABEL67:
__LABEL64:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL60:
	ld hl, 7
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL63
__LABEL62:
	ld hl, (_eey)
	ld a, l
	ld (63496), a
	ld de, 255
	ld hl, (_eex)
	call __BXOR16
	ld a, l
	ld (63498), a
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	call _sectionzwatchdog
	jp __LABEL50
__LABEL51:
__LABEL70:
	ld hl, 56832
	ld (_ee1), hl
	jp __LABEL72
__LABEL75:
	ld bc, (_ee1)
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_ee3), hl
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL77
__LABEL80:
	ld hl, (_ee2)
	ld a, l
	ld hl, (_ee1)
	ld (hl), a
	ld hl, 1
	ld (_eee), hl
	jp __LABEL82
__LABEL85:
__LABEL86:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL82:
	ld hl, 100
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL85
__LABEL84:
	call _sectionzwatchdog
__LABEL81:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL77:
	ld hl, 255
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL80
__LABEL79:
	ld hl, (_ee3)
	ld a, l
	ld hl, (_ee1)
	ld (hl), a
__LABEL76:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL72:
	ld hl, 57215
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL75
__LABEL74:
	jp __LABEL70
__LABEL71:
	ld hl, 1
	ld (_eee), hl
	jp __LABEL87
__LABEL90:
__LABEL91:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL87:
	ld hl, 10000
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL90
__LABEL89:
__LABEL92:
	ld hl, 59392
	ld (_eee), hl
	jp __LABEL94
__LABEL97:
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL99
__LABEL102:
__LABEL103:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL99:
	ld hl, 100
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL102
__LABEL101:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld (63501), a
__LABEL98:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL94:
	ld hl, 60671
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL97
__LABEL96:
	ld hl, 61184
	ld (_eee), hl
	jp __LABEL104
__LABEL107:
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL109
__LABEL112:
__LABEL113:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL109:
	ld hl, 100
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL112
__LABEL111:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld (63501), a
__LABEL108:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL104:
	ld hl, 61439
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL107
__LABEL106:
	ld hl, 57344
	ld (_eee), hl
	jp __LABEL114
__LABEL117:
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL119
__LABEL122:
__LABEL123:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL119:
	ld hl, 100
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL122
__LABEL121:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld (63501), a
__LABEL118:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL114:
	ld hl, 58111
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL117
__LABEL116:
	ld hl, 58368
	ld (_eee), hl
	jp __LABEL124
__LABEL127:
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL129
__LABEL132:
__LABEL133:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL129:
	ld hl, 400
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL132
__LABEL131:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld (63501), a
__LABEL128:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL124:
	ld hl, 58879
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL127
__LABEL126:
	ld hl, 56832
	ld (_eee), hl
	jp __LABEL134
__LABEL137:
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL139
__LABEL142:
__LABEL143:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL139:
	ld hl, 100
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL142
__LABEL141:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld (63501), a
__LABEL138:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL134:
	ld hl, 57215
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL137
__LABEL136:
	jp __LABEL92
__LABEL93:
__LABEL144:
	jp __LABEL144
__LABEL145:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_sectionzputchardeprecated:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	ld de, 2048
	call __MUL16_FAST
	ld de, 57344
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	ld de, 2048
	call __MUL16_FAST
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_sectionzputchardeprecated__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ld de, 57344
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_sectionzputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzputbgtile:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 59392
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 60416
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_sectionzputbgtile__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	push af
	ld h, 127
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 56832
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 127
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 56833
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 127
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 56834
	add hl, de
	push hl
	ld a, (ix+7)
	cpl
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 127
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 56835
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_sectionzputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzpalettetest:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 61440
	add hl, de
	push hl
	ld a, (ix+7)
	push af
	ld h, 15
	pop af
	and h
	ld h, 16
	call __MUL8_FAST
	push af
	ld a, (ix+9)
	push af
	ld h, 15
	pop af
	and h
	ld h, a
	pop af
	or h
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 62464
	add hl, de
	push hl
	ld a, (ix+11)
	push af
	ld h, 15
	pop af
	and h
	ld h, 16
	call __MUL8_FAST
	pop hl
	ld (hl), a
_sectionzpalettetest__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzpalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 62464
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 15
	pop hl
	call __BAND16
	ld de, 16
	call __MUL16_FAST
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 61440
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	ld de, 16
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	pop hl
	call __BOR16
	ld a, l
	pop hl
	ld (hl), a
_sectionzpalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_sectionzwatchdog:
	push ix
	ld ix, 0
	add ix, sp
	ld a, 255
	ld (63501), a
_sectionzwatchdog__leave:
	ld sp, ix
	pop ix
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1237 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1238 "example01.zxb"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 1239 "example01.zxb"
#line 1 "mul8.asm"
__MUL8:		; Performs 8bit x 8bit multiplication
		PROC
	
		;LOCAL __MUL8A
		LOCAL __MUL8LOOP
		LOCAL __MUL8B
				; 1st operand (byte) in A, 2nd operand into the stack (AF)
		pop hl	; return address
		ex (sp), hl ; CALLE convention
	
;;__MUL8_FAST: ; __FASTCALL__ entry
	;;	ld e, a
	;;	ld d, 0
	;;	ld l, d
	;;	
	;;	sla h	
	;;	jr nc, __MUL8A
	;;	ld l, e
	;;
;;__MUL8A:
	;;
	;;	ld b, 7
;;__MUL8LOOP:
	;;	add hl, hl
	;;	jr nc, __MUL8B
	;;
	;;	add hl, de
	;;
;;__MUL8B:
	;;	djnz __MUL8LOOP
	;;
	;;	ld a, l ; result = A and HL  (Truncate to lower 8 bits)
	
__MUL8_FAST: ; __FASTCALL__ entry, a = a * h (8 bit mul) and Carry
	
	    ld b, 8
	    ld l, a
	    xor a
	
__MUL8LOOP:
	    add a, a ; a *= 2
	    sla l
	    jp nc, __MUL8B
	    add a, h
	
__MUL8B:
	    djnz __MUL8LOOP
		
		ret		; result = HL
		ENDP
	
#line 1240 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1241 "example01.zxb"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1242 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1243 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1244 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1245 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_seed EQU 49170
	_eee EQU 49168
	_ee1 EQU 49172
	_ee2 EQU 49174
	_ee3 EQU 49176
	_ee4 EQU 49178
	_eex EQU 49180
	_eey EQU 49182
	_ej0 EQU 49184
	_ej1 EQU 49186
	_ej2 EQU 49188
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
