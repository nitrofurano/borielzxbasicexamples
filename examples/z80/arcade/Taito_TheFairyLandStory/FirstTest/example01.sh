# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm flstory.zip
rm flstory/*
rmdir flstory
mkdir flstory

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=flstory/cpu-a45.15
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=flstory/cpu-a45.16
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=flstory/cpu-a45.17
rm example01.bin

#audio
dd bs=$((0x2000)) count=1 if=/dev/zero of=flstory/snd.22
dd bs=$((0x2000)) count=1 if=/dev/zero of=flstory/snd.23

#mcu
dd bs=$((0x0800)) count=1 if=/dev/urandom of=flstory/a45.mcu

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.18
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.06
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.08
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.20
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.19
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.07
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.09
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=flstory/vid-a45.21
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=flstory/flstory.5f
#rm spritemap01.bin

##palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=flstory/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=flstory/82s126.4a
#rm clut01.bin
##prom
#dd bs=256 count=1 if=/dev/zero of=flstory/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=flstory/82s126.3m

rm smsboot.bin dummy64k.bin

zip -r flstory flstory
rm flstory/*
rmdir flstory

mame -w -video soft -resolution0 512x512 -rp ./ flstory

#	ROM_REGION( 0x20000, "gfx1", ROMREGION_INVERT )
#	ROM_LOAD( "vid-a45.18",   0x00000, 0x4000, CRC(6f08f69e) SHA1(8f1b7e63a38f855cf26d57aed678da7cf1378fdf) )
#	ROM_LOAD( "vid-a45.06",   0x04000, 0x4000, CRC(dc856a75) SHA1(6eedbf6b027c884502b6e7329f13829787138165) )
#	ROM_LOAD( "vid-a45.08",   0x08000, 0x4000, CRC(d0b028ca) SHA1(c8bd9136ad3180002961ecfe600fc91a3c891539) )
#	ROM_LOAD( "vid-a45.20",   0x0c000, 0x4000, CRC(1b0edf34) SHA1(e749c78053ed09bdb42c03cf4589b0fe122d9095) )
#	ROM_LOAD( "vid-a45.19",   0x10000, 0x4000, CRC(2b572dc9) SHA1(9e14428663819e18829c625b4ae91a8a5530eb33) )
#	ROM_LOAD( "vid-a45.07",   0x14000, 0x4000, CRC(aa4b0762) SHA1(6d4246753e80fe3ca05d47bd279f7ccc603f4700) )
#	ROM_LOAD( "vid-a45.09",   0x18000, 0x4000, CRC(8336be58) SHA1(b92d37856870c4128a860d8ae02fa647743b99e3) )
#	ROM_LOAD( "vid-a45.21",   0x1c000, 0x4000, CRC(fc382bd1) SHA1(a773c87454a3d7b80374a6d38ecb8633af2cd990) )

#	ROM_REGION( 0x10000, "maincpu", 0 ) /* 64k for the first CPU */
#	ROM_LOAD( "cpu-a45.15",   0x0000, 0x4000, CRC(f03fc969) SHA1(c8dd25ca25fd413b1a29bd4e58ce5820e5f852b2) )
#	ROM_LOAD( "cpu-a45.16",   0x4000, 0x4000, CRC(311aa82e) SHA1(c2dd806f70ea917818ec844a275fb2fecc2e6c19) )
#	ROM_LOAD( "cpu-a45.17",   0x8000, 0x4000, CRC(a2b5d17d) SHA1(0198d048aedcbd2498d490a5c0c506f8fc66ed03) )
#
#	ROM_REGION( 0x10000, "audiocpu", 0 )    /* 64k for the second CPU */
#	ROM_LOAD( "snd.22",       0x0000, 0x2000, CRC(d58b201d) SHA1(1c9c2936ec95a8fa920d58668bea420c5e15008f) )
#	ROM_LOAD( "snd.23",       0x2000, 0x2000, CRC(25e7fd9d) SHA1(b9237459e3d8acf8502a693914e50714a37d515e) )
#
#	ROM_REGION( 0x0800, "mcu", 0 )  /* 2k for the microcontroller */
#	ROM_LOAD( "a45.mcu",      0x0000, 0x0800, CRC(5378253c) SHA1(e1ae1ab01e470b896c1d74ad4088928602a21a1b) )



