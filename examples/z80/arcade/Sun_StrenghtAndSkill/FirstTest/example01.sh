# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm strnskil.zip
rm strnskil/*
rmdir strnskil
mkdir strnskil

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=strnskil/tvg3.7
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=strnskil/tvg4.8
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=strnskil/tvg5.9
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=strnskil/tvg6.10


#subcpu
dd bs=$((0x2000)) count=1 if=/dev/zero of=strnskil/tvg1.2
dd bs=$((0x2000)) count=1 if=/dev/zero of=strnskil/tvg2.3

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
xxd -p -c 3 -s 0 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg7.90
xxd -p -c 3 -s 1 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg8.92
xxd -p -c 3 -s 2 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg9.94
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=strnskil/tvg7.90
#dd ibs=1 count=$((0x2000)) skip=$((0x0001)) if=charmap01b.bin of=strnskil/tvg8.92
#dd ibs=1 count=$((0x2000)) skip=$((0x0002)) if=charmap01b.bin of=strnskil/tvg9.94
#gfx2
xxd -p -c 3 -s 0 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg12.102
xxd -p -c 3 -s 1 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg11.101
xxd -p -c 3 -s 2 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > strnskil/tvg10.100
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=strnskil/tvg12.102
#dd ibs=1 count=$((0x2000)) skip=$((0x0001)) if=charmap01b.bin of=strnskil/tvg11.101
#dd ibs=1 count=$((0x2000)) skip=$((0x0002)) if=charmap01b.bin of=strnskil/tvg10.100
rm charmap01b.bin




#proms
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
#rgb
#dd ibs=1 count=$((0x100)) skip=$((0x0000)) if=palette01b.bin of=strnskil/15-3.prm
#dd ibs=1 count=$((0x100)) skip=$((0x0100)) if=palette01b.bin of=strnskil/15-4.prm
#dd ibs=1 count=$((0x100)) skip=$((0x0200)) if=palette01b.bin of=strnskil/15-5.prm
xxd -p -c 3 -s 0 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > strnskil/15-3.prm
xxd -p -c 3 -s 1 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > strnskil/15-4.prm
xxd -p -c 3 -s 2 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > strnskil/15-5.prm
rm palette01b.bin

zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
rm clut01.bin
#clut
dd ibs=1 count=$((0x200)) skip=$((0x0000)) if=clut01b.bin of=strnskil/15-1.prm
dd ibs=1 count=$((0x200)) skip=$((0x0200)) if=clut01b.bin of=strnskil/15-2.prm
dd ibs=1 count=$((0x100)) skip=$((0x0400)) if=clut01b.bin of=strnskil/15-6.prm
rm clut01b.bin





#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=strnskil/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=strnskil/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=strnskil/strnskil.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=strnskil/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=strnskil/82s126.3m

rm dummy64k.bin example01.bin

zip -r strnskil strnskil
rm strnskil/*
rmdir strnskil

rm smsboot.bin
mame -w -video soft -resolution0 512x512 -rp ./ strnskil

