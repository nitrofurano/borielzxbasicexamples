rm example01.asm
rm flower.zip
rm flower/*
rmdir flower
mkdir flower

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=flower/1.5j
rm dummy64k.bin example01.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=flower/2.5f

#sound
dd bs=$((0x4000)) count=1 if=/dev/zero of=flower/3.d9
dd bs=$((0x8000)) count=1 if=/dev/zero of=flower/4.12a
dd bs=$((0x4000)) count=1 if=/dev/zero of=flower/5.16a

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/10.13e
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/14.19e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/13.17e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/12.16e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/11.14e
#gfx3
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/8.10e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/6.7e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/9.12e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=flower/15.9e
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=flower/flower.5f
#rm spritemap01.bin

#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 3 -s 0 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > flower/82s129.k3
xxd -p -c 3 -s 1 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > flower/82s129.k2
xxd -p -c 3 -s 2 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > flower/82s129.k1
rm palette01b.bin
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=flower/82s129.k1
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=flower/82s129.k2
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=flower/82s129.k3
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=flower/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=flower/82s126.4a
#rm clut01.bin

#prom
dd bs=$((0x0200)) count=1 if=/dev/urandom of=flower/82s147.d7
dd bs=$((0x0200)) count=1 if=/dev/urandom of=flower/82s147.j18
dd bs=$((0x0020)) count=1 if=/dev/urandom of=flower/82s123.k7
dd bs=$((0x0100)) count=1 if=/dev/urandom of=flower/82s129.a1

rm smsboot.bin

zip -r flower flower
rm flower/*
rmdir flower

mame -w -video soft -resolution0 512x512 -rp ./ flower


#	user1 - Other proms, (zoom table?)
#	ROM_LOAD( "82s147.d7",  0x0000, 0x0200
#	ROM_LOAD( "82s147.j18", 0x0200, 0x0200
#	ROM_LOAD( "82s123.k7",  0x0400, 0x0020
#	ROM_LOAD( "82s129.a1",  0x0420, 0x0100

#----

#	maincpu
#	ROM_LOAD( "1.5j",   0x0000, 0x8000
#	subcpu
#	ROM_LOAD( "2.5f",   0x0000, 0x8000

#	audiocpu - sound cpu
#	ROM_LOAD( "3.d9",   0x0000, 0x4000 - 1xxxxxxxxxxxxx = 0xFF
#	sound1
#	ROM_LOAD( "4.12a",  0x0000, 0x8000 - 8-bit samples
#	sound2
#	ROM_LOAD( "5.16a",  0x0000, 0x4000 - volume tables?

#	gfx1 - tx layer
#	ROM_LOAD( "10.13e", 0x0000, 0x2000 - FIRST AND SECOND HALF IDENTICAL
#	gfx2 - sprites
#	ROM_LOAD( "14.19e", 0x0000, 0x2000
#	ROM_LOAD( "13.17e", 0x2000, 0x2000
#	ROM_LOAD( "12.16e", 0x4000, 0x2000
#	ROM_LOAD( "11.14e", 0x6000, 0x2000
#	gfx3 - bg layers
#	ROM_LOAD( "8.10e",  0x0000, 0x2000
#	ROM_LOAD( "6.7e",   0x2000, 0x2000
#	ROM_LOAD( "9.12e",  0x4000, 0x2000
#	ROM_LOAD( "15.9e",  0x6000, 0x2000

#	proms - RGB proms
#	ROM_LOAD( "82s129.k1",  0x0200, 0x0100 - r
#	ROM_LOAD( "82s129.k2",  0x0100, 0x0100 - g
#	ROM_LOAD( "82s129.k3",  0x0000, 0x0100 - b


s

