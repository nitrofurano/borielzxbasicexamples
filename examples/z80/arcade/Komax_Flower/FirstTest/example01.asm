	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$DDFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 57344
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	xor a
	ld hl, (_eee)
	ld (hl), a
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 58367
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL10
__LABEL13:
	ld a, 4
	push af
	ld hl, (_ee2)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee3)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee2)
	ld de, 12
	add hl, de
	push hl
	ld hl, (_ee3)
	ld de, 16
	add hl, de
	push hl
	call _komaxflowerputchar
__LABEL14:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL10:
	ld hl, 15
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL5:
	ld hl, 15
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 56832
	ld (_eee), hl
	jp __LABEL15
__LABEL18:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL19:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL15:
	ld hl, 57343
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 4
	ld (_ex0), hl
	ld hl, 4
	ld (_ey0), hl
	ld hl, 0
	ld (_eee), hl
__LABEL20:
	ld a, (41216)
	cpl
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL23
	ld hl, (_ey0)
	dec hl
	ld (_ey0), hl
__LABEL23:
	ld de, 2
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL25
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL25:
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL27
	ld hl, (_ex0)
	dec hl
	ld (_ex0), hl
__LABEL27:
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL29
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL29:
	ld a, 5
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 8
	push hl
	call _komaxflowerputchar
	ld a, 5
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 7
	push hl
	call _komaxflowerputchar
	ld a, 5
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 6
	push hl
	call _komaxflowerputchar
	ld hl, (_ey0)
	srl h
	rr l
	ld de, 0
	ld a, l
	ld (61952), a
	ld hl, (_ey0)
	ld de, 0
	ld a, l
	ld (64000), a
	ld a, 6
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld de, 31
	ld hl, (_ey0)
	call __BAND16
	push hl
	ld de, 31
	ld hl, (_ex0)
	call __BAND16
	push hl
	call _komaxflowerputchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	ld hl, 1
	ld (_ee5), hl
	jp __LABEL30
__LABEL33:
__LABEL34:
	ld hl, (_ee5)
	inc hl
	ld (_ee5), hl
__LABEL30:
	ld hl, 1000
	ld de, (_ee5)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
	jp __LABEL20
__LABEL21:
__LABEL35:
	jp __LABEL35
__LABEL36:
	ld hl, 58368
	ld (_eee), hl
	jp __LABEL37
__LABEL40:
	xor a
	ld hl, (_eee)
	ld (hl), a
__LABEL41:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL37:
	ld hl, 59391
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL40
__LABEL39:
	ld hl, 56832
	ld (_eee), hl
	jp __LABEL42
__LABEL45:
	ld hl, 1
	ld (_i), hl
	jp __LABEL47
__LABEL50:
__LABEL51:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL47:
	ld hl, 100
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL50
__LABEL49:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL46:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL42:
	ld hl, 57343
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL45
__LABEL44:
__LABEL52:
	jp __LABEL52
__LABEL53:
__LABEL54:
	ld hl, 61440
	ld (_eee), hl
	jp __LABEL56
__LABEL59:
	ld hl, 1
	ld (_i), hl
	jp __LABEL61
__LABEL64:
__LABEL65:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL61:
	ld hl, 100
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL64
__LABEL63:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL60:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL56:
	ld hl, 61951
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL59
__LABEL58:
	ld hl, 55296
	ld (_eee), hl
	jp __LABEL66
__LABEL69:
	ld hl, 1
	ld (_i), hl
	jp __LABEL71
__LABEL74:
__LABEL75:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL71:
	ld hl, 100
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL74
__LABEL73:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL70:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL66:
	ld hl, 63999
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL69
__LABEL68:
	jp __LABEL54
__LABEL55:
__LABEL76:
	jp __LABEL76
__LABEL77:
	ld hl, 57344
	ld (_eee), hl
	jp __LABEL78
__LABEL81:
	ld hl, 1
	ld (_i), hl
	jp __LABEL83
__LABEL86:
__LABEL87:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL83:
	ld hl, 100
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL86
__LABEL85:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL82:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL78:
	ld hl, 59391
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL81
__LABEL80:
	ld hl, 56832
	ld (_eee), hl
	jp __LABEL88
__LABEL91:
	ld hl, 1
	ld (_i), hl
	jp __LABEL93
__LABEL96:
__LABEL97:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL93:
	ld hl, 100
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL96
__LABEL95:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL92:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL88:
	ld hl, 57343
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL91
__LABEL90:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_komaxflowerputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	push hl
	push hl
	inc sp
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 57344
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	push ix
	pop hl
	ld de, -5
	add hl, de
	call __PLOADF
	call __FTOU32REG
	ld a, l
	pop hl
	ld (hl), a
_komaxflowerputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 593 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 594 "example01.zxb"
#line 1 "ploadf.asm"
	; Parameter / Local var load
	; A => Offset
	; IX = Stack Frame
; RESULT: HL => IX + DE
	
#line 1 "iloadf.asm"
	; __FASTCALL__ routine which
	; loads a 40 bits floating point into A ED CB
	; stored at position pointed by POINTER HL
	;A DE, BC <-- ((HL))
	
__ILOADF:
	    ld a, (hl)
	    inc hl
	    ld h, (hl)
	    ld l, a
	
	; __FASTCALL__ routine which
	; loads a 40 bits floating point into A ED CB
	; stored at position pointed by POINTER HL
	;A DE, BC <-- (HL)
	
__LOADF:    ; Loads a 40 bits FP number from address pointed by HL
		ld a, (hl)	
		inc hl
		ld e, (hl)
		inc hl
		ld d, (hl)
		inc hl
		ld c, (hl)
		inc hl
		ld b, (hl)
		ret
	
#line 7 "ploadf.asm"
	
__PLOADF:
	    push ix
	    pop hl
	    add hl, de
	    jp __LOADF
	   
#line 595 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 596 "example01.zxb"
#line 1 "ftou32reg.asm"
	
	
__FTOU32REG:	; Converts a Float to (un)signed 32 bit integer (NOTE: It's ALWAYS 32 bit signed)
					; Input FP number in A EDCB (A exponent, EDCB mantissa)
				; Output: DEHL 32 bit number (signed)
		PROC
	
		LOCAL __IS_FLOAT
	
		or a
		jr nz, __IS_FLOAT 
		; Here if it is a ZX ROM Integer
	
		ld h, c
		ld l, d
	ld a, e	 ; Takes sign: FF = -, 0 = +
		ld de, 0
		inc a
		jp z, __NEG32	; Negates if negative
		ret
	
__IS_FLOAT:  ; Jumps here if it is a true floating point number
		ld h, e	
		push hl  ; Stores it for later (Contains Sign in H)
	
		push de
		push bc
	
		exx
		pop de   ; Loads mantissa into C'B' E'D' 
		pop bc	 ; 
	
		set 7, c ; Highest mantissa bit is always 1
		exx
	
		ld hl, 0 ; DEHL = 0
		ld d, h
		ld e, l
	
		;ld a, c  ; Get exponent
		sub 128  ; Exponent -= 128
		jr z, __FTOU32REG_END	; If it was <= 128, we are done (Integers must be > 128)
		jr c, __FTOU32REG_END	; It was decimal (0.xxx). We are done (return 0)
	
		ld b, a  ; Loop counter = exponent - 128
	
__FTOU32REG_LOOP:
		exx 	 ; Shift C'B' E'D' << 1, output bit stays in Carry
		sla d
		rl e
		rl b
		rl c
	
	    exx		 ; Shift DEHL << 1, inserting the carry on the right
		rl l
		rl h
		rl e
		rl d
	
		djnz __FTOU32REG_LOOP
	
__FTOU32REG_END:
		pop af   ; Take the sign bit
		or a	 ; Sets SGN bit to 1 if negative
		jp m, __NEG32 ; Negates DEHL
		
		ret
	
		ENDP
	
	
__FTOU8:	; Converts float in C ED LH to Unsigned byte in A
		call __FTOU32REG
		ld a, l
		ret
	
#line 597 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 598 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 599 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_i EQU 49168
	_eee EQU 49170
	_seed EQU 49172
	_ee2 EQU 49174
	_ee3 EQU 49176
	_ee4 EQU 49178
	_ee5 EQU 49180
	_ej0 EQU 49184
	_ej1 EQU 49186
	_ex0 EQU 49188
	_ey0 EQU 49190
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
