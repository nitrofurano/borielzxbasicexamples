# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm 1942/*
rmdir 1942
mkdir 1942

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=1942/srb-03.m3
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=1942/srb-04.m4
rm example01.bin
dd bs=$((0x4000)) count=1 if=/dev/zero of=1942/srb-05.m5
dd bs=$((0x2000)) count=1 if=/dev/zero of=1942/srb-06.m6
dd bs=$((0x4000)) count=1 if=/dev/zero of=1942/srb-07.m7

dd bs=$((0x4000)) count=1 if=/dev/zero of=1942/sr-01.c11


zxb.py library/charmap1942_01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap1942_01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap1942_01.bin of=charmap1942_01b.bin
rm charmap1942_01.bin
#characters
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap1942_01b.bin of=1942/sr-02.f2
#tiles
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap1942_01b.bin of=1942/sr-08.a1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmap1942_01b.bin of=1942/sr-09.a2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=charmap1942_01b.bin of=1942/sr-10.a3
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=charmap1942_01b.bin of=1942/sr-11.a4
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=charmap1942_01b.bin of=1942/sr-12.a5
dd ibs=1 count=$((0x2000)) skip=$((0xA000)) if=charmap1942_01b.bin of=1942/sr-13.a6
#sprites
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap1942_01b.bin of=1942/sr-14.l1
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=charmap1942_01b.bin of=1942/sr-15.l2
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=charmap1942_01b.bin of=1942/sr-16.n1
dd ibs=1 count=$((0x4000)) skip=$((0xC000)) if=charmap1942_01b.bin of=1942/sr-17.n2
rm charmap1942_01b.bin

#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=1942/1942.5f
#rm spritemap01.bin


zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0300)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 3 -s 0 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > 1942/sb-5.e8   #r
xxd -p -c 3 -s 1 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > 1942/sb-6.e9   #g
xxd -p -c 3 -s 2 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > 1942/sb-7.e10  #b
rm palette01b.bin
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-5.e8
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-6.e9
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-7.e10



zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x0300)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
rm clut01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=1942/sb-0.f1  #char
dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=1942/sb-4.d6  #tile
dd ibs=1 count=$((0x0100)) skip=$((0x0200)) if=clut01b.bin of=1942/sb-8.k3  #spri

rm clut01b.bin
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-0.f1
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-4.d6
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-8.k3


dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-2.d1
dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-3.d2
dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-1.k6
dd bs=$((0x0100)) count=1 if=/dev/urandom of=1942/sb-9.m11

zip -r 1942 1942
rm 1942/*
rmdir 1942
rm smsboot.bin dummy64k.bin

mame -w -video soft -resolution0 512x512 -rp ./ 1942


