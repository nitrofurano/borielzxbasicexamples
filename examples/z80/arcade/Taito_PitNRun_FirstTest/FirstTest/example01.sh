# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm pitnrun.zip
rm pitnrun/*
rmdir pitnrun
mkdir pitnrun

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=pitnrun/pr12
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=pitnrun/pr11
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=pitnrun/pr10
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=pitnrun/pr9
rm example01.bin

#sound
dd bs=$((0x1000)) count=1 if=/dev/zero of=pitnrun/pr13

#mcu
dd bs=$((0x800)) count=1 if=/dev/zero of=pitnrun/a11_17.3a

#gfx (pitnrun tiles uses pairs of sectionz tiles?)
zxb.py library/charmapsectionz_01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmapsectionz_01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmapsectionz_01.bin of=charmapsectionz_01b.bin
rm charmapsectionz_01.bin
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr4
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr5
#gfx3
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr6
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr7
#gfx1 (sprites?)
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapsectionz_01b.bin of=pitnrun/pr3
rm charmapsectionz_01b.bin

#user (?)
dd bs=$((0x2000)) count=1 if=/dev/urandom of=pitnrun/pr8

#proms
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=pitnrun/clr.1
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=pitnrun/clr.2
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=pitnrun/clr.3
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x020)) skip=$((0x0010)) if=palette01.bin of=pitnrun/clr.1
dd ibs=1 count=$((0x020)) skip=$((0x0030)) if=palette01.bin of=pitnrun/clr.2
dd ibs=1 count=$((0x020)) skip=$((0x0050)) if=palette01.bin of=pitnrun/clr.3
rm palette01.bin


#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=pitnrun/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=pitnrun/pitnrun.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=pitnrun/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=pitnrun/82s126.3m

zip -r pitnrun pitnrun
rm pitnrun/*
rmdir pitnrun

rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ pitnrun

