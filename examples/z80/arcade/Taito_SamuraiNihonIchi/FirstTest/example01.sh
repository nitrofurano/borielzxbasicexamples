# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm nunchaku.zip
rm nunchaku/*
rmdir nunchaku
mkdir nunchaku

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=nunchaku/nunchack.p1
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=nunchaku/nunchack.p2
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=nunchaku/nunchack.p3
rm dummy64k.bin example01.bin

#audio
dd bs=$((0x2000)) count=1 if=/dev/zero of=nunchaku/nunchack.m3
dd bs=$((0x2000)) count=1 if=/dev/zero of=nunchaku/nunchack.m4
dd bs=$((0x2000)) count=1 if=/dev/zero of=nunchaku/nunchack.m1
dd bs=$((0x2000)) count=1 if=/dev/zero of=nunchaku/nunchack.m2

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
xxd -p -c 3 -s 0 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.b1
xxd -p -c 3 -s 1 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.b2
xxd -p -c 3 -s 2 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.b3
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=nunchaku/nunchack.b1
#dd ibs=1 count=$((0x2000)) skip=$((0x0001)) if=charmap01b.bin of=nunchaku/nunchack.b2
#dd ibs=1 count=$((0x2000)) skip=$((0x0002)) if=charmap01b.bin of=nunchaku/nunchack.b3
#gfx2
xxd -p -c 3 -s 0 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.v1
xxd -p -c 3 -s 1 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.v2
xxd -p -c 3 -s 2 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.v3
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmap01b.bin of=nunchaku/nunchack.v1
#dd ibs=1 count=$((0x1000)) skip=$((0x0001)) if=charmap01b.bin of=nunchaku/nunchack.v2
#dd ibs=1 count=$((0x1000)) skip=$((0x0002)) if=charmap01b.bin of=nunchaku/nunchack.v3
#gfx3
xxd -p -c 3 -s 0 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.c1
xxd -p -c 3 -s 1 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.c2
xxd -p -c 3 -s 2 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > nunchaku/nunchack.c3
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=nunchaku/nunchack.c1
#dd ibs=1 count=$((0x4000)) skip=$((0x0001)) if=charmap01b.bin of=nunchaku/nunchack.c2
#dd ibs=1 count=$((0x4000)) skip=$((0x0002)) if=charmap01b.bin of=nunchaku/nunchack.c3
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=nunchaku/nunchaku.5f
#rm spritemap01.bin

#palette
#prom
dd bs=$((0x0100)) count=1 if=/dev/urandom of=nunchaku/nunchack.016
dd bs=$((0x0100)) count=1 if=/dev/urandom of=nunchaku/nunchack.017
dd bs=$((0x0100)) count=1 if=/dev/urandom of=nunchaku/nunchack.018
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=nunchaku/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=nunchaku/82s126.4a
#rm clut01.bin

rm smsboot.bin

zip -r nunchaku nunchaku
rm nunchaku/*
rmdir nunchaku

mame -w -video soft -resolution0 512x512 -rp ./ nunchaku





# ROM_REGION( 0x0300, "proms", 0 )
# ROM_LOAD( "nunchack.016", 0x000, 0x100, CRC(a7b077d4) SHA1(48c3e68d67de067c0ead0dbd34769b755fb5952f) )
# ROM_LOAD( "nunchack.017", 0x100, 0x100, CRC(1c04c087) SHA1(7179edf96f59a469353d9652900b99fef25f4054) )
# ROM_LOAD( "nunchack.018", 0x200, 0x100, CRC(f5ce3c45) SHA1(f2dcdaf95b55b8fd713bdbb965731c064b4a0757) )
#--------------
# ROM_REGION( 0x10000, "maincpu", 0 ) /* Z80 code  - main CPU */
# ROM_LOAD( "nunchack.p1", 0x0000, 0x4000, CRC(4385aca6) SHA1(bf6b40340b773929189fb2a0a271040c79a405a1) )
# ROM_LOAD( "nunchack.p2", 0x4000, 0x4000, CRC(f9beb72c) SHA1(548dc9187f87d7a47958691391d2494c2306d767) )
# ROM_LOAD( "nunchack.p3", 0x8000, 0x4000, CRC(cde5d674) SHA1(0360fe81acb6fd77ef581a36a756db550de73732) )

# ROM_REGION( 0x10000, "audiocpu", 0 ) /* Z80 code - sample player */
# ROM_LOAD( "nunchack.m3", 0x0000, 0x2000, CRC(9036c945) SHA1(8e7cb6313b32a78ca0a7fa8595fb872e0f27d8c7) )
# ROM_LOAD( "nunchack.m4", 0x2000, 0x2000, CRC(e7206724) SHA1(fb7f9bfe1e04e1f6af733fc3a79f88e942f3b0b1) )

# ROM_REGION( 0x10000, "audio2", 0 ) /* Z80 code - sample player */
# ROM_LOAD( "nunchack.m1", 0x0000, 0x2000, CRC(b53d73f6) SHA1(20b333646a1374fa566b6d608723296e6ded7bc8) )
# ROM_LOAD( "nunchack.m2", 0x2000, 0x2000, CRC(f37d7c49) SHA1(e26d32bf1ecf23b55511260596daf676d9824d37) )

# ROM_REGION( 0x6000, "gfx1", 0 )
# ROM_LOAD( "nunchack.b1", 0x0000, 0x2000, CRC(48c88fea) SHA1(2ab27fc69f060e8923f88f9e878e3911d670f5a8) ) // tiles
# ROM_LOAD( "nunchack.b2", 0x2000, 0x2000, CRC(eec818e4) SHA1(1d806dbf6589737e3a4fb52f17bc4c6766a6d6a1) )
# ROM_LOAD( "nunchack.b3", 0x4000, 0x2000, CRC(5f16473f) SHA1(32d8b4a0a7152d86f161d0f30496c25ceff46af3) )

# ROM_REGION( 0x6000, "gfx2", 0 )
# ROM_LOAD( "nunchack.v1", 0x0000, 0x1000, CRC(358a3714) SHA1(bf54bf4059cd344d4d861e172b5df5b7763d49d8) ) // characters
# ROM_LOAD( "nunchack.v2", 0x2000, 0x1000, CRC(54c18d8e) SHA1(edcd0a6b5fb1efa2f3693e170cb398574280f7fa) )
# ROM_LOAD( "nunchack.v3", 0x4000, 0x1000, CRC(f7ac203a) SHA1(148a003b48d858eb33f0fee295350483cef42481) )

# ROM_REGION( 0xc000, "gfx3", 0 )
# ROM_LOAD( "nunchack.c1", 0x0000, 0x4000, CRC(797cbc8a) SHA1(28ad936318d8b671d4031927c2ea666891a1a408) ) // sprites
# ROM_LOAD( "nunchack.c2", 0x4000, 0x4000, CRC(701a0cc3) SHA1(323ee1cba3da0ccb2c4d542c497de0e1c047f532) )
# ROM_LOAD( "nunchack.c3", 0x8000, 0x4000, CRC(ffb841fc) SHA1(c1285cf093360923307bc86f6a5473d689b16a2c) )

