
rm example01.asm
rm solomon.zip
rm solomon/*
rmdir solomon
mkdir solomon

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin





#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=solomon/6.3f
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=solomon/7.3h_b
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=solomon/7.3h
dd ibs=1 count=$((0x1000)) skip=$((0xF000)) if=example01.bin of=solomon/8.3jk
dd bs=$((0x7000)) count=1 if=/dev/zero of=solomon/8.3jk_b
cat solomon/7.3h_b >> solomon/7.3h
cat solomon/8.3jk_b >> solomon/8.3jk
rm solomon/7.3h_b solomon/8.3jk_b
rm example01.bin

#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "6.3f",  0x00000, 0x4000, CRC(645eb0f3) SHA1(b911aa157ad94aa539dbe329a197d8902860c809) )
#	ROM_LOAD( "7.3h",  0x08000, 0x4000, CRC(1bf5c482) SHA1(3b6a8dde72cddf95438539c5dc4622c95f932a04) )
#	ROM_CONTINUE(      0x04000, 0x4000 )
#	ROM_LOAD( "8.3jk", 0x0f000, 0x1000, CRC(0a6cdefc) SHA1(101acaa19b779cb8b4fffddbe63fe011c7d4b6e9) )
#	ROM_IGNORE(                 0x7000 )





#sound
dd bs=$((0x4000)) count=1 if=/dev/zero of=solomon/1.3jk

#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "1.3jk",  0x0000, 0x4000, CRC(fa6e562e) SHA1(713036c0a80b623086aa674bb5f8a135b6fedb01) )





#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=solomon/12.3t
dd ibs=1 count=$((0x8000)) skip=$((0x0001)) if=charmap01b.bin of=solomon/11.3r
#gfx2
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=solomon/10.3p
dd ibs=1 count=$((0x8000)) skip=$((0x0001)) if=charmap01b.bin of=solomon/9.3m
#gfx3
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=solomon/2.5lm
dd ibs=1 count=$((0x4000)) skip=$((0x0001)) if=charmap01b.bin of=solomon/3.6lm
dd ibs=1 count=$((0x4000)) skip=$((0x0002)) if=charmap01b.bin of=solomon/4.7lm
dd ibs=1 count=$((0x4000)) skip=$((0x0003)) if=charmap01b.bin of=solomon/5.8lm
rm charmap01b.bin

#	ROM_REGION( 0x10000, "gfx1", 0 )
#	ROM_LOAD( "12.3t",  0x00000, 0x08000, CRC(b371291c) SHA1(27302898c64330870c47025e61bd5acbd9483865) )    /* characters */
#	ROM_LOAD( "11.3r",  0x08000, 0x08000, CRC(6f94d2af) SHA1(2e070c0fd5b9d7eb9b7e0d53f25b1a5063ef3095) )
#
#	ROM_REGION( 0x10000, "gfx2", 0 )
#	ROM_LOAD( "10.3p",  0x00000, 0x08000, CRC(8310c2a1) SHA1(8cc87ab8faacdb1973791d207bb25ea9b444b66d) )
#	ROM_LOAD( "9.3m",   0x08000, 0x08000, CRC(ab7e6c42) SHA1(0fc3b4a0bd2b17b79e2d1f7d4fe445c09ce4e730) )
#
#	ROM_REGION( 0x10000, "gfx3", 0 )
#	ROM_LOAD( "2.5lm",  0x00000, 0x04000, CRC(80fa2be3) SHA1(8e7a78186473a6b5c42577ac9e4591ee2d1151f2) )    /* sprites */
#	ROM_LOAD( "3.6lm",  0x04000, 0x04000, CRC(236106b4) SHA1(8eaf3150568c407bd8dc1cdf874b8417e5cca3d2) )
#	ROM_LOAD( "4.7lm",  0x08000, 0x04000, CRC(088fe5d9) SHA1(e29ffb9fcff50ce982d5e502e10a8e29a4c47390) )
#	ROM_LOAD( "5.8lm",  0x0c000, 0x04000, CRC(8366232a) SHA1(1c7a01dab056ec7d787a6f55772b9fa6fe67305a) )






#samples
#dd bs=$((0x4000)) count=1 if=/dev/zero of=solomon/cpu_1f.bin
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=solomon/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=solomon/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=solomon/solomon.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=solomon/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=solomon/82s126.3m




rm smsboot.bin dummy64k.bin

zip -r solomon solomon
rm solomon/*
rmdir solomon

mame -w -video soft -resolution0 512x512 -rp ./ solomon
#xmame.SDL -rp ./ solomon



#ROM_START( solomon )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "6.3f",  0x00000, 0x4000, CRC(645eb0f3) SHA1(b911aa157ad94aa539dbe329a197d8902860c809) )
#	ROM_LOAD( "7.3h",  0x08000, 0x4000, CRC(1bf5c482) SHA1(3b6a8dde72cddf95438539c5dc4622c95f932a04) )
#	ROM_CONTINUE(      0x04000, 0x4000 )
#	ROM_LOAD( "8.3jk", 0x0f000, 0x1000, CRC(0a6cdefc) SHA1(101acaa19b779cb8b4fffddbe63fe011c7d4b6e9) )
#	ROM_IGNORE(                 0x7000 )
#
#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "1.3jk",  0x0000, 0x4000, CRC(fa6e562e) SHA1(713036c0a80b623086aa674bb5f8a135b6fedb01) )
#
#	ROM_REGION( 0x10000, "gfx1", 0 )
#	ROM_LOAD( "12.3t",  0x00000, 0x08000, CRC(b371291c) SHA1(27302898c64330870c47025e61bd5acbd9483865) )    /* characters */
#	ROM_LOAD( "11.3r",  0x08000, 0x08000, CRC(6f94d2af) SHA1(2e070c0fd5b9d7eb9b7e0d53f25b1a5063ef3095) )
#
#	ROM_REGION( 0x10000, "gfx2", 0 )
#	ROM_LOAD( "10.3p",  0x00000, 0x08000, CRC(8310c2a1) SHA1(8cc87ab8faacdb1973791d207bb25ea9b444b66d) )
#	ROM_LOAD( "9.3m",   0x08000, 0x08000, CRC(ab7e6c42) SHA1(0fc3b4a0bd2b17b79e2d1f7d4fe445c09ce4e730) )
#
#	ROM_REGION( 0x10000, "gfx3", 0 )
#	ROM_LOAD( "2.5lm",  0x00000, 0x04000, CRC(80fa2be3) SHA1(8e7a78186473a6b5c42577ac9e4591ee2d1151f2) )    /* sprites */
#	ROM_LOAD( "3.6lm",  0x04000, 0x04000, CRC(236106b4) SHA1(8eaf3150568c407bd8dc1cdf874b8417e5cca3d2) )
#	ROM_LOAD( "4.7lm",  0x08000, 0x04000, CRC(088fe5d9) SHA1(e29ffb9fcff50ce982d5e502e10a8e29a4c47390) )
#	ROM_LOAD( "5.8lm",  0x0c000, 0x04000, CRC(8366232a) SHA1(1c7a01dab056ec7d787a6f55772b9fa6fe67305a) )
#ROM_END




