	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$CFFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_ee0)
	push hl
	call _solomonskeypalettergb
__LABEL4:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL0:
	ld hl, 255
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 3840
	push hl
	ld hl, 0
	push hl
	call _solomonskeypalettergb
	ld hl, 240
	push hl
	ld hl, 1
	push hl
	call _solomonskeypalettergb
	ld hl, 15
	push hl
	ld hl, 2
	push hl
	call _solomonskeypalettergb
	ld hl, 1792
	push hl
	ld hl, 3
	push hl
	call _solomonskeypalettergb
	ld hl, 112
	push hl
	ld hl, 4
	push hl
	call _solomonskeypalettergb
	ld hl, 7
	push hl
	ld hl, 5
	push hl
	call _solomonskeypalettergb
	ld hl, 1092
	push hl
	ld hl, 128
	push hl
	call _solomonskeypalettergb
	ld hl, 1092
	push hl
	ld hl, 0
	push hl
	call _solomonskeypalettergb
	ld hl, 1638
	push hl
	ld hl, 5
	push hl
	call _solomonskeypalettergb
	ld hl, 2184
	push hl
	ld hl, 10
	push hl
	call _solomonskeypalettergb
	ld hl, 2730
	push hl
	ld hl, 15
	push hl
	call _solomonskeypalettergb
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL10
__LABEL13:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld (_ee2), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, 0
	push hl
	ld de, 240
	ld hl, (_ee2)
	call __BAND16
	ld a, l
	push af
	ld de, 15
	ld hl, (_seed)
	call __BAND16
	ld a, l
	push af
	ld hl, (_ee0)
	push hl
	ld hl, (_ee1)
	push hl
	call _solomonskeyputchar
__LABEL14:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL10:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL5:
	ld hl, 31
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL20
__LABEL23:
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee0)
	ld de, 13
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 14
	add hl, de
	push hl
	call _solomonskeyputchar
__LABEL24:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL20:
	ld hl, 16
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL15:
	ld hl, 16
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 8
	ld (_eex), hl
	ld hl, 8
	ld (_eey), hl
	ld hl, 0
	ld (_eee), hl
__LABEL25:
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld a, (58880)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld de, 1
	ld hl, (_ee0)
	call __BAND16
	ex de, hl
	ld hl, (_eex)
	add hl, de
	push hl
	ld de, 2
	ld hl, (_ee0)
	call __BAND16
	srl h
	rr l
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_eex), hl
	ld de, 4
	ld hl, (_ee0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_eey)
	or a
	sbc hl, de
	push hl
	ld de, 8
	ld hl, (_ee0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	add hl, de
	ld (_eey), hl
	ld a, (58881)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld a, (58882)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld a, (58883)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld a, (58884)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld a, (58885)
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 1
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 2
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 3
	push hl
	call _solomonskeyputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_eey)
	push hl
	ld hl, (_eex)
	push hl
	call _solomonskeyputchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	ld hl, 10000
	push hl
	call _smsdelay
	jp __LABEL25
__LABEL26:
__LABEL27:
	jp __LABEL27
__LABEL28:
__LABEL29:
	ld hl, 53248
	ld (_eee), hl
	jp __LABEL31
__LABEL34:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL35:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL31:
	ld hl, 54271
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL34
__LABEL33:
	ld hl, 54272
	ld (_eee), hl
	jp __LABEL36
__LABEL39:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL40:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL36:
	ld hl, 55295
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL39
__LABEL38:
	ld hl, 55296
	ld (_eee), hl
	jp __LABEL41
__LABEL44:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL45:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL41:
	ld hl, 56319
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL44
__LABEL43:
	ld hl, 56320
	ld (_eee), hl
	jp __LABEL46
__LABEL49:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL50:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL46:
	ld hl, 57343
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL49
__LABEL48:
	ld hl, 57344
	ld (_eee), hl
	jp __LABEL51
__LABEL54:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL55:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL51:
	ld hl, 57471
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL54
__LABEL53:
	ld hl, 58368
	ld (_eee), hl
	jp __LABEL56
__LABEL59:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL60:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL56:
	ld hl, 58879
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL59
__LABEL58:
	jp __LABEL29
__LABEL30:
__LABEL61:
	jp __LABEL61
__LABEL62:
	jp __LABEL__flg02
#line 178
		org $F000
#line 179
__LABEL__flg02:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_smsdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
smsdelayloop:
		dec bc
		ld a,b
		or c
		jp nz,smsdelayloop
#line 8
_smsdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_solomonskeyputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 54272
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_solomonskeyputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_solomonskeypalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
_solomonskeypalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_solomonskeypalettergb:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 256
	call __DIVU16
	ex de, hl
	pop hl
	call __BOR16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 15
	pop hl
	call __BAND16
	ld de, 256
	call __MUL16_FAST
	ex de, hl
	pop hl
	call __BOR16
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
_solomonskeypalettergb__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_solomonskeyputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 57344
	add hl, de
	push hl
	ld a, (ix+7)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 57345
	add hl, de
	push hl
	ld a, (ix+9)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 57346
	add hl, de
	push hl
	ld a, (ix+11)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 57347
	add hl, de
	push hl
	ld a, (ix+13)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
_solomonskeyputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1259 "example01.zxb"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 1260 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1261 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1262 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1263 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1264 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_eee EQU 49168
	_seed EQU 49170
	_ee0 EQU 49172
	_ee1 EQU 49174
	_ee2 EQU 49176
	_ee3 EQU 49178
	_ee4 EQU 49180
	_eex EQU 49184
	_eey EQU 49186
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
