rm warpsped.zip
rm example01.asm
rm warpsped/*
rmdir warpsped
mkdir warpsped





dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0000)) if=example01.bin of=warpsped/m16\ pro\ 0.c5
dd ibs=1 count=$((0x0200)) skip=$((0x0200)) if=example01.bin of=warpsped/m16\ pro\ 1.e5
dd ibs=1 count=$((0x0200)) skip=$((0x0400)) if=example01.bin of=warpsped/m16\ pro\ 2.c6
dd ibs=1 count=$((0x0200)) skip=$((0x0600)) if=example01.bin of=warpsped/m16\ pro\ 3.e6
dd ibs=1 count=$((0x0200)) skip=$((0x0800)) if=example01.bin of=warpsped/m16\ pro\ 4.c4
dd ibs=1 count=$((0x0200)) skip=$((0x0A00)) if=example01.bin of=warpsped/m16\ pro\ 5.e4
dd ibs=1 count=$((0x0200)) skip=$((0x0C00)) if=example01.bin of=warpsped/m16\ pro\ 6.c3

#ROM_START( warpsped )
#ROM_REGION(0x1000, "maincpu", 0)
#ROM_LOAD( "m16 pro 0.c5",  0x0000, 0x0200, CRC(81f33dfb) SHA1(5c4adef88e1e7f1a9f7c156f17b98ba522ddee81) )
#ROM_LOAD( "m16 pro 1.e5",  0x0200, 0x0200, CRC(135f7421) SHA1(0cabe9a2590fe4f81976a4d10e9b2a223a2d875e) )
#ROM_LOAD( "m16 pro 2.c6",  0x0400, 0x0200, CRC(0a36d152) SHA1(83a2e8f78a36e512da81c44a5ceca7f865bc3508) )
#ROM_LOAD( "m16 pro 3.e6",  0x0600, 0x0200, CRC(ba416cca) SHA1(afb831a79a4a4334fa4caf4e2244c2d4e2f25853) )
#ROM_LOAD( "m16 pro 4.c4",  0x0800, 0x0200, CRC(fc44f25b) SHA1(185f593f2ec6075fd68869d87ed584908031100a) )
#ROM_LOAD( "m16 pro 5.e4",  0x0a00, 0x0200, CRC(7a16bc2b) SHA1(48f58f0c7469da24a3ebee9183b5aae8c676e6ea) )
#ROM_LOAD( "m16 pro 6.c3",  0x0c00, 0x0200, CRC(e2e7940f) SHA1(78c9df32580784c278675d09b89095781893d48f) )







zxb.py library/b1r3f0_charmapspaceintruder01.bas --org=$((0x0000))
cat dummy32k.bin >> b1r3f0_charmapspaceintruder01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=warpsped/k1.g1
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=warpsped/k1.k1
rm b1r3f0_charmapspaceintruder01.bin

#ROM_REGION(0x0200, "gfx1", 0)
#ROM_LOAD( "k1.g1",  0x0000, 0x0200, CRC(63d4fa84) SHA1(3465ce27497e2d4fcae994c022480e37e1345686) )
#ROM_REGION(0x0200, "gfx2", 0)
#ROM_LOAD( "k1.k1",  0x0000, 0x0200, CRC(76b10d47) SHA1(e644a50df06535fe1fbfb8754cfc7b4a49fcb05e) )








dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l9\ l13\ l17\ g17.g17
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l10\ l15\ l18\ g18.g18
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l9\ l13\ l17\ g17.l9
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l10\ l15\ l18\ g18.l10
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l9\ l13\ l17\ g17.l13
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l10\ l15\ l18\ g18.l15
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l9\ l13\ l17\ g17.l17
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/l10\ l15\ l18\ g18.l18
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/e10.e10
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/c12.c12
dd bs=$((0x0200)) count=1 if=/dev/zero of=warpsped/e8.e8

#ROM_REGION(0x1000, "sprite", 0)
#ROM_LOAD( "l9 l13 l17 g17.g17",  0x0000, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.g18", 0x0200, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l9",   0x0400, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l10", 0x0600, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l13",  0x0800, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l15", 0x0a00, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l17",  0x0c00, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l18", 0x0e00, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_REGION(0x200, "starfield", 0)
#ROM_LOAD( "e10.e10", 0x0000, 0x0200, CRC(e0d4b72c) SHA1(ae5fae0df9e0bfc67f586649474ff8a69abd7579) )
#ROM_REGION(0x400, "unknown", 0)
#ROM_LOAD( "c12.c12", 0x0000, 0x0200, CRC(88a8db15) SHA1(3fdf4e23cf75cf5dd4d3bad08e9e71c0268f8d79) )
#ROM_LOAD( "e8.e8",   0x0200, 0x0200, CRC(3ef3a576) SHA1(905f9b8d3cabab944a0f6f0736c5c26d0e36107f) )







#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy32k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=warpsped/pp_e02.rom
#rm spritemap01.bin

#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=warpsped/warpsped.3j
#rm palette01.bin
##dd bs=$((0x0020)) count=1 if=/dev/urandom of=warpsped/warpsped.3j


#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=warpsped/warpsped.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=warpsped/warpsped.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=warpsped/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=warpsped/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r warpsped warpsped
rm warpsped/*
rmdir warpsped

mame -w -video soft -resolution0 512x512 -rp ./ warpsped








#ROM_START( warpsped )
#ROM_REGION(0x1000, "maincpu", 0)
#ROM_LOAD( "m16 pro 0.c5",  0x0000, 0x0200, CRC(81f33dfb) SHA1(5c4adef88e1e7f1a9f7c156f17b98ba522ddee81) )
#ROM_LOAD( "m16 pro 1.e5",  0x0200, 0x0200, CRC(135f7421) SHA1(0cabe9a2590fe4f81976a4d10e9b2a223a2d875e) )
#ROM_LOAD( "m16 pro 2.c6",  0x0400, 0x0200, CRC(0a36d152) SHA1(83a2e8f78a36e512da81c44a5ceca7f865bc3508) )
#ROM_LOAD( "m16 pro 3.e6",  0x0600, 0x0200, CRC(ba416cca) SHA1(afb831a79a4a4334fa4caf4e2244c2d4e2f25853) )
#ROM_LOAD( "m16 pro 4.c4",  0x0800, 0x0200, CRC(fc44f25b) SHA1(185f593f2ec6075fd68869d87ed584908031100a) )
#ROM_LOAD( "m16 pro 5.e4",  0x0a00, 0x0200, CRC(7a16bc2b) SHA1(48f58f0c7469da24a3ebee9183b5aae8c676e6ea) )
#ROM_LOAD( "m16 pro 6.c3",  0x0c00, 0x0200, CRC(e2e7940f) SHA1(78c9df32580784c278675d09b89095781893d48f) )


#ROM_REGION(0x0200, "gfx1", 0)
#ROM_LOAD( "k1.g1",  0x0000, 0x0200, CRC(63d4fa84) SHA1(3465ce27497e2d4fcae994c022480e37e1345686) )
#ROM_REGION(0x0200, "gfx2", 0)
#ROM_LOAD( "k1.k1",  0x0000, 0x0200, CRC(76b10d47) SHA1(e644a50df06535fe1fbfb8754cfc7b4a49fcb05e) )

#ROM_REGION(0x1000, "sprite", 0)
#ROM_LOAD( "l9 l13 l17 g17.g17",  0x0000, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.g18", 0x0200, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l9",   0x0400, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l10", 0x0600, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l13",  0x0800, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l15", 0x0a00, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_LOAD( "l9 l13 l17 g17.l17",  0x0c00, 0x0200, CRC(7449aae9) SHA1(1f49dad6f60103da6093592efa2087bc24dc0283) )
#ROM_LOAD( "l10 l15 l18 g18.l18", 0x0e00, 0x0200, CRC(5829699c) SHA1(20ffa7b81a0de159408d2668005b9ee8a2e588d1) )
#ROM_REGION(0x200, "starfield", 0)
#ROM_LOAD( "e10.e10", 0x0000, 0x0200, CRC(e0d4b72c) SHA1(ae5fae0df9e0bfc67f586649474ff8a69abd7579) )
#ROM_REGION(0x400, "unknown", 0)
#ROM_LOAD( "c12.c12", 0x0000, 0x0200, CRC(88a8db15) SHA1(3fdf4e23cf75cf5dd4d3bad08e9e71c0268f8d79) )
#ROM_LOAD( "e8.e8",   0x0200, 0x0200, CRC(3ef3a576) SHA1(905f9b8d3cabab944a0f6f0736c5c26d0e36107f) )



#ROM_END
#


