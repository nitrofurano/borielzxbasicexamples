rm example01.asm
rm higemaru.zip
rm higemaru/*
rmdir higemaru
mkdir higemaru
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=higemaru/hg4.p12
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=higemaru/hg5.m12
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=higemaru/hg6.p11
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=higemaru/hg7.m11
rm example01.bin

#gfx
zxb.py library/charmaphigemaru01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmaphigemaru01.bin of=charmaphigemaru01b.bin
rm charmaphigemaru01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=higemaru/hg3.m1
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=higemaru/hg1.c14
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmaphigemaru01b.bin of=higemaru/hg2.e14
rm charmaphigemaru01b.bin

#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=higemaru/hgb3.l6
rm palette01b.bin

#lookup
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
rm clut01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=higemaru/hgb5.m4  #?ch
dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=higemaru/hgb1.h7  #?sp
rm clut01b.bin

#proms
dd bs=$((0x100)) count=1 if=/dev/urandom of=higemaru/hgb4.l9
dd bs=$((0x100)) count=1 if=/dev/urandom of=higemaru/hgb2.k7

#-
zip -r higemaru higemaru
rm higemaru/*
rmdir higemaru
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ higemaru

