ldrun
(0x0000,0x7fff) AM_ROM
(0xc000,0xc0ff) AM_WRITEONLY AM_SHARE("spriteram")
(0xd000,0xdfff) AM_RAM_WRITE(m62_tileram_w) AM_SHARE("m62_tileram")
(0xe000,0xefff) AM_RAM

(0x00, 0x00) AM_READ_PORT("SYSTEM") AM_WRITE_LEGACY(irem_sound_cmd_w)
(0x01, 0x01) AM_READ_PORT("P1") AM_WRITE(m62_flipscreen_w)  /* + coin counters */
(0x02, 0x02) AM_READ_PORT("P2")
(0x03, 0x03) AM_READ_PORT("DSW1")
(0x04, 0x04) AM_READ_PORT("DSW2")
(0x80, 0x80) AM_WRITE(m62_vscroll_low_w)
(0x81, 0x81) AM_WRITE(m62_hscroll_high_w)
(0x82, 0x82) AM_WRITE(m62_hscroll_low_w)
(0x83, 0x83) AM_WRITE(battroad_bankswitch_w)



ROM_START( ldrun )
_REGION( 0x10000,"maincpu",0 )
("lr-a-4e",0x0000,0x2000,CRC(5d7e2a4d) SHA1(fe8aeff360f6c3a8606d67a8b95148c3c2ef7267) )
("lr-a-4d",0x2000,0x2000,CRC(96f20473) SHA1(e400c43f3f32e12f68ca204c60bcebdb2b3da55d) )
("lr-a-4b",0x4000,0x2000,CRC(b041c4a9) SHA1(77768b03ea2497e25c3e47b68a0eb2fe3e9aea35) )
("lr-a-4a",0x6000,0x2000,CRC(645e42aa) SHA1(c806ffce7ece418bad86854c987f78c70c13e492) )

_REGION( 0x10000,"iremsound",0 )   /* 64k for the audio CPU (6803) */
("lr-a-3f",0xc000,0x2000,CRC(7a96accd) SHA1(e94815dbfaabbb562df8f3298060aa6bd7825904) )
("lr-a-3h",0xe000,0x2000,CRC(3f7f3939) SHA1(7ee25a21e74995bfb36ac11b45d384b33a6d8515) )

_REGION( 0x6000,"gfx1",0 )
("lr-e-2d",0x0000,0x2000,CRC(24f9b58d) SHA1(e33224b910d37aaa85713b954c8dd50996245a8c) )    /* characters */
("lr-e-2j",0x2000,0x2000,CRC(43175e08) SHA1(9dbafb27d46cf7df35f343a8753e8d91ea706993) )
("lr-e-2f",0x4000,0x2000,CRC(e0317124) SHA1(b766bd21e2da1673d2054148f62d61c33c95d38e) )

_REGION( 0x6000,"gfx2",0 )
("lr-b-4k",0x0000,0x2000,CRC(8141403e) SHA1(65fa6bc872fb07c71aacbbcc35cee766b2877896) )    /* sprites */
("lr-b-3n",0x2000,0x2000,CRC(55154154) SHA1(35304676e1ab55adccdabdc766a4e0e0901d3cd0) )
("lr-b-4c",0x4000,0x2000,CRC(924e34d0) SHA1(6a841419797a129235fc7d0405a5be55e8d703da) )

_REGION( 0x0720,"proms",0 )
("lr-e-3m",0x0000,0x0100,CRC(53040416) SHA1(2c6915164d1c31afc60a21b557abdf023d5b3f46) )    /* character palette red component */
("lr-b-1m",0x0100,0x0100,CRC(4bae1c25) SHA1(17a9e2567d9d648dca69510bb201f8af0738b068) )    /* sprite palette red component */
("lr-e-3l",0x0200,0x0100,CRC(67786037) SHA1(cd40dfd94295afe57139733752643cf48b8566b1) )    /* character palette green component */
("lr-b-1n",0x0300,0x0100,CRC(9cd3db94) SHA1(bff95965f946df0e4af1f99db5b2468bf1d4403f) )    /* sprite palette green component */
("lr-e-3n",0x0400,0x0100,CRC(5b716837) SHA1(e3ea250891fec43a97e92ac1c3a4fbb5ee2d4a4d) )    /* character palette blue component */
("lr-b-1l",0x0500,0x0100,CRC(08d8cf9a) SHA1(a46213e0dc04e44b0544401eb341fd49eef331dd) )    /* sprite palette blue component */
("lr-b-5p",0x0600,0x0020,CRC(e01f69e2) SHA1(0d00ef348025ea4a9c274a7e3dbb006217d8449d) )    /* sprite height,one entry per 32 */
															/* sprites. Used at run time! */
("lr-b-6f",0x0620,0x0100,CRC(34d88d3c) SHA1(727f4c5cfff33538886fa0a29fd119aa085d7008) )    /* video timing - common to the other games */


