#!/bin/bash
#dd ibs=2 obs=1 skip=2 count=4 if=az.txt of=output.txt


xxd -p -c 3 -s 0 -l 15 az.txt | cut -b 1-2 | xxd -r -p > output_r.txt
xxd -p -c 3 -s 1 -l 15 az.txt | cut -b 1-2 | xxd -r -p > output_g.txt
xxd -p -c 3 -s 2 -l 15 az.txt | cut -b 1-2 | xxd -r -p > output_b.txt

xxd -p -c 3 -s 0 -l 15 bytes.bin | cut -b 1-2 | xxd -r -p > output_b_r.txt
xxd -p -c 3 -s 1 -l 15 bytes.bin | cut -b 1-2 | xxd -r -p > output_b_g.txt
xxd -p -c 3 -s 2 -l 15 bytes.bin | cut -b 1-2 | xxd -r -p > output_b_b.txt


# abcdefghijklmnopqrstuvwxyz -> egikm
# xxd -p -c 2 -s 4 -l 10 az.txt | cut -b 1-2 | xxd -r -p > output.txt
