rm example01.asm
rm mpatrol.zip
rm mpatrol/*
rmdir mpatrol
mkdir mpatrol

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=mpatrol/mpa-1.3m
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=mpatrol/mpa-2.3l
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=mpatrol/mpa-3.3k
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=mpatrol/mpa-4.3j
rm example01.bin

dd bs=4096 count=1 if=/dev/zero of=mpatrol/mp-s1.1a

zxb.py library/spritemap01.zxi --org=$((0x0000))
cat dummy32k.bin >> spritemap01b.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=spritemap01.bin of=spritemap01b.bin

xxd -p -c 2 -s 0 -l 8192 spritemap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpb-1.3n
xxd -p -c 2 -s 1 -l 8192 spritemap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpb-2.3m

rm spritemap01.bin spritemap01b.bin

zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy32k.bin >> charmap01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin

xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpe-4.3f
xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpe-5.3e

xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpe-1.3l
xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpe-2.3k
xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mpatrol/mpe-3.3h

rm charmap01.bin charmap01b.bin

zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=16384 skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0000)) if=palette01b.bin of=mpatrol/mpc-4.2a #ok
dd ibs=1 count=$((0x0020)) skip=$((0x0200)) if=palette01b.bin of=mpatrol/mpc-1.1f #(sprite colors?)
dd ibs=1 count=$((0x0100)) skip=$((0x0220)) if=palette01b.bin of=mpatrol/mpc-2.2h #(sprite color sets?)
dd ibs=1 count=$((0x0020)) skip=$((0x0320)) if=palette01b.bin of=mpatrol/mpc-3.1m #?
rm palette01.bin palette01b.bin

zip -r mpatrol mpatrol
rm mpatrol/*
rmdir mpatrol
rm smsboot.bin dummy32k.bin

mame -w -nosound -video soft -resolution0 512x512 -rp ./ mpatrol

