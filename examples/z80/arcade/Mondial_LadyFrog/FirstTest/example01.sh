rm example01.asm
rm ladyfrog.zip
rm ladyfrog/*
rmdir ladyfrog
mkdir ladyfrog

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=example01.bin of=ladyfrog/2.107
rm example01.bin

#audio
dd bs=$((0x8000)) count=1 if=/dev/zero of=ladyfrog/1.115

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x00010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1

xxd -p -c 4 -s 0 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/6.8
xxd -p -c 4 -s 2 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/3.32
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/6.8 #1st row, bdepth 0,1
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/3.32 #1st row, bdepth 2,3

xxd -p -c 4 -s 0 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/7.9
xxd -p -c 4 -s 2 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/4.33
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/7.9 #2nd row, bdepth 0,1
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/4.33 #2nd row, bdepth 2,3

xxd -p -c 4 -s 0 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/8.10
xxd -p -c 4 -s 2 -l $((0x20000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > ladyfrog/5.34
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/8.10 #3rd row, bdepth 0,1
#dd ibs=1 count=$((0x10000)) skip=$((0x00000)) if=charmap01b.bin of=ladyfrog/5.34 #3rd row, bdepth 2,3

#xxd -p -c 4 -s 0 -l 32 test1.txt | cut -b 1-4 | xxd -r -p > test1_a.txt
#xxd -p -c 4 -s 2 -l 32 test1.txt | cut -b 1-4 | xxd -r -p > test1_b.txt

#dd bs=$((0x10000)) count=1 if=/dev/zero of=ladyfrog/8.10

rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=ladyfrog/ladyfrog.5f
#rm spritemap01.bin

#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=ladyfrog/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=ladyfrog/82s126.4a
#rm clut01.bin
#prom
#dd bs=256 count=1 if=/dev/zero of=ladyfrog/82s126.3m

rm smsboot.bin dummy64k.bin

zip -r ladyfrog ladyfrog
rm ladyfrog/*
rmdir ladyfrog

mame -w -video soft -resolution0 512x512 -rp ./ ladyfrog

# ROM_REGION( 0x60000, "gfx1", ROMREGION_INVERT )
# ROM_LOAD( "3.32",   0x30000, 0x10000, CRC(8a27fc0a) SHA1(36e0365776e61ef830451e6351eca6b6c742086f) )
# ROM_LOAD( "4.33",   0x40000, 0x10000, CRC(e1a137d3) SHA1(add8140a9366a0d343b611ced10c804d3fb04c03) )
# ROM_LOAD( "5.34",   0x50000, 0x10000, CRC(7816925f) SHA1(037a69243b35e1739e5d7288e279d0d4289c61ed) )
# ROM_LOAD( "6.8",    0x00000, 0x10000, CRC(61b3baaa) SHA1(d65a235dbbb96c11e8307aa457d1c06f20eb8d5a) )
# ROM_LOAD( "7.9",    0x10000, 0x10000, CRC(88aaff58) SHA1(dfb143ef452dec530adf8b35a50a82d08f47d107) )
# ROM_LOAD( "8.10",   0x20000, 0x10000, CRC(8c73baa1) SHA1(50fb408be181ef3c125dee23b04daeb010c9f276) )
#--------------------------
# ROM_REGION( 0x10000, "maincpu", 0 )
# ROM_LOAD( "2.107",   0x0000, 0x10000, CRC(fa4466e6) SHA1(08e5cc8e1d3c845bc9c253267f2683671bffa9f2) )

# ROM_REGION( 0x10000, "audiocpu", 0 )
# ROM_LOAD( "1.115",   0x0000, 0x8000, CRC(b0932498) SHA1(13d90698f2682e64ff3597c9267ca9d33a6d62ba) ) /* NY Captor*/


