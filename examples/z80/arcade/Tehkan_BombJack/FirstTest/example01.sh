rm bombjack.zip
rm example01.asm
rm bombjack/*
rmdir bombjack
mkdir bombjack


dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=bombjack/09_j01b.bin
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=bombjack/10_l01b.bin
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=bombjack/11_m01b.bin
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=bombjack/12_n01b.bin
dd ibs=1 count=$((0x2000)) skip=$((0xC000)) if=example01.bin of=bombjack/13.1r

#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "09_j01b.bin",  0x0000, 0x2000, CRC(c668dc30) SHA1(51dd6a2688b42e9f28f0882bd76f75be7ec3222a) )
#	ROM_LOAD( "10_l01b.bin",  0x2000, 0x2000, CRC(52a1e5fb) SHA1(e1cdc4b4efbc6c7a1e4fa65019486617f2acba1b) )
#	ROM_LOAD( "11_m01b.bin",  0x4000, 0x2000, CRC(b68a062a) SHA1(43bae56494ac0202aaa8f1ed5c1ed1bff775b2b8) )
#	ROM_LOAD( "12_n01b.bin",  0x6000, 0x2000, CRC(1d3ecee5) SHA1(8b3c49e21ea4952cae7042890d1be2115f7d6fda) )
#	ROM_LOAD( "13.1r",        0xc000, 0x2000, CRC(70e0244d) SHA1(67654155e42821ea78a655f869fb81c8d6387f63) )






#audio
dd bs=$((0x2000)) count=1 if=/dev/zero of=bombjack/01_h03t.bin

#	ROM_REGION( 0x10000, "audiocpu", 0 )    /* 64k for sound board */
#	ROM_LOAD( "01_h03t.bin",  0x0000, 0x2000, CRC(8407917d) SHA1(318face9f7a7ab6c7eeac773995040425e780aaf) )







zxb.py library/b3r1f0_charmapnunchacken01.zxi --org=$((0x0000))
cat dummy64k.bin >> b3r1f0_charmapnunchacken01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=b3r1f0_charmapnunchacken01b.bin

xxd -p -c 3 -s 0 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/03_e08t.bin
xxd -p -c 3 -s 1 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/04_h08t.bin
xxd -p -c 3 -s 2 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/05_k08t.bin

xxd -p -c 3 -s 0 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/06_l08t.bin
xxd -p -c 3 -s 1 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/07_n08t.bin
xxd -p -c 3 -s 2 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/08_r08t.bin

xxd -p -c 3 -s 0 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/16_m07b.bin
xxd -p -c 3 -s 1 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/15_l07b.bin
xxd -p -c 3 -s 2 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > bombjack/14_j07b.bin

rm b3r1f0_charmapnunchacken01.bin b3r1f0_charmapnunchacken01b.bin




zxb.py library/backgroundtilemaps.zxi --org=$((0x0000))
cat dummy64k.bin >> backgroundtilemaps.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=backgroundtilemaps.bin of=bombjack/02_p04t.bin
rm b1r1f0_charmapkamikaze01.bin




#zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000))
#cat dummy64k.bin >> b1r1f0_charmapkamikaze01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/03_e08t.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/04_h08t.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/05_k08t.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/06_l08t.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/07_n08t.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/08_r08t.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/16_m07b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/15_l07b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/14_j07b.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=bombjack/02_p04t.bin
#rm b1r1f0_charmapkamikaze01.bin







#	ROM_REGION( 0x3000, "chars", 0 )
#	ROM_LOAD( "03_e08t.bin",  0x0000, 0x1000, CRC(9f0470d5) SHA1(94ef52ef47b4399a03528fe3efeac9c1d6983446) )    /* chars */
#	ROM_LOAD( "04_h08t.bin",  0x1000, 0x1000, CRC(81ec12e6) SHA1(e29ba193f21aa898499187603b25d2e226a07c7b) )
#	ROM_LOAD( "05_k08t.bin",  0x2000, 0x1000, CRC(e87ec8b1) SHA1(a66808ef2d62fca2854396898b86bac9be5f17a3) )
#	ROM_REGION( 0x6000, "tiles", 0 )
#	ROM_LOAD( "06_l08t.bin",  0x0000, 0x2000, CRC(51eebd89) SHA1(515128a3971fcb97b60c5b6bdd2b03026aec1921) )    /* background tiles */
#	ROM_LOAD( "07_n08t.bin",  0x2000, 0x2000, CRC(9dd98e9d) SHA1(6db6006a6e20ff7c243d88293ca53681c4703ea5) )
#	ROM_LOAD( "08_r08t.bin",  0x4000, 0x2000, CRC(3155ee7d) SHA1(e7897dca4c145f10b7d975b8ef0e4d8aa9354c25) )
#	ROM_REGION( 0x6000, "sprites", 0 )
#	ROM_LOAD( "16_m07b.bin",  0x0000, 0x2000, CRC(94694097) SHA1(de71bcd67f97d05527f2504fc8430be333fb9ec2) )    /* sprites */
#	ROM_LOAD( "15_l07b.bin",  0x2000, 0x2000, CRC(013f58f2) SHA1(20c64593ab9fcb04cefbce0cd5d17ce3ff26441b) )
#	ROM_LOAD( "14_j07b.bin",  0x4000, 0x2000, CRC(101c858d) SHA1(ed1746c15cdb04fae888601d940183d5c7702282) )
#	ROM_REGION( 0x1000, "gfx4", 0 ) /* background tilemaps */
#	ROM_LOAD( "02_p04t.bin",  0x0000, 0x1000, CRC(398d4a02) SHA1(ac18a8219f99ba9178b96c9564de3978e39c59fd) )
#ROM_END





#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=bombjack/pp_e02.rom
#rm spritemap01.bin



#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=bombjack/bombjack.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=bombjack/bombjack.3j


#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=bombjack/bombjack.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=bombjack/bombjack.11j
#rm clut01b.bin

#proms
dd bs=$((0x020)) count=1 if=/dev/urandom of=bombjack/ft.9c
dd bs=$((0x020)) count=1 if=/dev/urandom of=bombjack/ft.9b







rm smsboot.bin dummy64k.bin example01.bin backgroundtilemaps.bin

zip -r bombjack bombjack
rm bombjack/*
rmdir bombjack

mame -w -video soft -sound none -resolution0 512x512 -rp ./ bombjack








#ROM_START( bombjack )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "09_j01b.bin",  0x0000, 0x2000, CRC(c668dc30) SHA1(51dd6a2688b42e9f28f0882bd76f75be7ec3222a) )
#	ROM_LOAD( "10_l01b.bin",  0x2000, 0x2000, CRC(52a1e5fb) SHA1(e1cdc4b4efbc6c7a1e4fa65019486617f2acba1b) )
#	ROM_LOAD( "11_m01b.bin",  0x4000, 0x2000, CRC(b68a062a) SHA1(43bae56494ac0202aaa8f1ed5c1ed1bff775b2b8) )
#	ROM_LOAD( "12_n01b.bin",  0x6000, 0x2000, CRC(1d3ecee5) SHA1(8b3c49e21ea4952cae7042890d1be2115f7d6fda) )
#	ROM_LOAD( "13.1r",        0xc000, 0x2000, CRC(70e0244d) SHA1(67654155e42821ea78a655f869fb81c8d6387f63) )


#	ROM_REGION( 0x10000, "audiocpu", 0 )    /* 64k for sound board */
#	ROM_LOAD( "01_h03t.bin",  0x0000, 0x2000, CRC(8407917d) SHA1(318face9f7a7ab6c7eeac773995040425e780aaf) )

#gfx
#	ROM_REGION( 0x3000, "chars", 0 )
#	ROM_LOAD( "03_e08t.bin",  0x0000, 0x1000, CRC(9f0470d5) SHA1(94ef52ef47b4399a03528fe3efeac9c1d6983446) )    /* chars */
#	ROM_LOAD( "04_h08t.bin",  0x1000, 0x1000, CRC(81ec12e6) SHA1(e29ba193f21aa898499187603b25d2e226a07c7b) )
#	ROM_LOAD( "05_k08t.bin",  0x2000, 0x1000, CRC(e87ec8b1) SHA1(a66808ef2d62fca2854396898b86bac9be5f17a3) )
#	ROM_REGION( 0x6000, "tiles", 0 )
#	ROM_LOAD( "06_l08t.bin",  0x0000, 0x2000, CRC(51eebd89) SHA1(515128a3971fcb97b60c5b6bdd2b03026aec1921) )    /* background tiles */
#	ROM_LOAD( "07_n08t.bin",  0x2000, 0x2000, CRC(9dd98e9d) SHA1(6db6006a6e20ff7c243d88293ca53681c4703ea5) )
#	ROM_LOAD( "08_r08t.bin",  0x4000, 0x2000, CRC(3155ee7d) SHA1(e7897dca4c145f10b7d975b8ef0e4d8aa9354c25) )
#	ROM_REGION( 0x6000, "sprites", 0 )
#	ROM_LOAD( "16_m07b.bin",  0x0000, 0x2000, CRC(94694097) SHA1(de71bcd67f97d05527f2504fc8430be333fb9ec2) )    /* sprites */
#	ROM_LOAD( "15_l07b.bin",  0x2000, 0x2000, CRC(013f58f2) SHA1(20c64593ab9fcb04cefbce0cd5d17ce3ff26441b) )
#	ROM_LOAD( "14_j07b.bin",  0x4000, 0x2000, CRC(101c858d) SHA1(ed1746c15cdb04fae888601d940183d5c7702282) )
#	ROM_REGION( 0x1000, "gfx4", 0 ) /* background tilemaps */
#	ROM_LOAD( "02_p04t.bin",  0x0000, 0x1000, CRC(398d4a02) SHA1(ac18a8219f99ba9178b96c9564de3978e39c59fd) )
#ROM_END

