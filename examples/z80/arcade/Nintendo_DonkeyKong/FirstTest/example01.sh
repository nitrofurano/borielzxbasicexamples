# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm dkong.zip
rm dkong/*
rmdir dkong
mkdir dkong



dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=dkong/c_5et_g.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=dkong/c_5ct_g.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=dkong/c_5bt_g.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=dkong/c_5at_g.bin
dd bs=$((0x2000)) count=1 if=/dev/zero of=dkong/ao4_05.ic3
rm example01.bin

#ROM_START( dkong ) /* Confirmed TKG-04 Upgrade as mentioned in Nintendo Service Department Bulletin # TKG-02 12-11-81 */
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "c_5et_g.bin",  0x0000, 0x1000, CRC(ba70b88b) SHA1(d76ebecfea1af098d843ee7e578e480cd658ac1a) )
#ROM_LOAD( "c_5ct_g.bin",  0x1000, 0x1000, CRC(5ec461ec) SHA1(acb11a8fbdbb3ab46068385fe465f681e3c824bd) )
#ROM_LOAD( "c_5bt_g.bin",  0x2000, 0x1000, CRC(1c97d324) SHA1(c7966261f3a1d3296927e0b6ee1c58039fc53c1f) )
#ROM_LOAD( "c_5at_g.bin",  0x3000, 0x1000, CRC(b9005ac0) SHA1(3fe3599f6fa7c496f782053ddf7bacb453d197c4) )
#/* space for diagnostic ROM */



#mcu
dd bs=$((0x0800)) count=1 if=/dev/zero of=dkong/s_3i_b.bin
dd bs=$((0x0800)) count=1 if=/dev/zero of=dkong/s_3j_b.bin

#ROM_REGION( 0x1800, "soundcpu", 0 ) /* sound */
#ROM_LOAD( "s_3i_b.bin",   0x0000, 0x0800, CRC(45a4ed06) SHA1(144d24464c1f9f01894eb12f846952290e6e32ef) )
#ROM_RELOAD(               0x0800, 0x0800 )
#ROM_LOAD( "s_3j_b.bin",   0x1000, 0x0800, CRC(4743fe92) SHA1(6c82b57637c0212a580591397e6a5a1718f19fd2) )



#gfx

zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=b1r1f0_charmapkamikaze01b.bin

zxb.py library/b2r1f0_charmapscramble02.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b2r1f0_charmapscramble02.bin of=b2r1f0_charmapscramble02b.bin
xxd -p -c 2 -s 0 -l 65536 b2r1f0_charmapscramble02b.bin | cut -b 1-2 | xxd -r -p > dkong/v_5h_b.bin #ch_bit1
xxd -p -c 2 -s 1 -l 65536 b2r1f0_charmapscramble02b.bin | cut -b 1-2 | xxd -r -p > dkong/v_3pt.bin #ch_bit2

#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/v_5h_b.bin #ch_bit1
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/v_3pt.bin #ch_bit2
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/l_4m_b.bin #spr_high_bit1
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/l_4n_b.bin #spr_low_bit1
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/l_4r_b.bin #spr_high_bit2
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=dkong/l_4s_b.bin #spr_low_bit2

#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/v_5h_b.bin
#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/v_3pt.bin
#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/l_4m_b.bin
#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/l_4n_b.bin
#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/l_4r_b.bin
#dd bs=$((0x10000)) count=1 if=/dev/zero of=dkong/l_4s_b.bin




#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 b1r1f0_charmapkamikaze01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f0_charmapkamikaze01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 b1r1f0_charmapkamikaze01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f0_charmapkamikaze01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4_07.ic15 #-bitdepth1

rm b1r1f0_charmapkamikaze01.bin
rm b1r1f0_charmapkamikaze01b.bin
rm b2r1f0_charmapscramble01.bin
rm b2r1f0_charmapscramble01b.bin
rm b2r1f0_charmapscramble02.bin
rm b2r1f0_charmapscramble02b.bin

#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=dkong/dkong.5f
#rm spritemap01.bin

#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD( "v_5h_b.bin",   0x0000, 0x0800, CRC(12c8c95d) SHA1(a57ff5a231c45252a63b354137c920a1379b70a3) )
#ROM_LOAD( "v_3pt.bin",    0x0800, 0x0800, CRC(15e9c5e9) SHA1(976eb1e18c74018193a35aa86cff482ebfc5cc4e) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "l_4m_b.bin",   0x0000, 0x0800, CRC(59f8054d) SHA1(793dba9bf5a5fe76328acdfb90815c243d2a65f1) )
#ROM_LOAD( "l_4n_b.bin",   0x0800, 0x0800, CRC(672e4714) SHA1(92e5d379f4838ac1fa44d448ce7d142dae42102f) )
#ROM_LOAD( "l_4r_b.bin",   0x1000, 0x0800, CRC(feaa59ee) SHA1(ecf95db5a20098804fc8bd59232c66e2e0ed3db4) )
#ROM_LOAD( "l_4s_b.bin",   0x1800, 0x0800, CRC(20f2ef7e) SHA1(3bc482a38bf579033f50082748ee95205b0f673d) )



dd bs=$((0x0100)) count=1 if=/dev/urandom of=dkong/c-2k.bpr
dd bs=$((0x0100)) count=1 if=/dev/urandom of=dkong/c-2j.bpr
dd bs=$((0x0100)) count=1 if=/dev/urandom of=dkong/v-5e.bpr

#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > dkong/ao4-12.ic95
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=dkong/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=dkong/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=dkong/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=dkong/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=dkong/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=dkong/82s126.4a
#rm clut01.bin

#ROM_REGION( 0x0300, "proms", 0 )
#ROM_LOAD( "c-2k.bpr",     0x0000, 0x0100, CRC(e273ede5) SHA1(b50ec9e1837c00c20fb2a4369ec7dd0358321127) ) /* palette low 4 bits (inverted) */
#ROM_LOAD( "c-2j.bpr",     0x0100, 0x0100, CRC(d6412358) SHA1(f9c872da2fe8e800574ae3bf483fb3ccacc92eb3) ) /* palette high 4 bits (inverted) */
#ROM_LOAD( "v-5e.bpr",     0x0200, 0x0100, CRC(b869b8f5) SHA1(c2bdccbf2654b64ea55cd589fd21323a9178a660) ) /* character color codes on a per-column basis */
#ROM_END



rm smsboot.bin

zip -r dkong dkong
rm dkong/*
rmdir dkong
rm dummy64k.bin example01.bin

mame -w -video soft -sound none -resolution0 512x512 -rp ./ dkong



#ROM_START( dkong ) /* Confirmed TKG-04 Upgrade as mentioned in Nintendo Service Department Bulletin # TKG-02 12-11-81 */
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "c_5et_g.bin",  0x0000, 0x1000, CRC(ba70b88b) SHA1(d76ebecfea1af098d843ee7e578e480cd658ac1a) )
#ROM_LOAD( "c_5ct_g.bin",  0x1000, 0x1000, CRC(5ec461ec) SHA1(acb11a8fbdbb3ab46068385fe465f681e3c824bd) )
#ROM_LOAD( "c_5bt_g.bin",  0x2000, 0x1000, CRC(1c97d324) SHA1(c7966261f3a1d3296927e0b6ee1c58039fc53c1f) )
#ROM_LOAD( "c_5at_g.bin",  0x3000, 0x1000, CRC(b9005ac0) SHA1(3fe3599f6fa7c496f782053ddf7bacb453d197c4) )
#/* space for diagnostic ROM */

#ROM_REGION( 0x1800, "soundcpu", 0 ) /* sound */
#ROM_LOAD( "s_3i_b.bin",   0x0000, 0x0800, CRC(45a4ed06) SHA1(144d24464c1f9f01894eb12f846952290e6e32ef) )
#ROM_RELOAD(               0x0800, 0x0800 )
#ROM_LOAD( "s_3j_b.bin",   0x1000, 0x0800, CRC(4743fe92) SHA1(6c82b57637c0212a580591397e6a5a1718f19fd2) )

#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD( "v_5h_b.bin",   0x0000, 0x0800, CRC(12c8c95d) SHA1(a57ff5a231c45252a63b354137c920a1379b70a3) )
#ROM_LOAD( "v_3pt.bin",    0x0800, 0x0800, CRC(15e9c5e9) SHA1(976eb1e18c74018193a35aa86cff482ebfc5cc4e) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "l_4m_b.bin",   0x0000, 0x0800, CRC(59f8054d) SHA1(793dba9bf5a5fe76328acdfb90815c243d2a65f1) )
#ROM_LOAD( "l_4n_b.bin",   0x0800, 0x0800, CRC(672e4714) SHA1(92e5d379f4838ac1fa44d448ce7d142dae42102f) )
#ROM_LOAD( "l_4r_b.bin",   0x1000, 0x0800, CRC(feaa59ee) SHA1(ecf95db5a20098804fc8bd59232c66e2e0ed3db4) )
#ROM_LOAD( "l_4s_b.bin",   0x1800, 0x0800, CRC(20f2ef7e) SHA1(3bc482a38bf579033f50082748ee95205b0f673d) )

#ROM_REGION( 0x0300, "proms", 0 )
#ROM_LOAD( "c-2k.bpr",     0x0000, 0x0100, CRC(e273ede5) SHA1(b50ec9e1837c00c20fb2a4369ec7dd0358321127) ) /* palette low 4 bits (inverted) */
#ROM_LOAD( "c-2j.bpr",     0x0100, 0x0100, CRC(d6412358) SHA1(f9c872da2fe8e800574ae3bf483fb3ccacc92eb3) ) /* palette high 4 bits (inverted) */
#ROM_LOAD( "v-5e.bpr",     0x0200, 0x0100, CRC(b869b8f5) SHA1(c2bdccbf2654b64ea55cd589fd21323a9178a660) ) /* character color codes on a per-column basis */
#ROM_END




