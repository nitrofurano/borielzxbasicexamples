rm pturn.zip example01.asm
rm pturn/*
rmdir pturn
mkdir pturn



dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#cpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=pturn/prom4.8d
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=pturn/prom6.8b
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=pturn/prom5.7d
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=pturn/prom7.7b
rm example01.bin smsboot.bin

#ROM_START( pturn )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "prom4.8d", 0x00000,0x2000, CRC(d3ae0840) SHA1(5ac5f2626de7865cdf379cf15ae3872e798b7e25))
#	ROM_LOAD( "prom6.8b", 0x02000,0x2000, CRC(65f09c56) SHA1(c0a7a1bfaacfc4af14d8485e2b5f2c604937a1e4))
#	ROM_LOAD( "prom5.7d", 0x04000,0x2000, CRC(de48afb4) SHA1(9412288b63cf3ae8c9522b1fcacc4aa36ac7a23c))
#	ROM_LOAD( "prom7.7b", 0x06000,0x2000, CRC(bfaeff9f) SHA1(63972c311f28971e121fbccd4c0d78edbdb6bdb4))










#audio
dd bs=$((0x1000)) count=1 if=/dev/zero of=pturn/prom9.5n

#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "prom9.5n", 0x00000,0x1000, CRC(8b4d944e) SHA1(6f956d972c2c2ef875378910b80ca59701710957))






#gfx

zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000))
cat dummy128k.bin >> b1r1f0_charmapkamikaze01.bin
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom1.8k
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom3.6k
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom2.7k
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom11.16f
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom12.16h
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom13.16k
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom14.16l
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom15.16m
dd ibs=1 count=$((0x001000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=pturn/prom16.16p

rm b1r1f0_charmapkamikaze01.bin b1r1f0_charmapkamikaze01b.bin




#dd ibs=1 count=768 skip=$((0x0010)) if=palettechr01.bin of=palettechr01b.bin
#xxd -p -c 3 -s 0 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-3m
#xxd -p -c 3 -s 1 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-3l
#xxd -p -c 3 -s 2 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-3n


#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom1.8k
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom2.7k
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom3.6k
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom11.16f
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom12.16h
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom13.16k
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom14.16l
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom15.16m
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=pturn/prom16.16p

#	ROM_REGION( 0x3000, "gfx1", 0 )
#	ROM_LOAD( "prom1.8k", 0x00000,0x1000, CRC(10aba36d) SHA1(5f9ce00365b3be91f0942b282b3cfc0c791baf98))
#	ROM_LOAD( "prom2.7k", 0x01000,0x1000, CRC(b8a4d94e) SHA1(78f9db58ceb4a87ab2744529b0e7ad3eb826e627))
#	ROM_LOAD( "prom3.6k", 0x02000,0x1000, CRC(9f51185b) SHA1(84690556da013567133b7d8fcda25b9fb831e4b0))
#	ROM_REGION( 0x3000, "gfx2", 0 )
#	ROM_LOAD( "prom11.16f", 0x000000, 0x01000, CRC(129122c6) SHA1(feb6d9abddb4d888b49861a32a063d009ca994aa) )
#	ROM_LOAD( "prom12.16h", 0x001000, 0x01000, CRC(69b09323) SHA1(726749b625052984e1d8c71eb69511c35ca75f9c) )
#	ROM_LOAD( "prom13.16k", 0x002000, 0x01000, CRC(e9f67599) SHA1(b2eb144c8ce9ff57bd66ba57705d5e242115ef41) )
#	ROM_REGION( 0x6000, "gfx3", 0 )
#	ROM_LOAD( "prom14.16l", 0x000000, 0x02000, CRC(ffaa0b8a) SHA1(20b1acc2562e493539fe34d056e6254e4b2458be) )
#	ROM_LOAD( "prom15.16m", 0x002000, 0x02000, CRC(41445155) SHA1(36d81b411729447ca7ff712ac27d8a0f2015bcac) )
#	ROM_LOAD( "prom16.16p", 0x004000, 0x02000, CRC(94814c5d) SHA1(e4ab6c0ae94184d5270cadb887f56e3550b6d9f2) )







#proms
dd bs=$((0x100)) count=1 if=/dev/urandom of=pturn/prom_red.3p
dd bs=$((0x100)) count=1 if=/dev/urandom of=pturn/prom_grn.4p
dd bs=$((0x100)) count=1 if=/dev/urandom of=pturn/prom_blu.4r

#	ROM_REGION( 0x0300, "proms", 0 )
#	ROM_LOAD( "prom_red.3p", 0x0000, 0x0100, CRC(505fd8c2) SHA1(f2660fe512c76412a7b9f4be21fe549dd59fbda0) )
#	ROM_LOAD( "prom_grn.4p", 0x0100, 0x0100, CRC(6a00199d) SHA1(ff0ac7ae83d970778a756f7445afed3785fc1150) )
#	ROM_LOAD( "prom_blu.4r", 0x0200, 0x0100, CRC(7b4c5788) SHA1(ca02b12c19be7981daa070533455bd4d227d56cd) )



#user
dd bs=$((0x100)) count=1 if=/dev/zero of=pturn/prom10.16d

#	ROM_REGION( 0x2000, "user1", 0 )
#	ROM_LOAD( "prom10.16d", 0x0000,0x2000, CRC(a96e3c95) SHA1(a3b1c1723fcda80c11d9858819659e5e9dfe5dd3))





rm dummy64k.bin

zip -r pturn pturn
rm pturn/*
rmdir pturn

mame -w -video soft -resolution0 512x512 -rp ./ pturn
#xmame.SDL -rp ./ pturn




#ROM_START( pturn )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "prom4.8d", 0x00000,0x2000, CRC(d3ae0840) SHA1(5ac5f2626de7865cdf379cf15ae3872e798b7e25))
#	ROM_LOAD( "prom6.8b", 0x02000,0x2000, CRC(65f09c56) SHA1(c0a7a1bfaacfc4af14d8485e2b5f2c604937a1e4))
#	ROM_LOAD( "prom5.7d", 0x04000,0x2000, CRC(de48afb4) SHA1(9412288b63cf3ae8c9522b1fcacc4aa36ac7a23c))
#	ROM_LOAD( "prom7.7b", 0x06000,0x2000, CRC(bfaeff9f) SHA1(63972c311f28971e121fbccd4c0d78edbdb6bdb4))

#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "prom9.5n", 0x00000,0x1000, CRC(8b4d944e) SHA1(6f956d972c2c2ef875378910b80ca59701710957))

#	ROM_REGION( 0x3000, "gfx1", 0 )
#	ROM_LOAD( "prom1.8k", 0x00000,0x1000, CRC(10aba36d) SHA1(5f9ce00365b3be91f0942b282b3cfc0c791baf98))
#	ROM_LOAD( "prom2.7k", 0x01000,0x1000, CRC(b8a4d94e) SHA1(78f9db58ceb4a87ab2744529b0e7ad3eb826e627))
#	ROM_LOAD( "prom3.6k", 0x02000,0x1000, CRC(9f51185b) SHA1(84690556da013567133b7d8fcda25b9fb831e4b0))
#	ROM_REGION( 0x3000, "gfx2", 0 )
#	ROM_LOAD( "prom11.16f", 0x000000, 0x01000, CRC(129122c6) SHA1(feb6d9abddb4d888b49861a32a063d009ca994aa) )
#	ROM_LOAD( "prom12.16h", 0x001000, 0x01000, CRC(69b09323) SHA1(726749b625052984e1d8c71eb69511c35ca75f9c) )
#	ROM_LOAD( "prom13.16k", 0x002000, 0x01000, CRC(e9f67599) SHA1(b2eb144c8ce9ff57bd66ba57705d5e242115ef41) )
#	ROM_REGION( 0x6000, "gfx3", 0 )
#	ROM_LOAD( "prom14.16l", 0x000000, 0x02000, CRC(ffaa0b8a) SHA1(20b1acc2562e493539fe34d056e6254e4b2458be) )
#	ROM_LOAD( "prom15.16m", 0x002000, 0x02000, CRC(41445155) SHA1(36d81b411729447ca7ff712ac27d8a0f2015bcac) )
#	ROM_LOAD( "prom16.16p", 0x004000, 0x02000, CRC(94814c5d) SHA1(e4ab6c0ae94184d5270cadb887f56e3550b6d9f2) )

#	ROM_REGION( 0x0300, "proms", 0 )
#	ROM_LOAD( "prom_red.3p", 0x0000, 0x0100, CRC(505fd8c2) SHA1(f2660fe512c76412a7b9f4be21fe549dd59fbda0) )
#	ROM_LOAD( "prom_grn.4p", 0x0100, 0x0100, CRC(6a00199d) SHA1(ff0ac7ae83d970778a756f7445afed3785fc1150) )
#	ROM_LOAD( "prom_blu.4r", 0x0200, 0x0100, CRC(7b4c5788) SHA1(ca02b12c19be7981daa070533455bd4d227d56cd) )

#	ROM_REGION( 0x2000, "user1", 0 )
#	ROM_LOAD( "prom10.16d", 0x0000,0x2000, CRC(a96e3c95) SHA1(a3b1c1723fcda80c11d9858819659e5e9dfe5dd3))
#ROM_END


