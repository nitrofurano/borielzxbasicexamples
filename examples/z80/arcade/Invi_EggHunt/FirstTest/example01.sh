rm egghunt.zip
rm example01.asm
rm egghunt/*
rmdir egghunt
mkdir egghunt





dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin





#cpu
#- zxb.py example01.zxb --asm --org=0x0000
#- zxb.py example01.zxb --org=0x0000
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=0x0069
zxb.py example01.zxb --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=egghunt/prg.bin

#ROM_START( egghunt )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "prg.bin", 0x00000, 0x20000, CRC(eb647145) SHA1(792951b76b5fac01e72ae34a2fe2108e373c5b62) )





#audio
dd bs=$((0x10000)) count=1 if=/dev/zero of=egghunt/rom2.bin
dd bs=$((0x80000)) count=1 if=/dev/zero of=egghunt/rom1.bin

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "rom2.bin", 0x00000, 0x10000, CRC(88a71bc3) SHA1(cf5acccfda9fda0d55af91a415a54391d0d0b7a2) )
#ROM_REGION( 0x80000, "oki", 0 )
#ROM_LOAD( "rom1.bin", 0x00000, 0x80000, CRC(f03589bc) SHA1(4d9c8422ac3c4c3ecba3bcf0ed47b8c7d5903f8c) )
#ROM_END





#gfx
zxb.py library/b4r0f0_charmapyumefuda01.zxi --org=0x0000
cat dummy128k.bin >> b4r0f0_charmapyumefuda01.bin
dd ibs=1 count=$((0x100000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=b4r0f0_charmapyumefuda01b.bin
xxd -p -c 4 -s 0 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom3.bin
xxd -p -c 4 -s 1 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom4.bin
xxd -p -c 4 -s 2 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom5.bin
xxd -p -c 4 -s 3 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom6.bin
xxd -p -c 4 -s 0 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom7.bin
xxd -p -c 4 -s 1 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom8.bin
xxd -p -c 4 -s 2 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom9.bin
xxd -p -c 4 -s 3 -l $((0x100000)) b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > egghunt/rom10.bin

#dd ibs=1 count=$((0x40000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom3.bin
#dd ibs=1 count=$((0x40000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom4.bin
#dd ibs=1 count=$((0x40000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom5.bin
#dd ibs=1 count=$((0x40000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom6.bin
#dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom7.bin
#dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom8.bin
#dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom9.bin
#dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=egghunt/rom10.bin
rm b4r0f0_charmapyumefuda01.bin b4r0f0_charmapyumefuda01b.bin

#ROM_REGION( 0x100000, "gfx1", ROMREGION_INVERT )
#ROM_LOAD( "rom3.bin", 0x00000, 0x40000, CRC(9d51ac49) SHA1(b0785d746fb2872a04386016ffdee80e6174dfc0) )
#ROM_LOAD( "rom4.bin", 0x40000, 0x40000, CRC(41c63041) SHA1(24e9a21d448c144db2356329cf87dc99598c96dc) )
#ROM_LOAD( "rom5.bin", 0x80000, 0x40000, CRC(6f96cb97) SHA1(7dde7d2aec6b5f9929b98d06c07bb07bf7bd59dd) )
#ROM_LOAD( "rom6.bin", 0xc0000, 0x40000, CRC(b5a41d4b) SHA1(1b4cf9c944e3eb7dc2d26d8a73bf5efb7b53253a) )
#ROM_REGION( 0x80000, "gfx2", ROMREGION_INVERT )
#ROM_LOAD( "rom7.bin", 0x00000, 0x20000, CRC(1b43fb57) SHA1(f06e186bf514f2ad655df23636eab72e6fafd815) )
#ROM_LOAD( "rom8.bin", 0x20000, 0x20000, CRC(f8122d0d) SHA1(78551c689b9e4eeed5e1ae97d8c7a907a388a9ff) )
#ROM_LOAD( "rom9.bin", 0x40000, 0x20000, CRC(dbfa0ffe) SHA1(2aa759c0bd3945473a6d8fa48226ce6c6c94d740) )
#ROM_LOAD( "rom10.bin",0x60000, 0x20000, CRC(14f5fc74) SHA1(769bccf9c1b42c35c3aee3866ed015de7c83b710) )

#dd ibs=1 count=$((0x6000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
#xxd -p -c 3 -s 0 -l 24576 charmap01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-2d
#xxd -p -c 3 -s 1 -l 24576 charmap01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-2j
#xxd -p -c 3 -s 2 -l 24576 charmap01b.bin | cut -b 1-2 | xxd -r -p > ldrun/lr-e-2f






#zxb.py library/spritemap01.zxi --org=0x0000
#cat dummy128k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=egghunt/pp_e02.rom
#rm spritemap01.bin
#zxb.py library/palette01.bas --org=0x0000
#cat dummy128k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=egghunt/egghunt.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=egghunt/egghunt.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=0x0000
#cat dummy128k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=egghunt/egghunt.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=egghunt/egghunt.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=egghunt/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=egghunt/82s126.3m

rm smsboot.bin dummy128k.bin example01.bin
zip -r egghunt egghunt
rm egghunt/*
rmdir egghunt
mame -w -video soft -resolution0 512x512 -rp ./ egghunt





#ROM_START( egghunt )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "prg.bin", 0x00000, 0x20000, CRC(eb647145) SHA1(792951b76b5fac01e72ae34a2fe2108e373c5b62) )

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "rom2.bin", 0x00000, 0x10000, CRC(88a71bc3) SHA1(cf5acccfda9fda0d55af91a415a54391d0d0b7a2) )
#ROM_REGION( 0x80000, "oki", 0 )
#ROM_LOAD( "rom1.bin", 0x00000, 0x80000, CRC(f03589bc) SHA1(4d9c8422ac3c4c3ecba3bcf0ed47b8c7d5903f8c) )
#ROM_END

#ROM_REGION( 0x100000, "gfx1", ROMREGION_INVERT )
#ROM_LOAD( "rom3.bin", 0x00000, 0x40000, CRC(9d51ac49) SHA1(b0785d746fb2872a04386016ffdee80e6174dfc0) )
#ROM_LOAD( "rom4.bin", 0x40000, 0x40000, CRC(41c63041) SHA1(24e9a21d448c144db2356329cf87dc99598c96dc) )
#ROM_LOAD( "rom5.bin", 0x80000, 0x40000, CRC(6f96cb97) SHA1(7dde7d2aec6b5f9929b98d06c07bb07bf7bd59dd) )
#ROM_LOAD( "rom6.bin", 0xc0000, 0x40000, CRC(b5a41d4b) SHA1(1b4cf9c944e3eb7dc2d26d8a73bf5efb7b53253a) )
#ROM_REGION( 0x80000, "gfx2", ROMREGION_INVERT )
#ROM_LOAD( "rom7.bin", 0x00000, 0x20000, CRC(1b43fb57) SHA1(f06e186bf514f2ad655df23636eab72e6fafd815) )
#ROM_LOAD( "rom8.bin", 0x20000, 0x20000, CRC(f8122d0d) SHA1(78551c689b9e4eeed5e1ae97d8c7a907a388a9ff) )
#ROM_LOAD( "rom9.bin", 0x40000, 0x20000, CRC(dbfa0ffe) SHA1(2aa759c0bd3945473a6d8fa48226ce6c6c94d740) )
#ROM_LOAD( "rom10.bin",0x60000, 0x20000, CRC(14f5fc74) SHA1(769bccf9c1b42c35c3aee3866ed015de7c83b710) )





