rm murogmbl.zip
rm example01.asm
rm murogmbl/*
rmdir murogmbl
mkdir murogmbl






dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin






#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=murogmbl/2532.5e

#ROM_START(murogmbl)
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD("2532.5e", 0x0000, 0x1000, CRC(093d4560) SHA1(d5401b5f7a2ebe5099fefc5b09f8710886e243b2) )







zxb.py library/b1r0f0_charmapminivaders01.zxi --org=$((0x0000))
cat dummy32k.bin >> b1r0f0_charmapminivaders01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0010)) if=b1r0f0_charmapminivaders01.bin of=murogmbl/2532.8b
dd ibs=1 count=$((0x800)) skip=$((0x0010)) if=b1r0f0_charmapminivaders01.bin of=murogmbl/2516.5b
cat murogmbl/2516.5b >> murogmbl/2532.8b #<-!!!-fix
rm b1r0f0_charmapminivaders01.bin

#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD("2532.8b", 0x0000, 0x0800, CRC(4427ffc0) SHA1(45f5fd0ae967cdb6abbf2e6c6d12d787556488ef) )
#ROM_CONTINUE(0x0000, 0x0800)
#ROM_LOAD("2516.5b",     0x0800, 0x0800, CRC(496ad48c) SHA1(28380c9d02b64e7d5ef2763de92cd2ca8861eceb) )





#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=murogmbl/murogmbl.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=murogmbl/murogmbl.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=murogmbl/murogmbl.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=murogmbl/murogmbl.11j
#rm clut01b.bin

dd bs=256 count=1 if=/dev/urandom of=murogmbl/74s288.a8

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "74s288.a8",  0x0000, 0x0020, CRC(fc35201c) SHA1(4549e228c48992e0d10957f029b89a547392e72b) )
#ROM_END






rm smsboot.bin dummy32k.bin example01.bin

zip -r murogmbl murogmbl
rm murogmbl/*
rmdir murogmbl

mame -w -video soft -resolution0 512x512 -rp ./ murogmbl







#ROM_START(murogmbl)
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD("2532.5e", 0x0000, 0x1000, CRC(093d4560) SHA1(d5401b5f7a2ebe5099fefc5b09f8710886e243b2) )

#ROM_REGION( 0x1000, "gfx1", 0 )
#ROM_LOAD("2532.8b", 0x0000, 0x0800, CRC(4427ffc0) SHA1(45f5fd0ae967cdb6abbf2e6c6d12d787556488ef) )
#ROM_CONTINUE(0x0000, 0x0800)
#ROM_LOAD("2516.5b",     0x0800, 0x0800, CRC(496ad48c) SHA1(28380c9d02b64e7d5ef2763de92cd2ca8861eceb) )

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "74s288.a8",  0x0000, 0x0020, CRC(fc35201c) SHA1(4549e228c48992e0d10957f029b89a547392e72b) )
#ROM_END





