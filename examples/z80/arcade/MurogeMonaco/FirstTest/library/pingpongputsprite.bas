sub pingpongputsprite(tlayer as ubyte,tx as ubyte,ty as ubyte,tid as ubyte,tclut as ubyte):
  poke $4FF0+((tlayer band 7)*2),tid          '- shape sprite 0
  poke $4FF1+((tlayer band 7)*2),tclut        '- clut? sprite 0
  poke $5060+((tlayer band 7)*2),tx  bxor $ff '- x sprite 0
  poke $5061+((tlayer band 7)*2),ty bxor $ff  '- y sprite 0
  end sub


