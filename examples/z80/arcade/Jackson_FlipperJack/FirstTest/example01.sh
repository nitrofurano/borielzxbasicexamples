rm flipjack.zip
rm example01.asm
rm flipjack/*
rmdir flipjack
mkdir flipjack





dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=flipjack/3.d5
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=flipjack/4.f5
dd bs=$((0x2000)) count=1 if=/dev/zero of=flipjack/1.l5
dd bs=$((0x2000)) count=1 if=/dev/zero of=flipjack/2.m5
#ROM_START( flipjack )
#ROM_REGION( 0x14000, "maincpu", 0 )
#ROM_LOAD( "3.d5", 0x00000, 0x2000, CRC(123bd992) SHA1(d845e2b9af5b81d950e5edf35201f1dd1c4af651) )
#ROM_LOAD( "4.f5", 0x08000, 0x2000, CRC(d27e0184) SHA1(f108993fc3fce9173a4961a76fc60655fdd1cd25) )
#ROM_LOAD( "1.l5", 0x10000, 0x2000, CRC(4632263b) SHA1(b1fbb851ffd8aff36aff6f36672122fef3dd0af1) )
#ROM_LOAD( "2.m5", 0x12000, 0x2000, CRC(e2bdce13) SHA1(50d990095a35837570b3117763e990440d8656ae) )




#sound
dd bs=$((0x2000)) count=1 if=/dev/zero of=flipjack/s.s5
#ROM_REGION( 0x2000, "audiocpu", 0 )
#ROM_LOAD( "s.s5",  0x0000, 0x2000, CRC(34515a7b) SHA1(affe34198b77bddd314fae2851fd6a29d80f734e) )








zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000))
cat dummy64k.bin >> b1r1f0_charmapkamikaze01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=flipjack/cg.l6
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=flipjack/b.h6
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=flipjack/r.f6
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=flipjack/g.d6
rm b1r1f0_charmapkamikaze01.bin

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "cg.l6", 0x0000, 0x2000, CRC(8d87f6b9) SHA1(55ca726f190eac9ee7e26b8f4e519f1634bec0dd) )
#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "b.h6",  0x0000, 0x2000, CRC(bbc8fdcc) SHA1(93758ca13cc49b87508f01c86c652155945dd484) )
#ROM_LOAD( "r.f6",  0x2000, 0x2000, CRC(8c02fe71) SHA1(148e7382dc9b7678c447ada5ad19e03a3a051a7f) )
#ROM_LOAD( "g.d6",  0x4000, 0x2000, CRC(8624d07f) SHA1(fb51c9c785d56854a6530b71868e95ad6be7cbee) )

#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=flipjack/pp_e02.rom
#rm spritemap01.bin







dd bs=$((0x0100)) count=1 if=/dev/urandom of=flipjack/m3-7611-5.f8


#ROM_REGION( 0x0100, "proms", 0 )
#ROM_LOAD( "m3-7611-5.f8", 0x0000, 0x0100, CRC(f0248102) SHA1(22d87935c941e2e8bba5427599f6fd5fa1262ebc) )
#ROM_END

#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=flipjack/flipjack.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=flipjack/flipjack.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=flipjack/flipjack.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=flipjack/flipjack.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=flipjack/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=flipjack/82s126.3m





rm smsboot.bin dummy64k.bin example01.bin

zip -r flipjack flipjack
rm flipjack/*
rmdir flipjack

mame -w -video soft -resolution0 512x512 -rp ./ flipjack






#ROM_START( flipjack )
#ROM_REGION( 0x14000, "maincpu", 0 )
#ROM_LOAD( "3.d5", 0x00000, 0x2000, CRC(123bd992) SHA1(d845e2b9af5b81d950e5edf35201f1dd1c4af651) )
#ROM_LOAD( "4.f5", 0x08000, 0x2000, CRC(d27e0184) SHA1(f108993fc3fce9173a4961a76fc60655fdd1cd25) )
#ROM_LOAD( "1.l5", 0x10000, 0x2000, CRC(4632263b) SHA1(b1fbb851ffd8aff36aff6f36672122fef3dd0af1) )
#ROM_LOAD( "2.m5", 0x12000, 0x2000, CRC(e2bdce13) SHA1(50d990095a35837570b3117763e990440d8656ae) )

#ROM_REGION( 0x2000, "audiocpu", 0 )
#ROM_LOAD( "s.s5",  0x0000, 0x2000, CRC(34515a7b) SHA1(affe34198b77bddd314fae2851fd6a29d80f734e) )

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "cg.l6", 0x0000, 0x2000, CRC(8d87f6b9) SHA1(55ca726f190eac9ee7e26b8f4e519f1634bec0dd) )

#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "b.h6",  0x0000, 0x2000, CRC(bbc8fdcc) SHA1(93758ca13cc49b87508f01c86c652155945dd484) )
#ROM_LOAD( "r.f6",  0x2000, 0x2000, CRC(8c02fe71) SHA1(148e7382dc9b7678c447ada5ad19e03a3a051a7f) )
#ROM_LOAD( "g.d6",  0x4000, 0x2000, CRC(8624d07f) SHA1(fb51c9c785d56854a6530b71868e95ad6be7cbee) )

#ROM_REGION( 0x0100, "proms", 0 )
#ROM_LOAD( "m3-7611-5.f8", 0x0000, 0x0100, CRC(f0248102) SHA1(22d87935c941e2e8bba5427599f6fd5fa1262ebc) )
#ROM_END



