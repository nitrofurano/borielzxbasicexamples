rm superwng.zip
rm example01.asm
rm superwng/*
rmdir superwng
mkdir superwng






dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin






#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=superwng/2.5l
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=superwng/3.5m
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=superwng/4.5p
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=superwng/5.5r
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=superwng/6.8s

#ROM_START( superwng )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "2.5l",         0x0000, 0x2000, CRC(8d102f8d) SHA1(ff6d994273a2e493a68637822cd0b1a2f69fd054) )
#ROM_LOAD( "3.5m",         0x2000, 0x2000, CRC(3b08bd19) SHA1(2020e2835df86a6a279bbf9d013a489f0e32a4bd) )
#ROM_LOAD( "4.5p",         0x4000, 0x2000, CRC(6a49746d) SHA1(f5cd5eb77f60972a3897243f9ee3d61aac0878fc) )
#ROM_LOAD( "5.5r",         0x6000, 0x2000, CRC(ebd23487) SHA1(16e8faf989aa80dbf9934450ec4ba642a6f88c63) )
#ROM_LOAD( "6.8s",         0x8000, 0x4000, BAD_DUMP CRC(774433e0) SHA1(82b10d797581c14914bcce320f2aa5d3fb1fba33) ) // banked but probably bad, bits at 0xxx39 offsets appear to be missing / corrupt.







dd bs=$((0x2000)) count=1 if=/dev/zero of=superwng/1.1a

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "1.1a",         0x0000, 0x2000, CRC(a70aa39e) SHA1(b03de65d7bd020eb77495997128dce5ccbdbefac) )








zxb.py library/b2r1f0i_charmapsuperwing01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f0i_charmapsuperwing01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b2r1f0i_charmapsuperwing01.bin of=superwng/7.8p
rm b2r1f0i_charmapsuperwing01.bin

#ROM_REGION( 0x4000, "gfx1", 0 )
#ROM_LOAD( "7.8p",        0x0000, 0x4000, CRC(b472603c) SHA1(96f477a47a5be3db1292fea4f5c91ab155013f74) )







#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=superwng/superwng.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=superwng/superwng.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=superwng/superwng.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=superwng/superwng.11j
#rm clut01b.bin
#dd bs=256 count=1 if=/dev/zero of=superwng/82s126.1m

dd bs=256 count=1 if=/dev/zero of=superwng/bprom.bin

#ROM_REGION( 0x0040, "proms", 0 )
#ROM_LOAD( "bprom.bin", 0x0000, 0x0040, NO_DUMP)
#ROM_END



rm smsboot.bin dummy32k.bin example01.bin

zip -r superwng superwng
rm superwng/*
rmdir superwng

mame -w -video soft -resolution0 512x512 -rp ./ superwng







#ROM_START( superwng )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "2.5l",         0x0000, 0x2000, CRC(8d102f8d) SHA1(ff6d994273a2e493a68637822cd0b1a2f69fd054) )
#ROM_LOAD( "3.5m",         0x2000, 0x2000, CRC(3b08bd19) SHA1(2020e2835df86a6a279bbf9d013a489f0e32a4bd) )
#ROM_LOAD( "4.5p",         0x4000, 0x2000, CRC(6a49746d) SHA1(f5cd5eb77f60972a3897243f9ee3d61aac0878fc) )
#ROM_LOAD( "5.5r",         0x6000, 0x2000, CRC(ebd23487) SHA1(16e8faf989aa80dbf9934450ec4ba642a6f88c63) )
#ROM_LOAD( "6.8s",         0x8000, 0x4000, BAD_DUMP CRC(774433e0) SHA1(82b10d797581c14914bcce320f2aa5d3fb1fba33) ) // banked but probably bad, bits at 0xxx39 offsets appear to be missing / corrupt.

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "1.1a",         0x0000, 0x2000, CRC(a70aa39e) SHA1(b03de65d7bd020eb77495997128dce5ccbdbefac) )

#ROM_REGION( 0x4000, "gfx1", 0 )
#ROM_LOAD( "7.8p",        0x0000, 0x4000, CRC(b472603c) SHA1(96f477a47a5be3db1292fea4f5c91ab155013f74) )

#ROM_REGION( 0x0040, "proms", 0 )
#ROM_LOAD( "bprom.bin", 0x0000, 0x0040, NO_DUMP)
#ROM_END








