	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$97FF
#line 1
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 10
	push af
	ld a, 21
	push af
	xor a
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 30
	push af
	ld a, 21
	push af
	ld a, 1
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 50
	push af
	ld a, 21
	push af
	ld a, 2
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 70
	push af
	ld a, 21
	push af
	ld a, 3
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 90
	push af
	ld a, 21
	push af
	ld a, 4
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 110
	push af
	ld a, 21
	push af
	ld a, 5
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 130
	push af
	ld a, 21
	push af
	ld a, 6
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 150
	push af
	ld a, 21
	push af
	ld a, 7
	push af
	call _pingpongputsprite
	ld hl, 0
	ld (_seed), hl
	ld hl, 64
	ld (_ee2), hl
	jp __LABEL0
__LABEL3:
	ld hl, 1
	ld (_ee3), hl
	jp __LABEL5
__LABEL8:
	ld hl, (_ee2)
	ld de, 33792
	add hl, de
	push hl
	ld a, 1
	pop hl
	ld (hl), a
__LABEL9:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL5:
	ld hl, 100
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
__LABEL4:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL0:
	ld hl, 959
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	xor a
	ld (_ee4), a
	ld hl, 3
	ld (_ee2), hl
	jp __LABEL10
__LABEL13:
	ld hl, 15
	ld (_ee3), hl
	jp __LABEL15
__LABEL18:
	ld hl, (_ee3)
	ld de, 33792
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (_ee4)
	pop hl
	ld (hl), a
	ld a, (_ee4)
	inc a
	ld (_ee4), a
__LABEL19:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL15:
	ld hl, 30
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
__LABEL14:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL10:
	ld hl, 18
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
	xor a
	ld (_ee4), a
	ld hl, 20
	ld (_ee2), hl
	jp __LABEL20
__LABEL23:
	ld hl, 15
	ld (_ee3), hl
	jp __LABEL25
__LABEL28:
	ld hl, (_ee3)
	ld de, 33792
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld h, 3
	ld a, (_ee4)
	and h
	pop hl
	ld (hl), a
	ld hl, (_ee3)
	ld de, 32768
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (_ee4)
	ld h, 4
	call __DIVU8_FAST
	ld l, a
	ld h, 0
	ld e, h
	ld d, h
	ld a, l
	pop hl
	ld (hl), a
	ld a, (_ee4)
	inc a
	ld (_ee4), a
__LABEL29:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL25:
	ld hl, 30
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
__LABEL24:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL20:
	ld hl, 27
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
	ld hl, 4
	ld (_ee2), hl
	jp __LABEL30
__LABEL33:
	ld hl, 2
	ld (_ee3), hl
	jp __LABEL35
__LABEL38:
	ld hl, (_ee3)
	ld de, 33792
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, 88
	pop hl
	ld (hl), a
__LABEL39:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL35:
	ld hl, 5
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL38
__LABEL37:
__LABEL34:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL30:
	ld hl, 7
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
	ld hl, 2342
	push hl
	ld hl, 63888
	push hl
	ld hl, 2445
	push hl
	ld hl, 377
	push hl
	call _test1
	ld hl, 16903
	ld (_eee), hl
	ld hl, 81
	ld (_i), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	ld hl, 84
	ld (_i), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 0
	ld (_seed), hl
	ld hl, 17408
	ld (_eee), hl
	jp __LABEL40
__LABEL43:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL44:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL40:
	ld hl, 18431
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL43
__LABEL42:
	ld hl, 80
	ld (_ex0), hl
	ld hl, 176
	ld (_ey0), hl
	ld hl, 0
	ld (_eee), hl
	xor a
	ld (_ecsp), a
	xor a
	ld (_ecsq), a
__LABEL45:
	ld a, 255
	ld (42496), a
	ld a, (43008)
	cpl
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld a, (43136)
	cpl
	ld l, a
	ld h, 0
	ld (_ej1), hl
	ld hl, (_ej0)
	ld a, l
	ld (34470), a
	ld hl, (_ej1)
	ld a, l
	ld (34471), a
	ld hl, (_ex0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (34408), a
	ld hl, (_ex0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34407), a
	ld hl, (_ex0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34406), a
	ld hl, (_ey0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (34376), a
	ld hl, (_ey0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34375), a
	ld hl, (_ey0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34374), a
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (34344), a
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34343), a
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34342), a
	ld hl, (_eee)
	ld de, 1000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34341), a
	ld hl, (_eee)
	ld de, 10000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34340), a
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (34312), a
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34311), a
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34310), a
	ld hl, (_ej1)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (34280), a
	ld hl, (_ej1)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34279), a
	ld hl, (_ej1)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (34278), a
	ld a, (_ecsp)
	ld h, 10
	call __MODU8_FAST
	add a, 48
	ld (34248), a
	ld de, 64
	ld hl, (_ej1)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL48
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL48:
	ld de, 32
	ld hl, (_ej1)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL50
	ld hl, (_ex0)
	dec hl
	ld (_ex0), hl
__LABEL50:
	ld de, 4
	ld hl, (_ej1)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL52
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL52:
	ld de, 2
	ld hl, (_ej1)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL54
	ld hl, (_ey0)
	dec hl
	ld (_ey0), hl
__LABEL54:
	ld de, 15
	ld hl, (_ej1)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL56
	ld a, (_ecsq)
	inc a
	ld (_ecsq), a
__LABEL56:
	ld a, (_ecsq)
	ld h, 4
	call __DIVU8_FAST
	ld l, a
	ld h, 0
	ld e, h
	ld d, h
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __SWAP32
	call __MODI32
	ld a, l
	ld (_ecsp), a
	ld hl, (_eee)
	ld de, 4
	call __DIVU16
	ld de, 0
	ld (_ee3), hl
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	xor a
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 1
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 2
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 3
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	ld a, 4
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 5
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 6
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 7
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	ld a, 8
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 9
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 10
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 11
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	ld a, 12
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 13
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 14
	push af
	call _pingpongputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 15
	push af
	call _pingpongputsprite
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL45
__LABEL46:
__LABEL__text01:
#line 170
		defb "HELLO WORLD!!!!!"
#line 171
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_pacmandelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
		push bc
pacmandelayloop:
		dec bc
		ld a,b
		or c
		jp nz,pacmandelayloop
		pop bc
#line 10
_pacmandelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_pingpongputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	push af
	ld h, 15
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 36870
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 15
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 36868
	add hl, de
	push hl
	ld a, (ix+9)
	cpl
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 15
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 36869
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 15
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 36867
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
_pingpongputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_test1:
	push ix
	ld ix, 0
	add ix, sp
_test1__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_zpoke:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
_zpoke__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_putchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ld de, 33792
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_putchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1254 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1255 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1256 "example01.zxb"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1257 "example01.zxb"
#line 1 "div8.asm"
				; --------------------------------
__DIVU8:	; 8 bit unsigned integer division 
				; Divides (Top of stack, High Byte) / A
		pop hl	; --------------------------------
		ex (sp), hl	; CALLEE
	
__DIVU8_FAST:	; Does A / H
		ld l, h
		ld h, a		; At this point do H / L
	
		ld b, 8
		xor a		; A = 0, Carry Flag = 0
		
__DIV8LOOP:
		sla	h		
		rla			
		cp	l		
		jr	c, __DIV8NOSUB
		sub	l		
		inc	h		
	
__DIV8NOSUB:	
		djnz __DIV8LOOP
	
		ld	l, a		; save remainder
		ld	a, h		; 
		
		ret			; a = Quotient, 
	
	
					; --------------------------------
__DIVI8:		; 8 bit signed integer division Divides (Top of stack) / A
		pop hl		; --------------------------------
		ex (sp), hl
	
__DIVI8_FAST:
		ld e, a		; store operands for later
		ld c, h
	
		or a		; negative?
		jp p, __DIV8A
		neg			; Make it positive
	
__DIV8A:
		ex af, af'
		ld a, h
		or a
		jp p, __DIV8B
		neg
		ld h, a		; make it positive
	
__DIV8B:
		ex af, af'
	
		call __DIVU8_FAST
	
		ld a, c
		xor l		; bit 7 of A = 1 if result is negative
	
		ld a, h		; Quotient
		ret p		; return if positive	
	
		neg
		ret
		
	
__MODU8:		; 8 bit module. REturns A mod (Top of stack) (unsigned operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODU8_FAST:	; __FASTCALL__ entry
		call __DIVU8_FAST
		ld a, l		; Remainder
	
		ret		; a = Modulus
	
	
__MODI8:		; 8 bit module. REturns A mod (Top of stack) (For singed operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODI8_FAST:	; __FASTCALL__ entry
		call __DIVI8_FAST
		ld a, l		; remainder
	
		ret		; a = Modulus
	
#line 1258 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1259 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1260 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1261 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_i EQU 37120
	_eee EQU 37122
	_seed EQU 37124
	_ee2 EQU 37126
	_ee3 EQU 37128
	_ee4 EQU 37130
	_ecsp EQU 37132
	_ecsq EQU 37134
	_ex0 EQU 37136
	_ey0 EQU 37138
	_ej0 EQU 37140
	_ej1 EQU 37142
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
