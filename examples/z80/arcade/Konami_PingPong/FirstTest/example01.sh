rm pingpong.zip
rm example01.asm
rm pingpong/*
rmdir pingpong
mkdir pingpong

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=pingpong/pp_e04.rom
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=pingpong/pp_e03.rom



zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy32k.bin >> charmap01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=charmap01.bin of=pingpong/pp_e01.rom
rm charmap01.bin

zxb.py library/spritemap01.zxi --org=$((0x0000))
cat dummy32k.bin >> spritemap01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=pingpong/pp_e02.rom
rm spritemap01.bin



zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=pingpong/pingpong.3j
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=pingpong/pingpong.3j


#- clut01.zxi might need to be different files
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy32k.bin >> clut01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
rm clut01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=pingpong/pingpong.5h
dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=pingpong/pingpong.11j
rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=pingpong/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=pingpong/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r pingpong pingpong
rm pingpong/*
rmdir pingpong

mame -w -video soft -resolution0 512x512 -rp ./ pingpong


