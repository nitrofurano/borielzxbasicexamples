rm blueprnt.zip
rm example01.asm
rm blueprnt/*
rmdir blueprnt
mkdir blueprnt





dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin




#- zxb.py example01.zxb --asm --org=0x0000
#- zxb.py example01.zxb --org=0x0000
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=0x0069
zxb.py example01.zxb --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=blueprnt/bp-1.1m
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=blueprnt/bp-2.1n
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=blueprnt/bp-3.1p
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=blueprnt/bp-4.1r
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=blueprnt/bp-5.1s

#ROM_START( blueprnt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "bp-1.1m",   0x0000, 0x1000, CRC(b20069a6) SHA1(aa0a61c898ec58fc4872a24666f422e1abdc09f3) )
#ROM_LOAD( "bp-2.1n",   0x1000, 0x1000, CRC(4a30302e) SHA1(a3a22b78585cc9677bf03bbfeb20afb05f026075) )
#ROM_LOAD( "bp-3.1p",   0x2000, 0x1000, CRC(6866ca07) SHA1(a0df14eee9240fad42ceb6f926d34755e8442411) )
#ROM_LOAD( "bp-4.1r",   0x3000, 0x1000, CRC(5d3cfac3) SHA1(7e6ab8398d799aaf0fcaa0769a827471d8c872e9) )
#ROM_LOAD( "bp-5.1s",   0x4000, 0x1000, CRC(a556cac4) SHA1(0fe7070c70792d883c29f3d12a33238b5ed8af22) )







dd bs=$((0x1000)) count=1 if=/dev/zero of=blueprnt/snd-1.3u
dd bs=$((0x1000)) count=1 if=/dev/zero of=blueprnt/snd-2.3v

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "snd-1.3u",  0x0000, 0x1000, CRC(fd38777a) SHA1(0ed230e0fa047d3171e7141e5620b4c750b07629) )
#ROM_LOAD( "snd-2.3v",  0x2000, 0x1000, CRC(33d5bf5b) SHA1(3ac684cd48559cd0eab32f9e7ce3ec6eca88dcd4) )








zxb.py library/b2r3f0_charmapblueprint01.zxi --org=0x0000
cat dummy32k.bin >> b2r3f0_charmapblueprint01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r3f0_charmapblueprint01.bin of=b2r3f0_charmapblueprint01b.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r3f0_charmapblueprint01b.bin | cut -b 1-2 | xxd -r -p > blueprnt/bg-1.3c
xxd -p -c 2 -s 1 -l $((0x2000)) b2r3f0_charmapblueprint01b.bin | cut -b 1-2 | xxd -r -p > blueprnt/bg-2.3d
rm b2r3f0_charmapblueprint01.bin b2r3f0_charmapblueprint01b.bin



zxb.py library/b1r3f0_charmapspaceintruder01.zxi --org=0x0000
cat dummy32k.bin >> b1r3f0_charmapspaceintruder01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=blueprnt/bg-1.3c
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=blueprnt/bg-2.3d
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=blueprnt/red.17d
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=blueprnt/blue.18d
dd ibs=1 count=$((0x0001)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=blueprnt/green.20d
rm b1r3f0_charmapspaceintruder01.bin

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "bg-1.3c",   0x0000, 0x1000, CRC(ac2a61bc) SHA1(e56708d261648478d1dae4769118546411299e59) )
#ROM_LOAD( "bg-2.3d",   0x1000, 0x1000, CRC(81fe85d7) SHA1(fa637631d25f7499d2325cce77d11e1d624f5e07) )
#ROM_REGION( 0x3000, "gfx2", 0 )
#ROM_LOAD( "red.17d",   0x0000, 0x1000, CRC(a73b6483) SHA1(9f7756d032a8ffaa4aa236fc5117f476916986e0) )
#ROM_LOAD( "blue.18d",  0x1000, 0x1000, CRC(7d622550) SHA1(8283debff8253996513148629ec55831e48e8e92) )
#ROM_LOAD( "green.20d", 0x2000, 0x1000, CRC(2fcb4f26) SHA1(508cb2800737bad0a7dea0789d122b7c802aecfd) )
#ROM_END






#zxb.py library/palette01.bas --org=0x0000
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=blueprnt/blueprnt.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=blueprnt/blueprnt.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=0x0000
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=blueprnt/blueprnt.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=blueprnt/blueprnt.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=blueprnt/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=blueprnt/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r blueprnt blueprnt
rm blueprnt/*
rmdir blueprnt

mame -w -video soft -resolution0 512x512 -rp ./ blueprnt







#ROM_START( blueprnt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "bp-1.1m",   0x0000, 0x1000, CRC(b20069a6) SHA1(aa0a61c898ec58fc4872a24666f422e1abdc09f3) )
#ROM_LOAD( "bp-2.1n",   0x1000, 0x1000, CRC(4a30302e) SHA1(a3a22b78585cc9677bf03bbfeb20afb05f026075) )
#ROM_LOAD( "bp-3.1p",   0x2000, 0x1000, CRC(6866ca07) SHA1(a0df14eee9240fad42ceb6f926d34755e8442411) )
#ROM_LOAD( "bp-4.1r",   0x3000, 0x1000, CRC(5d3cfac3) SHA1(7e6ab8398d799aaf0fcaa0769a827471d8c872e9) )
#ROM_LOAD( "bp-5.1s",   0x4000, 0x1000, CRC(a556cac4) SHA1(0fe7070c70792d883c29f3d12a33238b5ed8af22) )

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "snd-1.3u",  0x0000, 0x1000, CRC(fd38777a) SHA1(0ed230e0fa047d3171e7141e5620b4c750b07629) )
#ROM_LOAD( "snd-2.3v",  0x2000, 0x1000, CRC(33d5bf5b) SHA1(3ac684cd48559cd0eab32f9e7ce3ec6eca88dcd4) )

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "bg-1.3c",   0x0000, 0x1000, CRC(ac2a61bc) SHA1(e56708d261648478d1dae4769118546411299e59) )
#ROM_LOAD( "bg-2.3d",   0x1000, 0x1000, CRC(81fe85d7) SHA1(fa637631d25f7499d2325cce77d11e1d624f5e07) )
#ROM_REGION( 0x3000, "gfx2", 0 )
#ROM_LOAD( "red.17d",   0x0000, 0x1000, CRC(a73b6483) SHA1(9f7756d032a8ffaa4aa236fc5117f476916986e0) )
#ROM_LOAD( "blue.18d",  0x1000, 0x1000, CRC(7d622550) SHA1(8283debff8253996513148629ec55831e48e8e92) )
#ROM_LOAD( "green.20d", 0x2000, 0x1000, CRC(2fcb4f26) SHA1(508cb2800737bad0a7dea0789d122b7c802aecfd) )
#ROM_END







