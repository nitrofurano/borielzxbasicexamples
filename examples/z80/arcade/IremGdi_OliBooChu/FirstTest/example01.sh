rm olibochu.zip
rm example01.asm
rm olibochu/*
rmdir olibochu
mkdir olibochu





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#cpu
#- zxb.py example01.zxb --asm --org=0x0000
#- zxb.py example01.zxb --org=0x0000
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=0x0069
zxb.py example01.zxb --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=olibochu/1b.3n
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=olibochu/2b.3lm
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=olibochu/3b.3k
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=olibochu/4b.3j
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=olibochu/5b.3h
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=olibochu/6b.3f
dd ibs=1 count=$((0x1000)) skip=$((0x6000)) if=example01.bin of=olibochu/7c.3e
dd ibs=1 count=$((0x1000)) skip=$((0x7000)) if=example01.bin of=olibochu/8b.3d

#ROM_START( olibochu )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* main CPU */
#ROM_LOAD( "1b.3n",        0x0000, 0x1000, CRC(bf17f4f4) SHA1(1075456f4b70a68548e0e1b6271fd4b845a77ce4) )
#ROM_LOAD( "2b.3lm",       0x1000, 0x1000, CRC(63833b0d) SHA1(0135c449c92470241d03a87709c739209139d660) )
#ROM_LOAD( "3b.3k",        0x2000, 0x1000, CRC(a4038e8b) SHA1(d7dce830239c8975ac135b213a99eec0c20ec3e2) )
#ROM_LOAD( "4b.3j",        0x3000, 0x1000, CRC(aad4bec4) SHA1(9203564ac841a8de2f9b8183d4086acce95e3d47) )
#ROM_LOAD( "5b.3h",        0x4000, 0x1000, CRC(66efa79f) SHA1(535369d958461834435d3202cd7310ecd0aa528c) )
#ROM_LOAD( "6b.3f",        0x5000, 0x1000, CRC(1123d1ef) SHA1(6094e732e61915c45b14acd90c1343f05385daf4) )
#ROM_LOAD( "7c.3e",        0x6000, 0x1000, CRC(89c26fb4) SHA1(ebc51e40612af894b20bd7fc3a5179cd35aaac9b) )
#ROM_LOAD( "8b.3d",        0x7000, 0x1000, CRC(af19e5a5) SHA1(5a55bbee5b2f20e2988171a310c8293dabbd9a72) )





#audio
dd bs=$((0x1000)) count=1 if=/dev/zero of=olibochu/17.4j
dd bs=$((0x1000)) count=1 if=/dev/zero of=olibochu/18.4l
dd bs=$((0x1000)) count=1 if=/dev/zero of=olibochu/15.1k
dd bs=$((0x1000)) count=1 if=/dev/zero of=olibochu/16.1m

#ROM_REGION( 0x10000, "audiocpu", 0 )    /* sound CPU */
#ROM_LOAD( "17.4j",        0x0000, 0x1000, CRC(57f07402) SHA1(a763a835ac512c69b4351c1ec72b0a64e46203aa) )
#ROM_LOAD( "18.4l",        0x1000, 0x1000, CRC(0a903e9c) SHA1(d893c2f5373f748d8bebf3673b15014f4a8d4b5c) )
#ROM_REGION( 0x2000, "samples", 0 )  /* samples? */
#ROM_LOAD( "15.1k",        0x0000, 0x1000, CRC(fb5dd281) SHA1(fba947ae7b619c2559b5af69ef02acfb15733f0d) )
#ROM_LOAD( "16.1m",        0x1000, 0x1000, CRC(c07614a5) SHA1(d13d271a324f99d008429c16193c4504e5894493) )






zxb.py library/b2r1f1_charmapmrdo01.zxi --org=0x0000
cat dummy32k.bin >> b2r1f1_charmapmrdo01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/13.6n
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/14.4n
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/9.6a
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/10.2a
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/11.4a
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=olibochu/12.2a
rm b2r1f1_charmapmrdo01.bin
xxd -p -c 2 -s 0 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/13.6n
xxd -p -c 2 -s 1 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/14.4n
xxd -p -c 2 -s 0 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/9.6a
xxd -p -c 2 -s 1 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/10.2a
xxd -p -c 2 -s 0 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/11.4a
xxd -p -c 2 -s 1 -l $((0x100000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > olibochu/12.2a
rm b2r1f1_charmapmrdo01b.bin

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "13.6n",        0x0000, 0x1000, CRC(b4fcf9af) SHA1(b360daa0670160dca61512823c98bc37ad99b9cf) )
#ROM_LOAD( "14.4n",        0x1000, 0x1000, CRC(af54407e) SHA1(1883928b721e03e452fd0c626c403dc374b02ed7) )
#ROM_REGION( 0x4000, "gfx2", 0 )
#ROM_LOAD( "9.6a",         0x0000, 0x1000, CRC(fa69e16e) SHA1(5a493a0a108b3e496884d1f499f3445d4e241ecd) )
#ROM_LOAD( "10.2a",        0x1000, 0x1000, CRC(10359f84) SHA1(df55f06fd98233d0efbc30e3e24bf9b8cab1a5cc) )
#ROM_LOAD( "11.4a",        0x2000, 0x1000, CRC(1d968f5f) SHA1(4acf78d865ca36355bb15dc1d476f5e97a5d91b7) )
#ROM_LOAD( "12.2a",        0x3000, 0x1000, CRC(d8f0c157) SHA1(a7b0c873e016c3b3252c2c9b6400b0fd3d650b2f) )




#proms
dd bs=$((0x100)) count=1 if=/dev/urandom of=olibochu/c-1
dd bs=$((0x100)) count=1 if=/dev/urandom of=olibochu/c-2
dd bs=$((0x100)) count=1 if=/dev/urandom of=olibochu/c-3

#ROM_REGION( 0x0220, "proms", 0 )
#ROM_LOAD( "c-1",          0x0000, 0x0020, CRC(e488e831) SHA1(6264741f7091c614093ae1ea4f6ead3d0cef83d3) )    /* palette */
#ROM_LOAD( "c-2",          0x0020, 0x0100, CRC(698a3ba0) SHA1(3c1a6cb881ef74647c651462a27d812234408e45) )    /* sprite lookup table */
#ROM_LOAD( "c-3",          0x0120, 0x0100, CRC(efc4e408) SHA1(f0796426cf324791853aa2ae6d0c3d1f8108d5c2) )    /* char lookup table */
#ROM_END






#zxb.py library/spritemap01.zxi --org=0x0000
#cat dummy32k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=olibochu/pp_e02.rom
#rm spritemap01.bin
#zxb.py library/palette01.bas --org=0x0000
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=olibochu/olibochu.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=olibochu/olibochu.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=0x0000
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=olibochu/olibochu.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=olibochu/olibochu.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=olibochu/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=olibochu/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r olibochu olibochu
rm olibochu/*
rmdir olibochu

mame -w -video soft -resolution0 512x512 -rp ./ olibochu







#ROM_START( olibochu )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* main CPU */
#ROM_LOAD( "1b.3n",        0x0000, 0x1000, CRC(bf17f4f4) SHA1(1075456f4b70a68548e0e1b6271fd4b845a77ce4) )
#ROM_LOAD( "2b.3lm",       0x1000, 0x1000, CRC(63833b0d) SHA1(0135c449c92470241d03a87709c739209139d660) )
#ROM_LOAD( "3b.3k",        0x2000, 0x1000, CRC(a4038e8b) SHA1(d7dce830239c8975ac135b213a99eec0c20ec3e2) )
#ROM_LOAD( "4b.3j",        0x3000, 0x1000, CRC(aad4bec4) SHA1(9203564ac841a8de2f9b8183d4086acce95e3d47) )
#ROM_LOAD( "5b.3h",        0x4000, 0x1000, CRC(66efa79f) SHA1(535369d958461834435d3202cd7310ecd0aa528c) )
#ROM_LOAD( "6b.3f",        0x5000, 0x1000, CRC(1123d1ef) SHA1(6094e732e61915c45b14acd90c1343f05385daf4) )
#ROM_LOAD( "7c.3e",        0x6000, 0x1000, CRC(89c26fb4) SHA1(ebc51e40612af894b20bd7fc3a5179cd35aaac9b) )
#ROM_LOAD( "8b.3d",        0x7000, 0x1000, CRC(af19e5a5) SHA1(5a55bbee5b2f20e2988171a310c8293dabbd9a72) )

#ROM_REGION( 0x10000, "audiocpu", 0 )    /* sound CPU */
#ROM_LOAD( "17.4j",        0x0000, 0x1000, CRC(57f07402) SHA1(a763a835ac512c69b4351c1ec72b0a64e46203aa) )
#ROM_LOAD( "18.4l",        0x1000, 0x1000, CRC(0a903e9c) SHA1(d893c2f5373f748d8bebf3673b15014f4a8d4b5c) )
#ROM_REGION( 0x2000, "samples", 0 )  /* samples? */
#ROM_LOAD( "15.1k",        0x0000, 0x1000, CRC(fb5dd281) SHA1(fba947ae7b619c2559b5af69ef02acfb15733f0d) )
#ROM_LOAD( "16.1m",        0x1000, 0x1000, CRC(c07614a5) SHA1(d13d271a324f99d008429c16193c4504e5894493) )

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "13.6n",        0x0000, 0x1000, CRC(b4fcf9af) SHA1(b360daa0670160dca61512823c98bc37ad99b9cf) )
#ROM_LOAD( "14.4n",        0x1000, 0x1000, CRC(af54407e) SHA1(1883928b721e03e452fd0c626c403dc374b02ed7) )
#ROM_REGION( 0x4000, "gfx2", 0 )
#ROM_LOAD( "9.6a",         0x0000, 0x1000, CRC(fa69e16e) SHA1(5a493a0a108b3e496884d1f499f3445d4e241ecd) )
#ROM_LOAD( "10.2a",        0x1000, 0x1000, CRC(10359f84) SHA1(df55f06fd98233d0efbc30e3e24bf9b8cab1a5cc) )
#ROM_LOAD( "11.4a",        0x2000, 0x1000, CRC(1d968f5f) SHA1(4acf78d865ca36355bb15dc1d476f5e97a5d91b7) )
#ROM_LOAD( "12.2a",        0x3000, 0x1000, CRC(d8f0c157) SHA1(a7b0c873e016c3b3252c2c9b6400b0fd3d650b2f) )

#ROM_REGION( 0x0220, "proms", 0 )
#ROM_LOAD( "c-1",          0x0000, 0x0020, CRC(e488e831) SHA1(6264741f7091c614093ae1ea4f6ead3d0cef83d3) )    /* palette */
#ROM_LOAD( "c-2",          0x0020, 0x0100, CRC(698a3ba0) SHA1(3c1a6cb881ef74647c651462a27d812234408e45) )    /* sprite lookup table */
#ROM_LOAD( "c-3",          0x0120, 0x0100, CRC(efc4e408) SHA1(f0796426cf324791853aa2ae6d0c3d1f8108d5c2) )    /* char lookup table */
#ROM_END






