palette01:
'------ ymcy.m.c
'------ bgrb.g.r
asm


  defb %11111111
  defb %11101111
  defb %11111110
  defb %11101110
  defb %11111011
  defb %11101011
  defb %11111010
  defb %11101010

  defb %11101010
  defb %01111111
  defb %11011111
  defb %01011111
  defb %10111111
  defb %00111111
  defb %10011111
  defb %00011111

  defb %00011111
  defb %01101111
  defb %11011110
  defb %01001110
  defb %10111011
  defb %00101011
  defb %10011010
  defb %00001010

  defb %00001010
  defb %11111111
  defb %11101010
  defb %11101010
  defb %00011111
  defb %00011111
  defb %00001010
  defb %00001010


  defb $00,$11,$22,$33,$44,$55,$66,$77
  defb $88,$99,$AA,$BB,$CC,$DD,$EE,$FF
  defb $00,$11,$22,$33,$44,$55,$66,$77
  defb $88,$99,$AA,$BB,$CC,$DD,$EE,$FF

  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF

  defb 0,0,0,0,0,0,0,0

  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF

  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF


  ;-'''''''''''''''''


  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF






;  org $0000

  defb %11010100 ;- 0x00
  defb %10010000 ;- 0x04
  defb %01100101 ;- 0x08
  defb %10110001 ;- 0x0C
  defb %01000100 ;- 0x10
  defb %00100001 ;- 0x14
  defb %11010100 ;- 0x18
  defb %11010100 ;- 0x1C

  defb %10010000 ;- 0x01
  defb %10010000 ;- 0x05
  defb %10010000 ;- 0x09
  defb %10010000 ;- 0x0D
  defb %10010000 ;- 0x21
  defb %10010000 ;- 0x25
  defb %10010000 ;- 0x29
  defb %10010000 ;- 0x2D

  defb %11010000 ;- 0x01
  defb %11010000 ;- 0x05
  defb %11010000 ;- 0x09
  defb %11010000 ;- 0x0D
  defb %11010000 ;- 0x21
  defb %11010000 ;- 0x25
  defb %11010000 ;- 0x29
  defb %11010000 ;- 0x2D

  defb %10010100 ;- 0x01
  defb %10010100 ;- 0x05
  defb %10010100 ;- 0x09
  defb %10010100 ;- 0x0D
  defb %10010100 ;- 0x21
  defb %10010100 ;- 0x25
  defb %10010100 ;- 0x29
  defb %10010100 ;- 0x2D


;  org $0020
;  defb $00,$01,$02,$03,$04,$05,$06,$07
;  defb $08,$09,$0A,$0B,$0C,$0D,$0E,$0F
;  defb $10,$11,$12,$13,$14,$15,$16,$17
;  defb $18,$19,$1A,$1B,$1C,$1D,$1E,$1F
;  org $C000



  defb %00000000
  defb %11111111
  defb %00000000
  defb %11111111



  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E
  defb %01001001 ;- #303745
  defb %10110001 ;- #2FD4B0
  defb %00101110 ;- #D4BE2F
  defb %11111111 ;- #E9F7CB
  defb %01010000 ;- #005E46
  defb %00011000 ;- #00713D
  defb %01110100 ;- #89C541
  defb %00110110 ;- #CEDD2A
  defb %00000111 ;- #F00000
  defb %00011111 ;- #F07800
  defb %00101111 ;- #F0A800
  defb %00110100 ;- #90C030
  defb %01010010 ;- #5A434D
  defb %00010111 ;- #FE4F3E
  defb %00101111 ;- #FBAF26
  defb %01110111 ;- #FAC14E

  end asm
