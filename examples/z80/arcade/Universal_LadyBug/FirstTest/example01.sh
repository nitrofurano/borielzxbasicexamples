rm ladybug.zip
rm example01.asm
rm ladybug/*
rmdir ladybug
mkdir ladybug






dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin






#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=ladybug/l1.c4
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=ladybug/l2.d4
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=ladybug/l3.e4
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=ladybug/l4.h4
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=ladybug/l5.j4
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=ladybug/l6.k4

#ROM_START( ladybug )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* Located on the UNIVERSAL 8106-A2 CPU PCB */
#ROM_LOAD( "l1.c4", 0x0000, 0x1000, CRC(d09e0adb) SHA1(ddc1f849cbcefb64b70a26c2a4c993f0516af814) ) /* PCB silkscreened ROM1 */
#ROM_LOAD( "l2.d4", 0x1000, 0x1000, CRC(88bc4a0a) SHA1(193c9f90b7550020c0923cb158dff7d5faa53bc6) ) /* PCB silkscreened ROM2 */
#ROM_LOAD( "l3.e4", 0x2000, 0x1000, CRC(53e9efce) SHA1(1960e9cd896b6a65197aefc3f10348103552b598) ) /* PCB silkscreened ROM3 */
#ROM_LOAD( "l4.h4", 0x3000, 0x1000, CRC(ffc424d7) SHA1(2a4b9533e61e265bdd38c126add8c26d5bc048d5) ) /* PCB silkscreened ROM4 */
#ROM_LOAD( "l5.j4", 0x4000, 0x1000, CRC(ad6af809) SHA1(276275d56c725b9d90eeb44c317ceb06bac27ae7) ) /* PCB silkscreened ROM5 */
#ROM_LOAD( "l6.k4", 0x5000, 0x1000, CRC(cf1acca4) SHA1(c05de7de4bd05d5c2af6aa752e057a9286f3effc) ) /* PCB silkscreened ROM6 */







zxb.py library/b2r1f1_charmapmrdo01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f1_charmapmrdo01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin
xxd -p -c 2 -s 0 -l $((0x8000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > ladybug/l9.f7
xxd -p -c 2 -s 1 -l $((0x8000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > ladybug/l0.h7
xxd -p -c 2 -s 0 -l $((0x8000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > ladybug/l8.l7
xxd -p -c 2 -s 1 -l $((0x8000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > ladybug/l7.m7
rm b2r1f1_charmapmrdo01.bin b2r1f1_charmapmrdo01b.bin

#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=ladybug/l9.f7
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=ladybug/l0.h7
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=ladybug/l8.l7
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=ladybug/l7.m7
#ROM_REGION( 0x2000, "gfx1", 0 ) /* Located on the UNIVERSAL 8106-B video PCB */
#ROM_LOAD( "l9.f7", 0x0000, 0x1000, CRC(77b1da1e) SHA1(58cb82417396a3d96acfc864f091b1a5988f228d) )
#ROM_LOAD( "l0.h7", 0x1000, 0x1000, CRC(aa82e00b) SHA1(83a5b745e58844b6dd7d05dfe9dbb5959aaf5c40) )
#ROM_REGION( 0x2000, "gfx2", 0 ) /* Located on the UNIVERSAL 8106-A2 CPU PCB */
#ROM_LOAD( "l8.l7", 0x0000, 0x1000, CRC(8b99910b) SHA1(0bc812cf872f04eacedb50feed53f1aa8a1f24b9) )
#ROM_LOAD( "l7.m7", 0x1000, 0x1000, CRC(86a5b448) SHA1(f8585a6fcf921e3e21f112dd2de474cb53cef290) )










zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=ladybug/10-2.k1
dd ibs=1 count=$((0x0020)) skip=$((0x0020)) if=palette01b.bin of=ladybug/10-1.f4
dd ibs=1 count=$((0x0020)) skip=$((0x0040)) if=palette01b.bin of=ladybug/10-3.c4
rm palette01.bin palette01b.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=ladybug/ladybug.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=ladybug/ladybug.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=ladybug/ladybug.11j
#rm clut01b.bin

#dd bs=256 count=1 if=/dev/urandom of=ladybug/10-2.k1 #0x00..0x1F
#dd bs=256 count=1 if=/dev/urandom of=ladybug/10-1.f4 #???
#dd bs=256 count=1 if=/dev/urandom of=ladybug/10-3.c4 #???

#touch ladybug/10-2.k1
#touch ladybug/10-1.f4
#touch ladybug/10-3.c4



#ROM_REGION( 0x0060, "proms", 0 ) /* BPROMs are 82s123 & located on the UNIVERSAL 8106-B video PCB */
#ROM_LOAD( "10-2.k1", 0x0000, 0x0020, CRC(df091e52) SHA1(4d7fea6d9ab31e5f280b1dc198a325f00c3826ef) ) /* palette */
#ROM_LOAD( "10-1.f4", 0x0020, 0x0020, CRC(40640d8f) SHA1(85d13a9b78c47174cff7c869f52b30263bae575e) ) /* sprite color lookup table */
#ROM_LOAD( "10-3.c4", 0x0040, 0x0020, CRC(27fa3a50) SHA1(7cf59b7a37c156640d6ea91554d1c4276c1780e0) ) /* ?? */
#ROM_END






rm smsboot.bin dummy32k.bin example01.bin

zip -r ladybug ladybug
rm ladybug/*
rmdir ladybug

mame -w -video soft -resolution0 512x512 -rp ./ ladybug






#ROM_START( ladybug )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* Located on the UNIVERSAL 8106-A2 CPU PCB */
#ROM_LOAD( "l1.c4", 0x0000, 0x1000, CRC(d09e0adb) SHA1(ddc1f849cbcefb64b70a26c2a4c993f0516af814) ) /* PCB silkscreened ROM1 */
#ROM_LOAD( "l2.d4", 0x1000, 0x1000, CRC(88bc4a0a) SHA1(193c9f90b7550020c0923cb158dff7d5faa53bc6) ) /* PCB silkscreened ROM2 */
#ROM_LOAD( "l3.e4", 0x2000, 0x1000, CRC(53e9efce) SHA1(1960e9cd896b6a65197aefc3f10348103552b598) ) /* PCB silkscreened ROM3 */
#ROM_LOAD( "l4.h4", 0x3000, 0x1000, CRC(ffc424d7) SHA1(2a4b9533e61e265bdd38c126add8c26d5bc048d5) ) /* PCB silkscreened ROM4 */
#ROM_LOAD( "l5.j4", 0x4000, 0x1000, CRC(ad6af809) SHA1(276275d56c725b9d90eeb44c317ceb06bac27ae7) ) /* PCB silkscreened ROM5 */
#ROM_LOAD( "l6.k4", 0x5000, 0x1000, CRC(cf1acca4) SHA1(c05de7de4bd05d5c2af6aa752e057a9286f3effc) ) /* PCB silkscreened ROM6 */

#ROM_REGION( 0x2000, "gfx1", 0 ) /* Located on the UNIVERSAL 8106-B video PCB */
#ROM_LOAD( "l9.f7", 0x0000, 0x1000, CRC(77b1da1e) SHA1(58cb82417396a3d96acfc864f091b1a5988f228d) )
#ROM_LOAD( "l0.h7", 0x1000, 0x1000, CRC(aa82e00b) SHA1(83a5b745e58844b6dd7d05dfe9dbb5959aaf5c40) )
#ROM_REGION( 0x2000, "gfx2", 0 ) /* Located on the UNIVERSAL 8106-A2 CPU PCB */
#ROM_LOAD( "l8.l7", 0x0000, 0x1000, CRC(8b99910b) SHA1(0bc812cf872f04eacedb50feed53f1aa8a1f24b9) )
#ROM_LOAD( "l7.m7", 0x1000, 0x1000, CRC(86a5b448) SHA1(f8585a6fcf921e3e21f112dd2de474cb53cef290) )

#ROM_REGION( 0x0060, "proms", 0 ) /* BPROMs are 82s123 & located on the UNIVERSAL 8106-B video PCB */
#ROM_LOAD( "10-2.k1", 0x0000, 0x0020, CRC(df091e52) SHA1(4d7fea6d9ab31e5f280b1dc198a325f00c3826ef) ) /* palette */
#ROM_LOAD( "10-1.f4", 0x0020, 0x0020, CRC(40640d8f) SHA1(85d13a9b78c47174cff7c869f52b30263bae575e) ) /* sprite color lookup table */
#ROM_LOAD( "10-3.c4", 0x0040, 0x0020, CRC(27fa3a50) SHA1(7cf59b7a37c156640d6ea91554d1c4276c1780e0) ) /* ?? */
#ROM_END









