	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$8FFF
#line 1
	ld hl, 0
	push hl
	ld a, 16
	push af
	ld a, 240
	push af
	call _wyvernf0scroll
	ld hl, 0
	ld (_seed), hl
	ld hl, 55296
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 56319
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 3204
	push hl
	ld hl, 16
	push hl
	call _wyvernf0palettergb
	ld hl, 0
	ld (_ey0), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ex0), hl
	jp __LABEL10
__LABEL13:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, 0
	push hl
	ld a, 8
	push af
	ld hl, (_seed)
	ld a, l
	push af
	ld hl, (_ey0)
	push hl
	ld hl, (_ex0)
	push hl
	call _wyvernf0putchar
__LABEL14:
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL10:
	ld hl, 31
	ld de, (_ex0)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL5:
	ld hl, 31
	ld de, (_ey0)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_ey0), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ex0), hl
	jp __LABEL20
__LABEL23:
	ld hl, 0
	push hl
	ld a, 16
	push af
	ld hl, (_ey0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ex0)
	add hl, de
	ld a, l
	push af
	ld hl, (_ey0)
	ld de, 15
	add hl, de
	push hl
	ld hl, (_ex0)
	ld de, 11
	add hl, de
	push hl
	call _wyvernf0putchar
__LABEL24:
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL20:
	ld hl, 15
	ld de, (_ex0)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL15:
	ld hl, 15
	ld de, (_ey0)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 8
	ld (_ex0), hl
	ld hl, 8
	ld (_ey0), hl
	ld hl, 0
	ld (_eee), hl
__LABEL25:
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54784)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54785)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 4
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54786)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54787)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54788)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld de, 8
	ld hl, (_ee2)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	ld hl, (_ex0)
	add hl, de
	push hl
	ld de, 4
	ld hl, (_ee2)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex0), hl
	ld de, 16
	ld hl, (_ee2)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey0)
	add hl, de
	push hl
	ld de, 32
	ld hl, (_ee2)
	call __BAND16
	ld de, 32
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey0), hl
	ld hl, 0
	push hl
	ld a, 48
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_ey0)
	push hl
	ld hl, (_ex0)
	push hl
	call _wyvernf0putchar
	ld a, (54789)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54790)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld a, (54791)
	ld l, a
	ld h, 0
	ld (_ee2), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 1
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 2
	push hl
	call _wyvernf0putchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee2)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 10
	push hl
	ld hl, 3
	push hl
	call _wyvernf0putchar
	ld hl, 2000
	push hl
	call _smsdelay
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL25
__LABEL26:
__LABEL27:
	jp __LABEL27
__LABEL28:
__LABEL29:
	ld hl, 55296
	ld (_eee), hl
	jp __LABEL31
__LABEL34:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL35:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL31:
	ld hl, 56319
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL34
__LABEL33:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL36
__LABEL39:
	ld hl, (_eee)
	ld de, 273
	call __MUL16_FAST
	push hl
	ld hl, (_eee)
	push hl
	call _wyvernf0palettergb
__LABEL40:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL36:
	ld hl, 15
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL39
__LABEL38:
	ld hl, 3204
	push hl
	ld hl, 16
	push hl
	call _wyvernf0palettergb
	ld hl, 0
	ld (_ey0), hl
	jp __LABEL41
__LABEL44:
	ld hl, 0
	ld (_ex0), hl
	jp __LABEL46
__LABEL49:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, 0
	push hl
	ld a, 2
	push af
	ld hl, (_seed)
	ld a, l
	push af
	ld hl, (_ey0)
	push hl
	ld hl, (_ex0)
	push hl
	call _wyvernf0putchar
__LABEL50:
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL46:
	ld hl, 31
	ld de, (_ex0)
	or a
	sbc hl, de
	jp nc, __LABEL49
__LABEL48:
__LABEL45:
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL41:
	ld hl, 31
	ld de, (_ey0)
	or a
	sbc hl, de
	jp nc, __LABEL44
__LABEL43:
	ld hl, 0
	ld (_ey0), hl
	jp __LABEL51
__LABEL54:
	ld hl, 0
	ld (_ex0), hl
	jp __LABEL56
__LABEL59:
	ld hl, 0
	push hl
	ld a, 2
	push af
	ld hl, (_ey0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ex0)
	add hl, de
	ld a, l
	push af
	ld hl, (_ey0)
	ld de, 8
	add hl, de
	push hl
	ld hl, (_ex0)
	ld de, 8
	add hl, de
	push hl
	call _wyvernf0putchar
__LABEL60:
	ld hl, (_ex0)
	inc hl
	ld (_ex0), hl
__LABEL56:
	ld hl, 15
	ld de, (_ex0)
	or a
	sbc hl, de
	jp nc, __LABEL59
__LABEL58:
__LABEL55:
	ld hl, (_ey0)
	inc hl
	ld (_ey0), hl
__LABEL51:
	ld hl, 15
	ld de, (_ey0)
	or a
	sbc hl, de
	jp nc, __LABEL54
__LABEL53:
	ld hl, 51200
	ld (_eee), hl
	jp __LABEL61
__LABEL64:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL65:
	ld hl, (_eee)
	inc hl
	inc hl
	ld (_eee), hl
__LABEL61:
	ld hl, 53247
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL64
__LABEL63:
	ld hl, 51201
	ld (_eee), hl
	jp __LABEL66
__LABEL69:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL70:
	ld hl, (_eee)
	inc hl
	inc hl
	ld (_eee), hl
__LABEL66:
	ld hl, 53247
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL69
__LABEL68:
	ld hl, 49152
	ld (_eee), hl
	jp __LABEL71
__LABEL74:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL75:
	ld hl, (_eee)
	inc hl
	inc hl
	ld (_eee), hl
__LABEL71:
	ld hl, 51199
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL74
__LABEL73:
	ld hl, 49153
	ld (_eee), hl
	jp __LABEL76
__LABEL79:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL80:
	ld hl, (_eee)
	inc hl
	inc hl
	ld (_eee), hl
__LABEL76:
	ld hl, 51199
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL79
__LABEL78:
	ld hl, 54528
	ld (_eee), hl
	jp __LABEL81
__LABEL84:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL85:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL81:
	ld hl, 54783
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL84
__LABEL83:
	jp __LABEL29
__LABEL30:
__LABEL86:
	jp __LABEL86
__LABEL87:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_smsdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
smsdelayloop:
		dec bc
		ld a,b
		or c
		jp nz,smsdelayloop
#line 8
_smsdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_wyvernf0putchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	ld de, 2048
	call __MUL16_FAST
	ld de, 49152
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 64
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	ld de, 2048
	call __MUL16_FAST
	ld de, 49153
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 64
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_wyvernf0putchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_wyvernf0palette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 0
	ld bc, 13
	push bc
	ld bc, 32768
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	ld (hl), e
	inc hl
	ld (hl), d
_wyvernf0palette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_wyvernf0palettergb:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 55297
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 55296
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 256
	call __DIVU16
	ld de, 0
	ld a, l
	pop hl
	ld (hl), a
_wyvernf0palettergb__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_wyvernf0scroll:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	add hl, hl
	ld de, 54017
	add hl, de
	push hl
	ld a, (ix+5)
	pop hl
	ld (hl), a
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld de, 1
	pop hl
	call __BAND16
	add hl, hl
	ld de, 54016
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
_wyvernf0scroll__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1473 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1474 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1475 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1476 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1477 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1478 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_eee EQU 32784
	_seed EQU 32786
	_ee2 EQU 32788
	_ee3 EQU 32790
	_ee4 EQU 32792
	_ee5 EQU 32794
	_ex0 EQU 32796
	_ey0 EQU 32798
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
