rm example01.asm
rm wyvernf0.zip
rm wyvernf0/*
rmdir wyvernf0
mkdir wyvernf0

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=wyvernf0/a39_01-1.ic37
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=wyvernf0/a39_02-1.ic36
rm example01.bin

#rombank
dd bs=$((0x4000)) count=1 if=/dev/zero of=wyvernf0/a39_03.ic35
dd bs=$((0x4000)) count=1 if=/dev/zero of=wyvernf0/a39_04.ic34
dd bs=$((0x4000)) count=1 if=/dev/zero of=wyvernf0/a39_05.ic33
dd bs=$((0x4000)) count=1 if=/dev/zero of=wyvernf0/a39_06.ic32

#audiocpu
dd bs=$((0x4000)) count=1 if=/dev/zero of=wyvernf0/a39_16.ic26


#gfx is alternatedly by bitdepth -legendofkageseemsfine, needs to rotate
#gfx
zxb.py library/charmapwyvern01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmapwyvern01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmapwyvern01.bin of=charmapwyvern01b.bin
rm charmapwyvern01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_14.ic99
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_14.ic73
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_13.ic100
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_12.ic74

xxd -p -c 4 -s 0 -l $((0x8000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_14.ic99
xxd -p -c 4 -s 1 -l $((0x8000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_14.ic73
xxd -p -c 4 -s 2 -l $((0x8000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_13.ic100
xxd -p -c 4 -s 3 -l $((0x8000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_12.ic74


#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_11.ic99
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_10.ic78
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_09.ic96
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmapwyvern01b.bin of=wyvernf0/a39_08.ic75

xxd -p -c 4 -s 0 -l $((0x10000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_11.ic99
xxd -p -c 4 -s 1 -l $((0x10000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_10.ic78
xxd -p -c 4 -s 2 -l $((0x10000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_09.ic96
xxd -p -c 4 -s 3 -l $((0x10000)) charmapwyvern01b.bin | cut -b 1-2 | xxd -r -p > wyvernf0/a39_08.ic75


rm charmapwyvern01b.bin






#prom
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=wyvernf0/mb7051.3
rm palette01.bin

#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=wyvernf0/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=wyvernf0/wyvernf0.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=wyvernf0/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=wyvernf0/82s126.3m

rm smsboot.bin dummy64k.bin

#rm wyvernf0.zip
#rm wyvernf0/*
#rmdir wyvernf0

zip -r wyvernf0 wyvernf0
rm wyvernf0/*
rmdir wyvernf0


mame -w -video soft -resolution0 512x512 -sound none -rp ./ wyvernf0

