rm mrjong.zip
rm example01.asm
rm mrjong/*
rmdir mrjong
mkdir mrjong





dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin





#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=mrjong/mj00
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=mrjong/mj01
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=mrjong/mj02
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=mrjong/mj03

#ROM_START( mrjong )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* code */
#ROM_LOAD( "mj00", 0x0000, 0x2000, CRC(d211aed3) SHA1(01f252ca1d2399146fa3ed44cb2daa1d5925cae5) )
#ROM_LOAD( "mj01", 0x2000, 0x2000, CRC(49a9ca7e) SHA1(fc5279ba782da2c8288042bd17282366fcd788cc) )
#ROM_LOAD( "mj02", 0x4000, 0x2000, CRC(4b50ae6a) SHA1(6fa6bae926c5e4cc154f5f1a6dc7bb7ef5bb484a) )
#ROM_LOAD( "mj03", 0x6000, 0x2000, CRC(2c375a17) SHA1(9719485cdca535771b498a37d57734463858f2cd) )





#gfx

zxb.py library/b2r1f1_charmapmrdo01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f1_charmapmrdo01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=mrjong/mj21
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=mrjong/mj20

dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrjong/mj21
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > mrjong/mj20
rm b2r1f1_charmapmrdo01.bin b2r1f1_charmapmrdo01b.bin

#ROM_REGION( 0x2000, "gfx1", 0 ) /* gfx */
#ROM_LOAD( "mj21", 0x0000, 0x1000, CRC(1ea99dab) SHA1(21a296d394e5cac0c7cb2ea8efaeeeee976ac4b5) )
#ROM_LOAD( "mj20", 0x1000, 0x1000, CRC(7eb1d381) SHA1(fa13700f132c03d2d2cee65abf24024db656aff7) )





#proms

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=mrjong/mj61
dd ibs=1 count=$((0x0100)) skip=$((0x0030)) if=palette01.bin of=mrjong/mj60
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=mrjong/mrjong.3j

#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=mrjong/mrjong.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=mrjong/mrjong.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=mrjong/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=mrjong/82s126.3m

#dd bs=256 count=1 if=/dev/urandom of=mrjong/mj61
#dd bs=256 count=1 if=/dev/urandom of=mrjong/mj60

#ROM_REGION( 0x0120, "proms", 0 )    /* color */
#ROM_LOAD( "mj61", 0x0000, 0x0020, CRC(a85e9b27) SHA1(55df208b771a98fcf6c2c19ffdf973891ebcabd1) )
#ROM_LOAD( "mj60", 0x0020, 0x0100, CRC(dd2b304f) SHA1(d7320521e83ddf269a9fc0c91f0e0e61428b187c) )
#ROM_END#








rm smsboot.bin dummy32k.bin example01.bin

zip -r mrjong mrjong
rm mrjong/*
rmdir mrjong

mame -w -video soft -resolution0 512x512 -rp ./ mrjong






#ROM_START( mrjong )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* code */
#ROM_LOAD( "mj00", 0x0000, 0x2000, CRC(d211aed3) SHA1(01f252ca1d2399146fa3ed44cb2daa1d5925cae5) )
#ROM_LOAD( "mj01", 0x2000, 0x2000, CRC(49a9ca7e) SHA1(fc5279ba782da2c8288042bd17282366fcd788cc) )
#ROM_LOAD( "mj02", 0x4000, 0x2000, CRC(4b50ae6a) SHA1(6fa6bae926c5e4cc154f5f1a6dc7bb7ef5bb484a) )
#ROM_LOAD( "mj03", 0x6000, 0x2000, CRC(2c375a17) SHA1(9719485cdca535771b498a37d57734463858f2cd) )

#ROM_REGION( 0x2000, "gfx1", 0 ) /* gfx */
#ROM_LOAD( "mj21", 0x0000, 0x1000, CRC(1ea99dab) SHA1(21a296d394e5cac0c7cb2ea8efaeeeee976ac4b5) )
#ROM_LOAD( "mj20", 0x1000, 0x1000, CRC(7eb1d381) SHA1(fa13700f132c03d2d2cee65abf24024db656aff7) )

#ROM_REGION( 0x0120, "proms", 0 )    /* color */
#ROM_LOAD( "mj61", 0x0000, 0x0020, CRC(a85e9b27) SHA1(55df208b771a98fcf6c2c19ffdf973891ebcabd1) )
#ROM_LOAD( "mj60", 0x0020, 0x0100, CRC(dd2b304f) SHA1(d7320521e83ddf269a9fc0c91f0e0e61428b187c) )
#ROM_END#





