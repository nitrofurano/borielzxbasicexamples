sub quizosldir79b(tvram as uinteger, tram as uinteger,tlen as uinteger):
'- testar....
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)


    quizosldir79bloop:

    ld a,0
    out($70),a
    ld a,(hl)    ; Get data byte
    ld (de),a    
    inc hl       

    ld a,8
    out($70),a
    ld a,(hl)    ; Get data byte
    ld (de),a    
    inc hl       

    inc de       ;- add 1 to de

    ld a,0
    out($70),a
    ld a,(hl)    ; Get data byte
    ld (de),a    
    inc hl       

    ld a,8
    out($70),a
    ld a,(hl)    ; Get data byte
    ld (de),a    
    inc hl

    ;add de,79    ;- add 79 to de
    ex de,hl
    push de
    ld de,79
    add hl,de
    pop de
    ex de,hl
    
    dec bc
    ld a,b
    or c
    jp nz,quizosldir79bloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
