palette01:
'-----  rrrgggbb
asm
  defb %00100101 ;- #334
  defb %00111010 ;- #2DB
  defb %11010100 ;- #DB2
  defb %00011111 ;- #EFC

  defb %00001001 ;- #054
  defb %00001100 ;- #073
  defb %10011001 ;- #8C4
  defb %11011000 ;- #CD2

  defb %11100000 ;- #F00
  defb %11101100 ;- #F70
  defb %11110100 ;- #FA0
  defb %10011000 ;- #9C3

  defb %01001001 ;- #544
  defb %11101000 ;- #F43
  defb %11110100 ;- #FA2
  defb %11111001 ;- #FC4


  defb %00000000

  defb %11111111

  defb %00000001
  defb %00000010
  defb %00000100
  defb %00001000
  defb %00010000
  defb %00100000
  defb %01000000
  defb %10000000


  defb %00100101 ;- #334
  defb %00111010 ;- #2DB
  defb %11010100 ;- #DB2
  defb %00011111 ;- #EFC

  defb %00001001 ;- #054
  defb %00001100 ;- #073
  defb %10011001 ;- #8C4
  defb %11011000 ;- #CD2

  defb %11100000 ;- #F00
  defb %11101100 ;- #F70
  defb %11110100 ;- #FA0
  defb %10011000 ;- #9C3

  defb %01001001 ;- #544
  defb %11101000 ;- #F43
  defb %11110100 ;- #FA2
  defb %11111001 ;- #FC4

  end asm


