rm example01.asm
rm 10yard.zip
rm 10yard/*
rmdir 10yard
mkdir 10yard

#cpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=0x0069
zxb.py example01.bas --heap-size=128 --org=0x0069
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=10yard/yf-a-3p-b
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=10yard/yf-a-3n-b
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=10yard/yf-a-3m-b
rm example01.bin



#sound
dd bs=$((0x2000)) count=1 if=/dev/zero of=10yard/yf-s.3b
dd bs=$((0x2000)) count=1 if=/dev/zero of=10yard/yf-s.1b
dd bs=$((0x2000)) count=1 if=/dev/zero of=10yard/yf-s.3a
dd bs=$((0x2000)) count=1 if=/dev/zero of=10yard/yf-s.1a




#gfx
zxb.py library/charmap01.zxi --org=0x0000
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin

zxb.py library/b3r0f0_charmapiremm62v01.zxi --org=0x0000
cat dummy64k.bin >> b3r0f0_charmapiremm62v01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=b3r0f0_charmapiremm62v01b.bin

#gfx1
xxd -p -c 3 -s 0 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-a.3e
xxd -p -c 3 -s 1 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-a.3d
xxd -p -c 3 -s 2 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-a.3c

#gfx2
xxd -p -c 3 -s 0 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5b
xxd -p -c 3 -s 1 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5c
xxd -p -c 3 -s 2 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5f
xxd -p -c 3 -s 0 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5e
xxd -p -c 3 -s 1 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5j
xxd -p -c 3 -s 2 -l $((0x6000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > 10yard/yf-b.5k





rm charmap01.bin charmap01b.bin


#zxb.py library/spritemap01.zxi --org=0x0000
#cat dummy64k.bin >> spritemap01b.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=spritemap01.bin of=spritemap01b.bin
#xxd -p -c 2 -s 0 -l 8192 spritemap01b.bin | cut -b 1-2 | xxd -r -p > 10yard/mpb-1.3n
#xxd -p -c 2 -s 1 -l 8192 spritemap01b.bin | cut -b 1-2 | xxd -r -p > 10yard/mpb-2.3m
#rm spritemap01.bin spritemap01b.bin




#zxb.py library/palette01.zxi --org=0x0000
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=16384 skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0000)) if=palette01b.bin of=10yard/mpc-4.2a #ok
#dd ibs=1 count=$((0x0020)) skip=$((0x0200)) if=palette01b.bin of=10yard/mpc-1.1f #(sprite colors?)
#dd ibs=1 count=$((0x0100)) skip=$((0x0220)) if=palette01b.bin of=10yard/mpc-2.2h #(sprite color sets?)
#dd ibs=1 count=$((0x0020)) skip=$((0x0320)) if=palette01b.bin of=10yard/mpc-3.1m #?
#rm palette01.bin palette01b.bin

dd bs=$((0x100)) count=1 if=/dev/urandom of=10yard/yard.1c
dd bs=$((0x100)) count=1 if=/dev/urandom of=10yard/yard.1d
dd bs=$((0x20)) count=1 if=/dev/urandom of=10yard/yard.1f
dd bs=$((0x100)) count=1 if=/dev/urandom of=10yard/yard.2h
dd bs=$((0x100)) count=1 if=/dev/urandom of=10yard/yard.2n
dd bs=$((0x100)) count=1 if=/dev/urandom of=10yard/yard.2m





zip -r 10yard 10yard
rm 10yard/*
rmdir 10yard
rm smsboot.bin dummy64k.bin
rm b3r0f0_charmapiremm62v01.bin b3r0f0_charmapiremm62v01b.bin

mame -w -sound none -video soft -resolution0 512x512 -rp ./ 10yard





#ROM_START( 10yard )
#// Dumped from an original Irem M52 board. Serial no. 307761/License Seal 09461.
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "yf-a-3p-b",    0x0000, 0x2000, CRC(2e205ec2) SHA1(fcfa08f45423b35f2c99d4e6b5474ab1b3a84fec) )
#ROM_LOAD( "yf-a-3n-b",    0x2000, 0x2000, CRC(82fcd980) SHA1(7846705b29961cb95ee1571ee7e16baceea522d4) )
#ROM_LOAD( "yf-a-3m-b",    0x4000, 0x2000, CRC(a8d5c311) SHA1(28edb5cfd943a2262d7e37ef9a7245f7017cbc51) )

#ROM_REGION( 0x10000, "iremsound", 0 )
#ROM_LOAD( "yf-s.3b",      0x8000, 0x2000, CRC(0392a60c) SHA1(68030504eafc58db250099edd3c3323bdb9eff6b) )
#ROM_LOAD( "yf-s.1b",      0xa000, 0x2000, CRC(6588f41a) SHA1(209305efc68171886427216b9a0b37333f40daa8) )
#ROM_LOAD( "yf-s.3a",      0xc000, 0x2000, CRC(bd054e44) SHA1(f10c32c70d60680229fc0891d0e1308015fa69d6) )
#ROM_LOAD( "yf-s.1a",      0xe000, 0x2000, CRC(2490d4c3) SHA1(e4da7b01e8ad075b7e3c8beb6668faff72db9aa2) )

#ROM_REGION( 0x06000, "gfx1", 0 )
#ROM_LOAD( "yf-a.3e",      0x00000, 0x2000, CRC(77e9e9cc) SHA1(90b0226fc125713dbee2804aeceeb5aa2c8e275e) )   /* chars */
#ROM_LOAD( "yf-a.3d",      0x02000, 0x2000, CRC(854d5ff4) SHA1(9ba09bfabf159facb57faecfe73a6258fa48d152) )
#ROM_LOAD( "yf-a.3c",      0x04000, 0x2000, CRC(0cd8ffad) SHA1(bd1262de3823c34f7394b718477fb5bc58a6e293) )
#ROM_REGION( 0x0c000, "gfx2", 0 )
#ROM_LOAD( "yf-b.5b",      0x00000, 0x2000, CRC(1299ae30) SHA1(07d47f827d8bc78a41011ec02ab64036fb8a7a18) )   /* sprites */
#ROM_LOAD( "yf-b.5c",      0x02000, 0x2000, CRC(8708b888) SHA1(8c4f305a339f23ec8ed40dfd72fac0f62ee65378) )
#ROM_LOAD( "yf-b.5f",      0x04000, 0x2000, CRC(d9bb8ab8) SHA1(1325308b4c85355298fec4aa3e5fec1b4b13ad86) )
#ROM_LOAD( "yf-b.5e",      0x06000, 0x2000, CRC(47077e8d) SHA1(5f78b15fb360e9926ef11841d5d86f2bd9af04d1) )
#ROM_LOAD( "yf-b.5j",      0x08000, 0x2000, CRC(713ef31f) SHA1(b48df9ed4f26fded3c7eaac3a52b580b2dd60477) )
#ROM_LOAD( "yf-b.5k",      0x0a000, 0x2000, CRC(f49651cc) SHA1(5b87d7360bcd5883ec265b2a01a3e02e10a85345) )

#ROM_REGION( 0x0520, "proms", 0 )
#ROM_LOAD( "yard.1c",      0x0000, 0x0100, CRC(08fa5103) SHA1(98af48dafbbaa42f58232bf74ccbf5da41723e71) ) /* chars palette low 4 bits */
#ROM_LOAD( "yard.1d",      0x0100, 0x0100, CRC(7c04994c) SHA1(790bf1616335b9df4943cffcafa48d8e8aee009e) ) /* chars palette high 4 bits */
#ROM_LOAD( "yard.1f",      0x0200, 0x0020, CRC(b8554da5) SHA1(963ca815b5f791b8a7b0937a5d392d5203049eb3) ) /* sprites palette */
#ROM_LOAD( "yard.2h",      0x0220, 0x0100, CRC(e1cdfb06) SHA1(a8cc3456cfc272e3faac80370b2298d8e1f8c2fe) ) /* sprites lookup table */
#ROM_LOAD( "yard.2n",      0x0320, 0x0100, CRC(cd85b646) SHA1(5268db705006058eec308afe474f4df3c15465bb) ) /* radar palette low 4 bits */
#ROM_LOAD( "yard.2m",      0x0420, 0x0100, CRC(45384397) SHA1(e4c662ee81aef63efd8b4a45f85c4a78dc2d419e) ) /* radar palette high 4 bits */
#ROM_END



