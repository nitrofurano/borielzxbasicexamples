asm
  ld sp,$EFFF
  end asm

#include "./library/pacmandelay.bas"
#include "./library/smsrnd.bas"

dim eee as uinteger at $E010
dim seed as uinteger at $E012
dim ee0 as uinteger at $E014
dim ee1 as uinteger at $E016
dim ee2 as uinteger at $E018
dim ee3 as uinteger at $E01A
dim eex as uinteger at $E01C
dim eey as uinteger at $E01E
seed=0

sub m58putchar(txpos as uinteger,typos as uinteger, tchr as uinteger, tatr as uinteger):
  poke $8000+(txpos*2)+(typos*64),tchr
  poke $8001+(txpos*2)+(typos*64),tatr
  end sub

sub m58scroll(txsc as uinteger,tysc as uinteger):
  poke $A000,txsc
  poke $A200,int(txsc/256)
  poke $A400,tysc
  end sub

poke $A800,$FF '-disable scoretable
m58scroll(0,6+240)

for ee0=0 to 27
  for ee1=0 to 31
     ee2=seed:seed=smsrnd(seed)
     ee2=seed:seed=smsrnd(seed)
     m58putchar(ee1,ee0,seed band 7,ee2)
     next:next

for ee0=0 to 15
  for ee1=0 to 15
     m58putchar(ee1+15,ee0+11,ee0*16+ee1,0)
     next:next

eee=0:eex=8:eey=8
do

m58putchar(1,1,48+(int(eee/10000) mod 10),0)
m58putchar(2,1,48+(int(eee/1000) mod 10),0)
m58putchar(3,1,48+(int(eee/100) mod 10),0)
m58putchar(4,1,48+(int(eee/10) mod 10),0)
m58putchar(5,1,48+(eee mod 10),0)

ee0=peek($D000) bxor $FF
m58putchar(3,3,48+(int(ee0/100) mod 10),0)
m58putchar(4,3,48+(int(ee0/10) mod 10),0)
m58putchar(5,3,48+(ee0 mod 10),0)

ee0=peek($D001) bxor $FF
m58putchar(3,4,48+(int(ee0/100) mod 10),0)
m58putchar(4,4,48+(int(ee0/10) mod 10),0)
m58putchar(5,4,48+(ee0 mod 10),0)

eex=eex+((ee0 band 1)/1)-((ee0 band 2)/2)
eey=eey+((ee0 band 4)/4)-((ee0 band 8)/8)

ee0=peek($D002) bxor $FF
m58putchar(3,5,48+(int(ee0/100) mod 10),0)
m58putchar(4,5,48+(int(ee0/10) mod 10),0)
m58putchar(5,5,48+(ee0 mod 10),0)

ee0=peek($D003) bxor $FF
m58putchar(3,6,48+(int(ee0/100) mod 10),0)
m58putchar(4,6,48+(int(ee0/10) mod 10),0)
m58putchar(5,6,48+(ee0 mod 10),0)

ee0=peek($D004) bxor $FF
m58putchar(3,7,48+(int(ee0/100) mod 10),0)
m58putchar(4,7,48+(int(ee0/10) mod 10),0)
m58putchar(5,7,48+(ee0 mod 10),0)


m58putchar(eex,eey,32+(eee mod 64),0)

eee=eee+1
loop


do:loop

for eee=$8000 to $8FFF step 2 '-videoram
  seed=smsrnd(seed)
  poke eee,seed
  next






do
loop



do

for eee=$8000 to $8FFF '-videoram
  seed=smsrnd(seed)
  poke eee,seed
  next

for eee=$C820 to $C87f '-spriteram
  seed=smsrnd(seed)
  poke eee,seed
  next

loop


do:loop

'static ADDRESS_MAP_START( yard_map, AS_PROGRAM, 8, m58_state )
'AM_RANGE(0x0000, 0x5fff) AM_ROM
'AM_RANGE(0x8000, 0x8fff) AM_RAM_WRITE(videoram_w) AM_SHARE("videoram")
'AM_RANGE(0x9000, 0x9fff) AM_WRITE(scroll_panel_w)
'AM_RANGE(0xc820, 0xc87f) AM_RAM AM_SHARE("spriteram")
'AM_RANGE(0xa000, 0xa000) AM_RAM AM_SHARE("scroll_x_low")
'AM_RANGE(0xa200, 0xa200) AM_RAM AM_SHARE("scroll_x_high")
'AM_RANGE(0xa400, 0xa400) AM_RAM AM_SHARE("scroll_y_low")
'AM_RANGE(0xa800, 0xa800) AM_RAM AM_SHARE("score_disable")
'AM_RANGE(0xd000, 0xd000) AM_DEVWRITE("irem_audio", irem_audio_device, cmd_w)
'AM_RANGE(0xd001, 0xd001) AM_WRITE(flipscreen_w)    /* + coin counters */
'AM_RANGE(0xd000, 0xd000) AM_READ_PORT("IN0")
'AM_RANGE(0xd001, 0xd001) AM_READ_PORT("IN1")
'AM_RANGE(0xd002, 0xd002) AM_READ_PORT("IN2")
'AM_RANGE(0xd003, 0xd003) AM_READ_PORT("DSW1")
'AM_RANGE(0xd004, 0xd004) AM_READ_PORT("DSW2")
'AM_RANGE(0xe000, 0xefff) AM_RAM
'ADDRESS_MAP_END

'
'sub m52putsprite(tlay as uinteger, txpos as uinteger,typos as uinteger, tid as uinteger, tclut as uinteger):
'  poke $C803+(tlay*4),txpos
'  poke $C800+(tlay*4),typos bxor 255
'  poke $C801+(tlay*4),tid
'  poke $C802+(tlay*4),tclut
'  end sub
'
'sub m52putcharattr(txpos as uinteger,typos as uinteger, tid as uinteger, tclut as uinteger):
'  poke $8000+txpos+(typos*32),tid
'  poke $8400+txpos+(typos*32),tclut
'  end sub
'
'
'seed=0
'
'for eee=$8000 to $83FF
'  poke eee,1
'  next
'
'for eee=$8400 to $87FF
'  poke eee,0
'  next
'
'for eee=0 to 15
'  poke $8000+2+(32*30)+eee,peek(@text01+eee) '- hello world
'  next
'
'poke $8035,code("A")
'poke $8435,2
'
'for ee2=0 to 15
'  for ee3=0 to 15
'    poke $8000+(ee3+2)+((ee2+2)*32),(ee3)+(ee2*16)
'    next:next
'
'for ee2=0 to 9
'  for ee3=0 to 15
'    m52putcharattr(ee3+2,ee2+19,4,(ee3)+(ee2*16))
'    next:next
'
'expos=112
'eypos=40
'crid=1
'crcl=1
'
'do
'  'for edl=1 to 512:next
'  '- note: mpatrol on mame has no vertical joystick connectors... :S (using the second joystick for that)
'  if (peek($D001)band 1=0) or (peek($D002)band 4=0) then:expos=expos+1:end if
'  if (peek($D001)band 2=0) or (peek($D002)band 8=0) then:expos=expos-1:end if
'  if (peek($D001)band 4=0) or (peek($D002)band 1=0) then:eypos=eypos+1:end if 
'  if (peek($D001)band 8=0) or (peek($D002)band 2=0) then:eypos=eypos-1:end if
'  if peek($D001)band 128=0 then:crid=crid+1:end if
'  if peek($D001)band  32=0 then:crcl=crcl+1:end if
'
'  expos=expos band 255
'  eypos=eypos band 255
'  crid=crid band 255
'  crcl=crcl band 255
'
'  for ee2=0 to 7
'    for ee3=0 to 7
'      m52putsprite((ee2*8)+ee3,expos+(ee3*16),eypos+(ee2*16),crid,crcl)
'      next:next
'
'  m52putcharattr(21,20,48+(expos mod 10),0)
'  m52putcharattr(20,20,48+((int(expos/10)) mod 10),0)
'  m52putcharattr(19,20,48+((int(expos/100)) mod 10),0)
'
'  m52putcharattr(21,21,48+(eypos mod 10),0)
'  m52putcharattr(20,21,48+((int(eypos/10)) mod 10),0)
'  m52putcharattr(19,21,48+((int(eypos/100)) mod 10),0)
'
'  m52putcharattr(21,22,48+(crid mod 10),0)
'  m52putcharattr(20,22,48+((int(crid/10)) mod 10),0)
'  m52putcharattr(19,22,48+((int(crid/100)) mod 10),0)
'
'  m52putcharattr(21,23,48+(crcl mod 10),0)
'  m52putcharattr(20,23,48+((int(crcl/10)) mod 10),0)
'  m52putcharattr(19,23,48+((int(crcl/100)) mod 10),0)
'
'  eee=peek($D000) bxor 255
'  m52putcharattr(21,25,48+(eee mod 10),0)
'  m52putcharattr(20,25,48+((int(eee/10)) mod 10),0)
'  m52putcharattr(19,25,48+((int(eee/100)) mod 10),0)
'
'  eee=peek($D001) bxor 255
'  m52putcharattr(21,26,48+(eee mod 10),0)
'  m52putcharattr(20,26,48+((int(eee/10)) mod 10),0)
'  m52putcharattr(19,26,48+((int(eee/100)) mod 10),0)
'
'  eee=peek($D002) bxor 255
'  m52putcharattr(21,27,48+(eee mod 10),0)
'  m52putcharattr(20,27,48+((int(eee/10)) mod 10),0)
'  m52putcharattr(19,27,48+((int(eee/100)) mod 10),0)
'
'  eee=peek($D003) bxor 255
'  m52putcharattr(21,28,48+(eee mod 10),0)
'  m52putcharattr(20,28,48+((int(eee/10)) mod 10),0)
'  m52putcharattr(19,28,48+((int(eee/100)) mod 10),0)
'
'  loop
'
''-------------------------------------------------------------------------------
'
'text01:
'asm
'  defb "HELLO WORLD!!!!"
'  end asm
'
