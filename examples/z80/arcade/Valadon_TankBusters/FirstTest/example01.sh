rm tankbust.zip
rm example01.asm
rm tankbust/*
rmdir tankbust
mkdir tankbust



dd bs=$((0x4000)) count=1 if=/dev/zero of=dummy32k.bin



#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=tankbust/a-s4-6.bin
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=tankbust/a-s7-9.bin

dd bs=$((0x2000)) count=1 if=/dev/zero of=tankbust/a-s5_7.bin
dd bs=$((0x2000)) count=1 if=/dev/zero of=tankbust/a-s6-8.bin
dd bs=$((0x4000)) count=1 if=/dev/zero of=tankbust/a-s8-10.bin


#- ROM_START( tankbust )
#- ROM_REGION( 0x1c000, "maincpu", 0 )
#- ROM_LOAD( "a-s4-6.bin",     0x00000, 0x4000, CRC(8ebe7317) SHA1(bc45d530ad6335312c9c3efdcedf7acd2cdeeb55) )
#- ROM_LOAD( "a-s7-9.bin",     0x04000, 0x2000, CRC(047aee33) SHA1(62ee776c403b228e065baa9218f32597951ca935) )
#- ROM_LOAD( "a-s5_7.bin",     0x12000, 0x2000, CRC(dd4800ca) SHA1(73a6caa029c27fb45217f9372d9541c6fe206f08) ) /* banked at 0x6000-0x9fff */
#- ROM_CONTINUE(                   0x10000, 0x2000)
#- ROM_LOAD( "a-s6-8.bin",     0x16000, 0x2000, CRC(f8801238) SHA1(fd3abe18542660a8c31dc316012a99d48c9bb5aa) ) /* banked at 0x6000-0x9fff */
#- ROM_CONTINUE(                   0x14000, 0x2000)
#- // ROM_LOAD( "a-s5_7.bin",     0x10000, 0x4000, CRC(dd4800ca) SHA1(73a6caa029c27fb45217f9372d9541c6fe206f08) ) /* banked at 0x6000-0x9fff */
#- // ROM_LOAD( "a-s6-8.bin",     0x14000, 0x4000, CRC(f8801238) SHA1(fd3abe18542660a8c31dc316012a99d48c9bb5aa) ) /* banked at 0x6000-0x9fff */
#- ROM_LOAD( "a-s8-10.bin",    0x18000, 0x4000, CRC(9e826faa) SHA1(6a252428c69133d3e9d7a9938140d5ae37fb0c7d) ) /* banked at 0xa000-0xbfff */







#sub
dd bs=$((0x2000)) count=1 if=/dev/zero of=tankbust/a-b3-1.bin


#- ROM_REGION( 0x10000, "sub", 0 )
#- ROM_LOAD( "a-b3-1.bin",     0x0000, 0x2000, CRC(b0f56102) SHA1(4f427c3bd6131b7cba42a0e24a69bd1b6a1b0a3c) )








#gfx

#gfx1 - 0/2
zxb.py library/b4r1f1_charmaptankbusterssprites01.zxi --org=$((0x0000)) #- 1/2 trocar por 3bp -- 0/2 trocar por 4bp
cat dummy32k.bin >> b4r1f1_charmaptankbusterssprites01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b4r1f1_charmaptankbusterssprites01.bin of=b4r1f1_charmaptankbusterssprites01b.bin
xxd -p -c 4 -s 0 -l $((0x8000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > tankbust/a-d5-2.bin
xxd -p -c 4 -s 1 -l $((0x8000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > tankbust/a-d6-3.bin
xxd -p -c 4 -s 2 -l $((0x8000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > tankbust/a-d7-4.bin
xxd -p -c 4 -s 3 -l $((0x8000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > tankbust/a-d8-5.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/a-d5-2.bin #- sprite? 0/2
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/a-d6-3.bin #- sprite? 0/2
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/a-d7-4.bin #- sprite? 0/2
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/a-d8-5.bin #- sprite? 0/2
rm b4r1f1_charmaptankbusterssprites01.bin b4r1f1_charmaptankbusterssprites01b.bin

#gfx2 - 1/2
zxb.py library/b3r1f1_charmaptankbusters01.zxi --org=$((0x0000)) #- 1/2 trocar por 3bp -- 0/2 trocar por 4bp
cat dummy32k.bin >> b3r1f1_charmaptankbusters01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r1f1_charmaptankbusters01.bin of=b3r1f1_charmaptankbusters01b.bin
xxd -p -c 3 -s 0 -l $((0x6000)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > tankbust/b-m4-11.bin
xxd -p -c 3 -s 1 -l $((0x6000)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > tankbust/b-m5-12.bin
xxd -p -c 3 -s 2 -l $((0x6000)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > tankbust/b-m6-13.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/b-m4-11.bin #- 1/2
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/b-m5-12.bin #- 1/2
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=tankbust/b-m6-13.bin #- 1/2
rm b3r1f1_charmaptankbusters01.bin b3r1f1_charmaptankbusters01b.bin

#gfx3 - 2/2
zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000)) #- 2/2
cat dummy32k.bin >> b1r1f0_charmapkamikaze01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=tankbust/b-r3-14.bin #- 2/2

rm b1r1f1_charmapbeaminvader01.bin b1r1f1_charmapbeaminvader01.bin b4r1f1_charmaptankbusterssprites01.bin
rm b1r1f0_charmapkamikaze01.bin b1r1f1_charmapbeaminvader01b.bin


#- ROM_REGION( 0x8000, "gfx1", 0 )
#- ROM_LOAD( "a-d5-2.bin",     0x0000, 0x2000, CRC(0bbf3fdb) SHA1(035c2db6eca701be690042e006c0d07c90d752f1) )  /* sprites 32x32 */
#- ROM_LOAD( "a-d6-3.bin",     0x2000, 0x2000, CRC(4398dc21) SHA1(3b23433d0c9daa554ad6615af2fdec715e4e3794) )
#- ROM_LOAD( "a-d7-4.bin",     0x4000, 0x2000, CRC(aca197fc) SHA1(03ecd94b84a31389539074079ed7f2a500e588ab) )
#- ROM_LOAD( "a-d8-5.bin",     0x6000, 0x2000, CRC(1e6edc17) SHA1(4dbc91938c999348bcbd5f960fc3bb49f3174059) )
#- ROM_REGION( 0xc000, "gfx2", ROMREGION_INVERT )
#- ROM_LOAD( "b-m4-11.bin",    0x0000, 0x4000, CRC(eb88ee1f) SHA1(60ec2d77186c196a27278b0639cbfa838986e2e2) )  /* background tilemap characters 8x8 */
#- ROM_LOAD( "b-m5-12.bin",    0x4000, 0x4000, CRC(4c65f399) SHA1(72db15884f346c001d3b86cb33e3f6d339eedb56) )
#- ROM_LOAD( "b-m6-13.bin",    0x8000, 0x4000, CRC(a5baa413) SHA1(dc772042706c3a92594ee8422aafed77375c0632) )
#- ROM_REGION( 0x2000, "gfx3", 0 )
#- ROM_LOAD( "b-r3-14.bin",    0x0000, 0x2000, CRC(4310a815) SHA1(bf58a7a8d3f82fcaa0c46d9ebb13cac1231b80ad) )  /* text tilemap characters 8x8 */













# proms


zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=tankbust/tb-prom.1s8
dd ibs=1 count=$((0x0020)) skip=$((0x0030)) if=palette01.bin of=tankbust/tb-prom.2r8
dd ibs=1 count=$((0x0020)) skip=$((0x0050)) if=palette01.bin of=tankbust/tb-prom.3p8
dd ibs=1 count=$((0x0020)) skip=$((0x0070)) if=palette01.bin of=tankbust/tb-prom.4k8
rm palette01.bin
##dd bs=$((0x0020)) count=1 if=/dev/urandom of=tankbust/tankbust.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=tankbust/tankbust.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=tankbust/tankbust.11j
#rm clut01b.bin

#dd bs=256 count=1 if=/dev/urandom of=tankbust/tb-prom.1s8
#dd bs=256 count=1 if=/dev/urandom of=tankbust/tb-prom.2r8
#dd bs=256 count=1 if=/dev/urandom of=tankbust/tb-prom.3p8
#dd bs=256 count=1 if=/dev/urandom of=tankbust/tb-prom.4k8

#- ROM_REGION( 0x0080, "proms", 0 )
#- ROM_LOAD( "tb-prom.1s8",    0x0000, 0x0020, CRC(dfaa086c) SHA1(f534aedddd18addd0833a3a28a4297689c4a46ac) ) //sprites
#- ROM_LOAD( "tb-prom.2r8",    0x0020, 0x0020, CRC(ec50d674) SHA1(64c8961eca33b23e14b7383eb7e64fcac8772ee7) ) //background
#- ROM_LOAD( "tb-prom.3p8",    0x0040, 0x0020, CRC(3e70eafd) SHA1(b200350a3f6c166228706734419dd3ef1207eeef) ) //background palette 2 ??
#- ROM_LOAD( "tb-prom.4k8",    0x0060, 0x0020, CRC(624f40d2) SHA1(8421f1d774afc72e0817d41edae74a2837021a5f) ) //text
#- ROM_END











rm smsboot.bin dummy32k.bin example01.bin

zip -r tankbust tankbust
rm tankbust/*
rmdir tankbust

mame -w -video soft -resolution0 512x512 -rp ./ tankbust






#- ROM_START( tankbust )
#- ROM_REGION( 0x1c000, "maincpu", 0 )
#- ROM_LOAD( "a-s4-6.bin",     0x00000, 0x4000, CRC(8ebe7317) SHA1(bc45d530ad6335312c9c3efdcedf7acd2cdeeb55) )
#- ROM_LOAD( "a-s7-9.bin",     0x04000, 0x2000, CRC(047aee33) SHA1(62ee776c403b228e065baa9218f32597951ca935) )
#- ROM_LOAD( "a-s5_7.bin",     0x12000, 0x2000, CRC(dd4800ca) SHA1(73a6caa029c27fb45217f9372d9541c6fe206f08) ) /* banked at 0x6000-0x9fff */
#- ROM_CONTINUE(                   0x10000, 0x2000)
#- ROM_LOAD( "a-s6-8.bin",     0x16000, 0x2000, CRC(f8801238) SHA1(fd3abe18542660a8c31dc316012a99d48c9bb5aa) ) /* banked at 0x6000-0x9fff */
#- ROM_CONTINUE(                   0x14000, 0x2000)
#- // ROM_LOAD( "a-s5_7.bin",     0x10000, 0x4000, CRC(dd4800ca) SHA1(73a6caa029c27fb45217f9372d9541c6fe206f08) ) /* banked at 0x6000-0x9fff */
#- // ROM_LOAD( "a-s6-8.bin",     0x14000, 0x4000, CRC(f8801238) SHA1(fd3abe18542660a8c31dc316012a99d48c9bb5aa) ) /* banked at 0x6000-0x9fff */
#- ROM_LOAD( "a-s8-10.bin",    0x18000, 0x4000, CRC(9e826faa) SHA1(6a252428c69133d3e9d7a9938140d5ae37fb0c7d) ) /* banked at 0xa000-0xbfff */



#- ROM_REGION( 0x10000, "sub", 0 )
#- ROM_LOAD( "a-b3-1.bin",     0x0000, 0x2000, CRC(b0f56102) SHA1(4f427c3bd6131b7cba42a0e24a69bd1b6a1b0a3c) )


#- ROM_REGION( 0x8000, "gfx1", 0 )
#- ROM_LOAD( "a-d5-2.bin",     0x0000, 0x2000, CRC(0bbf3fdb) SHA1(035c2db6eca701be690042e006c0d07c90d752f1) )  /* sprites 32x32 */
#- ROM_LOAD( "a-d6-3.bin",     0x2000, 0x2000, CRC(4398dc21) SHA1(3b23433d0c9daa554ad6615af2fdec715e4e3794) )
#- ROM_LOAD( "a-d7-4.bin",     0x4000, 0x2000, CRC(aca197fc) SHA1(03ecd94b84a31389539074079ed7f2a500e588ab) )
#- ROM_LOAD( "a-d8-5.bin",     0x6000, 0x2000, CRC(1e6edc17) SHA1(4dbc91938c999348bcbd5f960fc3bb49f3174059) )
#- ROM_REGION( 0xc000, "gfx2", ROMREGION_INVERT )
#- ROM_LOAD( "b-m4-11.bin",    0x0000, 0x4000, CRC(eb88ee1f) SHA1(60ec2d77186c196a27278b0639cbfa838986e2e2) )  /* background tilemap characters 8x8 */
#- ROM_LOAD( "b-m5-12.bin",    0x4000, 0x4000, CRC(4c65f399) SHA1(72db15884f346c001d3b86cb33e3f6d339eedb56) )
#- ROM_LOAD( "b-m6-13.bin",    0x8000, 0x4000, CRC(a5baa413) SHA1(dc772042706c3a92594ee8422aafed77375c0632) )
#- ROM_REGION( 0x2000, "gfx3", 0 )
#- ROM_LOAD( "b-r3-14.bin",    0x0000, 0x2000, CRC(4310a815) SHA1(bf58a7a8d3f82fcaa0c46d9ebb13cac1231b80ad) )  /* text tilemap characters 8x8 */




#- ROM_REGION( 0x0080, "proms", 0 )
#- ROM_LOAD( "tb-prom.1s8",    0x0000, 0x0020, CRC(dfaa086c) SHA1(f534aedddd18addd0833a3a28a4297689c4a46ac) ) //sprites
#- ROM_LOAD( "tb-prom.2r8",    0x0020, 0x0020, CRC(ec50d674) SHA1(64c8961eca33b23e14b7383eb7e64fcac8772ee7) ) //background
#- ROM_LOAD( "tb-prom.3p8",    0x0040, 0x0020, CRC(3e70eafd) SHA1(b200350a3f6c166228706734419dd3ef1207eeef) ) //background palette 2 ??
#- ROM_LOAD( "tb-prom.4k8",    0x0060, 0x0020, CRC(624f40d2) SHA1(8421f1d774afc72e0817d41edae74a2837021a5f) ) //text
#- ROM_END


