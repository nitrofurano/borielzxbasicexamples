rm bagman.zip
rm example01.asm
rm bagman/*
rmdir bagman
mkdir bagman






dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin






#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=bagman/e9_b05.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=bagman/f9_b06.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=bagman/f9_b07.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=bagman/k9_b08.bin
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=bagman/m9_b09s.bin
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=bagman/n9_b10.bin

#ROM_START( bagman )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "e9_b05.bin",   0x0000, 0x1000, CRC(e0156191) SHA1(bb5f16d49fbe48f3bac118acd1fea51ec4bc5355) )
#ROM_LOAD( "f9_b06.bin",   0x1000, 0x1000, CRC(7b758982) SHA1(c8460023b43fed4aca9c6b987faea334832c5e30) )
#ROM_LOAD( "f9_b07.bin",   0x2000, 0x1000, CRC(302a077b) SHA1(916c4a6ea1e631cc72bdb91ff9d263dcbaf08bb2) )
#ROM_LOAD( "k9_b08.bin",   0x3000, 0x1000, CRC(f04293cb) SHA1(ce6b0ae4088ce28c75d414f506fad2cf2b6920c2) )
#ROM_LOAD( "m9_b09s.bin",  0x4000, 0x1000, CRC(68e83e4f) SHA1(9454564885a1003cee7107db18bedb387b85e9ab) )
#ROM_LOAD( "n9_b10.bin",   0x5000, 0x1000, CRC(1d6579f7) SHA1(3ab54329f516156b1c9f68efbe59c95d5240bc8c) )









dd bs=$((0x1000)) count=1 if=/dev/zero of=bagman/r6.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=bagman/r9_b11.bin
dd bs=$((0x1000)) count=1 if=/dev/zero of=bagman/t9_b12.bin

#ROM_REGION( 0x0060, "5110ctrl", 0)
#ROM_LOAD( "r6.bin",       0x0000, 0x0020, CRC(c58a4f6a) SHA1(35ef244b3e94032df2610aa594ea5670b91e1449) ) /*state machine driving TMS5110*/
#ROM_REGION( 0x2000, "tmsprom", 0 ) /* data for the TMS5110 speech chip */
#ROM_LOAD( "r9_b11.bin",   0x0000, 0x1000, CRC(2e0057ff) SHA1(33e3ffa6418f86864eb81e5e9bda4bf540c143a6) )
#ROM_LOAD( "t9_b12.bin",   0x1000, 0x1000, CRC(b2120edd) SHA1(52b89dbcc749b084331fa82b13d0876e911fce52) )
#ROM_END








zxb.py library/b2r3f0_charmapbagman01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r3f0_charmapbagman01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b2r3f0_charmapbagman01.bin of=b2r3f0_charmapbagman01b.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r3f0_charmapbagman01b.bin | cut -b 1-2 | xxd -r -p > bagman/e1_b02.bin
xxd -p -c 2 -s 1 -l $((0x2000)) b2r3f0_charmapbagman01b.bin | cut -b 1-2 | xxd -r -p > bagman/j1_b04.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r3f0_charmapbagman01b.bin | cut -b 1-2 | xxd -r -p > bagman/c1_b01.bin
xxd -p -c 2 -s 1 -l $((0x2000)) b2r3f0_charmapbagman01b.bin | cut -b 1-2 | xxd -r -p > bagman/f1_b03s.bin
rm b2r3f0_charmapbagman01.bin b2r3f0_charmapbagman01b.bin
#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "e1_b02.bin",   0x0000, 0x1000, CRC(4a0a6b55) SHA1(955f8bd4bd9b0fc3c6c359c25ba543ba26c04cbd) )
#ROM_LOAD( "j1_b04.bin",   0x1000, 0x1000, CRC(c680ef04) SHA1(79406bc786374abfcd9f548268c445b5c8d8858d) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "c1_b01.bin",   0x0000, 0x1000, CRC(705193b2) SHA1(ca9cfd05f9195c2a38e8854012de51b6ee6bb403) )
#ROM_LOAD( "f1_b03s.bin",  0x1000, 0x1000, CRC(dba1eda7) SHA1(26d877028b3a31dd671f9e667316c8a14780ca73) )







#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=bagman/bagman.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=bagman/bagman.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=bagman/bagman.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=bagman/bagman.11j
#rm clut01b.bin

#????
dd bs=256 count=1 if=/dev/urandom of=bagman/p3.bin
dd bs=256 count=1 if=/dev/urandom of=bagman/r3.bin

#ROM_REGION( 0x0060, "proms", 0 )
#ROM_LOAD( "p3.bin",       0x0000, 0x0020, CRC(2a855523) SHA1(91e032233fee397c90b7c1662934aca9e0671482) )
#ROM_LOAD( "r3.bin",       0x0020, 0x0020, CRC(ae6f1019) SHA1(fd711882b670380cb4bd909c840ba06277b8fbe3) )









rm smsboot.bin dummy32k.bin example01.bin

zip -r bagman bagman
rm bagman/*
rmdir bagman

mame -w -video soft -resolution0 512x512 -rp ./ bagman







#ROM_START( bagman )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "e9_b05.bin",   0x0000, 0x1000, CRC(e0156191) SHA1(bb5f16d49fbe48f3bac118acd1fea51ec4bc5355) )
#ROM_LOAD( "f9_b06.bin",   0x1000, 0x1000, CRC(7b758982) SHA1(c8460023b43fed4aca9c6b987faea334832c5e30) )
#ROM_LOAD( "f9_b07.bin",   0x2000, 0x1000, CRC(302a077b) SHA1(916c4a6ea1e631cc72bdb91ff9d263dcbaf08bb2) )
#ROM_LOAD( "k9_b08.bin",   0x3000, 0x1000, CRC(f04293cb) SHA1(ce6b0ae4088ce28c75d414f506fad2cf2b6920c2) )
#ROM_LOAD( "m9_b09s.bin",  0x4000, 0x1000, CRC(68e83e4f) SHA1(9454564885a1003cee7107db18bedb387b85e9ab) )
#ROM_LOAD( "n9_b10.bin",   0x5000, 0x1000, CRC(1d6579f7) SHA1(3ab54329f516156b1c9f68efbe59c95d5240bc8c) )

#ROM_REGION( 0x0060, "5110ctrl", 0)
#ROM_LOAD( "r6.bin",       0x0000, 0x0020, CRC(c58a4f6a) SHA1(35ef244b3e94032df2610aa594ea5670b91e1449) ) /*state machine driving TMS5110*/
#ROM_REGION( 0x2000, "tmsprom", 0 ) /* data for the TMS5110 speech chip */
#ROM_LOAD( "r9_b11.bin",   0x0000, 0x1000, CRC(2e0057ff) SHA1(33e3ffa6418f86864eb81e5e9bda4bf540c143a6) )
#ROM_LOAD( "t9_b12.bin",   0x1000, 0x1000, CRC(b2120edd) SHA1(52b89dbcc749b084331fa82b13d0876e911fce52) )
#ROM_END

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "e1_b02.bin",   0x0000, 0x1000, CRC(4a0a6b55) SHA1(955f8bd4bd9b0fc3c6c359c25ba543ba26c04cbd) )
#ROM_LOAD( "j1_b04.bin",   0x1000, 0x1000, CRC(c680ef04) SHA1(79406bc786374abfcd9f548268c445b5c8d8858d) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "c1_b01.bin",   0x0000, 0x1000, CRC(705193b2) SHA1(ca9cfd05f9195c2a38e8854012de51b6ee6bb403) )
#ROM_LOAD( "f1_b03s.bin",  0x1000, 0x1000, CRC(dba1eda7) SHA1(26d877028b3a31dd671f9e667316c8a14780ca73) )

#ROM_REGION( 0x0060, "proms", 0 )
#ROM_LOAD( "p3.bin",       0x0000, 0x0020, CRC(2a855523) SHA1(91e032233fee397c90b7c1662934aca9e0671482) )
#ROM_LOAD( "r3.bin",       0x0020, 0x0020, CRC(ae6f1019) SHA1(fd711882b670380cb4bd909c840ba06277b8fbe3) )









