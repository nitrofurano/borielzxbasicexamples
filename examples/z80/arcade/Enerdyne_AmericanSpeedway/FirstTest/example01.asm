	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$E7FF
#line 1
	ld hl, 400
	ld (_delaytime), hl
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	push af
	ld hl, (_ee1)
	push hl
	call _americanspeedwaypalette
__LABEL4:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL0:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 2130
	push hl
	ld hl, 0
	push hl
	call _americanspeedwaypalettehex
	ld hl, 803
	push hl
	ld hl, 1
	push hl
	call _americanspeedwaypalettehex
	ld hl, 3477
	push hl
	ld hl, 2
	push hl
	call _americanspeedwaypalettehex
	ld hl, 563
	push hl
	ld hl, 3
	push hl
	call _americanspeedwaypalettehex
	ld hl, 2930
	push hl
	ld hl, 4
	push hl
	call _americanspeedwaypalettehex
	ld hl, 819
	push hl
	ld hl, 8
	push hl
	call _americanspeedwaypalettehex
	ld hl, 2730
	push hl
	ld hl, 9
	push hl
	call _americanspeedwaypalettehex
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL10
__LABEL13:
	ld a, 1
	push af
	ld a, 65
	push af
	ld hl, (_ee1)
	push hl
	ld hl, (_ee2)
	push hl
	call _americanspeedwayputchar
__LABEL14:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL10:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL5:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL20
__LABEL23:
	ld a, 2
	push af
	ld hl, (_ee1)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee1)
	ld de, 12
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 14
	add hl, de
	push hl
	call _americanspeedwayputchar
__LABEL24:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL20:
	ld hl, 15
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL15:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 0
	ld (_eee), hl
__LABEL25:
	ld a, (43008)
	ld l, a
	ld h, 0
	ld (_ex0), hl
	ld a, (44032)
	ld l, a
	ld h, 0
	ld (_ey0), hl
	ld a, 3
	push af
	ld hl, (_ex0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 8
	push hl
	call _americanspeedwayputchar
	ld a, 3
	push af
	ld hl, (_ex0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 7
	push hl
	call _americanspeedwayputchar
	ld a, 3
	push af
	ld hl, (_ex0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 6
	push hl
	call _americanspeedwayputchar
	ld a, 3
	push af
	ld hl, (_ey0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 8
	push hl
	call _americanspeedwayputchar
	ld a, 3
	push af
	ld hl, (_ey0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 7
	push hl
	call _americanspeedwayputchar
	ld a, 3
	push af
	ld hl, (_ey0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 6
	push hl
	call _americanspeedwayputchar
	ld a, 4
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_ey0)
	srl h
	rr l
	ld de, 0
	push hl
	ld hl, (_ex0)
	srl h
	rr l
	ld de, 0
	push hl
	call _americanspeedwayputchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL27
__LABEL30:
__LABEL31:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL27:
	ld hl, 1000
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL30
__LABEL29:
	jp __LABEL25
__LABEL26:
__LABEL32:
	jp __LABEL32
__LABEL33:
__LABEL34:
	ld hl, 49152
	ld (_eee), hl
	jp __LABEL36
__LABEL39:
	ld hl, 1
	ld (_i), hl
	jp __LABEL41
__LABEL44:
__LABEL45:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL41:
	ld hl, (_delaytime)
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL44
__LABEL43:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL40:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL36:
	ld hl, 49407
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL39
__LABEL38:
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL46
__LABEL49:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL51
__LABEL54:
	ld hl, 1
	ld (_ee1), hl
	jp __LABEL56
__LABEL59:
__LABEL60:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL56:
	ld hl, (_delaytime)
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL59
__LABEL58:
	ld hl, (_ee2)
	ld a, l
	push af
	ld hl, (_ee2)
	ld a, l
	push af
	ld hl, (_ee2)
	ld a, l
	push af
	ld hl, (_ee2)
	ld a, l
	push af
	ld hl, (_ee3)
	push hl
	call _americanspeedwayputsprite
__LABEL55:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL51:
	ld hl, 255
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL54
__LABEL53:
__LABEL50:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL46:
	ld hl, 63
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL49
__LABEL48:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL61
__LABEL64:
	ld hl, (_ee2)
	ld de, 49152
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_ee3), hl
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL66
__LABEL69:
	ld hl, 1
	ld (_eee), hl
	jp __LABEL71
__LABEL74:
__LABEL75:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL71:
	ld hl, (_delaytime)
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL74
__LABEL73:
	ld hl, (_ee2)
	ld de, 49152
	add hl, de
	push hl
	ld hl, (_ee1)
	ld a, l
	pop hl
	ld (hl), a
__LABEL70:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL66:
	ld hl, 255
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL69
__LABEL68:
	ld hl, (_ee2)
	ld de, 49152
	add hl, de
	push hl
	ld hl, (_ee3)
	ld a, l
	pop hl
	ld (hl), a
__LABEL65:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL61:
	ld hl, 255
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL64
__LABEL63:
	jp __LABEL34
__LABEL35:
__LABEL76:
	jp __LABEL76
__LABEL77:
__LABEL78:
	ld hl, 32768
	ld (_eee), hl
	jp __LABEL80
__LABEL83:
	ld hl, 1
	ld (_i), hl
	jp __LABEL85
__LABEL88:
__LABEL89:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL85:
	ld hl, (_delaytime)
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL88
__LABEL87:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL84:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL80:
	ld hl, 32799
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL83
__LABEL82:
	ld hl, 36864
	ld (_eee), hl
	jp __LABEL90
__LABEL93:
	ld hl, 1
	ld (_i), hl
	jp __LABEL95
__LABEL98:
__LABEL99:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL95:
	ld hl, (_delaytime)
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL98
__LABEL97:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL94:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL90:
	ld hl, 37887
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL93
__LABEL92:
	ld hl, 38912
	ld (_eee), hl
	jp __LABEL100
__LABEL103:
	ld hl, 1
	ld (_i), hl
	jp __LABEL105
__LABEL108:
__LABEL109:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL105:
	ld hl, (_delaytime)
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL108
__LABEL107:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL104:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL100:
	ld hl, 39935
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL103
__LABEL102:
	ld hl, 49152
	ld (_eee), hl
	jp __LABEL110
__LABEL113:
	ld hl, 1
	ld (_i), hl
	jp __LABEL115
__LABEL118:
__LABEL119:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL115:
	ld hl, (_delaytime)
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL118
__LABEL117:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL114:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL110:
	ld hl, 49407
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL113
__LABEL112:
	jp __LABEL78
__LABEL79:
__LABEL120:
	jp __LABEL120
__LABEL121:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_americanspeedwayputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 36864
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 31
	pop hl
	call __BXOR16
	ld de, 38912
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_americanspeedwayputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_americanspeedwaypalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32768
	add hl, de
	push hl
	ld a, (ix+7)
	cpl
	pop hl
	ld (hl), a
_americanspeedwaypalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_americanspeedwaypalettehex:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32768
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3584
	pop hl
	call __BAND16
	ld de, 512
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 224
	pop hl
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	pop hl
	call __BOR16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 12
	pop hl
	call __BAND16
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	pop hl
	call __BOR16
	push hl
	ld de, 255
	pop hl
	call __BXOR16
	ld a, l
	pop hl
	ld (hl), a
_americanspeedwaypalettehex__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_americanspeedwayputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 49152
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 49153
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 49154
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	ld de, 49155
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
_americanspeedwayputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 899 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 900 "example01.zxb"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 901 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 902 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 903 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 904 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 905 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_i EQU 57360
	_eee EQU 57362
	_seed EQU 57364
	_delaytime EQU 57366
	_ee1 EQU 57368
	_ee2 EQU 57370
	_ee3 EQU 57372
	_ee4 EQU 57374
	_ex0 EQU 57376
	_ey0 EQU 57378
	_ej0 EQU 57380
	_ej1 EQU 57382
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
