rm example01.asm
rm amspdwy.zip
rm amspdwy/*
rmdir amspdwy
mkdir amspdwy

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=amspdwy/game5807.u33
dd bs=$((0x8000)) count=1 if=/dev/zero of=amspdwy/trks6092.u34

#audio
dd bs=$((0x8000)) count=1 if=/dev/zero of=amspdwy/audi9463.u2

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
xxd -p -c 2 -s $((0x0000)) -l $((0x2000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > amspdwy/hilo9b3c.5a
xxd -p -c 2 -s $((0x0001)) -l $((0x2000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > amspdwy/lolo1d51.1a
xxd -p -c 2 -s $((0x2000)) -l $((0x2000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > amspdwy/hihie12a.4a
xxd -p -c 2 -s $((0x2001)) -l $((0x2000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > amspdwy/lohi4644.2a

#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmap01b.bin of=amspdwy/hilo9b3c.5a
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmap01b.bin of=amspdwy/hihie12a.4a
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmap01b.bin of=amspdwy/lolo1d51.1a
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmap01b.bin of=amspdwy/lohi4644.2a
#dd bs=$((0x1000)) count=1 if=/dev/zero of=amspdwy/hihie12a.4a
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=amspdwy/amspdwy.5f
#rm spritemap01.bin
#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=amspdwy/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=amspdwy/82s126.4a
#rm clut01.bin
#prom
#dd bs=$((0x8000)) count=1 if=/dev/zero of=amspdwy/82s126.1m
#dd bs=$((0x8000)) count=1 if=/dev/zero of=amspdwy/82s126.3m

rm smsboot.bin dummy64k.bin example01.bin charmap01b.bin charmap01.bin

zip -r amspdwy amspdwy
rm amspdwy/*
rmdir amspdwy

mame -w -video soft -resolution0 512x512 -rp ./ amspdwy

#-----------------------------------------------
# ROM_REGION( 0x18000, "maincpu", 0 )     /* Main Z80 Code */
# ROM_LOAD( "game5807.u33", 0x00000, 0x8000, CRC(88233b59) SHA1(bfdf10dde1731cde5c579a9a5173cafe9295a80c) )
# ROM_LOAD( "trks6092.u34", 0x10000, 0x8000, CRC(74a4e7b7) SHA1(b4f6e3faaf048351c6671205f52378a64b81bcb1) )

# ROM_REGION( 0x10000, "audiocpu", 0 )        /* Sound Z80 Code */
# ROM_LOAD( "audi9463.u2", 0x00000, 0x8000, CRC(61b0467e) SHA1(74509e7712838dd760919893aeda9241d308d0c3) )

# ROM_REGION( 0x4000, "gfx1", 0 ) /* Layer + Sprites */
# ROM_LOAD( "hilo9b3c.5a", 0x0000, 0x1000, CRC(f50f864c) SHA1(5b2412c1558b30a04523fcdf1d5cf6fdae1ba88d) )
# ROM_LOAD( "hihie12a.4a", 0x1000, 0x1000, CRC(3d7497f3) SHA1(34820ba42d9c9dab1d6fdda15795450ce08392c1) )
# ROM_LOAD( "lolo1d51.1a", 0x2000, 0x1000, CRC(58701c1c) SHA1(67b476e697652a6b684bd76ae6c0078ed4b3e3a2) )
# ROM_LOAD( "lohi4644.2a", 0x3000, 0x1000, CRC(a1d802b1) SHA1(1249ce406b1aa518885a02ab063fa14906ccec2e) )

