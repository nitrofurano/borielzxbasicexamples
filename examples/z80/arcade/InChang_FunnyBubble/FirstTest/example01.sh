# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm funybubl.zip example01.asm
rm funybubl/*
rmdir funybubl
mkdir funybubl

dd bs=$((0x200000)) count=1 if=/dev/zero of=dummy2m.bin

#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy2m.bin >> example01.bin
dd ibs=1 count=$((0x40000)) skip=$((0x0000)) if=example01.bin of=funybubl/a.ub16
rm example01.bin smsboot.bin

#sound
dd bs=$((0x10000)) count=1 if=/dev/zero of=funybubl/sound_prog

#gfx
#gfx1 ($40000)
#dd bs=$((0x00001)) count=1 if=/dev/urandom of=funybubl/f.ug13
#dd bs=$((0x00002)) count=1 if=/dev/urandom of=funybubl/g.uh13
#dd bs=$((0x00004)) count=1 if=/dev/urandom of=funybubl/h.ug15
#dd bs=$((0x00008)) count=1 if=/dev/urandom of=funybubl/i.uh15
#dd bs=$((0x00010)) count=1 if=/dev/urandom of=funybubl/l.ug16
#dd bs=$((0x00020)) count=1 if=/dev/urandom of=funybubl/m.uh16
#dd bs=$((0x00040)) count=1 if=/dev/urandom of=funybubl/n.ug17
#dd bs=$((0x00080)) count=1 if=/dev/urandom of=funybubl/o.uh17

zxb.py library/b8r0f0_charmapfunnybubblev01.zxi --org=$((0x0000))
cat dummy2m.bin >> b8r0f0_charmapfunnybubblev01.bin

dd ibs=1 count=$((0x200000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=b8r0f0_charmapfunnybubblev01b.bin
rm b8r0f0_charmapfunnybubblev01.bin

# -c 3, -s 0..2
xxd -p -c 8 -s 7 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/l.ug16
xxd -p -c 8 -s 6 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/m.uh16
xxd -p -c 8 -s 5 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/n.ug17
xxd -p -c 8 -s 4 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/o.uh17
xxd -p -c 8 -s 3 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/f.ug13
xxd -p -c 8 -s 2 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/g.uh13
xxd -p -c 8 -s 1 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/h.ug15
xxd -p -c 8 -s 0 -l $((0x200000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/i.uh15


rm b8r0f0_charmapfunnybubblev01b.bin



#gfx2 ($80000)
dd bs=$((0x00001)) count=1 if=/dev/urandom of=funybubl/d.ug1
dd bs=$((0x00002)) count=1 if=/dev/urandom of=funybubl/e.ug2
dd bs=$((0x00004)) count=1 if=/dev/urandom of=funybubl/j.ug3
dd bs=$((0x00008)) count=1 if=/dev/urandom of=funybubl/k.ug4
#zxb.py library/b8r0f0_charmapfunnybubblev01.bas --org=$((0x0000))
#cat dummy2m.bin >> b8r0f0_charmapfunnybubblev01.bin
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=funybubl/3_graphics
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=funybubl/4_graphics
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=funybubl/5_graphics
dd ibs=1 count=$((0x180000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=b8r0f0_charmapfunnybubblev01b.bin
#rm b8r0f0_charmapfunnybubblev01.bin
#xxd -p -c 3 -s 0 -l $((0x180000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/5_graphics
#xxd -p -c 3 -s 1 -l $((0x180000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/3_graphics
#xxd -p -c 3 -s 2 -l $((0x180000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > funybubl/4_graphics
#rm b8r0f0_charmapfunnybubblev01b.bin





#sound
dd bs=$((0x80000)) count=1 if=/dev/zero of=funybubl/p.su6
dd bs=$((0x20000)) count=1 if=/dev/zero of=funybubl/b.su12
dd bs=$((0x20000)) count=1 if=/dev/zero of=funybubl/c.su13








rm dummy2m.bin
zip -r funybubl funybubl
rm funybubl/*
rmdir funybubl

mame -w -video soft -resolution0 512x512 -rp ./ funybubl
#xmame.SDL -rp ./ funybubl


#- ROM_REGION( 0x40000, "maincpu", 0 ) /* main z80, lots of banked data */
#- ROM_LOAD( "a.ub16", 0x00000, 0x40000, CRC(4e799cdd) SHA1(c6474fd2f621c27224e847ecb88a1ae17a0dbaf9)  )

#- ROM_REGION( 0x200000, "gfx1", ROMREGION_INVERT  ) // bg gfx 8x8x8
#- ROM_LOAD( "f.ug13", 0x000000, 0x40000, CRC(64d7163d) SHA1(2619ac96e05779ea23c7f0f71665d284c79ba72f) )
#- ROM_LOAD( "g.uh13", 0x040000, 0x40000, CRC(6891e2b8) SHA1(ca711019e5c330759d2a90024dbc0e6731b6227f) )
#- ROM_LOAD( "h.ug15", 0x080000, 0x40000, CRC(ca7f7528) SHA1(6becfe8fabd19443a13b948838f41e10e5c9dc87) )
#- ROM_LOAD( "i.uh15", 0x0c0000, 0x40000, CRC(23608ec6) SHA1(1c0a5d6e300f9a1abfda73d6a6a31e29a42b30ad) )
#- ROM_LOAD( "l.ug16", 0x100000, 0x40000, CRC(0acf8143) SHA1(f49f45496b870d1f51f09a4dda8c5bb7763c40d3) )
#- ROM_LOAD( "m.uh16", 0x140000, 0x40000, CRC(55ed8d9c) SHA1(f17bb4d02d4eedc2f297bb008be2fa340bc321d2) )
#- ROM_LOAD( "n.ug17", 0x180000, 0x40000, CRC(52398b68) SHA1(522baa8123998e9161fa1ccaf760ac006c5be2dd) )
#- ROM_LOAD( "o.uh17", 0x1c0000, 0x40000, CRC(446e31b2) SHA1(7f37a7090c83f2c9b07f1993707540fb32bbed35) )

#- ROM_REGION( 0x200000, "gfx2", ROMREGION_INVERT )
#- ROM_LOAD( "d.ug1", 0x000000, 0x80000, CRC(b7ebbc00) SHA1(92520fda2f8f242b8cd49aeaac21b279f48276bf) ) /* Same as below, different labels */
#- ROM_LOAD( "e.ug2", 0x080000, 0x80000, CRC(28afc396) SHA1(555d51948ffb237311112dcfd0516a43f603ff03) )
#- ROM_LOAD( "j.ug3", 0x100000, 0x80000, CRC(9e8687cd) SHA1(42fcba2532ae5028fcfc1df50750d99ad2586820) )
#- ROM_LOAD( "k.ug4", 0x180000, 0x80000, CRC(63f0e810) SHA1(5c7ed32ee8dc1d9aabc8d136ec370471096356c2) )

#- ROM_REGION( 0x08000, "audiocpu", 0 ) /* sound z80 (not much code here ..) */
#- ROM_LOAD( "p.su6", 0x00000,  0x08000, CRC(33169d4d) SHA1(0ebc932d15b6df022c7e1f44df884e64b25ba745) ) /* Same as below, different label */

#- ROM_REGION( 0x80000, "oki", 0 )
#- ROM_LOAD( "b.su12", 0x00000,  0x20000, CRC(a2d780f4) SHA1(bebba3db21ab9ddde8c6f19db3b67c869df582eb) ) /* Same as below, different label */
#- ROM_RELOAD(         0x40000,  0x20000 )
#- ROM_LOAD( "c.su13", 0x20000,  0x20000, CRC(1f7e9269) SHA1(5c16b49a4e94aec7606d088c2d45a77842ab565b) ) /* Same as below, different label */
#- ROM_CONTINUE(       0x60000,  0x20000 )
