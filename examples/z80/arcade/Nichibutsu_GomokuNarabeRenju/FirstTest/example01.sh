rm gomoku.zip
rm example01.asm
rm gomoku/*
rmdir gomoku
mkdir gomoku


dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=gomoku/rj_1.7a
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=gomoku/rj_2.7c
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=gomoku/rj_3.7d
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=gomoku/rj_4.7f
dd ibs=1 count=$((0x0800)) skip=$((0x4000)) if=example01.bin of=gomoku/rj_5.4e

#	ROM_REGION( 0x10000, "maincpu", 0 ) // program
#	ROM_LOAD( "rj_1.7a",    0x0000, 0x1000, CRC(ed20d539) SHA1(7cbbc678cbe5c85b914ca44f82bdbd452cf694a0) )
#	ROM_LOAD( "rj_2.7c",    0x1000, 0x1000, CRC(26a28516) SHA1(53d5d134cd91020fa06e380d355deb1df6b9cb6e) )
#	ROM_LOAD( "rj_3.7d",    0x2000, 0x1000, CRC(d05db072) SHA1(9697c932c6dcee6f8536c9f0b3c84a719a7d3dee) )
#	ROM_LOAD( "rj_4.7f",    0x3000, 0x1000, CRC(6e3d1c18) SHA1(e2f7e4c0de3c78d1b8e686152458972f996b023a) )
#	ROM_LOAD( "rj_5.4e",    0x4000, 0x0800, CRC(eaf541b4) SHA1(bc7e7ec1ba68f71ab9ac86f9ae77971ddb9ce3a4) )




#sound
dd bs=$((0x1000)) count=1 if=/dev/zero of=gomoku/rj_7.3c

#	ROM_REGION( 0x1000, "sound", 0 )   // sound
#	ROM_LOAD( "rj_7.3c",    0x0000, 0x1000, CRC(d1ed1365) SHA1(4ef08f26fe7df4c400f72e09e56d8825d584f55f) )





#gfx
zxb.py library/b2r1f0sx_charmapchinesehero01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f0sx_charmapchinesehero01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=gomoku/rj_6.4r
rm charmap01.bin

#	ROM_REGION( 0x1000, "gfx1", 0 ) // text char
#	ROM_LOAD( "rj_6.4r",    0x0000, 0x1000, CRC(ed26ae36) SHA1(61cb73d7f2568e88e1c2981e7af3e9a3b26797d3) )






#palette
zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=gomoku/rj_prom.1m
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=gomoku/rj_prom.1l
rm palette01.bin palette01b.bin

#	ROM_REGION( 0x0040, "proms", 0 )
#	ROM_LOAD( "rj_prom.1m", 0x0000, 0x0020, CRC(5da2f2bd) SHA1(4355ccf06cb09ec3240dc92bda19b1f707a010ef) )  // TEXT color
#	ROM_LOAD( "rj_prom.1l", 0x0020, 0x0020, CRC(fe4ef393) SHA1(d4c63f8645afeadd13ff82087bcc497d8936d90b) )  // BG color



#prom
dd bs=$((0x0100)) count=1 if=/dev/urandom of=gomoku/rj_prom.8n
dd bs=$((0x0100)) count=1 if=/dev/urandom of=gomoku/rj_prom.7p
dd bs=$((0x0100)) count=1 if=/dev/urandom of=gomoku/rj_prom.7r
dd bs=$((0x0020)) count=1 if=/dev/urandom of=gomoku/rj_prom.9k

#	ROM_REGION( 0x0100, "user1", 0 )    // BG draw data X
#	ROM_LOAD( "rj_prom.8n", 0x0000, 0x0100, CRC(9ba43222) SHA1(a443df49d7ee9dbfd258b09731d392bf1249cbfa) )
#	ROM_REGION( 0x0100, "user2", 0 )    // BG draw data Y
#	ROM_LOAD( "rj_prom.7p", 0x0000, 0x0100, CRC(5b5464f8) SHA1(b945efb8a7233f501d67f6b1be4e9d4967dc6719) )
#	ROM_REGION( 0x0100, "user3", 0 )    // BG character data
#	ROM_LOAD( "rj_prom.7r", 0x0000, 0x0100, CRC(3004585a) SHA1(711b68140827f0f3dc71f2576fcf9b905c999e8d) )
#	ROM_REGION( 0x0020, "user4", 0 )    // unknown
#	ROM_LOAD( "rj_prom.9k", 0x0000, 0x0020, CRC(cff72923) SHA1(4f61375028ab62da46ed119bc81052f5f98c28d4) )





#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=gomoku/gomoku.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=gomoku/gomoku.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=gomoku/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=gomoku/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin b2r1f0sx_charmapchinesehero01.bin

zip -r gomoku gomoku
rm gomoku/*
rmdir gomoku

mame -w -video soft -resolution0 512x512 -rp ./ gomoku





#ROM_START( gomoku )
#	ROM_REGION( 0x10000, "maincpu", 0 ) // program
#	ROM_LOAD( "rj_1.7a",    0x0000, 0x1000, CRC(ed20d539) SHA1(7cbbc678cbe5c85b914ca44f82bdbd452cf694a0) )
#	ROM_LOAD( "rj_2.7c",    0x1000, 0x1000, CRC(26a28516) SHA1(53d5d134cd91020fa06e380d355deb1df6b9cb6e) )
#	ROM_LOAD( "rj_3.7d",    0x2000, 0x1000, CRC(d05db072) SHA1(9697c932c6dcee6f8536c9f0b3c84a719a7d3dee) )
#	ROM_LOAD( "rj_4.7f",    0x3000, 0x1000, CRC(6e3d1c18) SHA1(e2f7e4c0de3c78d1b8e686152458972f996b023a) )
#	ROM_LOAD( "rj_5.4e",    0x4000, 0x0800, CRC(eaf541b4) SHA1(bc7e7ec1ba68f71ab9ac86f9ae77971ddb9ce3a4) )
#
#
#	ROM_REGION( 0x1000, "sound", 0 )   // sound
#	ROM_LOAD( "rj_7.3c",    0x0000, 0x1000, CRC(d1ed1365) SHA1(4ef08f26fe7df4c400f72e09e56d8825d584f55f) )
#

#	ROM_REGION( 0x1000, "gfx1", 0 ) // text char
#	ROM_LOAD( "rj_6.4r",    0x0000, 0x1000, CRC(ed26ae36) SHA1(61cb73d7f2568e88e1c2981e7af3e9a3b26797d3) )


#	ROM_REGION( 0x0040, "proms", 0 )
#	ROM_LOAD( "rj_prom.1m", 0x0000, 0x0020, CRC(5da2f2bd) SHA1(4355ccf06cb09ec3240dc92bda19b1f707a010ef) )  // TEXT color
#	ROM_LOAD( "rj_prom.1l", 0x0020, 0x0020, CRC(fe4ef393) SHA1(d4c63f8645afeadd13ff82087bcc497d8936d90b) )  // BG color
#	ROM_REGION( 0x0100, "user1", 0 )    // BG draw data X
#	ROM_LOAD( "rj_prom.8n", 0x0000, 0x0100, CRC(9ba43222) SHA1(a443df49d7ee9dbfd258b09731d392bf1249cbfa) )
#	ROM_REGION( 0x0100, "user2", 0 )    // BG draw data Y
#	ROM_LOAD( "rj_prom.7p", 0x0000, 0x0100, CRC(5b5464f8) SHA1(b945efb8a7233f501d67f6b1be4e9d4967dc6719) )
#	ROM_REGION( 0x0100, "user3", 0 )    // BG character data
#	ROM_LOAD( "rj_prom.7r", 0x0000, 0x0100, CRC(3004585a) SHA1(711b68140827f0f3dc71f2576fcf9b905c999e8d) )
#	ROM_REGION( 0x0020, "user4", 0 )    // unknown
#	ROM_LOAD( "rj_prom.9k", 0x0000, 0x0020, CRC(cff72923) SHA1(4f61375028ab62da46ed119bc81052f5f98c28d4) )
#ROM_END







