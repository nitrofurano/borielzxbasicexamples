	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$CFFF
#line 1
	ld hl, 0
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_eee)
	add hl, hl
	ex de, hl
	ld hl, __LABEL__palette02
	add hl, de
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	push hl
	ld hl, (_eee)
	push hl
	call _pbactionpalette
	ld hl, (_eee)
	add hl, hl
	ex de, hl
	ld hl, __LABEL__palette02
	add hl, de
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	push hl
	ld hl, (_eee)
	ld de, 128
	add hl, de
	push hl
	call _pbactionpalette
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 15
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
__LABEL__loopmain:
	ld hl, 1024
	push hl
	ld hl, 96
	push hl
	ld hl, 55296
	push hl
	call _pacmanfillram
	ld hl, 1024
	push hl
	ld hl, 0
	push hl
	ld hl, 56320
	push hl
	call _pacmanfillram
	ld hl, 1
	push hl
	xor a
	push af
	ld hl, __LABEL__text01end - __LABEL__text01
	push hl
	ld hl, __LABEL__text01
	push hl
	ld hl, 25
	push hl
	ld hl, 3
	push hl
	call _pbactionwritetext
	ld hl, 1
	push hl
	xor a
	push af
	ld hl, __LABEL__text02end - __LABEL__text02
	push hl
	ld hl, __LABEL__text02
	push hl
	ld hl, 26
	push hl
	ld hl, 3
	push hl
	call _pbactionwritetext
	ld hl, 1
	push hl
	xor a
	push af
	ld hl, __LABEL__text03end - __LABEL__text03
	push hl
	ld hl, __LABEL__text03
	push hl
	ld hl, 28
	push hl
	ld hl, 3
	push hl
	call _pbactionwritetext
__LABEL__looptitlekbchk:
	ld a, (58880)
	ld (_keyband), a
	sub 1
	jp nc, __LABEL6
	jp __LABEL__looptitlekbchk
__LABEL6:
	xor a
	ld (_gameover), a
	ld hl, 1024
	push hl
	ld hl, 96
	push hl
	ld hl, 55296
	push hl
	call _pacmanfillram
	ld hl, 1024
	push hl
	ld hl, 0
	push hl
	ld hl, 56320
	push hl
	call _pacmanfillram
	xor a
	ld (_colisn), a
	ld hl, 12
	ld (_snklndc), hl
	xor a
	ld (_ccur), a
	xor a
	ld (_colisn), a
	ld hl, 8
	ld (_snkln), hl
	ld hl, 12
	ld (_snklndc), hl
	ld hl, 0
	ld (_rompos), hl
	ld a, 12
	ld (_xpos), a
	ld a, 14
	ld (_ypos), a
	ld a, 1
	ld (_xdir), a
	xor a
	ld (_ydir), a
	ld hl, (_eesd)
	call _smsrnd
	ld (_eesd), hl
	ld de, 26
	call __MODU16
	inc hl
	ld (_xntz), hl
	ld hl, (_eesd)
	call _smsrnd
	ld (_eesd), hl
	ld de, 30
	call __MODU16
	inc hl
	ld (_yntz), hl
__LABEL__loopgame:
	ld a, (58880)
	ld (_keybbf), a
	ld h, 1
	ld a, (_keybbf)
	and h
	push af
	ld a, (_ydir)
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL8
	ld a, 1
	ld (_xdir), a
	xor a
	ld (_ydir), a
__LABEL8:
	ld h, 4
	ld a, (_keybbf)
	and h
	push af
	ld a, (_ydir)
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL10
	ld a, 255
	ld (_xdir), a
	xor a
	ld (_ydir), a
__LABEL10:
	ld h, 8
	ld a, (_keybbf)
	and h
	push af
	ld a, (_xdir)
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL12
	xor a
	ld (_xdir), a
	ld a, 1
	ld (_ydir), a
__LABEL12:
	ld h, 16
	ld a, (_keybbf)
	and h
	push af
	ld a, (_xdir)
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL14
	xor a
	ld (_xdir), a
	ld a, 255
	ld (_ydir), a
__LABEL14:
	ld a, (_keybbf)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_eesd)
	call __BXOR16
	call _smsrnd
	ld (_eesd), hl
	ld hl, (_xpos - 1)
	ld a, (_xdir)
	add a, h
	ld (_xpos), a
	ld hl, (_ypos - 1)
	ld a, (_ydir)
	add a, h
	ld (_ypos), a
	ld a, (_xpos)
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	ld hl, (_xntz)
	ex de, hl
	pop hl
	call __EQ16
	push af
	ld a, (_ypos)
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	ld hl, (_yntz)
	ex de, hl
	pop hl
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL16
	ld hl, (_eesd)
	call _smsrnd
	ld (_eesd), hl
	ld de, 26
	call __MODU16
	inc hl
	ld (_xntz), hl
	ld hl, (_eesd)
	call _smsrnd
	ld (_eesd), hl
	ld de, 30
	call __MODU16
	inc hl
	ld (_yntz), hl
	ld hl, (_snkln)
	inc hl
	ld (_snkln), hl
__LABEL16:
	ld h, 0
	ld a, (_ypos)
	call __LTI8
	or a
	jp z, __LABEL18
	ld a, 31
	ld (_ypos), a
__LABEL18:
	ld h, 0
	ld a, (_xpos)
	call __LTI8
	or a
	jp z, __LABEL20
	ld a, 27
	ld (_xpos), a
__LABEL20:
	ld a, 31
	ld hl, (_ypos - 1)
	call __LTI8
	or a
	jp z, __LABEL22
	xor a
	ld (_ypos), a
__LABEL22:
	ld a, 27
	ld hl, (_xpos - 1)
	call __LTI8
	or a
	jp z, __LABEL24
	xor a
	ld (_xpos), a
__LABEL24:
	ld hl, 51965
	ld (_eelp), hl
	jp __LABEL25
__LABEL28:
	ld hl, (_eelp)
	inc hl
	inc hl
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	ex de, hl
	ld hl, (_eelp)
	ld (hl), e
	inc hl
	ld (hl), d
__LABEL29:
	ld hl, (_eelp)
	inc hl
	inc hl
	ld (_eelp), hl
__LABEL25:
	ld hl, 52989
	ld de, (_eelp)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
	ld a, (_xpos)
	ld (52990), a
	ld a, (_ypos)
	ld (52991), a
	ld a, (_ccur)
	inc a
	push af
	ld h, 15
	pop af
	and h
	ld (_ccur), a
	ld hl, 1
	push hl
	xor a
	push af
	ld a, 32
	push af
	ld hl, (_snkln)
	add hl, hl
	ex de, hl
	ld hl, 52991
	or a
	sbc hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	push hl
	ld hl, (_snkln)
	add hl, hl
	ex de, hl
	ld hl, 52990
	or a
	sbc hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	add a, 2
	ld l, a
	ld h, 0
	push hl
	call _pbactionputchar
	ld hl, 1
	push hl
	xor a
	push af
	ld a, 32
	push af
	ld hl, (_yntz)
	push hl
	ld hl, (_xntz)
	inc hl
	inc hl
	push hl
	call _pbactionputchar
	ld a, (_ypos)
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	ld a, (_xpos)
	add a, 2
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	call _peekmap
	push af
	ld h, 32
	pop af
	cp h
	jp nc, __LABEL31
	ld a, 1
	ld (_gameover), a
__LABEL31:
	ld hl, 1
	push hl
	xor a
	push af
	ld a, (_ccur)
	add a, 97
	push af
	ld hl, (_yntz)
	push hl
	ld hl, (_xntz)
	inc hl
	inc hl
	push hl
	call _pbactionputchar
	ld hl, 1
	push hl
	xor a
	push af
	ld a, (_ccur)
	add a, 16
	push af
	ld a, (_ypos)
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	ld a, (_xpos)
	add a, 2
	ld l, a
	add a, a
	sbc a, a
	ld h, a
	push hl
	call _pbactionputchar
	ld hl, 2000
	push hl
	call _pacmandelay
	ld a, (_gameover)
	sub 1
	jp nc, __LABEL33
	jp __LABEL__loopgame
__LABEL33:
	ld hl, 1
	ld (_ee1x), hl
	jp __LABEL34
__LABEL37:
	ld hl, 2000
	push hl
	call _pacmandelay
__LABEL38:
	ld hl, (_ee1x)
	inc hl
	ld (_ee1x), hl
__LABEL34:
	ld hl, 50
	ld de, (_ee1x)
	or a
	sbc hl, de
	jp nc, __LABEL37
__LABEL36:
	jp __LABEL__loopmain
__LABEL39:
	jp __LABEL39
__LABEL40:
__LABEL__palette02:
#line 277
		defw $BBB,$422,$B96,$B99
		defw $200,$442,$444,$644
		defw $942,$944,$664,$966
		defw $B64,$DB9,$555,$AAA
#line 281
__LABEL__text01:
#line 287
		defb " JUST ANOTHER SNAKE GAME "
#line 288
__LABEL__text01end:
__LABEL__text02:
#line 292
		defb " PAULO SILVA, '13-'15 "
#line 293
__LABEL__text02end:
__LABEL__text03:
#line 297
		defb " PUSH ANY KEY "
#line 298
__LABEL__text03end:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_pacmandelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
		push bc
pacmandelayloop:
		dec bc
		ld a,b
		or c
		jp nz,pacmandelayloop
		pop bc
#line 10
_pacmandelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_pacmanfillram:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld l, (ix+4)
		ld h, (ix+5)
		ld b, (ix+9)
		ld c, (ix+8)
pacmanfillramloop:
		ld a, (ix+6)
		ld (hl),a
		dec bc
		inc hl
		ld a,b
		or c
		jp nz,pacmanfillramloop
#line 13
_pacmanfillram__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pacmanldir:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d, (ix+5)
		ld e, (ix+4)
		ld h, (ix+7)
		ld l, (ix+6)
		ld b, (ix+9)
		ld c, (ix+8)
		ldir
#line 8
_pacmanldir__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57344
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57345
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57346
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57347
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_pbactionputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionpalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 256
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	call __BOR16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58369
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_pbactionpalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionpalettepeekuinteger:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58369
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 256
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	call __BOR16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_pbactionpalettepeekuinteger__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 54272
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_pbactionputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionwritetext:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_pbactionwritetext_twrt2), hl
	jp __LABEL41
__LABEL44:
	ld l, (ix+14)
	ld h, (ix+15)
	push hl
	ld a, (ix+13)
	push af
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	ld hl, (_pbactionwritetext_twrt2)
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, (_pbactionwritetext_twrt2)
	add hl, de
	push hl
	call _pbactionputchar
__LABEL45:
	ld hl, (_pbactionwritetext_twrt2)
	inc hl
	ld (_pbactionwritetext_twrt2), hl
__LABEL41:
	ld l, (ix+10)
	ld h, (ix+11)
	dec hl
	ld de, (_pbactionwritetext_twrt2)
	or a
	sbc hl, de
	jp nc, __LABEL44
__LABEL43:
_pbactionwritetext__leave:
	exx
	ld hl, 12
__EXIT_FUNCTION:
	ld sp, ix
	pop ix
	pop de
	add hl, sp
	ld sp, hl
	push de
	exx
	ret
_peekmap:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ld de, 55296
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	ld b, h
	ld c, l
	ld a, (bc)
_peekmap__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 968 "justanothersnakegame.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 969 "justanothersnakegame.zxb"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 970 "justanothersnakegame.zxb"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 971 "justanothersnakegame.zxb"
#line 1 "lti8.asm"
	
__LTI8: ; Test 8 bit values A < H
        ; Returns result in A: 0 = False, !0 = True
	        sub h
	
__LTI:  ; Signed CMP
	        PROC
	        LOCAL __PE
	
	        ld a, 0  ; Sets default to false
__LTI2:
	        jp pe, __PE
	        ; Overflow flag NOT set
	        ret p
	        dec a ; TRUE
	
__PE:   ; Overflow set
	        ret m
	        dec a ; TRUE
	        ret
	        
	        ENDP
#line 972 "justanothersnakegame.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 973 "justanothersnakegame.zxb"
#line 1 "eq16.asm"
__EQ16:	; Test if 16bit values HL == DE
		; Returns result in A: 0 = False, FF = True
			or a	; Reset carry flag
			sbc hl, de 
	
			ld a, h
			or l
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 974 "justanothersnakegame.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 975 "justanothersnakegame.zxb"
	
ZXBASIC_USER_DATA:
	_ee1x EQU 49232
	_ee1y EQU 49234
	_ee1q EQU 49236
	_eelp EQU 49238
	_rompos EQU 49240
	_snkln EQU 49242
	_snklndc EQU 49244
	_eesd EQU 49246
	_xntz EQU 49248
	_yntz EQU 49250
	_keybbf EQU 49252
	_gameover EQU 49253
	_xdir EQU 49254
	_ydir EQU 49255
	_xpos EQU 49256
	_ypos EQU 49257
	_ccur EQU 49258
	_colisn EQU 49259
	_keyband EQU 49260
	_i EQU 49168
	_eee EQU 49170
	_seed EQU 49172
	_ee1 EQU 49186
	_ee2 EQU 49174
	_ee3 EQU 49176
	_ee4 EQU 49178
	_ecsp EQU 49180
	_ecsq EQU 49182
	_ej0 EQU 49184
	_ex2 EQU 49192
	_ey2 EQU 49194
	_ex3 EQU 49196
	_ey3 EQU 49198
	_ex0 EQU 49200
	_ey0 EQU 49202
	_ex1 EQU 49204
	_ey1 EQU 49206
	_ex6 EQU 49208
	_ey6 EQU 49210
	_ex7 EQU 49212
	_ey7 EQU 49214
	_ex4 EQU 49216
	_ey4 EQU 49218
	_ex5 EQU 49220
	_ey5 EQU 49222
	_zzz EQU 49224
	_pbactionwritetext_twrt2 EQU 49152
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
