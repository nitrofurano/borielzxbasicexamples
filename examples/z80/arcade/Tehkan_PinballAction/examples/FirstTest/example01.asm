	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$CFFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_eee)
	add hl, hl
	ex de, hl
	ld hl, __LABEL__palettetest01z1
	add hl, de
	ld a, (hl)
	inc hl
	ld h, (hl)
	ld l, a
	push hl
	ld hl, (_eee)
	push hl
	call _pbactionpalettepeekuinteger
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 255
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 1
	ld (_eee), hl
	jp __LABEL5
__LABEL8:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
__LABEL9:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL5:
	ld hl, 10
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL10
__LABEL13:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _pbactionpalettepeekuinteger
__LABEL14:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL10:
	ld hl, 255
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
	ld hl, 2710
	push hl
	ld hl, 1
	push hl
	call _pbactionpalette
	ld hl, 342
	push hl
	ld hl, 128
	push hl
	call _pbactionpalette
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 10
	push af
	ld a, 21
	push af
	xor a
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 30
	push af
	ld a, 21
	push af
	ld a, 1
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 50
	push af
	ld a, 21
	push af
	ld a, 2
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 70
	push af
	ld a, 21
	push af
	ld a, 3
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 90
	push af
	ld a, 21
	push af
	ld a, 4
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 110
	push af
	ld a, 21
	push af
	ld a, 5
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 130
	push af
	ld a, 21
	push af
	ld a, 6
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld a, 150
	push af
	ld a, 21
	push af
	ld a, 7
	push af
	call _pbactionputsprite
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL20
__LABEL23:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, 1
	push hl
	ld de, 240
	ld hl, (_seed)
	call __BAND16
	ld de, 16
	call __DIVU16
	ld a, l
	push af
	ld de, 7
	ld hl, (_seed)
	call __BAND16
	ld a, l
	push af
	ld hl, (_ee3)
	push hl
	ld hl, (_ee2)
	push hl
	call _pbactionputchar
__LABEL24:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL20:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL15:
	ld hl, 31
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL25
__LABEL28:
	ld hl, 0
	push hl
	xor a
	push af
	ld de, __LABEL__text01
	ld hl, (_ee2)
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld hl, 13
	push hl
	ld hl, (_ee2)
	ld de, 13
	add hl, de
	push hl
	call _pbactionputchar
	ld hl, 1
	push hl
	xor a
	push af
	xor a
	push af
	ld hl, 13
	push hl
	ld hl, (_ee2)
	ld de, 13
	add hl, de
	push hl
	call _pbactionputchar
__LABEL29:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL25:
	ld hl, 11
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL30
__LABEL33:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL35
__LABEL38:
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ee3)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee3)
	ld de, 15
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 13
	add hl, de
	push hl
	call _pbactionputchar
	ld hl, 1
	push hl
	xor a
	push af
	xor a
	push af
	ld hl, (_ee3)
	ld de, 15
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 13
	add hl, de
	push hl
	call _pbactionputchar
__LABEL39:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL35:
	ld hl, 15
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL38
__LABEL37:
__LABEL34:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL30:
	ld hl, 15
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
	ld hl, 20
	ld (_ex0), hl
	ld hl, 10
	ld (_ey0), hl
	ld hl, 20
	ld (_ex1), hl
	ld hl, 30
	ld (_ey1), hl
	ld hl, 20
	ld (_ex2), hl
	ld hl, 50
	ld (_ey2), hl
	ld hl, 20
	ld (_ex3), hl
	ld hl, 70
	ld (_ey3), hl
	ld hl, 20
	ld (_ex4), hl
	ld hl, 90
	ld (_ey4), hl
	ld hl, 20
	ld (_ex5), hl
	ld hl, 110
	ld (_ey5), hl
	ld hl, 20
	ld (_ex6), hl
	ld hl, 130
	ld (_ey6), hl
	ld hl, 20
	ld (_ex7), hl
	ld hl, 150
	ld (_ey7), hl
	ld hl, 0
	ld (_eee), hl
	xor a
	ld (_ecsp), a
	xor a
	ld (_ecsq), a
__LABEL40:
	ld a, 255
	ld (20672), a
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ex0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 22
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ex0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 22
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ex0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 22
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ey0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 23
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ey0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 23
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ey0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 23
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 24
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 24
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 24
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 1000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 24
	push hl
	ld hl, 5
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_eee)
	ld de, 10000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 24
	push hl
	ld hl, 4
	push hl
	call _pbactionputchar
	ld a, (58880)
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 25
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld a, (_ecsp)
	sub 1
	jp nc, __LABEL43
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex0)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex0), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey0)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey0), hl
__LABEL43:
	ld a, (_ecsp)
	dec a
	sub 1
	jp nc, __LABEL45
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex1)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex1), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey1)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey1), hl
__LABEL45:
	ld a, (_ecsp)
	sub 2
	sub 1
	jp nc, __LABEL47
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex2)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex2), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey2)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey2), hl
__LABEL47:
	ld a, (_ecsp)
	sub 3
	sub 1
	jp nc, __LABEL49
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex3)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex3), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey3)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey3), hl
__LABEL49:
	ld a, (_ecsp)
	sub 4
	sub 1
	jp nc, __LABEL51
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex4)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex4), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey4)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey4), hl
__LABEL51:
	ld a, (_ecsp)
	sub 5
	sub 1
	jp nc, __LABEL53
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex5)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex5), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey5)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey5), hl
__LABEL53:
	ld a, (_ecsp)
	sub 6
	sub 1
	jp nc, __LABEL55
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex6)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex6), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey6)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey6), hl
__LABEL55:
	ld a, (_ecsp)
	sub 7
	sub 1
	jp nc, __LABEL57
	ld de, 4
	ld hl, (_ej0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ex7)
	add hl, de
	push hl
	ld de, 1
	ld hl, (_ej0)
	call __BAND16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex7), hl
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey7)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ej0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey7), hl
__LABEL57:
	ld a, (58881)
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 26
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld de, 15
	ld hl, (_ej0)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL59
	ld a, (_ecsq)
	inc a
	ld (_ecsq), a
__LABEL59:
	ld a, (_ecsq)
	ld h, 4
	call __DIVU8_FAST
	ld l, a
	ld h, 0
	ld e, h
	ld d, h
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __SWAP32
	call __MODI32
	ld a, l
	ld (_ecsp), a
	ld a, (58882)
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 27
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 27
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 27
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld a, (58883)
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 28
	push hl
	ld hl, 8
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 28
	push hl
	ld hl, 7
	push hl
	call _pbactionputchar
	ld hl, 0
	push hl
	xor a
	push af
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 28
	push hl
	ld hl, 6
	push hl
	call _pbactionputchar
	ld a, (_ecsp)
	ld h, 10
	call __MODU8_FAST
	add a, 48
	ld (54013), a
	ld hl, 0
	push hl
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 12
	push hl
	call _pbactionputchar
	ld hl, 1
	push hl
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 12
	push hl
	call _pbactionputchar
	ld hl, (_eee)
	ld de, 4
	call __DIVU16
	ld de, 0
	ld (_ee3), hl
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	xor a
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ey1)
	ld a, l
	push af
	ld hl, (_ex1)
	ld a, l
	push af
	ld a, 1
	push af
	call _pbactionputsprite
	ld hl, (_ee3)
	ld a, l
	push af
	ld a, 4
	push af
	ld hl, (_ey2)
	ld a, l
	push af
	ld hl, (_ex2)
	ld a, l
	push af
	ld a, 2
	push af
	call _pbactionputsprite
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ey3)
	ld a, l
	push af
	ld hl, (_ex3)
	ld a, l
	push af
	ld a, 3
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld a, 4
	push af
	ld hl, (_ey4)
	ld a, l
	push af
	ld hl, (_ex4)
	ld a, l
	push af
	ld a, 4
	push af
	call _pbactionputsprite
	ld a, 3
	push af
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ey5)
	ld a, l
	push af
	ld hl, (_ex5)
	ld a, l
	push af
	ld a, 5
	push af
	call _pbactionputsprite
	ld hl, (_ee3)
	ld a, l
	push af
	ld a, 4
	push af
	ld hl, (_ey6)
	ld a, l
	push af
	ld hl, (_ex6)
	ld a, l
	push af
	ld a, 6
	push af
	call _pbactionputsprite
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ee3)
	ld a, l
	push af
	ld hl, (_ey7)
	ld a, l
	push af
	ld hl, (_ex7)
	ld a, l
	push af
	ld a, 7
	push af
	call _pbactionputsprite
	ld hl, 0
	ld (_zzz), hl
	jp __LABEL60
__LABEL63:
__LABEL64:
	ld hl, (_zzz)
	inc hl
	ld (_zzz), hl
__LABEL60:
	ld hl, 1000
	ld de, (_zzz)
	or a
	sbc hl, de
	jp nc, __LABEL63
__LABEL62:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL40
__LABEL41:
__LABEL65:
	jp __LABEL65
__LABEL66:
__LABEL__text01:
#line 252
		defb "HELLO WORLD!!!!!"
#line 253
	jp __LABEL__palettetest01z1end
__LABEL__palettetest01z1:
#line 260
		defw $000,$00A,$A00,$A0A,$0A0,$0AA,$AA0,$AAA
		defw $000,$00F,$F00,$F0F,$0F0,$0FF,$FF0,$FFF
		defw $000,$010,$020,$030,$040,$050,$060,$070
		defw $080,$090,$0A0,$0B0,$0C0,$0D0,$0E0,$0F0
		defw $000,$100,$200,$300,$400,$500,$600,$700
		defw $800,$900,$A00,$B00,$C00,$D00,$E00,$F00
		defw $FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF
		defw $EDC,$999,$AAA,$BBB,$CCC,$DDD,$EEE,$FFF
		defw $FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF
		defw $008,$009,$00A,$00B,$00C,$00D,$00E,$00F
		defw $000,$010,$020,$030,$040,$050,$060,$070
		defw $080,$090,$0A0,$0B0,$0C0,$0D0,$0E0,$0F0
		defw $000,$100,$200,$300,$400,$500,$600,$700
		defw $800,$900,$A00,$B00,$C00,$D00,$E00,$F00
		defw $000,$111,$222,$333,$444,$555,$666,$777
		defw $888,$999,$AAA,$BBB,$CCC,$DDD,$EEE,$FFF
		defw $48C,$00A,$A00,$A0A,$0A0,$0AA,$AA0,$AAA
		defw $000,$00F,$F00,$F0F,$0F0,$0FF,$FF0,$FFF
		defw $000,$010,$020,$030,$040,$050,$060,$070
		defw $080,$090,$0A0,$0B0,$0C0,$0D0,$0E0,$0F0
		defw $000,$100,$200,$300,$400,$500,$600,$700
		defw $800,$900,$A00,$B00,$C00,$D00,$E00,$F00
		defw $000,$111,$222,$333,$444,$555,$666,$777
		defw $888,$999,$AAA,$BBB,$CCC,$DDD,$EEE,$FFF
		defw $FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF,$FFF
		defw $DCE,$009,$00A,$00B,$00C,$00D,$00E,$00F
		defw $000,$010,$020,$030,$040,$050,$060,$070
		defw $080,$090,$0A0,$0B0,$0C0,$0D0,$0E0,$0F0
		defw $000,$100,$200,$300,$400,$500,$600,$700
		defw $800,$900,$A00,$B00,$C00,$D00,$E00,$F00
		defw $000,$111,$222,$333,$444,$555,$666,$777
		defw $888,$999,$AAA,$BBB,$CCC,$DDD,$EEE,$FFF
#line 292
__LABEL__palettetest01z1end:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_pacmandelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
		push bc
pacmandelayloop:
		dec bc
		ld a,b
		or c
		jp nz,pacmandelayloop
		pop bc
#line 10
_pacmandelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_pbactionputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57344
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57345
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57346
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 31
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 57347
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_pbactionputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionpalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 256
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	call __BOR16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58369
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_pbactionpalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionpalettepeekuinteger:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58369
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 3840
	pop hl
	call __BAND16
	ld de, 256
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 240
	pop hl
	call __BAND16
	ex de, hl
	pop hl
	call __BOR16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	ld de, 58368
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_pbactionpalettepeekuinteger__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_pbactionputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+12)
	ld h, (ix+13)
	ld de, 2048
	call __MUL16_FAST
	ld de, 54272
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_pbactionputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_zpoke:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
_zpoke__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_putchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_putchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1952 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1953 "example01.zxb"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 1954 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1955 "example01.zxb"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1956 "example01.zxb"
#line 1 "div8.asm"
				; --------------------------------
__DIVU8:	; 8 bit unsigned integer division 
				; Divides (Top of stack, High Byte) / A
		pop hl	; --------------------------------
		ex (sp), hl	; CALLEE
	
__DIVU8_FAST:	; Does A / H
		ld l, h
		ld h, a		; At this point do H / L
	
		ld b, 8
		xor a		; A = 0, Carry Flag = 0
		
__DIV8LOOP:
		sla	h		
		rla			
		cp	l		
		jr	c, __DIV8NOSUB
		sub	l		
		inc	h		
	
__DIV8NOSUB:	
		djnz __DIV8LOOP
	
		ld	l, a		; save remainder
		ld	a, h		; 
		
		ret			; a = Quotient, 
	
	
					; --------------------------------
__DIVI8:		; 8 bit signed integer division Divides (Top of stack) / A
		pop hl		; --------------------------------
		ex (sp), hl
	
__DIVI8_FAST:
		ld e, a		; store operands for later
		ld c, h
	
		or a		; negative?
		jp p, __DIV8A
		neg			; Make it positive
	
__DIV8A:
		ex af, af'
		ld a, h
		or a
		jp p, __DIV8B
		neg
		ld h, a		; make it positive
	
__DIV8B:
		ex af, af'
	
		call __DIVU8_FAST
	
		ld a, c
		xor l		; bit 7 of A = 1 if result is negative
	
		ld a, h		; Quotient
		ret p		; return if positive	
	
		neg
		ret
		
	
__MODU8:		; 8 bit module. REturns A mod (Top of stack) (unsigned operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODU8_FAST:	; __FASTCALL__ entry
		call __DIVU8_FAST
		ld a, l		; Remainder
	
		ret		; a = Modulus
	
	
__MODI8:		; 8 bit module. REturns A mod (Top of stack) (For singed operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODI8_FAST:	; __FASTCALL__ entry
		call __DIVI8_FAST
		ld a, l		; remainder
	
		ret		; a = Modulus
	
#line 1957 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1958 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1959 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1960 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_i EQU 49168
	_eee EQU 49170
	_seed EQU 49172
	_ee1 EQU 49186
	_ee2 EQU 49174
	_ee3 EQU 49176
	_ee4 EQU 49178
	_ecsp EQU 49180
	_ecsq EQU 49182
	_ej0 EQU 49184
	_ex2 EQU 49192
	_ey2 EQU 49194
	_ex3 EQU 49196
	_ey3 EQU 49198
	_ex0 EQU 49200
	_ey0 EQU 49202
	_ex1 EQU 49204
	_ey1 EQU 49206
	_ex6 EQU 49208
	_ey6 EQU 49210
	_ex7 EQU 49212
	_ey7 EQU 49214
	_ex4 EQU 49216
	_ey4 EQU 49218
	_ex5 EQU 49220
	_ey5 EQU 49222
	_zzz EQU 49224
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
