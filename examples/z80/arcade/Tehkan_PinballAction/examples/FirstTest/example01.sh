# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm pbaction.zip
rm example01.asm
rm pbaction/*
rmdir pbaction
mkdir pbaction

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=pbaction/b-p7.bin
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=pbaction/b-n7.bin
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=pbaction/b-l7.bin
rm example01.bin

dd bs=$((0x2000)) count=1 if=/dev/zero of=pbaction/a-e3.bin

zxb.py library/b3r1f0_charmapnunchacken01.zxi --org=$((0x0000)) #fgchars
cat dummy64k.bin >> b3r1f0_charmapnunchacken01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=b3r1f0_charmapnunchacken01b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=pbaction/a-s6.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmap01b.bin of=pbaction/a-s7.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=charmap01b.bin of=pbaction/a-s8.bin
xxd -p -c 3 -s 0 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-s6.bin
xxd -p -c 3 -s 1 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-s7.bin
xxd -p -c 3 -s 2 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-s8.bin
#rm charmap01.bin charmap01b.bin
#zxb.py library/charmap01.bas --org=$((0x0000)) #sprites
#cat dummy64k.bin >> charmap01.bin
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=pbaction/b-c7.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmap01b.bin of=pbaction/b-d7.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=charmap01b.bin of=pbaction/b-f7.bin
xxd -p -c 3 -s 0 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/b-c7.bin
xxd -p -c 3 -s 1 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/b-d7.bin
xxd -p -c 3 -s 2 -l $((0x6000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > pbaction/b-f7.bin
rm b3r1f0_charmapnunchacken01.bin b3r1f0_charmapnunchacken01b.bin




zxb.py library/b4r1f0_charmappinballactionbg01.zxi --org=$((0x0000)) #bgchars
cat dummy64k.bin >> b4r1f0_charmappinballactionbg01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r1f0_charmappinballactionbg01.bin of=b4r1f0_charmappinballactionbg01b.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=b4r1f0_charmappinballactionbg01b.bin of=pbaction/a-j5.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=b4r1f0_charmappinballactionbg01b.bin of=pbaction/a-j6.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=b4r1f0_charmappinballactionbg01b.bin of=pbaction/a-j7.bin
#dd ibs=1 count=$((0x4000)) skip=$((0xC000)) if=b4r1f0_charmappinballactionbg01b.bin of=pbaction/a-j8.bin

xxd -p -c 4 -s 0 -l $((0x10000)) b4r1f0_charmappinballactionbg01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-j5.bin
xxd -p -c 4 -s 1 -l $((0x10000)) b4r1f0_charmappinballactionbg01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-j6.bin
xxd -p -c 4 -s 2 -l $((0x10000)) b4r1f0_charmappinballactionbg01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-j7.bin
xxd -p -c 4 -s 3 -l $((0x10000)) b4r1f0_charmappinballactionbg01b.bin | cut -b 1-2 | xxd -r -p > pbaction/a-j8.bin

rm b4r1f0_charmappinballactionbg01.bin b4r1f0_charmappinballactionbg01b.bin





rm smsboot.bin dummy64k.bin

zip -r pbaction pbaction
rm pbaction/*
rmdir pbaction

mame -w -video soft -resolution0 512x512 -rp ./ pbaction
#xmame.SDL -rp ./ pbaction

