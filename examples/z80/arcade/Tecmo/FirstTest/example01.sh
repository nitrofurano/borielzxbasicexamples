# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm rygar.zip
rm rygar/*
rmdir rygar
mkdir rygar

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=rygar/5.5p
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=rygar/cpu_5m.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=rygar/cpu_5j.bin
rm example01.bin

#sound
dd bs=$((0x2000)) count=1 if=/dev/zero of=rygar/cpu_4h.bin

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=rygar/cpu_8k.bin
#gfx2
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=rygar/vid_6k.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0001)) if=charmap01b.bin of=rygar/vid_6j.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0002)) if=charmap01b.bin of=rygar/vid_6h.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0003)) if=charmap01b.bin of=rygar/vid_6g.bin
#gfx3
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=rygar/vid_6p.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0001)) if=charmap01b.bin of=rygar/vid_6o.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0002)) if=charmap01b.bin of=rygar/vid_6n.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0003)) if=charmap01b.bin of=rygar/vid_6l.bin
#gfx4
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmap01b.bin of=rygar/vid_6f.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0001)) if=charmap01b.bin of=rygar/vid_6e.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0002)) if=charmap01b.bin of=rygar/vid_6c.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0003)) if=charmap01b.bin of=rygar/vid_6b.bin
rm charmap01b.bin

#samples
dd bs=$((0x4000)) count=1 if=/dev/zero of=rygar/cpu_1f.bin

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=rygar/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=rygar/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=rygar/rygar.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=rygar/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=rygar/82s126.3m

rm smsboot.bin dummy64k.bin

zip -r rygar rygar
rm rygar/*
rmdir rygar

mame -w -video soft -resolution0 512x512 -rp ./ rygar
#xmame.SDL -rp ./ rygar

