palette01:
'-----  rrrgggbb
asm


  ;- http://www.colourlovers.com/palette/1929913/lady_in_naature
  defb %00000100 ;#120
  defb %11101000 ;#E50
  defb %10101100 ;#A62
  defb %11111010 ;#ECA

  ;- http://www.colourlovers.com/palette/2269946/Williamsburg
  defb %01101110 ;#679
  defb %10010001 ;#986
  defb %01001001 ;#554
  defb %01100000 ;#600

  ;- http://www.colourlovers.com/palette/1484946/Spring
  defb %10110010 ;#B9A
  defb %11111000 ;#ED2
  defb %10111011 ;#BDF
  defb %10110100 ;#BB3

  ;- http://www.colourlovers.com/palette/888838/Grave_Weeping
  defb %01101101 ;#666
  defb %10010001 ;#997
  defb %01101110 ;#768
  defb %01001000 ;#443




  ;- primaries and halftones
  defb %00000000
  defb %00000010
  defb %10100000
  defb %10100010
  defb %00010100
  defb %00010110
  defb %10110100
  defb %10110110

  defb %00000000
  defb %00000011
  defb %11100000
  defb %11100011
  defb %00011100
  defb %00011111
  defb %11111100
  defb %11111111

  ;- black
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000

  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000

  end asm
