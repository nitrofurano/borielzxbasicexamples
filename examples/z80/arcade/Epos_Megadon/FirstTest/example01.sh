# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm megadon.zip example01.asm
rm megadon/*
rmdir megadon
mkdir megadon

dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=megadon/2732u10b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=megadon/2732u09b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=megadon/2732u08b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=megadon/2732u07b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=megadon/2732u06b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=megadon/2732u05b.bin
dd ibs=1 count=$((0x1000)) skip=$((0x6000)) if=example01.bin of=megadon/2732u04b.bin
dd ibs=1 count=$((0x0800)) skip=$((0x7000)) if=example01.bin of=megadon/2716u11b.bin
rm dummy64k.bin example01.bin smsboot.bin

#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "2732u10b.bin",   0x0000, 0x1000, CRC(af8fbe80) SHA1(2d7857616462112fe17343a9357ee51d8f965a0f) )
#- ROM_LOAD( "2732u09b.bin",   0x1000, 0x1000, CRC(097d1e73) SHA1(b6141155b2c63c33a367dd18fe53ff9f01b99380) )
#- ROM_LOAD( "2732u08b.bin",   0x2000, 0x1000, CRC(526784da) SHA1(7d9f43dc6975a018bec95982029ce7ac9f675869) )
#- ROM_LOAD( "2732u07b.bin",   0x3000, 0x1000, CRC(5b060910) SHA1(98a719bf0ffe8010437565de681aaefa647d9a6c) )
#- ROM_LOAD( "2732u06b.bin",   0x4000, 0x1000, CRC(8ac8af6d) SHA1(53c123f0e9f0443737c39c01dbdb685189cffa92) )
#- ROM_LOAD( "2732u05b.bin",   0x5000, 0x1000, CRC(052bb603) SHA1(eb74a9563f44cca50dc2c475e4a376ed14e4f56f) )
#- ROM_LOAD( "2732u04b.bin",   0x6000, 0x1000, CRC(9b8b7e92) SHA1(051ad9a8ba51740a865e3c95a738658b30bbbe60) )
#- ROM_LOAD( "2716u11b.bin",   0x7000, 0x0800, CRC(599b8b61) SHA1(e687c6f475a0fead3e47f05b1d1b3b29cf4a83a1) )



#proms
zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=megadon/74s288.bin
rm palette01.bin
#dd bs=$((0x20)) count=1 if=/dev/urandom of=megadon/74s288.bin

#- ROM_REGION( 0x0020, "proms", 0 )
#- ROM_LOAD( "74s288.bin",     0x0000, 0x0020, CRC(c779ea99) SHA1(7702ae3684579950b36274ea91d4267c96faeeb8) )





zip -r megadon megadon
rm megadon/*
rmdir megadon

mame -sound none -w -video soft -resolution0 512x512 -rp ./ megadon




