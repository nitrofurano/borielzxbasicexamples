rm example01.asm
rm ddayjlc.zip
rm ddayjlc/*
rmdir ddayjlc
mkdir ddayjlc
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=ddayjlc/1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=ddayjlc/2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=ddayjlc/3
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=ddayjlc/4
rm example01.bin

#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "1",  0x0000, 0x2000, CRC(dbfb8772) SHA1(1fbc9726d0cd1f8781ced2f8233107b65b9bdb1a) )
#	ROM_LOAD( "2",  0x2000, 0x2000, CRC(f40ea53e) SHA1(234ef686d3e9fe12aceada7098c4cc53e56eb1a3) )
#	ROM_LOAD( "3",  0x4000, 0x2000, CRC(0780ef60) SHA1(9247af38acbaea0f78892fc50081b2400abbdc1f) )
#	ROM_LOAD( "4",  0x6000, 0x2000, CRC(75991a24) SHA1(175f505da6eb80479a70181d6aed01130f6a64cc) )







#audio
dd bs=$((0x2000)) count=1 if=/dev/zero of=ddayjlc/11

#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "11", 0x0000, 0x2000, CRC(fe4de019) SHA1(16c5402e1a79756f8227d7e99dd94c5896c57444) )






#gfx
zxb.py library/b1r1f0_charmapkamikaze01.zxi --org=$((0x0000))
cat dummy64k.bin >> b1r1f0_charmapkamikaze01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=b1r1f0_charmapkamikaze01b.bin

#?????????????????
#gfx2
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/14
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/15
#gfx3
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/12
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/13
#dd bs=$((0x1000)) count=1 if=/dev/zero of=ddayjlc/14
#dd bs=$((0x1000)) count=1 if=/dev/zero of=ddayjlc/15
#dd bs=$((0x1000)) count=1 if=/dev/zero of=ddayjlc/12
#dd bs=$((0x1000)) count=1 if=/dev/zero of=ddayjlc/13

#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/16
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/17
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/18
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f0_charmapkamikaze01b.bin of=ddayjlc/19


rm b1r1f0_charmapkamikaze01.bin b1r1f0_charmapkamikaze01b.bin

#	ROM_REGION( 0x8000, "gfx1", 0 )
#	ROM_LOAD( "16", 0x0000, 0x2000, CRC(a167fe9a) SHA1(f2770d93ee5ae4eb9b3bcb052e14e36f53eec707) )
#	ROM_LOAD( "17", 0x2000, 0x2000, CRC(13ffe662) SHA1(2ea7855a14a4b8429751bae2e670e77608f93406) )
#	ROM_LOAD( "18", 0x4000, 0x2000, CRC(debe6531) SHA1(34b3b70a1872527266c664b2a82014d028a4ff1e) )
#	ROM_LOAD( "19", 0x6000, 0x2000, CRC(5816f947) SHA1(2236bed3e82980d3e7de3749aef0fbab042086e6) )
#	ROM_REGION( 0x2000, "gfx2", 0 )
#	ROM_LOAD( "14", 0x1000, 0x1000, CRC(2c0e9bbe) SHA1(e34ab774d2eb17ddf51af513dbcaa0c51f8dcbf7) )
#	ROM_LOAD( "15", 0x0000, 0x1000, CRC(a6eeaa50) SHA1(052cd3e906ca028e6f55d0caa1e1386482684cbf) )
#	ROM_REGION( 0x2000, "gfx3", 0 )
#	ROM_LOAD( "12", 0x1000, 0x1000, CRC(7f7afe80) SHA1(e8a549b8a8985c61d3ba452e348414146f2bc77e) )
#	ROM_LOAD( "13", 0x0000, 0x1000, CRC(f169b93f) SHA1(fb0617162542d688503fc6618dd430308e259455) )



#?????
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/5
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/6
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/7
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/8
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/9
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ddayjlc/10

#	ROM_REGION( 0xc0000, "user1", 0 )
#	ROM_LOAD( "5",  0x00000, 0x2000, CRC(299b05f2) SHA1(3c1804bccb514bada4bed68a6af08db63a8f1b19) )
#	ROM_LOAD( "6",  0x02000, 0x2000, CRC(38ae2616) SHA1(62c96f32532f0d7e2cf1606a303d81ebb4aada7d) )
#	ROM_LOAD( "7",  0x04000, 0x2000, CRC(4210f6ef) SHA1(525d8413afabf97cf1d04ee9a3c3d980b91bde65) )
#	ROM_LOAD( "8",  0x06000, 0x2000, CRC(91a32130) SHA1(cbcd673b47b672b9ce78c7354dacb5964a81db6f) )
#	ROM_LOAD( "9",  0x08000, 0x2000, CRC(ccb82f09) SHA1(37c23f13aa0728bae82dba9e2858a8d81fa8afa5) )
#	ROM_LOAD( "10", 0x0a000, 0x2000, CRC(5452aba1) SHA1(03ef47161d0ab047c8675d6ffd3b7acf81f74721) )






#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin

dd ibs=1 count=$((0x0200)) skip=$((0x0000)) if=palette01b.bin of=palette01b0.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0200)) if=palette01b.bin of=palette01b1.bin

xxd -p -c 2 -s 0 -l 512 palette01b0.bin | cut -b 1-2 | xxd -r -p > ddayjlc/4l.bin
xxd -p -c 2 -s 1 -l 512 palette01b0.bin | cut -b 1-2 | xxd -r -p > ddayjlc/4m.bin
xxd -p -c 2 -s 0 -l 512 palette01b1.bin | cut -b 1-2 | xxd -r -p > ddayjlc/5p.bin
xxd -p -c 2 -s 1 -l 512 palette01b1.bin | cut -b 1-2 | xxd -r -p > ddayjlc/5n.bin

#dd bs=$((0x100)) count=1 if=/dev/urandom of=ddayjlc/4l.bin #palette 0x00..0xFF - rgb?
#dd bs=$((0x100)) count=1 if=/dev/urandom of=ddayjlc/4m.bin #palette 0x00..0xFF - gb?
#dd bs=$((0x100)) count=1 if=/dev/urandom of=ddayjlc/5p.bin #palette 0x100..0x1FF - rgb?
#dd bs=$((0x100)) count=1 if=/dev/urandom of=ddayjlc/5n.bin #palette 0x100.0x1FF - gb?

dd bs=$((0x100)) count=1 if=/dev/zero of=ddayjlc/3l.bin #?

rm palette01.bin palette01b.bin palette01b0.bin palette01b1.bin

#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=ddayjlc/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=ddayjlc/hgb1.h7  #?sp
#rm clut01b.bin


#- zero - urandom

#proms



#	ROM_REGION( 0x0500, "proms", 0 )
#	ROM_LOAD( "4l.bin",  0x00000, 0x0100, CRC(2c3fa534) SHA1(e4c0d06cf62459c1835cb27a4e659b01ad4be20c) ) #palette 0x00..0xFF - rgb?
#	ROM_LOAD( "4m.bin",  0x00200, 0x0100, CRC(e0ab9a8f) SHA1(77010c4039f9d408f40cea079c1ef56132ddbd2b) ) #palette 0x00..0xFF - gb?
#	ROM_LOAD( "5p.bin",  0x00100, 0x0100, CRC(4fd96b26) SHA1(0fb9928ab6c4ee937cefcf82145a4c9d43ca8517) ) #palette 0x100..0x1FF - rgb?
#	ROM_LOAD( "5n.bin",  0x00300, 0x0100, CRC(61d85970) SHA1(189e9da3dade54936872b80893b1318e5fbfbe5e) ) #palette 0x100.0x1FF - gb?
#	ROM_LOAD( "3l.bin",  0x00400, 0x0100, CRC(da6fe846) SHA1(e8386cf7f552facf2d1a5b7b63ca3d2f1801d215) ) #?

#ROM_END










zip -r ddayjlc ddayjlc
rm ddayjlc/*
rmdir ddayjlc
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ ddayjlc










#ROM_START( ddayjlc )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "1",  0x0000, 0x2000, CRC(dbfb8772) SHA1(1fbc9726d0cd1f8781ced2f8233107b65b9bdb1a) )
#	ROM_LOAD( "2",  0x2000, 0x2000, CRC(f40ea53e) SHA1(234ef686d3e9fe12aceada7098c4cc53e56eb1a3) )
#	ROM_LOAD( "3",  0x4000, 0x2000, CRC(0780ef60) SHA1(9247af38acbaea0f78892fc50081b2400abbdc1f) )
#	ROM_LOAD( "4",  0x6000, 0x2000, CRC(75991a24) SHA1(175f505da6eb80479a70181d6aed01130f6a64cc) )

#	ROM_REGION( 0x10000, "audiocpu", 0 )
#	ROM_LOAD( "11", 0x0000, 0x2000, CRC(fe4de019) SHA1(16c5402e1a79756f8227d7e99dd94c5896c57444) )

#	ROM_REGION( 0x8000, "gfx1", 0 )
#	ROM_LOAD( "16", 0x0000, 0x2000, CRC(a167fe9a) SHA1(f2770d93ee5ae4eb9b3bcb052e14e36f53eec707) )
#	ROM_LOAD( "17", 0x2000, 0x2000, CRC(13ffe662) SHA1(2ea7855a14a4b8429751bae2e670e77608f93406) )
#	ROM_LOAD( "18", 0x4000, 0x2000, CRC(debe6531) SHA1(34b3b70a1872527266c664b2a82014d028a4ff1e) )
#	ROM_LOAD( "19", 0x6000, 0x2000, CRC(5816f947) SHA1(2236bed3e82980d3e7de3749aef0fbab042086e6) )
#	ROM_REGION( 0x2000, "gfx2", 0 )
#	ROM_LOAD( "14", 0x1000, 0x1000, CRC(2c0e9bbe) SHA1(e34ab774d2eb17ddf51af513dbcaa0c51f8dcbf7) )
#	ROM_LOAD( "15", 0x0000, 0x1000, CRC(a6eeaa50) SHA1(052cd3e906ca028e6f55d0caa1e1386482684cbf) )
#	ROM_REGION( 0x2000, "gfx3", 0 )
#	ROM_LOAD( "12", 0x1000, 0x1000, CRC(7f7afe80) SHA1(e8a549b8a8985c61d3ba452e348414146f2bc77e) )
#	ROM_LOAD( "13", 0x0000, 0x1000, CRC(f169b93f) SHA1(fb0617162542d688503fc6618dd430308e259455) )

#	ROM_REGION( 0xc0000, "user1", 0 )
#	ROM_LOAD( "5",  0x00000, 0x2000, CRC(299b05f2) SHA1(3c1804bccb514bada4bed68a6af08db63a8f1b19) )
#	ROM_LOAD( "6",  0x02000, 0x2000, CRC(38ae2616) SHA1(62c96f32532f0d7e2cf1606a303d81ebb4aada7d) )
#	ROM_LOAD( "7",  0x04000, 0x2000, CRC(4210f6ef) SHA1(525d8413afabf97cf1d04ee9a3c3d980b91bde65) )
#	ROM_LOAD( "8",  0x06000, 0x2000, CRC(91a32130) SHA1(cbcd673b47b672b9ce78c7354dacb5964a81db6f) )
#	ROM_LOAD( "9",  0x08000, 0x2000, CRC(ccb82f09) SHA1(37c23f13aa0728bae82dba9e2858a8d81fa8afa5) )
#	ROM_LOAD( "10", 0x0a000, 0x2000, CRC(5452aba1) SHA1(03ef47161d0ab047c8675d6ffd3b7acf81f74721) )

#	ROM_REGION( 0x0500, "proms", 0 )
#	ROM_LOAD( "3l.bin",  0x00400, 0x0100, CRC(da6fe846) SHA1(e8386cf7f552facf2d1a5b7b63ca3d2f1801d215) )
#	ROM_LOAD( "4l.bin",  0x00000, 0x0100, CRC(2c3fa534) SHA1(e4c0d06cf62459c1835cb27a4e659b01ad4be20c) )
#	ROM_LOAD( "4m.bin",  0x00200, 0x0100, CRC(e0ab9a8f) SHA1(77010c4039f9d408f40cea079c1ef56132ddbd2b) )
#	ROM_LOAD( "5n.bin",  0x00300, 0x0100, CRC(61d85970) SHA1(189e9da3dade54936872b80893b1318e5fbfbe5e) )
#	ROM_LOAD( "5p.bin",  0x00100, 0x0100, CRC(4fd96b26) SHA1(0fb9928ab6c4ee937cefcf82145a4c9d43ca8517) )
#ROM_END



