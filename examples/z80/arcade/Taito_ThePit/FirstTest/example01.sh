rm example01.asm
rm portman.zip
rm portman/*
rmdir portman
mkdir portman

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=portman/pe1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=portman/pe2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=portman/pe3.17
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=portman/pe4
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=portman/pe5
rm example01.bin

#audiocpu
dd bs=$((0x800)) count=1 if=/dev/zero of=portman/pe7.22
dd bs=$((0x800)) count=1 if=/dev/zero of=portman/pe6.23

#gfx
zxb.py library/charmapscramble_01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmapscramble_01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmapscramble_01.bin of=charmapscramble_01b.bin
rm charmapscramble_01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmapscramble_01b.bin of=portman/pe8.9
#dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=charmapscramble_01b.bin of=portman/pe9.8

xxd -p -c 2 -s 0 -l $((0x2000)) charmapscramble_01b.bin | cut -b 1-2 | xxd -r -p > portman/pe8.9
xxd -p -c 2 -s 1 -l $((0x2000)) charmapscramble_01b.bin | cut -b 1-2 | xxd -r -p > portman/pe9.8

rm charmapscramble_01b.bin


#prom
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=portman/mb7051.3
rm palette01.bin

#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=portman/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=portman/portman.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=portman/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=portman/82s126.3m

rm smsboot.bin dummy64k.bin

#rm portman.zip
#rm portman/*
#rmdir portman

zip -r portman portman
rm portman/*
rmdir portman


mame -w -video soft -resolution0 512x512 -rp ./ portman

