rm jackrabt.zip
rm example01.asm
rm jackrabt/*
rmdir jackrabt
mkdir jackrabt





dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=jackrabt/cpu-01.1a
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=jackrabt/cpu-01.2l
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=jackrabt/cpu-01.3l
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=jackrabt/cpu-01.4l
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=jackrabt/cpu-01.5l
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=jackrabt/cpu-01.6l
dd ibs=1 count=$((0x1000)) skip=$((0x8000)) if=example01.bin of=jackrabt/cpu-01.1a_cont
dd ibs=1 count=$((0x1000)) skip=$((0x9000)) if=example01.bin of=jackrabt/cpu-01.2h
dd ibs=1 count=$((0x1000)) skip=$((0xA000)) if=example01.bin of=jackrabt/cpu-01.3h
dd ibs=1 count=$((0x1000)) skip=$((0xB000)) if=example01.bin of=jackrabt/cpu-01.4h
dd ibs=1 count=$((0x1000)) skip=$((0xC000)) if=example01.bin of=jackrabt/cpu-01.5h
dd ibs=1 count=$((0x1000)) skip=$((0xD000)) if=example01.bin of=jackrabt/cpu-01.6h
cat jackrabt/cpu-01.1a_cont >> jackrabt/cpu-01.1a
rm jackrabt/cpu-01.1a_cont

#ROM_START( jackrabt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cpu-01.1a",    0x0000, 0x1000, CRC(499efe97) SHA1(f0efc910a5343001b27637779e1d4de218d44a4e) )
#ROM_CONTINUE(             0x8000, 0x1000 )
#ROM_LOAD( "cpu-01.2l",    0x1000, 0x1000, CRC(4772e557) SHA1(71c1eb49c978799294e732e65a77eba330d8da9b) )
#ROM_LOAD( "cpu-01.3l",    0x2000, 0x1000, CRC(1e844228) SHA1(0525fe95a0f90c50b54c0bf618eb083ccf20e6c4) )
#ROM_LOAD( "cpu-01.4l",    0x3000, 0x1000, CRC(ebffcc38) SHA1(abaf0e96d92f9c828a95446af6d5301053416f3d) )
#ROM_LOAD( "cpu-01.5l",    0x4000, 0x1000, CRC(275e0ed6) SHA1(c0789007a4de1aa848b7e5d26cf9fe847cc5d8a4) )
#ROM_LOAD( "cpu-01.6l",    0x5000, 0x1000, CRC(8a20977a) SHA1(ba15f4c62f600372390e56c2067b4a8ab1f2dba9) )
#ROM_LOAD( "cpu-01.2h",    0x9000, 0x1000, CRC(21f2be2a) SHA1(7d10489ca7325eebfa309ae4ffd4962a4310c403) )
#ROM_LOAD( "cpu-01.3h",    0xa000, 0x1000, CRC(59077027) SHA1(d6c2e68b4b2f1dce8a2141ec259812e732c1c69c) )
#ROM_LOAD( "cpu-01.4h",    0xb000, 0x1000, CRC(0b9db007) SHA1(836f8cacf2a097fd80d5c045bdc49b3a3174b89e) )
#ROM_LOAD( "cpu-01.5h",    0xc000, 0x1000, CRC(785e1a01) SHA1(a748d300be9455cad4f912e01c2279bb8465edfe) )
#ROM_LOAD( "cpu-01.6h",    0xd000, 0x1000, CRC(dd5979cf) SHA1(e9afe7002b2258a1c3132bdd951c6e20d473fb6a) )







dd bs=$((0x2000)) count=1 if=/dev/zero of=jackrabt/13snd.2g
dd bs=$((0x2000)) count=1 if=/dev/zero of=jackrabt/9snd.1i
dd bs=$((0x2000)) count=1 if=/dev/zero of=jackrabt/8snd.1h
dd bs=$((0x2000)) count=1 if=/dev/zero of=jackrabt/7snd.1g
dd bs=$((0x0104)) count=1 if=/dev/zero of=jackrabt/jr-pal16l8.6j
dd bs=$((0x0104)) count=1 if=/dev/zero of=jackrabt/jr-pal16l8.6k

#ROM_REGION( 0x10000, "audiocpu", 0 ) /* 64k for first 6802 */
#ROM_LOAD( "13snd.2g",     0x8000, 0x2000, CRC(fc05654e) SHA1(ed9c66672fe89c41e320e1d27b53f5efa92dce9c) )
#ROM_LOAD( "9snd.1i",      0xc000, 0x2000, CRC(3dab977f) SHA1(3e79c06d2e70b050f01b7ac58be5127ba87904b0) )
#ROM_REGION( 0x10000, "audio2", 0 ) /* 64k for second 6802 */
#ROM_LOAD( "8snd.1h",      0x2000, 0x1000, CRC(f4507111) SHA1(0513f0831b94aeda84aa4f3b4a7c60dfc5113b2d) )
#ROM_CONTINUE(             0x6000, 0x1000 )
#ROM_LOAD( "7snd.1g",      0x3000, 0x1000, CRC(c722eff8) SHA1(d8d1c091ab80ea2d6616e4dc030adc9905c0a496) )
#ROM_CONTINUE(             0x7000, 0x1000 )
#ROM_REGION( 0x0400, "plds", 0 )
#ROM_LOAD( "jr-pal16l8.6j",   0x0000, 0x0104, NO_DUMP ) /* PAL is read protected */
#ROM_LOAD( "jr-pal16l8.6k",   0x0200, 0x0104, NO_DUMP ) /* PAL is read protected */
#ROM_END









zxb.py library/b3r1f0_charmapnunchacken01.zxi --org=$((0x0000))
cat dummy64k.bin >> b3r1f0_charmapnunchacken01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=jackrabt/1bg.2d
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=jackrabt/2bg.1f
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=jackrabt/3bg.1e
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=b3r1f0_charmapnunchacken01b.bin
xxd -p -c 3 -s 0 -l $((0x2000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > jackrabt/1bg.2d
xxd -p -c 3 -s 1 -l $((0x2000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > jackrabt/2bg.1f
xxd -p -c 3 -s 2 -l $((0x2000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > jackrabt/3bg.1e
rm b3r1f0_charmapnunchacken01.bin b3r1f0_charmapnunchacken01b.bin
#ROM_REGION( 0x6000, "gfx1", 0 )
#ROM_LOAD( "1bg.2d",       0x0000, 0x2000, CRC(9f880ef5) SHA1(0ee20fb7c794f6dafdaf2c9ee8456221c9d668c5) )
#ROM_LOAD( "2bg.1f",       0x2000, 0x2000, CRC(afc04cd7) SHA1(f4349e86b9caee71c9bf9faf68b86603417d9a2b) )
#ROM_LOAD( "3bg.1e",       0x4000, 0x2000, CRC(14f23cdd) SHA1(e5f3dac52288c56f2fd2940b397bb6c896131a26) )










#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=jackrabt/jackrabt.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=jackrabt/jackrabt.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=jackrabt/jackrabt.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=jackrabt/jackrabt.11j
#rm clut01b.bin

dd bs=512 count=1 if=/dev/urandom of=jackrabt/jr-ic9g
dd bs=512 count=1 if=/dev/urandom of=jackrabt/jr-ic9f

#ROM_REGION( 0x0400, "proms", 0 )
#ROM_LOAD( "jr-ic9g",      0x0000, 0x0200, CRC(85577107) SHA1(76575fa68b66130b18dfe7374d1a03740963cc73) )
#ROM_LOAD( "jr-ic9f",      0x0200, 0x0200, CRC(085914d1) SHA1(3d6f9318f5a9f08ce89e4184e3efb9881f671fa7) )


rm smsboot.bin dummy64k.bin example01.bin

zip -r jackrabt jackrabt
rm jackrabt/*
rmdir jackrabt

mame -w -video soft -resolution0 512x512 -rp ./ jackrabt







#ROM_START( jackrabt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cpu-01.1a",    0x0000, 0x1000, CRC(499efe97) SHA1(f0efc910a5343001b27637779e1d4de218d44a4e) )
#ROM_CONTINUE(             0x8000, 0x1000 )
#ROM_LOAD( "cpu-01.2l",    0x1000, 0x1000, CRC(4772e557) SHA1(71c1eb49c978799294e732e65a77eba330d8da9b) )
#ROM_LOAD( "cpu-01.3l",    0x2000, 0x1000, CRC(1e844228) SHA1(0525fe95a0f90c50b54c0bf618eb083ccf20e6c4) )
#ROM_LOAD( "cpu-01.4l",    0x3000, 0x1000, CRC(ebffcc38) SHA1(abaf0e96d92f9c828a95446af6d5301053416f3d) )
#ROM_LOAD( "cpu-01.5l",    0x4000, 0x1000, CRC(275e0ed6) SHA1(c0789007a4de1aa848b7e5d26cf9fe847cc5d8a4) )
#ROM_LOAD( "cpu-01.6l",    0x5000, 0x1000, CRC(8a20977a) SHA1(ba15f4c62f600372390e56c2067b4a8ab1f2dba9) )
#ROM_LOAD( "cpu-01.2h",    0x9000, 0x1000, CRC(21f2be2a) SHA1(7d10489ca7325eebfa309ae4ffd4962a4310c403) )
#ROM_LOAD( "cpu-01.3h",    0xa000, 0x1000, CRC(59077027) SHA1(d6c2e68b4b2f1dce8a2141ec259812e732c1c69c) )
#ROM_LOAD( "cpu-01.4h",    0xb000, 0x1000, CRC(0b9db007) SHA1(836f8cacf2a097fd80d5c045bdc49b3a3174b89e) )
#ROM_LOAD( "cpu-01.5h",    0xc000, 0x1000, CRC(785e1a01) SHA1(a748d300be9455cad4f912e01c2279bb8465edfe) )
#ROM_LOAD( "cpu-01.6h",    0xd000, 0x1000, CRC(dd5979cf) SHA1(e9afe7002b2258a1c3132bdd951c6e20d473fb6a) )

#ROM_REGION( 0x10000, "audiocpu", 0 ) /* 64k for first 6802 */
#ROM_LOAD( "13snd.2g",     0x8000, 0x2000, CRC(fc05654e) SHA1(ed9c66672fe89c41e320e1d27b53f5efa92dce9c) )
#ROM_LOAD( "9snd.1i",      0xc000, 0x2000, CRC(3dab977f) SHA1(3e79c06d2e70b050f01b7ac58be5127ba87904b0) )
#ROM_REGION( 0x10000, "audio2", 0 ) /* 64k for second 6802 */
#ROM_LOAD( "8snd.1h",      0x2000, 0x1000, CRC(f4507111) SHA1(0513f0831b94aeda84aa4f3b4a7c60dfc5113b2d) )
#ROM_CONTINUE(             0x6000, 0x1000 )
#ROM_LOAD( "7snd.1g",      0x3000, 0x1000, CRC(c722eff8) SHA1(d8d1c091ab80ea2d6616e4dc030adc9905c0a496) )
#ROM_CONTINUE(             0x7000, 0x1000 )
#ROM_REGION( 0x0400, "plds", 0 )
#ROM_LOAD( "jr-pal16l8.6j",   0x0000, 0x0104, NO_DUMP ) /* PAL is read protected */
#ROM_LOAD( "jr-pal16l8.6k",   0x0200, 0x0104, NO_DUMP ) /* PAL is read protected */
#ROM_END

#ROM_REGION( 0x6000, "gfx1", 0 )
#ROM_LOAD( "1bg.2d",       0x0000, 0x2000, CRC(9f880ef5) SHA1(0ee20fb7c794f6dafdaf2c9ee8456221c9d668c5) )
#ROM_LOAD( "2bg.1f",       0x2000, 0x2000, CRC(afc04cd7) SHA1(f4349e86b9caee71c9bf9faf68b86603417d9a2b) )
#ROM_LOAD( "3bg.1e",       0x4000, 0x2000, CRC(14f23cdd) SHA1(e5f3dac52288c56f2fd2940b397bb6c896131a26) )

#ROM_REGION( 0x0400, "proms", 0 )
#ROM_LOAD( "jr-ic9g",      0x0000, 0x0200, CRC(85577107) SHA1(76575fa68b66130b18dfe7374d1a03740963cc73) )
#ROM_LOAD( "jr-ic9f",      0x0200, 0x0200, CRC(085914d1) SHA1(3d6f9318f5a9f08ce89e4184e3efb9881f671fa7) )








