rm example01.asm
rm istellar.zip
rm istellar/*
rmdir istellar
mkdir istellar



dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=istellar/rom2.top
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=istellar/rom3.top
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=istellar/rom4.top
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=istellar/rom5.top
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=istellar/rom6.top
rm example01.bin

#- /* Main program CPU */
#- ROM_REGION( 0xa000, "maincpu", 0 )
#- ROM_LOAD( "rom2.top", 0x0000, 0x2000, CRC(5d643381) SHA1(75ca52c28a52f534eda00c18b0db97e9923ff670) )    /* At IC location C63 (top board) - label ? */
#- ROM_LOAD( "rom3.top", 0x2000, 0x2000, CRC(ce5a2b09) SHA1(2de6a6e993c3411577ac0c834db8aaf16fb007ed) )    /* At IC location C64 (top board) - label ? */
#- ROM_LOAD( "rom4.top", 0x4000, 0x2000, CRC(7c2cb1f1) SHA1(ffd92510c03c2d35a59d233883c2b9f57394a51c) )    /* At IC location C65 (top board) - label ? */
#- ROM_LOAD( "rom5.top", 0x6000, 0x2000, CRC(354377f6) SHA1(bcf95b7ee1b47854e10baf24b0d8af3d56738b99) )    /* At IC location C66 (top board) - label ? */
#- ROM_LOAD( "rom6.top", 0x8000, 0x2000, CRC(0319bf40) SHA1(f324626e457c3eb7d6b74bc6afbfcc3aab2b3c72) )    /* At IC location C67 (top board) - label ? */









# sound and communications
dd bs=$((0x2000)) count=1 if=/dev/zero of=istellar/rom1.top
dd bs=$((0x2000)) count=1 if=/dev/zero of=istellar/rom11.bot

#- /* Sound CPU */
#- ROM_REGION( 0x2000, "audiocpu", 0 )
#- ROM_LOAD( "rom1.top", 0x0000, 0x2000, CRC(4f34fb1d) SHA1(56ca19344c84c5989d0be797e2759f84760310be) )    /* At IC location C62 (top board) - label ? */
#- /* LDP Communications CPU */
#- ROM_REGION( 0x2000, "sub", 0 )
#- ROM_LOAD( "rom11.bot", 0x0000, 0x2000, CRC(165cbc57) SHA1(39463888f22ec3125f0686066d923a9aae79a8f7) )   /* At IC location C12 (bottom board) - label IS11 */











#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
xxd -p -c 3 -s 0 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > istellar/rom9.bot
xxd -p -c 3 -s 1 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > istellar/rom8.bot
xxd -p -c 3 -s 2 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > istellar/rom7.bot

#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=istellar/a.m3
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=istellar/b.l3
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=istellar/c.k3
rm charmap01b.bin

#- /* Tiles */
#- ROM_REGION( 0x6000, "gfx1", 0 )
#- ROM_LOAD( "rom9.bot", 0x0000, 0x2000, CRC(9d79acb6) SHA1(72af972695face0016afce8a26c629d963e86d48) )    /* At IC location C47? (bottom board) - label ? */
#- ROM_LOAD( "rom8.bot", 0x2000, 0x2000, CRC(e9c9e490) SHA1(79aa35552b984018bc723adece5c40a0833a313c) )    /* At IC location C48? (bottom board) - label ? */
#- ROM_LOAD( "rom7.bot", 0x4000, 0x2000, CRC(1447ce3a) SHA1(8545cec108df6adab303802b1407c89b2dceba21) )    /* At IC location C49? (bottom board) - label ? */








#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin

xxd -p -c 3 -s 0 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > istellar/red6b.bot
xxd -p -c 3 -s 1 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > istellar/green6c.bot
xxd -p -c 3 -s 2 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > istellar/blue6d.bot

rm palette01b.bin

#dd bs=$((0x200)) count=1 if=/dev/urandom of=istellar/rgb.j1 #- palette?

#dd bs=$((0x100)) count=1 if=/dev/urandom of=istellar/red6b.bot
#dd bs=$((0x100)) count=1 if=/dev/urandom of=istellar/green6c.bot
#dd bs=$((0x100)) count=1 if=/dev/urandom of=istellar/blue6d.bot

#- /* Color PROMs */
#- ROM_REGION( 0x300, "proms", 0 )
#- ROM_LOAD( "red6b.bot",   0x000, 0x100, CRC(5c52f844) SHA1(a8a3d91f3247ad13c805d8d8288b07f3cdaf1189) )   /* At IC location C63? (bottom board) - label ? */
#- ROM_LOAD( "green6c.bot", 0x100, 0x100, CRC(7d8c845c) SHA1(04ae2ca0cc6679e21346ce34e9e01aa5bf4e2067) )   /* At IC location C62? (bottom board) - label ? */
#- ROM_LOAD( "blue6d.bot",  0x200, 0x100, CRC(5ebb81f9) SHA1(285d60f2894c524ca80fc68ad7c2dfd9093a67ea) )   /* At IC location C61? (bottom board) - label ? */








#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=istellar/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=istellar/hgb1.h7  #?sp
#rm clut01b.bin

#proms
dd bs=$((0x100)) count=1 if=/dev/zero of=istellar/h.c5      #- timing?
dd bs=$((0x100)) count=1 if=/dev/zero of=istellar/v.c6      #- ?????


#- /* Color (+other) PROMs */
#- ROM_REGION( 0x400, "proms", 0 )
#- ROM_LOAD( "rgb.j1", 0x000, 0x200, CRC(1e9f795f) SHA1(61a58694929fa39b2412bc9244e5681d65a0eacb) )
#- ROM_LOAD( "h.c5",   0x200, 0x100, CRC(abde5e4b) SHA1(9dd3a7fd523b519ac613b9f08ae9cc962992cf5d) )    /* Video timing? */
#- ROM_LOAD( "v.c6",   0x300, 0x100, CRC(7157ba22) SHA1(07355f30efe46196d216356eda48a59fc622e43f) )









#laserdisk

dd bs=$((0x100)) count=1 if=/dev/zero of=istellar/istellar


#- DISK_REGION( "laserdisc" )
#- DISK_IMAGE_READONLY( "istellar", 0, NO_DUMP )







#????????
dd bs=$((0x2000)) count=1 if=/dev/zero of=istellar/z03_1001_vyw-053_v1-0.bin







zip -r istellar istellar
rm istellar/*
rmdir istellar
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ istellar









#- /* There is a photo of the PCB with blurry IC locations and labels.  Comments reflect what I can (barely) see. */
#- ROM_START( istellar )
#- /* Main program CPU */
#- ROM_REGION( 0xa000, "maincpu", 0 )
#- ROM_LOAD( "rom2.top", 0x0000, 0x2000, CRC(5d643381) SHA1(75ca52c28a52f534eda00c18b0db97e9923ff670) )    /* At IC location C63 (top board) - label ? */
#- ROM_LOAD( "rom3.top", 0x2000, 0x2000, CRC(ce5a2b09) SHA1(2de6a6e993c3411577ac0c834db8aaf16fb007ed) )    /* At IC location C64 (top board) - label ? */
#- ROM_LOAD( "rom4.top", 0x4000, 0x2000, CRC(7c2cb1f1) SHA1(ffd92510c03c2d35a59d233883c2b9f57394a51c) )    /* At IC location C65 (top board) - label ? */
#- ROM_LOAD( "rom5.top", 0x6000, 0x2000, CRC(354377f6) SHA1(bcf95b7ee1b47854e10baf24b0d8af3d56738b99) )    /* At IC location C66 (top board) - label ? */
#- ROM_LOAD( "rom6.top", 0x8000, 0x2000, CRC(0319bf40) SHA1(f324626e457c3eb7d6b74bc6afbfcc3aab2b3c72) )    /* At IC location C67 (top board) - label ? */

#- /* Sound CPU */
#- ROM_REGION( 0x2000, "audiocpu", 0 )
#- ROM_LOAD( "rom1.top", 0x0000, 0x2000, CRC(4f34fb1d) SHA1(56ca19344c84c5989d0be797e2759f84760310be) )    /* At IC location C62 (top board) - label ? */

#- /* LDP Communications CPU */
#- ROM_REGION( 0x2000, "sub", 0 )
#- ROM_LOAD( "rom11.bot", 0x0000, 0x2000, CRC(165cbc57) SHA1(39463888f22ec3125f0686066d923a9aae79a8f7) )   /* At IC location C12 (bottom board) - label IS11 */

#- /* Tiles */
#- ROM_REGION( 0x6000, "gfx1", 0 )
#- ROM_LOAD( "rom9.bot", 0x0000, 0x2000, CRC(9d79acb6) SHA1(72af972695face0016afce8a26c629d963e86d48) )    /* At IC location C47? (bottom board) - label ? */
#- ROM_LOAD( "rom8.bot", 0x2000, 0x2000, CRC(e9c9e490) SHA1(79aa35552b984018bc723adece5c40a0833a313c) )    /* At IC location C48? (bottom board) - label ? */
#- ROM_LOAD( "rom7.bot", 0x4000, 0x2000, CRC(1447ce3a) SHA1(8545cec108df6adab303802b1407c89b2dceba21) )    /* At IC location C49? (bottom board) - label ? */

#- /* Color PROMs */
#- ROM_REGION( 0x300, "proms", 0 )
#- ROM_LOAD( "red6b.bot",   0x000, 0x100, CRC(5c52f844) SHA1(a8a3d91f3247ad13c805d8d8288b07f3cdaf1189) )   /* At IC location C63? (bottom board) - label ? */
#- ROM_LOAD( "green6c.bot", 0x100, 0x100, CRC(7d8c845c) SHA1(04ae2ca0cc6679e21346ce34e9e01aa5bf4e2067) )   /* At IC location C62? (bottom board) - label ? */
#- ROM_LOAD( "blue6d.bot",  0x200, 0x100, CRC(5ebb81f9) SHA1(285d60f2894c524ca80fc68ad7c2dfd9093a67ea) )   /* At IC location C61? (bottom board) - label ? */

#- DISK_REGION( "laserdisc" )
#- DISK_IMAGE_READONLY( "istellar", 0, NO_DUMP )







