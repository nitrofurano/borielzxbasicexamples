rm seicross.zip
rm example01.asm
rm seicross/*
rmdir seicross
mkdir seicross


dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=seicross/smc1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=seicross/smc2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=seicross/smc3
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=seicross/smc4
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=seicross/smc5
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=seicross/smc6
dd ibs=1 count=$((0x1000)) skip=$((0x6000)) if=example01.bin of=seicross/smc7
dd ibs=1 count=$((0x0800)) skip=$((0x7000)) if=example01.bin of=seicross/smc8

#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "smc1",         0x0000, 0x1000, CRC(f6c3aeca) SHA1(d57019e80f7e3d47ca74f54604e92d40ba9819fc) )
#	ROM_LOAD( "smc2",         0x1000, 0x1000, CRC(0ec6c218) SHA1(d8cffea48d8afd229f2008399afe3858c13653e5) )
#	ROM_LOAD( "smc3",         0x2000, 0x1000, CRC(ceb3c8f4) SHA1(e49f834637b4addcf362cd010e31802c3e145cbe) )
#	ROM_LOAD( "smc4",         0x3000, 0x1000, CRC(3112af59) SHA1(3d4e5a74a13bdeaf07f059f8c3a0d2ca8cbb3d32) )
#	ROM_LOAD( "smc5",         0x4000, 0x1000, CRC(b494a993) SHA1(ed60cbaef2ac780c11426d29a612d34e76b29a0e) )
#	ROM_LOAD( "smc6",         0x5000, 0x1000, CRC(09d5b9da) SHA1(636a8d4717df4ed1fc02fa83782fa8d96b88f969) )
#	ROM_LOAD( "smc7",         0x6000, 0x1000, CRC(13052b03) SHA1(2866f2533a788f734310a74789f762f3fa17a57a) )
#	ROM_LOAD( "smc8",         0x7000, 0x0800, CRC(2093461d) SHA1(0d640bc7ee1e9ffe32580e3143677475145b06d2) )




#mcu
dd bs=$((0x10000)) count=1 if=/dev/zero of=seicross/maincpu

#	ROM_REGION( 0x10000, "mcu", 0 ) /* 64k for the protection mcu */
#	ROM_COPY( "maincpu", 0x0000, 0x8000, 0x8000 )   /* shares the main program ROMs and RAM with the main CPU. */






#gfx

zxb.py library/b2r1f0sx_charmapchinesehero01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f0sx_charmapchinesehero01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=seicross/sz11.7k
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=seicross/smcd
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=seicross/sz9.7j
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=seicross/sz10.7h
rm b2r1f0sx_charmapchinesehero01.bin

#	ROM_REGION( 0x4000, "gfx1", 0 )
#	ROM_LOAD( "sz11.7k",      0x0000, 0x1000, CRC(fbd9b91d) SHA1(6b3581f4b518c058b970d569ced07dd7dc6a87e6) )
#	ROM_LOAD( "smcd",         0x1000, 0x1000, CRC(c3c953c4) SHA1(a96937a48b59b7e992e53d279c10a5f3ea7f9a6f) )
#	ROM_LOAD( "sz9.7j",       0x2000, 0x1000, CRC(4819f0cd) SHA1(fa8d371efc3198daf76ff1264e22673c5521becf) )
#	ROM_LOAD( "sz10.7h",      0x3000, 0x1000, CRC(4c268778) SHA1(a1444fb3eb397c8167d769aa1f935c5f19df4d6d) )





#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy32k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=seicross/pp_e02.rom
#rm spritemap01.bin




#proms/palette

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0040)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=seicross/sz73.10c
dd ibs=1 count=$((0x0020)) skip=$((0x0020)) if=palette01b.bin of=seicross/sz74.10b
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=seicross/seicross.3j

#	ROM_REGION( 0x0040, "proms", 0 )
#	ROM_LOAD( "sz73.10c",     0x0000, 0x0020, CRC(4d218a3c) SHA1(26364dfdb7e13080357328a06c3bcf504778defd) )
#	ROM_LOAD( "sz74.10b",     0x0020, 0x0020, CRC(c550531c) SHA1(d564aeb8a99861d29e00cf968242fe6c6cec478b) )



#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=seicross/seicross.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=seicross/seicross.11j
#rm clut01b.bin


#plds -?

dd bs=$((0x0044)) count=1 if=/dev/urandom of=seicross/pal16h2.3b

#	ROM_REGION( 0x0100, "plds", 0 )
#	ROM_LOAD( "pal16h2.3b", 0x0000, 0x0044, CRC(e1a6a86d) SHA1(740a5c2ef8a992f6a794c0fc4c81eb50cfcedc32) )






rm smsboot.bin dummy32k.bin example01.bin palette01b.bin

zip -r seicross seicross
rm seicross/*
rmdir seicross

mame -w -video soft -sound none -resolution0 512x512 -rp ./ seicross




#ROM_START( seicross )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "smc1",         0x0000, 0x1000, CRC(f6c3aeca) SHA1(d57019e80f7e3d47ca74f54604e92d40ba9819fc) )
#	ROM_LOAD( "smc2",         0x1000, 0x1000, CRC(0ec6c218) SHA1(d8cffea48d8afd229f2008399afe3858c13653e5) )
#	ROM_LOAD( "smc3",         0x2000, 0x1000, CRC(ceb3c8f4) SHA1(e49f834637b4addcf362cd010e31802c3e145cbe) )
#	ROM_LOAD( "smc4",         0x3000, 0x1000, CRC(3112af59) SHA1(3d4e5a74a13bdeaf07f059f8c3a0d2ca8cbb3d32) )
#	ROM_LOAD( "smc5",         0x4000, 0x1000, CRC(b494a993) SHA1(ed60cbaef2ac780c11426d29a612d34e76b29a0e) )
#	ROM_LOAD( "smc6",         0x5000, 0x1000, CRC(09d5b9da) SHA1(636a8d4717df4ed1fc02fa83782fa8d96b88f969) )
#	ROM_LOAD( "smc7",         0x6000, 0x1000, CRC(13052b03) SHA1(2866f2533a788f734310a74789f762f3fa17a57a) )
#	ROM_LOAD( "smc8",         0x7000, 0x0800, CRC(2093461d) SHA1(0d640bc7ee1e9ffe32580e3143677475145b06d2) )
#
#	ROM_REGION( 0x10000, "mcu", 0 ) /* 64k for the protection mcu */
#	ROM_COPY( "maincpu", 0x0000, 0x8000, 0x8000 )   /* shares the main program ROMs and RAM with the main CPU. */
#
#	ROM_REGION( 0x4000, "gfx1", 0 )
#	ROM_LOAD( "sz11.7k",      0x0000, 0x1000, CRC(fbd9b91d) SHA1(6b3581f4b518c058b970d569ced07dd7dc6a87e6) )
#	ROM_LOAD( "smcd",         0x1000, 0x1000, CRC(c3c953c4) SHA1(a96937a48b59b7e992e53d279c10a5f3ea7f9a6f) )
#	ROM_LOAD( "sz9.7j",       0x2000, 0x1000, CRC(4819f0cd) SHA1(fa8d371efc3198daf76ff1264e22673c5521becf) )
#	ROM_LOAD( "sz10.7h",      0x3000, 0x1000, CRC(4c268778) SHA1(a1444fb3eb397c8167d769aa1f935c5f19df4d6d) )
#
#	ROM_REGION( 0x0040, "proms", 0 )
#	ROM_LOAD( "sz73.10c",     0x0000, 0x0020, CRC(4d218a3c) SHA1(26364dfdb7e13080357328a06c3bcf504778defd) )
#	ROM_LOAD( "sz74.10b",     0x0020, 0x0020, CRC(c550531c) SHA1(d564aeb8a99861d29e00cf968242fe6c6cec478b) )
#
#	ROM_REGION( 0x0100, "plds", 0 )
#	ROM_LOAD( "pal16h2.3b", 0x0000, 0x0044, CRC(e1a6a86d) SHA1(740a5c2ef8a992f6a794c0fc4c81eb50cfcedc32) )
#ROM_END





