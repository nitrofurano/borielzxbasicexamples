rm example01.asm
rm fcombat.zip
rm fcombat/*
rmdir fcombat
mkdir fcombat
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=fcombat/fcombat2.t9
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=fcombat/fcombat3.10t
rm example01.bin

#ROM_START( fcombat )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "fcombat2.t9",  0x0000, 0x4000, CRC(30cb0c14) SHA1(8b5b6a4efaca2f138709184725e9e0e0b9cfc4c7) )
#	ROM_LOAD( "fcombat3.10t", 0x4000, 0x4000, CRC(e8511da0) SHA1(bab5c9244c970b97c025381c37ad372aa3b5cddf) )




dd bs=$((0x4000)) count=1 if=/dev/zero of=fcombat/fcombat1.t5

#	ROM_REGION( 0x10000, "audiocpu", 0 )     /* 64k for the second CPU */
#	ROM_LOAD( "fcombat1.t5",  0x0000, 0x4000, CRC(a0cc1216) SHA1(3a8963ffde2ff4a3f428369133f94bb37717cae5) )













#gfx
zxb.py library/b2r3f1c16_charmapfieldcombat01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r3f1c16_charmapfieldcombat01.bin of=b2r3f1c16_charmapfieldcombat01b.bin
rm b2r3f1c16_charmapfieldcombat01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f1c16_charmapfieldcombat01b.bin of=fcombat/fcombat7.l11
#gfx2
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=b2r3f1c16_charmapfieldcombat01b.bin of=fcombat/fcombat8.d10
dd ibs=1 count=$((0x4000)) skip=$((0x2000)) if=b2r3f1c16_charmapfieldcombat01b.bin of=fcombat/fcombat9.d11
dd ibs=1 count=$((0x4000)) skip=$((0x2000)) if=b2r3f1c16_charmapfieldcombat01b.bin of=fcombat/fcomba10.d12
#gfx3
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=b2r3f1c16_charmapfieldcombat01b.bin of=fcombat/fcombat6.f3
rm b2r3f1c16_charmapfieldcombat01b.bin

#	ROM_REGION( 0x02000, "gfx1", 0 )
#	ROM_LOAD( "fcombat7.l11", 0x00000, 0x2000, CRC(401061b5) SHA1(09dd23e86a56db8021e14432aced0eaf013fefe2) ) /* fg chars */
#	ROM_REGION( 0x0c000, "gfx2", 0 )
#	ROM_LOAD( "fcombat8.d10", 0x00000, 0x4000, CRC(e810941e) SHA1(19ae85af0bf245caf3afe10d65e618cfb47d33c2) ) /* sprites */
#	ROM_LOAD( "fcombat9.d11", 0x04000, 0x4000, CRC(f95988e6) SHA1(25876652decca7ec1e9b37a16536c15ca2d1cb12) )
#	ROM_LOAD( "fcomba10.d12", 0x08000, 0x4000, CRC(908f154c) SHA1(b3761ee60d4a5ea36376759875105d23c57b4bf2) )
#	ROM_REGION( 0x04000, "gfx3", 0 )
#	ROM_LOAD( "fcombat6.f3",  0x00000, 0x4000, CRC(97282729) SHA1(72db0593551c2d15631341bf621b96013b46ce72) )








dd bs=$((0x4000)) count=1 if=/dev/urandom of=fcombat/fcombat5.l3
dd bs=$((0x4000)) count=1 if=/dev/urandom of=fcombat/fcombat4.p3

#	ROM_REGION( 0x04000, "user1", 0 )
#	ROM_LOAD( "fcombat5.l3",  0x00000, 0x4000, CRC(96194ca7) SHA1(087d6ac8f93f087cb5e378dbe9a8cfcffa2cdddc) ) /* bg data */
#	ROM_REGION( 0x04000, "user2", 0 )
#	ROM_LOAD( "fcombat4.p3",  0x00000, 0x4000, CRC(efe098ab) SHA1(fe64a5e9170835d242368109b1b221b0f8090e7e) ) /* terrain info */







#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=fcombat/fcprom_a.c2
rm palette01.bin palette01b.bin

#lookup
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy64k.bin >> clut01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=fcombat/fcprom_d.k12
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=fcombat/fcprom_b.c4
dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=fcombat/fcprom_c.a9
rm clut01.bin clut01b.bin

#proms
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fcombat/fcprom_a.c2
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fcombat/fcprom_d.k12
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fcombat/fcprom_b.c4
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fcombat/fcprom_c.a9

#	ROM_REGION( 0x0420, "proms", 0 )
#	ROM_LOAD( "fcprom_a.c2",  0x0000, 0x0020, CRC(7ac480f0) SHA1(f491fe4da19d8c037e3733a5836de35cc438907e) ) /* palette */
#	ROM_LOAD( "fcprom_d.k12", 0x0020, 0x0100, CRC(9a348250) SHA1(faf8db4c42adee07795d06bea20704f8c51090ff) ) /* fg char lookup table */
#	ROM_LOAD( "fcprom_b.c4",  0x0120, 0x0100, CRC(ac9049f6) SHA1(57aa5b5df3e181bad76149745a422c3dd1edad49) ) /* sprite lookup table */
#	ROM_LOAD( "fcprom_c.a9",  0x0220, 0x0100, CRC(768ac120) SHA1(ceede1d6cbeae08da96ef52bdca2718a839d88ab) ) /* bg char mixer */





zip -r fcombat fcombat
rm fcombat/*
rmdir fcombat
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ fcombat





#ROM_START( fcombat )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "fcombat2.t9",  0x0000, 0x4000, CRC(30cb0c14) SHA1(8b5b6a4efaca2f138709184725e9e0e0b9cfc4c7) )
#	ROM_LOAD( "fcombat3.10t", 0x4000, 0x4000, CRC(e8511da0) SHA1(bab5c9244c970b97c025381c37ad372aa3b5cddf) )

#	ROM_REGION( 0x10000, "audiocpu", 0 )     /* 64k for the second CPU */
#	ROM_LOAD( "fcombat1.t5",  0x0000, 0x4000, CRC(a0cc1216) SHA1(3a8963ffde2ff4a3f428369133f94bb37717cae5) )

#	ROM_REGION( 0x02000, "gfx1", 0 )
#	ROM_LOAD( "fcombat7.l11", 0x00000, 0x2000, CRC(401061b5) SHA1(09dd23e86a56db8021e14432aced0eaf013fefe2) ) /* fg chars */
#	ROM_REGION( 0x0c000, "gfx2", 0 )
#	ROM_LOAD( "fcombat8.d10", 0x00000, 0x4000, CRC(e810941e) SHA1(19ae85af0bf245caf3afe10d65e618cfb47d33c2) ) /* sprites */
#	ROM_LOAD( "fcombat9.d11", 0x04000, 0x4000, CRC(f95988e6) SHA1(25876652decca7ec1e9b37a16536c15ca2d1cb12) )
#	ROM_LOAD( "fcomba10.d12", 0x08000, 0x4000, CRC(908f154c) SHA1(b3761ee60d4a5ea36376759875105d23c57b4bf2) )
#	ROM_REGION( 0x04000, "gfx3", 0 )
#	ROM_LOAD( "fcombat6.f3",  0x00000, 0x4000, CRC(97282729) SHA1(72db0593551c2d15631341bf621b96013b46ce72) )

#	ROM_REGION( 0x04000, "user1", 0 )
#	ROM_LOAD( "fcombat5.l3",  0x00000, 0x4000, CRC(96194ca7) SHA1(087d6ac8f93f087cb5e378dbe9a8cfcffa2cdddc) ) /* bg data */
#	ROM_REGION( 0x04000, "user2", 0 )
#	ROM_LOAD( "fcombat4.p3",  0x00000, 0x4000, CRC(efe098ab) SHA1(fe64a5e9170835d242368109b1b221b0f8090e7e) ) /* terrain info */

#	ROM_REGION( 0x0420, "proms", 0 )
#	ROM_LOAD( "fcprom_a.c2",  0x0000, 0x0020, CRC(7ac480f0) SHA1(f491fe4da19d8c037e3733a5836de35cc438907e) ) /* palette */
#	ROM_LOAD( "fcprom_d.k12", 0x0020, 0x0100, CRC(9a348250) SHA1(faf8db4c42adee07795d06bea20704f8c51090ff) ) /* fg char lookup table */
#	ROM_LOAD( "fcprom_b.c4",  0x0120, 0x0100, CRC(ac9049f6) SHA1(57aa5b5df3e181bad76149745a422c3dd1edad49) ) /* sprite lookup table */
#	ROM_LOAD( "fcprom_c.a9",  0x0220, 0x0100, CRC(768ac120) SHA1(ceede1d6cbeae08da96ef52bdca2718a839d88ab) ) /* bg char mixer */
#ROM_END


