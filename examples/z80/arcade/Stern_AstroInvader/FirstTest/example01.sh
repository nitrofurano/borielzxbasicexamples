# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm spaceint.zip
rm spaceint/*
rmdir spaceint
mkdir spaceint

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=16384 count=1 if=/dev/zero of=dummy16k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy16k.bin >> example01.bin
dd ibs=1 count=$((0x400)) skip=$((0x0000)) if=example01.bin of=spaceint/1
dd ibs=1 count=$((0x400)) skip=$((0x0400)) if=example01.bin of=spaceint/2
dd ibs=1 count=$((0x400)) skip=$((0x0800)) if=example01.bin of=spaceint/3
dd ibs=1 count=$((0x400)) skip=$((0x0C00)) if=example01.bin of=spaceint/4
dd ibs=1 count=$((0x400)) skip=$((0x1000)) if=example01.bin of=spaceint/5
dd ibs=1 count=$((0x400)) skip=$((0x1400)) if=example01.bin of=spaceint/6
rm dummy16k.bin example01.bin

#gfx
#zxb.py library/charmap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=spaceint/spaceint.5e
#rm charmap01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=spaceint/spaceint.5f
#rm spritemap01.bin

#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=spaceint/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=spaceint/82s126.4a
#rm clut01.bin

#prom
#dd bs=256 count=1 if=/dev/urandom of=spaceint/clr
zxb.py library/prom01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=prom01.bin of=spaceint/clr
rm prom01.bin


rm smsboot.bin

zip -r spaceint spaceint
rm spaceint/*
rmdir spaceint

mame -w -video soft -resolution0 512x512 -rp ./ spaceint
#xmame.SDL -rp ./ spaceint

