# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm chinhero.zip
rm chinhero/*
rmdir chinhero
mkdir chinhero



dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=chinhero/ic2.1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=chinhero/ic3.2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=chinhero/ic4.3
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=chinhero/ic5.4
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=chinhero/ic6.5
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ao4_05.ic3
rm dummy64k.bin example01.bin

#- ROM_START( chinhero )
#- ROM_REGION( 0x10000, "maincpu", 0 ) /* Z80 code (main) */
#- ROM_LOAD( "ic2.1",        0x0000, 0x2000, CRC(8974bac4) SHA1(932a677d0928f4146201f206b71ac2bcc0f6735c) )
#- ROM_LOAD( "ic3.2",        0x2000, 0x2000, CRC(9b7a02fe) SHA1(b17593262ec24b999d66634b84eee95c1088f7eb) )
#- ROM_LOAD( "ic4.3",        0x4000, 0x2000, CRC(e86d4195) SHA1(5081500e0a6d4fd19690134efd6f35b6047e6727) )
#- ROM_LOAD( "ic5.4",        0x6000, 0x2000, CRC(2b629d2c) SHA1(7e92e2c2d09d3501ddbf79a14228cf273f4a17df) )
#- ROM_LOAD( "ic6.5",        0x8000, 0x2000, CRC(35bf4a4f) SHA1(2600c57d40355775847eed8a9592c67f5d11f1f1) )



#mcu
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ic31.6
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ic32.7
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ic47.8
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ic48.9
dd bs=$((0x2000)) count=1 if=/dev/zero of=chinhero/ic49.10

#- ROM_REGION( 0x10000, "bbx", 0 ) /* Z80 code (coprocessor) */
#- ROM_LOAD( "ic31.6",       0x0000, 0x2000, CRC(7c56927b) SHA1(565a10b39f2d5d38cb5415aadd7fbdb90dcf13cb) )
#- ROM_LOAD( "ic32.7",       0x2000, 0x2000, CRC(d67b8045) SHA1(0374cafb8d4828e195791784ac187772c49c18f9) )
#- ROM_REGION( 0x10000, "audiocpu", 0 ) /* Z80 code (sound) */
#- ROM_LOAD( "ic47.8",       0x0000, 0x2000, CRC(3c396062) SHA1(2c1540eb123b3124e1679ba09e063e80f2423022) )
#- ROM_LOAD( "ic48.9",       0x2000, 0x2000, CRC(b14f2bab) SHA1(3643b430e3b464b0bc9aca81122b07fb8eb0fb9f) )
#- ROM_LOAD( "ic49.10",      0x4000, 0x2000, CRC(8c0e43d1) SHA1(acaead801b4782875c8b6092e987b73f9973f8b0) )






#gfx
zxb.py library/b2r1f0sx_charmapchinesehero01.zxi --org=$((0x0000))
cat dummy64k.bin >> b2r1f0sx_charmapchinesehero01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b2r1f0sx_charmapchinesehero01.bin of=b2r1f0sx_charmapchinesehero01b.bin
rm b2r1f0sx_charmapchinesehero01.bin

dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic21.11
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic22.12

dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic114.18
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic113.17
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic99.13

dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic112.16
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic111.15
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r1f0sx_charmapchinesehero01b.bin of=chinhero/ic110.14

#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 b2r1f0sx_charmapchinesehero01b.bin | cut -b 1-2 | xxd -r -p > chinhero/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b2r1f0sx_charmapchinesehero01b.bin | cut -b 1-2 | xxd -r -p > chinhero/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 b2r1f0sx_charmapchinesehero01b.bin | cut -b 1-2 | xxd -r -p > chinhero/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b2r1f0sx_charmapchinesehero01b.bin | cut -b 1-2 | xxd -r -p > chinhero/ao4_07.ic15 #-bitdepth1
rm b2r1f0sx_charmapchinesehero01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=chinhero/chinhero.5f
#rm spritemap01.bin

#- ROM_REGION( 0x4000, "gfx1", ROMREGION_INVERT ) /* tiles */
#- ROM_LOAD( "ic21.11",      0x0000, 0x2000, CRC(3a37fb45) SHA1(4c631cf924f1e1dfea6db3f014ab7d9cb9f4b0c4) )
#- ROM_LOAD( "ic22.12",      0x2000, 0x2000, CRC(bc21c002) SHA1(4fc5e4dfe8331a3feb1c370a8aca9c8303eb7b4e) )
#- ROM_REGION( 0x6000, "gfx2", ROMREGION_INVERT ) /* sprites */
#- ROM_LOAD( "ic114.18",     0x0000, 0x2000, CRC(fc4183a8) SHA1(4bc891a9e16cd84ce353180705cc8fcadf414a49) )
#- ROM_LOAD( "ic113.17",     0x2000, 0x2000, CRC(d713d7fe) SHA1(8dd97f96a1190c5be5e19721227dd80adf060b4d) )
#- ROM_LOAD(  "ic99.13",     0x4000, 0x2000, CRC(a8e2a3f4) SHA1(db9f954d4b46660f5f1cb4122838e6418f92d0a3) )
#- ROM_REGION( 0x6000, "gfx3", ROMREGION_INVERT ) /* sprites */
#- ROM_LOAD( "ic112.16",     0x0000, 0x2000, CRC(dd5170ca) SHA1(e0a9d1dbc021a8ad84dd7d1bd7e390e51e6328b7) )
#- ROM_LOAD( "ic111.15",     0x2000, 0x2000, CRC(20f6052e) SHA1(e22ddb3fb90ff8df5ce7fda6a26c1b9fce2f59ab) )
#- ROM_LOAD( "ic110.14",     0x4000, 0x2000, CRC(9bc2d568) SHA1(a4ee8822709645b0dc088635c0a9c263fb5a2245) )










#palette
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 3 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > chinhero/v_ic36_r
xxd -p -c 3 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > chinhero/v_ic35_g
xxd -p -c 3 -s 2 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > chinhero/v_ic27_b
rm palette01b.bin

#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=chinhero/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=chinhero/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=chinhero/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=chinhero/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=chinhero/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=chinhero/82s126.4a
#rm clut01.bin

#dd bs=$((0x100)) count=1 if=/dev/urandom of=chinhero/v_ic36_r
#dd bs=$((0x100)) count=1 if=/dev/urandom of=chinhero/v_ic35_g
#dd bs=$((0x100)) count=1 if=/dev/urandom of=chinhero/v_ic27_b
dd bs=$((0x100)) count=1 if=/dev/urandom of=chinhero/v_ic28_m
dd bs=$((0x200)) count=1 if=/dev/zero of=chinhero/v_ic69
dd bs=$((0x200)) count=1 if=/dev/zero of=chinhero/v_ic108
dd bs=$((0x100)) count=1 if=/dev/zero of=chinhero/v_ic12
dd bs=$((0x100)) count=1 if=/dev/zero of=chinhero/v_ic15_p
dd bs=$((0x20)) count=1 if=/dev/zero of=chinhero/v_ic8
dd bs=$((0x20)) count=1 if=/dev/zero of=chinhero/ic8
dd bs=$((0x20)) count=1 if=/dev/zero of=chinhero/ic22
dd bs=$((0x20)) count=1 if=/dev/zero of=chinhero/ic42

#- ROM_REGION( 0xa80, "proms", 0 )
#- ROM_LOAD( "v_ic36_r",     0x000, 0x100, CRC(16ae1692) SHA1(e287b96890da4815350af72e9f2189d0c72313b6) ) /* red */
#- ROM_LOAD( "v_ic35_g",     0x100, 0x100, CRC(b3d0a074) SHA1(e955fda8cb8df389507e17b7b4609e845e5ef0c4) ) /* green */
#- ROM_LOAD( "v_ic27_b",     0x200, 0x100, CRC(353a2d11) SHA1(76f21e3e092024592d9ccd33ae69c438254c5755) ) /* blue */
#- ROM_LOAD( "v_ic28_m",     0x300, 0x100, CRC(7ca273c1) SHA1(20d85547d96bea8b310c943c45e4978a7e5b5585) ) /* unknown */
#- ROM_LOAD( "v_ic69",       0x400, 0x200, CRC(410d6f86) SHA1(3cfaef3702dbda3e7c7eb84a93561e36778aec3e) ) /* zoom */
#- ROM_LOAD( "v_ic108",      0x600, 0x200, CRC(d33c02ae) SHA1(1a2146ae404a5e8a701e1d547a8409a376d4bee4) ) /* zoom */
#- ROM_LOAD( "v_ic12",       0x800, 0x100, CRC(0de07e89) SHA1(5655bce6ff3abad63f5b31add402cdbb51c323f0) ) /* tile pen priority */
#- ROM_LOAD( "v_ic15_p",     0x900, 0x100, CRC(7e0a0581) SHA1(e355a6ef21a65a1e828d7bd5b0f2224b06438b4a) ) /* sprite pen transparency */
#- ROM_LOAD( "v_ic8",        0xa00, 0x020, CRC(4c62974d) SHA1(fd5970b5ba1d9e986515ae06c2e83f8bf20b3cdc) )
#- ROM_LOAD( "ic8",          0xa20, 0x020, CRC(84bcd9af) SHA1(5a5afeb6aedb8ac6ac49fb8da62df57fbd8b1780) ) /* main CPU banking */
#- ROM_LOAD( "ic22",         0xa40, 0x020, CRC(84bcd9af) SHA1(5a5afeb6aedb8ac6ac49fb8da62df57fbd8b1780) ) /* coprocessor banking */
#- ROM_LOAD( "ic42",         0xa60, 0x020, CRC(2ccfe10a) SHA1(d89ea91e5da436805fca9ded9b33609f4a862724) ) /* sound cpu banking */
#- ROM_END






rm smsboot.bin

zip -r chinhero chinhero
rm chinhero/*
rmdir chinhero

mame -w -video soft -resolution0 512x512 -rp ./ chinhero
#xmame.SDL -rp ./ chinhero






#- ROM_START( chinhero )
#- ROM_REGION( 0x10000, "maincpu", 0 ) /* Z80 code (main) */
#- ROM_LOAD( "ic2.1",        0x0000, 0x2000, CRC(8974bac4) SHA1(932a677d0928f4146201f206b71ac2bcc0f6735c) )
#- ROM_LOAD( "ic3.2",        0x2000, 0x2000, CRC(9b7a02fe) SHA1(b17593262ec24b999d66634b84eee95c1088f7eb) )
#- ROM_LOAD( "ic4.3",        0x4000, 0x2000, CRC(e86d4195) SHA1(5081500e0a6d4fd19690134efd6f35b6047e6727) )
#- ROM_LOAD( "ic5.4",        0x6000, 0x2000, CRC(2b629d2c) SHA1(7e92e2c2d09d3501ddbf79a14228cf273f4a17df) )
#- ROM_LOAD( "ic6.5",        0x8000, 0x2000, CRC(35bf4a4f) SHA1(2600c57d40355775847eed8a9592c67f5d11f1f1) )

#- ROM_REGION( 0x10000, "bbx", 0 ) /* Z80 code (coprocessor) */
#- ROM_LOAD( "ic31.6",       0x0000, 0x2000, CRC(7c56927b) SHA1(565a10b39f2d5d38cb5415aadd7fbdb90dcf13cb) )
#- ROM_LOAD( "ic32.7",       0x2000, 0x2000, CRC(d67b8045) SHA1(0374cafb8d4828e195791784ac187772c49c18f9) )
#- ROM_REGION( 0x10000, "audiocpu", 0 ) /* Z80 code (sound) */
#- ROM_LOAD( "ic47.8",       0x0000, 0x2000, CRC(3c396062) SHA1(2c1540eb123b3124e1679ba09e063e80f2423022) )
#- ROM_LOAD( "ic48.9",       0x2000, 0x2000, CRC(b14f2bab) SHA1(3643b430e3b464b0bc9aca81122b07fb8eb0fb9f) )
#- ROM_LOAD( "ic49.10",      0x4000, 0x2000, CRC(8c0e43d1) SHA1(acaead801b4782875c8b6092e987b73f9973f8b0) )

#- ROM_REGION( 0x4000, "gfx1", ROMREGION_INVERT ) /* tiles */
#- ROM_LOAD( "ic21.11",      0x0000, 0x2000, CRC(3a37fb45) SHA1(4c631cf924f1e1dfea6db3f014ab7d9cb9f4b0c4) )
#- ROM_LOAD( "ic22.12",      0x2000, 0x2000, CRC(bc21c002) SHA1(4fc5e4dfe8331a3feb1c370a8aca9c8303eb7b4e) )
#- ROM_REGION( 0x6000, "gfx2", ROMREGION_INVERT ) /* sprites */
#- ROM_LOAD( "ic114.18",     0x0000, 0x2000, CRC(fc4183a8) SHA1(4bc891a9e16cd84ce353180705cc8fcadf414a49) )
#- ROM_LOAD( "ic113.17",     0x2000, 0x2000, CRC(d713d7fe) SHA1(8dd97f96a1190c5be5e19721227dd80adf060b4d) )
#- ROM_LOAD(  "ic99.13",     0x4000, 0x2000, CRC(a8e2a3f4) SHA1(db9f954d4b46660f5f1cb4122838e6418f92d0a3) )
#- ROM_REGION( 0x6000, "gfx3", ROMREGION_INVERT ) /* sprites */
#- ROM_LOAD( "ic112.16",     0x0000, 0x2000, CRC(dd5170ca) SHA1(e0a9d1dbc021a8ad84dd7d1bd7e390e51e6328b7) )
#- ROM_LOAD( "ic111.15",     0x2000, 0x2000, CRC(20f6052e) SHA1(e22ddb3fb90ff8df5ce7fda6a26c1b9fce2f59ab) )
#- ROM_LOAD( "ic110.14",     0x4000, 0x2000, CRC(9bc2d568) SHA1(a4ee8822709645b0dc088635c0a9c263fb5a2245) )

#- ROM_REGION( 0xa80, "proms", 0 )
#- ROM_LOAD( "v_ic36_r",     0x000, 0x100, CRC(16ae1692) SHA1(e287b96890da4815350af72e9f2189d0c72313b6) ) /* red */
#- ROM_LOAD( "v_ic35_g",     0x100, 0x100, CRC(b3d0a074) SHA1(e955fda8cb8df389507e17b7b4609e845e5ef0c4) ) /* green */
#- ROM_LOAD( "v_ic27_b",     0x200, 0x100, CRC(353a2d11) SHA1(76f21e3e092024592d9ccd33ae69c438254c5755) ) /* blue */
#- ROM_LOAD( "v_ic28_m",     0x300, 0x100, CRC(7ca273c1) SHA1(20d85547d96bea8b310c943c45e4978a7e5b5585) ) /* unknown */
#- ROM_LOAD( "v_ic69",       0x400, 0x200, CRC(410d6f86) SHA1(3cfaef3702dbda3e7c7eb84a93561e36778aec3e) ) /* zoom */
#- ROM_LOAD( "v_ic108",      0x600, 0x200, CRC(d33c02ae) SHA1(1a2146ae404a5e8a701e1d547a8409a376d4bee4) ) /* zoom */
#- ROM_LOAD( "v_ic12",       0x800, 0x100, CRC(0de07e89) SHA1(5655bce6ff3abad63f5b31add402cdbb51c323f0) ) /* tile pen priority */
#- ROM_LOAD( "v_ic15_p",     0x900, 0x100, CRC(7e0a0581) SHA1(e355a6ef21a65a1e828d7bd5b0f2224b06438b4a) ) /* sprite pen transparency */
#- ROM_LOAD( "v_ic8",        0xa00, 0x020, CRC(4c62974d) SHA1(fd5970b5ba1d9e986515ae06c2e83f8bf20b3cdc) )
#- ROM_LOAD( "ic8",          0xa20, 0x020, CRC(84bcd9af) SHA1(5a5afeb6aedb8ac6ac49fb8da62df57fbd8b1780) ) /* main CPU banking */
#- ROM_LOAD( "ic22",         0xa40, 0x020, CRC(84bcd9af) SHA1(5a5afeb6aedb8ac6ac49fb8da62df57fbd8b1780) ) /* coprocessor banking */
#- ROM_LOAD( "ic42",         0xa60, 0x020, CRC(2ccfe10a) SHA1(d89ea91e5da436805fca9ded9b33609f4a862724) ) /* sound cpu banking */
#- ROM_END



