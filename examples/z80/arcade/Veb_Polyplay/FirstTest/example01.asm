	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$0FFF
#line 1
	jp __LABEL__jmp01
#line 5
		org $1000
#line 6
__LABEL__jmp01:
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL0
__LABEL3:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL5
__LABEL8:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL10
__LABEL13:
	ld hl, 8
	push hl
	ld hl, (_ee0)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__tiles01
	add hl, de
	push hl
	ld hl, (_ee0)
	ld de, 25
	call __MUL16_FAST
	push hl
	ld hl, (_ee1)
	ld de, 5
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	add hl, hl
	add hl, hl
	add hl, hl
	ld de, 60416
	add hl, de
	push hl
	call _pacmanldir
	ld hl, 8
	push hl
	ld hl, (_ee1)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__tiles01
	add hl, de
	push hl
	ld hl, (_ee0)
	ld de, 25
	call __MUL16_FAST
	push hl
	ld hl, (_ee1)
	ld de, 5
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	add hl, hl
	add hl, hl
	add hl, hl
	ld de, 61440
	add hl, de
	push hl
	call _pacmanldir
	ld hl, 8
	push hl
	ld hl, (_ee2)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__tiles01
	add hl, de
	push hl
	ld hl, (_ee0)
	ld de, 25
	call __MUL16_FAST
	push hl
	ld hl, (_ee1)
	ld de, 5
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	add hl, hl
	add hl, hl
	add hl, hl
	ld de, 62464
	add hl, de
	push hl
	call _pacmanldir
__LABEL14:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL10:
	ld hl, 4
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL5:
	ld hl, 4
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
__LABEL4:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL0:
	ld hl, 4
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL20
__LABEL23:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld de, 125
	call __MODU16
	ld de, 128
	add hl, de
	ld a, l
	push af
	ld hl, (_ee0)
	push hl
	ld hl, (_ee1)
	push hl
	call _polyplayputchar
__LABEL24:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL20:
	ld hl, 63
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL15:
	ld hl, 31
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL25
__LABEL28:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL30
__LABEL33:
	ld hl, (_ee0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee0)
	ld de, 15
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 47
	add hl, de
	push hl
	call _polyplayputchar
__LABEL34:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL30:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
__LABEL29:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL25:
	ld hl, 15
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
	ld hl, 8
	ld (_eex), hl
	ld hl, 8
	ld (_eey), hl
	ld hl, 0
	ld (_eee), hl
__LABEL35:
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 1
	push hl
	call _polyplayputchar
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 2
	push hl
	call _polyplayputchar
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 1
	push hl
	ld hl, 3
	push hl
	call _polyplayputchar
	ld bc, 132
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 1
	push hl
	call _polyplayputchar
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 2
	push hl
	call _polyplayputchar
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 3
	push hl
	call _polyplayputchar
	ld de, 2
	ld hl, (_ee0)
	call __BAND16
	srl h
	rr l
	ex de, hl
	ld hl, (_eex)
	add hl, de
	push hl
	ld de, 4
	ld hl, (_ee0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_eex), hl
	ld de, 16
	ld hl, (_ee0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_eey)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ee0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_eey), hl
	ld de, 0
	ld hl, (_eex)
	or a
	sbc hl, de
	jp nc, __LABEL38
	ld hl, 0
	ld (_eex), hl
__LABEL38:
	ld hl, 63
	ld de, (_eex)
	or a
	sbc hl, de
	jp nc, __LABEL40
	ld hl, 63
	ld (_eex), hl
__LABEL40:
	ld de, 0
	ld hl, (_eey)
	or a
	sbc hl, de
	jp nc, __LABEL42
	ld hl, 0
	ld (_eey), hl
__LABEL42:
	ld hl, 31
	ld de, (_eey)
	or a
	sbc hl, de
	jp nc, __LABEL44
	ld hl, 31
	ld (_eey), hl
__LABEL44:
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_eey)
	push hl
	ld hl, (_eex)
	push hl
	call _polyplayputchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL35
__LABEL36:
__LABEL45:
	jp __LABEL45
__LABEL46:
__LABEL47:
	ld hl, 63488
	ld (_eee), hl
	jp __LABEL49
__LABEL52:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 500
	push hl
	call _pacmandelay
__LABEL53:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL49:
	ld hl, 65534
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL52
__LABEL51:
	ld hl, 63488
	ld (_eee), hl
	jp __LABEL54
__LABEL57:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 500
	push hl
	call _pacmandelay
__LABEL58:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL54:
	ld hl, 65534
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL57
__LABEL56:
	jp __LABEL47
__LABEL48:
	ld hl, 59912
	ld (_eee), hl
	jp __LABEL59
__LABEL62:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 500
	push hl
	call _pacmandelay
__LABEL63:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL59:
	ld hl, 60147
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL62
__LABEL61:
__LABEL64:
	jp __LABEL64
__LABEL65:
__LABEL66:
	jp __LABEL66
__LABEL67:
__LABEL68:
	jp __LABEL68
__LABEL69:
__LABEL__tiles01:
#line 167
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11101110
		defb %11101110
		defb %10111011
		defb %10111011
		defb %11101110
		defb %11101110
		defb %10111011
		defb %10111011
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
#line 207
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_pacmandelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
		push bc
pacmandelayloop:
		dec bc
		ld a,b
		or c
		jp nz,pacmandelayloop
		pop bc
#line 10
_pacmandelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_pacmanldir:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d, (ix+5)
		ld e, (ix+4)
		ld h, (ix+7)
		ld l, (ix+6)
		ld b, (ix+9)
		ld c, (ix+8)
		ldir
#line 8
_pacmanldir__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_polyplayputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 63488
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 64
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_polyplayputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 710 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 711 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 712 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 713 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 714 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_eee EQU 3088
	_seed EQU 3090
	_ee0 EQU 3092
	_ee1 EQU 3094
	_ee2 EQU 3096
	_ee3 EQU 3098
	_eex EQU 3100
	_eey EQU 3102
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
