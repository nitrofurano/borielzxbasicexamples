rm polyplay.zip
rm example01.asm
rm polyplay/*
rmdir polyplay
mkdir polyplay




dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))


cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x400)) skip=$((0x0000)) if=example01.bin of=polyplay/cpu_0000.37
dd ibs=1 count=$((0x400)) skip=$((0x0400)) if=example01.bin of=polyplay/cpu_0400.36
dd ibs=1 count=$((0x400)) skip=$((0x0800)) if=example01.bin of=polyplay/cpu_0800.35
dd ibs=1 count=$((0x400)) skip=$((0x0c00)) if=example01.bin of=polyplay/dummy01
#- ROM_LOAD( "cpu_0000.37",       0x0000, 0x0400, CRC(87884c5f) SHA1(849c6b3f40496c694a123d6eec268a7128c037f0) )
#- ROM_LOAD( "cpu_0400.36",       0x0400, 0x0400, CRC(d5c84829) SHA1(baa8790e77db66e1e543b3a0e5390cc71256de2f) )
#- ROM_LOAD( "cpu_0800.35",       0x0800, 0x0400, CRC(5f36d08e) SHA1(08ecf8143e818a9844b4f168e68629d6d4481a8a) )
#-----------  dummy01      ------ 0x0c00, 0x0400 (ram)

dd ibs=1 count=$((0x400)) skip=$((0x1000)) if=example01.bin of=polyplay/2_-_1000.14
dd ibs=1 count=$((0x400)) skip=$((0x1400)) if=example01.bin of=polyplay/2_-_1400.10
dd ibs=1 count=$((0x400)) skip=$((0x1800)) if=example01.bin of=polyplay/2_-_1800.6
dd ibs=1 count=$((0x400)) skip=$((0x1c00)) if=example01.bin of=polyplay/2_-_1c00.2
#- ROM_LOAD( "2_-_1000.14",       0x1000, 0x0400, CRC(950dfcdb) SHA1(74170d5c99d1ea61fe37d1fe023dca96efb1ca69) )
#- ROM_LOAD( "2_-_1400.10",       0x1400, 0x0400, CRC(829f74ca) SHA1(4df9d3c24e1bc4c2c953dce9530e43a00ecf67fc) )
#- ROM_LOAD( "2_-_1800.6",        0x1800, 0x0400, CRC(b69306f5) SHA1(66d7c3cf76782a5b6eafa3e1513ecc9a9df0e0e1) )
#- ROM_LOAD( "2_-_1c00.2",        0x1c00, 0x0400, CRC(aede2280) SHA1(0a01394ab70d07d666e955c87a08cb4d4945767e) )

dd ibs=1 count=$((0x400)) skip=$((0x2000)) if=example01.bin of=polyplay/2_-_2000.15
dd ibs=1 count=$((0x400)) skip=$((0x2400)) if=example01.bin of=polyplay/2_-_2400.11
dd ibs=1 count=$((0x400)) skip=$((0x2800)) if=example01.bin of=polyplay/2_-_2800.7
dd ibs=1 count=$((0x400)) skip=$((0x2c00)) if=example01.bin of=polyplay/2_-_2c00.3
#- ROM_LOAD( "2_-_2000.15",       0x2000, 0x0400, CRC(6c7ad0d8) SHA1(df959d1e43fde96b5e21e3c53b397209a98ea423) )
#- ROM_LOAD( "2_-_2400.11",       0x2400, 0x0400, CRC(bc7462f0) SHA1(01ca680c74b92b9ba5a85f98e0933ef1e754bfc1) )
#- ROM_LOAD( "2_-_2800.7",        0x2800, 0x0400, CRC(9ccf1958) SHA1(6bdf04d7796074af7327fab6717b52736540f97c) )
#- ROM_LOAD( "2_-_2c00.3",        0x2c00, 0x0400, CRC(21827930) SHA1(71d27d68f6973a59996102381f8754d9b353c65a) )

dd ibs=1 count=$((0x400)) skip=$((0x3000)) if=example01.bin of=polyplay/2_-_3000.16
dd ibs=1 count=$((0x400)) skip=$((0x3400)) if=example01.bin of=polyplay/2_-_3400.12
dd ibs=1 count=$((0x400)) skip=$((0x3800)) if=example01.bin of=polyplay/2_-_3800.8
dd ibs=1 count=$((0x400)) skip=$((0x3c00)) if=example01.bin of=polyplay/2_-_3c00.4
#- ROM_LOAD( "2_-_3000.16",       0x3000, 0x0400, CRC(b3b3c0ec) SHA1(a94cd9794d59ea2f9ddd8bef86e6e3a269b276ad) )
#- ROM_LOAD( "2_-_3400.12",       0x3400, 0x0400, CRC(bd416cd0) SHA1(57391cc4a417468455b45014969067629fd629b8) )
#- ROM_LOAD( "2_-_3800.8",        0x3800, 0x0400, CRC(1c470b7c) SHA1(f7c71ee1752ecd4f30a35f14ee392b37febefb9c) )
#- ROM_LOAD( "2_-_3c00.4",        0x3c00, 0x0400, CRC(b8354a19) SHA1(58ea7798ecc1be987b1217f4078c7cb366622dd3) )

dd ibs=1 count=$((0x400)) skip=$((0x4000)) if=example01.bin of=polyplay/2_-_4000.17
dd ibs=1 count=$((0x400)) skip=$((0x4400)) if=example01.bin of=polyplay/2_-_4400.13
dd ibs=1 count=$((0x400)) skip=$((0x4800)) if=example01.bin of=polyplay/2_-_4800.9
dd ibs=1 count=$((0x400)) skip=$((0x4c00)) if=example01.bin of=polyplay/2_-_4c00.5
#- ROM_LOAD( "2_-_4000.17",       0x4000, 0x0400, CRC(1e01041e) SHA1(ff63e4bb924d1c26e445a28c5f8cbc696b4b9f5a) )
#- ROM_LOAD( "2_-_4400.13",       0x4400, 0x0400, CRC(fe4d8959) SHA1(233f97956f4c819558d5d38034d92edc0e86a0de) )
#- ROM_LOAD( "2_-_4800.9",        0x4800, 0x0400, CRC(c45f1d9d) SHA1(f3373f1f5a3c6099fd38e65f66e024ef042a984c) )
#- ROM_LOAD( "2_-_4c00.5",        0x4c00, 0x0400, CRC(26950ad6) SHA1(881f5f0f4806ba6f21d0b28a70fc43363d51419b) )

dd ibs=1 count=$((0x400)) skip=$((0x5000)) if=example01.bin of=polyplay/1_-_5000.30
dd ibs=1 count=$((0x400)) skip=$((0x5400)) if=example01.bin of=polyplay/1_-_5400.26
dd ibs=1 count=$((0x400)) skip=$((0x5800)) if=example01.bin of=polyplay/1_-_5800.22
dd ibs=1 count=$((0x400)) skip=$((0x5c00)) if=example01.bin of=polyplay/1_-_5c00.18
#- ROM_LOAD( "1_-_5000.30",       0x5000, 0x0400, CRC(9f5e2ba1) SHA1(58c696afbda8932f5e401b0a82b2de5cdfc2d1fb) )
#- ROM_LOAD( "1_-_5400.26",       0x5400, 0x0400, CRC(b5f9a780) SHA1(eb785b7668f6af0a9df84cbd1905173869377e6c) )
#- ROM_LOAD( "1_-_5800.22",       0x5800, 0x0400, CRC(d973ad12) SHA1(81cc5e19e83f2e5b10b885583c250a2ff66bafe5) )
#- ROM_LOAD( "1_-_5c00.18",       0x5c00, 0x0400, CRC(9c22ea79) SHA1(e25ed745589a83e297dba936a6e5979f1b31b2d5) )

dd ibs=1 count=$((0x400)) skip=$((0x6000)) if=example01.bin of=polyplay/1_-_6000.31
dd ibs=1 count=$((0x400)) skip=$((0x6400)) if=example01.bin of=polyplay/1_-_6400.27
dd ibs=1 count=$((0x400)) skip=$((0x6800)) if=example01.bin of=polyplay/1_-_6800.23
dd ibs=1 count=$((0x400)) skip=$((0x6c00)) if=example01.bin of=polyplay/1_-_6c00.19
#- ROM_LOAD( "1_-_6000.31",       0x6000, 0x0400, CRC(245c49ca) SHA1(12e5a032327fb45b2a240aff11b0c5d1798932f4) )
#- ROM_LOAD( "1_-_6400.27",       0x6400, 0x0400, CRC(181e427e) SHA1(6b65409cd8410e632093662f5de2989dd9134620) )
#- ROM_LOAD( "1_-_6800.23",       0x6800, 0x0400, CRC(8a6c1f97) SHA1(bf9d4dda8ac933a4a700f52540dcd1197f0a64eb) )
#- ROM_LOAD( "1_-_6c00.19",       0x6c00, 0x0400, CRC(77901dc9) SHA1(b1132e06011aa8f7a95c43f447cd422f01139bb1) )

dd ibs=1 count=$((0x400)) skip=$((0x7000)) if=example01.bin of=polyplay/1_-_7000.32
dd ibs=1 count=$((0x400)) skip=$((0x7400)) if=example01.bin of=polyplay/1_-_7400.28
dd ibs=1 count=$((0x400)) skip=$((0x7800)) if=example01.bin of=polyplay/1_-_7800.24
dd ibs=1 count=$((0x400)) skip=$((0x7c00)) if=example01.bin of=polyplay/1_-_7c00.20
#- ROM_LOAD( "1_-_7000.32",       0x7000, 0x0400, CRC(83ffbe57) SHA1(1e06408f7b4c9a4e5cadab58f6efbc03a5bedc1e) )
#- ROM_LOAD( "1_-_7400.28",       0x7400, 0x0400, CRC(e2a66531) SHA1(1c9eb54e9c8a13f26335d8fb79fe5e39c28b3255) )
#- ROM_LOAD( "1_-_7800.24",       0x7800, 0x0400, CRC(1d0803ef) SHA1(15a1996f9262f26cf531f329e086b10b3c25ce92) )
#- ROM_LOAD( "1_-_7c00.20",       0x7c00, 0x0400, CRC(17dfa7e4) SHA1(afb471dc6cb2faccfb4305540f75162fcee3d622) )

dd ibs=1 count=$((0x400)) skip=$((0x8000)) if=example01.bin of=polyplay/1_-_8000.33
dd ibs=1 count=$((0x400)) skip=$((0x8400)) if=example01.bin of=polyplay/1_-_8400.29
dd ibs=1 count=$((0x400)) skip=$((0x8800)) if=example01.bin of=polyplay/1_-_8800.25
dd ibs=1 count=$((0x400)) skip=$((0x8c00)) if=example01.bin of=polyplay/1_-_8c00.21
#- ROM_LOAD( "1_-_8000.33",       0x8000, 0x0400, CRC(6ee02375) SHA1(fbf797b655639ee442804a30fd3a06bbf261999a) )
#- ROM_LOAD( "1_-_8400.29",       0x8400, 0x0400, CRC(9db09598) SHA1(8eb385542a617b23caad3ce7bbdd9714c1dd684f) )
#- ROM_LOAD( "1_-_8800.25",       0x8800, 0x0400, CRC(ca2f963f) SHA1(34295f02bfd1bca141d650bbbbc1989e01c67b2f) )
#- ROM_LOAD( "1_-_8c00.21",       0x8c00, 0x0400, CRC(0c7dec2d) SHA1(48d776b97c1eca851f89b0c5df4d5765d9aa0319) )










zxb.py library/b1r0f0_charmapminivaders01.zxi --org=$((0x0000))
cat dummy64k.bin >> b1r0f0_charmapminivaders01.bin
dd ibs=1 count=$((0x400)) skip=$((0x0010)) if=b1r0f0_charmapminivaders01.bin of=polyplay/char.1
rm b1r0f0_charmapminivaders01.bin

#- ROM_REGION( 0x800, "gfx1", 0 )
#- ROM_LOAD( "char.1",            0x0000, 0x0400, CRC(5242dd6b) SHA1(ba8f317df62fe4360757333215ce3c8223c68c4e) )
#- ROM_END









#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=polyplay/pp_e02.rom
#rm spritemap01.bin
#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=polyplay/polyplay.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=polyplay/polyplay.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=polyplay/polyplay.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=polyplay/polyplay.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=polyplay/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=polyplay/82s126.3m




rm smsboot.bin dummy64k.bin example01.bin

zip -r polyplay polyplay
rm polyplay/*
rmdir polyplay

mame -w -video soft -resolution0 512x512 -rp ./ polyplay








