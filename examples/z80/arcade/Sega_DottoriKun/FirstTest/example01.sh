rm example01.asm
rm dotrikun.zip
rm dotrikun/*
rmdir dotrikun
mkdir dotrikun
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=dotrikun/14479a.mpr
rm example01.bin

##gfx
#zxb.py library/charmapdotrikun01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmapdotrikun01.bin of=charmapdotrikun01b.bin
#rm charmapdotrikun01.bin
##gfx1
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdotrikun01b.bin of=dotrikun/hg3.m1
##gfx2
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdotrikun01b.bin of=dotrikun/hg1.c14
#dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmapdotrikun01b.bin of=dotrikun/hg2.e14
#rm charmapdotrikun01b.bin
#
##palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=dotrikun/hgb3.l6
#rm palette01b.bin
#
##lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=dotrikun/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=dotrikun/hgb1.h7  #?sp
#rm clut01b.bin
#
##proms
#dd bs=$((0x100)) count=1 if=/dev/urandom of=dotrikun/hgb4.l9
#dd bs=$((0x100)) count=1 if=/dev/urandom of=dotrikun/hgb2.k7

zip -r dotrikun dotrikun
rm dotrikun/*
rmdir dotrikun
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ dotrikun

