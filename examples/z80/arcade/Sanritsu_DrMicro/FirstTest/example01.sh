rm drmicro.zip
rm example01.asm
rm drmicro/*
rmdir drmicro
mkdir drmicro





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=drmicro/dm-00.13b
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=drmicro/dm-01.14b
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=drmicro/dm-02.15b
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=drmicro/dm-03.13d
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=drmicro/dm-04.14d
dd ibs=1 count=$((0x2000)) skip=$((0xA000)) if=example01.bin of=drmicro/dm-05.15d

#'- ROM_START( drmicro )
#'- ROM_REGION( 0x10000, "maincpu", 0 ) // CPU
#'- ROM_LOAD( "dm-00.13b", 0x0000,  0x2000, CRC(270f2145) SHA1(1557428387e2c0f711c676a13a763c8d48aa497b) )
#'- ROM_LOAD( "dm-01.14b", 0x2000,  0x2000, CRC(bba30c80) SHA1(a084429fad58fa6348936084652235d5f55e3b89) )
#'- ROM_LOAD( "dm-02.15b", 0x4000,  0x2000, CRC(d9e4ca6b) SHA1(9fb6d1d6b45628891deae389cf1d142332b110ba) )
#'- ROM_LOAD( "dm-03.13d", 0x6000,  0x2000, CRC(b7bcb45b) SHA1(61035afc642bac2e1c56c36c188bed4e1949523f) )
#'- ROM_LOAD( "dm-04.14d", 0x8000,  0x2000, CRC(071db054) SHA1(75929b7692bebf2246fa84581b6d1eedb02c9aba) )
#'- ROM_LOAD( "dm-05.15d", 0xa000,  0x2000, CRC(f41b8d8a) SHA1(802830f3f0362ec3df257f31dc22390e8ae4207c) )





dd bs=$((0x2000)) count=1 if=/dev/zero of=drmicro/dm-40.12m
dd bs=$((0x2000)) count=1 if=/dev/zero of=drmicro/dm-41.13m

#'- ROM_REGION( 0x04000, "adpcm", 0 ) // samples
#'- ROM_LOAD( "dm-40.12m",  0x0000,  0x2000, CRC(3d080af9) SHA1(f9527fae69fe3ca0762024ac4a44b1f02fbee66a) )
#'- ROM_LOAD( "dm-41.13m",  0x2000,  0x2000, CRC(ddd7bda2) SHA1(bbe9276cb47fa3e82081d592522640e04b4a9223) )





zxb.py library/b2r1f1_charmapmrdo01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f1_charmapmrdo01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin
xxd -p -c 2 -s 0 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > drmicro/dm-23.5l
xxd -p -c 2 -s 1 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > drmicro/dm-24.5n
rm b2r1f1_charmapmrdo01.bin b2r1f1_charmapmrdo01b.bin
#'- ROM_REGION( 0x04000, "gfx1", 0 ) // gfx 1
#'- ROM_LOAD( "dm-23.5l",  0x0000,  0x2000, CRC(279a76b8) SHA1(635650621bdce5873bb5faf64f8352149314e784) )
#'- ROM_LOAD( "dm-24.5n",  0x2000,  0x2000, CRC(ee8ed1ec) SHA1(7afc05c73186af9fe3d3f3ce13412c8ee560b146) )

zxb.py library/b3r1f1_charmaptankbusters01.zxi --org=$((0x0000))
cat dummy32k.bin >> b3r1f1_charmaptankbusters01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b3r1f1_charmaptankbusters01.bin of=b3r1f1_charmaptankbusters01b.bin
xxd -p -c 3 -s 0 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > drmicro/dm-20.4a
xxd -p -c 3 -s 1 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > drmicro/dm-21.4c
xxd -p -c 3 -s 2 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > drmicro/dm-22.4d
rm b3r1f1_charmaptankbusters01.bin b3r1f1_charmaptankbusters01b.bin
#'- ROM_REGION( 0x06000, "gfx2", 0 ) // gfx 2
#'- ROM_LOAD( "dm-20.4a",  0x0000,  0x2000, CRC(6f5dbf22) SHA1(41ef084336e2ebb1016b28505dcb43483e37a0de) )
#'- ROM_LOAD( "dm-21.4c",  0x2000,  0x2000, CRC(8b17ff47) SHA1(5bcc14489ea1d4f1fe8e51c24a72a8e787ab8159) )
#'- ROM_LOAD( "dm-22.4d",  0x4000,  0x2000, CRC(84daf771) SHA1(d187debcca59ceab6cd696be246370120ee575c6) )





#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=drmicro/drmicro.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=drmicro/drmicro.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=drmicro/drmicro.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=drmicro/drmicro.11j
#rm clut01b.bin

dd bs=$((0x20)) count=1 if=/dev/urandom of=drmicro/dm-62.9h
dd bs=$((0x100)) count=1 if=/dev/urandom of=drmicro/dm-61.4m
dd bs=$((0x100)) count=1 if=/dev/urandom of=drmicro/dm-60.6e

#'- ROM_REGION( 0x00220, "proms", 0 ) // PROMs
#'- ROM_LOAD( "dm-62.9h", 0x0000,  0x0020, CRC(e3e36eaf) SHA1(5954400190e587a20cad60f5829f4bddc85ea526) )
#'- ROM_LOAD( "dm-61.4m", 0x0020,  0x0100, CRC(0dd8e365) SHA1(cbd43a2d4af053860932af32ca5e13bef728e38a) )
#'- ROM_LOAD( "dm-60.6e", 0x0120,  0x0100, CRC(540a3953) SHA1(bc65388a1019dadf8c71705e234763f5c735e282) )
#'- ROM_END





#????
#dd bs=256 count=1 if=/dev/zero of=drmicro/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=drmicro/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r drmicro drmicro
rm drmicro/*
rmdir drmicro

mame -w -video soft -resolution0 512x512 -rp ./ drmicro





#'- ROM_START( drmicro )
#'- ROM_REGION( 0x10000, "maincpu", 0 ) // CPU
#'- ROM_LOAD( "dm-00.13b", 0x0000,  0x2000, CRC(270f2145) SHA1(1557428387e2c0f711c676a13a763c8d48aa497b) )
#'- ROM_LOAD( "dm-01.14b", 0x2000,  0x2000, CRC(bba30c80) SHA1(a084429fad58fa6348936084652235d5f55e3b89) )
#'- ROM_LOAD( "dm-02.15b", 0x4000,  0x2000, CRC(d9e4ca6b) SHA1(9fb6d1d6b45628891deae389cf1d142332b110ba) )
#'- ROM_LOAD( "dm-03.13d", 0x6000,  0x2000, CRC(b7bcb45b) SHA1(61035afc642bac2e1c56c36c188bed4e1949523f) )
#'- ROM_LOAD( "dm-04.14d", 0x8000,  0x2000, CRC(071db054) SHA1(75929b7692bebf2246fa84581b6d1eedb02c9aba) )
#'- ROM_LOAD( "dm-05.15d", 0xa000,  0x2000, CRC(f41b8d8a) SHA1(802830f3f0362ec3df257f31dc22390e8ae4207c) )

#'- ROM_REGION( 0x04000, "gfx1", 0 ) // gfx 1
#'- ROM_LOAD( "dm-23.5l",  0x0000,  0x2000, CRC(279a76b8) SHA1(635650621bdce5873bb5faf64f8352149314e784) )
#'- ROM_LOAD( "dm-24.5n",  0x2000,  0x2000, CRC(ee8ed1ec) SHA1(7afc05c73186af9fe3d3f3ce13412c8ee560b146) )
#'- ROM_REGION( 0x06000, "gfx2", 0 ) // gfx 2
#'- ROM_LOAD( "dm-20.4a",  0x0000,  0x2000, CRC(6f5dbf22) SHA1(41ef084336e2ebb1016b28505dcb43483e37a0de) )
#'- ROM_LOAD( "dm-21.4c",  0x2000,  0x2000, CRC(8b17ff47) SHA1(5bcc14489ea1d4f1fe8e51c24a72a8e787ab8159) )
#'- ROM_LOAD( "dm-22.4d",  0x4000,  0x2000, CRC(84daf771) SHA1(d187debcca59ceab6cd696be246370120ee575c6) )

#'- ROM_REGION( 0x04000, "adpcm", 0 ) // samples
#'- ROM_LOAD( "dm-40.12m",  0x0000,  0x2000, CRC(3d080af9) SHA1(f9527fae69fe3ca0762024ac4a44b1f02fbee66a) )
#'- ROM_LOAD( "dm-41.13m",  0x2000,  0x2000, CRC(ddd7bda2) SHA1(bbe9276cb47fa3e82081d592522640e04b4a9223) )

#'- ROM_REGION( 0x00220, "proms", 0 ) // PROMs
#'- ROM_LOAD( "dm-62.9h", 0x0000,  0x0020, CRC(e3e36eaf) SHA1(5954400190e587a20cad60f5829f4bddc85ea526) )
#'- ROM_LOAD( "dm-61.4m", 0x0020,  0x0100, CRC(0dd8e365) SHA1(cbd43a2d4af053860932af32ca5e13bef728e38a) )
#'- ROM_LOAD( "dm-60.6e", 0x0120,  0x0100, CRC(540a3953) SHA1(bc65388a1019dadf8c71705e234763f5c735e282) )
#'- ROM_END





