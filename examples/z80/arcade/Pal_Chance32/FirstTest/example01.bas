asm
  ld sp,$DFFF
  end asm

#include "./library/pacmandelay.bas"
#include "./library/smsrnd.bas"

dim eee as uinteger at $D810
dim seed as uinteger at $D812
dim i as uinteger at $D814
dim ee0 as uinteger at $D816
dim ee1 as uinteger at $D818
dim ee2 as uinteger at $D81A
dim ee3 as uinteger at $D81C
dim ee4 as ubyte at $D81E
dim ecsp as ubyte at $D81F
dim ecsq as ubyte at $D820
dim delay as uinteger at $D822
dim ex0 as uinteger at $D824
dim ey0 as uinteger at $D826

seed=0
  
sub chance32putchar(txp1 as uinteger,typ1 as uinteger,tch1 as ubyte,tat1 as ubyte,tla1 as uinteger):
  poke $F000+($800*(tla1 band 1))+((28-typ1)*70)+((34-txp1)*2),tch1
  poke $F001+($800*(tla1 band 1))+((28-typ1)*70)+((34-txp1)*2),tat1
  end sub

sub chance32palette(tid2 as uinteger,tvl2 as uinteger):
  poke uinteger $E000+(tid2*2),tvl2
  end sub

sub chance32palettergb(tid3 as uinteger,tvl3 as uinteger):
  dim tvz3 as uinteger at $D800
  tvz3=((tvl3 band $00F)*2)
  tvz3=tvz3 bor ((tvl3 band $0F0)*128)
  tvz3=tvz3 bor ((tvl3 band $F00)/4)
  poke uinteger $E000+(tid3*2),tvz3
  end sub

chance32palettergb(0,$F00)
chance32palettergb(1,$0F0)
chance32palettergb(2,$00F)

for eee=0 to 255
  seed=smsrnd(seed)
  chance32palettergb(eee,seed)
  next

for ee0=0 to 28
  for ee1=0 to 34
    seed=smsrnd(seed)
    chance32putchar(ee1,ee0,seed band $F,$10,0)
    pacmandelay(100)
    next:next

for ee0=0 to 15
  for ee1=0 to 15
    chance32putchar(ee1+18,ee0+12,ee0*16+ee1,$10,0)
    pacmandelay(100)
    next:next

eee=0:ex0=8:ey0=8
do
chance32putchar(1,1,48+int(eee/10000) mod 10,$10,0)
chance32putchar(2,1,48+int(eee/1000) mod 10,$10,0)
chance32putchar(3,1,48+int(eee/100) mod 10,$10,0)
chance32putchar(4,1,48+int(eee/10) mod 10,$10,0)
chance32putchar(5,1,48+eee mod 10,$10,0)

out $13,1:ee0=in($25)
chance32putchar(3,3,48+int(ee0/100) mod 10,$10,0)
chance32putchar(4,3,48+int(ee0/10) mod 10,$10,0)
chance32putchar(5,3,48+ee0 mod 10,$10,0)

out $13,2:ee0=in($25)
chance32putchar(3,4,48+int(ee0/100) mod 10,$10,0)
chance32putchar(4,4,48+int(ee0/10) mod 10,$10,0)
chance32putchar(5,4,48+ee0 mod 10,$10,0)

ex0=ex0+((ee0 band 1)/1)-((ee0 band 2)/2)
ey0=ey0+((ee0 band 4)/4)-((ee0 band 8)/8)

chance32putchar(ex0,ey0,eee band 127,$10,0)

eee=eee+1
loop


do:loop

do
for eee=$E000 to $EFFF 'palette
  seed=smsrnd(seed)
  poke eee,seed
  next
  
poke uinteger $E000,$369C
poke uinteger $E000,$6930 'grbx?
  
for eee=$F000 to $F7FF step 2 'layer1
  seed=smsrnd(seed)
  poke eee,seed band 127
  poke eee+1,$10
  pacmandelay(1000)
  next
loop






do
for eee=$E000 to $EFFF '
  seed=smsrnd(seed)
  poke eee,seed
  next
for eee=$F000 to $F7FF step 2 'palette
  seed=smsrnd(seed)
  poke eee,seed band 127
  poke eee+1,$10
  pacmandelay(1000)
  next
for eee=$F800 to $FFFF 'palette
  seed=smsrnd(seed)
  poke eee,seed
  pacmandelay(1000)
  next
loop


do
for eee=$E000 to $FFFF
  seed=smsrnd(seed)
  poke eee,seed
  next
loop


'- static ADDRESS_MAP_START( chance32_map, AS_PROGRAM, 8, chance32_state )
'- AM_RANGE(0x0000, 0xcfff) AM_ROM
'- AM_RANGE(0xd800, 0xdfff) AM_RAM
'- AM_RANGE(0xe000, 0xefff) AM_RAM_DEVWRITE("palette", palette_device, write) AM_SHARE("palette")
'- AM_RANGE(0xf000, 0xf7ff) AM_RAM_WRITE(chance32_fgram_w) AM_SHARE("fgram")
'- AM_RANGE(0xf800, 0xffff) AM_RAM_WRITE(chance32_bgram_w) AM_SHARE("bgram")
'- ADDRESS_MAP_END

'- static ADDRESS_MAP_START( chance32_portmap, AS_IO, 8, chance32_state )
'- ADDRESS_MAP_GLOBAL_MASK(0xff)
'- AM_RANGE(0x10, 0x10) AM_WRITENOP        // writing bit3 constantly... watchdog?
'- AM_RANGE(0x13, 0x13) AM_WRITE(mux_w)
'- AM_RANGE(0x20, 0x20) AM_READ_PORT("DSW0")
'- AM_RANGE(0x21, 0x21) AM_READ_PORT("DSW1")
'- AM_RANGE(0x22, 0x22) AM_READ_PORT("DSW2")
'- AM_RANGE(0x23, 0x23) AM_READ_PORT("DSW3")
'- AM_RANGE(0x24, 0x24) AM_READ_PORT("DSW4")
'- AM_RANGE(0x25, 0x25) AM_READ(mux_r)
'- AM_RANGE(0x26, 0x26) AM_READ_PORT("UNK") // vblank (other bits are checked for different reasons)
'- AM_RANGE(0x30, 0x30) AM_DEVWRITE("crtc", mc6845_device, address_w)
'- AM_RANGE(0x31, 0x31) AM_DEVWRITE("crtc", mc6845_device, register_w)
'- AM_RANGE(0x50, 0x50) AM_DEVREADWRITE("oki", okim6295_device, read, write)
'- AM_RANGE(0x60, 0x60) AM_WRITE(muxout_w)
'- ADDRESS_MAP_END



'----------------------------------


'- static ADDRESS_MAP_START( shisen_map, AS_PROGRAM, 8, shisen_state )
'- AM_RANGE(0x0000, 0x7fff) AM_ROM
'- AM_RANGE(0x8000, 0xbfff) AM_ROMBANK("bank1")
'- AM_RANGE(0xc800, 0xcaff) AM_RAM_WRITE(paletteram_w) AM_SHARE("paletteram")
'- AM_RANGE(0xd000, 0xdfff) AM_RAM_WRITE(videoram_w) AM_SHARE("videoram")
'- AM_RANGE(0xe000, 0xffff) AM_RAM



'
'dim ex0 as uinteger at $9110
'dim ey0 as uinteger at $9112
'
'dim ej0 as uinteger at $9114
'dim ej1 as uinteger at $9116
'
'sub pingpongputsprite(tlayer as ubyte,tx as ubyte,ty as ubyte,tid as ubyte,tclut as ubyte):
'  poke $9006+((tlayer band 15)*4),tx  '- x sprite 0
'  poke $9004+((tlayer band 15)*4),ty bxor 255  '- y sprite 0
'  poke $9005+((tlayer band 15)*4),tid          '- shape sprite 0
'  poke $9003+((tlayer band 15)*4),tclut        '- clut? sprite 0
'  end sub
'
'function test1(t1 as uinteger,t2 as uinteger,t3 as uinteger,t4 as uinteger)  as uinteger
'  end function
'
'sub zpoke(t1 as uinteger,t2 as ubyte)
'  poke t1,t2
'  end sub
'
'sub putchar(tx1 as uinteger, ty1 as uinteger, tch1 as uinteger)
'  poke ($8400+(tx1*32)+ty1) bxor %0000001111100000,(tch1 band 255)
'  end sub
'
'pingpongputsprite(0,21,10,4,3)
'pingpongputsprite(1,21,30,4,3)
'pingpongputsprite(2,21,50,4,3)
'pingpongputsprite(3,21,70,4,3)
'pingpongputsprite(4,21,90,4,3)
'pingpongputsprite(5,21,110,4,3)
'pingpongputsprite(6,21,130,4,3)
'pingpongputsprite(7,21,150,4,3)
'
'seed=0
'
'for ee2=64 to 1023-64
'  for ee3=1 to 100
'    poke $8400+ee2,$01
'    next:next
'
'ee4=0
'for ee2=3 to 3+15
'  for ee3=15 to 15+15
'    poke $8400+ee3+(ee2*32),ee4
'    ee4=ee4+1
'    next:next
'
'ee4=0
'for ee2=20 to 20+7
'  for ee3=15 to 15+15
'    poke $8400+ee3+(ee2*32),ee4 band 3
'    poke $8000+ee3+(ee2*32),int(ee4/4)
'    ee4=ee4+1
'    next:next
'
'
'
'
'for ee2=4 to 7
'  for ee3=2 to 5
'    poke $8400+ee3+(ee2*32),code("X")
'    next
'    next
'
'test1(377,2445,63888,2342)
'
'eee=$4207
'i=code("Q")
'poke eee,i
'eee=eee+1
'i=code("T")
'poke eee,i
'
'seed=0
'for eee=$4400 to $47FF
'  seed=smsrnd(seed)
'  poke eee,seed
'  next
'
'ex0=80:ey0=176
'
'eee=0
'ecsp=0:ecsq=0
'
'
'do
'poke $A600,255 '- watchdog? - it seems to work! thanks, Jim Bagley!!!!
'
'ej0=peek($A800) bxor $ff
'ej1=peek($A880) bxor $ff
'
'poke ($8400+(10*32)+6) bxor %0000001111100000,ej0
'poke ($8400+(10*32)+7) bxor %0000001111100000,ej1
'
'poke ($8400+(12*32)+8) bxor %0000001111100000,48+(ex0 mod 10)
'poke ($8400+(12*32)+7) bxor %0000001111100000,48+((int(ex0/10)) mod 10)
'poke ($8400+(12*32)+6) bxor %0000001111100000,48+((int(ex0/100)) mod 10)
'
'poke ($8400+(13*32)+8) bxor %0000001111100000,48+(ey0 mod 10)
'poke ($8400+(13*32)+7) bxor %0000001111100000,48+((int(ey0/10)) mod 10)
'poke ($8400+(13*32)+6) bxor %0000001111100000,48+((int(ey0/100)) mod 10)
'
'poke ($8400+(14*32)+8) bxor %0000001111100000,48+(eee mod 10)
'poke ($8400+(14*32)+7) bxor %0000001111100000,48+((int(eee/10)) mod 10)
'poke ($8400+(14*32)+6) bxor %0000001111100000,48+((int(eee/100)) mod 10)
'poke ($8400+(14*32)+5) bxor %0000001111100000,48+((int(eee/1000)) mod 10)
'poke ($8400+(14*32)+4) bxor %0000001111100000,48+((int(eee/10000)) mod 10)
'
'poke ($8400+(15*32)+8) bxor %0000001111100000,48+(ej0 mod 10)
'poke ($8400+(15*32)+7) bxor %0000001111100000,48+((int(ej0/10)) mod 10)
'poke ($8400+(15*32)+6) bxor %0000001111100000,48+((int(ej0/100)) mod 10)
'
'poke ($8400+(16*32)+8) bxor %0000001111100000,48+(ej1 mod 10)
'poke ($8400+(16*32)+7) bxor %0000001111100000,48+((int(ej1/10)) mod 10)
'poke ($8400+(16*32)+6) bxor %0000001111100000,48+((int(ej1/100)) mod 10)
'
'poke ($8400+(17*32)+8) bxor %0000001111100000,48+(ecsp mod 10)
'
'if (ej1 band 64)<>0 then: ex0=ex0+1:end if
'if (ej1 band 32)<>0 then: ex0=ex0-1:end if
'if (ej1 band 4)<>0 then: ey0=ey0+1:end if
'if (ej1 band 2)<>0 then: ey0=ey0-1:end if
'
'if (ej1 band 15)<>0 then: ecsq=ecsq+1:end if
'ecsp=(int(ecsq/4))mod 8
'
'ee3=int(eee/4)
'
'pingpongputsprite( 0,ex0   ,ey0   ,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 1,ex0+16,ey0   ,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 2,ex0+32,ey0   ,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 3,ex0+48,ey0   ,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 4,ex0   ,ey0+16,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 5,ex0+16,ey0+16,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 6,ex0+32,ey0+16,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 7,ex0+48,ey0+16,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 8,ex0   ,ey0+32,4,3)  '- layer,x,y,id,clut
'pingpongputsprite( 9,ex0+16,ey0+32,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(10,ex0+32,ey0+32,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(11,ex0+48,ey0+32,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(12,ex0   ,ey0+48,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(13,ex0+16,ey0+48,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(14,ex0+32,ey0+48,4,3)  '- layer,x,y,id,clut
'pingpongputsprite(15,ex0+48,ey0+48,4,3)  '- layer,x,y,id,clut
'
'
'eee=eee+1
'loop
'
''-------------------------------------------------------------------------------
'
'text01:
'asm
'  defb "HELLO WORLD!!!!!"
'  end asm
'
''-------------------------------------------------------------------------------
'
'
