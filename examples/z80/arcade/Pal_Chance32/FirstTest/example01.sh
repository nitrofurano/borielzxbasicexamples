rm chance32.zip
rm example01.asm
rm chance32/*
rmdir chance32
mkdir chance32

dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin

#- zxb.py example01.bas --asm --org=$((0x0000))
#- zxb.py example01.bas --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=example01.bin of=chance32/0.u52

#'- ROM_START( chance32 )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "0.u52", 0x00000, 0x10000, CRC(331048b2) SHA1(deb4da570b3efe6e15deefb6351f925b642b4614)  )

dd bs=$((0x40000)) count=1 if=/dev/zero of=chance32/8.u21

#'- ROM_REGION( 0x40000, "oki", 0 )
#'- ROM_LOAD( "8.u21", 0x00000, 0x40000, CRC(161b35dd) SHA1(d20a75a4c4ed9cd9cfc12faee921122274840f06) )

#zxb.py library/b8r0f0e0_charmapskylancer01.zxi --org=$((0x0000))
#cat dummy128k.bin >> b8r0f0e0_charmapskylancer01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/pp_e02.rom
#rm b8r0f0e0_charmapskylancer01.bin

zxb.py library/b8r0f0e0_charmapskylancer01.zxi --org=$((0x0000))
cat dummy128k.bin >> b8r0f0e0_charmapskylancer01.bin
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/4.u64
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/5.u65
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/6.u66
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/7.u67
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/1.u71
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=b8r0f0e0_charmapskylancer01.bin of=chance32/2.u72
rm b8r0f0e0_charmapskylancer01.bin

#'- ROM_REGION( 0x80000, "gfx1", ROMREGION_INVERT )
#'- ROM_LOAD16_BYTE( "4.u64", 0x00001, 0x20000, CRC(da80d9bd) SHA1(3b5235ab59fd55f0ec5584b3cf1aa5c8f36c76f6) )
#'- ROM_LOAD16_BYTE( "5.u65", 0x00000, 0x20000, CRC(7528773b) SHA1(95c8e55cdec2c5c1dcdcc5a7edc6e590e3829f92) )
#'- ROM_LOAD16_BYTE( "6.u66", 0x40001, 0x20000, CRC(cee2ffb0) SHA1(527c2072d39484317b0320afd975df1bbe244a01) )
#'- ROM_LOAD16_BYTE( "7.u67", 0x40000, 0x20000, CRC(42dc4b69) SHA1(44c8f902db4c7ac235d5ea15d1b509f98663690a) )
#'- ROM_REGION( 0x40000, "gfx2", ROMREGION_INVERT )
#'- ROM_LOAD16_BYTE( "1.u71", 0x00001, 0x20000, CRC(f8e85873) SHA1(6ad24f7fcbc62a03180e168d70239df1ce662f0d) )
#'- ROM_LOAD16_BYTE( "2.u72", 0x00000, 0x20000, CRC(860b534d) SHA1(44649ea93acdf173356bfcd7e81916253b52c378) )

#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy128k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=chance32/chance32.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=chance32/chance32.3j

#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy128k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=chance32/chance32.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=chance32/chance32.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=chance32/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=chance32/82s126.3m

#ssamples
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u53.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u54.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u55.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u56.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u57.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal20v8a.u58.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal16v8a.u47.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal16v8a.u48.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal16v8a.u32.jed.bin
dd bs=$((0x0157)) count=1 if=/dev/urandom of=chance32/gal16v8a.u24.jed.bin

#'- ROM_REGION( 0x40000, "gals", 0 ) // no idea if these are any good
#'- ROM_LOAD( "gal20v8a.u53.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u54.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u55.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u56.jed.bin", 0x0000, 0x157, CRC(6bab01ad) SHA1(c69e4be41a989a52788af8062f48bbe26bc3dab8) )
#'- ROM_LOAD( "gal20v8a.u57.jed.bin", 0x0000, 0x157, CRC(787c4159) SHA1(f4a869b317c6be1024f1ca21bcc4af478c8227c8) )
#'- ROM_LOAD( "gal20v8a.u58.jed.bin", 0x0000, 0x157, CRC(7b16053b) SHA1(cdb289d4f27c7a1a918393943bb8db9712e2f52e) )
#'- ROM_LOAD( "gal16v8a.u47.jed.bin", 0x0000, 0x117, CRC(a733f0de) SHA1(6eec26043cedb3cae4efe93faa84a07327be468b) )
#'- ROM_LOAD( "gal16v8a.u48.jed.bin", 0x0000, 0x117, CRC(5f1360ef) SHA1(56e4ee0dbae5602d810b2f7c744a71eb1a1e08a8) )
#'- ROM_LOAD( "gal16v8a.u32.jed.bin", 0x0000, 0x117, CRC(c0784cd1) SHA1(0ae2ce482d379e29c2a9f130fc0d9ed928faef98) )
#'- ROM_LOAD( "gal16v8a.u24.jed.bin", 0x0000, 0x117, CRC(a733f0de) SHA1(6eec26043cedb3cae4efe93faa84a07327be468b) )
#'- ROM_END

rm smsboot.bin dummy128k.bin example01.bin

zip -r chance32 chance32
rm chance32/*
rmdir chance32

mame -w -video soft -resolution0 512x512 -rp ./ chance32

#'- ROM_START( chance32 )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "0.u52", 0x00000, 0x10000, CRC(331048b2) SHA1(deb4da570b3efe6e15deefb6351f925b642b4614)  )

#'- ROM_REGION( 0x80000, "gfx1", ROMREGION_INVERT )
#'- ROM_LOAD16_BYTE( "4.u64", 0x00001, 0x20000, CRC(da80d9bd) SHA1(3b5235ab59fd55f0ec5584b3cf1aa5c8f36c76f6) )
#'- ROM_LOAD16_BYTE( "5.u65", 0x00000, 0x20000, CRC(7528773b) SHA1(95c8e55cdec2c5c1dcdcc5a7edc6e590e3829f92) )
#'- ROM_LOAD16_BYTE( "6.u66", 0x40001, 0x20000, CRC(cee2ffb0) SHA1(527c2072d39484317b0320afd975df1bbe244a01) )
#'- ROM_LOAD16_BYTE( "7.u67", 0x40000, 0x20000, CRC(42dc4b69) SHA1(44c8f902db4c7ac235d5ea15d1b509f98663690a) )
#'- ROM_REGION( 0x40000, "gfx2", ROMREGION_INVERT )
#'- ROM_LOAD16_BYTE( "1.u71", 0x00001, 0x20000, CRC(f8e85873) SHA1(6ad24f7fcbc62a03180e168d70239df1ce662f0d) )
#'- ROM_LOAD16_BYTE( "2.u72", 0x00000, 0x20000, CRC(860b534d) SHA1(44649ea93acdf173356bfcd7e81916253b52c378) )

#'- ROM_REGION( 0x40000, "oki", 0 )
#'- ROM_LOAD( "8.u21", 0x00000, 0x40000, CRC(161b35dd) SHA1(d20a75a4c4ed9cd9cfc12faee921122274840f06) )

#'- ROM_REGION( 0x40000, "gals", 0 ) // no idea if these are any good
#'- ROM_LOAD( "gal20v8a.u53.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u54.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u55.jed.bin", 0x0000, 0x157, CRC(9f680800) SHA1(2fa41ead85136e851d465432a7b9d3ec848c7a22) )
#'- ROM_LOAD( "gal20v8a.u56.jed.bin", 0x0000, 0x157, CRC(6bab01ad) SHA1(c69e4be41a989a52788af8062f48bbe26bc3dab8) )
#'- ROM_LOAD( "gal20v8a.u57.jed.bin", 0x0000, 0x157, CRC(787c4159) SHA1(f4a869b317c6be1024f1ca21bcc4af478c8227c8) )
#'- ROM_LOAD( "gal20v8a.u58.jed.bin", 0x0000, 0x157, CRC(7b16053b) SHA1(cdb289d4f27c7a1a918393943bb8db9712e2f52e) )
#'- ROM_LOAD( "gal16v8a.u47.jed.bin", 0x0000, 0x117, CRC(a733f0de) SHA1(6eec26043cedb3cae4efe93faa84a07327be468b) )
#'- ROM_LOAD( "gal16v8a.u48.jed.bin", 0x0000, 0x117, CRC(5f1360ef) SHA1(56e4ee0dbae5602d810b2f7c744a71eb1a1e08a8) )
#'- ROM_LOAD( "gal16v8a.u32.jed.bin", 0x0000, 0x117, CRC(c0784cd1) SHA1(0ae2ce482d379e29c2a9f130fc0d9ed928faef98) )
#'- ROM_LOAD( "gal16v8a.u24.jed.bin", 0x0000, 0x117, CRC(a733f0de) SHA1(6eec26043cedb3cae4efe93faa84a07327be468b) )
#'- ROM_END
