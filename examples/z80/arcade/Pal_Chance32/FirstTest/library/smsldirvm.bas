sub smsldirvm(tvram as uinteger, tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
    ;call $005C

    ld a,e
    out ($bf),a
    ;ld a,$78     ;- ld a,$38|$40 
    ld a,d
    or $40
    out ($bf),a
    ; 2. Output tilemap data
    ;ld hl,Message
    ;ld bc,MessageEnd-Message  ; Counter for number of bytes to write

    smsldirvmloop:
    ld a,(hl)    ; Get data byte
    out ($be),a
    inc hl       ; Point to next letter
    dec bc
    ld a,b
    or c
    jp nz,smsldirvmloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
