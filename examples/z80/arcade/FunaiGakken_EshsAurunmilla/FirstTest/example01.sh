rm example01.asm
rm esh.zip
rm esh/*
rmdir esh
mkdir esh



dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=esh/is1.h8
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=esh/is2.f8
rm example01.bin

#- ROM_START( esh )
#- /* Main program CPU */
#- ROM_REGION( 0x4000, "maincpu", 0 )
#- ROM_LOAD( "is1.h8", 0x0000, 0x2000, CRC(114c912b) SHA1(7c033a102d046199f3e2c6787579dac5b5295d50) )
#- ROM_LOAD( "is2.f8", 0x2000, 0x2000, CRC(0e3b6e62) SHA1(5e8160180e20705e727329f9d70305fcde176a25) )






#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1

xxd -p -c 3 -s 0 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > esh/a.m3
xxd -p -c 3 -s 1 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > esh/b.l3
xxd -p -c 3 -s 2 -l $((0x3000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > esh/c.k3




#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=esh/a.m3
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=esh/b.l3
#dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=esh/c.k3
rm charmap01b.bin

#- /* Tiles */
#- ROM_REGION( 0x3000, "gfx1", 0 )
#- ROM_LOAD( "a.m3", 0x0000, 0x1000, CRC(a04736d8) SHA1(3b642b5d7168cf4a09328eee54c532be815d2bcf) )
#- ROM_LOAD( "b.l3", 0x1000, 0x1000, CRC(9366dde7) SHA1(891db65384d47d13355b2eea37f57c34bc775c8f) )
#- ROM_LOAD( "c.k3", 0x2000, 0x1000, CRC(a936ef01) SHA1(bcacb281ccb72ceb57fb6a79380cc3a9688743c4) )








#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0000)) if=palette01b.bin of=esh/rgb.j1
rm palette01b.bin

#dd bs=$((0x200)) count=1 if=/dev/urandom of=esh/rgb.j1 #- palette?






#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=esh/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=esh/hgb1.h7  #?sp
#rm clut01b.bin

#proms
dd bs=$((0x100)) count=1 if=/dev/zero of=esh/h.c5      #- timing?
dd bs=$((0x100)) count=1 if=/dev/zero of=esh/v.c6      #- ?????


#- /* Color (+other) PROMs */
#- ROM_REGION( 0x400, "proms", 0 )
#- ROM_LOAD( "rgb.j1", 0x000, 0x200, CRC(1e9f795f) SHA1(61a58694929fa39b2412bc9244e5681d65a0eacb) )
#- ROM_LOAD( "h.c5",   0x200, 0x100, CRC(abde5e4b) SHA1(9dd3a7fd523b519ac613b9f08ae9cc962992cf5d) )    /* Video timing? */
#- ROM_LOAD( "v.c6",   0x300, 0x100, CRC(7157ba22) SHA1(07355f30efe46196d216356eda48a59fc622e43f) )









#laserdisk

dd bs=$((0x100)) count=1 if=/dev/zero of=esh/esh


#- DISK_REGION( "laserdisc" )
#- DISK_IMAGE_READONLY( "esh", 0, NO_DUMP )







#????????

dd bs=$((0x2000)) count=1 if=/dev/zero of=esh/z03_1001_vyw-053_v1-0.bin







zip -r esh esh
rm esh/*
rmdir esh
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ esh










