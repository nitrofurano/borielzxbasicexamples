#include "library/_sms_setz80stackpoint.bas"
#include "library/smsvdp.bas"
#include "library/smsfilvrm.bas"
#include "library/smsldirvm.bas"
#include "library/smsscroll.bas"
#include "library/smsvpoke.bas"
#include "library/smsrnd.bas"
#include "library/smsjoypad.bas"
#include "library/smspalette.bas"
#include "library/smsdelay.bas"

'#include "library/ncsax27e8_256x64_tiles.bas"
#include "library/ch01v16c_256x64_tiles.bas"
'#include "library/abm4scr4_256x64_tiles.bas"

'#include "library/6x8charset01.bas"

start01:

'- mode 4,2
smsvdp(0,%00000100):smsvdp(1,%11100010) '- smsvdp(1,$84)
smsvdp(2,$ff):smsvdp(5,$ff):smsvdp(10,$ff)
smsvdp(6,$fb) '- sprite patterns - $fb for $0000 (256 sprites available), $ff for $2000 (192 sprites available)
smsvdp(7,$00) '- border colour (sprite palette)

'smsscroll($30,$c8) '- (for locating the gamegear display on the top-left edge of pattern area)

smsfilvrm($0000,0,$4000) '- replaces "clear vram"
'smsldirvm($0800,@charset01,2048)

'-------------------------------------------------------------------------------

dim ee1 as uinteger at $c010
dim ee2 as uinteger at $c012
dim ee3 as uinteger at $c014

'-------------------------------------------------------------------------------

'eesd=0
for ee2=0 to 15
  smspalette(ee2,peek(@palette01+ee2))
  smspalette(ee2+16,peek(@palette01+ee2))
  next

'-------------------------------------------------------------------------------

ee3=0
for ee2=0 to 767
  smsvpoke($3800+(ee2*2),ee3 band $FF)
  ee3=ee3+1
  next

'-------------------------------------------------------------------------------

for ee2=0 to $1FFF
  smsvpoke(ee2,peek(@charset01+ee2))
  next

for ee2=0 to $17FF
  smsvpoke(ee2+$2000,peek(@charset01+ee2))
  next

'-------------------------------------------------------------------------------

for ee2=$3E00 to $3FFF
  ee1=smsrnd(ee1)
  smsvpoke(ee2,ee1)
  smsvpoke(ee2,15)
  next

'-------------------------------------------------------------------------------

for ee2=$3F07 to $3F3F step 8
  'ee1=smsrnd(ee1)
  smsvpoke(ee2,$D0)
  next

'-------------------------------------------------------------------------------

loopqq1q:

for ee2=0 to 255
  smsdelay(5000)
  smsvpoke($3F00,160)
  smsvpoke($3F80,ee2)
  smsvpoke($3F81,ee2 band $FC)
  smsvpoke($3F01,160)
  smsvpoke($3F82,ee2+8)
  smsvpoke($3F83,(ee2 band $FC) bor 2)
  smsvpoke($3F07,$D0)
  next

  goto loopqq1q

'-------------------------------------------------------------------------------

testch01aaa:
asm 
  end asm

'-------------------------------------------------------------------------------
''- mode 2,2 - vdp entries for screen1 - base vram addresses
'smsvdp(0,$02):smsvdp(1,$E2) '- screen1,2
'smsvdp(2,$06):smsvdp(3,$FF):smsvdp(4,$03):smsvdp(5,$36):smsvdp(6,$07)
'smsvdp(7,$F4) 'smsvdp(7,F*16+4) '- ink*16+border - color(3,?,1)
'-------------------------------------------------------------------------------


