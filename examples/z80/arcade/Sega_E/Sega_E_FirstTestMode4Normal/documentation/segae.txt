


static ADDRESS_MAP_START( io_map, AS_IO, 8, systeme_state )
	ADDRESS_MAP_GLOBAL_MASK(0xff)
	AM_RANGE(0x7b, 0x7b) AM_DEVWRITE("sn1", segapsg_device, write )
	AM_RANGE(0x7e, 0x7f) AM_DEVWRITE("sn2", segapsg_device, write )
	AM_RANGE(0x7e, 0x7e) AM_DEVREAD( "vdp1", sega315_5124_device, vcount_read )
	AM_RANGE(0xba, 0xba) AM_DEVREADWRITE( "vdp1", sega315_5124_device, vram_read, vram_write )
	AM_RANGE(0xbb, 0xbb) AM_DEVREADWRITE( "vdp1", sega315_5124_device, register_read, register_write )
	AM_RANGE(0xbe, 0xbe) AM_DEVREADWRITE( "vdp2", sega315_5124_device, vram_read, vram_write )
	AM_RANGE(0xbf, 0xbf) AM_DEVREADWRITE( "vdp2", sega315_5124_device, register_read, register_write )
	AM_RANGE(0xe0, 0xe0) AM_READ_PORT( "e0" )
	AM_RANGE(0xe1, 0xe1) AM_READ_PORT( "e1" )
	AM_RANGE(0xe2, 0xe2) AM_READ_PORT( "e2" )
	AM_RANGE(0xf2, 0xf2) AM_READ_PORT( "f2" )
	AM_RANGE(0xf3, 0xf3) AM_READ_PORT( "f3" )
	AM_RANGE(0xf7, 0xf7) AM_WRITE( bank_write )
ADDRESS_MAP_END


static ADDRESS_MAP_START( systeme_map, AS_PROGRAM, 8, systeme_state )
	AM_RANGE(0x0000, 0x7fff) AM_ROM                                                     /* Fixed ROM */
	AM_RANGE(0x8000, 0xbfff) AM_ROMBANK("bank1") AM_WRITE( videoram_write )             /* Banked ROM */
	AM_RANGE(0xc000, 0xffff) AM_RAM
ADDRESS_MAP_END


ROM_START( ridleofp )
	ROM_REGION( 0x30000, "maincpu", 0 )
	ROM_LOAD( "epr10426.bin",   0x00000, 0x08000, CRC(4404c7e7) SHA1(555f44786976a009d96a6395c9173929ad6138a7) ) /* Fixed Code */
	/* The following are 8 0x4000 banks that get mapped to reads from 0x8000 - 0xbfff */
	ROM_LOAD( "epr10425.bin",   0x10000, 0x08000, CRC(35964109) SHA1(a7bc64a87b23139b0edb9c3512f47dcf73feb854) )
	ROM_LOAD( "epr10424.bin",   0x18000, 0x08000, CRC(fcda1dfa) SHA1(b8497b04de28fc0d6b7cb0206ad50948cff07840) )
	ROM_LOAD( "epr10423.bin",   0x20000, 0x08000, CRC(0b87244f) SHA1(c88041614735a9b6cba1edde0a11ed413e115361) )
	ROM_LOAD( "epr10422.bin",   0x28000, 0x08000, CRC(14781e56) SHA1(f15d9d89e1ebff36c3867cfc8f0bdf7f6b3c96bc) )
ROM_END


ROM_START( transfrm )
	ROM_REGION( 0x30000, "maincpu", 0 )
	ROM_LOAD( "ic7.top",    0x00000, 0x08000, CRC(ccf1d123) SHA1(5ade9b00e2a36d034fafdf1902d47a9a00e96fc4) ) /* Fixed Code */
	/* The following are 8 0x4000 banks that get mapped to reads from 0x8000 - 0xbfff */
	ROM_LOAD( "epr-7347.ic5",   0x10000, 0x08000, CRC(df0f639f) SHA1(a09a9841b66de246a585be63d911b9a42a323503) )
	ROM_LOAD( "epr-7348.ic4",   0x18000, 0x08000, CRC(0f38ea96) SHA1(d4d421c5d93832e2bc1f22f39dffb6b80f2750bd) )
	ROM_LOAD( "ic3.top",        0x20000, 0x08000, CRC(9d485df6) SHA1(b25f04803c8f7188021f3039aa13aac80d480823) )
	ROM_LOAD( "epr-7350.ic2",   0x28000, 0x08000, CRC(0052165d) SHA1(cf4b5dffa54238e513515b3fc90faa7ce0b65d34) )
ROM_END

> ridleofp - fixed - rotated - 160kb
> transfrm - fixed - normal - 160kb
- hangonjr - fixed - normal - 160kb
- tetrisse - fixed - normal - 96kb
- astrofl - encrypted - normal - 160kb
- fantzn2 - encrypted - normal - 296kb
- opaopa  - encrypted - normal - 168kb
