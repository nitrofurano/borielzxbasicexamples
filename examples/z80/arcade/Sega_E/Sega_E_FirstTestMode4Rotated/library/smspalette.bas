sub smspalette(tad as ubyte, tvl as ubyte): '- format: $BGR 6bit
  asm
    ld d,(ix+7)
    ld a,(ix+5)
    and 31
    out ($bf),a
    ld a,$c0
    out ($bf),a
    ld a,d
    out ($be),a
    end asm
  end sub




sub smspalettergb(tad as ubyte, tvl as uinteger): '- format: $BGR 12bit
  asm
    ld e,(ix+7)
    ld d,(ix+6)

    ld a,d
    and %00001100
    rl a
    rl a
    ld b,a

    ld a,e
    and %00001100
    rr a
    rr a
    ld c,a

    ld a,d
    and %11000000
    rr a
    rr a
    rr a
    rr a
    ld h,a
    
    ld a,b
    or c
    or h

    ld c,a

    ld a,(ix+5)
    and 31
    ;rl a
    out ($bf),a
    ld a,$c0
    out ($bf),a
    ld a,c
    out ($be),a
    end asm
  end sub

