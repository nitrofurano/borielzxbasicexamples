rm example01.asm
rm ridleofp.zip
rm ridleofp/*
rmdir ridleofp
mkdir ridleofp
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=example01.sm_
zxb.py example01.bas --org=$((0x0069))
zxb.py example01.bas --asm --org=$((0x0069))
cat example01.bin >> example01.sm_
cat dummy64k.bin >> example01.sm_
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.sm_ of=ridleofp/epr10426.bin
rm smsboot.bin example01.sm_ _dummybytes.bin example01.bin

dd bs=$((0x8000)) count=1 if=/dev/zero of=ridleofp/epr10425.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=ridleofp/epr10424.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=ridleofp/epr10423.bin
dd bs=$((0x8000)) count=1 if=/dev/zero of=ridleofp/epr10422.bin

rm smsboot.bin

zip -r ridleofp ridleofp
rm ridleofp/*
rmdir ridleofp

mame -w -video soft -resolution0 512x512 -rp ./ ridleofp

