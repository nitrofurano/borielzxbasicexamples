# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin





dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin





rm example01.asm
rm dai3wksi.zip
rm dai3wksi/*
rmdir dai3wksi
mkdir dai3wksi
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=example01.bin of=dai3wksi/tvg_13.6
dd ibs=1 count=$((0x0800)) skip=$((0x0800)) if=example01.bin of=dai3wksi/tvg_14.7
dd ibs=1 count=$((0x0800)) skip=$((0x1000)) if=example01.bin of=dai3wksi/tvg_15.8
dd ibs=1 count=$((0x0400)) skip=$((0x1800)) if=example01.bin of=dai3wksi/tvg_16.9
dd bs=$((0x2000)) count=1 if=/dev/zero of=dai3wksi/ao4_05.ic3
rm dummy64k.bin example01.bin

#ROM_START( dai3wksi )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "tvg_13.6",  0x0000, 0x0800, CRC(8e8b40b1) SHA1(25b9223486dd348ea302e8e8f1d47c804a88b142) )
#ROM_LOAD( "tvg_14.7",  0x0800, 0x0800, CRC(d48cbabe) SHA1(64b571cd778fc7d67a5fa998a0defd36c04f111f) )
#ROM_LOAD( "tvg_15.8",  0x1000, 0x0800, CRC(cf44bd60) SHA1(61e0b3f9c4a1f9da1de57fb8276d4fc9e43b8f24) )
#ROM_LOAD( "tvg_16.9",  0x1800, 0x0400, CRC(ae723f56) SHA1(c25c27d6144533b2b2a888bfa8dbf48ed8d8b09a) )
#ROM_END







#mcu
#dd bs=$((0x0800)) count=1 if=/dev/zero of=dai3wksi/ao4_06.ic23

#gfx
#zxb.py library/charmap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
#rm charmap01.bin
#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4_07.ic15 #-bitdepth1
#rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=dai3wksi/dai3wksi.5f
#rm spritemap01.bin

#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin

#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > dai3wksi/ao4-12.ic95

#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=dai3wksi/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=dai3wksi/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=dai3wksi/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=dai3wksi/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=dai3wksi/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=dai3wksi/82s126.4a
#rm clut01.bin

rm smsboot.bin

zip -r dai3wksi dai3wksi
rm dai3wksi/*
rmdir dai3wksi

mame -w -video soft -resolution0 512x512 -rp ./ dai3wksi
#xmame.SDL -rp ./ dai3wksi





#ROM_START( dai3wksi )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "tvg_13.6",  0x0000, 0x0800, CRC(8e8b40b1) SHA1(25b9223486dd348ea302e8e8f1d47c804a88b142) )
#ROM_LOAD( "tvg_14.7",  0x0800, 0x0800, CRC(d48cbabe) SHA1(64b571cd778fc7d67a5fa998a0defd36c04f111f) )
#ROM_LOAD( "tvg_15.8",  0x1000, 0x0800, CRC(cf44bd60) SHA1(61e0b3f9c4a1f9da1de57fb8276d4fc9e43b8f24) )
#ROM_LOAD( "tvg_16.9",  0x1800, 0x0400, CRC(ae723f56) SHA1(c25c27d6144533b2b2a888bfa8dbf48ed8d8b09a) )
#ROM_END








