# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm bankp.zip
rm bankp/*
rmdir bankp
mkdir bankp

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=bankp/epr-6175.7e
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=bankp/epr-6174.7f
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=bankp/epr-6173.7h
dd ibs=1 count=$((0x2000)) skip=$((0xC000)) if=example01.bin of=bankp/epr-6176.7d
rm example01.bin

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6165.5l
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6166.5k
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6172.5b
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6171.5d
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6170.5e
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6169.5f
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6168.5h
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=bankp/epr-6167.5i
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=bankp/bankp.5f
#rm spritemap01.bin

#palette
dd bs=$((0x0020)) count=1 if=/dev/urandom of=bankp/pr-6177.8a
dd bs=$((0x0100)) count=1 if=/dev/urandom of=bankp/pr-6178.6f
dd bs=$((0x0100)) count=1 if=/dev/urandom of=bankp/pr-6179.5a
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=bankp/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=bankp/82s126.4a
#rm clut01.bin

#prom
dd bs=$((0x025B)) count=1 if=/dev/urandom of=bankp/315-5074.2c.bin
dd bs=$((0x0001)) count=1 if=/dev/urandom of=bankp/315-5073.pal16l4

rm smsboot.bin dummy64k.bin

zip -r bankp bankp
rm bankp/*
rmdir bankp

mame -w -video soft -resolution0 512x512 -rp ./ bankp

#  ROM_REGION( 0x0220, "proms", 0 )
#  ROM_LOAD( "pr-6177.8a",   0x0000, 0x020, CRC(eb70c5ae) SHA1(13613dad6c14004278f777d6f3f62712a2a85773) )     /* palette */
#  ROM_LOAD( "pr-6178.6f",   0x0020, 0x100, CRC(0acca001) SHA1(54c354d825a24a9085867b114a2cd6835baebe55) )     /* charset #1 lookup table */
#  ROM_LOAD( "pr-6179.5a",   0x0120, 0x100, CRC(e53bafdb) SHA1(7a414f6db5476dd7d0217e5b846ed931381eda02) )     /* charset #2 lookup table */

#  ROM_REGION( 0x025c, "user1", 0 )
#  ROM_LOAD( "315-5074.2c.bin",   0x0000, 0x025b, CRC(2e57bbba) SHA1(c3e45e8a972342779442e50872a2f5f2d61e9c0a) )
#  ROM_LOAD( "315-5073.pal16l4",  0x0000, 0x0001, NO_DUMP ) /* read protected */

#  cpu
#  ROM_LOAD( "epr-6175.7e",  0x0000, 0x4000, CRC(044552b8) SHA1(8d50ba062483d4789cfd3ed86cea53dff0ff6968) )
#  ROM_LOAD( "epr-6174.7f",  0x4000, 0x4000, CRC(d29b1598) SHA1(8c1ee4d23d8d6f93af3e22f2cba189b0055994fb) )
#  ROM_LOAD( "epr-6173.7h",  0x8000, 0x4000, CRC(b8405d38) SHA1(0f62a972f38b4ddcea77eb0e1d76c70ddbcb7b11) )
#  ROM_LOAD( "epr-6176.7d",  0xc000, 0x2000, CRC(c98ac200) SHA1(1bdb87868deebe03da18280e617530c24118da1c) )

#  gfx1
#  ROM_LOAD( "epr-6165.5l",  0x0000, 0x2000, CRC(aef34a93) SHA1(513895cd3144977b3d9b5ac7f2bf40384d69e157) )    /* playfield #1 chars */
#  ROM_LOAD( "epr-6166.5k",  0x2000, 0x2000, CRC(ca13cb11) SHA1(3aca0b0d3f052a742e1cd0b96bfad834e78fcd7d) )

#  gfx2
#  ROM_LOAD( "epr-6172.5b",  0x0000, 0x2000, CRC(c4c4878b) SHA1(423143d81408eda96f87bdc3a306517c473cbe00) )    /* playfield #2 chars */
#  ROM_LOAD( "epr-6171.5d",  0x2000, 0x2000, CRC(a18165a1) SHA1(9a7513ea84f9231edba4e637df28a1705c8cdeb0) )
#  ROM_LOAD( "epr-6170.5e",  0x4000, 0x2000, CRC(b58aa8fa) SHA1(432b43cd9af4e3dab579cfd191b731aa11ceb121) )
#  ROM_LOAD( "epr-6169.5f",  0x6000, 0x2000, CRC(1aa37fce) SHA1(6e2402683145de8972a53c9ec01da9a422392bed) )
#  ROM_LOAD( "epr-6168.5h",  0x8000, 0x2000, CRC(05f3a867) SHA1(9da11c3cea967c5f0d7397c0ff4f87b4b1446c4c) )
#  ROM_LOAD( "epr-6167.5i",  0xa000, 0x2000, CRC(3fa337e1) SHA1(5fdc45436be27cceb5157bd6201c30e3de28fd7b) )





