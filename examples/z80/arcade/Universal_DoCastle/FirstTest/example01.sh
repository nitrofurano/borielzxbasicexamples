rm example01.asm
rm docastle.zip
rm docastle/*
rmdir docastle
mkdir docastle


#first cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=docastle/01p_a1.bin
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=docastle/01n_a2.bin
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=docastle/01l_a3.bin
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=docastle/01k_a4.bin
rm example01.bin






#second cpu
#dd bs=$((0x4000)) count=1 if=/dev/zero of=docastle/07n_a0.bin
zxb.py library/dorunrunsecondcpu.bas --org=2048
dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=dorunrunsecondcpu.bin of=dorunrunsecondcpu2.bin
cat dummy64k.bin >> dorunrunsecondcpu2.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=dorunrunsecondcpu2.bin of=docastle/07n_a0.bin
rm dorunrunsecondcpu.bin dorunrunsecondcpu2.bin









#third cpu
dd bs=$((0x2000)) count=1 if=/dev/zero of=docastle/01d.bin




#gfx
zxb.py library/charmapdocastle01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmapdocastle01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmapdocastle01.bin of=charmapdocastle01b.bin
rm charmapdocastle01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmapdocastle01b.bin of=docastle/03a_a5.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdocastle01b.bin of=docastle/04m_a6.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdocastle01b.bin of=docastle/04l_a7.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdocastle01b.bin of=docastle/04j_a8.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmapdocastle01b.bin of=docastle/04h_a9.bin
rm charmapdocastle01b.bin



#prom
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=palette01.bin of=docastle/09c.bin
rm palette01.bin

#dd bs=256 count=1 if=/dev/urandom of=docastle/docastle.clr




#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=docastle/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=docastle/docastle.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=docastle/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=docastle/82s126.3m

rm smsboot.bin dummy64k.bin

#rm docastle.zip
#rm docastle/*
#rmdir docastle

zip -r docastle docastle
rm docastle/*
rmdir docastle


mame -w -video soft -resolution0 512x512 -rp ./ docastle

