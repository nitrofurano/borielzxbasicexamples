# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm lkage.zip
rm lkage/*
rmdir lkage
mkdir lkage

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=lkage/a54-01-2.37
dd ibs=1 count=$((0x8000)) skip=$((0x8000)) if=example01.bin of=lkage/a54-02-2.38
rm example01.bin

#audio
dd bs=$((0x8000)) count=1 if=/dev/zero of=lkage/a54-04.54
#mcu
dd bs=$((0x8000)) count=1 if=/dev/urandom of=lkage/a54-09.53
#user
dd bs=$((0x8000)) count=1 if=/dev/urandom of=lkage/a54-03.51

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmap01b.bin of=lkage/a54-05-1.84 #skip 0x0000
#dd ibs=1 count=$((0x4000)) skip=$((0x0001)) if=charmap01b.bin of=lkage/a54-06-1.85 #skip 0x4000
#dd ibs=1 count=$((0x4000)) skip=$((0x0002)) if=charmap01b.bin of=lkage/a54-07-1.86 #skip 0x8000
#dd ibs=1 count=$((0x4000)) skip=$((0x0003)) if=charmap01b.bin of=lkage/a54-08-1.87 #skip 0xC000

xxd -p -c 4 -s 0 -l $((0x10000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > lkage/a54-05-1.84
xxd -p -c 4 -s 1 -l $((0x10000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > lkage/a54-06-1.85
xxd -p -c 4 -s 2 -l $((0x10000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > lkage/a54-07-1.86
xxd -p -c 4 -s 3 -l $((0x10000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > lkage/a54-08-1.87

rm charmap01b.bin

#?????
dd bs=$((0x200)) count=1 if=/dev/urandom of=lkage/a54-10.2
dd bs=$((0x104)) count=1 if=/dev/urandom of=lkage/pal16l8-a54-11.34
dd bs=$((0x104)) count=1 if=/dev/urandom of=lkage/pal16l8-a54-12.76
dd bs=$((0x104)) count=1 if=/dev/urandom of=lkage/pal16l8a-a54-13.27
dd bs=$((0x104)) count=1 if=/dev/urandom of=lkage/pal16l8a-a54-14.35

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=lkage/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=lkage/82s126.4a
#rm clut01.bin
#zxb.py library/charmap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=lkage/lkage.5e
#rm charmap01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=lkage/lkage.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=lkage/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=lkage/82s126.3m

rm smsboot.bin dummy64k.bin

zip -r lkage lkage
rm lkage/*
rmdir lkage

mame -w -video soft -sound none -resolution0 512x512 -rp ./ lkage






