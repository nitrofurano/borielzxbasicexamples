rm rcasino.zip
rm example01.asm
rm rcasino/*
rmdir rcasino
mkdir rcasino



dd bs=$((0x4000)) count=1 if=/dev/zero of=dummy32k.bin



#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=rcasino/ri-w1.18b
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=rcasino/ri-w2.16b
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=rcasino/ri-w3.15b

#dd bs=$((0x2000)) count=1 if=/dev/zero of=rcasino/a-s5_7.bin
#dd bs=$((0x2000)) count=1 if=/dev/zero of=rcasino/a-s6-8.bin
#dd bs=$((0x4000)) count=1 if=/dev/zero of=rcasino/a-s8-10.bin

#ROM_START( rcasino )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "ri-w1.18b", 0x0000, 0x2000, CRC(ed105d69) SHA1(951697e1050f72967f0710155aa8ff72db73fce1) )
#ROM_LOAD( "ri-w2.16b", 0x2000, 0x2000, CRC(a1a80b33) SHA1(2f969713cae288de1985d7baa70cad50c4148970) )
#ROM_LOAD( "ri-w3.15b", 0x4000, 0x1000, CRC(acf77a36) SHA1(599470e461a261130e942d174051648459f37a37) )








#sub
dd bs=$((0x2000)) count=1 if=/dev/zero of=rcasino/a-b3-1.bin


#- ROM_REGION( 0x10000, "sub", 0 )
#- ROM_LOAD( "a-b3-1.bin",     0x0000, 0x2000, CRC(b0f56102) SHA1(4f427c3bd6131b7cba42a0e24a69bd1b6a1b0a3c) )








#gfx

zxb.py library/b2r1f1_charmapmrdo01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f1_charmapmrdo01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b2r1f1_charmapmrdo01.bin of=b2r1f1_charmapmrdo01b.bin

xxd -p -c 2 -s 0 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w5.9b
xxd -p -c 2 -s 1 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w6.8b
xxd -p -c 2 -s 0 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w4.11b
xxd -p -c 2 -s 1 -l $((0x4000)) b2r1f1_charmapmrdo01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w7.6b

rm b2r1f1_charmapmrdo01.bin b2r1f1_charmapmrdo01b.bin

#ROM_REGION( 0x6000, "gfx1", 0 )
#bit 0 upper:ROM_LOAD( "ri-w5.9b",  0x3000, 0x2000, CRC(81d20577) SHA1(50a1e0231400c106539ffa78deb3e0e6c8afc3f5) )
#bit 1 upper: ROM_LOAD( "ri-w6.8b",  0x0000, 0x2000, CRC(b2dd4e1e) SHA1(323dcfb26653c17951db65ce2ced3325d35489e4) )
#bit 0 lower:ROM_LOAD( "ri-w4.11b", 0x5000, 0x1000, CRC(7ca0e78c) SHA1(163cfd1f76ecbd14219146963d1abc4c09c0ac8c) )
#bit 1 lower: ROM_LOAD( "ri-w7.6b",  0x2000, 0x1000, CRC(8e0d3b9c) SHA1(c5211d834b0db488839a5c53d00435a0b59cd4ca) )


#xxd -p -c 2 -s 0 -l $((0x4000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w5.9b
#xxd -p -c 2 -s 1 -l $((0x4000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > rcasino/ri-w6.8b










# proms

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy32k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=rcasino/prom1.9e
dd ibs=1 count=$((0x0020)) skip=$((0x0030)) if=palette01.bin of=rcasino/prom2.8e
rm palette01.bin
##dd bs=$((0x0020)) count=1 if=/dev/urandom of=rcasino/rcasino.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=rcasino/rcasino.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=rcasino/rcasino.11j
#rm clut01b.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=rcasino/prom1.9e
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=rcasino/prom2.8e

#dd bs=256 count=1 if=/dev/urandom of=rcasino/tb-prom.3p8
#dd bs=256 count=1 if=/dev/urandom of=rcasino/tb-prom.4k8

#ROM_REGION( 0x40, "proms", 0 )
#ROM_LOAD( "prom1.9e",  0x0000, 0x0020, CRC(93312432) SHA1(3c7abc165e6bc7e0c56ca97d89b0b5e06323b82e) )
#ROM_LOAD( "prom2.8e",  0x0020, 0x0020, CRC(2b5c7826) SHA1(c0de392aebd6982e5846c12aeb2e871358be60d7) )
#ROM_END











rm smsboot.bin dummy32k.bin example01.bin

zip -r rcasino rcasino
rm rcasino/*
rmdir rcasino

mame -w -video soft -resolution0 512x512 -rp ./ rcasino









#ROM_START( rcasino )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "ri-w1.18b", 0x0000, 0x2000, CRC(ed105d69) SHA1(951697e1050f72967f0710155aa8ff72db73fce1) )
#ROM_LOAD( "ri-w2.16b", 0x2000, 0x2000, CRC(a1a80b33) SHA1(2f969713cae288de1985d7baa70cad50c4148970) )
#ROM_LOAD( "ri-w3.15b", 0x4000, 0x1000, CRC(acf77a36) SHA1(599470e461a261130e942d174051648459f37a37) )

#ROM_REGION( 0x6000, "gfx1", 0 )
#ROM_LOAD( "ri-w6.8b",  0x0000, 0x2000, CRC(b2dd4e1e) SHA1(323dcfb26653c17951db65ce2ced3325d35489e4) )
#ROM_LOAD( "ri-w7.6b",  0x2000, 0x1000, CRC(8e0d3b9c) SHA1(c5211d834b0db488839a5c53d00435a0b59cd4ca) )
#ROM_LOAD( "ri-w5.9b",  0x3000, 0x2000, CRC(81d20577) SHA1(50a1e0231400c106539ffa78deb3e0e6c8afc3f5) )
#ROM_LOAD( "ri-w4.11b", 0x5000, 0x1000, CRC(7ca0e78c) SHA1(163cfd1f76ecbd14219146963d1abc4c09c0ac8c) )

#ROM_REGION( 0x40, "proms", 0 )
#ROM_LOAD( "prom1.9e",  0x0000, 0x0020, CRC(93312432) SHA1(3c7abc165e6bc7e0c56ca97d89b0b5e06323b82e) )
#ROM_LOAD( "prom2.8e",  0x0020, 0x0020, CRC(2b5c7826) SHA1(c0de392aebd6982e5846c12aeb2e871358be60d7) )
#ROM_END







