rm funkybee.zip
rm example01.asm
rm funkybee/*
rmdir funkybee
mkdir funkybee



dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin



#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=funkybee/funkybee.1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=funkybee/funkybee.2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=funkybee/funkybee.3
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=funkybee/funkybee.4

#ROM_START( funkybee )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "funkybee.1",    0x0000, 0x1000, CRC(3372cb33) SHA1(09f2673cdeaadba8211d86a19e727aebb4d8be9d) )
#ROM_LOAD( "funkybee.3",    0x1000, 0x1000, CRC(7bf7c62f) SHA1(f8e5514c17fddb8ed95e5e18aab81ad0ebcc41af) )
#ROM_LOAD( "funkybee.2",    0x2000, 0x1000, CRC(8cc0fe8e) SHA1(416d97db0a2219ea46f2caa55787253e16a5ef32) )
#ROM_LOAD( "funkybee.4",    0x3000, 0x1000, CRC(1e1aac26) SHA1(a2974e6a8da5568f91aa44adb58941b0a60b1536) )







zxb.py library/b2r1f0_charmapfunkybee01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f0_charmapfunkybee01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f0_charmapfunkybee01.bin of=funkybee/funkybee.5
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r1f0_charmapfunkybee01.bin of=funkybee/funkybee.6
rm b2r1f0_charmapfunkybee01.bin

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "funkybee.5",    0x0000, 0x2000, CRC(86126655) SHA1(d91682121d7f6a70f10a946ab81b248cc29bdf8c) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "funkybee.6",    0x0000, 0x2000, CRC(5fffd323) SHA1(9de9c869bd1e2daab3b94275444ecbe904bcd6aa) )








#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=funkybee/funkybee.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=funkybee/funkybee.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=funkybee/funkybee.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=funkybee/funkybee.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=funkybee/82s126.1m

dd bs=32 count=1 if=/dev/urandom of=funkybee/funkybee.clr

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "funkybee.clr",  0x0000, 0x0020, CRC(e2cf5fe2) SHA1(50b293f48f078cbcebccb045aa779ced2fb298c8) )
#ROM_END






rm smsboot.bin dummy32k.bin example01.bin

zip -r funkybee funkybee
rm funkybee/*
rmdir funkybee

mame -w -video soft -resolution0 512x512 -rp ./ funkybee




#ROM_START( funkybee )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "funkybee.1",    0x0000, 0x1000, CRC(3372cb33) SHA1(09f2673cdeaadba8211d86a19e727aebb4d8be9d) )
#ROM_LOAD( "funkybee.3",    0x1000, 0x1000, CRC(7bf7c62f) SHA1(f8e5514c17fddb8ed95e5e18aab81ad0ebcc41af) )
#ROM_LOAD( "funkybee.2",    0x2000, 0x1000, CRC(8cc0fe8e) SHA1(416d97db0a2219ea46f2caa55787253e16a5ef32) )
#ROM_LOAD( "funkybee.4",    0x3000, 0x1000, CRC(1e1aac26) SHA1(a2974e6a8da5568f91aa44adb58941b0a60b1536) )

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "funkybee.5",    0x0000, 0x2000, CRC(86126655) SHA1(d91682121d7f6a70f10a946ab81b248cc29bdf8c) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "funkybee.6",    0x0000, 0x2000, CRC(5fffd323) SHA1(9de9c869bd1e2daab3b94275444ecbe904bcd6aa) )

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "funkybee.clr",  0x0000, 0x0020, CRC(e2cf5fe2) SHA1(50b293f48f078cbcebccb045aa779ced2fb298c8) )
#ROM_END





