rm enigma2/*
rmdir enigma2
rm enigma2_/*
rmdir enigma2_
rm example01.asm
rm enigma2/*
rmdir enigma2
mkdir enigma2

dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin




#cpu

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin

dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=enigma2/1.5d
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=enigma2/2.7d
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=enigma2/3.8d
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=enigma2/4.10d
# dont forget org $4000 in the code
dd ibs=1 count=$((0x800)) skip=$((0x4000)) if=example01.bin of=enigma2/5.11d
dd ibs=1 count=$((0x800)) skip=$((0x4800)) if=example01.bin of=enigma2/6.13d
rm dummy64k.bin example01.bin smsboot.bin
#- 12kb rom - i have to rearrange it for avoiding waste






#- audio
dd bs=$((0x1000)) count=1 if=/dev/zero of=enigma2/enigma2.s

#- proms
dd if=/dev/zero ibs=1k count=100 | tr "\000" "\377" > enigma2/7.11f   #- fill rom display with white/black attributes
dd bs=$((0x800)) count=1 if=/dev/zero of=enigma2/8.13f                #- all stars are dark







zip -r enigma2 enigma2
rm enigma2/* && rmdir enigma2
#mv enigma2 enigma2_ #-for debugging

mame -w -video soft -sound none -resolution0 512x512 -rp ./ enigma2



#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "1.5d",         0x0000, 0x0800, CRC(499749de) SHA1(401928ff41d3b4cbb68e6ad3bf3be4a10ae1781f) )
#- ROM_LOAD( "2.7d",         0x0800, 0x0800, CRC(173c1329) SHA1(3f1ad46d0e58ab236e4ff2b385d09fbf113627da) )
#- ROM_LOAD( "3.8d",         0x1000, 0x0800, CRC(c7d3e6b1) SHA1(43f7c3a02b46747998260d5469248f21714fe12b) )
#- ROM_LOAD( "4.10d",        0x1800, 0x0800, CRC(c6a7428c) SHA1(3503f09856655c5973fb89f60d1045fe41012aa9) )
#- ROM_LOAD( "5.11d",        0x4000, 0x0800, CRC(098ac15b) SHA1(cce28a2540a9eabb473391fff92895129ae41751) )
#- ROM_LOAD( "6.13d",        0x4800, 0x0800, CRC(240a9d4b) SHA1(ca1c69fafec0471141ce1254ddfaef54fecfcbf0) )

#- ROM_REGION( 0x10000, "audiocpu", 0 )
#- ROM_LOAD( "enigma2.s",    0x0000, 0x1000, CRC(68fd8c54) SHA1(69996d5dfd996f0aacb26e397bef314204a2a88a) )

#- ROM_REGION( 0x1000, "proms", 0 )    /* color map/star map */
#- ROM_LOAD( "7.11f",        0x0000, 0x0800, CRC(409b5aad) SHA1(1b774a70f725637458ed68df9ed42476291b0e43) )
#- ROM_LOAD( "8.13f",        0x0800, 0x0800, CRC(e9cb116d) SHA1(41da4f46c5614ec3345c233467ebad022c6b0bf5) )
