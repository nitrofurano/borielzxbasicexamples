asm
  ld sp,$23FF
  end asm

#include "./library/pacmandelay.bas"
#include "./library/smsrnd.bas"
#include "./library/b1r1f1_charmapenigma2_01.zxi"

dim eee as uinteger at $2010
dim seed as uinteger at $2012
dim ee0 as uinteger at $2024
dim ee1 as uinteger at $2026
dim ee2 as uinteger at $2014
dim ee3 as uinteger at $2016
dim ee4 as uinteger at $2018
dim ee5 as uinteger at $201A
dim eex as uinteger at $201C
dim eey as uinteger at $201E

sub enigma2putchar(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,txr1 as ubyte,tad1 as uinteger):
  dim tlp1 as uinteger at $2000
  'replace with ldir?
  for tlp1=0 to 7
    poke $2400+(((tlp1*32)+(txp1*256)+(typ1))bxor %00011111 ),peek(tad1+tlp1+(tch1*8)) bxor txr1
    next
  end sub

'-----------------------------------------------------
'$????
'-----------------------------------------------------
'- this will allocate the code to the second part of the rom, after ram area
'- first part: $0000 to $1FFF (8kb) - second part: $4000 to $4FFF (4kb)
'- copied from a code made for Casio PV1000
goto flg01:
asm
  org $4000
  end asm
flg01:
'-----------------------------------------------------
'$4000
'-----------------------------------------------------

for ee2=0 to 31
  for ee3=0 to 27
    seed=smsrnd(seed)
    enigma2putchar(ee3,ee2,96+seed mod 9,$FF,@charmap01-256)
    'pacmandelay(50)
    next:next

for ee2=0 to 15
  for ee3=0 to 15
    enigma2putchar(ee3+11,ee2+15,ee2*16+ee3,$FF,@charmap01-256)
    'pacmandelay(50)
    next:next

eee=0:eex=8:eey=8
do

enigma2putchar(1,1,48+int(eee/10000) mod 10,$FF,@charmap01-256)
enigma2putchar(2,1,48+int(eee/1000) mod 10,$FF,@charmap01-256)
enigma2putchar(3,1,48+int(eee/100) mod 10,$FF,@charmap01-256)
enigma2putchar(4,1,48+int(eee/10) mod 10,$FF,@charmap01-256)
enigma2putchar(5,1,48+eee mod 10,$FF,@charmap01-256)

ee0=peek($5800)
enigma2putchar(3,3,48+int(ee0/100) mod 10,$FF,@charmap01-256)
enigma2putchar(4,3,48+int(ee0/10) mod 10,$FF,@charmap01-256)
enigma2putchar(5,3,48+ee0 mod 10,$FF,@charmap01-256)

ee0=peek($5801)
enigma2putchar(3,4,48+int(ee0/100) mod 10,$FF,@charmap01-256)
enigma2putchar(4,4,48+int(ee0/10) mod 10,$FF,@charmap01-256)
enigma2putchar(5,4,48+ee0 mod 10,$FF,@charmap01-256)

eex=eex-((ee0 band 32)/32)+((ee0 band 64)/64)
eey=eey+((ee0 band 8)/8)-((ee0 band 16)/16)

enigma2putchar(eex,eey,eee band 127,$FF,@charmap01-256)

eee=eee+1
loop

'--------------------------------------------------------------------------------------------------

'- static ADDRESS_MAP_START( engima2_main_cpu_map, AS_PROGRAM, 8, enigma2_state )
'- ADDRESS_MAP_GLOBAL_MASK(0x7fff)
'- AM_RANGE(0x0000, 0x1fff) AM_ROM AM_WRITENOP
'- AM_RANGE(0x2000, 0x3fff) AM_MIRROR(0x4000) AM_RAM AM_SHARE("videoram")
'- AM_RANGE(0x4000, 0x4fff) AM_ROM AM_WRITENOP
'- AM_RANGE(0x5000, 0x57ff) AM_READ(dip_switch_r) AM_WRITENOP
'- AM_RANGE(0x5800, 0x5800) AM_MIRROR(0x07f8) AM_NOP
'- AM_RANGE(0x5801, 0x5801) AM_MIRROR(0x07f8) AM_READ_PORT("IN0") AM_WRITENOP
'- AM_RANGE(0x5802, 0x5802) AM_MIRROR(0x07f8) AM_READ_PORT("IN1") AM_WRITENOP
'- AM_RANGE(0x5803, 0x5803) AM_MIRROR(0x07f8) AM_READNOP AM_WRITE(sound_data_w)
'- AM_RANGE(0x5804, 0x5804) AM_MIRROR(0x07f8) AM_NOP
'- AM_RANGE(0x5805, 0x5805) AM_MIRROR(0x07f8) AM_READNOP AM_WRITE(enigma2_flip_screen_w)
'- AM_RANGE(0x5806, 0x5807) AM_MIRROR(0x07f8) AM_NOP
