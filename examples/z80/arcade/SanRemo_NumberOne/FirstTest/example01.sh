rm number1.zip
rm example01.asm
rm number1/*
rmdir number1
mkdir number1




dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin




#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
cat dummy32k.bin >> example01.bin
rm example01.bin
mv smsboot2.bin example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=number1/no_g0.ic26


#- ROM_START( number1 )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "no_g0.ic26", 0x0000, 0x8000, CRC(2d83646f) SHA1(d1fafcce44ed3ec3dd53d84338c42244ebfca820) )







zxb.py library/b4r0f0_charmapyumefuda01.bas --org=$((0x0000))
cat dummy32k.bin >> b4r0f0_charmapyumefuda01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=b4r0f0_charmapyumefuda01b.bin
xxd -p -c 4 -s 0 -l 65536 b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > number1/no_i4.ic30
xxd -p -c 4 -s 1 -l 65536 b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > number1/no_b4.ic27
xxd -p -c 4 -s 2 -l 65536 b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > number1/no_g4.ic28
xxd -p -c 4 -s 3 -l 65536 b4r0f0_charmapyumefuda01b.bin | cut -b 1-2 | xxd -r -p > number1/no_r4.ic29
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=number1/no_i4.ic30
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=number1/no_b4.ic27
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=number1/no_g4.ic28
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0_charmapyumefuda01.bin of=number1/no_r4.ic29
rm b4r0f0_charmapyumefuda01.bin b4r0f0_charmapyumefuda01b.bin





#- ROM_REGION( 0x40000, "gfx", 0 )
#- ROM_LOAD( "no_i4.ic30", 0x00000, 0x10000, CRC(55b351a4) SHA1(b0c8a30dde076520234281da051f21f1b7cb3166) )    // I
#- ROM_LOAD( "no_b4.ic27", 0x10000, 0x10000, CRC(e48b1c8a) SHA1(88f60268fd43c06e146d936a1bdc078c44e2a213) )    // B
#- ROM_LOAD( "no_g4.ic28", 0x20000, 0x10000, CRC(4eea9a9b) SHA1(c86c083ccf08c3c310028920f9a0fe809fd7ccbe) )    // G
#- ROM_LOAD( "no_r4.ic29", 0x30000, 0x10000, CRC(ab08cdaf) SHA1(e0518403039b6bada79ffe4c6bc22fbb64d16e43) )    // R







#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=number1/number1.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=number1/number1.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=number1/number1.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=number1/number1.11j
#rm clut01b.bin
#????

dd bs=$((0x0800)) count=1 if=/dev/urandom of=number1/number1_nvram.bin
dd bs=$((0x0104)) count=1 if=/dev/urandom of=number1/palce1.bin
dd bs=$((0x0104)) count=1 if=/dev/urandom of=number1/palce2.bin
dd bs=$((0x0104)) count=1 if=/dev/urandom of=number1/palce3.bin



#- ROM_REGION( 0x0800, "nvram", 0 )    /* default NVRAM */
#- ROM_LOAD( "number1_nvram.bin", 0x0000, 0x0800, CRC(4ece7b39) SHA1(49815571d75a39ab67d26691f902dfbd4e05feb4) )

#- ROM_REGION( 0x0600, "plds", 0 )
#- ROM_LOAD( "palce1.bin", 0x0000, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_LOAD( "palce2.bin", 0x0200, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_LOAD( "palce3.bin", 0x0400, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_END#-








rm smsboot.bin dummy32k.bin example01.bin

zip -r number1 number1
rm number1/*
rmdir number1

#mame -w -video soft -resolution0 512x512 -rp ./ number1
mame -video soft -c -cheatpath ~/.mame/ -resolution0 716x448 -aspect 85:64 -skip_gameinfo -nowindow -rp ./ number1


#- ROM_START( number1 )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "no_g0.ic26", 0x0000, 0x8000, CRC(2d83646f) SHA1(d1fafcce44ed3ec3dd53d84338c42244ebfca820) )

#- ROM_REGION( 0x40000, "gfx", 0 )
#- ROM_LOAD( "no_i4.ic30", 0x00000, 0x10000, CRC(55b351a4) SHA1(b0c8a30dde076520234281da051f21f1b7cb3166) )    // I
#- ROM_LOAD( "no_b4.ic27", 0x10000, 0x10000, CRC(e48b1c8a) SHA1(88f60268fd43c06e146d936a1bdc078c44e2a213) )    // B
#- ROM_LOAD( "no_g4.ic28", 0x20000, 0x10000, CRC(4eea9a9b) SHA1(c86c083ccf08c3c310028920f9a0fe809fd7ccbe) )    // G
#- ROM_LOAD( "no_r4.ic29", 0x30000, 0x10000, CRC(ab08cdaf) SHA1(e0518403039b6bada79ffe4c6bc22fbb64d16e43) )    // R

#- ROM_REGION( 0x0800, "nvram", 0 )    /* default NVRAM */
#- ROM_LOAD( "number1_nvram.bin", 0x0000, 0x0800, CRC(4ece7b39) SHA1(49815571d75a39ab67d26691f902dfbd4e05feb4) )

#- ROM_REGION( 0x0600, "plds", 0 )
#- ROM_LOAD( "palce1.bin", 0x0000, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_LOAD( "palce2.bin", 0x0200, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_LOAD( "palce3.bin", 0x0400, 0x0104, NO_DUMP )   /* PALCE is read protected */
#- ROM_END#-
