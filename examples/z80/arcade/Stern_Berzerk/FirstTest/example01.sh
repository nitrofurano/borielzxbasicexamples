rm berzerk1/*
rmdir berzerk1
rm berzerk1_/*
rmdir berzerk1_
rm example01.asm
rm berzerk1/*
rmdir berzerk1
mkdir berzerk1

dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

#- boot
#zxb.py library/berzerkboot.bas --org=1024
#cat dummy64k.bin >> berzerkboot.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=berzerkboot.bin of=berzerk1/rom0.1c
#- program
#zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0000))
#zxb.py example01.zxb --heap-size=128 --org=$((0x0000))
#cat dummy64k.bin >> example01.bin

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin

dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=berzerk1/rom0.1c
# dont forget org $1000 in the code
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=berzerk1/rom1.1d
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=berzerk1/rom2.3d
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=berzerk1/rom3.5d
dd ibs=1 count=$((0x800)) skip=$((0x2800)) if=example01.bin of=berzerk1/rom4.6d
dd ibs=1 count=$((0x800)) skip=$((0x3000)) if=example01.bin of=berzerk1/rom5.5c
rm dummy64k.bin example01.bin berzerkboot.bin smsboot.bin
#- 12kb rom - i have to rearrange it for avoiding waste



#- voice
dd bs=2048 count=1 if=/dev/zero of=berzerk1/berzerk_r_vo_1c.1c
dd bs=2048 count=1 if=/dev/zero of=berzerk1/berzerk_r_vo_2c.2c

zip -r berzerk1 berzerk1
rm berzerk1/* && rmdir berzerk1
#mv berzerk1 berzerk1_ #-for debugging

mame -w -video soft -sound none -resolution0 512x512 -rp ./ berzerk1


