rm beaminv.zip
rm example01.asm
rm beaminv/*
rmdir beaminv
mkdir beaminv





dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin






#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x400)) skip=$((0x0000)) if=example01.bin of=beaminv/0a
dd ibs=1 count=$((0x400)) skip=$((0x0400)) if=example01.bin of=beaminv/1a
dd ibs=1 count=$((0x400)) skip=$((0x0800)) if=example01.bin of=beaminv/2a
dd ibs=1 count=$((0x400)) skip=$((0x0C00)) if=example01.bin of=beaminv/3a
dd ibs=1 count=$((0x400)) skip=$((0x1000)) if=example01.bin of=beaminv/4a
dd ibs=1 count=$((0x400)) skip=$((0x1400)) if=example01.bin of=beaminv/5a



#- ROM_START( beaminv )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "0a", 0x0000, 0x0400, CRC(17503086) SHA1(18c789216e5c4330dba3eeb24919dae636bf803d) )
#- ROM_LOAD( "1a", 0x0400, 0x0400, CRC(aa9e1666) SHA1(050e2bd169f1502f49b7e6f5f2df9dac0d8107aa) )
#- ROM_LOAD( "2a", 0x0800, 0x0400, CRC(ebaa2fc8) SHA1(b4ff1e1bdfe9efdc08873bba2f0a30d24678f9d8) )
#- ROM_LOAD( "3a", 0x0c00, 0x0400, CRC(4f62c2e6) SHA1(4bd7d5e4f18d250003c7d771f1cdab08d699a765) )
#- ROM_LOAD( "4a", 0x1000, 0x0400, CRC(3eebf757) SHA1(990eebda80ec52b7e3a36912c6e9230cd97f9f25) )
#- ROM_LOAD( "5a", 0x1400, 0x0400, CRC(ec08bc1f) SHA1(e1df6704298e470a77158740c275fdca105e8f69) )
#- ROM_END




rm smsboot.bin dummy32k.bin example01.bin

zip -r beaminv beaminv
rm beaminv/*
rmdir beaminv

mame -w -video soft -resolution0 512x512 -rp ./ beaminv




