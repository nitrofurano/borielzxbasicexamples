rm example01.asm
rm stratvox.zip
rm stratvox/*
rmdir stratvox
mkdir stratvox

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu1 - 12kb?
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=stratvox/ls01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=stratvox/ls02.bin
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=stratvox/ls03.bin
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=stratvox/ls04.bin
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=stratvox/ls05.bin
dd ibs=1 count=$((0x800)) skip=$((0x2800)) if=example01.bin of=stratvox/ls06.bin
rm example01.bin
#cpu2
dd bs=$((0x800)) count=1 if=/dev/zero of=stratvox/ls07.bin
dd bs=$((0x800)) count=1 if=/dev/zero of=stratvox/ls08.bin

#proms
zxb.py library/prom01.zxi --org=0
dd ibs=1 count=$((0x800)) skip=$((0x0010)) if=prom01.bin of=stratvox/im5623.f10
zxb.py library/prom02.zxi --org=0
dd ibs=1 count=$((0x800)) skip=$((0x0010)) if=prom02.bin of=stratvox/im5623.f12
rm prom01.bin prom02.bin
#dd bs=$((0x800)) count=1 if=/dev/urandom of=stratvox/im5623.f10
#dd bs=$((0x800)) count=1 if=/dev/urandom of=stratvox/im5623.f12

zip -r stratvox stratvox
rm stratvox/*
rmdir stratvox

rm smsboot.bin dummy64k.bin
mame -w -video soft -sound none -resolution0 512x512 -rp ./ stratvox

