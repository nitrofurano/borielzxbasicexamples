# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm laserbas.zip example01.asm
rm laserbas/*
rmdir laserbas
mkdir laserbas

dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=laserbas/mb8532.1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=laserbas/mb8532.2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=laserbas/mb8532.3
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=laserbas/mb8532.4
dd ibs=1 count=$((0x1000)) skip=$((0xC000)) if=example01.bin of=laserbas/lb2532.5
dd ibs=1 count=$((0x1000)) skip=$((0xD000)) if=example01.bin of=laserbas/lb2532.6
dd ibs=1 count=$((0x1000)) skip=$((0xE000)) if=example01.bin of=laserbas/mb8532.7
dd ibs=1 count=$((0x0800)) skip=$((0xF000)) if=example01.bin of=laserbas/mb8516.8


#- ROM_LOAD( "mb8532.1",   0x0000, 0x1000, CRC(ef85e0c5) SHA1(c26da919c206a23eb6e53ffe39acd5a5dfd47c18) )
#- ROM_LOAD( "mb8532.2",   0x1000, 0x1000, CRC(0ba2236c) SHA1(416e4be957c395b05537d2e513e0c4561d8ca7c5) )
#- ROM_LOAD( "mb8532.3",   0x2000, 0x1000, CRC(83998e0b) SHA1(ac435fb8dd67aec0737d6c750c527841b2b91a5b) )
#- ROM_LOAD( "mb8532.4",   0x3000, 0x1000, CRC(00f9d909) SHA1(90b800cc5fcea53454584f8ad93eebbd193bdd21) )
#- ROM_LOAD( "lb2532.5",   0xc000, 0x1000, CRC(6459073e) SHA1(78b8a23534826dd2d3b3c6c5d5708c8a78a4b6bf) )
#- ROM_LOAD( "lb2532.6",   0xd000, 0x1000, CRC(a2dc1e7e) SHA1(78643a3aa852c73dab12e09a6cfc53141c936d12) )
#- ROM_LOAD( "mb8532.7",   0xe000, 0x1000, CRC(9d2148d7) SHA1(24954d82a09d9fcfdc61e91b7c824daa5dd701c3) )
#- ROM_LOAD( "mb8516.8",   0xf000, 0x0800, CRC(623f558f) SHA1(be6c6565df658555f21c43a8c2459cf399794a84) )







rm dummy64k.bin example01.bin smsboot.bin

zip -r laserbas laserbas
rm laserbas/*
rmdir laserbas

mame -w -video soft -resolution0 512x512 -rp ./ laserbas
#xmame.SDL -rp ./ laserbas



