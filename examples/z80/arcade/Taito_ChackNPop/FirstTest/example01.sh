# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm chaknpop.zip
rm chaknpop/*
rmdir chaknpop
mkdir chaknpop

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=chaknpop/ao4_01.ic28
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=chaknpop/ao4_02.ic27
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=chaknpop/ao4_03.ic26
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=chaknpop/ao4_04.ic25
dd bs=$((0x2000)) count=1 if=/dev/zero of=chaknpop/ao4_05.ic3
rm dummy64k.bin example01.bin

#mcu
dd bs=$((0x0800)) count=1 if=/dev/zero of=chaknpop/ao4_06.ic23

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx2 - text
xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4_09.ic98 #-bitdepth0
xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4_08.ic14 #-bitdepth0
xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4_07.ic15 #-bitdepth1
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=chaknpop/chaknpop.5f
#rm spritemap01.bin

#palette
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin

xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4-11.ic96
xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > chaknpop/ao4-12.ic95

#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=chaknpop/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=chaknpop/ao4-12.ic95
rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=chaknpop/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=chaknpop/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=chaknpop/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=chaknpop/82s126.4a
#rm clut01.bin

rm smsboot.bin

zip -r chaknpop chaknpop
rm chaknpop/*
rmdir chaknpop

mame -w -video soft -resolution0 512x512 -rp ./ chaknpop
#xmame.SDL -rp ./ chaknpop

#	ROM_REGION( 0x4000, "gfx2", 0 ) /* Text */
#	ROM_LOAD( "ao4_09.ic98", 0x0000, 0x2000, CRC(757a723a) SHA1(62ab84d2aaa9bc1ea5aa9df8155aa3b5a1e93889) )
#	ROM_LOAD( "ao4_10.ic97", 0x2000, 0x2000, CRC(3e3fd608) SHA1(053a8fbdb35bf1c142349f78a63e8cd1adb41ef6) )


#	ROM_REGION( 0x4000, "gfx1", 0 ) /* Sprite */
#	ROM_LOAD( "ao4_08.ic14", 0x0000, 0x2000, CRC(5575a021) SHA1(c2fad53fe6a12c19cec69d27c13fce6aea2502f2) )
#	ROM_LOAD( "ao4_07.ic15", 0x2000, 0x2000, CRC(ae687c18) SHA1(65b25263da88d30cbc0dad94511869596e5c975a) )
#
#
#-------------------------------------------------------------------------------
#	ROM_REGION( 0x18000, "maincpu", 0 )         /* Main CPU */
#	ROM_LOAD( "ao4_01.ic28", 0x00000, 0x2000, CRC(386fe1c8) SHA1(cca24abfb8a7f439251e7936036475c694002561) )
#	ROM_LOAD( "ao4_02.ic27", 0x02000, 0x2000, CRC(5562a6a7) SHA1(0c5d81f9aaf858f88007a6bca7f83dc3ef59c5b5) )
#	ROM_LOAD( "ao4_03.ic26", 0x04000, 0x2000, CRC(3e2f0a9c) SHA1(f1cf87a4cb07f77104d4a4d369807dac522e052c) )
#	ROM_LOAD( "ao4_04.ic25", 0x06000, 0x2000, CRC(5209c7d4) SHA1(dcba785a697df55d84d65735de38365869a1da9d) )
#	ROM_LOAD( "ao4_05.ic3",  0x0a000, 0x2000, CRC(8720e024) SHA1(99e445c117d1501a245f9eb8d014abc4712b4963) )

#	ROM_REGION( 0x0800, "mcu", 0 )  /* 2k for the Motorola MC68705P5 Micro-controller */
#	ROM_LOAD( "ao4_06.ic23", 0x0000, 0x0800, NO_DUMP )

#	ROM_REGION( 0x0800, "proms", 0 )            /* Palette */
#	ROM_LOAD( "ao4-11.ic96", 0x0000, 0x0400, CRC(9bf0e85f) SHA1(44f0a4712c99a715dec54060afb0b27dc48998b4) )
#	ROM_LOAD( "ao4-12.ic95", 0x0400, 0x0400, CRC(954ce8fc) SHA1(e187f9e2cb754264d149c2896ca949dea3bcf2eb) )



