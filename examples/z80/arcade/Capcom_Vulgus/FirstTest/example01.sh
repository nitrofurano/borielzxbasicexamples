# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm vulgus.zip
rm vulgus/*
rmdir vulgus
mkdir vulgus

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=vulgus/vulgus.002
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=vulgus/vulgus.003
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=vulgus/vulgus.004
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=vulgus/vulgus.005
dd ibs=1 count=$((0x2000)) skip=$((0x8000)) if=example01.bin of=vulgus/1-8n.bin
rm example01.bin

#sound?
dd bs=$((0x2000)) count=1 if=/dev/zero of=vulgus/1-11c.bin

#gfx
zxb.py library/b2r3f0sx_charmap1943_01.zxi --org=0x8000
cat dummy64k.bin >> b2r3f0sx_charmap1943_01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01.bin of=b2r3f0sx_charmap1943_01b.bin
rm b2r3f0sx_charmap1943_01.bin
#characters
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/1-3d.bin
#tiles
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-2a.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-3a.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-4a.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-5a.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-6a.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-7a.bin
#sprites
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-2n.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-3n.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-4n.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b2r3f0sx_charmap1943_01b.bin of=vulgus/2-5n.bin
rm b2r3f0sx_charmap1943_01b.bin

#proms
#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 3 -s 0 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > vulgus/e8.bin #r
xxd -p -c 3 -s 1 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > vulgus/e9.bin #g
xxd -p -c 3 -s 2 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > vulgus/e10.bin #b
rm palette01b.bin

#lookup
zxb.py library/lookup01.zxi --org=$((0x0000))
cat dummy64k.bin >> lookup01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=lookup01.bin of=lookup01b.bin
rm lookup01.bin
dd ibs=1 count=$((0x100)) skip=$((0x0000)) if=lookup01b.bin of=vulgus/d1.bin
dd ibs=1 count=$((0x100)) skip=$((0x0000)) if=lookup01b.bin of=vulgus/j2.bin
dd ibs=1 count=$((0x100)) skip=$((0x0000)) if=lookup01b.bin of=vulgus/c9.bin
rm lookup01b.bin

#timings?
dd bs=$((0x0100)) count=1 if=/dev/urandom of=vulgus/82s126.9k
dd bs=$((0x0100)) count=1 if=/dev/urandom of=vulgus/82s129.8n



zip -r vulgus vulgus
rm vulgus/*
rmdir vulgus

rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ vulgus



