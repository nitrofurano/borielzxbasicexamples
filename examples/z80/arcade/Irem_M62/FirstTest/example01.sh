# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm kungfum.zip
rm example01.asm
rm kungfum/*
rmdir kungfum
mkdir kungfum

dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin




#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=kungfum/a-4e-c.bin
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=kungfum/a-4d-c.bin
rm example01.bin

#- ROM_START( kungfum )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "a-4e-c.bin",   0x0000, 0x4000, CRC(b6e2d083) SHA1(17e2cfe2b9d6121239803aba7132918e54ae02bf) )
#- ROM_LOAD( "a-4d-c.bin",   0x4000, 0x4000, CRC(7532918e) SHA1(9d513d5a3b99cc54c4491371cd44af048ef0fb33) )






#sound
dd bs=$((0x4000)) count=1 if=/dev/zero of=kungfum/a-3e-.bin
dd bs=$((0x4000)) count=1 if=/dev/zero of=kungfum/a-3f-.bin
dd bs=$((0x4000)) count=1 if=/dev/zero of=kungfum/a-3h-.bin

#- ROM_REGION( 0x10000, "iremsound", 0 )   /* 64k for the audio CPU (6803) */
#- ROM_LOAD( "a-3e-.bin",    0xa000, 0x2000, CRC(58e87ab0) SHA1(3b03c101fec58eac13fc309a78df9a2cd44f7604) )    /* samples (ADPCM 4-bit) */
#- ROM_LOAD( "a-3f-.bin",    0xc000, 0x2000, CRC(c81e31ea) SHA1(f0fc58b929188c8802cd85549bdf9f4566e6a677) )    /* samples (ADPCM 4-bit) */
#- ROM_LOAD( "a-3h-.bin",    0xe000, 0x2000, CRC(d99fb995) SHA1(caa6acdbc3b02d248fd123be95ea6fdcb4f35b59) )






#gfx1

zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy32k.bin >> charmap01.bin
dd ibs=1 count=$((0x6000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
xxd -p -c 3 -s 0 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-4c-a.bin
xxd -p -c 3 -s 1 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-4d-a.bin
xxd -p -c 3 -s 2 -l $((0x6000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-4e-a.bin
#cat charmap01b.bin charmap01b.bin > kungfum/lr-e-2d
#cat charmap01b.bin charmap01b.bin > kungfum/lr-e-2j
#cat charmap01b.bin charmap01b.bin > kungfum/lr-e-2f
rm charmap01.bin charmap01b.bin

#- ROM_REGION( 0x06000, "gfx1", 0 )
#- ROM_LOAD( "g-4c-a.bin",   0x00000, 0x2000, CRC(6b2cc9c8) SHA1(ba7c902d08c21a1e33f450406bfbfa35abde3b3f) )   /* characters */
#- ROM_LOAD( "g-4d-a.bin",   0x02000, 0x2000, CRC(c648f558) SHA1(7cc085d8dc4a770d2828e39859b7b18e80148a00) )
#- ROM_LOAD( "g-4e-a.bin",   0x04000, 0x2000, CRC(fbe9276e) SHA1(84181c8da79e2c92af04aef3ab5d23f70969dad8) )








#gfx
zxb.py library/spritemap01.zxi --org=$((0x0000))
cat dummy32k.bin >> spritemap01b.bin
dd ibs=1 count=$((0x6000)) skip=$((0x0010)) if=spritemap01.bin of=spritemap01b.bin

xxd -p -c 3 -s 0 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4k-.bin
xxd -p -c 3 -s 1 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4f-.bin
xxd -p -c 3 -s 2 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4l-.bin
xxd -p -c 3 -s 0 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4h-.bin
xxd -p -c 3 -s 1 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-3n-.bin
xxd -p -c 3 -s 2 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4n-.bin
xxd -p -c 3 -s 0 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4m-.bin
xxd -p -c 3 -s 1 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-3m-.bin
xxd -p -c 3 -s 2 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4c-.bin
xxd -p -c 3 -s 0 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4e-.bin
xxd -p -c 3 -s 1 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4d-.bin
xxd -p -c 3 -s 2 -l $((0x6000)) spritemap01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-4a-.bin


#cat spritemap01b.bin spritemap01b.bin > kungfum/lr-b-4k
#cat spritemap01b.bin spritemap01b.bin > kungfum/lr-b-3n
#cat spritemap01b.bin spritemap01b.bin > kungfum/lr-b-4c
rm spritemap01.bin spritemap01b.bin

#- ROM_REGION( 0x18000, "gfx2", 0 )
#- ROM_LOAD( "b-4k-.bin",    0x00000, 0x2000, CRC(16fb5150) SHA1(a49faf617f948d3ccec2bc6ef97bd399f0958f65) )   /* sprites */
#- ROM_LOAD( "b-4f-.bin",    0x02000, 0x2000, CRC(67745a33) SHA1(fcc642fb1b932676c84c1a0901b989673c57c0e5) )
#- ROM_LOAD( "b-4l-.bin",    0x04000, 0x2000, CRC(bd1c2261) SHA1(7155789a01801a9e1a55d4e68c94a3a3ee7d1b2e) )
#- ROM_LOAD( "b-4h-.bin",    0x06000, 0x2000, CRC(8ac5ed3a) SHA1(9c88e8c82420428b43923cdee7eb4504882bec69) )
#- ROM_LOAD( "b-3n-.bin",    0x08000, 0x2000, CRC(28a213aa) SHA1(0d6d668490bdf4394bc9fed2f3cdc72f2fea46f9) )
#- ROM_LOAD( "b-4n-.bin",    0x0a000, 0x2000, CRC(d5228df3) SHA1(836c4f95f873fbf07f9bec63a72c20a14651117c) )
#- ROM_LOAD( "b-4m-.bin",    0x0c000, 0x2000, CRC(b16de4f2) SHA1(512260e76c9cd21b8add771de53fbd27c2719213) )
#- ROM_LOAD( "b-3m-.bin",    0x0e000, 0x2000, CRC(eba0d66b) SHA1(028f82fc1853b86a3201b24871f41091c3e0b542) )
#- ROM_LOAD( "b-4c-.bin",    0x10000, 0x2000, CRC(01298885) SHA1(d4edf5fe707c5b7231ba72b731b96120064a7ecd) )
#- ROM_LOAD( "b-4e-.bin",    0x12000, 0x2000, CRC(c77b87d4) SHA1(c0f66f0130f6a290a58a3d77bba1d06f16016901) )
#- ROM_LOAD( "b-4d-.bin",    0x14000, 0x2000, CRC(6a70615f) SHA1(f4683dc0a566567e95e85268612bcf0e6297d955) )
#- ROM_LOAD( "b-4a-.bin",    0x16000, 0x2000, CRC(6189d626) SHA1(ce8e5e95c2684c685481e9c8d921380b20ac0460) )













zxb.py library/palettechr01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palettechr01.bin of=kungfum/82s123.7f
dd ibs=1 count=768 skip=$((0x0010)) if=palettechr01.bin of=palettechr01b.bin
xxd -p -c 3 -s 0 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-1j-.bin
xxd -p -c 3 -s 1 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-1f-.bin
xxd -p -c 3 -s 2 -l 768 palettechr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/g-1h-.bin
rm palettechr01.bin palettechr01b.bin

#- ROM_REGION( 0x0720, "proms", 0 )
#- ROM_LOAD( "g-1j-.bin",    0x0000, 0x0100, CRC(668e6bca) SHA1(cd5262b1310821ba7b12873e4db35f081d6b9df4) )    /* character palette red component */
#- ROM_LOAD( "g-1f-.bin",    0x0200, 0x0100, CRC(964b6495) SHA1(76f30a65a0ded14babad2006221aa40621fb7ea1) )    /* character palette green component */
#- ROM_LOAD( "g-1h-.bin",    0x0400, 0x0100, CRC(550563e1) SHA1(11edb45acba8b28a462c49956ebb1ba0a8b2ff26) )    /* character palette blue component */






zxb.py library/palettespr01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palettespr01.bin of=kungfum/82s123.7f
dd ibs=1 count=768 skip=$((0x0010)) if=palettespr01.bin of=palettespr01b.bin
xxd -p -c 3 -s 0 -l 768 palettespr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-1m-.bin
xxd -p -c 3 -s 1 -l 768 palettespr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-1n-.bin
xxd -p -c 3 -s 2 -l 768 palettespr01b.bin | cut -b 1-2 | xxd -r -p > kungfum/b-1l-.bin
rm palettespr01.bin palettespr01b.bin

#- ROM_LOAD( "b-1m-.bin",    0x0100, 0x0100, CRC(76c05a9c) SHA1(1f46f436a17f8c883bdd6d9804b828a81a76f880) )    /* sprite palette red component */
#- ROM_LOAD( "b-1n-.bin",    0x0300, 0x0100, CRC(23f06b99) SHA1(6b3d6349f019aeab33838ae392bc3f3f89906326) )    /* sprite palette green component */
#- ROM_LOAD( "b-1l-.bin",    0x0500, 0x0100, CRC(35e45021) SHA1(511b94507f41b377f38184ed9a85f34949b28d26) )    /* sprite palette blue component */






dd bs=32 count=1 if=/dev/zero of=kungfum/b-5f-.bin
dd bs=256 count=1 if=/dev/zero of=kungfum/b-6f-.bin

#- ROM_LOAD( "b-5f-.bin",    0x0600, 0x0020, CRC(7a601c3d) SHA1(5c5cdf51b2c9fdb2b05402d9c260208ae73fe245) )    /* sprite height, one entry per 32 sprites. Used at run time! */
#- ROM_LOAD( "b-6f-.bin",    0x0620, 0x0100, CRC(82c20d12) SHA1(268903f7d9be58a70d030b02bf31a2d6b5b6e249) )    /* video timing - same as battroad */



#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=kungfum/82s126.4a
#rm clut01.bin

rm smsboot.bin dummy32k.bin

zip -r kungfum kungfum
rm kungfum/*
rmdir kungfum


mame -w -sound none -video soft -resolution0 512x512 -rp ./ kungfum
#xmame.SDL -rp ./ kungfum














