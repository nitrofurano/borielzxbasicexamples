rm example01.asm
rm blktiger.zip
rm blktiger/*
rmdir blktiger
mkdir blktiger
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=blktiger/bdu-01a.5e
rm example01.bin
dd bs=$((0x10000)) count=1 if=/dev/zero of=blktiger/bdu-02a.6e
dd bs=$((0x10000)) count=1 if=/dev/zero of=blktiger/bdu-03a.8e
dd bs=$((0x10000)) count=1 if=/dev/zero of=blktiger/bd-04.9e
dd bs=$((0x10000)) count=1 if=/dev/zero of=blktiger/bd-05.10e

#audiocpu
dd bs=$((0x8000)) count=1 if=/dev/zero of=blktiger/bd-06.1l

#mcu
dd bs=$((0x1000)) count=1 if=/dev/zero of=blktiger/bd.6k




#gfx
zxb.py library/charmaphigemaru01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmaphigemaru01.bin of=charmaphigemaru01b.bin
rm charmaphigemaru01.bin
#gfx1
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-15.2n
#gfx2
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-12.5b
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-11.4b
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-14.9b
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-13.8b
#gfx3
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-08.5a
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-07.4a
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-10.9a
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=blktiger/bd-09.8a
rm charmaphigemaru01b.bin




#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=blktiger/hgb3.l6
#rm palette01b.bin
#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=blktiger/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=blktiger/hgb1.h7  #?sp
#rm clut01b.bin

#proms
dd bs=$((0x100)) count=1 if=/dev/urandom of=blktiger/bd01.8j
dd bs=$((0x100)) count=1 if=/dev/urandom of=blktiger/bd02.9j
dd bs=$((0x100)) count=1 if=/dev/urandom of=blktiger/bd03.11k
dd bs=$((0x100)) count=1 if=/dev/urandom of=blktiger/bd04.11l



zip -r blktiger blktiger
rm blktiger/*
rmdir blktiger
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ blktiger

