	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$FDFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_eee), hl
	ld hl, 0
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _blktigerpalette
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 1023
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 0
	push hl
	ld hl, 768
	push hl
	call _blktigerpalette
	ld hl, 1365
	push hl
	ld hl, 769
	push hl
	call _blktigerpalette
	ld hl, 2730
	push hl
	ld hl, 770
	push hl
	call _blktigerpalette
	ld hl, 4095
	push hl
	ld hl, 771
	push hl
	call _blktigerpalette
	ld hl, 3840
	push hl
	ld hl, 0
	push hl
	call _blktigerpalette
	ld hl, 3920
	push hl
	ld hl, 5
	push hl
	call _blktigerpalette
	ld hl, 4000
	push hl
	ld hl, 10
	push hl
	call _blktigerpalette
	ld hl, 4080
	push hl
	ld hl, 15
	push hl
	call _blktigerpalette
	ld hl, 2
	ld (_ee2), hl
	jp __LABEL5
__LABEL8:
	ld hl, 2
	ld (_ee1), hl
	jp __LABEL10
__LABEL13:
	xor a
	push af
	ld a, 64
	push af
	ld hl, (_ee2)
	ld de, 4
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 4
	add hl, de
	push hl
	call _blktigerputtile
__LABEL14:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL10:
	ld hl, 5
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL9:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL5:
	ld hl, 5
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 2
	ld (_ee2), hl
	jp __LABEL15
__LABEL18:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL20
__LABEL23:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld (_ee0), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_ee0)
	ld a, l
	push af
	ld de, 3
	ld hl, (_seed)
	call __BAND16
	ld a, l
	push af
	ld hl, (_ee2)
	push hl
	ld hl, (_ee1)
	push hl
	call _blktigerputchar
__LABEL24:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL20:
	ld hl, 31
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL15:
	ld hl, 29
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL25
__LABEL28:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL30
__LABEL33:
	xor a
	push af
	ld hl, (_ee2)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld a, l
	push af
	ld hl, (_ee2)
	ld de, 13
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 15
	add hl, de
	push hl
	call _blktigerputchar
__LABEL34:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL30:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
__LABEL29:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL25:
	ld hl, 15
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
	ld hl, 8
	ld (_ex1), hl
	ld hl, 8
	ld (_ey1), hl
	ld hl, 0
	ld (_eee), hl
__LABEL35:
	xor a
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld hl, (_ey1)
	push hl
	ld hl, (_ex1)
	push hl
	call _blktigerputchar
	ld de, 1023
	ld hl, (_ey1)
	call __BAND16
	push hl
	ld de, 1023
	ld hl, (_ex1)
	call __BAND16
	push hl
	call _blktigerscroll
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 3
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld bc, 0
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 5
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld bc, 1
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 6
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld de, 1
	ld hl, (_ee0)
	call __BAND16
	ex de, hl
	ld hl, (_ex1)
	add hl, de
	push hl
	ld de, 2
	ld hl, (_ee0)
	call __BAND16
	srl h
	rr l
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ex1), hl
	ld de, 4
	ld hl, (_ee0)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	ld hl, (_ey1)
	add hl, de
	push hl
	ld de, 8
	ld hl, (_ee0)
	call __BAND16
	ld de, 8
	call __DIVU16
	ex de, hl
	pop hl
	or a
	sbc hl, de
	ld (_ey1), hl
	ld bc, 2
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 7
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld bc, 3
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 8
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld bc, 4
	in a, (c)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 3
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 2
	push hl
	call _blktigerputchar
	xor a
	push af
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	push af
	ld hl, 9
	push hl
	ld hl, 1
	push hl
	call _blktigerputchar
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL35
__LABEL36:
__LABEL37:
	jp __LABEL37
__LABEL38:
	ld hl, 0
	ld (_seed), hl
	ld hl, 0
	ld (_eee), hl
	jp __LABEL39
__LABEL42:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _blktigerpalette
__LABEL43:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL39:
	ld hl, 1023
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL42
__LABEL41:
__LABEL44:
	ld hl, 65024
	ld (_eee), hl
	jp __LABEL46
__LABEL49:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld a, 255
	ld bc, 6
	out (c), a
__LABEL50:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL46:
	ld hl, 65535
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL49
__LABEL48:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL51
__LABEL54:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL56
__LABEL59:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld (_ee2), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_ee2)
	ld a, l
	push af
	ld hl, (_seed)
	ld a, l
	push af
	ld hl, (_ee0)
	push hl
	ld hl, (_ee1)
	push hl
	call _blktigerputtile
	ld a, 255
	ld bc, 6
	out (c), a
__LABEL60:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL56:
	ld hl, 127
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL59
__LABEL58:
__LABEL55:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL51:
	ld hl, 31
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL54
__LABEL53:
	jp __LABEL44
__LABEL45:
__LABEL61:
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL63
__LABEL66:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL68
__LABEL71:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL73
__LABEL76:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL78
__LABEL81:
	ld hl, (_ee3)
	ld de, 2048
	call __MUL16_FAST
	push hl
	ld hl, (_ee2)
	ld de, 512
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_ee0)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_ee1)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld (_eee), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 49152
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 49153
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL82:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL78:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL81
__LABEL80:
__LABEL77:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL73:
	ld hl, 3
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL76
__LABEL75:
__LABEL72:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL68:
	ld hl, 15
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL71
__LABEL70:
__LABEL67:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL63:
	ld hl, 1
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL66
__LABEL65:
	jp __LABEL61
__LABEL62:
__LABEL83:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL85
__LABEL88:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL90
__LABEL93:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL95
__LABEL98:
	ld hl, (_ee2)
	ld de, 512
	call __MUL16_FAST
	push hl
	ld hl, (_ee0)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_ee1)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld (_eee), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 51200
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 51201
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 30000
	push hl
	call _smsdelay
__LABEL99:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL95:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL98
__LABEL97:
__LABEL94:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL90:
	ld hl, 3
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL93
__LABEL92:
__LABEL89:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL85:
	ld hl, 15
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL88
__LABEL87:
	jp __LABEL83
__LABEL84:
__LABEL100:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL102
__LABEL105:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL107
__LABEL110:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL112
__LABEL115:
	ld hl, (_ee2)
	ld de, 512
	call __MUL16_FAST
	push hl
	ld hl, (_ee0)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_ee1)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld (_eee), hl
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 49152
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 49153
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 30000
	push hl
	call _smsdelay
__LABEL116:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL112:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL115
__LABEL114:
__LABEL111:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL107:
	ld hl, 3
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL110
__LABEL109:
__LABEL106:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL102:
	ld hl, 15
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL105
__LABEL104:
	jp __LABEL100
__LABEL101:
__LABEL117:
	ld hl, 51200
	ld (_eee), hl
	jp __LABEL119
__LABEL122:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 60000
	push hl
	call _smsdelay
	ld hl, 60000
	push hl
	call _smsdelay
	ld hl, 60000
	push hl
	call _smsdelay
__LABEL123:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL119:
	ld hl, 53247
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL122
__LABEL121:
	jp __LABEL117
__LABEL118:
__LABEL124:
	ld hl, 49152
	ld (_eee), hl
	jp __LABEL126
__LABEL129:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 2000
	push hl
	call _smsdelay
__LABEL130:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL126:
	ld hl, 51199
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL129
__LABEL128:
	ld hl, 51200
	ld (_eee), hl
	jp __LABEL131
__LABEL134:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 2000
	push hl
	call _smsdelay
__LABEL135:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL131:
	ld hl, 53247
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL134
__LABEL133:
	jp __LABEL124
__LABEL125:
__LABEL136:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL138
__LABEL141:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _blktigerpalette
	ld hl, 4000
	push hl
	call _smsdelay
__LABEL142:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL138:
	ld hl, 1023
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL141
__LABEL140:
	jp __LABEL136
__LABEL137:
__LABEL143:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL145
__LABEL148:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 55296
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, (_eee)
	ld de, 56320
	add hl, de
	push hl
	ld hl, (_seed)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 4000
	push hl
	call _smsdelay
__LABEL149:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL145:
	ld hl, 1023
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL148
__LABEL147:
	jp __LABEL143
__LABEL144:
__LABEL150:
	ld hl, 55296
	ld (_eee), hl
	jp __LABEL152
__LABEL155:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 3000
	push hl
	call _smsdelay
__LABEL156:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL152:
	ld hl, 56319
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL155
__LABEL154:
	ld hl, 56320
	ld (_eee), hl
	jp __LABEL157
__LABEL160:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 3000
	push hl
	call _smsdelay
__LABEL161:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL157:
	ld hl, 57343
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL160
__LABEL159:
	jp __LABEL150
__LABEL151:
__LABEL162:
	ld hl, 54272
	ld (_eee), hl
	jp __LABEL164
__LABEL167:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL168:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL164:
	ld hl, 55295
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL167
__LABEL166:
	ld hl, 53248
	ld (_eee), hl
	jp __LABEL169
__LABEL172:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
	ld hl, 1000
	push hl
	call _smsdelay
__LABEL173:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL169:
	ld hl, 54271
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL172
__LABEL171:
	ld hl, 65024
	ld (_eee), hl
	jp __LABEL174
__LABEL177:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL178:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL174:
	ld hl, 65535
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL177
__LABEL176:
	jp __LABEL162
__LABEL163:
__LABEL179:
	jp __LABEL179
__LABEL180:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_smsdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
smsdelayloop:
		dec bc
		ld a,b
		or c
		jp nz,smsdelayloop
#line 8
_smsdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_blktigerputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 54272
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_blktigerputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_blktigerputtile:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 15
	pop hl
	call __BAND16
	add hl, hl
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 15
	pop hl
	call __BAND16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 48
	pop hl
	call __BAND16
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 64
	pop hl
	call __BAND16
	ld de, 64
	call __DIVU16
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 16
	pop hl
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	pop hl
	call __BXOR16
	ld de, 2048
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	ld (_blktigerputtile_tad1), hl
	ld de, 49152
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld hl, (_blktigerputtile_tad1)
	ld de, 49153
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_blktigerputtile__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_blktigerpalette:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 55296
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 4080
	pop hl
	call __BAND16
	ld de, 16
	call __DIVU16
	ld a, l
	pop hl
	ld (hl), a
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 56320
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 15
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_blktigerpalette__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_blktigerscroll:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	ld bc, 8
	out (c), a
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 256
	call __DIVU16
	ld de, 0
	ld a, l
	ld bc, 9
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	ld bc, 10
	out (c), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 256
	call __DIVU16
	ld de, 0
	ld a, l
	ld bc, 11
	out (c), a
_blktigerscroll__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_blktigerputsprite:
	push ix
	ld ix, 0
	add ix, sp
_blktigerputsprite__leave:
	ld sp, ix
	pop ix
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1710 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1711 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1712 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1713 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1714 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1715 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_eee EQU 57360
	_seed EQU 57362
	_ee0 EQU 57364
	_ee1 EQU 57366
	_ee2 EQU 57368
	_ee3 EQU 57370
	_ee4 EQU 57372
	_ee5 EQU 57374
	_ex1 EQU 57376
	_ey1 EQU 57378
	_dx1 EQU 57380
	_dy1 EQU 57382
	_blktigerputtile_tad1 EQU 57344
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
