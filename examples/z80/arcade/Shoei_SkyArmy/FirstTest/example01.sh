rm skyarmy.zip
rm example01.asm
rm skyarmy/*
rmdir skyarmy
mkdir skyarmy





dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin





#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=skyarmy/a1h.bin
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=skyarmy/a2h.bin
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=skyarmy/a3h.bin
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=skyarmy/j4.bin

#- ROM_START( skyarmy )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "a1h.bin", 0x0000, 0x2000, CRC(e3fb9d70) SHA1(b8e3a6d7d6ef30c1397f9b741132c5257c16be2d) )
#- ROM_LOAD( "a2h.bin", 0x2000, 0x2000, CRC(0417653e) SHA1(4f6ad7335b5b7e85b4e16cce3c127488c02401b2) )
#- ROM_LOAD( "a3h.bin", 0x4000, 0x2000, CRC(95485e56) SHA1(c4cbcd31ba68769d2d0d0875e2a92982265339ae) )
#- ROM_LOAD( "j4.bin",  0x6000, 0x2000, CRC(843783df) SHA1(256d8375a8af7de080d456dbc6290a22473d011b) )








#gfx

zxb.py library/b2r1f0_charmapscramble01.zxi --org=$((0x0000))
cat dummy128k.bin >> b2r1f0_charmapscramble01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=b2r1f0_charmapscramble01b.bin

#dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=skyarmy/13b.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=skyarmy/15b.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=skyarmy/8b.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01.bin of=skyarmy/10b.bin

xxd -p -c 2 -s 0 -l $((0x1000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > skyarmy/13b.bin
xxd -p -c 2 -s 1 -l $((0x1000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > skyarmy/15b.bin
xxd -p -c 2 -s 0 -l $((0x1000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > skyarmy/8b.bin
xxd -p -c 2 -s 1 -l $((0x1000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > skyarmy/10b.bin

rm b1r1f0_charmapkamikaze01.bin b2r1f0_charmapscramble01.bin b2r1f0_charmapscramble01b.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=skyarmy/skyarmy.3j
#dd bs=$((0x20)) count=1 if=/dev/urandom of=skyarmy/pr.2l

#- ROM_REGION( 0x1000, "gfx1", 0 )
#- ROM_LOAD( "13b.bin", 0x0000, 0x0800, CRC(3b0e0f7c) SHA1(2bbba10121d3e745146f50c14dc6df97de40fb96) )
#- ROM_LOAD( "15b.bin", 0x0800, 0x0800, CRC(5ccfd782) SHA1(408406ae068e5578b8a742abed1c37dcd3720fe5) )
#- ROM_REGION( 0x1000, "gfx2", 0 )
#- ROM_LOAD( "8b.bin",  0x0000, 0x0800, CRC(6ac6bd98) SHA1(e653d80ec1b0f8e07821ea781942dae3de7d238d) )
#- ROM_LOAD( "10b.bin", 0x0800, 0x0800, CRC(cada7682) SHA1(83ce8336274cb8006a445ac17a179d9ffd4d6809) )












#proms

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy128k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=skyarmy/a6.bin
rm palette01.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=skyarmy/skyarmy.3j
#dd bs=$((0x20)) count=1 if=/dev/urandom of=skyarmy/pr.2l

#- ROM_REGION( 0x0020, "proms", 0 )
#- ROM_LOAD( "a6.bin",  0x0000, 0x0020, CRC(c721220b) SHA1(61b3320fb616c0600d56840cb6438616c7e0c6eb) )
#- ROM_END










rm smsboot.bin dummy128k.bin example01.bin

zip -r skyarmy skyarmy
rm skyarmy/*
rmdir skyarmy

mame -w -video soft -resolution0 512x512 -rp ./ skyarmy





#- ROM_START( skyarmy )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "a1h.bin", 0x0000, 0x2000, CRC(e3fb9d70) SHA1(b8e3a6d7d6ef30c1397f9b741132c5257c16be2d) )
#- ROM_LOAD( "a2h.bin", 0x2000, 0x2000, CRC(0417653e) SHA1(4f6ad7335b5b7e85b4e16cce3c127488c02401b2) )
#- ROM_LOAD( "a3h.bin", 0x4000, 0x2000, CRC(95485e56) SHA1(c4cbcd31ba68769d2d0d0875e2a92982265339ae) )
#- ROM_LOAD( "j4.bin",  0x6000, 0x2000, CRC(843783df) SHA1(256d8375a8af7de080d456dbc6290a22473d011b) )

#- ROM_REGION( 0x1000, "gfx1", 0 )
#- ROM_LOAD( "13b.bin", 0x0000, 0x0800, CRC(3b0e0f7c) SHA1(2bbba10121d3e745146f50c14dc6df97de40fb96) )
#- ROM_LOAD( "15b.bin", 0x0800, 0x0800, CRC(5ccfd782) SHA1(408406ae068e5578b8a742abed1c37dcd3720fe5) )

#- ROM_REGION( 0x1000, "gfx2", 0 )
#- ROM_LOAD( "8b.bin",  0x0000, 0x0800, CRC(6ac6bd98) SHA1(e653d80ec1b0f8e07821ea781942dae3de7d238d) )
#- ROM_LOAD( "10b.bin", 0x0800, 0x0800, CRC(cada7682) SHA1(83ce8336274cb8006a445ac17a179d9ffd4d6809) )

#- ROM_REGION( 0x0020, "proms", 0 )
#- ROM_LOAD( "a6.bin",  0x0000, 0x0020, CRC(c721220b) SHA1(61b3320fb616c0600d56840cb6438616c7e0c6eb) )
#- ROM_END






