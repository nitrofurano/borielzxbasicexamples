rm espial.zip
rm example01.asm
rm espial/*
rmdir espial
mkdir espial





dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=espial/esp3.4f
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=espial/esp4.4h
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=espial/esp6.bin
dd ibs=1 count=$((0x1000)) skip=$((0xC000)) if=example01.bin of=espial/esp5.bin

#ROM_START( espial )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "esp3.4f",      0x0000, 0x2000, CRC(0973c8a4) SHA1(d1fc6775870710b3dfea4e58a937ab996021adb1) )
#ROM_LOAD( "esp4.4h",      0x2000, 0x2000, CRC(6034d7e5) SHA1(62c9699088f4ee1c69ec10a2f82feddd4083efef) )
#ROM_LOAD( "esp6.bin",     0x4000, 0x1000, CRC(357025b4) SHA1(8bc62f564fcbe37bd490452b2d569d1981f76db1) )
#ROM_LOAD( "esp5.bin",     0xc000, 0x1000, CRC(d03a2fc4) SHA1(791d70e4354350507f4c39d6115c046254168895) )









dd bs=$((0x1000)) count=1 if=/dev/zero of=espial/esp1.4n
dd bs=$((0x1000)) count=1 if=/dev/zero of=espial/esp2.4r

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "esp1.4n",      0x0000, 0x1000, CRC(fc7729e9) SHA1(96dfec574521fa4fe2588fbac2ef1caba6c1b884) )
#ROM_LOAD( "esp2.4r",      0x1000, 0x1000, CRC(e4e256da) SHA1(8007471405bdcf90e29657a3ac2c2f84c9db7c9b) )


















zxb.py library/b2r0f0_charmapespial01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r0f0_charmapespial01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0_charmapespial01.bin of=espial/espial8.4b
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0_charmapespial01.bin of=espial/espial7.4a
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0_charmapespial01.bin of=espial/espial10.4e
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0_charmapespial01.bin of=espial/espial9.4d
rm b2r0f0_charmapespial01.bin

#ROM_REGION( 0x3000, "gfx1", 0 )
#ROM_LOAD( "espial8.4b",   0x0000, 0x2000, CRC(2f43036f) SHA1(316e9fab778d6c0abb0b6673aba33dfbe44b1262) )
#ROM_LOAD( "espial7.4a",   0x2000, 0x1000, CRC(ebfef046) SHA1(5aa6efb7254fb42e814c1a29c5363f2d0727452f) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "espial10.4e",  0x0000, 0x1000, CRC(de80fbc1) SHA1(f5601eac8cb35a92c51bf81e5ac5a2b79bcde28f) )
#ROM_LOAD( "espial9.4d",   0x1000, 0x1000, CRC(48c258a0) SHA1(55e72b9072ddc05f848e5a6fae159c554102010b) )







#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=espial/espial.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=espial/espial.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=espial/espial.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=espial/espial.11j
#rm clut01b.bin
#????
dd bs=256 count=1 if=/dev/urandom of=espial/mmi6301.1f
dd bs=256 count=1 if=/dev/urandom of=espial/mmi6301.1h

#ROM_REGION( 0x0200, "proms", 0 ) /* The MMI6301 Bipolar PROM is compatible to the 82s129 */
#ROM_LOAD( "mmi6301.1f",   0x0000, 0x0100, CRC(d12de557) SHA1(53e8a57dfab677cc5b9cdd83d2fbeb93169bcefd) ) /* palette low 4 bits */
#ROM_LOAD( "mmi6301.1h",   0x0100, 0x0100, CRC(4c84fe70) SHA1(7ac52bd5b19663b9526ecb678e61db9939d2285d) ) /* palette high 4 bits */
#ROM_END





rm smsboot.bin dummy32k.bin example01.bin

zip -r espial espial
rm espial/*
rmdir espial

mame -w -video soft -resolution0 512x512 -rp ./ espial






#ROM_START( espial )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "esp3.4f",      0x0000, 0x2000, CRC(0973c8a4) SHA1(d1fc6775870710b3dfea4e58a937ab996021adb1) )
#ROM_LOAD( "esp4.4h",      0x2000, 0x2000, CRC(6034d7e5) SHA1(62c9699088f4ee1c69ec10a2f82feddd4083efef) )
#ROM_LOAD( "esp6.bin",     0x4000, 0x1000, CRC(357025b4) SHA1(8bc62f564fcbe37bd490452b2d569d1981f76db1) )
#ROM_LOAD( "esp5.bin",     0xc000, 0x1000, CRC(d03a2fc4) SHA1(791d70e4354350507f4c39d6115c046254168895) )

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "esp1.4n",      0x0000, 0x1000, CRC(fc7729e9) SHA1(96dfec574521fa4fe2588fbac2ef1caba6c1b884) )
#ROM_LOAD( "esp2.4r",      0x1000, 0x1000, CRC(e4e256da) SHA1(8007471405bdcf90e29657a3ac2c2f84c9db7c9b) )

#ROM_REGION( 0x3000, "gfx1", 0 )
#ROM_LOAD( "espial8.4b",   0x0000, 0x2000, CRC(2f43036f) SHA1(316e9fab778d6c0abb0b6673aba33dfbe44b1262) )
#ROM_LOAD( "espial7.4a",   0x2000, 0x1000, CRC(ebfef046) SHA1(5aa6efb7254fb42e814c1a29c5363f2d0727452f) )
#ROM_REGION( 0x2000, "gfx2", 0 )
#ROM_LOAD( "espial10.4e",  0x0000, 0x1000, CRC(de80fbc1) SHA1(f5601eac8cb35a92c51bf81e5ac5a2b79bcde28f) )
#ROM_LOAD( "espial9.4d",   0x1000, 0x1000, CRC(48c258a0) SHA1(55e72b9072ddc05f848e5a6fae159c554102010b) )

#ROM_REGION( 0x0200, "proms", 0 ) /* The MMI6301 Bipolar PROM is compatible to the 82s129 */
#ROM_LOAD( "mmi6301.1f",   0x0000, 0x0100, CRC(d12de557) SHA1(53e8a57dfab677cc5b9cdd83d2fbeb93169bcefd) ) /* palette low 4 bits */
#ROM_LOAD( "mmi6301.1h",   0x0100, 0x0100, CRC(4c84fe70) SHA1(7ac52bd5b19663b9526ecb678e61db9939d2285d) ) /* palette high 4 bits */
#ROM_END





