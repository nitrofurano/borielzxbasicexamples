# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm pacman/*
rmdir pacman
mkdir pacman

#- zxb.py example04.zxb --asm --org=$((0x0000))
#- zxb.py example04.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example04.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example04.zxb --heap-size=128 --org=$((0x0069))
dd bs=16384 count=1 if=/dev/zero of=dummy16k.bin

cat example04.bin >> smsboot2.bin
rm example04.bin
mv smsboot2.bin example04.bin

cat dummy16k.bin >> example04.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example04.bin of=pacman/pacman.6e
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example04.bin of=pacman/pacman.6f
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example04.bin of=pacman/pacman.6h
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example04.bin of=pacman/pacman.6j
rm dummy16k.bin example04.bin

zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=pacman/82s123.7f
rm palette01.bin

zxb.py library/clut01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=pacman/82s126.4a
rm clut01.bin

zxb.py library/charmap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=pacman/pacman.5e
rm charmap01.bin

zxb.py library/spritemap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=pacman/pacman.5f
rm spritemap01.bin

dd bs=256 count=1 if=/dev/zero of=pacman/82s126.1m
dd bs=256 count=1 if=/dev/zero of=pacman/82s126.3m

rm smsboot.bin

zip -r pacman pacman
rm pacman/*
rmdir pacman

mame -w -video soft -resolution0 512x512 -rp ./ pacman
#xmame.SDL -rp ./ pacman

