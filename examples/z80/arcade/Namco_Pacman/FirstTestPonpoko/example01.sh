rm example01.asm
rm ponpoko.zip
rm ponpoko/*
rmdir ponpoko
mkdir ponpoko

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin





#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=ponpoko/ppokoj1.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=ponpoko/ppokoj2.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=ponpoko/ppokoj3.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=ponpoko/ppokoj4.bin
dd ibs=1 count=$((0x1000)) skip=$((0x8000)) if=example01.bin of=ponpoko/ppoko5.bin
dd ibs=1 count=$((0x1000)) skip=$((0x9000)) if=example01.bin of=ponpoko/ppoko6.bin
dd ibs=1 count=$((0x1000)) skip=$((0xA000)) if=example01.bin of=ponpoko/ppoko7.bin
dd ibs=1 count=$((0x1000)) skip=$((0xB000)) if=example01.bin of=ponpoko/ppokoj8.bin

#- ROM_START( ponpoko )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "ppokoj1.bin",  0x0000, 0x1000, CRC(ffa3c004) SHA1(d9e3186dcd4eb94d02bd24ad56030b248721537f) )
#- ROM_LOAD( "ppokoj2.bin",  0x1000, 0x1000, CRC(4a496866) SHA1(4b8bd13e58040c30ca032b54fb47d889677e8c6f) )
#- ROM_LOAD( "ppokoj3.bin",  0x2000, 0x1000, CRC(17da6ca3) SHA1(1a57767557c13fa3d08e4451fb9fb1f7219b26ef) )
#- ROM_LOAD( "ppokoj4.bin",  0x3000, 0x1000, CRC(9d39a565) SHA1(d4835ee97c9b3c63504d8b576a11f0a3a97057ec) )
#- ROM_LOAD( "ppoko5.bin",   0x8000, 0x1000, CRC(54ca3d7d) SHA1(b54299b00573fbd6d3278586df0c12c09235615d) )
#- ROM_LOAD( "ppoko6.bin",   0x9000, 0x1000, CRC(3055c7e0) SHA1(ab3fb9c8846effdcea0569d08a84c5fa19057a8f) )
#- ROM_LOAD( "ppoko7.bin",   0xa000, 0x1000, CRC(3cbe47ca) SHA1(577c79c016be26a9fc7895cef0f30bf3f0b15097) )
#- ROM_LOAD( "ppokoj8.bin",  0xb000, 0x1000, CRC(04b63fc6) SHA1(9b86ae34aaefa2813d29a4f7b24cee40eadcc6a1) )





#gfx
zxb.py library/b2r0f0s0_charmaprealmahjongmaihai01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r0f0s0_charmaprealmahjongmaihai01.bin of=ponpoko/ppoko9.bin
zxb.py library/spritemap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=ponpoko/ppoko10.bin
rm b2r0f0s0_charmaprealmahjongmaihai01.bin spritemap01.bin

#- ROM_REGION( 0x2000, "gfx1", 0 )
#- ROM_LOAD( "ppoko9.bin",   0x0000, 0x1000, CRC(b73e1a06) SHA1(f1229e804eb15827b71f0e769a8c9e496c6d1de7) )
#- ROM_LOAD( "ppoko10.bin",  0x1000, 0x1000, CRC(62069b5d) SHA1(1b58ad1c2cc2d12f4e492fdd665b726d50c80364) )






#palette
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=ponpoko/82s123.7f
zxb.py library/clut01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=ponpoko/82s126.4a

#- ROM_REGION( 0x0120, "proms", 0 )
#- ROM_LOAD( "82s123.7f",    0x0000, 0x0020, CRC(2fc650bd) SHA1(8d0268dee78e47c712202b0ec4f1f51109b1f2a5) )
#- ROM_LOAD( "82s126.4a",    0x0020, 0x0100, CRC(3eb3a8e4) SHA1(19097b5f60d1030f8b82d9f1d3a241f93e5c75d6) )






#prom
dd bs=$((0x0100)) count=1 if=/dev/zero of=ponpoko/82s126.1m
dd bs=$((0x0100)) count=1 if=/dev/zero of=ponpoko/82s126.3m

#- ROM_REGION( 0x0200, "namco", 0 )    /* sound PROMs */
#- ROM_LOAD( "82s126.1m",    0x0000, 0x0100, CRC(a9cc86bf) SHA1(bbcec0570aeceb582ff8238a4bc8546a23430081) )
#- ROM_LOAD( "82s126.3m",    0x0100, 0x0100, CRC(77245b66) SHA1(0c4d0bee858b97632411c440bea6948a74759746) )    /* timing - not used */
#- ROM_END






rm smsboot.bin dummy64k.bin example01.bin spritemap01.bin clut01.bin charmap01.bin palette01.bin

zip -r ponpoko ponpoko
rm ponpoko/*
rmdir ponpoko

mame -w -video soft -resolution0 512x512 -rp ./ ponpoko




#- ROM_START( ponpoko )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "ppokoj1.bin",  0x0000, 0x1000, CRC(ffa3c004) SHA1(d9e3186dcd4eb94d02bd24ad56030b248721537f) )
#- ROM_LOAD( "ppokoj2.bin",  0x1000, 0x1000, CRC(4a496866) SHA1(4b8bd13e58040c30ca032b54fb47d889677e8c6f) )
#- ROM_LOAD( "ppokoj3.bin",  0x2000, 0x1000, CRC(17da6ca3) SHA1(1a57767557c13fa3d08e4451fb9fb1f7219b26ef) )
#- ROM_LOAD( "ppokoj4.bin",  0x3000, 0x1000, CRC(9d39a565) SHA1(d4835ee97c9b3c63504d8b576a11f0a3a97057ec) )
#- ROM_LOAD( "ppoko5.bin",   0x8000, 0x1000, CRC(54ca3d7d) SHA1(b54299b00573fbd6d3278586df0c12c09235615d) )
#- ROM_LOAD( "ppoko6.bin",   0x9000, 0x1000, CRC(3055c7e0) SHA1(ab3fb9c8846effdcea0569d08a84c5fa19057a8f) )
#- ROM_LOAD( "ppoko7.bin",   0xa000, 0x1000, CRC(3cbe47ca) SHA1(577c79c016be26a9fc7895cef0f30bf3f0b15097) )
#- ROM_LOAD( "ppokoj8.bin",  0xb000, 0x1000, CRC(04b63fc6) SHA1(9b86ae34aaefa2813d29a4f7b24cee40eadcc6a1) )

#- ROM_REGION( 0x2000, "gfx1", 0 )
#- ROM_LOAD( "ppoko9.bin",   0x0000, 0x1000, CRC(b73e1a06) SHA1(f1229e804eb15827b71f0e769a8c9e496c6d1de7) )
#- ROM_LOAD( "ppoko10.bin",  0x1000, 0x1000, CRC(62069b5d) SHA1(1b58ad1c2cc2d12f4e492fdd665b726d50c80364) )

#- ROM_REGION( 0x0120, "proms", 0 )
#- ROM_LOAD( "82s123.7f",    0x0000, 0x0020, CRC(2fc650bd) SHA1(8d0268dee78e47c712202b0ec4f1f51109b1f2a5) )
#- ROM_LOAD( "82s126.4a",    0x0020, 0x0100, CRC(3eb3a8e4) SHA1(19097b5f60d1030f8b82d9f1d3a241f93e5c75d6) )

#- ROM_REGION( 0x0200, "namco", 0 )    /* sound PROMs */
#- ROM_LOAD( "82s126.1m",    0x0000, 0x0100, CRC(a9cc86bf) SHA1(bbcec0570aeceb582ff8238a4bc8546a23430081) )
#- ROM_LOAD( "82s126.3m",    0x0100, 0x0100, CRC(77245b66) SHA1(0c4d0bee858b97632411c440bea6948a74759746) )    /* timing - not used */
#- ROM_END

