rm ppmast93.zip
rm example01.asm
rm ppmast93/*
rmdir ppmast93
mkdir ppmast93




dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin



#cpu
#- zxb.py example01.zxb --asm --org=0x0000
#- zxb.py example01.zxb --org=0x0000
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=0x0069
zxb.py example01.zxb --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x20000)) skip=$((0x0000)) if=example01.bin of=ppmast93/2.up7

##ROM_START( ppmast93 )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "2.up7", 0x00000, 0x20000, CRC(8854d8db) SHA1(9d93ddfb44d533772af6519747a6cb50b42065cd) )





#sub

dd bs=$((0x20000)) count=1 if=/dev/zero of=ppmast93/1.ue7

#ROM_REGION( 0x30000, "sub", 0 )
#ROM_LOAD( "1.ue7", 0x10000, 0x20000, CRC(8e26939e) SHA1(e62441e523f5be6a3889064cc5e0f44545260e93) )





#gfx

zxb.py library/charmap01.zxi --org=0x0000
cat dummy128k.bin >> charmap01.bin
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=charmap01.bin of=ppmast93/3.ug16
dd ibs=1 count=$((0x20000)) skip=$((0x0010)) if=charmap01.bin of=ppmast93/4.ug15
rm charmap01.bin

#ROM_REGION( 0x40000, "gfx1", 0 )
#ROM_LOAD( "3.ug16", 0x00000, 0x20000, CRC(8ab24641) SHA1(c0ebee90bf3fe208947ae5ea56f31469ed24d198) )
#ROM_LOAD( "4.ug15", 0x20000, 0x20000, CRC(b16e9fb6) SHA1(53aa962c63319cd649e0c8cf0c26e2308598e1aa) )






#proms

zxb.py library/palette01.bas --org=0x0000
cat dummy128k.bin >> palette01.bin
dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
xxd -p -c 3 -s 0 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > ppmast93/prom3.ug24
xxd -p -c 3 -s 1 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > ppmast93/prom2.ug25
xxd -p -c 3 -s 2 -l $((0x300)) palette01b.bin | cut -b 1-2 | xxd -r -p > ppmast93/prom1.ug26
rm palette01.bin palette01b.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=ppmast93/ppmast93.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=0x0000
#cat dummy128k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=ppmast93/ppmast93.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=ppmast93/ppmast93.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=ppmast93/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=ppmast93/82s126.3m

#dd bs=256 count=1 if=/dev/urandom of=ppmast93/prom3.ug24 #r
#dd bs=256 count=1 if=/dev/urandom of=ppmast93/prom2.ug25 #g
#dd bs=256 count=1 if=/dev/urandom of=ppmast93/prom1.ug26 #b

#touch ppmast93/prom3.ug24
#touch ppmast93/prom2.ug25
#touch ppmast93/prom1.ug26


#ROM_REGION( 0x300, "proms", 0 )
#ROM_LOAD( "prom3.ug24", 0x000, 0x100, CRC(b1a4415a) SHA1(1dd22260f7dbdc9c812a2349069ed5f3c9c92826) )
#ROM_LOAD( "prom2.ug25", 0x100, 0x100, CRC(4b5055ba) SHA1(6213e79492d35593c643ef5c01ce6a58a77866aa) )
#ROM_LOAD( "prom1.ug26", 0x200, 0x100, CRC(d979c64e) SHA1(172c9579013d58e35a5b4f732e360811ac36295e) )
#ROM_END






rm smsboot.bin dummy128k.bin example01.bin

zip -r ppmast93 ppmast93
rm ppmast93/*
rmdir ppmast93

mame -w -video soft -resolution0 512x512 -rp ./ ppmast93






##ROM_START( ppmast93 )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "2.up7", 0x00000, 0x20000, CRC(8854d8db) SHA1(9d93ddfb44d533772af6519747a6cb50b42065cd) )

#ROM_REGION( 0x30000, "sub", 0 )
#ROM_LOAD( "1.ue7", 0x10000, 0x20000, CRC(8e26939e) SHA1(e62441e523f5be6a3889064cc5e0f44545260e93) )

#ROM_REGION( 0x40000, "gfx1", 0 )
#ROM_LOAD( "3.ug16", 0x00000, 0x20000, CRC(8ab24641) SHA1(c0ebee90bf3fe208947ae5ea56f31469ed24d198) )
#ROM_LOAD( "4.ug15", 0x20000, 0x20000, CRC(b16e9fb6) SHA1(53aa962c63319cd649e0c8cf0c26e2308598e1aa) )

#ROM_REGION( 0x300, "proms", 0 )
#ROM_LOAD( "prom3.ug24", 0x000, 0x100, CRC(b1a4415a) SHA1(1dd22260f7dbdc9c812a2349069ed5f3c9c92826) )
#ROM_LOAD( "prom2.ug25", 0x100, 0x100, CRC(4b5055ba) SHA1(6213e79492d35593c643ef5c01ce6a58a77866aa) )
#ROM_LOAD( "prom1.ug26", 0x200, 0x100, CRC(d979c64e) SHA1(172c9579013d58e35a5b4f732e360811ac36295e) )
#ROM_END




