rm kncljoe.zip
rm example01.asm
rm kncljoe/*
rmdir kncljoe
mkdir kncljoe





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin






#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=kncljoe/kj-1.bin
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=kncljoe/kj-2.bin
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=kncljoe/kj-3.bin
#ROM_START( kncljoe )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "kj-1.bin", 0x0000, 0x4000, CRC(4e4f5ff2) SHA1(7d889aa4f4138f01014c1dda391f82396074cfab) )
#ROM_LOAD( "kj-2.bin", 0x4000, 0x4000, CRC(cb11514b) SHA1(c75d4019d1617493ff074ce8187a81ad70d9b60c) )
#ROM_LOAD( "kj-3.bin", 0x8000, 0x4000, CRC(0f50697b) SHA1(412c6aba270824299ca2a74e9bea42b83e69797b) )






#soundcpu
dd bs=$((0x6000)) count=1 if=/dev/zero of=kncljoe/kj-13.bin
#ROM_REGION( 0x8000, "soundcpu", 0 )  /* 64k for audio code */
#ROM_LOAD( "kj-13.bin",0x6000, 0x2000, CRC(0a0be3f5) SHA1(00be47fc76500843b6f5de63622edb1748ef5f7d) )










#gfx
zxb.py library/b3r0f0_charmapiremm62v01.zxi --org=$((0x0000))
cat dummy32k.bin >> b3r0f0_charmapiremm62v01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=b3r0f0_charmapiremm62v01b.bin
xxd -p -c 3 -s 0 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-10.bin
xxd -p -c 3 -s 1 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-11.bin
xxd -p -c 3 -s 2 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-12.bin
xxd -p -c 3 -s 0 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-4.bin
xxd -p -c 3 -s 1 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-6.bin
xxd -p -c 3 -s 2 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-5.bin
xxd -p -c 3 -s 0 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-7.bin
xxd -p -c 3 -s 1 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-9.bin
xxd -p -c 3 -s 2 -l $((0x100000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > kncljoe/kj-8.bin
rm b3r0f0_charmapiremm62v01.bin b3r0f0_charmapiremm62v01b.bin

#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-10.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-11.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-12.bin
#dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-4.bin
#dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-6.bin
#dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-5.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-7.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-9.bin
#dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=kncljoe/kj-8.bin

#ROM_REGION( 0xc000, "gfx1", 0 ) /* tiles */
#ROM_LOAD( "kj-10.bin", 0x0000,  0x4000, CRC(74d3ba33) SHA1(c7887d690cb7f7a7b24d59d490ffc088fb6cc49c) )
#ROM_LOAD( "kj-11.bin", 0x4000,  0x4000, CRC(8ea01455) SHA1(b4b42fe373a1019b4f2a4b763a8a7219a5c9987e) )
#ROM_LOAD( "kj-12.bin", 0x8000,  0x4000, CRC(33367c41) SHA1(e6c56bcad008f3af4bc0f7d7afe8e23c8eb9d943) )
#ROM_REGION( 0x18000, "gfx2", 0 )    /* sprites */
#ROM_LOAD( "kj-4.bin", 0x00000,  0x8000, CRC(a499ea10) SHA1(cb671cc75b3c6029dd3529e62d83025f78b45271) )
#ROM_LOAD( "kj-6.bin", 0x08000,  0x8000, CRC(815f5c0a) SHA1(ad0b59eeebb2e57035a3f643ac0ef575569bec0f) )
#ROM_LOAD( "kj-5.bin", 0x10000,  0x8000, CRC(11111759) SHA1(504c62fc6778a4afa86cba69634652708535bef6) )
#ROM_REGION( 0xc000, "gfx3", 0 ) /* sprites */
#ROM_LOAD( "kj-7.bin", 0x0000,   0x4000, CRC(121fcccb) SHA1(77f3e7e49787d6a893c5d8c0c3ac612b1180e866) )
#ROM_LOAD( "kj-9.bin", 0x4000,   0x4000, CRC(affbe3eb) SHA1(056111fc5b04ff14b114b5f724d02789c8e3ee10) )
#ROM_LOAD( "kj-8.bin", 0x8000,   0x4000, CRC(e057e72a) SHA1(3a85750c72caaa027f302dc6ca4086bdbd49b5ff) )









#proms

#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=kncljoe/kncljoe.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=kncljoe/kncljoe.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=kncljoe/kncljoe.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=kncljoe/kncljoe.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/urandom of=kncljoe/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=kncljoe/82s126.3m

dd bs=$((0x0100)) count=1 if=/dev/urandom of=kncljoe/kjclr1.bin
dd bs=$((0x0100)) count=1 if=/dev/urandom of=kncljoe/kjclr2.bin
dd bs=$((0x0100)) count=1 if=/dev/urandom of=kncljoe/kjclr3.bin
dd bs=$((0x0100)) count=1 if=/dev/urandom of=kncljoe/kjprom5.bin
dd bs=$((0x0100)) count=1 if=/dev/urandom of=kncljoe/kjprom4.bin

#ROM_REGION( 0x420, "proms", 0 )
#ROM_LOAD( "kjclr1.bin",  0x000, 0x100, CRC(c3378ac2) SHA1(264fdc0718b36e02fc1fc1064a9566e349f4bf25) ) /* tile red */
#ROM_LOAD( "kjclr2.bin",  0x100, 0x100, CRC(2126da97) SHA1(6ca394a5977fab72200a00716a1f25f2a9447896) ) /* tile green */
#ROM_LOAD( "kjclr3.bin",  0x200, 0x100, CRC(fde62164) SHA1(d0f6b8d0dce63ce592a5f0c9dc8e6260f69a9141) ) /* tile blue */
#ROM_LOAD( "kjprom5.bin", 0x300, 0x020, CRC(5a81dd9f) SHA1(090ec9135b12e85ed02ab71fca55cc8d1ea8215a) ) /* sprite palette */
#ROM_LOAD( "kjprom4.bin", 0x320, 0x100, CRC(48dc2066) SHA1(b8007a5115d475b535284965681ae341f819d3db) ) /* sprite clut */
#ROM_END








rm smsboot.bin dummy32k.bin example01.bin

zip -r kncljoe kncljoe
rm kncljoe/*
rmdir kncljoe

mame -w -video soft -resolution0 512x512 -rp ./ kncljoe









#ROM_START( kncljoe )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "kj-1.bin", 0x0000, 0x4000, CRC(4e4f5ff2) SHA1(7d889aa4f4138f01014c1dda391f82396074cfab) )
#ROM_LOAD( "kj-2.bin", 0x4000, 0x4000, CRC(cb11514b) SHA1(c75d4019d1617493ff074ce8187a81ad70d9b60c) )
#ROM_LOAD( "kj-3.bin", 0x8000, 0x4000, CRC(0f50697b) SHA1(412c6aba270824299ca2a74e9bea42b83e69797b) )

#ROM_REGION( 0x8000, "soundcpu", 0 )  /* 64k for audio code */
#ROM_LOAD( "kj-13.bin",0x6000, 0x2000, CRC(0a0be3f5) SHA1(00be47fc76500843b6f5de63622edb1748ef5f7d) )

#ROM_REGION( 0xc000, "gfx1", 0 ) /* tiles */
#ROM_LOAD( "kj-10.bin", 0x0000,  0x4000, CRC(74d3ba33) SHA1(c7887d690cb7f7a7b24d59d490ffc088fb6cc49c) )
#ROM_LOAD( "kj-11.bin", 0x4000,  0x4000, CRC(8ea01455) SHA1(b4b42fe373a1019b4f2a4b763a8a7219a5c9987e) )
#ROM_LOAD( "kj-12.bin", 0x8000,  0x4000, CRC(33367c41) SHA1(e6c56bcad008f3af4bc0f7d7afe8e23c8eb9d943) )
#ROM_REGION( 0x18000, "gfx2", 0 )    /* sprites */
#ROM_LOAD( "kj-4.bin", 0x00000,  0x8000, CRC(a499ea10) SHA1(cb671cc75b3c6029dd3529e62d83025f78b45271) )
#ROM_LOAD( "kj-6.bin", 0x08000,  0x8000, CRC(815f5c0a) SHA1(ad0b59eeebb2e57035a3f643ac0ef575569bec0f) )
#ROM_LOAD( "kj-5.bin", 0x10000,  0x8000, CRC(11111759) SHA1(504c62fc6778a4afa86cba69634652708535bef6) )
#ROM_REGION( 0xc000, "gfx3", 0 ) /* sprites */
#ROM_LOAD( "kj-7.bin", 0x0000,   0x4000, CRC(121fcccb) SHA1(77f3e7e49787d6a893c5d8c0c3ac612b1180e866) )
#ROM_LOAD( "kj-9.bin", 0x4000,   0x4000, CRC(affbe3eb) SHA1(056111fc5b04ff14b114b5f724d02789c8e3ee10) )
#ROM_LOAD( "kj-8.bin", 0x8000,   0x4000, CRC(e057e72a) SHA1(3a85750c72caaa027f302dc6ca4086bdbd49b5ff) )

#ROM_REGION( 0x420, "proms", 0 )
#ROM_LOAD( "kjclr1.bin",  0x000, 0x100, CRC(c3378ac2) SHA1(264fdc0718b36e02fc1fc1064a9566e349f4bf25) ) /* tile red */
#ROM_LOAD( "kjclr2.bin",  0x100, 0x100, CRC(2126da97) SHA1(6ca394a5977fab72200a00716a1f25f2a9447896) ) /* tile green */
#ROM_LOAD( "kjclr3.bin",  0x200, 0x100, CRC(fde62164) SHA1(d0f6b8d0dce63ce592a5f0c9dc8e6260f69a9141) ) /* tile blue */
#ROM_LOAD( "kjprom5.bin", 0x300, 0x020, CRC(5a81dd9f) SHA1(090ec9135b12e85ed02ab71fca55cc8d1ea8215a) ) /* sprite palette */
#ROM_LOAD( "kjprom4.bin", 0x320, 0x100, CRC(48dc2066) SHA1(b8007a5115d475b535284965681ae341f819d3db) ) /* sprite clut */
#ROM_END






