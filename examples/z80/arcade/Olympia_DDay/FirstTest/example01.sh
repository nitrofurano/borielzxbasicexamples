rm dday.zip
rm example01.asm
rm dday/*
rmdir dday
mkdir dday




dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=dday/e8_63co.bin
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=dday/e7_64co.bin
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=dday/e6_65co.bin
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=dday/e5_66co.bin

#ROM_START( dday )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "e8_63co.bin",  0x0000, 0x1000, CRC(13d53793) SHA1(045f4b02803cb24305f90593777bb4a59f1bbb34) )
#ROM_LOAD( "e7_64co.bin",  0x1000, 0x1000, CRC(e1ef2a70) SHA1(946ef20e2cd441ca858f969e7f25ab7c940671f8) )
#ROM_LOAD( "e6_65co.bin",  0x2000, 0x1000, CRC(fe414a83) SHA1(1ca1d30b71b62af5230dfe862a67c4cff5a71f41) )
#ROM_LOAD( "e5_66co.bin",  0x3000, 0x1000, CRC(fc9f7774) SHA1(1071d05e2f0ee8869eeeb46ad219303b417f4c90) )





zxb.py library/b3r0f1_charmapappoooh01.zxi --org=$((0x0000))
cat dummy32k.bin >> b3r0f1_charmapappoooh01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f1_charmapappoooh01.bin of=b3r0f1_charmapappoooh01.bin
xxd -p -c 3 -s 0 -l $((0x1800)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > dday/k4_73.bin
xxd -p -c 3 -s 1 -l $((0x1800)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > dday/k2_71.bin
xxd -p -c 3 -s 2 -l $((0x1800)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > dday/k3_72.bin
rm b3r0f1_charmapappoooh01.bin b3r0f1_charmapappoooh01.bin

zxb.py library/b2r0f1_charmapdday01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r0f1_charmapdday01.bin
dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b2r0f1_charmapdday01.bin of=b2r0f1_charmapdday01b.bin
xxd -p -c 2 -s 0 -l $((0x1000)) b2r0f1_charmapdday01b.bin | cut -b 1-2 | xxd -r -p > dday/j8_70co.bin
xxd -p -c 2 -s 1 -l $((0x1000)) b2r0f1_charmapdday01b.bin | cut -b 1-2 | xxd -r -p > dday/j9_69co.bin
xxd -p -c 2 -s 0 -l $((0x1000)) b2r0f1_charmapdday01b.bin | cut -b 1-2 | xxd -r -p > dday/k6_74o.bin
xxd -p -c 2 -s 1 -l $((0x1000)) b2r0f1_charmapdday01b.bin | cut -b 1-2 | xxd -r -p > dday/k7_75o.bin
rm b2r0f1_charmapdday01.bin b2r0f1_charmapdday01b.bin

zxb.py library/b1r0f1_charmapdday01.zxi --org=$((0x0000))
cat dummy32k.bin >> b1r0f1_charmapdday01.bin
dd ibs=1 count=$((0x0800)) skip=$((0x0010)) if=b1r0f1_charmapdday01.bin of=dday/d4_68.bin
rm b1r0f1_charmapdday01.bin

#ROM_REGION( 0x1800, "gfx1", 0 )
#ROM_LOAD( "k4_73.bin",    0x0000, 0x0800, CRC(fa6237e4) SHA1(0dfe2a0079324a78b462203fe93f7fb186a42122) )
#ROM_LOAD( "k2_71.bin",    0x0800, 0x0800, CRC(f85461de) SHA1(e2ed34e993cd657681124df5531e35afd7d8c34b) )
#ROM_LOAD( "k3_72.bin",    0x1000, 0x0800, CRC(fdfe88b6) SHA1(cdc37d90500f4ce813b6efee31139e6776aa2bff) )
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "j8_70co.bin",  0x0000, 0x0800, CRC(0c60e94c) SHA1(136df37b858a7fd399acc89e59917a068165d749) )
#ROM_LOAD( "j9_69co.bin",  0x0800, 0x0800, CRC(ba341c10) SHA1(c2c7350f87d5e47ac47cb19020681f0e7340e427) )
#ROM_REGION( 0x1000, "gfx3", 0 )
#ROM_LOAD( "k6_74o.bin",   0x0000, 0x0800, CRC(66719aea) SHA1(dd29f8d079868af3c7fd16dc8c383f1eba4543d2) )
#ROM_LOAD( "k7_75o.bin",   0x0800, 0x0800, CRC(5f8772e2) SHA1(16194a02bc7d5248dea7a80bf6d6d263ec8a7fd6) )
#ROM_REGION( 0x0800, "gfx4", 0 )
#ROM_LOAD( "d4_68.bin",    0x0000, 0x0800, CRC(f3649264) SHA1(5486a33fa1f7803e68d141992d6105206da1beba) )
#one of the tilesets looks empty, try to check where the bug is








#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=dday/dday.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=dday/dday.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=dday/dday.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=dday/dday.11j
#rm clut01b.bin
#????

dd bs=$((0x1000)) count=1 if=/dev/urandom of=dday/d2_67.bin
dd bs=$((0x0100)) count=1 if=/dev/urandom of=dday/dday.m11
dd bs=$((0x0100)) count=1 if=/dev/urandom of=dday/dday.m8
dd bs=$((0x0100)) count=1 if=/dev/urandom of=dday/dday.m3

#ROM_REGION( 0x1000, "user1", 0 )
#ROM_LOAD( "d2_67.bin",    0x0000, 0x1000, CRC(2b693e42) SHA1(e52b987cf929ddfc7916b05456b1114076956d12) )  /* search light map */
#ROM_REGION( 0x0300, "proms", 0 )
#ROM_LOAD( "dday.m11",     0x0000, 0x0100, CRC(aef6bbfc) SHA1(9e07729a4389221bc120af91d8275e1d05f3be7a) )  /* red component */
#ROM_LOAD( "dday.m8",      0x0100, 0x0100, CRC(ad3314b9) SHA1(d103f4f6103987ea85f0791ffc66a1cf9c711031) )  /* green component */
#ROM_LOAD( "dday.m3",      0x0200, 0x0100, CRC(e877ab82) SHA1(03e3905aee37f6743e7a4a87338f9504c832a55b) )  /* blue component */
#ROM_END






rm smsboot.bin dummy32k.bin example01.bin

zip -r dday dday
rm dday/*
rmdir dday

mame -w -video soft -resolution0 512x512 -rp ./ dday









#ROM_START( dday )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "e8_63co.bin",  0x0000, 0x1000, CRC(13d53793) SHA1(045f4b02803cb24305f90593777bb4a59f1bbb34) )
#ROM_LOAD( "e7_64co.bin",  0x1000, 0x1000, CRC(e1ef2a70) SHA1(946ef20e2cd441ca858f969e7f25ab7c940671f8) )
#ROM_LOAD( "e6_65co.bin",  0x2000, 0x1000, CRC(fe414a83) SHA1(1ca1d30b71b62af5230dfe862a67c4cff5a71f41) )
#ROM_LOAD( "e5_66co.bin",  0x3000, 0x1000, CRC(fc9f7774) SHA1(1071d05e2f0ee8869eeeb46ad219303b417f4c90) )

#ROM_REGION( 0x1800, "gfx1", 0 )
#ROM_LOAD( "k4_73.bin",    0x0000, 0x0800, CRC(fa6237e4) SHA1(0dfe2a0079324a78b462203fe93f7fb186a42122) )
#ROM_LOAD( "k2_71.bin",    0x0800, 0x0800, CRC(f85461de) SHA1(e2ed34e993cd657681124df5531e35afd7d8c34b) )
#ROM_LOAD( "k3_72.bin",    0x1000, 0x0800, CRC(fdfe88b6) SHA1(cdc37d90500f4ce813b6efee31139e6776aa2bff) )
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "j8_70co.bin",  0x0000, 0x0800, CRC(0c60e94c) SHA1(136df37b858a7fd399acc89e59917a068165d749) )
#ROM_LOAD( "j9_69co.bin",  0x0800, 0x0800, CRC(ba341c10) SHA1(c2c7350f87d5e47ac47cb19020681f0e7340e427) )
#ROM_REGION( 0x1000, "gfx3", 0 )
#ROM_LOAD( "k6_74o.bin",   0x0000, 0x0800, CRC(66719aea) SHA1(dd29f8d079868af3c7fd16dc8c383f1eba4543d2) )
#ROM_LOAD( "k7_75o.bin",   0x0800, 0x0800, CRC(5f8772e2) SHA1(16194a02bc7d5248dea7a80bf6d6d263ec8a7fd6) )
#ROM_REGION( 0x0800, "gfx4", 0 )
#ROM_LOAD( "d4_68.bin",    0x0000, 0x0800, CRC(f3649264) SHA1(5486a33fa1f7803e68d141992d6105206da1beba) )

#ROM_REGION( 0x1000, "user1", 0 )
#ROM_LOAD( "d2_67.bin",    0x0000, 0x1000, CRC(2b693e42) SHA1(e52b987cf929ddfc7916b05456b1114076956d12) )  /* search light map */
#ROM_REGION( 0x0300, "proms", 0 )
#ROM_LOAD( "dday.m11",     0x0000, 0x0100, CRC(aef6bbfc) SHA1(9e07729a4389221bc120af91d8275e1d05f3be7a) )  /* red component */
#ROM_LOAD( "dday.m8",      0x0100, 0x0100, CRC(ad3314b9) SHA1(d103f4f6103987ea85f0791ffc66a1cf9c711031) )  /* green component */
#ROM_LOAD( "dday.m3",      0x0200, 0x0100, CRC(e877ab82) SHA1(03e3905aee37f6743e7a4a87338f9504c832a55b) )  /* blue component */
#ROM_END






