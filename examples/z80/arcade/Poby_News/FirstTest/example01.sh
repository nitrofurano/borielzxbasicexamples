# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm news.zip
rm news/*
rmdir news
mkdir news

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x80000)) count=1 if=/dev/zero of=dummy512k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy512k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=news/virus.4
rm example01.bin

zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy512k.bin >> charmap01.bin
dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin

#dd ibs=1 count=$((0x40000)) skip=$((0x00000)) if=charmap01b.bin of=news/virus.2
##dd ibs=1 count=$((0x40000)) skip=$((0x40000)) if=charmap01b.bin of=news/virus.3
#dd ibs=1 count=$((0x40000)) skip=$((0x00000)) if=charmap01b.bin of=news/virus.3

xxd -p -c 2 -s 0 -l $((0x80000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > news/virus.2
xxd -p -c 2 -s 1 -l $((0x80000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > news/virus.3

rm charmap01b.bin

dd bs=$((0x40000)) count=1 if=/dev/urandom of=news/virus.1

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=news/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=news/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=news/news.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=news/82s126.3m

rm smsboot.bin dummy512k.bin

zip -r news news
rm news/*
rmdir news

mame -w -video soft -resolution0 512x512 -rp ./ news


