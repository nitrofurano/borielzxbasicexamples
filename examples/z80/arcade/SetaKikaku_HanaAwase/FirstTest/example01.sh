rm hanaawas.zip
rm example01.asm
rm hanaawas/*
rmdir hanaawas
mkdir hanaawas





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=hanaawas/1.1e
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=hanaawas/2.3e
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=hanaawas/3.4e
dd ibs=1 count=$((0x1000)) skip=$((0x6000)) if=example01.bin of=hanaawas/4.6e
#'- ROM_START( hanaawas )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "1.1e",       0x0000, 0x2000, CRC(618dc1e3) SHA1(31817f256512352db0d27322998d9dcf95a993cf) )
#'- ROM_LOAD( "2.3e",       0x2000, 0x1000, CRC(5091b67f) SHA1(5a66740b8829b9b4d3aea274f9ff36e0b9e8c151) )
#'- ROM_LOAD( "3.4e",       0x4000, 0x1000, CRC(dcb65067) SHA1(37964ff4016bd927b9f13b4358b831bb667f993b) )
#'- ROM_LOAD( "4.6e",       0x6000, 0x1000, CRC(24bee0dc) SHA1(a4237ad3611c923b563923462e79b0b3f66cc721) )





zxb.py library/b2r0f0_charmaphanaawase01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r0f0_charmaphanaawase01.bin

#1/1 colours 0..3 above
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r0f0_charmaphanaawase01.bin of=hanaawas/5.9a
#touch hanaawas/5.9a
#dd if=/dev/zero ibs=1k count=$((0x1000)) | tr "\000" "\377" > hanaawas/5.9a
#dd if=/dev/zero ibs=1k count=$((0x1000)) > hanaawas/5.9a

#0/1 colours 0..1 above
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r0f0_charmaphanaawase01.bin of=hanaawas/7.12a
#touch hanaawas/7.12a
#dd if=/dev/zero ibs=1k count=$((0x1000)) | tr "\000" "\377" > hanaawas/7.12a
#dd if=/dev/zero ibs=1k count=$((0x1000)) > hanaawas/7.12a


#1/1 colours 0..3 below
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r0f0_charmaphanaawase01.bin of=hanaawas/6.10a
#touch hanaawas/6.10a
#dd if=/dev/zero ibs=1k count=$((0x1000)) | tr "\000" "\377" > hanaawas/6.10a
#dd if=/dev/zero ibs=1k count=$((0x1000)) > hanaawas/6.10a

#0/1 colours 0..1 below
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r0f0_charmaphanaawase01.bin of=hanaawas/8.13a
#touch hanaawas/8.13a
#dd if=/dev/zero ibs=1k count=$((0x1000)) | tr "\000" "\377" > hanaawas/8.13a
#dd if=/dev/zero ibs=1k count=$((0x1000)) > hanaawas/8.13a

rm b2r0f0_charmaphanaawase01.bin

#'- ROM_REGION( 0x4000, "gfx1", 0 )
#'- ROM_LOAD( "5.9a",       0x0000, 0x1000, CRC(304ae219) SHA1(c1eac4973a6aec9fd8e848c206870667a8bb0922) )
#'- ROM_LOAD( "6.10a",      0x1000, 0x1000, CRC(765a4e5f) SHA1(b2f148c60cffb75d1a841be8b924a874bff22ce4) )
#'- ROM_LOAD( "7.12a",      0x2000, 0x1000, CRC(5245af2d) SHA1(a1262fa5828a52de28cc953ab465cbc719c56c32) )
#'- ROM_LOAD( "8.13a",      0x3000, 0x1000, CRC(3356ddce) SHA1(68818d0692fca548a49a74209bd0ef6f16484eba) )





#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=hanaawas/hanaawas.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=hanaawas/hanaawas.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=hanaawas/hanaawas.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=hanaawas/hanaawas.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=hanaawas/82s126.1m
dd bs=256 count=1 if=/dev/urandom of=hanaawas/13j.bpr
dd bs=256 count=1 if=/dev/urandom of=hanaawas/2a.bpr
dd bs=256 count=1 if=/dev/urandom of=hanaawas/6g.bpr

#'- ROM_REGION( 0x0220, "proms", 0 )
#'- ROM_LOAD( "13j.bpr",    0x0000, 0x0020, CRC(99300d85) SHA1(dd383db1f3c8c6d784121d32f20ffed3d83e2278) )  /* color PROM */
#'- ROM_LOAD( "2a.bpr",     0x0020, 0x0100, CRC(e26f21a2) SHA1(d0df06f833e0f97872d9d2ffeb7feef94aaaa02a) )  /* lookup table */
#'- ROM_LOAD( "6g.bpr",     0x0120, 0x0100, CRC(4d94fed5) SHA1(3ea8e6fb95d5677991dc90fe7435f91e5320bb16) )  /* I don't know what this is */
#'- ROM_END





rm smsboot.bin dummy32k.bin example01.bin

zip -r hanaawas hanaawas
rm hanaawas/*
rmdir hanaawas

mame -w -video soft -resolution0 512x512 -rp ./ hanaawas






#'- ROM_START( hanaawas )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "1.1e",       0x0000, 0x2000, CRC(618dc1e3) SHA1(31817f256512352db0d27322998d9dcf95a993cf) )
#'- ROM_LOAD( "2.3e",       0x2000, 0x1000, CRC(5091b67f) SHA1(5a66740b8829b9b4d3aea274f9ff36e0b9e8c151) )
#'- ROM_LOAD( "3.4e",       0x4000, 0x1000, CRC(dcb65067) SHA1(37964ff4016bd927b9f13b4358b831bb667f993b) )
#'- ROM_LOAD( "4.6e",       0x6000, 0x1000, CRC(24bee0dc) SHA1(a4237ad3611c923b563923462e79b0b3f66cc721) )

#'- ROM_REGION( 0x4000, "gfx1", 0 )
#'- ROM_LOAD( "5.9a",       0x0000, 0x1000, CRC(304ae219) SHA1(c1eac4973a6aec9fd8e848c206870667a8bb0922) )
#'- ROM_LOAD( "6.10a",      0x1000, 0x1000, CRC(765a4e5f) SHA1(b2f148c60cffb75d1a841be8b924a874bff22ce4) )
#'- ROM_LOAD( "7.12a",      0x2000, 0x1000, CRC(5245af2d) SHA1(a1262fa5828a52de28cc953ab465cbc719c56c32) )
#'- ROM_LOAD( "8.13a",      0x3000, 0x1000, CRC(3356ddce) SHA1(68818d0692fca548a49a74209bd0ef6f16484eba) )

#'- ROM_REGION( 0x0220, "proms", 0 )
#'- ROM_LOAD( "13j.bpr",    0x0000, 0x0020, CRC(99300d85) SHA1(dd383db1f3c8c6d784121d32f20ffed3d83e2278) )  /* color PROM */
#'- ROM_LOAD( "2a.bpr",     0x0020, 0x0100, CRC(e26f21a2) SHA1(d0df06f833e0f97872d9d2ffeb7feef94aaaa02a) )  /* lookup table */
#'- ROM_LOAD( "6g.bpr",     0x0120, 0x0100, CRC(4d94fed5) SHA1(3ea8e6fb95d5677991dc90fe7435f91e5320bb16) )  /* I don't know what this is */
#'- ROM_END





