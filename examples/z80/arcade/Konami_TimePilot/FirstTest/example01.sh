# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm timeplt.zip
rm timeplt/*
rmdir timeplt
mkdir timeplt

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=timeplt/tm1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=timeplt/tm2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=timeplt/tm3
rm example01.bin

#sound
dd bs=$((0x1000)) count=1 if=/dev/zero of=timeplt/tm7

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=timeplt/tm6
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=timeplt/tm4
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=charmap01b.bin of=timeplt/tm5
rm charmap01b.bin

#palette and lookup - improvement and fix needed
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=timeplt/timeplt.b4
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=timeplt/timeplt.b5
rm palette01.bin
zxb.py library/lookup01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=lookup01.bin of=timeplt/timeplt.e9
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=lookup01.bin of=timeplt/timeplt.e12
rm lookup01.bin
#dd bs=$((0x020)) count=1 if=/dev/urandom of=timeplt/timeplt.b4
#dd bs=$((0x020)) count=1 if=/dev/urandom of=timeplt/timeplt.b5
#dd bs=$((0x100)) count=1 if=/dev/urandom of=timeplt/timeplt.e9
#dd bs=$((0x100)) count=1 if=/dev/urandom of=timeplt/timeplt.e12


#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=timeplt/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=timeplt/timeplt.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=timeplt/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=timeplt/82s126.3m

rm smsboot.bin dummy64k.bin

zip -r timeplt timeplt
rm timeplt/*
rmdir timeplt

mame -w -video soft -sound none -resolution0 512x512 -rp ./ timeplt

