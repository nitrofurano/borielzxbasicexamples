# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm rampage.zip
rm rampage/*
rmdir rampage
mkdir rampage

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#maincpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=rampage/pro-0_3b_rev_3_8-27-86.3b
dd ibs=1 count=$((0x8000)) skip=$((0x8000)) if=example01.bin of=rampage/pro-1_5b_rev_3_8-27-86.5b
rm example01.bin

#sound
dd bs=$((0x8000)) count=1 if=/dev/zero of=rampage/u-7_rev.2_8-14-86.u7
dd bs=$((0x8000)) count=1 if=/dev/zero of=rampage/u-17_rev.2_8-14-86.u17
dd bs=$((0x8000)) count=1 if=/dev/zero of=rampage/u-8_rev.2_8-14-86.u8
dd bs=$((0x8000)) count=1 if=/dev/zero of=rampage/u-18_rev.2_8-14-86.u18

#gfx
zxb.py library/charmaprampage01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmaprampage01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmaprampage01.bin of=charmaprampage01b.bin
rm charmaprampage01.bin
#gfx1
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=charmaprampage01b.bin of=rampage/bg-0_u15_7-23-86.15a
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=charmaprampage01b.bin of=rampage/bg-1_u14_7-23-86.14b
#gfx2
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaprampage01b.bin of=rampage/fg-0_8e_6-30-86.8e
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaprampage01b.bin of=rampage/fg-1_6e_6-30-86.6e
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaprampage01b.bin of=rampage/fg-2_5e_6-30-86.5e
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmaprampage01b.bin of=rampage/fg-3_4e_6-30-86.4e
rm charmaprampage01b.bin


dd bs=$((0x0001)) count=1 if=/dev/zero of=rampage/e36a31axnaxqd.u15.bin



#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=rampage/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=rampage/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=rampage/rampage.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=rampage/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=rampage/82s126.3m


zip -r rampage rampage
rm rampage/*
rmdir rampage
rm smsboot.bin dummy64k.bin

#rm rampage.zip
#rm rampage/*
#rmdir rampage

mame -w -video soft -resolution0 512x512 -rp ./ rampage

