rm wiz.zip
rm example01.asm
rm wiz/*
rmdir wiz
mkdir wiz





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=wiz/ic07_01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=wiz/ic05_03.bin
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=wiz/ic06_02.bin

#'- ROM_START( wiz )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "ic07_01.bin",  0x0000, 0x4000, CRC(c05f2c78) SHA1(98b93234684a3a228552ef41a08512fef1befedd) )
#'- ROM_LOAD( "ic05_03.bin",  0x4000, 0x4000, CRC(7978d879) SHA1(866efdff3c111793d5a3cc2fa0b03a2b4e371c49) )
#'- ROM_LOAD( "ic06_02.bin",  0x8000, 0x4000, CRC(9c406ad2) SHA1(cd82c3dc622886b6ebb30ba565f3c34d5a4e229b) )







dd bs=$((0x2000)) count=1 if=/dev/zero of=wiz/ic57_10.bin

#'- ROM_REGION( 0x10000, "audiocpu", 0 )
#'- ROM_LOAD( "ic57_10.bin",  0x0000, 0x2000, CRC(8a7575bd) SHA1(5470c4c3a40139f45db7a9e260f40b5244f10123) )







zxb.py library/b3r3f0_charmapsegagigas01.zxi --org=$((0x0000))
cat dummy32k.bin >> b1r3f0_charmapspaceintruder01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r3f0_charmapsegagigas01.bin of=b3r3f0_charmapsegagigas01b.bin
xxd -p -c 3 -s 0 -l $((0x2000)) b3r3f0_charmapsegagigas01b.bin | cut -b 1-2 | xxd -r -p > wiz/ic12_04.bin
xxd -p -c 3 -s 1 -l $((0x2000)) b3r3f0_charmapsegagigas01b.bin | cut -b 1-2 | xxd -r -p > wiz/ic13_05.bin
xxd -p -c 3 -s 2 -l $((0x2000)) b3r3f0_charmapsegagigas01b.bin | cut -b 1-2 | xxd -r -p > wiz/ic14_06.bin
rm b3r3f0_charmapsegagigas01.bin b3r3f0_charmapsegagigas01b.bin

#'- ROM_REGION( 0x6000,  "gfx1", 0 )    /* sprites/chars */
#'- ROM_LOAD( "ic12_04.bin",  0x0000, 0x2000, CRC(8969acdd) SHA1(f37c4697232b4fb4171d6290c9407f740e7d1448) )
#'- ROM_LOAD( "ic13_05.bin",  0x2000, 0x2000, CRC(2868e6a5) SHA1(1b8ac71a6b901df845bab945bfcf11df47932990) )
#'- ROM_LOAD( "ic14_06.bin",  0x4000, 0x2000, CRC(b398e142) SHA1(1cafaf5cbfa96b410ae236a298473ff51122d9fc) )

zxb.py library/b1r3f0_charmapspaceintruder01.zxi --org=$((0x0000))
cat dummy32k.bin >> b1r3f0_charmapspaceintruder01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic12_04.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic13_05.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic14_06.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic03_07.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic02_08.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder01.bin of=wiz/ic01_09.bin
rm b1r3f0_charmapspaceintruder01.bin

#'- ROM_REGION( 0xc000,  "gfx2", 0 )    /* sprites/chars */
#'- ROM_LOAD( "ic03_07.bin",  0x0000, 0x2000, CRC(297c02fc) SHA1(8eee765a660e3ff1b6cdcdac0d068177098cc339) )
#'- ROM_CONTINUE(             0x6000, 0x2000  )
#'- ROM_LOAD( "ic02_08.bin",  0x2000, 0x2000, CRC(ede77d37) SHA1(01fe35fc3373b7513ea90e8262d66200629b89fe) )
#'- ROM_CONTINUE(             0x8000, 0x2000  )
#'- ROM_LOAD( "ic01_09.bin",  0x4000, 0x2000, CRC(4d86b041) SHA1(fe7f8c89ef16020f45a97ed875ddd7396a32665d) )
#'- ROM_CONTINUE(             0xa000, 0x2000  )




#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=wiz/wiz.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=wiz/wiz.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=wiz/wiz.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=wiz/wiz.11j
#rm clut01b.bin

dd bs=256 count=1 if=/dev/urandom of=wiz/ic23_3-1.bin
dd bs=256 count=1 if=/dev/urandom of=wiz/ic23_3-2.bin
dd bs=256 count=1 if=/dev/urandom of=wiz/ic23_3-3.bin
#'- ROM_REGION( 0x0300, "proms", 0 )
#'- ROM_LOAD( "ic23_3-1.bin", 0x0000, 0x0100, CRC(2dd52fb2) SHA1(61722aba7a370f4a97cafbd5df88ec7c6263c4ad) )    /* palette red component */
#'- ROM_LOAD( "ic23_3-2.bin", 0x0100, 0x0100, CRC(8c2880c9) SHA1(9b4c17f7fa5d6dc01d79c40cec9725ab97f514cb) )    /* palette green component */
#'- ROM_LOAD( "ic23_3-3.bin", 0x0200, 0x0100, CRC(a488d761) SHA1(6dade1dd16905b4751778d49f374936795c3fb6e) )    /* palette blue component */
#'- ROM_END






rm smsboot.bin dummy32k.bin example01.bin

zip -r wiz wiz
rm wiz/*
rmdir wiz

mame -w -video soft -resolution0 512x512 -rp ./ wiz





#'- ROM_START( wiz )
#'- ROM_REGION( 0x10000, "maincpu", 0 )
#'- ROM_LOAD( "ic07_01.bin",  0x0000, 0x4000, CRC(c05f2c78) SHA1(98b93234684a3a228552ef41a08512fef1befedd) )
#'- ROM_LOAD( "ic05_03.bin",  0x4000, 0x4000, CRC(7978d879) SHA1(866efdff3c111793d5a3cc2fa0b03a2b4e371c49) )
#'- ROM_LOAD( "ic06_02.bin",  0x8000, 0x4000, CRC(9c406ad2) SHA1(cd82c3dc622886b6ebb30ba565f3c34d5a4e229b) )

#'- ROM_REGION( 0x10000, "audiocpu", 0 )
#'- ROM_LOAD( "ic57_10.bin",  0x0000, 0x2000, CRC(8a7575bd) SHA1(5470c4c3a40139f45db7a9e260f40b5244f10123) )

#'- ROM_REGION( 0x6000,  "gfx1", 0 )    /* sprites/chars */
#'- ROM_LOAD( "ic12_04.bin",  0x0000, 0x2000, CRC(8969acdd) SHA1(f37c4697232b4fb4171d6290c9407f740e7d1448) )
#'- ROM_LOAD( "ic13_05.bin",  0x2000, 0x2000, CRC(2868e6a5) SHA1(1b8ac71a6b901df845bab945bfcf11df47932990) )
#'- ROM_LOAD( "ic14_06.bin",  0x4000, 0x2000, CRC(b398e142) SHA1(1cafaf5cbfa96b410ae236a298473ff51122d9fc) )
#'- ROM_REGION( 0xc000,  "gfx2", 0 )    /* sprites/chars */
#'- ROM_LOAD( "ic03_07.bin",  0x0000, 0x2000, CRC(297c02fc) SHA1(8eee765a660e3ff1b6cdcdac0d068177098cc339) )
#'- ROM_CONTINUE(             0x6000, 0x2000  )
#'- ROM_LOAD( "ic02_08.bin",  0x2000, 0x2000, CRC(ede77d37) SHA1(01fe35fc3373b7513ea90e8262d66200629b89fe) )
#'- ROM_CONTINUE(             0x8000, 0x2000  )
#'- ROM_LOAD( "ic01_09.bin",  0x4000, 0x2000, CRC(4d86b041) SHA1(fe7f8c89ef16020f45a97ed875ddd7396a32665d) )
#'- ROM_CONTINUE(             0xa000, 0x2000  )

#'- ROM_REGION( 0x0300, "proms", 0 )
#'- ROM_LOAD( "ic23_3-1.bin", 0x0000, 0x0100, CRC(2dd52fb2) SHA1(61722aba7a370f4a97cafbd5df88ec7c6263c4ad) )    /* palette red component */
#'- ROM_LOAD( "ic23_3-2.bin", 0x0100, 0x0100, CRC(8c2880c9) SHA1(9b4c17f7fa5d6dc01d79c40cec9725ab97f514cb) )    /* palette green component */
#'- ROM_LOAD( "ic23_3-3.bin", 0x0200, 0x0100, CRC(a488d761) SHA1(6dade1dd16905b4751778d49f374936795c3fb6e) )    /* palette blue component */
#'- ROM_END



