# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm crbaloon.zip
rm crbaloon/*
rmdir crbaloon
mkdir crbaloon




dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=crbaloon/cl01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=crbaloon/cl02.bin
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=crbaloon/cl03.bin
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=crbaloon/cl04.bin
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=crbaloon/cl05.bin
dd ibs=1 count=$((0x800)) skip=$((0x2800)) if=example01.bin of=crbaloon/cl06.bin
dd bs=$((0x2000)) count=1 if=/dev/zero of=crbaloon/ao4_05.ic3
rm dummy64k.bin example01.bin

#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cl01.bin",     0x0000, 0x0800, CRC(9d4eef0b) SHA1(a8dd814ac2612073982123c91fa62deaf5bee242) )
#ROM_LOAD( "cl02.bin",     0x0800, 0x0800, CRC(10f7a6f7) SHA1(e672a7dcdaae08b202cfc2e19033846ebb267e1b) )
#ROM_LOAD( "cl03.bin",     0x1000, 0x0800, CRC(44ed6030) SHA1(8bbf5d9e893710138be15e56682037f128c83527) )
#ROM_LOAD( "cl04.bin",     0x1800, 0x0800, CRC(62f66f6c) SHA1(d173b12d6b5e0719d7b25ff0cafebbe64ec6b134) )
#ROM_LOAD( "cl05.bin",     0x2000, 0x0800, CRC(c8f1e2be) SHA1(a4603ce0268fa987f7f780702b6ca04e28759674) )
#ROM_LOAD( "cl06.bin",     0x2800, 0x0800, CRC(7d465691) SHA1(f5dc7abe8db232f702419d126cee6607ea6a5168) )





#gfx
zxb.py library/b1r1f1_charmapbeaminvader01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=b1r1f1_charmapbeaminvader01b.bin
rm b1r1f1_charmapbeaminvader01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=crbaloon/cl07.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=crbaloon/cl08.bin
rm b1r1f1_charmapbeaminvader01b.bin

#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4_07.ic15 #-bitdepth1
#rm b1r1f1_charmapbeaminvader01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=crbaloon/crbaloon.5f
#rm spritemap01.bin

#ROM_REGION( 0x0800, "gfx1", 0 )
#ROM_LOAD( "cl07.bin",     0x0000, 0x0800, CRC(2c1fbea8) SHA1(41cf2aef74d56173057886512d989f6fa3682056) )
#ROM_REGION( 0x0800, "gfx2", 0 )
#ROM_LOAD( "cl08.bin",     0x0000, 0x0800, CRC(ba898659) SHA1(4291059b113ff91896f1f61a4c14956716edfe1e) )






#mcu
#dd bs=$((0x0800)) count=1 if=/dev/zero of=crbaloon/ao4_06.ic23
#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > crbaloon/ao4-12.ic95
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=crbaloon/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=crbaloon/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=crbaloon/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=crbaloon/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=crbaloon/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=crbaloon/82s126.4a
#rm clut01.bin




rm smsboot.bin

zip -r crbaloon crbaloon
rm crbaloon/*
rmdir crbaloon

mame -w -video soft -resolution0 512x512 -rp ./ crbaloon
#xmame.SDL -rp ./ crbaloon




#ROM_START( crbaloon )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "cl01.bin",     0x0000, 0x0800, CRC(9d4eef0b) SHA1(a8dd814ac2612073982123c91fa62deaf5bee242) )
#ROM_LOAD( "cl02.bin",     0x0800, 0x0800, CRC(10f7a6f7) SHA1(e672a7dcdaae08b202cfc2e19033846ebb267e1b) )
#ROM_LOAD( "cl03.bin",     0x1000, 0x0800, CRC(44ed6030) SHA1(8bbf5d9e893710138be15e56682037f128c83527) )
#ROM_LOAD( "cl04.bin",     0x1800, 0x0800, CRC(62f66f6c) SHA1(d173b12d6b5e0719d7b25ff0cafebbe64ec6b134) )
#ROM_LOAD( "cl05.bin",     0x2000, 0x0800, CRC(c8f1e2be) SHA1(a4603ce0268fa987f7f780702b6ca04e28759674) )
#ROM_LOAD( "cl06.bin",     0x2800, 0x0800, CRC(7d465691) SHA1(f5dc7abe8db232f702419d126cee6607ea6a5168) )

#ROM_REGION( 0x0800, "gfx1", 0 )
#ROM_LOAD( "cl07.bin",     0x0000, 0x0800, CRC(2c1fbea8) SHA1(41cf2aef74d56173057886512d989f6fa3682056) )
#ROM_REGION( 0x0800, "gfx2", 0 )
#ROM_LOAD( "cl08.bin",     0x0000, 0x0800, CRC(ba898659) SHA1(4291059b113ff91896f1f61a4c14956716edfe1e) )
#ROM_END





