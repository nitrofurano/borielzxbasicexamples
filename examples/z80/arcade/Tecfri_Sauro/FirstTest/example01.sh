rm sauro.zip
rm example01.asm
rm sauro/*
rmdir sauro
mkdir sauro


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=sauro/sauro-2.bin
dd ibs=1 count=$((0x8000)) skip=$((0x8000)) if=example01.bin of=sauro/sauro-1.bin



#audio cpu
dd bs=$((0x8000)) count=1 if=/dev/zero of=sauro/sauro-3.bin



#gfx
zxb.py library/charmaptecmo01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmaptecmo01.bin
#gfx1
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-6.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-7.bin
#gfx2
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-4.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-5.bin
#gfx3
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-8.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-9.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-10.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmaptecmo01.bin of=sauro/sauro-11.bin
rm charmaptecmo01.bin


#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=sauro/pp_e02.rom
#rm spritemap01.bin
#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=sauro/sauro.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=sauro/sauro.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=sauro/sauro.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=sauro/sauro.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=sauro/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=sauro/82s126.3m


#prom
#dd bs=$((0x0400))  count=1 if=/dev/urandom of=sauro/82s137-3.bin
#dd bs=$((0x0400))  count=1 if=/dev/urandom of=sauro/82s137-2.bin
#dd bs=$((0x0400))  count=1 if=/dev/urandom of=sauro/82s137-1.bin

zxb.py library/palettechr01.zxi --org=$((0x0000))
cat dummy64k.bin >> palettechr01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=palettechr01.bin of=palettechr01b.bin
xxd -p -c 3 -s 0 -l $((0x0C00)) palettechr01b.bin | cut -b 1-2 | xxd -r -p > sauro/82s137-3.bin
xxd -p -c 3 -s 1 -l $((0x0C00)) palettechr01b.bin | cut -b 1-2 | xxd -r -p > sauro/82s137-2.bin
xxd -p -c 3 -s 2 -l $((0x0C00)) palettechr01b.bin | cut -b 1-2 | xxd -r -p > sauro/82s137-1.bin
rm palettechr01.bin palettechr01b.bin



#speech
dd bs=$((0x0800)) count=1 if=/dev/zero of=sauro/sp0256-al2.bin


rm smsboot.bin dummy64k.bin example01.bin

zip -r sauro sauro
rm sauro/*
rmdir sauro

mame -w -video soft -resolution0 512x512 -rp ./ sauro


