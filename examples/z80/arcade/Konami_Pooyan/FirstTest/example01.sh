# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm pooyan/*
rmdir pooyan
mkdir pooyan

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=pooyan/1.4a
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=pooyan/2.5a
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=pooyan/3.6a
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=pooyan/4.7a
rm dummy64k.bin example01.bin

dd bs=$((0x1000)) count=1 if=/dev/zero of=pooyan/xx.7a
dd bs=$((0x1000)) count=1 if=/dev/zero of=pooyan/xx.8a

zxb.py library/charmaptimepilot_01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=charmaptimepilot_01.bin of=charmaptimepilot_01b.bin
rm charmaptimepilot_01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmaptimepilot_01b.bin of=pooyan/8.10g
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=charmaptimepilot_01b.bin of=pooyan/7.9g
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=charmaptimepilot_01b.bin of=pooyan/6.9a
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=charmaptimepilot_01b.bin of=pooyan/5.8a
rm charmaptimepilot_01b.bin


#palette and lookup - improvement and fix needed
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=pooyan/pooyan.pr1
rm palette01.bin
zxb.py library/lookup01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=lookup01.bin of=pooyan/pooyan.pr3
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=lookup01.bin of=pooyan/pooyan.pr2
rm lookup01.bin
#dd bs=$((0x20)) count=1 if=/dev/urandom of=pooyan/pooyan.pr1
#dd bs=$((0x100)) count=1 if=/dev/urandom of=pooyan/pooyan.pr3
#dd bs=$((0x100)) count=1 if=/dev/urandom of=pooyan/pooyan.pr2



zip -r pooyan pooyan
rm pooyan/*
rmdir pooyan
rm smsboot.bin

mame -w -video soft -resolution0 512x512 -sound none -rp ./ pooyan

