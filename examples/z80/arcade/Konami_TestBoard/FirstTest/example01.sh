# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm kontest.zip example01.asm
rm kontest/*
rmdir kontest
mkdir kontest

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=kontest/800b01.10d
rm dummy64k.bin example01.bin smsboot.bin

#prom
#dd bs=$((0x20)) count=1 if=/dev/urandom of=kontest/800a02.4f
zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=kontest/800a02.4f
rm palette01.bin

zip -r kontest kontest
rm kontest/*
rmdir kontest

mame -w -video soft -resolution0 512x512 -rp ./ kontest
#xmame.SDL -rp ./ kontest

