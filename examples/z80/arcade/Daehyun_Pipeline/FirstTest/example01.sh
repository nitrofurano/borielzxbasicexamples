rm pipeline.zip
rm example01.asm
rm pipeline/*
rmdir pipeline
mkdir pipeline


dd bs=$((0x100000)) count=1 if=/dev/zero of=dummy1m.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy1m.bin >> example01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.bin of=pipeline/rom1.u77

#- ROM_START( pipeline )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "rom1.u77", 0x00000, 0x08000, CRC(6e928290) SHA1(e2c8c35c04fd8ce3ddd6ecec04b0193a248e4362) )







#audio,mcu
dd bs=$((0x8000)) count=1 if=/dev/zero of=pipeline/rom2.u84
dd bs=$((0x1000)) count=1 if=/dev/zero of=pipeline/68705r3.u74

#- ROM_REGION( 0x10000, "audiocpu", 0 )
#- ROM_LOAD( "rom2.u84", 0x00000, 0x08000, CRC(e77c43b7) SHA1(8b04005bc448083a429ace3319fc7e168a61f2f9) )
#- ROM_REGION( 0x1000, "mcu", 0 )
#- ROM_LOAD( "68705r3.u74", 0x00000, 0x01000, CRC(9bef427e) SHA1(d8e9b144190ac1c837e379e4be69d1e258a6c666) )





zxb.py library/b3r0f0_charmapiremm62v01.zxi --org=$((0x0000))
cat dummy1m.bin >> b3r0f0_charmapiremm62v01.bin
dd ibs=1 count=$((0x100000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=b3r0f0_charmapiremm62v01b.bin
rm b3r0f0_charmapiremm62v01.bin
xxd -p -c 3 -s 0 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom3.u32
xxd -p -c 3 -s 1 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom4.u31
xxd -p -c 3 -s 2 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom5.u30
rm b3r0f0_charmapiremm62v01b.bin


zxb.py library/b8r0f0_charmapfunnybubblev01.zxi --org=$((0x0000))
cat dummy1m.bin >> b8r0f0_charmapfunnybubblev01.bin
dd ibs=1 count=$((0x100000)) skip=$((0x0010)) if=b8r0f0_charmapfunnybubblev01.bin of=b8r0f0_charmapfunnybubblev01b.bin
rm b8r0f0_charmapfunnybubblev01.bin
xxd -p -c 8 -s 0 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom13.u1
xxd -p -c 8 -s 1 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom12.u2
xxd -p -c 8 -s 2 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom11.u3
xxd -p -c 8 -s 3 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom10.u4
xxd -p -c 8 -s 4 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom9.u5
xxd -p -c 8 -s 5 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom8.u6
xxd -p -c 8 -s 6 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom7.u7
xxd -p -c 8 -s 7 -l $((0x100000)) b8r0f0_charmapfunnybubblev01b.bin | cut -b 1-2 | xxd -r -p > pipeline/rom6.u8
rm b8r0f0_charmapfunnybubblev01b.bin









zxb.py library/palette01.bas --org=$((0x0000))
cat dummy1m.bin >> palette01.bin
dd ibs=1 count=$((0x0400)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
dd ibs=1 count=$((0x0100)) skip=$((0x000)) if=palette01b.bin of=pipeline/82s129.u10
dd ibs=1 count=$((0x0100)) skip=$((0x100)) if=palette01b.bin of=pipeline/82s129.u9
dd ibs=1 count=$((0x0020)) skip=$((0x200)) if=palette01b.bin of=pipeline/82s123.u79
rm palette01b.bin

#cp library/_palette_original_/* ./pipeline/    #- replace it later

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=pipeline/pipeline.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy1m.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=pipeline/pipeline.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=pipeline/pipeline.11j
#rm clut01b.bin

#proms
#dd bs=$((0x100)) count=1 if=/dev/urandom of=pipeline/82s129.u10   #-r?
#dd bs=$((0x100)) count=1 if=/dev/zero of=pipeline/82s129.u9
#dd bs=$((0x020)) count=1 if=/dev/urandom of=pipeline/82s123.u79

#- ROM_REGION( 0x00220, "proms", 0 )
#- ROM_LOAD( "82s129.u10", 0x00000, 0x00100,CRC(e91a1f9e) SHA1(a293023ebe96a5438e89457a98d94beb6dad5418) )
#- ROM_LOAD( "82s129.u9",  0x00100, 0x00100,CRC(1cc09f6f) SHA1(35c857e0f3df0dcceec963459978e779e94f76f6) )
#- ROM_LOAD( "82s123.u79", 0x00200, 0x00020,CRC(6df3f972) SHA1(0096a7f7452b70cac6c0752cb62e24b643015b5c) )






rm smsboot.bin dummy1m.bin example01.bin

zip -r pipeline pipeline
rm pipeline/*
rmdir pipeline

mame -w -video soft -sound none -resolution0 512x512 -rp ./ pipeline













