rm champbas.zip
rm example01.asm
rm champbas/*
rmdir champbas
mkdir champbas



dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=champbas/champbb.1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=champbas/champbb.2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=champbas/champbb.3
#- ROM_START( champbas )
#ROM_START( champbas )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "champbb.1", 0x0000, 0x2000, CRC(218de21e) SHA1(7577fd04bdda4666c017f3b36e81ec23bcddd845) )
#ROM_LOAD( "champbb.2", 0x2000, 0x2000, CRC(5ddd872e) SHA1(68e21572e27707c991180b1bd0a6b31f7b64abf6) )
#ROM_LOAD( "champbb.3", 0x4000, 0x2000, CRC(f39a7046) SHA1(3097bffe84ac74ce9e6481028a0ebbe8b1d6eaf9) )



#sub
dd bs=$((0x2000)) count=1 if=/dev/zero of=champbas/champbb.6
dd bs=$((0x2000)) count=1 if=/dev/zero of=champbas/champbb.7
dd bs=$((0x2000)) count=1 if=/dev/zero of=champbas/champbb.8

#ROM_REGION( 0x10000, "sub", 0 )
#ROM_LOAD( "champbb.6", 0x0000, 0x2000, CRC(26ab3e16) SHA1(019b9d34233a6b7a53e204154b782ceb42915d2b) )
#ROM_LOAD( "champbb.7", 0x2000, 0x2000, CRC(7c01715f) SHA1(b15b2001b8c110f2599eee3aeed79f67686ebd7e) )
#ROM_LOAD( "champbb.8", 0x4000, 0x2000, CRC(3c911786) SHA1(eea0c467e213d237b5bb9d04b19a418d6090c2dc) )



#gfx
zxb.py library/b2r0f0s1_charmapchampbas01.zxi --org=$((0x0000))
cat dummy128k.bin >> b2r0f0s1_charmapchampbas01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0s1_charmapchampbas01.bin of=champbas/champbb.4
dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=b2r0f0s1_charmapchampbas01.bin of=champbas/champbb.5
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic18.14
rm b2r0f0s1_charmapchampbas01.bin

#ROM_REGION( 0x2000, "gfx1", 0 ) // chars + sprites: rearranged by DRIVER_INIT to leave only chars
#ROM_LOAD( "champbb.4", 0x0000, 0x2000, CRC(1930fb52) SHA1(cae0b2701c2b53b79e9df3a7496442ba3472e996) )
#ROM_REGION( 0x2000, "gfx2", 0 ) // chars + sprites: rearranged by DRIVER_INIT to leave only sprites
#ROM_LOAD( "champbb.5", 0x0000, 0x2000, CRC(a4cef5a1) SHA1(fa00ed0d075e00992a1ddce3c1327ed74770a735) )
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic09.05
#dd bs=$((0x10000)) count=1 if=/dev/urandom of=champbas/ic19.15
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic12.08
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic20.16
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic13.09
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic21.17
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic14.10
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic22.18
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic15.11
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic23.19
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic16.12
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic10.06
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic17.13
#dd bs=$((0x10000)) count=1 if=/dev/zero of=champbas/ic11.07








#prom

#palette
zxb.py library/palette01.bas --org=$((0x0000))
cat dummy128k.bin >> palette01.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=champbas/champbb.pr2
rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=champbas/champbas.3j

#lookup
#- clut01.zxi might need to be different files
zxb.py library/clut01.zxi --org=$((0x0000))
cat dummy128k.bin >> clut01.bin
dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=champbas/champbb.pr1
rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=champbas/champbas.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=champbas/champbas.11j
#rm clut01b.bin

#dd bs=$((0x0020)) count=1 if=/dev/urandom of=champbas/champbb.pr2
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=champbas/champbb.pr1

#ROM_REGION( 0x0120, "proms", 0 )
#ROM_LOAD( "champbb.pr2", 0x0000, 0x020, CRC(2585ffb0) SHA1(ce7f62f37955c2bbb4f82b139cc716978b084767) ) /* palette */
#ROM_LOAD( "champbb.pr1", 0x0020, 0x100, CRC(872dd450) SHA1(6c1e2c4a2fc072f4bf4996c731adb0b01b347506) ) /* look-up table */
#ROM_END







rm smsboot.bin dummy128k.bin example01.bin
zip -r champbas champbas
rm champbas/*
rmdir champbas
mame -w -video soft -resolution0 512x512 -rp ./ champbas



#ROM_START( champbas )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "champbb.1", 0x0000, 0x2000, CRC(218de21e) SHA1(7577fd04bdda4666c017f3b36e81ec23bcddd845) )
#ROM_LOAD( "champbb.2", 0x2000, 0x2000, CRC(5ddd872e) SHA1(68e21572e27707c991180b1bd0a6b31f7b64abf6) )
#ROM_LOAD( "champbb.3", 0x4000, 0x2000, CRC(f39a7046) SHA1(3097bffe84ac74ce9e6481028a0ebbe8b1d6eaf9) )

#ROM_REGION( 0x10000, "sub", 0 )
#ROM_LOAD( "champbb.6", 0x0000, 0x2000, CRC(26ab3e16) SHA1(019b9d34233a6b7a53e204154b782ceb42915d2b) )
#ROM_LOAD( "champbb.7", 0x2000, 0x2000, CRC(7c01715f) SHA1(b15b2001b8c110f2599eee3aeed79f67686ebd7e) )
#ROM_LOAD( "champbb.8", 0x4000, 0x2000, CRC(3c911786) SHA1(eea0c467e213d237b5bb9d04b19a418d6090c2dc) )

#ROM_REGION( 0x2000, "gfx1", 0 ) // chars + sprites: rearranged by DRIVER_INIT to leave only chars
#ROM_LOAD( "champbb.4", 0x0000, 0x2000, CRC(1930fb52) SHA1(cae0b2701c2b53b79e9df3a7496442ba3472e996) )
#ROM_REGION( 0x2000, "gfx2", 0 ) // chars + sprites: rearranged by DRIVER_INIT to leave only sprites
#ROM_LOAD( "champbb.5", 0x0000, 0x2000, CRC(a4cef5a1) SHA1(fa00ed0d075e00992a1ddce3c1327ed74770a735) )

#ROM_REGION( 0x0120, "proms", 0 )
#ROM_LOAD( "champbb.pr2", 0x0000, 0x020, CRC(2585ffb0) SHA1(ce7f62f37955c2bbb4f82b139cc716978b084767) ) /* palette */
#ROM_LOAD( "champbb.pr1", 0x0020, 0x100, CRC(872dd450) SHA1(6c1e2c4a2fc072f4bf4996c731adb0b01b347506) ) /* look-up table */
#ROM_END




