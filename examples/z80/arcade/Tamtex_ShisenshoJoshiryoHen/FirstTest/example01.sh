rm shisen.zip
rm example01.asm
rm shisen/*
rmdir shisen
mkdir shisen



dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x20000)) skip=$((0x0000)) if=example01.bin of=shisen/a-27-a.rom

#- ROM_START( shisen )
#- ROM_REGION( 0x30000, "maincpu", 0 ) /* 64k+128k for main CPU */
#- ROM_LOAD( "a-27-a.rom",   0x00000, 0x20000, CRC(de2ecf05) SHA1(7256c5587f92db10a52c43001e3236f3be3df5df) )
#- ROM_RELOAD(               0x10000, 0x20000 )







dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic01.01

#- ROM_REGION( 0x10000, "soundcpu", 0 )
#- ROM_LOAD( "ic01.01",      0x00000, 0x10000, CRC(51b0a26c) SHA1(af2482cfe8d395848c8e1bf07bf1049ffc6ee69b) )









zxb.py library/b4r0f0s0_charmapshisen01.zxi --org=$((0x0000))
cat dummy128k.bin >> b4r0f0s0_charmapshisen01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b4r0f0s0_charmapshisen01.bin of=b4r0f0s0_charmapshisen01b.bin
xxd -p -c 2 -s 0 -l $((0x20000)) b4r0f0s0_charmapshisen01b.bin | cut -b 1-2 | xxd -r -p > shisen/ic08.04
xxd -p -c 2 -s 1 -l $((0x20000)) b4r0f0s0_charmapshisen01b.bin | cut -b 1-2 | xxd -r -p > shisen/ic18.14




#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=shisen/ic08.04
#dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic18.14


dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic09.05
dd bs=$((0x10000)) count=1 if=/dev/urandom of=shisen/ic19.15

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic12.08
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic20.16

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic13.09
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic21.17

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic14.10
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic22.18

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic15.11
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic23.19

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic16.12
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic10.06

dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic17.13
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic11.07


rm b4r0f0s0_charmapshisen01.bin b4r0f0s0_charmapshisen01b.bin




#- ROM_REGION( 0x100000, "gfx1", 0 )
#- ROM_LOAD( "ic08.04",      0x00000, 0x10000, CRC(1c0e221c) SHA1(87561f7dabf25309be784e797ac237aa3956ea1c) ) # $0000?
#- ROM_LOAD( "ic09.05",      0x10000, 0x10000, CRC(8a7d8284) SHA1(56b5d352b506c5bfab24102b11c877dd28c8ad36) ) # $1000?
#- ROM_LOAD( "ic12.08",      0x20000, 0x10000, CRC(48e1d043) SHA1(4fbd409aff593c0b27fc58c218a470adf48ee0b7) )
#- ROM_LOAD( "ic13.09",      0x30000, 0x10000, CRC(3feff3f2) SHA1(2e87e4fb158379486de5d13feff5bf1965690e14) )
#- ROM_LOAD( "ic14.10",      0x40000, 0x10000, CRC(b76a517d) SHA1(6dcab31ecc127c2fdc6912802cddfa62161d83a2) )
#- ROM_LOAD( "ic15.11",      0x50000, 0x10000, CRC(8ff5ee7a) SHA1(c8f4d374a43fcc818378c4e73af2c03238a93ad0) )
#- ROM_LOAD( "ic16.12",      0x60000, 0x10000, CRC(64e5d837) SHA1(030f9704dfdb06cd3ac583b857ce9239e1409f60) )
#- ROM_LOAD( "ic17.13",      0x70000, 0x10000, CRC(02c1b2c4) SHA1(6e4fe801189766559859eb0d628f3ae65c05ad16) )
#- ROM_LOAD( "ic18.14",      0x80000, 0x10000, CRC(f5a8370e) SHA1(b270af116ee15b5de048fc218215bf91a2ce6b46) ) # $0000?
#- ROM_LOAD( "ic19.15",      0x90000, 0x10000, CRC(7a9b7671) SHA1(ddb9b412b494f2f259c56a163e5511353cf4ebb5) ) # $1000
#- ROM_LOAD( "ic20.16",      0xa0000, 0x10000, CRC(7fb396ad) SHA1(b245bb9a6a4c1ec518906c59c3a3da5e412369ab) )
#- ROM_LOAD( "ic21.17",      0xb0000, 0x10000, CRC(fb83c652) SHA1(3d124e5c751732e8055bbb7b6853b6e64435a85d) )
#- ROM_LOAD( "ic22.18",      0xc0000, 0x10000, CRC(d8b689e9) SHA1(14db7ba2246f12db9b6b5b4dcb4a648e4a65ace4) )
#- ROM_LOAD( "ic23.19",      0xd0000, 0x10000, CRC(e6611947) SHA1(9267dad097b318174706d51fb94d613208b04f60) )
#- ROM_LOAD( "ic10.06",      0xe0000, 0x10000, CRC(473b349a) SHA1(9f5d08e07c8175bc7ec3854499177af2c398bd76) )
#- ROM_LOAD( "ic11.07",      0xf0000, 0x10000, CRC(d9a60285) SHA1(f8ef211e022e9c8ea25f6d8fb16266867656a591) )







#zxb.py library/charmap01.zxi --org=$((0x0000))
#cat dummy128k.bin >> charmap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=charmap01.bin of=shisen/pp_e02.rom
#rm charmap01.bin













#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy128k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=shisen/shisen.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=shisen/shisen.3j


#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy128k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=shisen/shisen.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=shisen/shisen.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=shisen/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=shisen/82s126.3m



#ssamples
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic02.02
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic03.03
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic04.04
dd bs=$((0x10000)) count=1 if=/dev/zero of=shisen/ic05.05




#- ROM_REGION( 0x40000, "samples", 0 ) /* samples */
#- ROM_LOAD( "ic02.02",      0x00000, 0x10000, CRC(92f0093d) SHA1(530b924aa991283045577d03524dfc7eacf1be49) )
#- ROM_LOAD( "ic03.03",      0x10000, 0x10000, CRC(116a049c) SHA1(656c0d1d7f945c5f5637892721a58421b682fd01) )
#- ROM_LOAD( "ic04.04",      0x20000, 0x10000, CRC(6840692b) SHA1(f6f7b063ecf7206e172843515be38376f8845b42) )
#- ROM_LOAD( "ic05.05",      0x30000, 0x10000, CRC(92ffe22a) SHA1(19dcaf6e25bb7498d4ab19fa0a63f3326b9bff8f) )







rm smsboot.bin dummy128k.bin example01.bin

zip -r shisen shisen
rm shisen/*
rmdir shisen

mame -w -video soft -resolution0 512x512 -rp ./ shisen














