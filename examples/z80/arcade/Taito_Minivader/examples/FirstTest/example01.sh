# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm minivadr.zip example01.asm
rm minivadr/*
rmdir minivadr
mkdir minivadr

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=16384 count=1 if=/dev/zero of=dummy16k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy16k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=minivadr/d26-01.bin
rm dummy16k.bin example01.bin smsboot.bin

zip -r minivadr minivadr
rm minivadr/*
rmdir minivadr

mame -w -video soft -resolution0 512x512 -rp ./ minivadr
#xmame.SDL -rp ./ minivadr

