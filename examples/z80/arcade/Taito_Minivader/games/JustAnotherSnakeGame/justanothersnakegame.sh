# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm minivadr.zip justanothersnakegame.asm
rm minivadr/*
rmdir minivadr
mkdir minivadr

#- zxb.py justanothersnakegame.bas --asm --org=$((0x0000))
#- zxb.py justanothersnakegame.bas --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py justanothersnakegame.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py justanothersnakegame.bas --heap-size=128 --org=$((0x0069))
dd bs=16384 count=1 if=/dev/zero of=dummy16k.bin

cat justanothersnakegame.bin >> smsboot2.bin
rm justanothersnakegame.bin
mv smsboot2.bin justanothersnakegame.bin

cat dummy16k.bin >> justanothersnakegame.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=justanothersnakegame.bin of=minivadr/d26-01.bin
rm dummy16k.bin justanothersnakegame.bin smsboot.bin

zip -r minivadr minivadr
rm minivadr/*
rmdir minivadr

mame -w -video soft -resolution0 512x512 -rp ./ minivadr
#xmame.SDL -rp ./ minivadr

