asm
  ld sp,$BFFF
  end asm

#include "./library/smsrnd.bas"
#include "./library/charmap01.bas"
#include "./library/pacmanfillram.bas"
#include "./library/pacmandelay.bas"
#include "./library/minivadersldirxor32b.bas"

dim eee as uinteger at $A010
dim seed as uinteger at $A012
dim delay as uinteger at $A014
dim eex as uinteger at $A016
dim eey as uinteger at $A018
dim debug as ubyte at $A01A
dim ejst as uinteger at $A01C
dim exps as integer at $A01E
dim eyps as integer at $A020
dim eqq as uinteger at $A022
dim dirct as uinteger at $A024
dim jprsd as ubyte at $A026:jprs=0
dim snsize as uinteger at $A028
dim cxps as uinteger at $A02A
dim cyps as uinteger at $A02C
dim vpk1 as ubyte at $A02E

debug=0

sub putchar(txpos as uinteger,typos as uinteger,tch as uinteger, tadr as uinteger,txor as ubyte)
  minivadersldirxor32b($A200+(txpos band 31)+((typos mod 28)*256), tadr+(tch*8),8,txor)
  end sub

sub writetext(txpos2 as uinteger,typos2 as uinteger,ttxt as uinteger, tleng as uinteger,tadr2 as uinteger, txor2 as uinteger)
  dim trt2 as uinteger at $A002
  for trt2=0 to tleng-1
    putchar(txpos2+trt2,typos2,peek(ttxt+trt2),tadr2,txor2)
    next
  end sub

oops:

for eey=0 to 111
  pacmanfillram($A200+(eey*64),$AA,32)
  pacmanfillram($A220+(eey*64),$55,32)
  next

writetext(2,21,@text01a,@text01b-@text01a,@charmap01-256,$FF)
writetext(2,22,@text01b,@text01c-@text01b,@charmap01-256,$FF)
writetext(2,24,@text01c,@text01d-@text01c,@charmap01-256,$FF)

while peek($E008) band 4=4:end while

for eey=0 to 111
  pacmanfillram($A200+(eey*64),$AA,32)
  pacmanfillram($A220+(eey*64),$55,32)
  next

exps=16:eyps=22:dirct=0:eee=0:snsize=10
seed=smsrnd(seed):cxps=seed band 31
seed=smsrnd(seed):cyps=seed mod 28
for eey=$BE00 to $BEEF step 2
  poke eey,exps
  poke eey+1,eyps
  next

do
  ejst=peek($E008) bxor 255
  eee=eee+1

  if (ejst band 3)=0 then:jprsd=0:end if

  if jprsd=0 then:
    if (ejst band 1)<>0 then:dirct=(dirct+3)band 3:jprsd=1:end if
    if (ejst band 2)<>0 then:dirct=(dirct+1)band 3:jprsd=1:end if
    end if

  if (ejst band 4)<>0 then:goto oops:end if

  if dirct=0 then:eyps=eyps-1:end if
  if dirct=1 then:exps=exps+1:end if
  if dirct=2 then:eyps=eyps+1:end if
  if dirct=3 then:exps=exps-1:end if

  if eyps<0 then:eyps=27:end if
  if exps<0 then:exps=31:end if
  if eyps>27 then:eyps=0:end if
  if exps>31 then:exps=0:end if

  vpk1=peek($A200+(exps band 31)+((eyps mod 28)*256))

  if debug<> 0 then
    putchar(0,27,48+ ( int(vpk1/100) mod 10)  ,@charmap01-256,$FF)
    putchar(1,27,48+ ( int(vpk1/10) mod 10)  ,@charmap01-256,$FF)
    putchar(2,27,48+ (vpk1 mod 10)  ,@charmap01-256,$FF)
    end if

  if ((vpk1<>170) and (vpk1<>255)) and ((cxps<>exps) or (cyps<>eyps)) then:
    pacmandelay(50000)
    pacmandelay(50000)
    goto oops
    end if

  for eey=$BE00 to $BE7F step 2
    poke eey,peek(eey+2)
    poke eey+1,peek(eey+3)
    next
  poke $BE7E,exps
  poke $BE7F,eyps

  eqq=code("j")+(eee band 7)
  putchar(exps,eyps,eqq,@charmap01-256,$FF)
  putchar(peek($BE7E-(snsize*2)),peek($BE7F-(snsize*2)),32,@charmap01-256,$FF)
  putchar(cxps,cyps,eqq,@charmap01-256,$FF)

  if (cxps=exps) and (cyps=eyps) then:
    seed=smsrnd(seed):cxps=seed band 31
    seed=smsrnd(seed):cyps=seed mod 28
    snsize=snsize+1
    end if

  pacmandelay(10000)
  loop

do:loop

'--------------------
text01:
text01a:
asm
  defb " JUST ANOTHER SNAKE GAME "
  end asm
text01b:
asm
  defb " PAULO SILVA, OCT'14, JAN'17 "
  end asm
text01c:
asm
  defb " PUSH FIRE BUTTON "
  end asm
text01d:
asm
  defb " NO COINS NEEDED!!!! :D "
  end asm

'-------------------------------------------------------------------------------

