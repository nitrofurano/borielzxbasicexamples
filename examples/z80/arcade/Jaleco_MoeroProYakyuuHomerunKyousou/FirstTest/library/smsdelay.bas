sub smsdelay(ttm as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)

    smsdelayloop:
    dec bc
    ld a,b
    or c
    jp nz,smsdelayloop

    end asm
  end sub


