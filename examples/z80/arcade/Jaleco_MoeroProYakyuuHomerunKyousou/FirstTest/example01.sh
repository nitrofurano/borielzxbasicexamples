rm example01.asm
rm homerun.zip
rm homerun/*
rmdir homerun
mkdir homerun
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=homerun/homerun.ic43
rm example01.bin

#ROM_START( homerun )
#	ROM_REGION( 0x30000, "maincpu", 0 )
#	ROM_LOAD( "homerun.ic43",   0x00000, 0x04000, CRC(e759e476) SHA1(ad4f356ff26209033320a3e6353e4d4d9beb59c1) )
#	ROM_CONTINUE(               0x10000, 0x1c000)







#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
#gfx1
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=charmap01b.bin of=homerun/homerun.ic60
#gfx2
dd ibs=1 count=$((0x20000)) skip=$((0x0000)) if=charmap01b.bin of=homerun/homerun.ic120
rm charmap01.bin charmap01b.bin

#	ROM_REGION( 0x10000, "gfx1", 0 )
#	ROM_LOAD( "homerun.ic60",   0x00000, 0x10000, CRC(69a720d1) SHA1(0f0a4877578f358e9e829ece8c31e23f01adcf83) )
#	ROM_REGION( 0x20000, "gfx2", 0 )
#	ROM_LOAD( "homerun.ic120",  0x00000, 0x20000, CRC(52f0709b) SHA1(19e675bcccadb774f60ec5929fc1fb5cf0d3f617) )





#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=homerun/hgb3.l6
#rm palette01b.bin

#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=homerun/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=homerun/hgb1.h7  #?sp
#rm clut01b.bin

#proms
dd bs=$((0x8000)) count=1 if=/dev/urandom of=homerun/d7756c.ic98



#	ROM_REGION( 0x08000, "d7756", ROMREGION_ERASE00 )
#	ROM_LOAD( "d7756c.ic98",    0x00000, 0x08000, NO_DUMP ) /* D7756C built-in rom - very likely the same rom as [Moero!! Pro Yakyuu (Black/Red)] on Famicom, and [Moero!! Nettou Yakyuu '88] on MSX2 */





zip -r homerun homerun
rm homerun/*
rmdir homerun
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ homerun






#ROM_START( homerun )
#	ROM_REGION( 0x30000, "maincpu", 0 )
#	ROM_LOAD( "homerun.ic43",   0x00000, 0x04000, CRC(e759e476) SHA1(ad4f356ff26209033320a3e6353e4d4d9beb59c1) )
#	ROM_CONTINUE(               0x10000, 0x1c000)

#	ROM_REGION( 0x10000, "gfx1", 0 )
#	ROM_LOAD( "homerun.ic60",   0x00000, 0x10000, CRC(69a720d1) SHA1(0f0a4877578f358e9e829ece8c31e23f01adcf83) )
#	ROM_REGION( 0x20000, "gfx2", 0 )
#	ROM_LOAD( "homerun.ic120",  0x00000, 0x20000, CRC(52f0709b) SHA1(19e675bcccadb774f60ec5929fc1fb5cf0d3f617) )

#	ROM_REGION( 0x08000, "d7756", ROMREGION_ERASE00 )
#	ROM_LOAD( "d7756c.ic98",    0x00000, 0x08000, NO_DUMP ) /* D7756C built-in rom - very likely the same rom as [Moero!! Pro Yakyuu (Black/Red)] on Famicom, and [Moero!! Nettou Yakyuu '88] on MSX2 */
#ROM_END



