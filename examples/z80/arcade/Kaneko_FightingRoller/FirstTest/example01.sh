rm example01.asm
rm fightrol.zip
rm fightrol/*
rmdir fightrol
mkdir fightrol
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=fightrol/4.8k
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=fightrol/5.8h
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=fightrol/6.8f
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=fightrol/7.8d
rm example01.bin

#ROM_START( fightrol )
#ROM_REGION( 0x10000, "maincpu",0 )
#ROM_LOAD( "4.8k", 0x0000, 0x2000, CRC(efa2f430) SHA1(6aeb2a41e4fba97a0ac1b24fe5437e25b6c6b6c5) )
#ROM_LOAD( "5.8h", 0x2000, 0x2000, CRC(2497d9f6) SHA1(4f4cfed47efc603bf057dd24b761beecf5b929f4) )
#ROM_LOAD( "6.8f", 0x4000, 0x2000, CRC(f39727b9) SHA1(08a1300172b4100cb80c9a5d8942408255d8e330) )
#ROM_LOAD( "7.8d", 0x6000, 0x2000, CRC(ee65b728) SHA1(871918d505ad8bab60c55bbb95fe37556a204dc9) )






#sound
dd bs=$((0x1000)) count=1 if=/dev/zero of=fightrol/8.6f

#ROM_REGION( 0x10000,"audiocpu",0 )
#ROM_LOAD( "8.6f", 0x0000, 0x1000, CRC(6ec3c545) SHA1(1a2477b9e1563734195b0743f5dbbb005e06022e) )
#ROM_END







#gfx
zxb.py library/b3r3f1_charmapfightingroller01.zxi --org=$((0x0000))
cat dummy64k.bin >> b3r3f1_charmapfightingroller01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b3r3f1_charmapfightingroller01.bin of=b3r3f1_charmapfightingroller01b.bin

xxd -p -c 3 -s 0 -l 25536 b3r3f1_charmapfightingroller01b.bin | cut -b 1-2 | xxd -r -p > fightrol/3.7m
xxd -p -c 3 -s 1 -l 25536 b3r3f1_charmapfightingroller01b.bin | cut -b 1-2 | xxd -r -p > fightrol/2.8m
xxd -p -c 3 -s 2 -l 25536 b3r3f1_charmapfightingroller01b.bin | cut -b 1-2 | xxd -r -p > fightrol/1.9m

rm b3r3f1_charmapfightingroller01.bin b3r3f1_charmapfightingroller01b.bin

#gfx
zxb.py library/b1r3f1_charmapredclash01.zxi --org=$((0x0000))
cat dummy64k.bin >> b1r3f1_charmapredclash01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b1r3f1_charmapredclash01.bin of=b1r3f1_charmapredclash01b.bin

#gfx1
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/3.7m
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/2.8m
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/1.9m
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/6.20k
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/7.18k
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/5.20f
#gfx3
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/8.17m
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/9.17r
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/10.17t
#gfx4
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/11.18m
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/12.18r
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/13.18t
#gfx5
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/14.19m
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/15.19r
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r3f1_charmapredclash01b.bin of=fightrol/16.19u

rm b1r3f1_charmapredclash01.bin b1r3f1_charmapredclash01b.bin

#ROM_REGION( 0x6000,"gfx1",0 )   /* characters */
#ROM_LOAD( "3.7m", 0x0000, 0x2000, CRC(ca4f353c) SHA1(754838c6ad6886052a018966d55f40a7ed4b684d) )
#ROM_LOAD( "2.8m", 0x2000, 0x2000, CRC(93786171) SHA1(3928aad8bc43adeaad5e53c1d4e9df64f1d23704) )
#ROM_LOAD( "1.9m", 0x4000, 0x2000, CRC(dc072be1) SHA1(94d379a4c5a53050a18cd572cc82edb337182f3b) )
#ROM_REGION( 0x6000, "gfx2",0 )  /* road graphics */
#ROM_LOAD ( "6.20k", 0x0000, 0x2000, CRC(003d7515) SHA1(d8d84d690478cad16101f2ef9a1ae1ae74d01c88) )
#ROM_LOAD ( "7.18k", 0x2000, 0x2000, CRC(27843afa) SHA1(81d3031a2c06086461110696a0ee11d32992ecac) )
#ROM_LOAD ( "5.20f", 0x4000, 0x2000, CRC(51dd0108) SHA1(138c0aba6c952204e794216193def17b390c4ba2) )
#ROM_REGION( 0x6000, "gfx3",0 )  /* sprite bank 0*/
#ROM_LOAD ( "8.17m",  0x0000, 0x2000, CRC(08ad783e) SHA1(fea91e41916cfc7b29c5f9a578e2c82a54f66829) )
#ROM_LOAD ( "9.17r",  0x2000, 0x2000, CRC(69b23461) SHA1(73eca5e721425f37df311454bd5b4e632b096eba) )
#ROM_LOAD ( "10.17t", 0x4000, 0x2000, CRC(ba6ccd8c) SHA1(29a13e3161aba4db080434685869f8b79ad7997c) )
#ROM_REGION( 0x6000, "gfx4",0 )  /* sprite bank 1*/
#ROM_LOAD ( "11.18m", 0x0000, 0x2000, CRC(06a5d849) SHA1(b9f604edf4fdc053b738041493aef91dd730fe6b) )
#ROM_LOAD ( "12.18r", 0x2000, 0x2000, CRC(569815ef) SHA1(db261799892f60b2274b73fb25cde58219bb44db) )
#ROM_LOAD ( "13.18t", 0x4000, 0x2000, CRC(4f8af872) SHA1(6c07ff0733b8d8440309c9ae0db0876587b740a6) )
#ROM_REGION( 0x6000, "gfx5",0 )  /* sprite bank 2*/
#ROM_LOAD ( "14.19m", 0x0000, 0x2000, CRC(93f3c649) SHA1(38d6bb4b6108a67b135ae1a145532f4a0c2568b8) )
#ROM_LOAD ( "15.19r", 0x2000, 0x2000, CRC(5b3d87e4) SHA1(e47f7b62bf7101afba8d5e181f4bd8f8eb6eeb08) )
#ROM_LOAD ( "16.19u", 0x4000, 0x2000, CRC(a2c24b64) SHA1(e76558785ea337ab902fb6f94dc1a4bdfcd6335e) )






#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=fightrol/hgb3.l6
#rm palette01b.bin

#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=fightrol/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=fightrol/hgb1.h7  #?sp
#rm clut01b.bin

#proms
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fightrol/tbp24s10.7u
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fightrol/tbp24s10.7t
#dd bs=$((0x100)) count=1 if=/dev/urandom of=fightrol/tbp24s10.6t

zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=2048 skip=$((0x0010)) if=palette01.bin of=palette01b.bin
xxd -p -c 3 -s 0 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > fightrol/tbp24s10.7u
xxd -p -c 3 -s 1 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > fightrol/tbp24s10.7t
xxd -p -c 3 -s 2 -l 768 palette01b.bin | cut -b 1-2 | xxd -r -p > fightrol/tbp24s10.6t
rm palette01.bin palette01b.bin






#ROM_REGION( 0x300, "proms",0 )  /* colour */
#ROM_LOAD("tbp24s10.7u", 0x0000, 0x0100, CRC(9d199d33) SHA1(b8982f7da2b85f10d117177e4e73cbb486931cf5) )
#ROM_LOAD("tbp24s10.7t", 0x0100, 0x0100, CRC(c0426582) SHA1(8e3e4d1e76243cce272aa099d2d6ad4fa6c99f7c) )
#ROM_LOAD("tbp24s10.6t", 0x0200, 0x0100, CRC(c096e05c) SHA1(cb5b509e6124453f381a683ba446f8f4493d4610) )




dd bs=$((0x2000)) count=1 if=/dev/zero of=fightrol/1.17a
dd bs=$((0x2000)) count=1 if=/dev/zero of=fightrol/3.18b
dd bs=$((0x2000)) count=1 if=/dev/zero of=fightrol/2.17d
dd bs=$((0x2000)) count=1 if=/dev/zero of=fightrol/4.18d

#ROM_REGION( 0x8000, "user1",0 ) /* road layout */
#ROM_LOAD ( "1.17a", 0x0000, 0x2000, CRC(f0fa72fc) SHA1(b73e794df635630f29a79adfe2951dc8f1d17e20) )
#ROM_LOAD ( "3.18b", 0x2000, 0x2000, CRC(954268f7) SHA1(07057296e0281f90b18dfe4223aad18bff7cfa6e) )
#ROM_LOAD ( "2.17d", 0x4000, 0x2000, CRC(2e38bb0e) SHA1(684f14a06ff957e40780be21c0ad5f10088a55ed) )
#ROM_LOAD ( "4.18d", 0x6000, 0x2000, CRC(3d9e16ab) SHA1(e99628ffc54e3ff4818313a287ca111617120910) )






zip -r fightrol fightrol
rm fightrol/*
rmdir fightrol
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ fightrol







#ROM_START( fightrol )
#ROM_REGION( 0x10000, "maincpu",0 )
#ROM_LOAD( "4.8k", 0x0000, 0x2000, CRC(efa2f430) SHA1(6aeb2a41e4fba97a0ac1b24fe5437e25b6c6b6c5) )
#ROM_LOAD( "5.8h", 0x2000, 0x2000, CRC(2497d9f6) SHA1(4f4cfed47efc603bf057dd24b761beecf5b929f4) )
#ROM_LOAD( "6.8f", 0x4000, 0x2000, CRC(f39727b9) SHA1(08a1300172b4100cb80c9a5d8942408255d8e330) )
#ROM_LOAD( "7.8d", 0x6000, 0x2000, CRC(ee65b728) SHA1(871918d505ad8bab60c55bbb95fe37556a204dc9) )

#ROM_REGION( 0x6000,"gfx1",0 )   /* characters */
#ROM_LOAD( "3.7m", 0x0000, 0x2000, CRC(ca4f353c) SHA1(754838c6ad6886052a018966d55f40a7ed4b684d) )
#ROM_LOAD( "2.8m", 0x2000, 0x2000, CRC(93786171) SHA1(3928aad8bc43adeaad5e53c1d4e9df64f1d23704) )
#ROM_LOAD( "1.9m", 0x4000, 0x2000, CRC(dc072be1) SHA1(94d379a4c5a53050a18cd572cc82edb337182f3b) )
#ROM_REGION( 0x6000, "gfx2",0 )  /* road graphics */
#ROM_LOAD ( "6.20k", 0x0000, 0x2000, CRC(003d7515) SHA1(d8d84d690478cad16101f2ef9a1ae1ae74d01c88) )
#ROM_LOAD ( "7.18k", 0x2000, 0x2000, CRC(27843afa) SHA1(81d3031a2c06086461110696a0ee11d32992ecac) )
#ROM_LOAD ( "5.20f", 0x4000, 0x2000, CRC(51dd0108) SHA1(138c0aba6c952204e794216193def17b390c4ba2) )
#ROM_REGION( 0x6000, "gfx3",0 )  /* sprite bank 0*/
#ROM_LOAD ( "8.17m",  0x0000, 0x2000, CRC(08ad783e) SHA1(fea91e41916cfc7b29c5f9a578e2c82a54f66829) )
#ROM_LOAD ( "9.17r",  0x2000, 0x2000, CRC(69b23461) SHA1(73eca5e721425f37df311454bd5b4e632b096eba) )
#ROM_LOAD ( "10.17t", 0x4000, 0x2000, CRC(ba6ccd8c) SHA1(29a13e3161aba4db080434685869f8b79ad7997c) )
#ROM_REGION( 0x6000, "gfx4",0 )  /* sprite bank 1*/
#ROM_LOAD ( "11.18m", 0x0000, 0x2000, CRC(06a5d849) SHA1(b9f604edf4fdc053b738041493aef91dd730fe6b) )
#ROM_LOAD ( "12.18r", 0x2000, 0x2000, CRC(569815ef) SHA1(db261799892f60b2274b73fb25cde58219bb44db) )
#ROM_LOAD ( "13.18t", 0x4000, 0x2000, CRC(4f8af872) SHA1(6c07ff0733b8d8440309c9ae0db0876587b740a6) )
#ROM_REGION( 0x6000, "gfx5",0 )  /* sprite bank 2*/
#ROM_LOAD ( "14.19m", 0x0000, 0x2000, CRC(93f3c649) SHA1(38d6bb4b6108a67b135ae1a145532f4a0c2568b8) )
#ROM_LOAD ( "15.19r", 0x2000, 0x2000, CRC(5b3d87e4) SHA1(e47f7b62bf7101afba8d5e181f4bd8f8eb6eeb08) )
#ROM_LOAD ( "16.19u", 0x4000, 0x2000, CRC(a2c24b64) SHA1(e76558785ea337ab902fb6f94dc1a4bdfcd6335e) )

#ROM_REGION( 0x8000, "user1",0 ) /* road layout */
#ROM_LOAD ( "1.17a", 0x0000, 0x2000, CRC(f0fa72fc) SHA1(b73e794df635630f29a79adfe2951dc8f1d17e20) )
#ROM_LOAD ( "3.18b", 0x2000, 0x2000, CRC(954268f7) SHA1(07057296e0281f90b18dfe4223aad18bff7cfa6e) )
#ROM_LOAD ( "2.17d", 0x4000, 0x2000, CRC(2e38bb0e) SHA1(684f14a06ff957e40780be21c0ad5f10088a55ed) )
#ROM_LOAD ( "4.18d", 0x6000, 0x2000, CRC(3d9e16ab) SHA1(e99628ffc54e3ff4818313a287ca111617120910) )

#ROM_REGION( 0x300, "proms",0 )  /* colour */
#ROM_LOAD("tbp24s10.7u", 0x0000, 0x0100, CRC(9d199d33) SHA1(b8982f7da2b85f10d117177e4e73cbb486931cf5) )
#ROM_LOAD("tbp24s10.7t", 0x0100, 0x0100, CRC(c0426582) SHA1(8e3e4d1e76243cce272aa099d2d6ad4fa6c99f7c) )
#ROM_LOAD("tbp24s10.6t", 0x0200, 0x0100, CRC(c096e05c) SHA1(cb5b509e6124453f381a683ba446f8f4493d4610) )

#ROM_REGION( 0x10000,"audiocpu",0 )
#ROM_LOAD( "8.6f", 0x0000, 0x1000, CRC(6ec3c545) SHA1(1a2477b9e1563734195b0743f5dbbb005e06022e) )
#ROM_END






