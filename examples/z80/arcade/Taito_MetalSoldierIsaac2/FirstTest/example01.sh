# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm msisaac.zip
rm msisaac/*
rmdir msisaac
mkdir msisaac

dd bs=$((0x80000)) count=1 if=/dev/zero of=dummy512k.bin



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy512k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=msisaac/a34_11.bin
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=msisaac/a34_12.bin
dd ibs=1 count=$((0x4000)) skip=$((0x8000)) if=example01.bin of=msisaac/a34_13.bin
dd ibs=1 count=$((0x2000)) skip=$((0xC000)) if=example01.bin of=msisaac/a34_10.bin
dd bs=$((0x2000)) count=1 if=/dev/zero of=msisaac/ao4_05.ic3


#ROM_REGION( 0x10000, "maincpu", 0 ) /* Z80 main CPU */
#ROM_LOAD( "a34_11.bin", 0x0000, 0x4000, CRC(40819334) SHA1(65352607165043909a09e96c07f7060f6ce087e6) )
#ROM_LOAD( "a34_12.bin", 0x4000, 0x4000, CRC(4c50b298) SHA1(5962882ad37ba6990ba2a6312b570f214cd4c103) )
#ROM_LOAD( "a34_13.bin", 0x8000, 0x4000, CRC(2e2b09b3) SHA1(daa715282ed9ef2e519e252a684ef28085becabd) )
#ROM_LOAD( "a34_10.bin", 0xc000, 0x2000, CRC(a2c53dc1) SHA1(14f23511f92bcfc94447dabe2826555d68bc1caa) )







#mcu
dd bs=$((0x4000)) count=1 if=/dev/zero of=msisaac/a34_01.bin
dd bs=$((0x0800)) count=1 if=/dev/zero of=msisaac/a34.mcu

#ROM_REGION( 0x10000, "audiocpu", 0 ) /* Z80 sound CPU */
#ROM_LOAD( "a34_01.bin", 0x0000, 0x4000, CRC(545e45e7) SHA1(18ddb1ec8809bb62ae1c1068cd16cd3c933bf6ba) )
#ROM_REGION( 0x0800,  "cpu2", 0 )    /* 2k for the microcontroller */
#ROM_LOAD( "a34.mcu"       , 0x0000, 0x0800, NO_DUMP )







#gfx

zxb.py library/b4r1f1_charmaptankbusterssprites01.zxi --org=$((0x0000))
cat dummy512k.bin >> b4r1f1_charmaptankbusterssprites01.bin
dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=b4r1f1_charmaptankbusterssprites01.bin of=b4r1f1_charmaptankbusterssprites01b.bin
xxd -p -c 4 -s 3 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_02.bin
xxd -p -c 4 -s 2 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_03.bin
xxd -p -c 4 -s 1 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_04.bin
xxd -p -c 4 -s 0 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_05.bin
xxd -p -c 4 -s 3 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_06.bin
xxd -p -c 4 -s 2 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_07.bin
xxd -p -c 4 -s 1 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_08.bin
xxd -p -c 4 -s 0 -l $((0x10000)) b4r1f1_charmaptankbusterssprites01b.bin | cut -b 1-2 | xxd -r -p > msisaac/a34_09.bin

#zxb.py library/b1r1f1_charmapbeaminvader01.zxi --org=$((0x0000))
#cat dummy512k.bin >> b1r1f1_charmapbeaminvader01.bin
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=b1r1f1_charmapbeaminvader01b.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_02.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_03.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_04.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_05.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_06.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_07.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_08.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=msisaac/a34_09.bin





#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r1f1_charmapbeaminvader01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4_07.ic15 #-bitdepth1

rm b1r1f1_charmapbeaminvader01.bin b1r1f1_charmapbeaminvader01b.bin
rm b8r1f1_charmapmetalsoldierisaac01.bin b8r1f1_charmapmetalsoldierisaac01b.bin


#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=msisaac/msisaac.5f
#rm spritemap01.bin

#ROM_REGION( 0x8000, "gfx1", 0 )
#ROM_LOAD( "a34_02.bin", 0x0000, 0x2000, CRC(50da1a81) SHA1(8aa5a896f3e1173155d4574f5e1c2703e334cf44) )
#ROM_LOAD( "a34_03.bin", 0x2000, 0x2000, CRC(728a549e) SHA1(8969569d4b7a3ba7b740dbd236c047a46b723617) )
#ROM_LOAD( "a34_04.bin", 0x4000, 0x2000, CRC(e7d19f1c) SHA1(d55ee8085256c1f6a254d3249997326eebba7d88) )
#ROM_LOAD( "a34_05.bin", 0x6000, 0x2000, CRC(bed2107d) SHA1(83b16ca8a1b131aa6a2976cdbe907109750eaf71) )
#ROM_REGION( 0x8000, "gfx2", 0 )
#ROM_LOAD( "a34_06.bin", 0x0000, 0x2000, CRC(4ec71687) SHA1(e88f0c61a172fbca1784c95246776bf64c071bf7) )
#ROM_LOAD( "a34_07.bin", 0x2000, 0x2000, CRC(24922abf) SHA1(e42b4947b8c84bdf62990205308b8c187352d001) )
#ROM_LOAD( "a34_08.bin", 0x4000, 0x2000, CRC(3ddbf4c0) SHA1(7dd82aba661addd0a905bc185c1a6d7f2e21e0c6) )
#ROM_LOAD( "a34_09.bin", 0x6000, 0x2000, CRC(23eb089d) SHA1(fcf48862825bf09ba3718cbade0e163a660e1a68) )









#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > msisaac/ao4-12.ic95
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=msisaac/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=msisaac/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=msisaac/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=msisaac/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=msisaac/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=msisaac/82s126.4a
#rm clut01.bin



rm dummy512k.bin example01.bin
rm b4r1f1_charmaptankbusterssprites01b.bin b4r1f1_charmaptankbusterssprites01.bin


rm smsboot.bin

zip -r msisaac msisaac
rm msisaac/*
rmdir msisaac

mame -w -video soft -resolution0 512x512 -rp ./ msisaac
#xmame.SDL -rp ./ msisaac




#ROM_START( msisaac )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* Z80 main CPU */
#ROM_LOAD( "a34_11.bin", 0x0000, 0x4000, CRC(40819334) SHA1(65352607165043909a09e96c07f7060f6ce087e6) )
#ROM_LOAD( "a34_12.bin", 0x4000, 0x4000, CRC(4c50b298) SHA1(5962882ad37ba6990ba2a6312b570f214cd4c103) )
#ROM_LOAD( "a34_13.bin", 0x8000, 0x4000, CRC(2e2b09b3) SHA1(daa715282ed9ef2e519e252a684ef28085becabd) )
#ROM_LOAD( "a34_10.bin", 0xc000, 0x2000, CRC(a2c53dc1) SHA1(14f23511f92bcfc94447dabe2826555d68bc1caa) )

#ROM_REGION( 0x10000, "audiocpu", 0 ) /* Z80 sound CPU */
#ROM_LOAD( "a34_01.bin", 0x0000, 0x4000, CRC(545e45e7) SHA1(18ddb1ec8809bb62ae1c1068cd16cd3c933bf6ba) )

#ROM_REGION( 0x0800,  "cpu2", 0 )    /* 2k for the microcontroller */
#ROM_LOAD( "a34.mcu"       , 0x0000, 0x0800, NO_DUMP )

#// I tried following MCUs; none of them work with this game:
#//  ROM_LOAD( "a30-14"    , 0x0000, 0x0800, CRC(c4690279) ) //40love
#//  ROM_LOAD( "a22-19.31",  0x0000, 0x0800, CRC(06a71df0) )     //buggy challenge
#//  ROM_LOAD( "a45-19",     0x0000, 0x0800, CRC(5378253c) )     //flstory
#//  ROM_LOAD( "a54-19",     0x0000, 0x0800, CRC(e08b8846) )     //lkage

#ROM_REGION( 0x8000, "gfx1", 0 )
#ROM_LOAD( "a34_02.bin", 0x0000, 0x2000, CRC(50da1a81) SHA1(8aa5a896f3e1173155d4574f5e1c2703e334cf44) )
#ROM_LOAD( "a34_03.bin", 0x2000, 0x2000, CRC(728a549e) SHA1(8969569d4b7a3ba7b740dbd236c047a46b723617) )
#ROM_LOAD( "a34_04.bin", 0x4000, 0x2000, CRC(e7d19f1c) SHA1(d55ee8085256c1f6a254d3249997326eebba7d88) )
#ROM_LOAD( "a34_05.bin", 0x6000, 0x2000, CRC(bed2107d) SHA1(83b16ca8a1b131aa6a2976cdbe907109750eaf71) )
#ROM_REGION( 0x8000, "gfx2", 0 )
#ROM_LOAD( "a34_06.bin", 0x0000, 0x2000, CRC(4ec71687) SHA1(e88f0c61a172fbca1784c95246776bf64c071bf7) )
#ROM_LOAD( "a34_07.bin", 0x2000, 0x2000, CRC(24922abf) SHA1(e42b4947b8c84bdf62990205308b8c187352d001) )
#ROM_LOAD( "a34_08.bin", 0x4000, 0x2000, CRC(3ddbf4c0) SHA1(7dd82aba661addd0a905bc185c1a6d7f2e21e0c6) )
#ROM_LOAD( "a34_09.bin", 0x6000, 0x2000, CRC(23eb089d) SHA1(fcf48862825bf09ba3718cbade0e163a660e1a68) )

#ROM_END




