rm example01.asm
rm cosmica.zip
rm cosmica/*
rmdir cosmica
mkdir cosmica




dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=cosmica/ii-1.e3
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=cosmica/ii-2.e4
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=cosmica/ii-3.e5
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=cosmica/ii-4.e6
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=cosmica/ii-5.e7
rm example01.bin

#ROM_START( cosmica ) /* later revision 7910-AII pcb; some roms are marked II-x; note that this set does NOT have the 1979 copyright date on the titlescreen! */
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "ii-1.e3",        0x0000, 0x0800, CRC(535ee0c5) SHA1(3ec3056b7fabe07ef49a9179114aa74be44a943e) ) /* tms2516 */
#ROM_LOAD( "ii-2.e4",        0x0800, 0x0800, CRC(ed3cf8f7) SHA1(6ba1d98d82400519e844b950cb2fb1274c06d89a) ) /* tms2516; has an & stamped on the chip */
#ROM_LOAD( "ii-3.e5",        0x1000, 0x0800, CRC(6a111e5e) SHA1(593be409bc969cece2ff88623e53c166b4dc43cd) ) /* tms2516 */
#ROM_LOAD( "ii-4.e6",        0x1800, 0x0800, CRC(c9b5ca2a) SHA1(3384b98954b6bc9a64e753b95757f61ce1d3c52e) ) /* tms2516 */
#ROM_LOAD( "ii-5.e7",        0x2000, 0x0800, CRC(43666d68) SHA1(e44492360a77d93aeaaaa0f38f4ac19732998559) ) /* tms2516; has an & stamped on the chip */





#gfx
zxb.py library/charmaphigemaru01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmaphigemaru01.bin of=charmaphigemaru01b.bin
rm charmaphigemaru01.bin
#gfx1
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=cosmica/ii-7.n2
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=charmaphigemaru01b.bin of=cosmica/ii-6.n1
rm charmaphigemaru01b.bin

#ROM_REGION( 0x1000, "gfx1", 0 ) /* sprites */
#ROM_LOAD( "ii-7.n2",        0x0000, 0x0800, CRC(aa6c6079) SHA1(af4ab73e9e1c189290b26bf42adb511d5a347df9) ) // verify marking
#ROM_LOAD( "ii-6.n1",        0x0800, 0x0800, CRC(431e866c) SHA1(b007cd3cc856360a0247bd78bb49d173f5cef321) ) // verify marking







#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=cosmica/hgb3.l6
#rm palette01b.bin
#lookup
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=cosmica/hgb5.m4  #?ch
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=cosmica/hgb1.h7  #?sp
#rm clut01b.bin

#proms
dd bs=$((0x020)) count=1 if=/dev/urandom of=cosmica/u7910.d9 #unknown
#dd bs=$((0x400)) count=1 if=/dev/urandom of=cosmica/9.e2 #raster
#dd bs=$((0x400)) count=1 if=/dev/urandom of=cosmica/8.k3 #stars

#touch cosmica/u7910.d9
#touch cosmica/9.e2
touch cosmica/8.k3

dd if=/dev/zero ibs=1k count=$((0x400)) | tr "\000" "\377" > cosmica/9.e2

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "u7910.d9",    0x0000, 0x0020, CRC(dfb60f19) SHA1(d510327ff3492f098659c551f7245835f61a2959) ) // verify marking
#ROM_REGION( 0x0400, "user1", 0 ) /* color map */
#ROM_LOAD( "9.e2",        0x0000, 0x0400, CRC(ea4ee931) SHA1(d0a4afda4b493efb40286c2d67bf56a2a8b8da9d) ) /* 2708 */
#ROM_REGION( 0x0400, "user2", 0 ) /* starfield generator */
#ROM_LOAD( "8.k3",       0x0000, 0x0400, CRC(acbd4e98) SHA1(d33fe8bdc77bb18a3ffb369ea692210d1b890771) ) // verify marking
#ROM_END







#-
zip -r cosmica cosmica
rm cosmica/*
rmdir cosmica
rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ cosmica





#ROM_START( cosmica ) /* later revision 7910-AII pcb; some roms are marked II-x; note that this set does NOT have the 1979 copyright date on the titlescreen! */
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "ii-1.e3",        0x0000, 0x0800, CRC(535ee0c5) SHA1(3ec3056b7fabe07ef49a9179114aa74be44a943e) ) /* tms2516 */
#ROM_LOAD( "ii-2.e4",        0x0800, 0x0800, CRC(ed3cf8f7) SHA1(6ba1d98d82400519e844b950cb2fb1274c06d89a) ) /* tms2516; has an & stamped on the chip */
#ROM_LOAD( "ii-3.e5",        0x1000, 0x0800, CRC(6a111e5e) SHA1(593be409bc969cece2ff88623e53c166b4dc43cd) ) /* tms2516 */
#ROM_LOAD( "ii-4.e6",        0x1800, 0x0800, CRC(c9b5ca2a) SHA1(3384b98954b6bc9a64e753b95757f61ce1d3c52e) ) /* tms2516 */
#ROM_LOAD( "ii-5.e7",        0x2000, 0x0800, CRC(43666d68) SHA1(e44492360a77d93aeaaaa0f38f4ac19732998559) ) /* tms2516; has an & stamped on the chip */

#ROM_REGION( 0x1000, "gfx1", 0 ) /* sprites */
#ROM_LOAD( "ii-7.n2",        0x0000, 0x0800, CRC(aa6c6079) SHA1(af4ab73e9e1c189290b26bf42adb511d5a347df9) ) // verify marking
#ROM_LOAD( "ii-6.n1",        0x0800, 0x0800, CRC(431e866c) SHA1(b007cd3cc856360a0247bd78bb49d173f5cef321) ) // verify marking

#ROM_REGION( 0x0020, "proms", 0 )
#ROM_LOAD( "u7910.d9",    0x0000, 0x0020, CRC(dfb60f19) SHA1(d510327ff3492f098659c551f7245835f61a2959) ) // verify marking
#ROM_REGION( 0x0400, "user1", 0 ) /* color map */
#ROM_LOAD( "9.e2",        0x0000, 0x0400, CRC(ea4ee931) SHA1(d0a4afda4b493efb40286c2d67bf56a2a8b8da9d) ) /* 2708 */
#ROM_REGION( 0x0400, "user2", 0 ) /* starfield generator */
#ROM_LOAD( "8.k3",       0x0000, 0x0400, CRC(acbd4e98) SHA1(d33fe8bdc77bb18a3ffb369ea692210d1b890771) ) // verify marking
#ROM_END

