rm skylncr.zip
rm example01.asm
rm skylncr/*
rmdir skylncr
mkdir skylncr

dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin
dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin
dd bs=$((0x40000)) count=1 if=/dev/zero of=dummy256k.bin
dd bs=$((0x80000)) count=1 if=/dev/zero of=dummy512k.bin
dd bs=$((0x100000)) count=1 if=/dev/zero of=dummy1m.bin


#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=example01.bin of=skylncr/27512.u35




#gfx

zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy1m.bin >> charmap01.bin
dd ibs=1 count=$((0x100000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin

xxd -p -c 4 -s 0 -l $((0x80000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > charmap01b0.bin
xxd -p -c 4 -s 2 -l $((0x80000)) charmap01b.bin | cut -b 1-4 | xxd -r -p > charmap01b1.bin
#xxd -p -c 4 -s 2 -l $((0x100000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > charmap01b2.bin
#xxd -p -c 4 -s 3 -l $((0x100000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > charmap01b3.bin

touch skylncr/574200.u32
cat charmap01b0.bin >> skylncr/574200.u32
cat charmap01b1.bin >> skylncr/574200.u32

touch skylncr/574200.u33
cat charmap01b0.bin >> skylncr/574200.u33
cat charmap01b1.bin >> skylncr/574200.u33


#zxb.py library/charmap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> charmap01.bin
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin

#touch skylncr/574200.u32
#cat charmap01b.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32
#cat charmap01b.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32
#cat dummy64k.bin >> skylncr/574200.u32

#touch skylncr/574200.u33
#cat charmap01b.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33
#cat charmap01b.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33
#cat dummy64k.bin >> skylncr/574200.u33

#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=skylncr/574200.u32
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=skylncr/574200.u33
rm charmap01.bin charmap01b.bin
rm charmap01b0.bin charmap01b1.bin charmap01b2.bin charmap01b3.bin




#zxb.py library/spritemap01.zxi --org=$((0x0000))
#cat dummy64k.bin >> spritemap01.bin
#dd ibs=1 count=$((0x2000)) skip=$((0x0010)) if=spritemap01.bin of=skylncr/pp_e02.rom
#rm spritemap01.bin



#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy64k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=skylncr/skylncr.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=skylncr/skylncr.3j


#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy64k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=skylncr/skylncr.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=skylncr/skylncr.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=skylncr/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=skylncr/82s126.3m

rm smsboot.bin example01.bin
rm dummy64k.bin dummy128k.bin dummy256k.bin dummy512k.bin dummy1m.bin

zip -r skylncr skylncr
rm skylncr/*
rmdir skylncr

mame -w -video soft -resolution0 512x512 -rp ./ skylncr




#ROM_START( skylncr )
#ROM_REGION( 0x80000, "maincpu", 0 )
#ROM_LOAD( "27512.u35",  0x00000, 0x10000, CRC(98b1c9fe) SHA1(9ca1706d25038a078fb07ba5c2e6681ed468bc88) )

#ROM_REGION( 0x80000, "gfx1", 0 )
#ROM_LOAD( "574200.u32", 0x00000, 0x80000, CRC(b36f11fe) SHA1(1d8660ac1ca44e33976ac14210e4a3a201f8f3c4) )
#ROM_REGION( 0x80000, "gfx2", 0 )
#ROM_LOAD( "574200.u33", 0x00000, 0x80000, CRC(19b25221) SHA1(2f32d337125a9fd0bc7f50713b05e564fd4f81b2) )
#ROM_END



