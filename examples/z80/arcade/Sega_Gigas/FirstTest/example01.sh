# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm freekick.zip
rm freekick/*
rmdir freekick
mkdir freekick

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#maincpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0xD000)) skip=$((0x0000)) if=example01.bin of=freekick/ns6201-a_1987.10_free_kick.cpu
rm example01.bin

#sound
dd bs=$((0x8000)) count=1 if=/dev/zero of=freekick/11.1e

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
xxd -p -c 3 -s 0 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/12.1h
xxd -p -c 3 -s 1 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/13.1j
xxd -p -c 3 -s 2 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/14.1l
#gfx2
xxd -p -c 3 -s 0 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/15.1m
xxd -p -c 3 -s 1 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/16.1p
xxd -p -c 3 -s 2 -l $((0xC000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > freekick/17.1r
rm charmap01b.bin

#misc
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 3 -s 0 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.8j
xxd -p -c 3 -s 1 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.8k
xxd -p -c 3 -s 2 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.8h
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.8j #r - tiles
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.8k #g - tiles
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.8h #b - tiles
xxd -p -c 3 -s 0 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.7j
xxd -p -c 3 -s 1 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.7k
xxd -p -c 3 -s 2 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > freekick/24s10n.7h
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.7j #r - sprites
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.7k #g - sprites
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=freekick/24s10n.7h #b - sprites
rm palette01b.bin

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=freekick/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=freekick/82s126.4a
#rm clut01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=freekick/freekick.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=freekick/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=freekick/82s126.3m

zip -r freekick freekick
rm freekick/*
rmdir freekick

rm smsboot.bin dummy64k.bin
mame -w -video soft -resolution0 512x512 -rp ./ freekick

