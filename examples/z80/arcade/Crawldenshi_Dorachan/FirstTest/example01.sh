rm dorachan.zip
rm example01.asm
rm dorachan/*
rmdir dorachan
mkdir dorachan







dd bs=$((0x10000)) count=1 if=/dev/zero of=dummy64k.bin







#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy64k.bin >> example01.bin
# 0x0000..0x17FF
dd ibs=1 count=$((0x400)) skip=$((0x0000)) if=example01.bin of=dorachan/c1.e1
dd ibs=1 count=$((0x400)) skip=$((0x0400)) if=example01.bin of=dorachan/d2.e2
dd ibs=1 count=$((0x400)) skip=$((0x0800)) if=example01.bin of=dorachan/d3.e3
dd ibs=1 count=$((0x400)) skip=$((0x0C00)) if=example01.bin of=dorachan/d4.e5
dd ibs=1 count=$((0x400)) skip=$((0x1000)) if=example01.bin of=dorachan/c5.e6
dd ibs=1 count=$((0x400)) skip=$((0x1400)) if=example01.bin of=dorachan/d6.e7
# 0x2000..0x23FF
dd ibs=1 count=$((0x400)) skip=$((0x2000)) if=example01.bin of=dorachan/c7.e8
# 0x6000..0x77FF
dd ibs=1 count=$((0x400)) skip=$((0x6000)) if=example01.bin of=dorachan/d8.rom
dd ibs=1 count=$((0x400)) skip=$((0x6400)) if=example01.bin of=dorachan/d9.rom
dd ibs=1 count=$((0x400)) skip=$((0x6800)) if=example01.bin of=dorachan/d10.rom
dd ibs=1 count=$((0x400)) skip=$((0x6C00)) if=example01.bin of=dorachan/d11.rom
dd ibs=1 count=$((0x400)) skip=$((0x7000)) if=example01.bin of=dorachan/d12.rom
dd ibs=1 count=$((0x400)) skip=$((0x7400)) if=example01.bin of=dorachan/d13.rom

#- ROM_START( dorachan )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "c1.e1",      0x0000, 0x0400, CRC(29d66a96) SHA1(a0297d87574af65c6ded99aeb377ac407f6f163f) )
#- ROM_LOAD( "d2.e2",      0x0400, 0x0400, CRC(144b6cd1) SHA1(195ce86e912a4b395097008c6d812fd75a1a2482) )
#- ROM_LOAD( "d3.e3",      0x0800, 0x0400, CRC(a9a1bed7) SHA1(98af6f851c4477f770b6bd67e5465b5a271311ee) )
#- ROM_LOAD( "d4.e5",      0x0c00, 0x0400, CRC(099ddf4b) SHA1(e4dd2b17a4320615204c66c24f60e58db13a5319) )
#- ROM_LOAD( "c5.e6",      0x1000, 0x0400, CRC(49449dab) SHA1(3627c16cc17fae9de2294a37602b726e107d0a13) )
#- ROM_LOAD( "d6.e7",      0x1400, 0x0400, CRC(5e409680) SHA1(f5e4d820c0f0493d724cd0d3da1113bccc09c2c3) )
#- ROM_LOAD( "c7.e8",      0x2000, 0x0400, CRC(b331a5ff) SHA1(1053953c76dddff450b9c9037e7797d50f9c7046) )
#- ROM_LOAD( "d8.rom",     0x6000, 0x0400, CRC(5fe1e731) SHA1(8e5dcb5f8d1d6f8c06808dd808f8bce7b07014ee) )
#- ROM_LOAD( "d9.rom",     0x6400, 0x0400, CRC(338881a8) SHA1(cd725b42c3f96826e94345698738f6b5a532d3d5) )
#- ROM_LOAD( "d10.rom",    0x6800, 0x0400, CRC(f8c59517) SHA1(655a976b1221e5aff69e0c0cc58d02c0b7bb6197) )
#- ROM_LOAD( "d11.rom",    0x6c00, 0x0400, CRC(c2e0f066) SHA1(be6b780a8957d945e5634ac9689b440a41e9a2a4) )
#- ROM_LOAD( "d12.rom",    0x7000, 0x0400, CRC(275e5dc1) SHA1(ac07db4b428daa49a52c679de95ddedbea0076b9) )
#- ROM_LOAD( "d13.rom",    0x7400, 0x0400, CRC(24ccfcf9) SHA1(85e5052ee657f518b0509eb64e494bc3a74e651e) )









#prom
dd if=/dev/zero ibs=$((0x0400)) count=$((0x0400)) | tr "\000" "\377" > _t_.bin
dd ibs=1 count=$((0x400)) skip=$((0x0000)) if=_t_.bin of=dorachan/d14.rom
rm _t_.bin


#- ROM_REGION( 0x0400, "proms", 0 )  /* color map */
#- ROM_LOAD( "d14.rom",    0x0000, 0x0400, CRC(c0d3ee84) SHA1(f2207c685ce8d5144a373c28f11d2cebf9518b65) )
#- ROM_END











rm smsboot.bin dummy64k.bin example01.bin

zip -r dorachan dorachan
rm dorachan/*
rmdir dorachan

mame -w -video soft -resolution0 512x512 -rp ./ dorachan









#- ROM_START( dorachan )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "c1.e1",      0x0000, 0x0400, CRC(29d66a96) SHA1(a0297d87574af65c6ded99aeb377ac407f6f163f) )
#- ROM_LOAD( "d2.e2",      0x0400, 0x0400, CRC(144b6cd1) SHA1(195ce86e912a4b395097008c6d812fd75a1a2482) )
#- ROM_LOAD( "d3.e3",      0x0800, 0x0400, CRC(a9a1bed7) SHA1(98af6f851c4477f770b6bd67e5465b5a271311ee) )
#- ROM_LOAD( "d4.e5",      0x0c00, 0x0400, CRC(099ddf4b) SHA1(e4dd2b17a4320615204c66c24f60e58db13a5319) )
#- ROM_LOAD( "c5.e6",      0x1000, 0x0400, CRC(49449dab) SHA1(3627c16cc17fae9de2294a37602b726e107d0a13) )
#- ROM_LOAD( "d6.e7",      0x1400, 0x0400, CRC(5e409680) SHA1(f5e4d820c0f0493d724cd0d3da1113bccc09c2c3) )
#- ROM_LOAD( "c7.e8",      0x2000, 0x0400, CRC(b331a5ff) SHA1(1053953c76dddff450b9c9037e7797d50f9c7046) )
#- ROM_LOAD( "d8.rom",     0x6000, 0x0400, CRC(5fe1e731) SHA1(8e5dcb5f8d1d6f8c06808dd808f8bce7b07014ee) )
#- ROM_LOAD( "d9.rom",     0x6400, 0x0400, CRC(338881a8) SHA1(cd725b42c3f96826e94345698738f6b5a532d3d5) )
#- ROM_LOAD( "d10.rom",    0x6800, 0x0400, CRC(f8c59517) SHA1(655a976b1221e5aff69e0c0cc58d02c0b7bb6197) )
#- ROM_LOAD( "d11.rom",    0x6c00, 0x0400, CRC(c2e0f066) SHA1(be6b780a8957d945e5634ac9689b440a41e9a2a4) )
#- ROM_LOAD( "d12.rom",    0x7000, 0x0400, CRC(275e5dc1) SHA1(ac07db4b428daa49a52c679de95ddedbea0076b9) )
#- ROM_LOAD( "d13.rom",    0x7400, 0x0400, CRC(24ccfcf9) SHA1(85e5052ee657f518b0509eb64e494bc3a74e651e) )

#- ROM_REGION( 0x0400, "proms", 0 )  /* color map */
#- ROM_LOAD( "d14.rom",    0x0000, 0x0400, CRC(c0d3ee84) SHA1(f2207c685ce8d5144a373c28f11d2cebf9518b65) )
#- ROM_END






