	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$47FF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 18432
	ld (_eee), hl
	jp __LABEL0
__LABEL3:
	ld a, 45
	ld hl, (_eee)
	ld (hl), a
__LABEL4:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL0:
	ld hl, 19455
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL5
__LABEL8:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL10
__LABEL13:
__LABEL14:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL10:
	ld hl, 1000
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _scrambleattr
__LABEL9:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL5:
	ld hl, 31
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	ld hl, 0
	ld (_ee3), hl
	jp __LABEL15
__LABEL18:
	ld hl, 8
	ld (_ee2), hl
	jp __LABEL20
__LABEL23:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL25
__LABEL28:
__LABEL29:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL25:
	ld hl, 100
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL28
__LABEL27:
	ld hl, 46
	push hl
	ld hl, (_ee3)
	push hl
	ld hl, (_ee2)
	push hl
	call _scrambleputchar
__LABEL24:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL20:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL23
__LABEL22:
__LABEL19:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL15:
	ld hl, 31
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL30
__LABEL33:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL35
__LABEL38:
__LABEL39:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL35:
	ld hl, 1000
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL38
__LABEL37:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	push hl
	ld hl, (_eee)
	push hl
	call _scramblescroll
__LABEL34:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL30:
	ld hl, 31
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL33
__LABEL32:
	ld hl, 20544
	ld (_eee), hl
	jp __LABEL40
__LABEL43:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL44:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL40:
	ld hl, 20575
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL43
__LABEL42:
	ld hl, 20576
	ld (_eee), hl
	jp __LABEL45
__LABEL48:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL50
__LABEL53:
__LABEL54:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL50:
	ld hl, 100
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL53
__LABEL52:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld a, l
	ld hl, (_eee)
	ld (hl), a
__LABEL49:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL45:
	ld hl, 20607
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL48
__LABEL47:
	ld hl, 20480
	ld (_eee), hl
	jp __LABEL55
__LABEL58:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL60
__LABEL63:
__LABEL64:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL60:
	ld hl, 1000
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL63
__LABEL62:
	xor a
	ld hl, (_eee)
	ld (hl), a
__LABEL59:
	ld hl, (_eee)
	inc hl
	inc hl
	ld (_eee), hl
__LABEL55:
	ld hl, 20543
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL58
__LABEL57:
	xor a
	ld (_ee4), a
	ld hl, 12
	ld (_ee3), hl
	jp __LABEL65
__LABEL68:
	ld hl, 12
	ld (_ee2), hl
	jp __LABEL70
__LABEL73:
	ld hl, (_ee2)
	ld de, 32
	call __MUL16_FAST
	ld de, 18432
	add hl, de
	ex de, hl
	ld hl, (_ee3)
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld a, (_ee4)
	pop hl
	ld (hl), a
	ld a, (_ee4)
	inc a
	ld (_ee4), a
__LABEL74:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL70:
	ld hl, 27
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL73
__LABEL72:
__LABEL69:
	ld hl, (_ee3)
	inc hl
	ld (_ee3), hl
__LABEL65:
	ld hl, 27
	ld de, (_ee3)
	or a
	sbc hl, de
	jp nc, __LABEL68
__LABEL67:
	ld hl, 0
	ld (_eee), hl
	jp __LABEL75
__LABEL78:
	ld hl, 1
	ld (_dly), hl
	jp __LABEL80
__LABEL83:
__LABEL84:
	ld hl, (_dly)
	inc hl
	ld (_dly), hl
__LABEL80:
	ld hl, 100
	ld de, (_dly)
	or a
	sbc hl, de
	jp nc, __LABEL83
__LABEL82:
	xor a
	push af
	xor a
	push af
	ld a, 128
	push af
	ld hl, (_eee)
	ld a, l
	push af
	xor a
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 128
	push af
	ld hl, (_eee)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 1
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 128
	push af
	ld hl, (_eee)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 2
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 128
	push af
	ld hl, (_eee)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 3
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 144
	push af
	ld hl, (_eee)
	ld a, l
	push af
	ld a, 4
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 144
	push af
	ld hl, (_eee)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 5
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 144
	push af
	ld hl, (_eee)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 6
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld a, 144
	push af
	ld hl, (_eee)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 7
	push af
	call _scrambleputsprite
__LABEL79:
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
__LABEL75:
	ld hl, 255
	ld de, (_eee)
	or a
	sbc hl, de
	jp nc, __LABEL78
__LABEL77:
	xor a
	push af
	xor a
	push af
	ld a, 128
	push af
	ld a, 128
	push af
	xor a
	push af
	call _scrambleputsprite
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL85
__LABEL88:
	ld de, __LABEL__text01
	ld hl, (_ee2)
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	push hl
	ld hl, 10
	push hl
	ld hl, (_ee2)
	ld de, 12
	add hl, de
	push hl
	call _scrambleputchar
__LABEL89:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL85:
	ld hl, 11
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL88
__LABEL87:
	ld hl, 128
	ld (_ex0), hl
	ld hl, 128
	ld (_ey0), hl
	ld hl, 0
	ld (_eee), hl
__LABEL90:
	ld hl, (_seed)
	call _smsrnd
	ld (_seed), hl
	ld hl, 1
	ld (_ee2), hl
	jp __LABEL92
__LABEL95:
__LABEL96:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL92:
	ld hl, 1000
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL95
__LABEL94:
	ld a, (33024)
	cpl
	ld l, a
	ld h, 0
	ld (_ej0), hl
	ld a, (33025)
	cpl
	ld l, a
	ld h, 0
	ld (_ej1), hl
	ld hl, (_ej0)
	ld a, l
	ld (19252), a
	ld hl, (_ej1)
	ld a, l
	ld (19220), a
	ld hl, (_ej0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (19193), a
	ld hl, (_ej0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19225), a
	ld hl, (_ej0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19257), a
	ld hl, (_ej1)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (19194), a
	ld hl, (_ej1)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19226), a
	ld hl, (_ej1)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19258), a
	ld hl, (_ex0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (19190), a
	ld hl, (_ex0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19222), a
	ld hl, (_ex0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19254), a
	ld hl, (_ey0)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (19191), a
	ld hl, (_ey0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19223), a
	ld hl, (_ey0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19255), a
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 48
	add hl, de
	ld a, l
	ld (19192), a
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19224), a
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19256), a
	ld hl, (_eee)
	ld de, 1000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19288), a
	ld hl, (_eee)
	ld de, 10000
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (19320), a
	ld de, 16
	ld hl, (_ej0)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ex0)
	add hl, de
	ld (_ex0), hl
	ld de, 32
	ld hl, (_ej0)
	call __BAND16
	ld de, 32
	call __DIVU16
	ex de, hl
	ld hl, (_ex0)
	or a
	sbc hl, de
	ld (_ex0), hl
	ld de, 16
	ld hl, (_ej1)
	call __BAND16
	ld de, 16
	call __DIVU16
	ex de, hl
	ld hl, (_ey0)
	add hl, de
	ld (_ey0), hl
	ld de, 32
	ld hl, (_ej1)
	call __BAND16
	ld de, 32
	call __DIVU16
	ex de, hl
	ld hl, (_ey0)
	or a
	sbc hl, de
	ld (_ey0), hl
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	xor a
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 1
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 2
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 3
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld a, l
	push af
	ld a, 4
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld a, 5
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 32
	add hl, de
	ld a, l
	push af
	ld a, 6
	push af
	call _scrambleputsprite
	xor a
	push af
	xor a
	push af
	ld hl, (_ey0)
	ld de, 16
	add hl, de
	ld a, l
	push af
	ld hl, (_ex0)
	ld de, 48
	add hl, de
	ld a, l
	push af
	ld a, 7
	push af
	call _scrambleputsprite
	ld hl, (_seed)
	push hl
	ld hl, (_ey0)
	ld de, 12
	call __MODU16
	ld de, 29
	add hl, de
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	call _scrambleattr
	ld hl, (_ex0)
	push hl
	ld hl, (_ey0)
	ld de, 12
	call __MODU16
	ld de, 29
	add hl, de
	push hl
	ld de, 31
	pop hl
	call __BAND16
	push hl
	call _scramblescroll
	jp __LABEL90
__LABEL91:
__LABEL97:
	jp __LABEL97
__LABEL98:
__LABEL__text01:
#line 179
		defb "HELLO WORLD!!!!!"
#line 180
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_scrambleputsprite:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 20544
	add hl, de
	push hl
	ld a, (ix+7)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 20545
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 20546
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ld de, 20547
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
_scrambleputsprite__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_scrambleputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __MUL16_FAST
	ld de, 18432
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld de, 992
	pop hl
	call __BXOR16
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
_scrambleputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_scramblescroll:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	add hl, hl
	ld de, 20480
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld a, l
	pop hl
	ld (hl), a
_scramblescroll__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_scrambleattr:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 31
	pop hl
	call __BAND16
	add hl, hl
	ld de, 20481
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ld a, l
	pop hl
	ld (hl), a
_scrambleattr__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1220 "example01.zxb"
#line 1 "bxor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise xor 16 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
	
__BXOR16:
		ld a, h
		xor d
	    ld h, a
	
	    ld a, l
	    xor e
	    ld l, a
	
	    ret 
	
#line 1221 "example01.zxb"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 1222 "example01.zxb"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1223 "example01.zxb"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1224 "example01.zxb"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1225 "example01.zxb"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 1226 "example01.zxb"
	
ZXBASIC_USER_DATA:
	_dly EQU 16400
	_eee EQU 16402
	_seed EQU 16404
	_ee2 EQU 16406
	_ee3 EQU 16408
	_ee4 EQU 16410
	_ecsp EQU 16412
	_ecsq EQU 16414
	_ex0 EQU 16416
	_ey0 EQU 16418
	_ej0 EQU 16420
	_ej1 EQU 16422
	_i EQU 16424
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
