# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm triplep.zip
rm triplep/*
rmdir triplep
mkdir triplep

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=16384 count=1 if=/dev/zero of=dummy16k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy16k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=triplep/triplep.2g
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=triplep/triplep.2h
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=triplep/triplep.2k
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=triplep/triplep.2l
rm example01.bin

zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy16k.bin >> charmap01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=charmap01b.bin of=triplep/triplep.5f
#dd ibs=1 count=$((0x0800)) skip=$((0x0800)) if=charmap01b.bin of=triplep/triplep.5h

xxd -p -c 2 -s 0 -l $((0x1000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > triplep/triplep.5f
xxd -p -c 2 -s 1 -l $((0x1000)) charmap01b.bin | cut -b 1-2 | xxd -r -p > triplep/triplep.5h

rm charmap01b.bin

zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=triplep/tripprom.6e
rm palette01.bin

#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=triplep/82s123.7f
#rm palette01.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=triplep/triplep.5f
#rm spritemap01.bin
#dd bs=256 count=1 if=/dev/zero of=triplep/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=triplep/82s126.3m

rm smsboot.bin dummy16k.bin

zip -r triplep triplep
rm triplep/*
rmdir triplep

mame -w -video soft -nosound -resolution0 512x512 -rp ./ triplep

