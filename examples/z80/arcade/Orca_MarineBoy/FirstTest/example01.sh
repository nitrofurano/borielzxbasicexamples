rm example01.asm
rm marineb.zip
rm marineb/*
rmdir marineb
mkdir marineb

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin

zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin

cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

#cpu
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=marineb/marineb.1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=marineb/marineb.2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=marineb/marineb.3
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=marineb/marineb.4
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=marineb/marineb.5
rm example01.bin

#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy64k.bin >> charmap01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin
rm charmap01.bin
#gfx1
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=marineb/marineb.6
#gfx2
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=marineb/marineb.7
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=charmap01b.bin of=marineb/marineb.8
rm charmap01b.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=marineb/marineb.5f
#rm spritemap01.bin

#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
rm palette01.bin
xxd -p -c 2 -s 0 -l 512 palette01b.bin | cut -b 1-2 | xxd -r -p > marineb/marineb.1b
xxd -p -c 2 -s 1 -l 512 palette01b.bin | cut -b 1-2 | xxd -r -p > marineb/marineb.1c
rm palette01b.bin

#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=palette01b.bin of=marineb/marineb.1b
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=palette01b.bin of=marineb/marineb.1c

#dd bs=$((0x0100)) count=1 if=/dev/urandom of=marineb/marineb.1b
#dd bs=$((0x0100)) count=1 if=/dev/urandom of=marineb/marineb.1c
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=marineb/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=marineb/82s126.4a
#rm clut01.bin

rm smsboot.bin  dummy64k.bin

zip -r marineb marineb
rm marineb/*
rmdir marineb

mame -w -video soft -resolution0 512x512 -rp ./ marineb

