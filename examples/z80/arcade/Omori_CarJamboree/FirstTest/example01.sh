rm carjmbre.zip
rm example01.asm
rm carjmbre/*
rmdir carjmbre
mkdir carjmbre






#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
dd bs=32768 count=1 if=/dev/zero of=dummy32k.bin
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin

cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=example01.bin of=carjmbre/c1
dd ibs=1 count=$((0x1000)) skip=$((0x1000)) if=example01.bin of=carjmbre/c2
dd ibs=1 count=$((0x1000)) skip=$((0x2000)) if=example01.bin of=carjmbre/c3
dd ibs=1 count=$((0x1000)) skip=$((0x3000)) if=example01.bin of=carjmbre/c4
dd ibs=1 count=$((0x1000)) skip=$((0x4000)) if=example01.bin of=carjmbre/c5
dd ibs=1 count=$((0x1000)) skip=$((0x5000)) if=example01.bin of=carjmbre/c6
dd ibs=1 count=$((0x1000)) skip=$((0x6000)) if=example01.bin of=carjmbre/c7
dd ibs=1 count=$((0x1000)) skip=$((0x7000)) if=example01.bin of=carjmbre/c8

#ROM_START( carjmbre )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "c1",      0x0000, 0x1000, CRC(62b21739) SHA1(710e5c52f27603aa8f864f6f28d7272f21271d60) )
#ROM_LOAD( "c2",      0x1000, 0x1000, CRC(9ab1a0fa) SHA1(519cf67b98e62b2b42232788ba01ab6637880afc) )
#ROM_LOAD( "c3",      0x2000, 0x1000, CRC(bb29e100) SHA1(93e3cfcf7f8b0b36327f402d9a64c04c3b2c7549) )
#ROM_LOAD( "c4",      0x3000, 0x1000, CRC(c63d8f97) SHA1(9f08fd1cd24a1fb4011864c06580985e009d9af4) )
#ROM_LOAD( "c5",      0x4000, 0x1000, CRC(4d593942) SHA1(30cc649a4be3d7f3705f55d8d0dadb0b63d59ec9) )
#ROM_LOAD( "c6",      0x5000, 0x1000, CRC(fb576963) SHA1(5bf5c54a7c12aa55272629c12b414bf49cda0f1f) )
#ROM_LOAD( "c7",      0x6000, 0x1000, CRC(2b8c4511) SHA1(428a48d6b14455d66720a115bc5f35293dc50de7) )
#ROM_LOAD( "c8",      0x7000, 0x1000, CRC(51cc22a7) SHA1(f614368bfee04f084c70bf145801ac46e5631acb) )







dd bs=$((0x1000)) count=1 if=/dev/zero of=carjmbre/c15

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "c15",     0x0000, 0x1000, CRC(7d7779d1) SHA1(f8f5246be4cc9632076d3330fc3d3343b911dfee) )










zxb.py library/b2r1f0_charmapscramble01.zxi --org=$((0x0000))
cat dummy32k.bin >> b2r1f0_charmapscramble01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=b2r1f0_charmapscramble01b.bin
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c9
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c10
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c11
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c12
xxd -p -c 2 -s 0 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c13
xxd -p -c 2 -s 1 -l $((0x2000)) b2r1f0_charmapscramble01b.bin | cut -b 1-2 | xxd -r -p > carjmbre/c14
rm b2r1f0_charmapscramble01.bin b2r1f0_charmapscramble01b.bin

#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c9
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c10
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c11
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c12
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c13
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r1f0_charmapscramble01.bin of=carjmbre/c14

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "c9",     0x0000, 0x1000, CRC(2accb821) SHA1(ce2804536fc1abd3377dc864c8c9976ca28c1b6e) )
#ROM_LOAD( "c10",    0x1000, 0x1000, CRC(75ddbe56) SHA1(5e1363967a822265618793ccb74bf3ef5e0e00b5) )
#ROM_REGION( 0x4000, "gfx2", 0 )
#ROM_LOAD( "c11",    0x0000, 0x1000, CRC(d90cd126) SHA1(7ee110cf19b45ee654016ba0ce92f3db6ea2ed92) )
#ROM_LOAD( "c12",    0x1000, 0x1000, CRC(b3bb39d7) SHA1(89c901be6fae2356ce4d2653e94bf28d6bcf41fe) )
#ROM_LOAD( "c13",    0x2000, 0x1000, CRC(3004010b) SHA1(00d5d2185014159112eb90d8ed50092a3b4ab664) )
#ROM_LOAD( "c14",    0x3000, 0x1000, CRC(fb5f0d31) SHA1(7a27af91efc836bb48c6ed3b283b7c5f7b31c4b5) )








#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=carjmbre/carjmbre.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=carjmbre/carjmbre.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=carjmbre/carjmbre.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=carjmbre/carjmbre.11j
#rm clut01b.bin

dd bs=$((0x0020)) count=1 if=/dev/urandom of=carjmbre/c.d19
dd bs=$((0x0020)) count=1 if=/dev/urandom of=carjmbre/c.d18

#ROM_REGION( 0x0040, "proms", 0 )
#ROM_LOAD( "c.d19",  0x0000, 0x0020, CRC(220bceeb) SHA1(46b9f867d014596e2aa7503f104dc721965f0ed5) )
#ROM_LOAD( "c.d18",  0x0020, 0x0020, CRC(7b9ed1b0) SHA1(ec5e1f56e5a2fc726083866c08ac0e1de0ed6ace) )
#ROM_END







rm smsboot.bin dummy32k.bin example01.bin

zip -r carjmbre carjmbre
rm carjmbre/*
rmdir carjmbre

mame -w -video soft -resolution0 512x512 -rp ./ carjmbre








#ROM_START( carjmbre )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "c1",      0x0000, 0x1000, CRC(62b21739) SHA1(710e5c52f27603aa8f864f6f28d7272f21271d60) )
#ROM_LOAD( "c2",      0x1000, 0x1000, CRC(9ab1a0fa) SHA1(519cf67b98e62b2b42232788ba01ab6637880afc) )
#ROM_LOAD( "c3",      0x2000, 0x1000, CRC(bb29e100) SHA1(93e3cfcf7f8b0b36327f402d9a64c04c3b2c7549) )
#ROM_LOAD( "c4",      0x3000, 0x1000, CRC(c63d8f97) SHA1(9f08fd1cd24a1fb4011864c06580985e009d9af4) )
#ROM_LOAD( "c5",      0x4000, 0x1000, CRC(4d593942) SHA1(30cc649a4be3d7f3705f55d8d0dadb0b63d59ec9) )
#ROM_LOAD( "c6",      0x5000, 0x1000, CRC(fb576963) SHA1(5bf5c54a7c12aa55272629c12b414bf49cda0f1f) )
#ROM_LOAD( "c7",      0x6000, 0x1000, CRC(2b8c4511) SHA1(428a48d6b14455d66720a115bc5f35293dc50de7) )
#ROM_LOAD( "c8",      0x7000, 0x1000, CRC(51cc22a7) SHA1(f614368bfee04f084c70bf145801ac46e5631acb) )

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "c15",     0x0000, 0x1000, CRC(7d7779d1) SHA1(f8f5246be4cc9632076d3330fc3d3343b911dfee) )

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "c9",     0x0000, 0x1000, CRC(2accb821) SHA1(ce2804536fc1abd3377dc864c8c9976ca28c1b6e) )
#ROM_LOAD( "c10",    0x1000, 0x1000, CRC(75ddbe56) SHA1(5e1363967a822265618793ccb74bf3ef5e0e00b5) )
#ROM_REGION( 0x4000, "gfx2", 0 )
#ROM_LOAD( "c11",    0x0000, 0x1000, CRC(d90cd126) SHA1(7ee110cf19b45ee654016ba0ce92f3db6ea2ed92) )
#ROM_LOAD( "c12",    0x1000, 0x1000, CRC(b3bb39d7) SHA1(89c901be6fae2356ce4d2653e94bf28d6bcf41fe) )
#ROM_LOAD( "c13",    0x2000, 0x1000, CRC(3004010b) SHA1(00d5d2185014159112eb90d8ed50092a3b4ab664) )
#ROM_LOAD( "c14",    0x3000, 0x1000, CRC(fb5f0d31) SHA1(7a27af91efc836bb48c6ed3b283b7c5f7b31c4b5) )

#ROM_REGION( 0x0040, "proms", 0 )
#ROM_LOAD( "c.d19",  0x0000, 0x0020, CRC(220bceeb) SHA1(46b9f867d014596e2aa7503f104dc721965f0ed5) )
#ROM_LOAD( "c.d18",  0x0020, 0x0020, CRC(7b9ed1b0) SHA1(ec5e1f56e5a2fc726083866c08ac0e1de0ed6ace) )
#ROM_END




