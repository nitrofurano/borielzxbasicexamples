# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm onetwo.zip example01.asm
rm onetwo/*
rmdir onetwo
mkdir onetwo

dd bs=$((0x200000)) count=1 if=/dev/zero of=dummy2m.bin

#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=0x0069
zxb.py example01.zxb --heap-size=128 --org=0x0069
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy2m.bin >> example01.bin
dd ibs=1 count=$((0x20000)) skip=$((0x0000)) if=example01.bin of=onetwo/maincpu
rm example01.bin smsboot.bin

#sound
dd bs=$((0x10000)) count=1 if=/dev/zero of=onetwo/sound_prog

#gfx
#dd bs=$((0x80000)) count=1 if=/dev/urandom of=onetwo/3_graphics
#dd bs=$((0x80000)) count=1 if=/dev/urandom of=onetwo/4_graphics
#dd bs=$((0x80000)) count=1 if=/dev/urandom of=onetwo/5_graphics
zxb.py library/charmaponeplustwo01.bas --org=$((0x0000))
cat dummy2m.bin >> charmaponeplustwo01.bin
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=charmaponeplustwo01.bin of=onetwo/3_graphics
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=charmaponeplustwo01.bin of=onetwo/4_graphics
#dd ibs=1 count=$((0x80000)) skip=$((0x0010)) if=charmaponeplustwo01.bin of=onetwo/5_graphics

dd ibs=1 count=$((0x180000)) skip=$((0x0010)) if=charmaponeplustwo01.bin of=charmaponeplustwo01b.bin
rm charmaponeplustwo01.bin

xxd -p -c 3 -s 0 -l $((0x180000)) charmaponeplustwo01b.bin | cut -b 1-2 | xxd -r -p > onetwo/5_graphics
xxd -p -c 3 -s 1 -l $((0x180000)) charmaponeplustwo01b.bin | cut -b 1-2 | xxd -r -p > onetwo/3_graphics
xxd -p -c 3 -s 2 -l $((0x180000)) charmaponeplustwo01b.bin | cut -b 1-2 | xxd -r -p > onetwo/4_graphics

rm charmaponeplustwo01b.bin

#sound
dd bs=$((0x40000)) count=1 if=/dev/zero of=onetwo/sample

rm dummy2m.bin
zip -r onetwo onetwo
rm onetwo/*
rmdir onetwo

mame -w -video soft -resolution0 512x512 -rp ./ onetwo
#xmame.SDL -rp ./ onetwo

