# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm ksayakyu.zip
rm ksayakyu/*
rmdir ksayakyu
mkdir ksayakyu




dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin




#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin of=ksayakyu/1.3a
dd ibs=1 count=$((0x4000)) skip=$((0x4000)) if=example01.bin of=ksayakyu/2.3b
dd bs=$((0x4000)) count=1 if=/dev/zero of=ksayakyu/4.3d
dd bs=$((0x4000)) count=1 if=/dev/zero of=ksayakyu/3.3c
rm dummy64k.bin example01.bin

#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "1.3a", 0x00000, 0x4000, CRC(6607976d) SHA1(23b15cae04922e54faf35a5a499ba5064b18ed46) )
#ROM_LOAD( "2.3b", 0x04000, 0x4000, CRC(a289de5c) SHA1(d3b14364ef77ca74ac79c5099cf0e8f3baa97612) )
#ROM_RELOAD(       0x10000, 0x4000 )
#ROM_LOAD( "4.3d", 0x08000, 0x2000, CRC(db0ca023) SHA1(1356fa0239209dea6a4ac0af177fe8be47f12cd0) )
#ROM_LOAD( "3.3c", 0x14000, 0x4000, CRC(bb4104a5) SHA1(1f793c4431a3476eeb92556228bf855efb73fb83) )









#mcu
dd bs=$((0x2000)) count=1 if=/dev/zero of=ksayakyu/8.5l
dd bs=$((0x2000)) count=1 if=/dev/zero of=ksayakyu/7.5j
dd bs=$((0x2000)) count=1 if=/dev/zero of=ksayakyu/6.5h
dd bs=$((0x2000)) count=1 if=/dev/zero of=ksayakyu/5.5f

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "8.5l",  0x0000, 0x2000, CRC(3fbae535) SHA1(87b66091e33eee66f4d63c213a5a19b70e7b1e5a) )
#ROM_LOAD( "7.5j",  0x2000, 0x2000, CRC(ad242eda) SHA1(928c04e4a48077ec9db148cb71d52ebfe01efa53) )
#ROM_LOAD( "6.5h",  0x4000, 0x2000, CRC(17986662) SHA1(81b65e381b923c5544f4708efef09f0894c716b2) )
#ROM_LOAD( "5.5f",  0x6000, 0x2000, CRC(b0b21817) SHA1(da2a1a6865dbc335775fa2e0ad0fb899be95af03) )






#gfx
zxb.py library/b3r2f1_charmapkusayakyuu01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b3r2f1_charmapkusayakyuu01.bin of=b3r2f1_charmapkusayakyuu01b.bin

#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/9.3j
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/10.3k
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/11.3l

#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/14.9j
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/15.9k
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/16.9m

#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/17.9n
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=b3r2f1_charmapkusayakyuu01b.bin of=ksayakyu/18.9r

xxd -p -c 3 -s 0 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/9.3j
xxd -p -c 3 -s 1 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/10.3k
xxd -p -c 3 -s 2 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/11.3l

xxd -p -c 3 -s 0 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/14.9j
xxd -p -c 3 -s 1 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/15.9k
xxd -p -c 3 -s 2 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/16.9m

xxd -p -c 3 -s 0 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/17.9n
xxd -p -c 3 -s 1 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/18.9r
xxd -p -c 3 -s 2 -l $((0x6000)) b3r2f1_charmapkusayakyuu01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/18.9r


#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=ksayakyu/ksayakyu.5f
#rm spritemap01.bin

rm b3r2f1_charmapkusayakyuu01.bin b3r2f1_charmapkusayakyuu01b.bin

#ROM_REGION( 0x6000, "gfx1", 0  )
#ROM_LOAD( "9.3j",  0x0000, 0x2000, CRC(ef8411dd) SHA1(1dbc959d3aec9face19b2a5ae873ca34bfeff5cd) )
#ROM_LOAD( "10.3k", 0x2000, 0x2000, CRC(1bdee573) SHA1(7b92a8133cb83404505d21f462e3ca6c85647dca) )
#ROM_LOAD( "11.3l", 0x4000, 0x2000, CRC(c5859887) SHA1(41685fb8f8e7c44acd5e0e3ccc629e5f64a59fbd) )

#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "14.9j", 0x0000, 0x2000, CRC(982d06f0) SHA1(e107c56ee4f2695a790b8cec6d52337ba9d8b2ad) )
#ROM_LOAD( "15.9k", 0x2000, 0x2000, CRC(dc126df9) SHA1(368efb36bf197e3eac23ef543e25e9f4efba785e) )
#ROM_LOAD( "16.9m", 0x4000, 0x2000, CRC(574a172d) SHA1(7dee073b5c8ff5825062e30cff29343dc767daaa) )

#ROM_REGION( 0x4000, "gfx3", 0 )
#ROM_LOAD( "17.9n", 0x0000, 0x2000, CRC(a4c4e4ce) SHA1(f8b0a8dfab972e23f268c69fd9ef30fc80f62533) )
#ROM_LOAD( "18.9r", 0x2000, 0x2000, CRC(9d75b104) SHA1(062884fdca9f705f555b828aff136d8f52fbf6eb) )









#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > ksayakyu/ao4-12.ic95
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=ksayakyu/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=ksayakyu/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=ksayakyu/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=ksayakyu/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=ksayakyu/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=ksayakyu/82s126.4a
#rm clut01.bin

dd bs=$((0x2000)) count=1 if=/dev/urandom of=ksayakyu/13.7r
dd bs=$((0x2000)) count=1 if=/dev/urandom of=ksayakyu/12.7n
dd bs=$((0x0100)) count=1 if=/dev/urandom of=ksayakyu/9f.bin


#ROM_REGION( 0x4000, "user1", 0 )
#ROM_LOAD( "13.7r", 0x0000, 0x2000, CRC(0acb8c61) SHA1(0ca3b07b21501d4c8feda2b4295f534da2a1c745) )
#ROM_LOAD( "12.7n", 0x2000, 0x2000, CRC(ae8d6ed2) SHA1(92d6484040a9b82f77b92f3dd452333fd618eb0d) )

#ROM_REGION( 0x100, "proms", 0 )
#ROM_LOAD( "9f.bin", 0x0000, 0x0100, CRC(ff71b27f) SHA1(6aad2bd2be997595a05ddb81d24df8fe1435910b) )





rm smsboot.bin

zip -r ksayakyu ksayakyu
rm ksayakyu/*
rmdir ksayakyu

mame -w -video soft -resolution0 512x512 -rp ./ ksayakyu
#xmame.SDL -rp ./ ksayakyu







#ROM_START( ksayakyu )
#ROM_REGION( 0x20000, "maincpu", 0 )
#ROM_LOAD( "1.3a", 0x00000, 0x4000, CRC(6607976d) SHA1(23b15cae04922e54faf35a5a499ba5064b18ed46) )
#ROM_LOAD( "2.3b", 0x04000, 0x4000, CRC(a289de5c) SHA1(d3b14364ef77ca74ac79c5099cf0e8f3baa97612) )
#ROM_RELOAD(       0x10000, 0x4000 )
#ROM_LOAD( "4.3d", 0x08000, 0x2000, CRC(db0ca023) SHA1(1356fa0239209dea6a4ac0af177fe8be47f12cd0) )
#ROM_LOAD( "3.3c", 0x14000, 0x4000, CRC(bb4104a5) SHA1(1f793c4431a3476eeb92556228bf855efb73fb83) )

#ROM_REGION( 0x10000, "audiocpu", 0 )
#ROM_LOAD( "8.5l",  0x0000, 0x2000, CRC(3fbae535) SHA1(87b66091e33eee66f4d63c213a5a19b70e7b1e5a) )
#ROM_LOAD( "7.5j",  0x2000, 0x2000, CRC(ad242eda) SHA1(928c04e4a48077ec9db148cb71d52ebfe01efa53) )
#ROM_LOAD( "6.5h",  0x4000, 0x2000, CRC(17986662) SHA1(81b65e381b923c5544f4708efef09f0894c716b2) )
#ROM_LOAD( "5.5f",  0x6000, 0x2000, CRC(b0b21817) SHA1(da2a1a6865dbc335775fa2e0ad0fb899be95af03) )

#ROM_REGION( 0x6000, "gfx1", 0  )
#ROM_LOAD( "9.3j",  0x0000, 0x2000, CRC(ef8411dd) SHA1(1dbc959d3aec9face19b2a5ae873ca34bfeff5cd) )
#ROM_LOAD( "10.3k", 0x2000, 0x2000, CRC(1bdee573) SHA1(7b92a8133cb83404505d21f462e3ca6c85647dca) )
#ROM_LOAD( "11.3l", 0x4000, 0x2000, CRC(c5859887) SHA1(41685fb8f8e7c44acd5e0e3ccc629e5f64a59fbd) )

#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "14.9j", 0x0000, 0x2000, CRC(982d06f0) SHA1(e107c56ee4f2695a790b8cec6d52337ba9d8b2ad) )
#ROM_LOAD( "15.9k", 0x2000, 0x2000, CRC(dc126df9) SHA1(368efb36bf197e3eac23ef543e25e9f4efba785e) )
#ROM_LOAD( "16.9m", 0x4000, 0x2000, CRC(574a172d) SHA1(7dee073b5c8ff5825062e30cff29343dc767daaa) )

#ROM_REGION( 0x4000, "gfx3", 0 )
#ROM_LOAD( "17.9n", 0x0000, 0x2000, CRC(a4c4e4ce) SHA1(f8b0a8dfab972e23f268c69fd9ef30fc80f62533) )
#ROM_LOAD( "18.9r", 0x2000, 0x2000, CRC(9d75b104) SHA1(062884fdca9f705f555b828aff136d8f52fbf6eb) )

#ROM_REGION( 0x4000, "user1", 0 )
#ROM_LOAD( "13.7r", 0x0000, 0x2000, CRC(0acb8c61) SHA1(0ca3b07b21501d4c8feda2b4295f534da2a1c745) )
#ROM_LOAD( "12.7n", 0x2000, 0x2000, CRC(ae8d6ed2) SHA1(92d6484040a9b82f77b92f3dd452333fd618eb0d) )

#ROM_REGION( 0x100, "proms", 0 )
#ROM_LOAD( "9f.bin", 0x0000, 0x0100, CRC(ff71b27f) SHA1(6aad2bd2be997595a05ddb81d24df8fe1435910b) )
#ROM_END

