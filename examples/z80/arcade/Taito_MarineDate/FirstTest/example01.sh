# rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin

rm example01.asm
rm marinedt.zip
rm marinedt/*
rmdir marinedt
mkdir marinedt





dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin



#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=marinedt/mg01
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=marinedt/mg02
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=marinedt/mg03
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=marinedt/mg04
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=marinedt/mg05
dd ibs=1 count=$((0x800)) skip=$((0x2800)) if=example01.bin of=marinedt/mg06
dd ibs=1 count=$((0x800)) skip=$((0x3000)) if=example01.bin of=marinedt/mg07
dd bs=$((0x2000)) count=1 if=/dev/zero of=marinedt/ao4_05.ic3
rm dummy64k.bin example01.bin

#ROM_START( marinedt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "mg01",     0x0000, 0x0800, CRC(ad09f04d) SHA1(932fc973b4a2fbbebd7e6437ed30c8444e3d4afb))
#ROM_LOAD( "mg02",     0x0800, 0x0800, CRC(555a2b0f) SHA1(143a8953ce5070c31dc4c1f623833b2a5a2cf657))
#ROM_LOAD( "mg03",     0x1000, 0x0800, CRC(2abc79b3) SHA1(1afb331a2c0e320b6d026bc5cb47a53ac3356c2a))
#ROM_LOAD( "mg04",     0x1800, 0x0800, CRC(be928364) SHA1(8d9ae71e2751c009187e41d84fbad9519ab551e1) )
#ROM_LOAD( "mg05",     0x2000, 0x0800, CRC(44cd114a) SHA1(833165c5c00c6e505acf29fef4a3ae3f9647b443) )
#ROM_LOAD( "mg06",     0x2800, 0x0800, CRC(a7e2c69b) SHA1(614fc479d13c1726382fe7b4b0379c1dd4915af0) )
#ROM_LOAD( "mg07",     0x3000, 0x0800, CRC(b85d1f9a) SHA1(4fd3e76b1816912df84477dba4655d395f5e7072) )






#mcu
#dd bs=$((0x0800)) count=1 if=/dev/zero of=marinedt/ao4_06.ic23





#gfx

#zxb.py library/b1r1f1_charmapbeaminvader01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r1f1_charmapbeaminvader01.bin of=b1r1f1_charmapbeaminvader01b.bin
#dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=marinedt/mg09
#dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=marinedt/mg10
#dd ibs=1 count=$((0x0800)) skip=$((0x0000)) if=b1r1f1_charmapbeaminvader01b.bin of=marinedt/mg11

zxb.py library/b3r1f1_charmaptankbusters01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b3r1f1_charmaptankbusters01.bin of=b3r1f1_charmaptankbusters01b.bin
xxd -p -c 3 -s 0 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > marinedt/mg09
xxd -p -c 3 -s 1 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > marinedt/mg10
xxd -p -c 3 -s 2 -l $((0x1800)) b3r1f1_charmaptankbusters01b.bin | cut -b 1-2 | xxd -r -p > marinedt/mg11




#gfx
zxb.py library/b1r3f0_charmapspaceintruder.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b1r3f0_charmapspaceintruder.bin of=b1r3f0_charmapspaceintruderb.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=b1r3f0_charmapspaceintruderb.bin of=marinedt/mg12
dd ibs=1 count=$((0x1000)) skip=$((0x0000)) if=b1r3f0_charmapspaceintruderb.bin of=marinedt/mg13


#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 b1r3f0_charmapspaceintruderb.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r3f0_charmapspaceintruderb.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 b1r3f0_charmapspaceintruderb.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 b1r3f0_charmapspaceintruderb.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4_07.ic15 #-bitdepth1

rm b1r3f0_charmapspaceintruder.bin b1r3f0_charmapspaceintruderb.bin
#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=marinedt/marinedt.5f
#rm spritemap01.bin

#ROM_REGION( 0x1800, "gfx1", 0 )
#ROM_LOAD( "mg09",     0x0000, 0x0800, CRC(f4c349ca) SHA1(077f65eeac616a778d6c42bb95677fa2892ab697) )
#ROM_LOAD( "mg10",     0x0800, 0x0800, CRC(b41251e3) SHA1(e125a971b401c78efeb4b03d0fab43e392d3fc14) )
#ROM_LOAD( "mg11",     0x1000, 0x0800, CRC(50d66dd7) SHA1(858d1d2a75e091b0e382d964c5e4ddcd8e6f07dd))
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "mg12",     0x0000, 0x1000, CRC(7c6486d5) SHA1(a7f17a803937937f05fc90621883a0fd44b297a0) )
#ROM_REGION( 0x1000, "gfx3", 0 )
#ROM_LOAD( "mg13",     0x0000, 0x1000, CRC(17817044) SHA1(8c9b96620e3c414952e6d85c6e81b0df85c88e7a) )










#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > marinedt/ao4-12.ic95
#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=marinedt/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=marinedt/ao4-12.ic95
#rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=marinedt/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=marinedt/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=marinedt/82s123.7f
#rm palette01.bin

zxb.py library/palette01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=marinedt/mg14.bpr
dd ibs=1 count=$((0x0020)) skip=$((0x0020)) if=palette01b.bin of=marinedt/mg15.bpr
rm clut01.bin





#dd bs=$((0x0020)) count=1 if=/dev/urandom of=marinedt/mg14.bpr
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=marinedt/mg15.bpr

dd bs=$((0x0020)) count=1 if=/dev/urandom of=marinedt/mg16.bpr
dd bs=$((0x0020)) count=1 if=/dev/urandom of=marinedt/mg17.bpr


#ROM_REGION( 0x0080, "proms", 0 )
#ROM_LOAD( "mg14.bpr", 0x0000, 0x0020, CRC(f75f4e3a) SHA1(36e665987f475c57435fa8c224a2a3ce0c5e672b) )    //char clr
#ROM_LOAD( "mg15.bpr", 0x0020, 0x0020, CRC(cd3ab489) SHA1(a77478fb94d0cf8f4317f89cc9579def7c294b4f) )    //obj clr
#ROM_LOAD( "mg16.bpr", 0x0040, 0x0020, CRC(92c868bc) SHA1(483ae6f47845ddacb701528e82bd388d7d66a0fb) )    //?? collisions
#ROM_LOAD( "mg17.bpr", 0x0060, 0x0020, CRC(13261a02) SHA1(050edd18e4f79d19d5206f55f329340432fd4099) )    //?? table of increasing values





rm b3r1f1_charmaptankbusters01b.bin b3r1f1_charmaptankbusters01.bin
rm b1r1f1_charmapbeaminvader01.bin b1r1f1_charmapbeaminvader01b.bin
rm palette01.bin palette01b.bin

rm smsboot.bin

zip -r marinedt marinedt
rm marinedt/*
rmdir marinedt

mame -w -video soft -resolution0 512x512 -rp ./ marinedt






#ROM_START( marinedt )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "mg01",     0x0000, 0x0800, CRC(ad09f04d) SHA1(932fc973b4a2fbbebd7e6437ed30c8444e3d4afb))
#ROM_LOAD( "mg02",     0x0800, 0x0800, CRC(555a2b0f) SHA1(143a8953ce5070c31dc4c1f623833b2a5a2cf657))
#ROM_LOAD( "mg03",     0x1000, 0x0800, CRC(2abc79b3) SHA1(1afb331a2c0e320b6d026bc5cb47a53ac3356c2a))
#ROM_LOAD( "mg04",     0x1800, 0x0800, CRC(be928364) SHA1(8d9ae71e2751c009187e41d84fbad9519ab551e1) )
#ROM_LOAD( "mg05",     0x2000, 0x0800, CRC(44cd114a) SHA1(833165c5c00c6e505acf29fef4a3ae3f9647b443) )
#ROM_LOAD( "mg06",     0x2800, 0x0800, CRC(a7e2c69b) SHA1(614fc479d13c1726382fe7b4b0379c1dd4915af0) )
#ROM_LOAD( "mg07",     0x3000, 0x0800, CRC(b85d1f9a) SHA1(4fd3e76b1816912df84477dba4655d395f5e7072) )

#ROM_REGION( 0x1800, "gfx1", 0 )
#ROM_LOAD( "mg09",     0x0000, 0x0800, CRC(f4c349ca) SHA1(077f65eeac616a778d6c42bb95677fa2892ab697) )
#ROM_LOAD( "mg10",     0x0800, 0x0800, CRC(b41251e3) SHA1(e125a971b401c78efeb4b03d0fab43e392d3fc14) )
#ROM_LOAD( "mg11",     0x1000, 0x0800, CRC(50d66dd7) SHA1(858d1d2a75e091b0e382d964c5e4ddcd8e6f07dd))
#ROM_REGION( 0x1000, "gfx2", 0 )
#ROM_LOAD( "mg12",     0x0000, 0x1000, CRC(7c6486d5) SHA1(a7f17a803937937f05fc90621883a0fd44b297a0) )
#ROM_REGION( 0x1000, "gfx3", 0 )
#ROM_LOAD( "mg13",     0x0000, 0x1000, CRC(17817044) SHA1(8c9b96620e3c414952e6d85c6e81b0df85c88e7a) )

#ROM_REGION( 0x0080, "proms", 0 )
#ROM_LOAD( "mg14.bpr", 0x0000, 0x0020, CRC(f75f4e3a) SHA1(36e665987f475c57435fa8c224a2a3ce0c5e672b) )    //char clr
#ROM_LOAD( "mg15.bpr", 0x0020, 0x0020, CRC(cd3ab489) SHA1(a77478fb94d0cf8f4317f89cc9579def7c294b4f) )    //obj clr
#ROM_LOAD( "mg16.bpr", 0x0040, 0x0020, CRC(92c868bc) SHA1(483ae6f47845ddacb701528e82bd388d7d66a0fb) )    //?? collisions
#ROM_LOAD( "mg17.bpr", 0x0060, 0x0020, CRC(13261a02) SHA1(050edd18e4f79d19d5206f55f329340432fd4099) )    //?? table of increasing values
#ROM_END





