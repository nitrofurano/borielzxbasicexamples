rm markham.zip
rm example01.asm
rm markham/*
rmdir markham
mkdir markham





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=markham/tv3.9
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=markham/tvg4.10
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=markham/tvg5.11

#ROM_START( markham )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* main CPU */
#ROM_LOAD( "tv3.9",   0x0000,  0x2000, CRC(59391637) SHA1(e0cfe49a5591d6a6e64c3277319a19235b0ee6ea) )
#ROM_LOAD( "tvg4.10", 0x2000,  0x2000, CRC(1837bcce) SHA1(50e1ae0a4937f09a3dced48bb12f57cee846487a) )
#ROM_LOAD( "tvg5.11", 0x4000,  0x2000, CRC(651da602) SHA1(9f33d6ea0526af9be8ac9210910ea768da825ee5) )






dd bs=$((0x2000)) count=1 if=/dev/zero of=markham/tvg1.5
dd bs=$((0x2000)) count=1 if=/dev/zero of=markham/tvg2.6

#ROM_REGION( 0x10000, "sub", 0 ) /* sub CPU */
#ROM_LOAD( "tvg1.5",  0x0000,  0x2000, CRC(c5299766) SHA1(a6c903088ffd6c5ae0ba7ff50c8509a185f88220) )
#ROM_LOAD( "tvg2.6",  0x4000,  0x2000, CRC(b216300a) SHA1(036fafd0277b3422cf491db77748358da1ecfb43) )







zxb.py library/b3r0f1_charmapappoooh01.zxi --org=$((0x0000))
cat dummy32k.bin >> b3r0f1_charmapappoooh01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f1_charmapappoooh01.bin of=b3r0f1_charmapappoooh01b.bin
xxd -p -c 3 -s 0 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg6.84
xxd -p -c 3 -s 1 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg7.85
xxd -p -c 3 -s 2 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg8.86
xxd -p -c 3 -s 0 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg9.87
xxd -p -c 3 -s 1 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg10.88
xxd -p -c 3 -s 2 -l $((0x6000)) b3r0f1_charmapappoooh01b.bin | cut -b 1-2 | xxd -r -p > markham/tvg11.89
rm b3r0f1_charmapappoooh01.bin b3r0f1_charmapappoooh01b.bin

#ROM_REGION( 0x6000, "gfx1", 0 ) /* sprite */
#ROM_LOAD( "tvg6.84", 0x0000,  0x2000, CRC(ab933ae5) SHA1(d2bdbc35d751480ddf8b89b90063510684b00db2) )
#ROM_LOAD( "tvg7.85", 0x2000,  0x2000, CRC(ce8edda7) SHA1(5312754aec20791398de57f08857d4097a7cfc2c) )
#ROM_LOAD( "tvg8.86", 0x4000,  0x2000, CRC(74d1536a) SHA1(ff2efbbe1420282643558a65bfa5fd278cdaf135) )
#ROM_REGION( 0x6000, "gfx2", 0 ) /* bg */
#ROM_LOAD( "tvg9.87",  0x0000,  0x2000, CRC(42168675) SHA1(d2cce79a05ca7fda9347630fe0045a2d8182025d) )
#ROM_LOAD( "tvg10.88", 0x2000,  0x2000, CRC(fa9feb67) SHA1(669c6e1defc33541c36d4deb9667b67254f53a37) )
#ROM_LOAD( "tvg11.89", 0x4000,  0x2000, CRC(71f3dd49) SHA1(8fecb6b76907c592d545dafeaa47cf765513b3fe) )











#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=markham/markham.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=markham/markham.3j
#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=markham/markham.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=markham/markham.11j
#rm clut01b.bin
#????
#dd bs=256 count=1 if=/dev/zero of=markham/82s126.3m

dd bs=$((0x0100)) count=1 if=/dev/urandom of=markham/14-3.99
dd bs=$((0x0100)) count=1 if=/dev/urandom of=markham/14-4.100
dd bs=$((0x0100)) count=1 if=/dev/urandom of=markham/14-5.101
dd bs=$((0x0200)) count=1 if=/dev/urandom of=markham/14-1.61
dd bs=$((0x0200)) count=1 if=/dev/urandom of=markham/14-2.115
#ROM_REGION( 0x0700, "proms", 0 ) /* color PROMs */
#ROM_LOAD( "14-3.99",  0x0000,  0x0100, CRC(89d09126) SHA1(1f78f3b3ef8c6ba9c00a58ae89837d9a92e5078f) ) /* R */
#ROM_LOAD( "14-4.100", 0x0100,  0x0100, CRC(e1cafe6c) SHA1(8c37c3829bf1b96690fb853a2436f1b5e8d45e8c) ) /* G */
#ROM_LOAD( "14-5.101", 0x0200,  0x0100, CRC(2d444fa6) SHA1(66b64133ca740686bedd33bafd20a3f9f3df97d4) ) /* B */
#ROM_LOAD( "14-1.61",  0x0300,  0x0200, CRC(3ad8306d) SHA1(877f1d58cb8da9098ec71a7c7aec633dbf9e76e6) ) /* sprite */
#ROM_LOAD( "14-2.115", 0x0500,  0x0200, CRC(12a4f1ff) SHA1(375e37d7162053d45da66eee23d66bd432303c1c) ) /* bg */
#ROM_END





rm smsboot.bin dummy32k.bin example01.bin
zip -r markham markham
rm markham/*
rmdir markham
mame -w -video soft -resolution0 512x512 -rp ./ markham









#ROM_START( markham )
#ROM_REGION( 0x10000, "maincpu", 0 ) /* main CPU */
#ROM_LOAD( "tv3.9",   0x0000,  0x2000, CRC(59391637) SHA1(e0cfe49a5591d6a6e64c3277319a19235b0ee6ea) )
#ROM_LOAD( "tvg4.10", 0x2000,  0x2000, CRC(1837bcce) SHA1(50e1ae0a4937f09a3dced48bb12f57cee846487a) )
#ROM_LOAD( "tvg5.11", 0x4000,  0x2000, CRC(651da602) SHA1(9f33d6ea0526af9be8ac9210910ea768da825ee5) )

#ROM_REGION( 0x10000, "sub", 0 ) /* sub CPU */
#ROM_LOAD( "tvg1.5",  0x0000,  0x2000, CRC(c5299766) SHA1(a6c903088ffd6c5ae0ba7ff50c8509a185f88220) )
#ROM_LOAD( "tvg2.6",  0x4000,  0x2000, CRC(b216300a) SHA1(036fafd0277b3422cf491db77748358da1ecfb43) )

#ROM_REGION( 0x6000, "gfx1", 0 ) /* sprite */
#ROM_LOAD( "tvg6.84", 0x0000,  0x2000, CRC(ab933ae5) SHA1(d2bdbc35d751480ddf8b89b90063510684b00db2) )
#ROM_LOAD( "tvg7.85", 0x2000,  0x2000, CRC(ce8edda7) SHA1(5312754aec20791398de57f08857d4097a7cfc2c) )
#ROM_LOAD( "tvg8.86", 0x4000,  0x2000, CRC(74d1536a) SHA1(ff2efbbe1420282643558a65bfa5fd278cdaf135) )
#ROM_REGION( 0x6000, "gfx2", 0 ) /* bg */
#ROM_LOAD( "tvg9.87",  0x0000,  0x2000, CRC(42168675) SHA1(d2cce79a05ca7fda9347630fe0045a2d8182025d) )
#ROM_LOAD( "tvg10.88", 0x2000,  0x2000, CRC(fa9feb67) SHA1(669c6e1defc33541c36d4deb9667b67254f53a37) )
#ROM_LOAD( "tvg11.89", 0x4000,  0x2000, CRC(71f3dd49) SHA1(8fecb6b76907c592d545dafeaa47cf765513b3fe) )

#ROM_REGION( 0x0700, "proms", 0 ) /* color PROMs */
#ROM_LOAD( "14-3.99",  0x0000,  0x0100, CRC(89d09126) SHA1(1f78f3b3ef8c6ba9c00a58ae89837d9a92e5078f) ) /* R */
#ROM_LOAD( "14-4.100", 0x0100,  0x0100, CRC(e1cafe6c) SHA1(8c37c3829bf1b96690fb853a2436f1b5e8d45e8c) ) /* G */
#ROM_LOAD( "14-5.101", 0x0200,  0x0100, CRC(2d444fa6) SHA1(66b64133ca740686bedd33bafd20a3f9f3df97d4) ) /* B */
#ROM_LOAD( "14-1.61",  0x0300,  0x0200, CRC(3ad8306d) SHA1(877f1d58cb8da9098ec71a7c7aec633dbf9e76e6) ) /* sprite */
#ROM_LOAD( "14-2.115", 0x0500,  0x0200, CRC(12a4f1ff) SHA1(375e37d7162053d45da66eee23d66bd432303c1c) ) /* bg */
#ROM_END






