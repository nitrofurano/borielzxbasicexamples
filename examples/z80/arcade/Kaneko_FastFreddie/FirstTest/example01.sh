rm boggy84.zip
rm example01.asm
rm boggy84/*
rmdir boggy84
mkdir boggy84





dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin





#cpu

#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy128k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=boggy84/p1.d1
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=boggy84/p2.d2
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=boggy84/p3.d3
dd ibs=1 count=$((0x2000)) skip=$((0x6000)) if=example01.bin of=boggy84/p4.d4

#- ROM_START( boggy84 )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "p1.d1", 0x0000, 0x2000, CRC(722cc0ec) SHA1(7daacffd9dfcbc8a441485943e45cc8958d19167) )
#- ROM_LOAD( "p2.d2", 0x2000, 0x2000, CRC(6c096798) SHA1(74ea860ef10cb566bcb07d67e6c79f542a66de91) )
#- ROM_LOAD( "p3.d3", 0x4000, 0x2000, CRC(9da59104) SHA1(167af18d50d99e66111e4ebd52d0dd86d5d6d391) )
#- ROM_LOAD( "p4.d4", 0x6000, 0x2000, CRC(73ef6807) SHA1(3144285019ab5cc7f2e1ba0a31956964ea1c706c) )






#gfx

zxb.py library/b3r1f0_charmapnunchacken01.zxi --org=$((0x0000))
cat dummy128k.bin >> b3r1f0_charmapnunchacken01.bin
dd ibs=1 count=$((0x3000)) skip=$((0x0010)) if=b3r1f0_charmapnunchacken01.bin of=b3r1f0_charmapnunchacken01b.bin

#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01b.bin of=boggy84/g1.h10
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01b.bin of=boggy84/g2.h11
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b1r1f0_charmapkamikaze01b.bin of=boggy84/g3.h12

xxd -p -c 3 -s 0 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > boggy84/g1.h10
xxd -p -c 3 -s 1 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > boggy84/g2.h11
xxd -p -c 3 -s 2 -l $((0x3000)) b3r1f0_charmapnunchacken01b.bin | cut -b 1-2 | xxd -r -p > boggy84/g3.h12
rm b3r1f0_charmapnunchacken01.bin b3r1f0_charmapnunchacken01b.bin




#dd bs=$((0x1000)) count=1 if=/dev/urandom of=boggy84/g1.h10
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=boggy84/g2.h11
#dd bs=$((0x1000)) count=1 if=/dev/urandom of=boggy84/g3.h12

#- ROM_REGION( 0x3000, "gfx1", 0 )
#- ROM_LOAD( "g1.h10", 0x0000, 0x1000, CRC(f4238c68) SHA1(a14cedb126e49e40bab6f46870af64c04ccb01f4) )
#- ROM_LOAD( "g2.h11", 0x1000, 0x1000, CRC(ce285bd2) SHA1(61e58920553f56448e76d859c1b0f316f299363f) )
#- ROM_LOAD( "g3.h12", 0x2000, 0x1000, CRC(02f5f4fa) SHA1(d28dc23cd3a39bb483d05b59869ed2300e5e77a7) )







#proms

zxb.py library/palette01.bas --org=$((0x0000))
cat dummy128k.bin >> palette01.bin
dd ibs=1 count=$((0x0300)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin

xxd -p -c 3 -s 0 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > boggy84/r.e10
xxd -p -c 3 -s 1 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > boggy84/g.e11
xxd -p -c 3 -s 2 -l $((0x0300)) palette01b.bin | cut -b 1-2 | xxd -r -p > boggy84/b.e12

#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=boggy84/boggy84.3j
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=boggy84/boggy84.3j
#dd bs=$((0x100)) count=1 if=/dev/urandom of=boggy84/r.e10
#dd bs=$((0x100)) count=1 if=/dev/urandom of=boggy84/g.e11
#dd bs=$((0x100)) count=1 if=/dev/urandom of=boggy84/b.e12

rm palette01.bin palette01b.bin

#- ROM_REGION( 0x0300, "proms", 0 )
#- ROM_LOAD( "r.e10", 0x0000, 0x0100, CRC(f3862912) SHA1(128ba48202299ef5852f08fd0f910d8e9f68f22c) )
#- ROM_LOAD( "g.e11", 0x0100, 0x0100, CRC(80b87220) SHA1(7bd81060b986d5cd4a27dc8a9394423959deaa05) )
#- ROM_LOAD( "b.e12", 0x0200, 0x0100, CRC(52b7f445) SHA1(6395ac705a35e602a355cbf700025ff917e89b37) )
#- ROM_END






rm smsboot.bin dummy128k.bin example01.bin

zip -r boggy84 boggy84
rm boggy84/*
rmdir boggy84

mame -w -video soft -resolution0 512x512 -rp ./ boggy84


#- ROM_START( boggy84 )
#- ROM_REGION( 0x10000, "maincpu", 0 )
#- ROM_LOAD( "p1.d1", 0x0000, 0x2000, CRC(722cc0ec) SHA1(7daacffd9dfcbc8a441485943e45cc8958d19167) )
#- ROM_LOAD( "p2.d2", 0x2000, 0x2000, CRC(6c096798) SHA1(74ea860ef10cb566bcb07d67e6c79f542a66de91) )
#- ROM_LOAD( "p3.d3", 0x4000, 0x2000, CRC(9da59104) SHA1(167af18d50d99e66111e4ebd52d0dd86d5d6d391) )
#- ROM_LOAD( "p4.d4", 0x6000, 0x2000, CRC(73ef6807) SHA1(3144285019ab5cc7f2e1ba0a31956964ea1c706c) )

#- ROM_REGION( 0x3000, "gfx1", 0 )
#- ROM_LOAD( "g1.h10", 0x0000, 0x1000, CRC(f4238c68) SHA1(a14cedb126e49e40bab6f46870af64c04ccb01f4) )
#- ROM_LOAD( "g2.h11", 0x1000, 0x1000, CRC(ce285bd2) SHA1(61e58920553f56448e76d859c1b0f316f299363f) )
#- ROM_LOAD( "g3.h12", 0x2000, 0x1000, CRC(02f5f4fa) SHA1(d28dc23cd3a39bb483d05b59869ed2300e5e77a7) )

#- ROM_REGION( 0x0300, "proms", 0 )
#- ROM_LOAD( "r.e10", 0x0000, 0x0100, CRC(f3862912) SHA1(128ba48202299ef5852f08fd0f910d8e9f68f22c) )
#- ROM_LOAD( "g.e11", 0x0100, 0x0100, CRC(80b87220) SHA1(7bd81060b986d5cd4a27dc8a9394423959deaa05) )
#- ROM_LOAD( "b.e12", 0x0200, 0x0100, CRC(52b7f445) SHA1(6395ac705a35e602a355cbf700025ff917e89b37) )
#- ROM_END

















