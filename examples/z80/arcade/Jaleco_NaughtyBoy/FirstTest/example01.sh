rm example01.asm
rm naughtyb.zip
rm naughtyb/*
rmdir naughtyb
mkdir naughtyb
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin







#maincpu
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x800)) skip=$((0x0000)) if=example01.bin of=naughtyb/1.30
dd ibs=1 count=$((0x800)) skip=$((0x0800)) if=example01.bin of=naughtyb/2.29
dd ibs=1 count=$((0x800)) skip=$((0x1000)) if=example01.bin of=naughtyb/3.28
dd ibs=1 count=$((0x800)) skip=$((0x1800)) if=example01.bin of=naughtyb/4.27
dd ibs=1 count=$((0x800)) skip=$((0x2000)) if=example01.bin of=naughtyb/5.26
dd ibs=1 count=$((0x800)) skip=$((0x2800)) if=example01.bin of=naughtyb/6.25
dd ibs=1 count=$((0x800)) skip=$((0x3000)) if=example01.bin of=naughtyb/7.24
dd ibs=1 count=$((0x800)) skip=$((0x3800)) if=example01.bin of=naughtyb/8.23
rm example01.bin

#ROM_START( naughtyb )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "1.30",      0x0000, 0x0800, CRC(f6e1178e) SHA1(5cd428e1f085ff82d7237b3e261b33ff876fd4cb) )
#	ROM_LOAD( "2.29",      0x0800, 0x0800, CRC(b803eb8c) SHA1(c21b781eb329195e36e6fd1d7467bd9b0d9cbc5b) )
#	ROM_LOAD( "3.28",      0x1000, 0x0800, CRC(004d0ba7) SHA1(5c182fa6f65f7caa3459fcc5cdc3b7faa8b34769) )
#	ROM_LOAD( "4.27",      0x1800, 0x0800, CRC(3c7bcac6) SHA1(ef291cd5b2f8a64999dc015e16d3ea479fefaf8f) )
#	ROM_LOAD( "5.26",      0x2000, 0x0800, CRC(ea80f39b) SHA1(f05cc4ca48245053a8b35b594fb4c0c3b19304e0) )
#	ROM_LOAD( "6.25",      0x2800, 0x0800, CRC(66d9f942) SHA1(756b188836e9e9d86f8be59c9505288339b91899) )
#	ROM_LOAD( "7.24",      0x3000, 0x0800, CRC(00caf9be) SHA1(0599b28dfe8dd9c18564202af56ba8f272d7ac54) )
#	ROM_LOAD( "8.23",      0x3800, 0x0800, CRC(17c3b6fb) SHA1(c01c8ae27f5b9be90778f7c459c5ba0dddf443ba) )







#gfx
zxb.py library/b2r3f1_charmapnaughtyboy.zxi --org=$((0x0000))
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=b2r3f1_charmapnaughtyboy.bin of=b2r3f1_charmapnaughtyboyb.bin
#gfx1
xxd -p -c 2 -s 0 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/13.46
xxd -p -c 2 -s 1 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/15.44
xxd -p -c 2 -s 0 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/14.45
xxd -p -c 2 -s 1 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/16.43
#gfx2
xxd -p -c 2 -s 0 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/9.50
xxd -p -c 2 -s 1 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/11.48
xxd -p -c 2 -s 0 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/10.49
xxd -p -c 2 -s 1 -l 4096 b2r3f1_charmapnaughtyboyb.bin | cut -b 1-2 | xxd -r -p > naughtyb/12.47
rm b2r3f1_charmapnaughtyboy.bin b2r3f1_charmapnaughtyboyb.bin
#	ROM_REGION( 0x2000, "gfx1", 0 )
#	ROM_LOAD( "15.44",     0x0000, 0x0800, CRC(d692f9c7) SHA1(3573c518868690b140340d19f88c670026a6696d) )
#	ROM_LOAD( "16.43",     0x0800, 0x0800, CRC(d3ba8b27) SHA1(0ff14b8b983ab75870fb19b64327070ccd0888d6) )
#	ROM_LOAD( "13.46",     0x1000, 0x0800, CRC(c1669cd5) SHA1(9b4370ed54424e3615fa2e4d07cadae37ab8cd10) )
#	ROM_LOAD( "14.45",     0x1800, 0x0800, CRC(eef2c8e5) SHA1(5077c4052342958ee26c25047704c62eed44eb89) )
#	ROM_REGION( 0x2000, "gfx2", 0 )
#	ROM_LOAD( "11.48",     0x0000, 0x0800, CRC(75ec9710) SHA1(b41606930eff79ccf5bfcad01362251d7bab114a) )
#	ROM_LOAD( "12.47",     0x0800, 0x0800, CRC(ef0706c3) SHA1(0e0b82d29d710d1244384db84344bfba2e867b2e) )
#	ROM_LOAD( "9.50",      0x1000, 0x0800, CRC(8c8db764) SHA1(2641a1b8bc30896293ebd9396e304ce5eb7eb705) )
#	ROM_LOAD( "10.49",     0x1800, 0x0800, CRC(c97c97b9) SHA1(5da7fb378e85b6c9d5ab6e75544f1e64fae9997a) )










#palette
zxb.py library/palette01.zxi --org=$((0x0000))
cat dummy64k.bin >> palette01.bin
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0000)) if=palette01b.bin of=naughtyb/hgb3.l6
xxd -p -c 2 -s 0 -l 512 palette01b.bin | cut -b 1-2 | xxd -r -p > naughtyb/6301-1.64
xxd -p -c 2 -s 1 -l 512 palette01b.bin | cut -b 1-2 | xxd -r -p > naughtyb/6301-1.63
rm palette01.bin palette01b.bin

#proms
#dd bs=$((0x100)) count=1 if=/dev/urandom of=naughtyb/6301-1.63
#dd bs=$((0x100)) count=1 if=/dev/urandom of=naughtyb/6301-1.64

#	ROM_REGION( 0x0200, "proms", 0 )
#	ROM_LOAD( "6301-1.63", 0x0000, 0x0100, CRC(98ad89a1) SHA1(ddee7dcb003b66fbc7d6d6e90d499ed090c59227) ) /* palette low bits */
#	ROM_LOAD( "6301-1.64", 0x0100, 0x0100, CRC(909107d4) SHA1(138ace7845424bc3ca86b0889be634943c8c2d19) ) /* palette high bits */








#-
zip -r naughtyb naughtyb
rm naughtyb/*
rmdir naughtyb
rm smsboot.bin dummy64k.bin
mame -w -sound none -video soft -resolution0 512x512 -rp ./ naughtyb





#ROM_START( naughtyb )
#	ROM_REGION( 0x10000, "maincpu", 0 )
#	ROM_LOAD( "1.30",      0x0000, 0x0800, CRC(f6e1178e) SHA1(5cd428e1f085ff82d7237b3e261b33ff876fd4cb) )
#	ROM_LOAD( "2.29",      0x0800, 0x0800, CRC(b803eb8c) SHA1(c21b781eb329195e36e6fd1d7467bd9b0d9cbc5b) )
#	ROM_LOAD( "3.28",      0x1000, 0x0800, CRC(004d0ba7) SHA1(5c182fa6f65f7caa3459fcc5cdc3b7faa8b34769) )
#	ROM_LOAD( "4.27",      0x1800, 0x0800, CRC(3c7bcac6) SHA1(ef291cd5b2f8a64999dc015e16d3ea479fefaf8f) )
#	ROM_LOAD( "5.26",      0x2000, 0x0800, CRC(ea80f39b) SHA1(f05cc4ca48245053a8b35b594fb4c0c3b19304e0) )
#	ROM_LOAD( "6.25",      0x2800, 0x0800, CRC(66d9f942) SHA1(756b188836e9e9d86f8be59c9505288339b91899) )
#	ROM_LOAD( "7.24",      0x3000, 0x0800, CRC(00caf9be) SHA1(0599b28dfe8dd9c18564202af56ba8f272d7ac54) )
#	ROM_LOAD( "8.23",      0x3800, 0x0800, CRC(17c3b6fb) SHA1(c01c8ae27f5b9be90778f7c459c5ba0dddf443ba) )

#	ROM_REGION( 0x2000, "gfx1", 0 )
#	ROM_LOAD( "15.44",     0x0000, 0x0800, CRC(d692f9c7) SHA1(3573c518868690b140340d19f88c670026a6696d) )
#	ROM_LOAD( "16.43",     0x0800, 0x0800, CRC(d3ba8b27) SHA1(0ff14b8b983ab75870fb19b64327070ccd0888d6) )
#	ROM_LOAD( "13.46",     0x1000, 0x0800, CRC(c1669cd5) SHA1(9b4370ed54424e3615fa2e4d07cadae37ab8cd10) )
#	ROM_LOAD( "14.45",     0x1800, 0x0800, CRC(eef2c8e5) SHA1(5077c4052342958ee26c25047704c62eed44eb89) )
#	ROM_REGION( 0x2000, "gfx2", 0 )
#	ROM_LOAD( "11.48",     0x0000, 0x0800, CRC(75ec9710) SHA1(b41606930eff79ccf5bfcad01362251d7bab114a) )
#	ROM_LOAD( "12.47",     0x0800, 0x0800, CRC(ef0706c3) SHA1(0e0b82d29d710d1244384db84344bfba2e867b2e) )
#	ROM_LOAD( "9.50",      0x1000, 0x0800, CRC(8c8db764) SHA1(2641a1b8bc30896293ebd9396e304ce5eb7eb705) )
#	ROM_LOAD( "10.49",     0x1800, 0x0800, CRC(c97c97b9) SHA1(5da7fb378e85b6c9d5ab6e75544f1e64fae9997a) )

#	ROM_REGION( 0x0200, "proms", 0 )
#	ROM_LOAD( "6301-1.63", 0x0000, 0x0100, CRC(98ad89a1) SHA1(ddee7dcb003b66fbc7d6d6e90d499ed090c59227) ) /* palette low bits */
#	ROM_LOAD( "6301-1.64", 0x0100, 0x0100, CRC(909107d4) SHA1(138ace7845424bc3ca86b0889be634943c8c2d19) ) /* palette high bits */
#ROM_END










