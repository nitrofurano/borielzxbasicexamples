rm example01.asm
rm mario.zip
rm mario/*
rmdir mario
mkdir mario



#cpu
#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.bas --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.bas --heap-size=128 --org=$((0x0069))
dd bs=65536 count=1 if=/dev/zero of=dummy64k.bin
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy64k.bin >> example01.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin of=mario/tma1-c-7f_f.7f
dd ibs=1 count=$((0x2000)) skip=$((0x2000)) if=example01.bin of=mario/tma1-c-7e_f.7e
dd ibs=1 count=$((0x2000)) skip=$((0x4000)) if=example01.bin of=mario/tma1-c-7d_f.7d
dd ibs=1 count=$((0x1000)) skip=$((0xF000)) if=example01.bin of=mario/tma1-c-7c_f.7c
dd bs=$((0x2000)) count=1 if=/dev/zero of=mario/ao4_05.ic3
rm example01.bin

#ROM_START( mario )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "tma1-c-7f_f.7f",  0x0000, 0x2000, CRC(c0c6e014) SHA1(36a04f9ca1c2a583477cb8a6f2ef94e044e08296) )
#ROM_LOAD( "tma1-c-7e_f.7e",  0x2000, 0x2000, CRC(94fb60d6) SHA1(e74d74aa27f87a164bdd453ab0076efeeb7d4ea3) )
#ROM_LOAD( "tma1-c-7d_f.7d",  0x4000, 0x2000, CRC(dcceb6c1) SHA1(b19804e69ce2c98cf276c6055c3a250316b96b45) )
#ROM_LOAD( "tma1-c-7c_f.7c",  0xf000, 0x1000, CRC(4a63d96b) SHA1(b09060b2c84ab77cc540a27b8f932cb60ec8d442) )



#mcu
dd bs=$((0x0800)) count=1 if=/dev/zero of=mario/tma1-c-6k_e.6k

#ROM_REGION( 0x1800, "audiocpu", 0 ) /* sound */
#/* internal rom */
#ROM_FILL(                 0x0000, 0x0800, 0x00)
#/* first half banked */
#ROM_LOAD( "tma1-c-6k_e.6k",   0x1000, 0x0800, CRC(06b9ff85) SHA1(111a29bcb9cda0d935675fa26eca6b099a88427f) )
#/* second half always read */
#ROM_CONTINUE(             0x0800, 0x0800)



#gfx
zxb.py library/charmap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=charmap01.bin of=charmap01b.bin

zxb.py library/b2r0f0_charmapamericanspeedway01.zxi --org=$((0x0000))
dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=b2r0f0_charmapamericanspeedway01.bin of=b2r0f0_charmapamericanspeedway01b.bin
xxd -p -c 2 -s 0 -l 65536 b2r0f0_charmapamericanspeedway01b.bin | cut -b 1-2 | xxd -r -p > mario/tma1-v-3f.3f #ch_bit1
xxd -p -c 2 -s 1 -l 65536 b2r0f0_charmapamericanspeedway01b.bin | cut -b 1-2 | xxd -r -p > mario/tma1-v-3j.3j #ch_bit2



#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-3f.3f #ch_bit1
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-3j.3j #ch_bit2
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7m.7m #spr_bit1_left
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7n.7n #spr_bit1_right
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7p.7p #spr_bit2_left
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7s.7s #spr_bit2_right
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7t.7t #spr_bit3_left
dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=charmap01.bin of=mario/tma1-v-7u.7u #spr_bit3_right

#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-3f.3f
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-3j.3j
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7m.7m
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7n.7n
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7p.7p
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7s.7s
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7t.7t
#dd bs=$((0x10000)) count=1 if=/dev/zero of=mario/tma1-v-7u.7u




#gfx2 - text
#xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4_09.ic98 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4_10.ic97 #-bitdepth1
#gfx1 - sprite
#xxd -p -c 2 -s 0 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4_08.ic14 #-bitdepth0
#xxd -p -c 2 -s 1 -l 8192 charmap01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4_07.ic15 #-bitdepth1

rm charmap01.bin
rm charmap01b.bin
rm b2r0f0_charmapamericanspeedway01.bin
rm b2r0f0_charmapamericanspeedway01b.bin

#zxb.py library/spritemap01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x1000)) skip=$((0x0010)) if=spritemap01.bin of=mario/mario.5f
#rm spritemap01.bin

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "tma1-v-3f.3f",     0x0000, 0x1000, CRC(28b0c42c) SHA1(46749568aff88a28c3b6a1ac423abd1b90742a4d) )
#ROM_LOAD( "tma1-v-3j.3j",     0x1000, 0x1000, CRC(0c8cc04d) SHA1(15fae47d701dc1ef15c943cee6aa991776ecffdf) )
#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "tma1-v-7m.7m",     0x0000, 0x1000, CRC(22b7372e) SHA1(4a1c1e239cb6d483e76f50d7a3b941025963c6a3) )
#ROM_LOAD( "tma1-v-7n.7n",     0x1000, 0x1000, CRC(4f3a1f47) SHA1(0747d693b9482f6dd28b0bc484fd1d3e29d35654) )
#ROM_LOAD( "tma1-v-7p.7p",     0x2000, 0x1000, CRC(56be6ccd) SHA1(15a6e16c189d45f72761ebcbe9db5001bdecd659) )
#ROM_LOAD( "tma1-v-7s.7s",     0x3000, 0x1000, CRC(56f1d613) SHA1(9af6844dbaa3615433d0595e9e85e72493e31a54) )
#ROM_LOAD( "tma1-v-7t.7t",     0x4000, 0x1000, CRC(641f0008) SHA1(589fe108c7c11278fd897f2ded8f0498bc149cfd) )
#ROM_LOAD( "tma1-v-7u.7u",     0x5000, 0x1000, CRC(7baf5309) SHA1(d9194ff7b89a18273d37b47228fc7fb7e2a0ed1f) )



#palette
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x10000)) skip=$((0x0010)) if=palette01.bin of=palette01b.bin
#rm palette01.bin
#xxd -p -c 2 -s 0 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4-11.ic96
#xxd -p -c 2 -s 1 -l $((0x0800)) palette01b.bin | cut -b 1-2 | xxd -r -p > mario/ao4-12.ic95

#dd ibs=1 count=$((0x0400)) skip=$((0x0000)) if=palette01b.bin of=mario/ao4-11.ic96
#dd ibs=1 count=$((0x0400)) skip=$((0x0400)) if=palette01b.bin of=mario/ao4-12.ic95
rm palette01b.bin
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=mario/ao4-11.ic96
#dd bs=$((0x0400)) count=1 if=/dev/urandom of=mario/ao4-12.ic95
#zxb.py library/palette01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=mario/82s123.7f
#rm palette01.bin
#zxb.py library/clut01.zxi --org=$((0x0000))
#dd ibs=1 count=$((0x0100)) skip=$((0x0010)) if=clut01.bin of=mario/82s126.4a
#rm clut01.bin

dd bs=$((0x0200)) count=1 if=/dev/urandom of=mario/tma1-c-4p_1.4p
dd bs=$((0x0200)) count=1 if=/dev/urandom of=mario/tma1-c-5p.5p

#ROM_REGION( 0x0200, "proms", 0 )
#ROM_LOAD( "tma1-c-4p_1.4p", 0x0000, 0x0200, CRC(8187d286) SHA1(8a6d8e622599f1aacaeb10f7b1a39a23c8a840a0) ) /* BPROM was a MB7124E read as 82S147 */
#ROM_REGION( 0x0020, "unk_proms", 0 ) /* is this the color prom? */
#ROM_LOAD( "tma1-c-5p.5p", 0x0000, 0x0020, CRC(58d86098) SHA1(d654995004b9052b12d3b682a2b39530e70030fc) ) /* BPROM was a TBP18S030N read as 82S123, unknown use */
#ROM_END



rm smsboot.bin

zip -r mario mario
rm mario/*
rmdir mario
rm dummy64k.bin example01.bin

mame -w -sound none -video soft -resolution0 512x512 -rp ./ mario



#ROM_START( mario )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "tma1-c-7f_f.7f",  0x0000, 0x2000, CRC(c0c6e014) SHA1(36a04f9ca1c2a583477cb8a6f2ef94e044e08296) )
#ROM_LOAD( "tma1-c-7e_f.7e",  0x2000, 0x2000, CRC(94fb60d6) SHA1(e74d74aa27f87a164bdd453ab0076efeeb7d4ea3) )
#ROM_LOAD( "tma1-c-7d_f.7d",  0x4000, 0x2000, CRC(dcceb6c1) SHA1(b19804e69ce2c98cf276c6055c3a250316b96b45) )
#ROM_LOAD( "tma1-c-7c_f.7c",  0xf000, 0x1000, CRC(4a63d96b) SHA1(b09060b2c84ab77cc540a27b8f932cb60ec8d442) )

#ROM_REGION( 0x1800, "audiocpu", 0 ) /* sound */
#/* internal rom */
#ROM_FILL(                 0x0000, 0x0800, 0x00)
#/* first half banked */
#ROM_LOAD( "tma1-c-6k_e.6k",   0x1000, 0x0800, CRC(06b9ff85) SHA1(111a29bcb9cda0d935675fa26eca6b099a88427f) )
#/* second half always read */
#ROM_CONTINUE(             0x0800, 0x0800)

#ROM_REGION( 0x2000, "gfx1", 0 )
#ROM_LOAD( "tma1-v-3f.3f",     0x0000, 0x1000, CRC(28b0c42c) SHA1(46749568aff88a28c3b6a1ac423abd1b90742a4d) )
#ROM_LOAD( "tma1-v-3j.3j",     0x1000, 0x1000, CRC(0c8cc04d) SHA1(15fae47d701dc1ef15c943cee6aa991776ecffdf) )
#ROM_REGION( 0x6000, "gfx2", 0 )
#ROM_LOAD( "tma1-v-7m.7m",     0x0000, 0x1000, CRC(22b7372e) SHA1(4a1c1e239cb6d483e76f50d7a3b941025963c6a3) )
#ROM_LOAD( "tma1-v-7n.7n",     0x1000, 0x1000, CRC(4f3a1f47) SHA1(0747d693b9482f6dd28b0bc484fd1d3e29d35654) )
#ROM_LOAD( "tma1-v-7p.7p",     0x2000, 0x1000, CRC(56be6ccd) SHA1(15a6e16c189d45f72761ebcbe9db5001bdecd659) )
#ROM_LOAD( "tma1-v-7s.7s",     0x3000, 0x1000, CRC(56f1d613) SHA1(9af6844dbaa3615433d0595e9e85e72493e31a54) )
#ROM_LOAD( "tma1-v-7t.7t",     0x4000, 0x1000, CRC(641f0008) SHA1(589fe108c7c11278fd897f2ded8f0498bc149cfd) )
#ROM_LOAD( "tma1-v-7u.7u",     0x5000, 0x1000, CRC(7baf5309) SHA1(d9194ff7b89a18273d37b47228fc7fb7e2a0ed1f) )

#ROM_REGION( 0x0200, "proms", 0 )
#ROM_LOAD( "tma1-c-4p_1.4p", 0x0000, 0x0200, CRC(8187d286) SHA1(8a6d8e622599f1aacaeb10f7b1a39a23c8a840a0) ) /* BPROM was a MB7124E read as 82S147 */
#ROM_REGION( 0x0020, "unk_proms", 0 ) /* is this the color prom? */
#ROM_LOAD( "tma1-c-5p.5p", 0x0000, 0x0020, CRC(58d86098) SHA1(d654995004b9052b12d3b682a2b39530e70030fc) ) /* BPROM was a TBP18S030N read as 82S123, unknown use */
#ROM_END



