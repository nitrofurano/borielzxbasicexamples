rm dominob.zip
rm example01.asm
rm dominob/*
rmdir dominob
mkdir dominob





dd bs=$((0x8000)) count=1 if=/dev/zero of=dummy32k.bin





#- zxb.py example01.zxb --asm --org=$((0x0000))
#- zxb.py example01.zxb --org=$((0x0000))
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=smsboot2.bin
zxb.py example01.zxb --heap-size=128 --asm --org=$((0x0069))
zxb.py example01.zxb --heap-size=128 --org=$((0x0069))
cat example01.bin >> smsboot2.bin
rm example01.bin
mv smsboot2.bin example01.bin
cat dummy32k.bin >> example01.bin
dd ibs=1 count=$((0x10000)) skip=$((0x0000)) if=example01.bin of=dominob/u81

#ROM_START( dominob )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "u81",   0x0000, 0x10000, CRC(709b7a29) SHA1(7c95cbaf669a0885101a48e937868c245f87567e) )




zxb.py library/b3r0f0_charmapiremm62v01.zxi --org=$((0x0000))
cat dummy32k.bin >> b3r0f0_charmapiremm62v01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=b3r0f0_charmapiremm62v01.bin of=b3r0f0_charmapiremm62v01b.bin
xxd -p -c 3 -s 0 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > dominob/u33
xxd -p -c 3 -s 1 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > dominob/u34
xxd -p -c 3 -s 2 -l $((0x18000)) b3r0f0_charmapiremm62v01b.bin | cut -b 1-2 | xxd -r -p > dominob/u35
rm b3r0f0_charmapiremm62v01.bin b3r0f0_charmapiremm62v01b.bin
#ROM_REGION( 0x18000, "gfx1", 0 )
#ROM_LOAD( "u33",   0x00000, 0x8000, CRC(359c98de) SHA1(5c96dfb538c6b25530582f8c2a0cb20d85c39f68) )
#ROM_LOAD( "u34",   0x08000, 0x8000, CRC(0031f713) SHA1(9341f84081e2d8954e476236e93e49b4d8819b8f) )
#ROM_LOAD( "u35",   0x10000, 0x8000, CRC(6eb87657) SHA1(40ff9d6f21ade48b16f0cefea08a9364a4ee9144) )



zxb.py library/charmap01.zxi --org=$((0x0000))
cat dummy32k.bin >> charmap01.bin
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmap01.bin of=dominob/u111
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmap01.bin of=dominob/u112
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmap01.bin of=dominob/u113
dd ibs=1 count=$((0x8000)) skip=$((0x0010)) if=charmap01.bin of=dominob/u114
rm charmap01.bin

#ROM_REGION( 0x100000, "gfx2", 0 )
#ROM_LOAD( "u111",   0x00000, 0x40000, CRC(b835be84) SHA1(bbb744a28df00017f81d6eac12b00b5f3aca3a8b) )#ROM_CONTINUE(0x00000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u112",   0x40000, 0x40000, CRC(60d7bfd7) SHA1(9f6475ce717e3d5a42aaaacc3ec340e74b7e40b4) )#ROM_CONTINUE(0x40000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u113",   0x80000, 0x40000, CRC(3c999d1e) SHA1(e10c61d7868d9c902e337c9947506761878e31a5) )#ROM_CONTINUE(0x80000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u114",   0xc0000, 0x40000, CRC(2364124e) SHA1(55dc6c46f54655c574a96f5c920e4e1f2e05fd5d) )#ROM_CONTINUE(0xc0000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_END



#zxb.py library/palette01.bas --org=$((0x0000))
#cat dummy32k.bin >> palette01.bin
#dd ibs=1 count=$((0x0020)) skip=$((0x0010)) if=palette01.bin of=dominob/dominob.3j
#rm palette01.bin
#dd bs=$((0x0020)) count=1 if=/dev/urandom of=dominob/dominob.3j


#- clut01.zxi might need to be different files
#zxb.py library/clut01.zxi --org=$((0x0000))
#cat dummy32k.bin >> clut01.bin
#dd ibs=1 count=$((0x0200)) skip=$((0x0010)) if=clut01.bin of=clut01b.bin
#rm clut01.bin
#dd ibs=1 count=$((0x0100)) skip=$((0x0000)) if=clut01b.bin of=dominob/dominob.5h
#dd ibs=1 count=$((0x0100)) skip=$((0x0100)) if=clut01b.bin of=dominob/dominob.11j
#rm clut01b.bin

#????
#dd bs=256 count=1 if=/dev/zero of=dominob/82s126.1m
#dd bs=256 count=1 if=/dev/zero of=dominob/82s126.3m

rm smsboot.bin dummy32k.bin example01.bin

zip -r dominob dominob
rm dominob/*
rmdir dominob

mame -w -video soft -resolution0 512x512 -rp ./ dominob





#ROM_START( dominob )
#ROM_REGION( 0x10000, "maincpu", 0 )
#ROM_LOAD( "u81",   0x0000, 0x10000, CRC(709b7a29) SHA1(7c95cbaf669a0885101a48e937868c245f87567e) )

#ROM_REGION( 0x18000, "gfx1", 0 )
#ROM_LOAD( "u33",   0x00000, 0x8000, CRC(359c98de) SHA1(5c96dfb538c6b25530582f8c2a0cb20d85c39f68) )
#ROM_LOAD( "u34",   0x08000, 0x8000, CRC(0031f713) SHA1(9341f84081e2d8954e476236e93e49b4d8819b8f) )
#ROM_LOAD( "u35",   0x10000, 0x8000, CRC(6eb87657) SHA1(40ff9d6f21ade48b16f0cefea08a9364a4ee9144) )
#ROM_REGION( 0x100000, "gfx2", 0 )
#ROM_LOAD( "u111",   0x00000, 0x40000, CRC(b835be84) SHA1(bbb744a28df00017f81d6eac12b00b5f3aca3a8b) )
#ROM_CONTINUE(0x00000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u112",   0x40000, 0x40000, CRC(60d7bfd7) SHA1(9f6475ce717e3d5a42aaaacc3ec340e74b7e40b4) )
#ROM_CONTINUE(0x40000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u113",   0x80000, 0x40000, CRC(3c999d1e) SHA1(e10c61d7868d9c902e337c9947506761878e31a5) )
#ROM_CONTINUE(0x80000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_LOAD( "u114",   0xc0000, 0x40000, CRC(2364124e) SHA1(55dc6c46f54655c574a96f5c920e4e1f2e05fd5d) )
#ROM_CONTINUE(0xc0000,0x40000) // 1ST AND 2ND HALF IDENTICAL
#ROM_END









