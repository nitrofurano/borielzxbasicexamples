sub ggpalette(tad as ubyte, tvl as uinteger): '- format: $BGR 12bit
  asm
    ld d,(ix+7)
    ld e,(ix+6)
    ld a,(ix+5)
    and 31
    rl a
    out ($bf),a
    ld a,$c0
    out ($bf),a
    ld a,e
    out ($be),a
    ld a,d
    out ($be),a
    end asm
  end sub




sub ggpalettergb(tad as ubyte, tvl as uinteger): '- format: $BGR 12bit
  asm
    ld d,(ix+7)
    ld e,(ix+6)

    ld a,d
    or e
    and $f0
    ld b,a

    ld a,d
    and $0f
    or b
    ld d,a

    ld a,e
    and $0f
    or b
    ld e,a

    ld a,(ix+5)
    and 31
    rl a
    out ($bf),a
    ld a,$c0
    out ($bf),a
    ld a,d
    out ($be),a
    ld a,e
    out ($be),a
    end asm
  end sub

