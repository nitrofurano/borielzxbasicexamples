asm
  ld sp,$FFFF
  end asm

#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/charmap01.bas"

dim eee as uinteger at $E010
dim seed as uinteger at $E012
dim ee0 as uinteger at $E014
dim ee1 as uinteger at $E016
dim ee2 as uinteger at $E018
dim ee3 as uinteger at $E01A
dim ex0 as uinteger at $E01C
dim ey0 as uinteger at $E01E

seed=9133

sub putcharalt01(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,tat1 as ubyte,tad1 as uinteger):
  dim tlp1 as uinteger at $E000
  out $71,(txp1*8):out $71,typ1 bor $40
  for tlp1=0 to 7
    out $70,peek(tad1+tch1*8+tlp1)
    next
  out $71,(txp1*8):out $71,typ1 bor $60
  for tlp1=0 to 7
    out $70,tat1
    next
  end sub



'out $70,$23
'out $70,$87
out $71,$23
out $71,$87
'out $72,$23
'out $72,$87
'out $73,$23
'out $73,$87

'do:loop

sub sothellovdp(tad as ubyte, tvl as ubyte):
  asm
    ld a,(ix+7) ;- previously was 6
    out ($71),a
    ld a,(ix+5) ;- previously was 6
    or $80
    out ($71),a
    end asm
  end sub

sub sothellovpoke(tvram as uinteger,tvl as uinteger):
  asm
    ld l,(ix+4)
    ld a,l
    out ($71),a
    ld h,(ix+5)
    ld a,h
    or $40
    out ($71),a
    ld a,(ix+6)
    out ($70),a
    end asm
  end sub



out $71,$89
out $71,$87


out $71,$61:out $71,$87
out $71,%00000010:out $71,$80
out $71,%11100010:out $71,$81
out $71,$06:out $71,$82
out $71,$FF:out $71,$83
out $71,$03:out $71,$84
out $71,$36:out $71,$85
out $71,$07:out $71,$86




'- palette
for eee=0 to 15
  out $71,eee:out $71,$96
  seed=smsrnd(seed)
  out $72,seed
  seed=smsrnd(seed)
  out $72,seed
  next




out $71,$EE
out $71,$87


out $71,$00:out $71,$40
for eee=0 to $3FFF
  seed=smsrnd(seed)
  out $70,0
  next




out $71,$00:out $71,$00 bor $40
for eee=0 to $17FF step 8
  seed=smsrnd(seed)
  for ee0=0 to 7
    out $70,peek(@charmap01+((64+seed mod 9)*8)+ee0)
    next:next

out $71,$00:out $71,$18 bor $40
for eee=0 to 767
  out $70,eee band $FF
  next

out $71,$00:out $71,$20 bor $40
for eee=0 to $17FF step 8
  seed=smsrnd(seed)
  ee1=$11+(seed mod 15)+ ((int(seed/16)mod 15)*16)
  for ee0=0 to 7
    out $70,ee1
    next
  next

for ee0=0 to 7
  for ee1=0 to 15
    putcharalt01(ee1+15,ee0+15,ee0*16+ee1,$EF,@charmap01-256)
    next:next





eee=0:ex0=8:ey0=8
do

putcharalt01(1,1,48+int(eee/10000) mod 10,$EF,@charmap01-256)
putcharalt01(2,1,48+int(eee/1000) mod 10,$EF,@charmap01-256)
putcharalt01(3,1,48+int(eee/100) mod 10,$EF,@charmap01-256)
putcharalt01(4,1,48+int(eee/10) mod 10,$EF,@charmap01-256)
putcharalt01(5,1,48+eee mod 10,$EF,@charmap01-256)

ee0=in($00) bxor $FF
putcharalt01(3,3,48+int(ee0/100) mod 10,$EF,@charmap01-256)
putcharalt01(4,3,48+int(ee0/10) mod 10,$EF,@charmap01-256)
putcharalt01(5,3,48+ee0 mod 10,$EF,@charmap01-256)

ey0=ey0-((ee0 band 1)/1)+((ee0 band 2)/2)
ex0=ex0-((ee0 band 4)/4)+((ee0 band 8)/8)

ee0=in($10) bxor $FF
putcharalt01(3,4,48+int(ee0/100) mod 10,$EF,@charmap01-256)
putcharalt01(4,4,48+int(ee0/10) mod 10,$EF,@charmap01-256)
putcharalt01(5,4,48+ee0 mod 10,$EF,@charmap01-256)

ee0=in($20) bxor $FF
putcharalt01(3,5,48+int(ee0/100) mod 10,$EF,@charmap01-256)
putcharalt01(4,5,48+int(ee0/10) mod 10,$EF,@charmap01-256)
putcharalt01(5,5,48+ee0 mod 10,$EF,@charmap01-256)

putcharalt01(ex0,ey0,32+(eee mod 73),$EF,@charmap01-256)

eee=eee+1
loop








do:loop

'msxcolor($6,$F,$1)
sothellovdp(7,$61) 'sothellovdp(7,F*16+4) '- ink*16+border - color(3,?,1)

'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
sothellovdp(0,%00000010):sothellovdp(1,%11100010) '- screen2,2 (graphics1)
'- vdp entries for screen2 - base vram addresses
sothellovdp(2,$06) '- $1800
sothellovdp(3,$FF) '- $2000
sothellovdp(4,$03) '- $0000
sothellovdp(5,$36) '- $1B00
sothellovdp(6,$07) '- $3800





seed=0


do
for eee=$0000 to $3FFF
  seed=smsrnd(seed)
  sothellovpoke(eee,seed)
  smsdelay(100)
  next

loop





'-------------------------------------------------------------
'
'
'#include "library/sothellovdp.bas"
'#include "library/smsfilvrm.bas"
'#include "library/smsldirvm.bas"
'#include "library/smsscroll.bas"
'#include "library/smsvpoke.bas"
'#include "library/smsjoypad.bas"
'#include "library/smspalette.bas"

'
''#include "library/ncsax27e8_256x64_tiles.bas"
'#include "library/ch01v16c_256x64_tiles.bas"
''#include "library/abm4scr4_256x64_tiles.bas"
'
''#include "library/6x8charset01.bas"
'
'start01:
'
''- mode 4,2
'sothellovdp(0,%00000100):sothellovdp(1,%11100010) '- sothellovdp(1,$84)
'sothellovdp(2,$ff):sothellovdp(5,$ff):sothellovdp(10,$ff)
'sothellovdp(6,$fb) '- sprite patterns - $fb for $0000 (256 sprites available), $ff for $2000 (192 sprites available)
'sothellovdp(7,$00) '- border colour (sprite palette)
'
''smsscroll($30,$c8) '- (for locating the gamegear display on the top-left edge of pattern area)
'
'smsfilvrm($0000,0,$4000) '- replaces "clear vram"
''smsldirvm($0800,@charset01,2048)
'
''-------------------------------------------------------------------------------
'
'dim ee1 as uinteger at $c010
'dim ee2 as uinteger at $c012
'dim ee3 as uinteger at $c014
'
''-------------------------------------------------------------------------------
'
''eesd=0
'for ee2=0 to 15
'  smspalette(ee2,peek(@palette01+ee2))
'  smspalette(ee2+16,peek(@palette01+ee2))
'  next
'
''-------------------------------------------------------------------------------
'
'ee3=0
'for ee2=0 to 767
'  smsvpoke($3800+(ee2*2),ee3 band $FF)
'  ee3=ee3+1
'  next
'
''-------------------------------------------------------------------------------
'
'for ee2=0 to $1FFF
'  smsvpoke(ee2,peek(@charset01+ee2))
'  next
'
'for ee2=0 to $17FF
'  smsvpoke(ee2+$2000,peek(@charset01+ee2))
'  next
'
''-------------------------------------------------------------------------------
'
'for ee2=$3E00 to $3FFF
'  ee1=smsrnd(ee1)
'  smsvpoke(ee2,ee1)
'  smsvpoke(ee2,15)
'  next
'
''-------------------------------------------------------------------------------
'
'for ee2=$3F07 to $3F3F step 8
'  'ee1=smsrnd(ee1)
'  smsvpoke(ee2,$D0)
'  next
'
''-------------------------------------------------------------------------------
'
'loopqq1q:
'
'for ee2=0 to 255
'  smsdelay(5000)
'  smsvpoke($3F00,160)
'  smsvpoke($3F80,ee2)
'  smsvpoke($3F81,ee2 band $FC)
'  smsvpoke($3F01,160)
'  smsvpoke($3F82,ee2+8)
'  smsvpoke($3F83,(ee2 band $FC) bor 2)
'  smsvpoke($3F07,$D0)
'  next
'
'  goto loopqq1q
'
''-------------------------------------------------------------------------------
'
'testch01aaa:
'asm 
'  end asm
'
''-------------------------------------------------------------------------------
'''- mode 2,2 - vdp entries for screen1 - base vram addresses
''sothellovdp(0,$02):sothellovdp(1,$E2) '- screen1,2
''sothellovdp(2,$06):sothellovdp(3,$FF):sothellovdp(4,$03):sothellovdp(5,$36):sothellovdp(6,$07)
''sothellovdp(7,$F4) 'sothellovdp(7,F*16+4) '- ink*16+border - color(3,?,1)
''-------------------------------------------------------------------------------
'
'
