rm example01.asm
rm sothello.zip
rm sothello/*
rmdir sothello
mkdir sothello


dd bs=$((0x20000)) count=1 if=/dev/zero of=dummy128k.bin

zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=example01.sm_
zxb.py example01.bas --org=$((0x0069))
zxb.py example01.bas --asm --org=$((0x0069))
cat example01.bin >> example01.sm_
cat dummy128k.bin >> example01.sm_
dd ibs=1 count=$((0x8000)) skip=$((0x00000)) if=example01.sm_ of=sothello/3.7c
dd ibs=1 count=$((0x8000)) skip=$((0x08000)) if=example01.sm_ of=sothello/4.8c
dd ibs=1 count=$((0x8000)) skip=$((0x10000)) if=example01.sm_ of=sothello/5.9c

rm smsboot.bin example01.sm_ _dummybytes.bin example01.bin






dd bs=$((0x8000)) count=1 if=/dev/zero of=sothello/1.7a
dd bs=$((0x8000)) count=1 if=/dev/zero of=sothello/2.8a
dd bs=$((0x8000)) count=1 if=/dev/zero of=sothello/6.7f



rm smsboot.bin dummy128k.bin
zip -r sothello sothello
rm sothello/*
rmdir sothello

mame -w -video soft -resolution0 512x512 -rp ./ sothello



#- ROM_START( sothello )
#- ROM_REGION( 0x20000, "maincpu", 0 )
#- ROM_LOAD( "3.7c",   0x00000, 0x8000, CRC(47f97bd4) SHA1(52c9638f098fdcf66903fad7dafe3ab171758572) )
#- ROM_LOAD( "4.8c",   0x08000, 0x8000, CRC(a98414e9) SHA1(6d14e1f9c79b95101e0aa101034f398af09d7f32) )
#- ROM_LOAD( "5.9c",   0x10000, 0x8000, CRC(e5b5d61e) SHA1(2e4b3d85f41d0796a4d61eae40dd824769e1db86) )

#- ROM_REGION( 0x10000, "soundcpu", 0 )
#- ROM_LOAD( "1.7a",   0x0000, 0x8000, CRC(6951536a) SHA1(64d07a692d6a167334c825dc173630b02584fdf6) )
#- ROM_LOAD( "2.8a",   0x8000, 0x8000, CRC(9c535317) SHA1(b2e69b489e111d6f8105e68fade6e5abefb825f7) )

#- ROM_REGION( 0x10000, "sub", 0 )
#- ROM_LOAD( "6.7f",   0x8000, 0x8000, CRC(ee80fc78) SHA1(9a9d7925847d7a36930f0761c70f67a9affc5e7c) )
#- ROM_END
