sub pacmanfillram(tvram as uinteger,tvl as uinteger, tlen as uinteger):
  asm
    ld l,(ix+4)
    ld h,(ix+5)
    ld b,(ix+9)
    ld c,(ix+8)

    pacmanfillramloop:
    ld a,(ix+6)
    ld (hl),a
    dec bc
    inc hl
    ld a,b
    or c
    jp nz,pacmanfillramloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : 
'           HL - start address
'           A  - data byte
'           BC - length of the area to be written
'Registers: AF, BC
