rm example01.asm example01.bin
#zxb.py library/smsboot.bas --org=1024
#dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=example01.bin_
zxb.py example01.bas --org=$((0xE800))
zxb.py example01.bas --asm --org=$((0xE800))
cat example01.bin >> example01.bin_
dd bs=$((0x10000)) count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> example01.bin_
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin_ of=example01.bin
rm _dummybytes.bin example01.bin_ # smsboot.bin example01.bin
#rm smsboot.bin example01.bin_ _dummybytes.bin example01.bin
#cp example01.bin example01.sms
#smshead8k example01.sms
mame astrocdl -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -bp ~/.mess/roms -cart1 example01.bin

