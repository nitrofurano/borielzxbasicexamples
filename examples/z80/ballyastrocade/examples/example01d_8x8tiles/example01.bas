#include "library/ballyastrocadeheader.bas"
#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/charmapballyastrocade01.bas"
#include "library/ballyastrocadeldir40b2.bas"

dim eee as uinteger at $4F10
dim seed as uinteger at $4F12
dim ee0 as uinteger at $4F14
dim ee1 as uinteger at $4F16
dim ee2 as uinteger at $4F18
dim ee3 as uinteger at $4F1A
dim eex as uinteger at $4F1C
dim eey as uinteger at $4F1E

sub ballyastrocadeputchar8x8(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,tad1 as uinteger):
  ballyastrocadeldir40b2($4000+typ1*320+txp1*2,tad1+tch1*16,8)
  ballyastrocadeldir40b2($4001+typ1*320+txp1*2,tad1+1+tch1*16,8)
  end sub

sub ballyastrocadescreenmode(tvl1 as ubyte):
  out $09,tvl1
  end sub

sub ballyastrocadepalette(tid1 as uinteger,tvl1 as ubyte):
  out (tid1 band 7),tvl1
  end sub

ballyastrocadescreenmode(%00101100)  '- 00xxxxb? = background color  ; 00=clear ???, 01=blue, 10=yellow, 11=red ; Does this depend on the palatte??? ; xx101100b = set right/left palette ???
ballyastrocadepalette(4,$02)
ballyastrocadepalette(5,$C2)
ballyastrocadepalette(6,$CD)
ballyastrocadepalette(7,$06)
ballyastrocadepalette(0,$03)
ballyastrocadepalette(1,$12)
ballyastrocadepalette(2,$1D)
ballyastrocadepalette(3,$04)

for ee0=0 to 11
  for ee1=0 to 19
    seed=smsrnd(seed)
    ballyastrocadeputchar8x8(ee1,ee0,96+(seed band 3),@charmap01-512)
    next:next

for ee0=0 to 4
  for ee1=0 to 15
    seed=smsrnd(seed)
    ballyastrocadeputchar8x8(ee1+3,ee0+6,32+ee0*16+ee1,@charmap01-512)
    next:next

eee=0:eex=12:eey=2
do
ballyastrocadeputchar8x8(1,0,48+(int(eee/10000) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(2,0,48+(int(eee/1000) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(3,0,48+(int(eee/100) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(4,0,48+(int(eee/10) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(5,0,48+(eee mod 10),@charmap01-512)

ee0=in($10)
ballyastrocadeputchar8x8(1,1,48+(int(ee0/100) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(2,1,48+(int(ee0/10) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(3,1,48+(ee0 mod 10),@charmap01-512)

eex=eex+((ee0 band 8)/8)-((ee0 band 4)/4)
eey=eey+((ee0 band 2)/2)-((ee0 band 1)/1)

ee0=in($11)
ballyastrocadeputchar8x8(1,2,48+(int(ee0/100) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(2,2,48+(int(ee0/10) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(3,2,48+(ee0 mod 10),@charmap01-512)

ee0=in($12)
ballyastrocadeputchar8x8(1,3,48+(int(ee0/100) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(2,3,48+(int(ee0/10) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(3,3,48+(ee0 mod 10),@charmap01-512)

ee0=in($13)
ballyastrocadeputchar8x8(1,4,48+(int(ee0/100) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(2,4,48+(int(ee0/10) mod 10),@charmap01-512)
ballyastrocadeputchar8x8(3,4,48+(ee0 mod 10),@charmap01-512)

ballyastrocadeputchar8x8(eex,eey,32+(eee mod 80),@charmap01-512)

eee=eee+1
loop

do:loop


