sub ballyastrocadeldir40b2(tvram as uinteger, tram as uinteger,tlen as uinteger):
'- testar....
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)

    ballyastrocadeldir40b2loop:
    ld a,(hl)    ; Get data byte
    ld (de),a    ; funciona?
    
    inc hl       ; Point to next letter
    inc hl       ; Point to next letter again (important on bally astrocade, defw stuff)

    ;add de,40    ; funciona?
    ex de,hl
    push de
    ld de,40
    add hl,de
    pop de
    ex de,hl
    
    dec bc
    ld a,b
    or c
    jp nz,ballyastrocadeldir40b2loop

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
