#include "library/ballyastrocadeheader.bas"
#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/charmapballyastrocade4x6_64x36x2wdefbasm.bas"
#include "library/ballyastrocadeldir40b.bas"
#include "library/pacmanfillram.bas"

dim eee as uinteger at $4F10
dim seed as uinteger at $4F12
dim ee0 as uinteger at $4F14
dim ee1 as uinteger at $4F16
dim ee2 as uinteger at $4F18
dim ee3 as uinteger at $4F1A
dim eex as uinteger at $4F1C
dim eey as uinteger at $4F1E

sub ballyastrocadeputchar4x6(txp1 as uinteger,typ1 as uinteger,tch1 as uinteger,tad1 as uinteger):
  ballyastrocadeldir40b($4000+typ1*240+txp1,tad1+tch1*6,6)
  end sub

sub ballyastrocadescreenmode(tvl1 as ubyte):
  out $08,tvl1
  end sub

sub ballyastrocadeborderandpaletteoffset(tbc1 as ubyte,tof1 as ubyte):
  out $09, ((tbc1 band 3)*64) bor (tof1 band 63)
  end sub

sub ballyastrocadepalette(tid1 as uinteger,tvl1 as ubyte):
  out (tid1 band 7),tvl1
  end sub

pacmanfillram($4000,$00,$F00)

ballyastrocadescreenmode(%00101100)  '- 00xxxxb? = background color  ; 00=clear ???, 01=blue, 10=yellow, 11=red ; Does this depend on the palatte??? ; xx101100b = set right/left palette ???
ballyastrocadepalette(4,$02)
ballyastrocadepalette(5,$73)
ballyastrocadepalette(6,$7C)
ballyastrocadepalette(7,$06)
ballyastrocadepalette(0,$05)
ballyastrocadepalette(1,$BC)
ballyastrocadepalette(2,$B3)
ballyastrocadepalette(3,$01)

'out $09,(3*64)+(20 band 63)   '- ccoooooo
ballyastrocadeborderandpaletteoffset(3,20)

for ee0=0 to 15
  for ee1=0 to 39
    seed=smsrnd(seed)
    'ballyastrocadeputchar4x6(ee1,ee0,128+(seed band 3),@charmap01-192)
    ballyastrocadeputchar4x6(ee1,ee0,128+(seed mod 46),@charmap01-192)
    next:next

for ee0=2 to 10
  for ee1=0 to 15
    seed=smsrnd(seed)
    ballyastrocadeputchar4x6(ee1+23,ee0+4,ee0*16+ee1,@charmap01-192)
    next:next


eee=0:eex=12:eey=2
do
ballyastrocadeputchar4x6(1,1,48+(int(eee/10000) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(2,1,48+(int(eee/1000) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(3,1,48+(int(eee/100) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(4,1,48+(int(eee/10) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(5,1,48+(eee mod 10),@charmap01-192)

ee0=in($10)
ballyastrocadeputchar4x6(1,3,48+(int(ee0/100) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(2,3,48+(int(ee0/10) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(3,3,48+(ee0 mod 10),@charmap01-192)

eex=eex+((ee0 band 8)/8)-((ee0 band 4)/4)
eey=eey+((ee0 band 2)/2)-((ee0 band 1)/1)

ee0=in($11)
ballyastrocadeputchar4x6(1,4,48+(int(ee0/100) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(2,4,48+(int(ee0/10) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(3,4,48+(ee0 mod 10),@charmap01-192)

ee0=in($12)
ballyastrocadeputchar4x6(1,5,48+(int(ee0/100) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(2,5,48+(int(ee0/10) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(3,5,48+(ee0 mod 10),@charmap01-192)

ee0=in($13)
ballyastrocadeputchar4x6(1,6,48+(int(ee0/100) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(2,6,48+(int(ee0/10) mod 10),@charmap01-192)
ballyastrocadeputchar4x6(3,6,48+(ee0 mod 10),@charmap01-192)

ballyastrocadeputchar4x6(eex,eey,32+(eee mod 138),@charmap01-192)
ballyastrocadeborderandpaletteoffset(eey,eex)

eee=eee+1
loop

do:loop


