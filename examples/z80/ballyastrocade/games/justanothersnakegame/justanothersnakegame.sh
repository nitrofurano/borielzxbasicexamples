rm justanothersnakegame.asm justanothersnakegame.bin
#zxb.py library/smsboot.bas --org=1024
#dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=justanothersnakegame.bin_
zxb.py justanothersnakegame.bas --org=$((0xE800))
zxb.py justanothersnakegame.bas --asm --org=$((0xE800))
cat justanothersnakegame.bin >> justanothersnakegame.bin_
dd bs=$((0x10000)) count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> justanothersnakegame.bin_
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=justanothersnakegame.bin_ of=justanothersnakegame.bin
rm _dummybytes.bin justanothersnakegame.bin_ # smsboot.bin justanothersnakegame.bin
#rm smsboot.bin justanothersnakegame.bin_ _dummybytes.bin justanothersnakegame.bin
#cp justanothersnakegame.bin justanothersnakegame.sms
#smshead8k justanothersnakegame.sms
mame astrocdl -skip_gameinfo -resolution0 754x448 -aspect 425:320 -video soft -w -bp ~/.mess/roms -cart1 justanothersnakegame.bin

