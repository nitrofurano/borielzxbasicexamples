sub minivadersfillramxor32b(tvram as uinteger,tvl as uinteger, tlen as uinteger):
  asm
    ld l,(ix+4)
    ld h,(ix+5)
    ld b,(ix+9)
    ld c,(ix+8)

    minivadersfillramxor32bloop:

    ld a,(ix+6)
    ld d,a
    ld a,(hl)
    xor d
    ld (hl),a
    dec bc

    ;inc hl
    push de
    ld de,32
    add hl,de
    pop de

    ld a,b
    or c
    jp nz,minivadersfillramxor32bloop

;    ld a,(hl)    ; Get data byte
;    ld (de),a    ; funciona?
;    inc hl       ; Point to next letter
;    ;add de,32    ; funciona?
;    ex de,hl
;    push de
;    ld de,32
;    add hl,de
;    pop de
;    ex de,hl
;    dec bc
;    ld a,b
;    or c

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
