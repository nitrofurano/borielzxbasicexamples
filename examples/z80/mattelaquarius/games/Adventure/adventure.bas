#include "library/aquariusfillbytes.bas"
#include "library/aquariuscls.bas"
#include "library/aquariusputchar.bas"
#include "library/aquariusputtile.bas"
#include "library/aquariuswaitvbl.bas"
#include "library/aquariusdelay.bas"
#include "library/aquariuskeymap.bas"
#include "library/aquariuswritetext.bas"
#include "library/aquariuskeychk.bas"

dim xpos as uinteger at $3810
dim ypos as uinteger at $3812
dim ee0 as uinteger at $3814
dim ee1 as uinteger at $3816
dim roomcur as uinteger at $3818

'-------------------------------------------------------------------------------

sub bytechrbgxy(txpos as uinteger,typos as uinteger,tval as ubyte,tc0 as ubyte,tc1 as ubyte,ta0 as ubyte,ta1 as ubyte,tdr as ubyte):
  dim zlp as uinteger at $380E
  dim zte as uinteger at $380C
  dim zae as uinteger at $3808
  dim zva as ubyte at $380A:zva=tval
  if (tdr band 1)=0 then:
    for zpl=0 to 7
      zte=tc1:zae=ta1
      if (zva band 128)>0 then
        zte=tc0:zae=ta0:end if
      aquariusputchar(txpos+zpl,typos,zte,zae)
      zva=zva*2
      next
    else
    for zpl=0 to 7
      zte=tc1:zae=ta1
      if (zva band 1)>0 then
        zte=tc0:zae=ta0:end if
      aquariusputchar(txpos+zpl,typos,zte,zae)
      zva=zva/2
      next
    end if
  end sub

sub drawroom(troom as uinteger):
  dim zhl as uinteger at $3806
  for zhl=0 to 35
    bytechrbgxy((int(zhl/12)*8),((zhl mod 12)*2),peek(@roombmap02+(troom*36)+zhl),65,66,peek(@roomcl01+troom)*$11,$88,0)
    bytechrbgxy((int(zhl/12)*8),((zhl mod 12)*2)+1,peek(@roombmap02+(troom*36)+zhl),65,66,peek(@roomcl01+troom)*$11,$88,0)
    bytechrbgxy((32-(int(zhl/12)*8)),((zhl mod 12)*2),peek(@roombmap02+(troom*36)+zhl),65,66,peek(@roomcl01+troom)*$11,$88,1)
    bytechrbgxy((32-(int(zhl/12)*8)),((zhl mod 12)*2)+1,peek(@roombmap02+(troom*36)+zhl),65,66,peek(@roomcl01+troom)*$11,$88,1)
    next
  if (troom=11) or (troom=13) then:
    for zhl=0 to 23
      aquariusputchar(1+((troom band 4)*9)+((troom band 4)/4),zhl,0,0)
      next
    end if
  end sub

'-------------------------------------------------------------------------------

'- aquariuscls($86,$78) '- not working properly, weird...
aquariusfillbytes($2FFF,$86,$03E8) '- chr
aquariusfillbytes($3400,$00,$03E8) '- attr

loopiiii:

'for ee0=0 to 13
'  drawroom(ee0)
'  'bytechrbgxy(4,3,%11101010,65,66,$12,$34,0)
'  aquariusdelay(50000)
'  aquariusdelay(50000)
'  aquariusdelay(50000)
'  aquariusdelay(50000)
'  next
'goto loopiiii

xpos=20:ypos=18:roomcur=6
drawroom(roomcur)
aquariusputchar(xpos,ypos,code("a"),$00)

'for ee0=0 to 15
'  poke $3400+ee0,ee0*$11
'  next

'-------------------------------------------------------------------------------

loopgameplay:

if ((aquariuskeymap(%10000000) band  2)<>0) then:
  aquariusputchar(xpos,ypos,code("a"),$88)
  ypos=ypos+1
  aquariusputchar(xpos,ypos,code("a"),$00)
  end if  '- w

if ((aquariuskeymap(%01000000) band  4)<>0) then:
  aquariusputchar(xpos,ypos,code("a"),$88)
  ypos=ypos-1
  aquariusputchar(xpos,ypos,code("a"),$00)
  end if  '- s

if ((aquariuskeymap(%00100000) band 16)<>0) then:
  aquariusputchar(xpos,ypos,code("a"),$88)
  xpos=xpos-1
  aquariusputchar(xpos,ypos,code("a"),$00)
  end if  '- a

if ((aquariuskeymap(%01000000) band 32)<>0) then:
  aquariusputchar(xpos,ypos,code("a"),$88)
  xpos=xpos+1
  aquariusputchar(xpos,ypos,code("a"),$00)
  end if  '- d

if xpos>38 then:
  roomcur=peek(@neighbrooms01+(roomcur*4))
  drawroom(roomcur)
  xpos=1
  end if

if xpos<1 then:
  roomcur=peek(@neighbrooms01+(roomcur*4)+1)
  drawroom(roomcur)
  xpos=38
  end if

if ypos>22 then:
  roomcur=peek(@neighbrooms01+(roomcur*4)+2)
  drawroom(roomcur)
  ypos=1
  end if

if ypos<1 then:
  roomcur=peek(@neighbrooms01+(roomcur*4)+3)
  drawroom(roomcur)
  ypos=22
  end if

'aquariusdelay(2500)
aquariusdelay(10000)

goto loopgameplay


'-------------------------------------------------------------------------------

goto roombmap01end
roombmap01:

roomq00:

roomcl01:
asm
  defb 4,4,4,4,4,0,3,3,5,1,1,13,2,3
  end asm

neighbrooms01:
asm
  defb  3, 4, 1, 5
  defb  2, 2, 4, 0
  defb  1, 1,11, 3
  defb  4, 0, 2, 3
  defb  0, 3, 4, 1
  defb  5, 5, 0, 9
  defb  6, 6,12, 7
  defb  7, 7, 6,15
  defb  8, 8, 9,15
  defb  9, 9, 5, 8
  defb 10,10,10,13
  defb 12,11,11, 2
  defb 13,11,12, 6
  defb 13,12,10,13
  end asm

spritq1:
asm
  defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00
  end asm

roombmap01end:

'-------------------------------------------------------------------------------

goto roombmap02end
roombmap02:

roomp00:
asm
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %11110000
  defb %11110000
  defb %11110000
  defb %11110000
  defb %11111111
  defb %11111111
  defb %00000011
  defb %00000011
  defb %11110011
  defb %11111111 ;
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %11000011
  defb %11000011
  defb %00000000
  defb %00000000
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  end asm

roomp01:
asm
  defb %11110011 ;
  defb %00000011
  defb %00000011
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %11110011
  defb %11110011
  defb %00000011
  defb %00000011
  defb %11111111
  defb %00111111 ;
  defb %00000011
  defb %00000011
  defb %11110011
  defb %11110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %11000011 ;
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %11000011
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp02:
asm
  defb %11111111 ;
  defb %00000011
  defb %00000011
  defb %11110011
  defb %11110011
  defb %00000011
  defb %00000011
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00110011 ;
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110011
  defb %00110000
  defb %00110000
  defb %00111111
  defb %00111111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00111100 ;
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp03:
asm
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111
  defb %11110000
  defb %11110000
  defb %11110011
  defb %11110011
  defb %00000011
  defb %00000011
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %11001111
  defb %11001111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111
  defb %00000011
  defb %00000011
  defb %00110011
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111
  defb %00111100
  defb %00111100
  defb %00111100
  defb %00111100
  defb %00111100
  defb %00111100
  defb %00111100
  end asm

roomp04:
asm
  defb %11111111 ;
  defb %00000011
  defb %00000011
  defb %11110011
  defb %11110011
  defb %11110000
  defb %11110000
  defb %11111111
  defb %11111111
  defb %00000011
  defb %00000011
  defb %11111111
  defb %00110011 ;
  defb %00000011
  defb %00000011
  defb %11111111
  defb %11111111
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  end asm

roomp05:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11101010 ;
  defb %00111111
  defb %00111111
  defb %00111111
  defb %00111111
  defb %00001111
  defb %00001111
  defb %00001111
  defb %00001111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %10000001 ;
  defb %10000001
  defb %10000001
  defb %11111111
  defb %11111111
  defb %11111111
  defb %11111111
  defb %11000011
  defb %11000011
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp06:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11101010 ;
  defb %00111111
  defb %00111111
  defb %00111111
  defb %00111111
  defb %00001111
  defb %00001111
  defb %00001111
  defb %00001111
  defb %00000000
  defb %00000000
  defb %11111111
  defb %10000001 ;
  defb %10000001
  defb %10000001
  defb %11111111
  defb %11111111
  defb %11111111
  defb %11111111
  defb %11000011
  defb %11000011
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp07:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp08:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp09:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roomp10:
asm
  defb %11111111 ;
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  end asm

roomp11:
asm
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  end asm

roomp12:
asm
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %00000000 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  end asm

roomp13:
asm
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %11111111
  defb %11111111 ;
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  defb %00000000
  end asm

roombmap02end:

