rm adventure.asm adventure.rom
zxb.py library/aquariusromheader.bas --org=$((0x0000))
dd ibs=1 count=$((0x0010)) skip=$((0x0010)) if=aquariusromheader.bin of=adventure.tmr
rm aquariusromheader.bin
zxb.py adventure.bas --asm --org=$((0xE010))
zxb.py adventure.bas --org=$((0xE010))
cat adventure.bin >> adventure.tmr
# filesize=$(stat -c%s "adventure.tmr")
dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin
cat _dummybytes.bin >> adventure.tmr
rm _dummybytes.bin adventure.bin
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=adventure.tmr of=adventure.rom
rm adventure.tmr

mame aquarius -video soft -w -bp ~/.mess/roms -skip_gameinfo -resolution0 640x400 -aspect0 8:6 -skip_gameinfo -cart1 adventure.rom

#mess aquarius -skip_gameinfo -resolution0 640x400 -aspect0 8:6 -cart1 adventure.rom
# mess aquarius -resolution0 320x200 -cart1 adventure.rom
# mess aquarius -cart1 adventure.rom


