	org 57360
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
	ld hl, 1000
	push hl
	ld hl, 134
	push hl
	ld hl, 12287
	push hl
	call _aquariusfillbytes
	ld hl, 1000
	push hl
	ld hl, 0
	push hl
	ld hl, 13312
	push hl
	call _aquariusfillbytes
__LABEL__loopiiii:
	ld hl, 20
	ld (_xpos), hl
	ld hl, 18
	ld (_ypos), hl
	ld hl, 6
	ld (_roomcur), hl
	push hl
	call _drawroom
	xor a
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
__LABEL__loopgameplay:
	ld a, 128
	call _aquariuskeymap
	push af
	ld h, 2
	pop af
	and h
	or a
	jp z, __LABEL1
	ld a, 136
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
	ld hl, (_ypos)
	inc hl
	ld (_ypos), hl
	xor a
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
__LABEL1:
	ld a, 64
	call _aquariuskeymap
	push af
	ld h, 4
	pop af
	and h
	or a
	jp z, __LABEL3
	ld a, 136
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
	ld hl, (_ypos)
	dec hl
	ld (_ypos), hl
	xor a
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
__LABEL3:
	ld a, 32
	call _aquariuskeymap
	push af
	ld h, 16
	pop af
	and h
	or a
	jp z, __LABEL5
	ld a, 136
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
	ld hl, (_xpos)
	dec hl
	ld (_xpos), hl
	xor a
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
__LABEL5:
	ld a, 64
	call _aquariuskeymap
	push af
	ld h, 32
	pop af
	and h
	or a
	jp z, __LABEL7
	ld a, 136
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
	ld hl, (_xpos)
	inc hl
	ld (_xpos), hl
	xor a
	push af
	ld a, 97
	push af
	ld hl, (_ypos)
	push hl
	ld hl, (_xpos)
	push hl
	call _aquariusputchar
__LABEL7:
	ld hl, 38
	ld de, (_xpos)
	or a
	sbc hl, de
	jp nc, __LABEL9
	ld hl, (_roomcur)
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__neighbrooms01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_roomcur), hl
	push hl
	call _drawroom
	ld hl, 1
	ld (_xpos), hl
__LABEL9:
	ld de, 1
	ld hl, (_xpos)
	or a
	sbc hl, de
	jp nc, __LABEL11
	ld hl, (_roomcur)
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__neighbrooms01
	add hl, de
	inc hl
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_roomcur), hl
	push hl
	call _drawroom
	ld hl, 38
	ld (_xpos), hl
__LABEL11:
	ld hl, 22
	ld de, (_ypos)
	or a
	sbc hl, de
	jp nc, __LABEL13
	ld hl, (_roomcur)
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__neighbrooms01
	add hl, de
	inc hl
	inc hl
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_roomcur), hl
	push hl
	call _drawroom
	ld hl, 1
	ld (_ypos), hl
__LABEL13:
	ld de, 1
	ld hl, (_ypos)
	or a
	sbc hl, de
	jp nc, __LABEL15
	ld hl, (_roomcur)
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, __LABEL__neighbrooms01
	add hl, de
	inc hl
	inc hl
	inc hl
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	ld (_roomcur), hl
	push hl
	call _drawroom
	ld hl, 22
	ld (_ypos), hl
__LABEL15:
	ld hl, 10000
	push hl
	call _aquariusdelay
	jp __LABEL__loopgameplay
	jp __LABEL__roombmap01end
__LABEL__roombmap01:
__LABEL__roomq00:
__LABEL__roomcl01:
#line 149
		defb 4,4,4,4,4,0,3,3,5,1,1,13,2,3
#line 150
__LABEL__neighbrooms01:
#line 154
		defb  3, 4, 1, 5
		defb  2, 2, 4, 0
		defb  1, 1,11, 3
		defb  4, 0, 2, 3
		defb  0, 3, 4, 1
		defb  5, 5, 0, 9
		defb  6, 6,12, 7
		defb  7, 7, 6,15
		defb  8, 8, 9,15
		defb  9, 9, 5, 8
		defb 10,10,10,13
		defb 12,11,11, 2
		defb 13,11,12, 6
		defb 13,12,10,13
#line 168
__LABEL__spritq1:
#line 172
		defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
		defb $00,$00,$00,$00,$00,$00,$00,$00
		defb $00,$00,$00,$00,$00,$00,$00,$00
		defb $00,$00,$00,$00,$00,$00,$00,$00
#line 176
__LABEL__roombmap01end:
	jp __LABEL__roombmap02end
__LABEL__roombmap02:
__LABEL__roomp00:
#line 187
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11110000
		defb %11110000
		defb %11110000
		defb %11110000
		defb %11111111
		defb %11111111
		defb %00000011
		defb %00000011
		defb %11110011
		defb %11111111
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000011
		defb %11000011
		defb %00000000
		defb %00000000
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
#line 223
__LABEL__roomp01:
#line 227
		defb %11110011
		defb %00000011
		defb %00000011
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11110011
		defb %11110011
		defb %00000011
		defb %00000011
		defb %11111111
		defb %00111111
		defb %00000011
		defb %00000011
		defb %11110011
		defb %11110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %11000011
		defb %00000000
		defb %00000000
		defb %00000000
#line 263
__LABEL__roomp02:
#line 267
		defb %11111111
		defb %00000011
		defb %00000011
		defb %11110011
		defb %11110011
		defb %00000011
		defb %00000011
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110011
		defb %00110000
		defb %00110000
		defb %00111111
		defb %00111111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00111100
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
#line 303
__LABEL__roomp03:
#line 307
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %11110000
		defb %11110000
		defb %11110011
		defb %11110011
		defb %00000011
		defb %00000011
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11001111
		defb %11001111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000011
		defb %00000011
		defb %00110011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00111100
		defb %00111100
		defb %00111100
		defb %00111100
		defb %00111100
		defb %00111100
		defb %00111100
#line 343
__LABEL__roomp04:
#line 347
		defb %11111111
		defb %00000011
		defb %00000011
		defb %11110011
		defb %11110011
		defb %11110000
		defb %11110000
		defb %11111111
		defb %11111111
		defb %00000011
		defb %00000011
		defb %11111111
		defb %00110011
		defb %00000011
		defb %00000011
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
#line 383
__LABEL__roomp05:
#line 387
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11101010
		defb %00111111
		defb %00111111
		defb %00111111
		defb %00111111
		defb %00001111
		defb %00001111
		defb %00001111
		defb %00001111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %10000001
		defb %10000001
		defb %10000001
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11000011
		defb %11000011
		defb %00000000
		defb %00000000
		defb %00000000
#line 423
__LABEL__roomp06:
#line 427
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11101010
		defb %00111111
		defb %00111111
		defb %00111111
		defb %00111111
		defb %00001111
		defb %00001111
		defb %00001111
		defb %00001111
		defb %00000000
		defb %00000000
		defb %11111111
		defb %10000001
		defb %10000001
		defb %10000001
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11000011
		defb %11000011
		defb %00000000
		defb %00000000
		defb %00000000
#line 463
__LABEL__roomp07:
#line 467
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
#line 503
__LABEL__roomp08:
#line 507
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
#line 543
__LABEL__roomp09:
#line 547
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
#line 583
__LABEL__roomp10:
#line 587
		defb %11111111
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
#line 623
__LABEL__roomp11:
#line 627
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
#line 663
__LABEL__roomp12:
#line 667
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
#line 703
__LABEL__roomp13:
#line 707
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
#line 743
__LABEL__roombmap02end:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_aquariusfillbytes:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+6)
		ld d, (ix+9)
		ld e, (ix+8)
		ld h, (ix+5)
		ld l, (ix+4)
fillbytes:
		ld (hl),b
		inc hl
		dec de
		ld a,d
		or e
		jr nz,fillbytes
#line 13
_aquariusfillbytes__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_aquariuscls:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 12288
	push hl
	ld hl, 1000
	push hl
	ld a, (ix+5)
	ld l, a
	ld h, 0
	push hl
	call _aquariusfillbytes
	ld hl, 13312
	push hl
	ld hl, 1000
	push hl
	ld a, (ix+7)
	ld l, a
	ld h, 0
	push hl
	call _aquariusfillbytes
_aquariuscls__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_aquariusputchar:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 40
	call __MUL16_FAST
	ld de, 12288
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+9)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 40
	call __MUL16_FAST
	ld de, 13312
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+11)
	pop hl
	ld (hl), a
_aquariusputchar__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_aquariusputtile:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_aquariusputtile_tylp), hl
	jp __LABEL16
__LABEL20:
	ld hl, 0
	ld (_aquariusputtile_txlp), hl
	jp __LABEL22
__LABEL26:
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld hl, (_aquariusputtile_tylp)
	ex de, hl
	pop hl
	add hl, de
	ld de, 40
	call __MUL16_FAST
	ld de, 12288
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld hl, (_aquariusputtile_txlp)
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld hl, (_aquariusputtile_tylp)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	pop hl
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_aquariusputtile_txlp)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld hl, (_aquariusputtile_tylp)
	ex de, hl
	pop hl
	add hl, de
	ld de, 40
	call __MUL16_FAST
	ld de, 13312
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld hl, (_aquariusputtile_txlp)
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld a, (ix+11)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_aquariusputtile_tylp)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	pop hl
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_aquariusputtile_txlp)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
__LABEL27:
	ld hl, (_aquariusputtile_txlp)
	inc hl
	ld (_aquariusputtile_txlp), hl
__LABEL22:
	ld hl, (_aquariusputtile_txlp)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	dec hl
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL26
__LABEL25:
__LABEL21:
	ld hl, (_aquariusputtile_tylp)
	inc hl
	ld (_aquariusputtile_tylp), hl
__LABEL16:
	ld hl, (_aquariusputtile_tylp)
	push hl
	ld a, (ix+11)
	dec a
	ld l, a
	ld h, 0
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL20
__LABEL19:
_aquariusputtile__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_aquariuswaitvbl:
	push ix
	ld ix, 0
	add ix, sp
#line 1
loopvbl:
		in a, ($FD)
		jr z,loopvbl
#line 4
_aquariuswaitvbl__leave:
	ld sp, ix
	pop ix
	ret
_aquariusdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d, (ix+5)
		ld e, (ix+4)
loopdelay:
		dec de
		ld a,d
		or e
		jp nz, loopdelay
#line 8
_aquariusdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_aquariuskeymap:
#line 1
		xor $FF
		ld b,a
		ld c,$FF
		in a, (c)
#line 5
_aquariuskeymap__leave:
	ret
_aquariuswritetext:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_aquariuswritetext_txlp), hl
	jp __LABEL28
__LABEL32:
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 40
	call __MUL16_FAST
	ld de, 12288
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_aquariuswritetext_txlp)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld hl, (_aquariuswritetext_txlp)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 40
	call __MUL16_FAST
	ld de, 13312
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_aquariuswritetext_txlp)
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix+13)
	pop hl
	ld (hl), a
__LABEL33:
	ld hl, (_aquariuswritetext_txlp)
	inc hl
	ld (_aquariuswritetext_txlp), hl
__LABEL28:
	ld hl, (_aquariuswritetext_txlp)
	push hl
	ld l, (ix+10)
	ld h, (ix+11)
	dec hl
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL32
__LABEL31:
_aquariuswritetext__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_aquariuskeychk:
#line 1
		xor a
		ld ($380b),a
		ld ($380c),a
		ld ($380e),a
		ld ($380f),a
		call $1e80
#line 7
_aquariuskeychk__leave:
	ret
_bytechrbgxy:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	inc sp
	ld a, (ix+9)
	ld (_bytechrbgxy_zva), a
	ld a, (ix+19)
	push af
	ld h, 1
	pop af
	and h
	sub 1
	jp nc, __LABEL34
	ld (ix-1), 0
	jp __LABEL36
__LABEL40:
	ld a, (ix+13)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zte), hl
	ld a, (ix+17)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zae), hl
	ld a, (_bytechrbgxy_zva)
	push af
	ld h, 128
	pop af
	and h
	push af
	xor a
	pop hl
	cp h
	jp nc, __LABEL43
	ld a, (ix+11)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zte), hl
	ld a, (ix+15)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zae), hl
__LABEL43:
	ld hl, (_bytechrbgxy_zae)
	ld a, l
	push af
	ld hl, (_bytechrbgxy_zte)
	ld a, l
	push af
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _aquariusputchar
	ld a, (_bytechrbgxy_zva)
	add a, a
	ld (_bytechrbgxy_zva), a
__LABEL41:
	ld a, (ix-1)
	inc a
	ld (ix-1), a
__LABEL36:
	ld a, (ix-1)
	push af
	ld a, 7
	pop hl
	cp h
	jp nc, __LABEL40
__LABEL39:
	jp __LABEL35
__LABEL34:
	ld (ix-1), 0
	jp __LABEL44
__LABEL48:
	ld a, (ix+13)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zte), hl
	ld a, (ix+17)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zae), hl
	ld a, (_bytechrbgxy_zva)
	push af
	ld h, 1
	pop af
	and h
	push af
	xor a
	pop hl
	cp h
	jp nc, __LABEL51
	ld a, (ix+11)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zte), hl
	ld a, (ix+15)
	ld l, a
	ld h, 0
	ld (_bytechrbgxy_zae), hl
__LABEL51:
	ld hl, (_bytechrbgxy_zae)
	ld a, l
	push af
	ld hl, (_bytechrbgxy_zte)
	ld a, l
	push af
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _aquariusputchar
	ld a, (_bytechrbgxy_zva)
	srl a
	ld (_bytechrbgxy_zva), a
__LABEL49:
	ld a, (ix-1)
	inc a
	ld (ix-1), a
__LABEL44:
	ld a, (ix-1)
	push af
	ld a, 7
	pop hl
	cp h
	jp nc, __LABEL48
__LABEL47:
__LABEL35:
_bytechrbgxy__leave:
	exx
	ld hl, 16
__EXIT_FUNCTION:
	ld sp, ix
	pop ix
	pop de
	add hl, sp
	ld sp, hl
	push de
	exx
	ret
_drawroom:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_drawroom_zhl), hl
	jp __LABEL52
__LABEL56:
	xor a
	push af
	ld a, 136
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, __LABEL__roomcl01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld h, 17
	call __MUL8_FAST
	push af
	ld a, 66
	push af
	ld a, 65
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 36
	call __MUL16_FAST
	ex de, hl
	ld hl, __LABEL__roombmap02
	add hl, de
	push hl
	ld hl, (_drawroom_zhl)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __MODU16
	add hl, hl
	push hl
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __MUL32
	push hl
	call _bytechrbgxy
	xor a
	push af
	ld a, 136
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, __LABEL__roomcl01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld h, 17
	call __MUL8_FAST
	push af
	ld a, 66
	push af
	ld a, 65
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 36
	call __MUL16_FAST
	ex de, hl
	ld hl, __LABEL__roombmap02
	add hl, de
	push hl
	ld hl, (_drawroom_zhl)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __MODU16
	add hl, hl
	inc hl
	push hl
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __MUL32
	push hl
	call _bytechrbgxy
	ld a, 1
	push af
	ld a, 136
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, __LABEL__roomcl01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld h, 17
	call __MUL8_FAST
	push af
	ld a, 66
	push af
	ld a, 65
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 36
	call __MUL16_FAST
	ex de, hl
	ld hl, __LABEL__roombmap02
	add hl, de
	push hl
	ld hl, (_drawroom_zhl)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __MODU16
	add hl, hl
	push hl
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __MUL32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	call __SUB32
	push hl
	call _bytechrbgxy
	ld a, 1
	push af
	ld a, 136
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, __LABEL__roomcl01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld h, 17
	call __MUL8_FAST
	push af
	ld a, 66
	push af
	ld a, 65
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 36
	call __MUL16_FAST
	ex de, hl
	ld hl, __LABEL__roombmap02
	add hl, de
	push hl
	ld hl, (_drawroom_zhl)
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __MODU16
	add hl, hl
	inc hl
	push hl
	ld hl, (_drawroom_zhl)
	ld de, 12
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	call __MUL32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	call __SUB32
	push hl
	call _bytechrbgxy
__LABEL57:
	ld hl, (_drawroom_zhl)
	inc hl
	ld (_drawroom_zhl), hl
__LABEL52:
	ld hl, (_drawroom_zhl)
	push hl
	ld hl, 35
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL56
__LABEL55:
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 11
	pop hl
	call __EQ16
	push af
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 13
	pop hl
	call __EQ16
	ld h, a
	pop af
	or h
	or a
	jp z, __LABEL59
	ld hl, 0
	ld (_drawroom_zhl), hl
	jp __LABEL60
__LABEL64:
	xor a
	push af
	xor a
	push af
	ld hl, (_drawroom_zhl)
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 4
	pop hl
	call __BAND16
	ld de, 9
	call __MUL16_FAST
	inc hl
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 4
	pop hl
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _aquariusputchar
__LABEL65:
	ld hl, (_drawroom_zhl)
	inc hl
	ld (_drawroom_zhl), hl
__LABEL60:
	ld hl, (_drawroom_zhl)
	push hl
	ld hl, 23
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL64
__LABEL63:
__LABEL59:
_drawroom__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1727 "adventure.bas"
#line 1 "mul8.asm"
__MUL8:		; Performs 8bit x 8bit multiplication
		PROC
	
		;LOCAL __MUL8A
		LOCAL __MUL8LOOP
		LOCAL __MUL8B
				; 1st operand (byte) in A, 2nd operand into the stack (AF)
		pop hl	; return address
		ex (sp), hl ; CALLE convention
	
;;__MUL8_FAST: ; __FASTCALL__ entry
	;;	ld e, a
	;;	ld d, 0
	;;	ld l, d
	;;	
	;;	sla h	
	;;	jr nc, __MUL8A
	;;	ld l, e
	;;
;;__MUL8A:
	;;
	;;	ld b, 7
;;__MUL8LOOP:
	;;	add hl, hl
	;;	jr nc, __MUL8B
	;;
	;;	add hl, de
	;;
;;__MUL8B:
	;;	djnz __MUL8LOOP
	;;
	;;	ld a, l ; result = A and HL  (Truncate to lower 8 bits)
	
__MUL8_FAST: ; __FASTCALL__ entry, a = a * h (8 bit mul) and Carry
	
	    ld b, 8
	    ld l, a
	    xor a
	
__MUL8LOOP:
	    add a, a ; a *= 2
	    sla l
	    jp nc, __MUL8B
	    add a, h
	
__MUL8B:
	    djnz __MUL8LOOP
		
		ret		; result = HL
		ENDP
	
#line 1728 "adventure.bas"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1729 "adventure.bas"
#line 1 "sub32.asm"
	; SUB32 
	; TOP of the stack - DEHL
	; Pops operand out of the stack (CALLEE)
	; and returns result in DEHL
	; Operands come reversed => So we swap then using EX (SP), HL
	
__SUB32:
		exx
		pop bc		; Return address
		exx
	
		ex (sp), hl
		pop bc
		or a 
		sbc hl, bc
	
		ex de, hl
		ex (sp), hl
		pop bc
		sbc hl, bc
		ex de, hl
	
		exx
		push bc		; Put return address
		exx
		ret
		
	
	
#line 1730 "adventure.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1731 "adventure.bas"
#line 1 "mul32.asm"
#line 1 "_mul32.asm"
	
; Ripped from: http://www.andreadrian.de/oldcpu/z80_number_cruncher.html#moztocid784223
	; Used with permission.
	; Multiplies 32x32 bit integer (DEHL x D'E'H'L')
	; 64bit result is returned in H'L'H L B'C'A C
	
	
__MUL32_64START:
			push hl
			exx
			ld b, h
			ld c, l		; BC = Low Part (A)
			pop hl		; HL = Load Part (B)
			ex de, hl	; DE = Low Part (B), HL = HightPart(A) (must be in B'C')
			push hl
	
			exx
			pop bc		; B'C' = HightPart(A)
			exx			; A = B'C'BC , B = D'E'DE
	
				; multiply routine 32 * 32bit = 64bit
				; h'l'hlb'c'ac = b'c'bc * d'e'de
				; needs register a, changes flags
				;
				; this routine was with tiny differences in the
				; sinclair zx81 rom for the mantissa multiply
	
__LMUL:
	        and     a               ; reset carry flag
	        sbc     hl,hl           ; result bits 32..47 = 0
	        exx
	        sbc     hl,hl           ; result bits 48..63 = 0
	        exx
	        ld      a,b             ; mpr is b'c'ac
	        ld      b,33            ; initialize loop counter
	        jp      __LMULSTART  
	
__LMULLOOP:
	        jr      nc,__LMULNOADD  ; JP is 2 cycles faster than JR. Since it's inside a LOOP
	                                ; it can save up to 33 * 2 = 66 cycles
	                                ; But JR if 3 cycles faster if JUMP not taken!
	        add     hl,de           ; result += mpd
	        exx
	        adc     hl,de
	        exx
	
__LMULNOADD:
	        exx
	        rr      h               ; right shift upper
	        rr      l               ; 32bit of result
	        exx
	        rr      h
	        rr      l
	
__LMULSTART:
	        exx
	        rr      b               ; right shift mpr/
	        rr      c               ; lower 32bit of result
	        exx
	        rra                     ; equivalent to rr a
	        rr      c
	        djnz    __LMULLOOP
	
			ret						; result in h'l'hlb'c'ac
	       
#line 2 "mul32.asm"
	
__MUL32:	; multiplies 32 bit un/signed integer.
				; First operand stored in DEHL, and 2nd onto stack
				; Lowest part of 2nd operand on top of the stack
				; returns the result in DE.HL
			exx
			pop hl	; Return ADDRESS
			pop de	; Low part
			ex (sp), hl ; CALLEE -> HL = High part
			ex de, hl
			call __MUL32_64START
	
__TO32BIT:  ; Converts H'L'HLB'C'AC to DEHL (Discards H'L'HL)
			exx
			push bc
			exx
			pop de
			ld h, a
			ld l, c
			ret
	
	
#line 1732 "adventure.bas"
#line 1 "eq16.asm"
__EQ16:	; Test if 16bit values HL == DE
		; Returns result in A: 0 = False, FF = True
			or a	; Reset carry flag
			sbc hl, de 
	
			ld a, h
			or l
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 1733 "adventure.bas"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1734 "adventure.bas"
	
ZXBASIC_USER_DATA:
	_aquariuswritetext_txlp EQU 14592
	_bytechrbgxy_zlp EQU 14350
	_aquariuswritetext_tylp EQU 14594
	_ee1 EQU 14358
	_ee0 EQU 14356
	_xpos EQU 14352
	_ypos EQU 14354
	_aquariusputtile_txlp EQU 14592
	_bytechrbgxy_zva EQU 14346
	_bytechrbgxy_zae EQU 14344
	_bytechrbgxy_zte EQU 14348
	_roomcur EQU 14360
	_drawroom_zhl EQU 14342
	_aquariusputtile_tylp EQU 14594
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
