rm example01.asm example01.rom
dd bs=$((0x4000)) count=1 if=/dev/zero of=dummy16k.bin
zxb.py example01.bas --asm --org=$((0x1FF0))
zxb.py example01.bas --org=$((0x1FF0))
dd ibs=1 skip=$((0x0010)) if=example01.bin of=example01.rom

cat dummy16k.bin >> example01.rom
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.rom of=example01.rom_

rm example01.rom example01.bin dummy16k.bin
mv example01.rom_ example01.rom

mame m5 -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 example01.rom

