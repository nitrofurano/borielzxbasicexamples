asm
  defb $00,$05,$20,$2E,$00

  ld sp,$7FFF
  end asm

#include "library/m5vdp.bas"
#include "library/m5filvrm.bas"
#include "library/m5ldirvm.bas"
#include "library/m5vpoke.bas"
#include "library/smsrnd.bas"
#include "library/arcade_b.bas"

'-------------------------------------------------------------------------------
'- defining variables

dim eee as uinteger at $7010
dim ee0 as uinteger at $7012
dim ee1 as uinteger at $7014
dim ee2 as uinteger at $7016
dim ee3 as uinteger at $7018
dim xdr as uinteger at $701A
dim trk as uinteger at $701C
dim spd as uinteger at $701E
dim eex as uinteger at $7020
dim eey as uinteger at $7022

'-------------------------------------------------------------------------------
'- whatever...

'msxcolor($6,$F,$1)
m5vdp(7,$61) 'm5vdp(7,F*16+4) '- ink*16+border - color(3,?,1)

'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
m5vdp(0,%00000010):m5vdp(1,%11100010) '- screen2,2 (graphics1)
'- vdp entries for screen2 - base vram addresses
m5vdp(2,$06) '- $1800
m5vdp(3,$FF) '- $2000
m5vdp(4,$03) '- $0000
m5vdp(5,$36) '- $1B00
m5vdp(6,$07) '- $3800

'-------------------------------------------------------------------------------

sub m5putchar(txp1 as uinteger,typ1 as uinteger,tch1 as ubyte):
  m5vpoke(6144+(typ1*32)+txp1,tch1)
  end sub

for ee1=$0000 to $17FF step $0800
  m5filvrm($0000+ee1,$F1,$0800)
  m5filvrm($2000+ee1,$1F,$0800)
  next

m5ldirvm($0100,@typeface01,768)
m5ldirvm($0900,@typeface01,768)
m5ldirvm($1100,@typeface01,768)

for ee1=$0000 to $17FF step $0800
  for ee0=0 to 15
    m5filvrm($2000+(ee0*8)+ee1,(ee0*$11) band $FF,8)
    next:next

m5filvrm($1800,$0C,768)

for ee0=0 to 15
  m5vpoke($1800+ee0,(ee0 band 255))
  m5vpoke($1900+ee0,(ee0 band 255))
  m5vpoke($1a00+ee0,(ee0 band 255))
  next

'xdr=5:trk=0:spd=0

eee=0:eex=20:eey=12
do

m5vpoke(6144+32+1,48+int(eee/10000) mod 10)
m5vpoke(6144+32+2,48+int(eee/1000) mod 10)
m5vpoke(6144+32+3,48+int(eee/100) mod 10)
m5vpoke(6144+32+4,48+int(eee/10) mod 10)
m5vpoke(6144+32+5,48+eee mod 10)

ee0=in($30)
m5putchar(3,3,48+int(ee0/100) mod 10)
m5putchar(4,3,48+int(ee0/10) mod 10)
m5putchar(5,3,48+ee0 mod 10)

ee0=in($31)
m5putchar(3,4,48+int(ee0/100) mod 10)
m5putchar(4,4,48+int(ee0/10) mod 10)
m5putchar(5,4,48+ee0 mod 10)

ee0=in($32)
m5putchar(3,5,48+int(ee0/100) mod 10)
m5putchar(4,5,48+int(ee0/10) mod 10)
m5putchar(5,5,48+ee0 mod 10)

ee0=in($33)
m5putchar(3,6,48+int(ee0/100) mod 10)
m5putchar(4,6,48+int(ee0/10) mod 10)
m5putchar(5,6,48+ee0 mod 10)

ee0=in($34)
m5putchar(3,7,48+int(ee0/100) mod 10)
m5putchar(4,7,48+int(ee0/10) mod 10)
m5putchar(5,7,48+ee0 mod 10)

ee0=in($35)
m5putchar(3,8,48+int(ee0/100) mod 10)
m5putchar(4,8,48+int(ee0/10) mod 10)
m5putchar(5,8,48+ee0 mod 10)

ee0=in($36)
m5putchar(3,9,48+int(ee0/100) mod 10)
m5putchar(4,9,48+int(ee0/10) mod 10)
m5putchar(5,9,48+ee0 mod 10)

ee0=in($37)
m5putchar(3,10,48+int(ee0/100) mod 10)
m5putchar(4,10,48+int(ee0/10) mod 10)
m5putchar(5,10,48+ee0 mod 10)

eex=eex+((ee0 band 1)/1)-((ee0 band 4)/4)
eey=eey+((ee0 band 8)/8)-((ee0 band 2)/2)
m5putchar(eex,eey,eee band 127)

asm
  halt
  end asm

eee=eee+1
loop

'-------------------------------------------------------------------------------




