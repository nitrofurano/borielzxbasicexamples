sub m5filvrm(tvram as uinteger,tvl as uinteger, tlen as uinteger):
  asm
    ld l,(ix+4)
    ld a,l
    out ($11),a

    ld h,(ix+5)
    ld a,h
    or $40
    out ($11),a

    ld b,(ix+9)
    ld c,(ix+8)

    m5filvrmloop:
    ld a,(ix+6)
    out ($10),a
    dec bc
    ld a,b
    or c
    jp nz,m5filvrmloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : 
'           HL - start address
'           A  - data byte
'           BC - length of the area to be written
'Registers: AF, BC
