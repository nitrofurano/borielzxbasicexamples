sub m5vpoke(tvram as uinteger,tvl as uinteger):
  asm
    ;ld h,(ix+5)
    ;ld l,(ix+4)
    ;ld a,(ix+6)

    ld l,(ix+4)
    ld a,l
    out ($11),a

    ld h,(ix+5)
    ld a,h
    or $40
    out ($11),a

    ld a,(ix+6)
    out ($10),a

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : A  - data byte
'           BC - length of the area to be written
'           HL - start address
'Registers: AF, BC
