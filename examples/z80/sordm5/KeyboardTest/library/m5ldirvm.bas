sub m5ldirvm(tvram as uinteger, tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
    ;call $005C

    ld a,e
    out ($11),a
    ;ld a,$78     ;- ld a,$38|$40 
    ld a,d
    or $40
    out ($11),a
    ; 2. Output tilemap data
    ;ld hl,Message
    ;ld bc,MessageEnd-Message  ; Counter for number of bytes to write

    m5ldirvmloop:
    ld a,(hl)    ; Get data byte
    out ($10),a
    inc hl       ; Point to next letter
    dec bc
    ld a,b
    or c
    jp nz,m5ldirvmloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
