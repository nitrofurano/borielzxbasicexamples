'- writes a ubyte value (bin) inside 8 bytes of string in ram
sub binubyte2stradr(tdecvb as ubyte,tadr as uinteger):
  t2decvb=tdecvb
  for i=0 to 7
    poke tadr+7-i,48+(t2decvb mod 2)
    t2decvb=int(t2decvb/2)
    next
  end sub
