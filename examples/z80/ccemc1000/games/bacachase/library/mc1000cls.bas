sub mc1000cls(tmsm as ubyte):
  dim tsz as uinteger
  dim tbv as ubyte
  dim tvr as uinteger
  tsz=512:tbv=0
  if (peek($00F5) band $F8)=$80 then:tsz=1024:end if   '- modes 128,130,132,134
  if (peek($00F5) band $FC)=$88 then:tsz=2048:end if   '- modes 136,138
  if (peek($00F5) band $FC)=$8C then:tsz=1536:end if   '- modes 140,142
  if (peek($00F5) band $F8)=$90 then:tsz=3072:end if   '- modes 144,146,148,150
  if (peek($00F5) band $F8)=$98 then:tsz=6144:end if   '- modes 152,154,156,158
  if (peek($00F5) band $84)=$80 then:tbv=$55*(tmsm band 3):end if   '- modes 4c
  if (peek($00F5) band $84)=$84 then:tbv=$FF*(tmsm band 1):end if   '- modes 2c
  if (peek($00F5) band $60)=$60 then:
    tbv=$00
    if (tmsm band 7)<4 then:
      tbv=63 bor ((tmsm band 3) shl 6)
      end if:end if
  if (peek($00F5) band $60)=$40 then:
    tbv=$00
    if (tmsm band 15)<8 then:
      tbv=15 bor ((tmsm band 7) shl 4)
      end if:end if
  if (peek($00F5) band $FC)=$00 then:
    tbv=32 bor ((tmsm band 1) shl 7)
    end if
  for tvr=0 to tsz-1
    poke $8000+tvr,tbv
    next
  end sub

