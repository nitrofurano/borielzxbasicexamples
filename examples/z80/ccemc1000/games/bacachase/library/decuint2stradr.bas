'- writes a uinteger value (dec) inside 5 bytes of string in ram
sub decuint2stradr(tdecv as uinteger,tadr as uinteger):
  t2decv=tdecv
  for i=0 to 4
    poke tadr+4-i,48+(t2decv mod 10):t2decv=int(t2decv/10)
    next
  end sub
