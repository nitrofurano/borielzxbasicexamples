#include "library/minivadersldir32b.bas"
#include "library/aquariusfillram.bas"
#include "library/mc1000screen.bas"
#include "library/mc1000cls.bas"
#include "library/mc1000vpokechr.bas"
#include "library/mc1000rnd.bas"
#include "library/bacachase_charset.bas"
#include "library/mc1000keymapinv.bas"

mc1000screen(152)
mc1000cls(0)

dim p1,p2,s1,s2,vl as integer
dim rseed, debug as uinteger
rseed=50
dim ee0,ee1,ee2,ee3,eex,eey as uinteger
dim kbbf as ubyte

sub mc1000writetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tloc2 as uinteger)
  for tq2=0 to tle2-1
    mc1000vpokechr((ty2*32)+tx2+tq2,peek(ttx2+tq2),tloc2)
    next
  end sub

'mc1000vpokechr(5,34,@charmap01-256)

for eey=0 to 15
  for eex=0 to 15
    mc1000vpokechr(7*32+15+eey*32+eex,eey*16+eex,@charmap01-256)
    next:next

'-----------------------------------------------------------------------

'#include "library/bacachase_charmapudgpalette.bas"

'- Bacachase - nov'11 - Paulo Silva (copyleft, gpl3) - rev 20111115170341
'- for the csscgc2011 competition, hosted by mojon twins

debug=0

'-----------------------------------------------
variables:
  dim c,seed as uinteger
  dim i,j,enam,cnt,stam,stg,score,hiscore as integer
  dim x,y,d,kprs as byte
  dim txmm(768) as byte:dim xin(768) as byte:dim yin(768) as byte
  dim stin(768) as byte:dim din(768) as byte
  gosub startup

  dim kmp0,kmp1,kmp2,kmp3,kmp4,kmp5,kmp6,kmp7 as ubyte


'-----------------------------------------------
do
  gosub title
  gosub gamestart
playloop:
  gosub stagestart
  gosub stageplay
  if stam>0 then: goto playloop:end if
loop

'-----------------------------------------------
startup:
  aquariusfillram($8000,$00,6*1024)   'bright 0:flash 0: inverse 0:border 0:ink 0:paper 0:cls
  hiscore=0
  return

'-----------------------------------------------
title:
aquariusfillram($8000,$00,6*1024)  'paper 4:bright 1:border 0:cls
aquariusfillram($8000,$FF,256*6)   'paper 2:for y=0 to 5:for x=0 to 31 step 4:print at y,x;chr$(32)+chr$(32)+chr$(32)+chr$(32):next:next

for c=0 to 15
  seed=mc1000rnd(seed)
  mc1000vpokechr((((seed mod 8)+15)*32)+(int(seed mod 32)),110,@charmap01-256)   'paper 4:ink 3:print at int(rnd*8)+15,int(rnd*32);chr$(145)
  next

mc1000writetext(2,18,@text01,@text02-@text01,@charmap01-256) 'ink 1:print at 18,2;"BACACHASE"
mc1000writetext(2,19,@text02,@text03-@text02,@charmap01-256) 'print at 19,2;chr$(127);" PAULO SILVA, '11"
mc1000writetext(2,20,@text03,@text04-@text03,@charmap01-256) 'print at 20,2;"PUSH ANY KEY"

'  pause 0
lpkbbf01:
  kbbf=mc1000keymapinv(1) bor mc1000keymapinv(2) bor mc1000keymapinv(4) bor mc1000keymapinv(8)
  kbbf=kbbf bor mc1000keymapinv(16) bor mc1000keymapinv(32) bor mc1000keymapinv(64) bor mc1000keymapinv(128)
  if kbbf=0 then
    goto lpkbbf01
    end if
  return

'-----------------------------------------------
gamestart:
  score=0:stg=1:stam=99
  return

'-----------------------------------------------
stagestart:
aquariusfillram($8000,$00,6*1024)   'paper 4:bright 1:border 0:cls
aquariusfillram($9000,$11,1024)     'test
aquariusfillram($9000,$00,1024)     'test

gosub displayscore
gosub displaystamina

enam=4+(4*stg):cnt=enam
kprs=0

seed=mc1000rnd(seed):x=seed mod 32  'x=int(rnd*32)
seed=mc1000rnd(seed):y=xeed mod 24  'y=int(rnd*24)

for i=0 to 255
  seed=mc1000rnd(seed):xin(i)=(seed mod 32)      'xin(i)=int(rnd*32)
  seed=mc1000rnd(seed):yin(i)=(seed mod 23)+2    'yin(i)=int(rnd*23)+2
  din(i)=0:if xin(i)>x then:din(i)=1:end if
  stin(i)=0
  next

'--- testing - wrong characters
for i=0 to enam-1
  mc1000vpokechr((yin(i)*32)+xin(i),34,@charmap01-256)    'ink 6:print at yin(i),xin(i);chr$(144+din(i))
  next
mc1000vpokechr((y*32)+x,51,@charmap01-256)     'ink 1:print at y,x;chr$(144+d)
'---

return

'-----------------------------------------------
stageplay:

lp01:
'pause 1

kmp0=mc1000keymapinv(1)

'7:4,3:4,1:1,4:1 - wsad

'if inkey$="w" and kprs=0 then
if ((mc1000keymapinv(128)band 4)<>0) and kprs=0 then:
'  print at y,x;" "
  y=y-1:kprs=1:if y<3 then:y=2:end if
'  ink 1:print at y,x;chr$(144+d)
  end if

'if inkey$="s" and kprs=0 then
if ((mc1000keymapinv(8)band 4)<>0) and kprs=0 then:
'  print at y,x;" "
  y=y+1:kprs=1:if y>22 then:y=23:end if
'  ink 1:print at y,x;chr$(144+d)
  end if

'if inkey$="a" and kprs=0 then
if ((mc1000keymapinv(2)band 1)<>0) and kprs=0 then:
'  print at y,x;" "
  x=x-1:kprs=1:d=1:if x<1 then:x=0:end if
'  ink 1:print at y,x;chr$(144+d)
  end if

'if inkey$="d" and kprs=0 then
if ((mc1000keymapinv(16)band 1)<>0) and kprs=0 then:
'  print at y,x;" "
  x=x+1:kprs=1:d=0:if x>30 then:x=31:end if
'  ink 1:print at y,x;chr$(144+d)
  end if

'if inkey$="p" and kprs=0 then
if ((mc1000keymapinv(1)band 4)<>0) and kprs=0 then:
'  print at y,x;" "
'  x=int(rnd*32):y=int(rnd*23)+2:kprs=1
'  ink 1:print at y,x;chr$(144+d)
  end if

'if inkey$="" and kprs=1 then
  kprs=0
  for i=0 to enam-1
    if stin(i)=0 then:
'      print at yin(i),xin(i);" "
      if yin(i)>y then:yin(i)=yin(i)-1:end if
      if yin(i)<y then:yin(i)=yin(i)+1:end if
      if xin(i)>x then:xin(i)=xin(i)-1:end if
      if xin(i)<x then:xin(i)=xin(i)+1:end if
      end if
    for j=0 to i-1
      if yin(i)=yin(j) and xin(i)=xin(j) then
        stin(i)=1:stin(j)=1:score=score+10
        end if
      next
    if hiscore<score then:hiscore=score:end if
    gosub displayscore
    if xin(i)>x then:din(i)=1:end if
    if xin(i)<x then:din(i)=0:end if
'    ink 6:print at yin(i),xin(i);chr$(144+din(i))
    if stin(i)=1 then
'      ink 5:print at yin(i),xin(i);chr$(146)
      end if
    next
  cnt=0
  for i=0 to enam-1
    if stin(i)=0 then:cnt=cnt+1:end if
    if x=xin(i) and y=yin(i) then:stam=stam-1:end if
    next
'  ink 1:print at y,x;chr$(144+d)
  gosub displaystamina
'  end if
'
if cnt>0 then: goto lp01:end if

stg=stg+1

gosub displayscore
gosub displaystamina

return

'-------------------------------------------------
displayscore:
'  paper 0:print at 0,10;"                      ";
'  ink 5:print at 0,0;"SCORE:";:ink 7:print score;
'  ink 5:print" HISCORE:";:ink 7:print hiscore;
'  paper 8
  return
'-------------------------------------------------
displaystamina:
'  paper 0:print at 1,28;"    ";
'  ink 5:print at 1,0;"ZOMBIES:";:ink 7:print cnt;
'  ink 5:print" STAMINA:";:ink 7:print stam;
'  ink 5:print" STAGE:";:ink 7:print stg;" "
'  paper 8
  return
'-------------------------------------------------

text01:
asm
defb "BACACHASE"
end asm
text02:
asm
defb 95
defb " PAULO SILVA, '11"
end asm
text03:
asm
defb "PUSH ANY KEY"
end asm
text04:
asm
defb "SCORE:"
end asm
text05:
asm
defb " HISCORE:"
end asm
text06:
asm
defb "ZOMBIES:"
end asm
text07:
asm
defb " STAMINA:"
end asm
text08:
asm
defb " STAGE:"
end asm
text09:

