rm bacachase.bin bacachase.asm mc1000header.bin mc1000tail.bin
zxb.py bacachase.bas --asm --org=$((0x0200))
zxb.py bacachase.bas --org=$((0x0200))
createmc1000header.py bacachase.bin
cat mc1000header.bin > bacachase.final.bin
cat bacachase.bin >> bacachase.final.bin
cat mc1000tail.bin >> bacachase.final.bin
mv bacachase.final.bin bacachase.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py bacachase.bin
zip bacachase.bin.wav.zip bacachase.bin.wav
rm bacachase.bin.wav

mame mc1000 -video soft -w -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass bacachase.bin.wav.zip
#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass bacachase.bin.wav.zip

# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]
