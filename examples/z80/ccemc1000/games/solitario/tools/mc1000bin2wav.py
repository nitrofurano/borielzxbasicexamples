#!/usr/bin/python
# -*- coding: latin-1 -*-
# - converts mc1000 binary cassette file into wav file  - Paulo Silva (GPL licence)

import os,sys
avl=0

finp_st=sys.argv[1]

if finp_st!="":
  avl=1
else:
  print "argv is empty"

if avl==1:
  foutraw_st=finp_st+".raw"
  foutwav_st=finp_st+".wav"

  finp_fl=open(finp_st,"r")
  foutraw_fl=open(foutraw_st,"w")

  print os.path.getsize(finp_st)   #-fix...

  for i in range (0,4096,1):
    for j in range (0,4,1):
      foutraw_fl.write(chr(0))
    for j in range (0,4,1):
      foutraw_fl.write(chr(255))

  for i in range (0,256,1):
    for j in range (0,8,1):
      foutraw_fl.write(chr(0))
    for j in range (0,8,1):
      foutraw_fl.write(chr(255))

  while True:
    byte_st=finp_fl.read(1)
    if len(byte_st)==0:break

    for j in range (0,4,1):
      foutraw_fl.write(chr(0))
    for j in range (0,4,1):
      foutraw_fl.write(chr(255))
    u=ord(byte_st)
    p=1

    for i in range (0,8,1):
      if u&(2**i)!=0:
        for j in range (0,4,1):
          foutraw_fl.write(chr(0))
        for j in range (0,4,1):
          foutraw_fl.write(chr(255))
        p=abs(p-1)
      else:
        for j in range (0,8,1):
          foutraw_fl.write(chr(0))
        for j in range (0,8,1):
          foutraw_fl.write(chr(255))

    if p==1:
      for j in range (0,4,1):
        foutraw_fl.write(chr(0))
      for j in range (0,4,1):
        foutraw_fl.write(chr(255))
    else:
      for j in range (0,8,1):
        foutraw_fl.write(chr(0))
      for j in range (0,8,1):
        foutraw_fl.write(chr(255))

  finp_fl.close()
  foutraw_fl.close()




  lngt=os.path.getsize(foutraw_st)
  #print os.path.getsize(foutraw_st)


  foutraw_fl=open(foutraw_st,"r")
  foutraw_fl.close()



  lngt36=lngt+36
  foutwav_fl=open(foutwav_st,"w")

  foutwav_fl.write(chr(0x52));foutwav_fl.write(chr(0x49))
  foutwav_fl.write(chr(0x46));foutwav_fl.write(chr(0x46))

  foutwav_fl.write(chr(lngt36%256));lngt36=int(lngt36/256.0)
  foutwav_fl.write(chr(lngt36%256));lngt36=int(lngt36/256.0)
  foutwav_fl.write(chr(lngt36%256));lngt36=int(lngt36/256.0)
  foutwav_fl.write(chr(lngt36%256))

  foutwav_fl.write(chr(0x57));foutwav_fl.write(chr(0x41));foutwav_fl.write(chr(0x56));foutwav_fl.write(chr(0x45))
  foutwav_fl.write(chr(0x66));foutwav_fl.write(chr(0x6D));foutwav_fl.write(chr(0x74));foutwav_fl.write(chr(0x20))
  foutwav_fl.write(chr(0x10));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x00))
  foutwav_fl.write(chr(0x01));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x01));foutwav_fl.write(chr(0x00))
  foutwav_fl.write(chr(0x11));foutwav_fl.write(chr(0x2B));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x00))
  foutwav_fl.write(chr(0x11));foutwav_fl.write(chr(0x2B));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x00))
  foutwav_fl.write(chr(0x01));foutwav_fl.write(chr(0x00));foutwav_fl.write(chr(0x08));foutwav_fl.write(chr(0x00))
  foutwav_fl.write(chr(0x64));foutwav_fl.write(chr(0x61));foutwav_fl.write(chr(0x74));foutwav_fl.write(chr(0x61))

  foutwav_fl.write(chr(lngt%256));lngt=int(lngt/256.0)
  foutwav_fl.write(chr(lngt%256));lngt=int(lngt/256.0)
  foutwav_fl.write(chr(lngt%256));lngt=int(lngt/256.0)
  foutwav_fl.write(chr(lngt%256))

  foutwav_fl.close()
  os.system("cat "+foutraw_st+" >> "+foutwav_st+" && rm "+foutraw_st)




