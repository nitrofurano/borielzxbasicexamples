sub fastcall mc1000cls(fillcolor as ubyte):
  asm
    ;col32:
    ;equ $80
    ;modbuf:
    ;equ $00f5
    ;vram:
    ;equ $8000
    
    ld d,a ; Save parameter for later use.
    ld a,($00f5)
    push af
    bit 7,a
    jr nz,grmode
    nongr:
    ld bc,511
    bit 6,a
    jr nz,sgmode
    alpha:
    ; Mode: Alphanumeric.
    ; Fill with spaces.
    ld a,$20
    jr fill
    sgmode:
    bit 5,a
    ld a,d
    jr nz,sg6
    sg4:
    ; Mode: Semigraphics Four.
    ; Fill with 2x2 colored block.
    ; Byte: 0-[C-C-C]-1-1-1-1, CCC=color (0~7)
    and 7
    rlca
    rlca
    rlca
    rlca
    or $0f
    jr fill
    sg6:
    ; Mode: Semigraphics Six.
    ; Fill with 3x2 colored block.
    ; Byte: [C-C]-1-1-1-1-1-1, CC=color (0~3)
    and 3
    rrca
    rrca
    or $3f
    jr fill
    grmode:
    bit 1,a
    ld a,d
    jr nz,rgn
    cgn:
    ; Mode: Color Graphics n.
    ; Byte: [C-C]-[C-C]-[C-C]-[C-C], CC=color (0~3)
    and $3
    ld d,a
    rlca
    rlca
    or d
    ld d,a
    rlca
    rlca
    rlca
    rlca
    or d
    jr gmsize
    rgn:
    ; Mode: Resolution Graphics n.
    ; Byte: $00 if color=0, or $ff if color=1.
    rra
    ld a,0
    sbc a,0
    gmsize:
    ; Gets fillsize from a table
    ; based on graphics mode.
    and $0e
    ld e,a
    ld d,0
    ld hl,gmsztb
    add hl,de
    ld c,(hl)
    inc hl
    ld b,(hl)
    fill:
    ld d,a
    pop af
    and $fe
    out ($80),a
    ld hl,$8000
    ld (hl),d
    ld d,h
    ld e,l
    inc de
    ldir
    ret
    gmsztb:
    dw 1023 ; Color Graphics 1
    dw 1023 ; Resolution Graphics 1
    dw 2047 ; Color Graphics 2
    dw 1535 ; Resolution Graphics 2
    dw 3071 ; Color Graphics 3
    dw 3071 ; Resolution Graphics 3
    dw 6143 ; Color Graphics 6
    dw 6143 ; Resolution Graphics 6
    
    end asm
  end sub
