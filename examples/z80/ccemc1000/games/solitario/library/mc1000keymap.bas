function fastcall mc1000keymap(tvl1 as ubyte) as ubyte
  asm
    di
    ld b,a
    ld a,$0E
    out($20),a
    ld a,b
    out($60),a
    ld a,$0F
    out($20),a
    in a,($40)
    ei
    end asm
  end function

''function mc1000keymap(tvl1 as uinteger) as ubyte
''  asm
''    di
''    ld a,$0E
''    out($20),a
''    ld a,(ix+4)
''    out($60),a
''    ld a,$0F
''    out($20),a
''    in a,($40)
''    ei
''    end asm
''  end function



'255-(2^linha)
'DI
'OUT ($20),$0E ;[seleciona IOA],
'OUT ($60),$EF ;(=11101111) [seleciona linha 4]
'OUT ($20),$0F ; (selection IOB)
'A=IN($40)
'EI
'- CALL #C465

'- 11111110 (#FE) - ct sh 8  0  X  P  H  @
'- 11111101 (#FD) - ct sh 9  1  Y  Q  I  A
'- 11111011 (#FB) - ct sh :  2  Z  R  J  B
'- 11110111 (#F7) - ct sh ;  3  en S  K  C
'- 11101111 (#EF) - ct sh ,  4  sp T  L  D
'- 11011111 (#DF) - ct sh -  5  ru U  M  E
'- 10111111 (#BF) - ct sh .  6  ^  V  N  F
'- 01111111 (#7F) - ct sh /  7  re W  O  G

'- joystick  1 2
'- Botão     A @
'- Acima     I H
'- Abaixo    Q P
'- Esquerda  Y X
'- Direita   1 0
