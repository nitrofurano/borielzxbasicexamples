sub mc1000plot(tpx as ubyte,tpy as ubyte):

  dim tad as integer
  dim tpyq, tpxq as integer
  dim tpm,tbt,tpk as ubyte
  tpyq=tpy:tpxq=tpx

  if (peek($00F5) band $84)=$80 then: '- 4c modes
    tpm=3
    if (tpx band 3)<>3 then
      tpm=3 shl ( ((tpx band 3) bxor 3) * 2)
      end if
    tbt=((peek($00F6) band 3) *$55 ) band tpm
    end if

  if (peek($00F5) band $84)=$84 then: '- 2c modes
    tpm=1
    if (tpx band 7)<>7 then
      tpm=1 shl ((tpx band 7) bxor 7) 
      end if
    tbt=((peek($00F6) band 1) *$FF ) band tpm
    end if

  if (peek($00F5) band $FC)=$98 then:   '-(128x192 4c)
    tad=((tpyq*32)+int(tpxq shr 2))mod 6144
    end if
  if (peek($00F5) band $FC)=$90 then:    '-(128x96 4c)
    tad=((tpyq*32)+int(tpxq shr 2))mod 3072
    end if
  if (peek($00F5) band $FC)=$88 then:   '- (128x64 4c)
    tad=((tpyq*32)+int(tpxq shr 2))mod 2048
    end if
  if (peek($00F5) band $FC)=$80 then:    '- (64x64 4c)
    tad=((tpyq*16)+int(tpxq shr 2))mod 1024
    end if

  if (peek($00F5) band $FC)=$9C then:   '-(256x192 2c)
    tad=((tpyq*32)+int(tpxq shr 3))mod 6144
    end if
  if (peek($00F5) band $FC)=$94 then:  '- (128x192 2c)
    tad=((tpyq*16)+int(tpxq shr 3))mod 3072
    end if
  if (peek($00F5) band $FC)=$8C then:  '- (128x96 2c)
    tad=((tpyq*16)+int(tpxq shr 3))mod 1536
    end if
  if (peek($00F5) band $FC)=$84 then:  '- (128x64 2c)
    tad=((tpyq*16)+int(tpxq shr 3))mod 1024
    end if

  if (peek($00F5) band $80)=$80 then:
    tpk=(peek($8000+tad))
    poke $8000+tad,(tpk band (tpm bxor 255 )) bor tbt
    end if
    
  '--------------------------------------------------------------

  '- not working - i don't know why
  'if peek($00F5) band $FC=$00 then:
  '  tad=((tpyq*32)+tpxq)mod 512
  '  poke $8000+tad,(( (peek($00F6)band 1) * 128) bor 32)
  '  end if

  '- these ones are somehow complicated! :D :D :D :D :D :D 
  'if peek($00F5) band $FC=$60 then: '- incompleto
  '  tad=(((int(tpyq/3))*32)+int(tpxq/2))mod 512
  '  tpm=2^(( (tpx band 1)+(2* (tpy mod 3) )  ))
  '  tpk=peek($8000+tad)
  '  poke $8000+tad,(tpk band (63-tpm))
  '  end if
  'if peek($00F5) band $FC=$40 then: '- incompleto
  '  tad=(((int(tpy/2))*32)+int(tpx/2))mod 512
  '  tpm=2^((    (tpx band 1)+(2* (tpy band 1)  )          ))
  '  tpk=peek($8000+tad)
  '  poke $8000+tad,(tpk band (15-tpm))
  '  end if

  end sub

