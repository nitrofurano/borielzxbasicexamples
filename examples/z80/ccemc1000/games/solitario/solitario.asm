	org 512
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
	jp __LABEL__charmap01end
__LABEL__charmap01:
#line 2
		defb $00,$00,$00,$00,$00,$00,$00,$00
		defb $55,$55,$55,$55,$55,$55,$55,$55
		defb $AA,$AA,$AA,$AA,$AA,$AA,$AA,$AA
		defb $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
		defb $55,$55,$55,$55,$55,$55,$55,$55
		defb $69,$9A,$96,$96,$96,$56,$96,$69
		defb $7D,$7D,$F7,$FD,$FD,$FF,$7D,$7D
		defb $41,$41,$04,$01,$01,$00,$41,$41
		defb $55,$55,$55,$55,$55,$55,$56,$56
		defb $55,$55,$55,$55,$55,$55,$96,$55
		defb $55,$55,$55,$55,$55,$55,$95,$95
		defb $56,$56,$55,$55,$55,$55,$56,$56
		defb $95,$95,$55,$55,$55,$55,$95,$95
		defb $56,$56,$55,$55,$55,$55,$55,$55
		defb $55,$96,$55,$55,$55,$55,$55,$55
		defb $95,$95,$55,$55,$55,$55,$55,$55
		defb $00,$00,$00,$00,$00,$00,$00,$00
		defb $00,$00,$08,$08,$08,$00,$08,$00
		defb $00,$00,$88,$22,$00,$00,$00,$00
		defb $00,$00,$22,$AA,$22,$AA,$22,$00
		defb $00,$08,$AA,$80,$AA,$02,$AA,$08
		defb $00,$00,$82,$08,$20,$20,$82,$00
		defb $00,$00,$A8,$88,$AA,$88,$AA,$00
		defb $00,$00,$20,$08,$00,$00,$00,$00
		defb $00,$00,$08,$20,$20,$20,$08,$00
		defb $00,$00,$20,$08,$08,$08,$20,$00
		defb $00,$00,$82,$28,$AA,$28,$82,$00
		defb $00,$00,$08,$08,$2A,$08,$08,$00
		defb $00,$00,$00,$00,$00,$08,$08,$28
		defb $00,$00,$00,$00,$2A,$00,$00,$00
		defb $00,$00,$00,$00,$00,$08,$08,$00
		defb $00,$00,$02,$08,$08,$20,$80,$00
		defb $00,$00,$28,$82,$82,$82,$28,$00
		defb $00,$00,$28,$08,$08,$08,$08,$00
		defb $00,$00,$28,$02,$28,$80,$AA,$00
		defb $00,$00,$A8,$02,$28,$02,$A8,$00
		defb $00,$00,$28,$88,$88,$AA,$08,$00
		defb $00,$00,$AA,$80,$A8,$02,$A8,$00
		defb $00,$00,$28,$80,$A8,$82,$28,$00
		defb $00,$00,$AA,$02,$08,$20,$20,$00
		defb $00,$00,$28,$82,$28,$82,$28,$00
		defb $00,$00,$28,$82,$2A,$02,$28,$00
		defb $00,$00,$00,$00,$08,$00,$08,$00
		defb $00,$00,$00,$00,$08,$00,$08,$28
		defb $00,$00,$02,$08,$20,$08,$02,$00
		defb $00,$00,$00,$2A,$00,$2A,$00,$00
		defb $00,$00,$20,$08,$02,$08,$20,$00
		defb $00,$00,$A8,$02,$28,$00,$20,$00
		defb $00,$00,$28,$8A,$AA,$80,$28,$00
		defb $00,$00,$20,$28,$88,$AA,$82,$00
		defb $00,$00,$A8,$82,$A8,$82,$A8,$00
		defb $00,$00,$2A,$80,$80,$80,$2A,$00
		defb $00,$00,$A8,$82,$82,$82,$A8,$00
		defb $00,$00,$AA,$80,$A8,$80,$AA,$00
		defb $00,$00,$AA,$80,$A8,$80,$80,$00
		defb $00,$00,$28,$80,$8A,$82,$28,$00
		defb $00,$00,$82,$82,$AA,$82,$82,$00
		defb $00,$00,$20,$20,$20,$20,$20,$00
		defb $00,$00,$02,$02,$02,$82,$28,$00
		defb $00,$00,$82,$88,$A0,$A8,$8A,$00
		defb $00,$00,$80,$80,$80,$80,$AA,$00
		defb $00,$00,$8A,$AA,$A2,$A2,$82,$00
		defb $00,$00,$82,$A2,$8A,$82,$82,$00
		defb $00,$00,$28,$82,$82,$82,$28,$00
		defb $00,$00,$A8,$82,$82,$A8,$80,$00
		defb $00,$00,$28,$82,$82,$8A,$2A,$00
		defb $00,$00,$A8,$82,$82,$A8,$82,$00
		defb $00,$00,$28,$80,$28,$02,$A8,$00
		defb $00,$00,$AA,$20,$20,$20,$20,$00
		defb $00,$00,$82,$82,$82,$82,$A8,$00
		defb $00,$00,$82,$82,$88,$28,$20,$00
		defb $00,$00,$82,$AA,$AA,$AA,$A2,$00
		defb $00,$00,$82,$82,$28,$82,$82,$00
		defb $00,$00,$82,$88,$28,$20,$20,$00
		defb $00,$00,$AA,$08,$20,$80,$AA,$00
		defb $00,$00,$28,$20,$20,$20,$28,$00
		defb $00,$00,$80,$20,$08,$08,$02,$00
		defb $00,$00,$28,$08,$08,$08,$28,$00
		defb $00,$08,$22,$00,$00,$00,$00,$00
		defb $28,$82,$AA,$8A,$8A,$AA,$82,$28
#line 82
__LABEL__charmap01end:
	ld a, 1
	ld (_debug), a
	ld a, 154
	call _mc1000screen
	xor a
	push af
	call _mc1000cls
__LABEL__gametitle:
	xor a
	push af
	call _mc1000cls
	ld a, (_debug)
	or a
	jp z, __LABEL1
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL2
__LABEL5:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL7
__LABEL10:
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_ee0)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	push hl
	ld hl, (_ee0)
	inc hl
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld de, 15
	add hl, de
	push hl
	call _mc1000vpokechr
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL7:
	ld hl, 15
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL10
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL2:
	ld hl, 5
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL5
__LABEL1:
	ld hl, 0
	ld (_timecount), hl
	ld hl, 23
	push hl
	ld hl, __LABEL__text02
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 387
	push hl
	call _mc1000vpprintpk
	ld hl, 14
	push hl
	ld hl, __LABEL__text03
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 675
	push hl
	call _mc1000vpprintpk
	ld hl, 192
	push hl
	ld hl, __LABEL__text04
	push hl
	ld hl, (__LABEL__charmap01) - (344)
	push hl
	ld hl, 448
	push hl
	call _mc1000vpprintpk
__LABEL__gametitledelay:
	ld a, 1
	call _mc1000keymapinv
	push af
	ld a, 2
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	push af
	ld a, 4
	call _mc1000keymapinv
	push af
	ld a, 8
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	ld h, a
	pop af
	or h
	ld (_eb0), a
	ld a, 16
	call _mc1000keymapinv
	push af
	ld a, 32
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	push af
	ld a, 64
	call _mc1000keymapinv
	push af
	ld a, 128
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	ld h, a
	pop af
	or h
	ld hl, (_eb0 - 1)
	or h
	ld (_eb0), a
	or a
	jp nz, __LABEL__gameplay
	ld hl, (_timecount)
	inc hl
	ld (_timecount), hl
	ld hl, 20
	call _mc1000delayb
	ld de, 600
	ld hl, (_timecount)
	call __LTI16
	or a
	jp nz, __LABEL__gametitledelay
__LABEL__scoretable:
	xor a
	push af
	call _mc1000cls
	ld hl, 0
	ld (_timecount), hl
	ld hl, 8
	push hl
	ld hl, __LABEL__text05
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 292
	push hl
	call _mc1000vpprintpk
	ld hl, 4
	push hl
	ld hl, (__LABEL__text05) + (8)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 305
	push hl
	call _mc1000vpprintpk
	ld hl, 4
	push hl
	ld hl, (__LABEL__text05) + (12)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 310
	push hl
	call _mc1000vpprintpk
__LABEL__scoretabledelay:
	ld hl, (_timecount)
	ld de, 15
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 0
	ld hl, 1
	call __SUB32
	ld (_i), hl
	ld hl, 0
	ld de, (_i)
	call __LEI16
	push af
	ld de, 10
	ld hl, (_i)
	call __LTI16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL17
	ld hl, 12
	push hl
	ld hl, (_i)
	ld de, 12
	call __MUL16_FAST
	ex de, hl
	ld hl, (__LABEL__texthisc01) & 0xFFFF
	add hl, de
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_i)
	ld de, 11
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ld de, 4
	add hl, de
	push hl
	call _mc1000vpprintpk
	ld hl, 5
	push hl
	ld hl, __LABEL__text03
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_i)
	ld de, 11
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ld de, 17
	add hl, de
	push hl
	call _mc1000vpprintpk
	ld hl, 5
	push hl
	ld hl, __LABEL__text03
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_i)
	ld de, 11
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ld de, 22
	add hl, de
	push hl
	call _mc1000vpprintpk
__LABEL17:
	ld hl, (_timecount)
	inc hl
	ld (_timecount), hl
	ld hl, 20
	call _mc1000delayb
	ld a, 1
	call _mc1000keymapinv
	push af
	ld a, 2
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	push af
	ld a, 4
	call _mc1000keymapinv
	push af
	ld a, 8
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	ld h, a
	pop af
	or h
	ld (_eb0), a
	ld a, 16
	call _mc1000keymapinv
	push af
	ld a, 32
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	push af
	ld a, 64
	call _mc1000keymapinv
	push af
	ld a, 128
	call _mc1000keymapinv
	ld h, a
	pop af
	or h
	ld h, a
	pop af
	or h
	ld hl, (_eb0 - 1)
	or h
	ld (_eb0), a
	push af
	ld hl, 600
	ld de, (_timecount)
	call __LEI16
	ld h, a
	pop af
	or h
	or a
	jp nz, __LABEL__gametitle
	jp __LABEL__scoretabledelay
__LABEL__gameplay:
	xor a
	push af
	call _mc1000cls
	ld hl, 0
	ld (_timecount), hl
__LABEL__drawingbackground:
	ld hl, 6
	ld (_sx), hl
	ld hl, 6
	ld (_sy), hl
	ld hl, 0
	ld (_y), hl
	jp __LABEL20
__LABEL23:
	ld hl, 7
	push hl
	ld hl, (__LABEL__text05) + (16)
	push hl
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld de, (_y)
	ld hl, (_sy)
	add hl, de
	dec hl
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	inc hl
	inc hl
	inc hl
	push hl
	call _mc1000vpprintpk
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL20:
	ld hl, 3
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL23
	ld hl, 4
	ld (_y), hl
	jp __LABEL25
__LABEL28:
	ld hl, 15
	push hl
	ld hl, (__LABEL__text05) + (16)
	push hl
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld de, (_y)
	ld hl, (_sy)
	add hl, de
	dec hl
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	dec hl
	push hl
	call _mc1000vpprintpk
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL25:
	ld hl, 10
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL28
	ld hl, 11
	ld (_y), hl
	jp __LABEL30
__LABEL33:
	ld hl, 7
	push hl
	ld hl, (__LABEL__text05) + (16)
	push hl
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld de, (_y)
	ld hl, (_sy)
	add hl, de
	dec hl
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	inc hl
	inc hl
	inc hl
	push hl
	call _mc1000vpprintpk
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL30:
	ld hl, 14
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL33
	ld hl, 4
	push hl
	ld hl, __LABEL__text08
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 182
	push hl
	call _mc1000vpprintpk
	ld hl, 4
	push hl
	ld hl, (__LABEL__text08) + (4)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 278
	push hl
	call _mc1000vpprintpk
	call __LABEL__restartboard
	ld hl, 3
	ld (_px), hl
	ld hl, 3
	ld (_py), hl
	ld hl, 3
	ld (_ox), hl
	ld hl, 3
	ld (_oy), hl
	ld hl, 0
	ld (_se), hl
	ld hl, 0
	ld (_sl), hl
	ld hl, 0
	ld (_pf), hl
	call __LABEL__arraytodisplay
	call __LABEL__restartboard
	ld hl, 1
	ld (_pf), hl
	call __LABEL__arraytodisplay
	ld hl, 0
	ld (_gv), hl
	ld hl, 32
	ld (_pa), hl
	ld hl, 0
	ld (_je), hl
	ld hl, 0
	ld (_timecount), hl
	ld (_tt), hl
	call __LABEL__displaytimer
	ld hl, 0
	ld (_x1), hl
	ld hl, 0
	ld (_y1), hl
	ld hl, 6
	ld (_x2), hl
	ld hl, 6
	ld (_y2), hl
	xor a
	ld (_kprs), a
__LABEL__gameloop:
	ld hl, 20
	call _mc1000delayb
	ld a, 128
	call _mc1000keymapinv
	ld h, 4
	and h
	ld h, 4
	call __DIVU8_FAST
	push af
	ld a, 8
	call _mc1000keymapinv
	ld h, 4
	and h
	srl a
	ld h, a
	pop af
	or h
	push af
	ld a, 2
	call _mc1000keymapinv
	ld h, 1
	and h
	add a, a
	add a, a
	ld h, a
	pop af
	or h
	push af
	ld a, 16
	call _mc1000keymapinv
	ld h, 1
	and h
	ld h, 8
	call __MUL8_FAST
	ld h, a
	pop af
	or h
	ld (_stick), a
	ld de, 1
	ld hl, (_je)
	call __EQ16
	push af
	ld a, (_stick)
	sub 1
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL36
	ld hl, 0
	ld (_je), hl
__LABEL36:
	ld de, 0
	ld hl, (_je)
	call __EQ16
	push af
	ld a, (_stick)
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL38
	call __LABEL__cursormotion
__LABEL38:
	ld hl, 3
	push hl
	ld hl, __LABEL__text01a
	push hl
	ld hl, (__LABEL__charmap01) - (320)
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld de, -33
	add hl, de
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) + (32)
	push hl
	ld hl, 7
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	dec hl
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) + (32)
	push hl
	ld hl, 8
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	inc hl
	push hl
	call _mc1000vpokechr
	ld hl, 3
	push hl
	ld hl, __LABEL__text01a
	push hl
	ld hl, (__LABEL__charmap01) - (280)
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld de, 31
	add hl, de
	push hl
	call _mc1000vpprintpk
	ld hl, (_tt)
	ld (_qq), hl
	ld hl, (_timecount)
	ld (_tt), hl
	ld hl, (_qq)
	ld de, 60
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld hl, (_tt)
	ld de, 60
	call __DIVI16
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	call __EQ32
	cpl
	or a
	jp z, __LABEL40
	call __LABEL__displaytimer
__LABEL40:
	ld a, 32
	call _mc1000keymapinv
	ld h, 2
	and h
	srl a
	ld (_button1), a
	push af
	ld de, 0
	ld hl, (_se)
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL42
	ld hl, 1
	ld (_se), hl
	call __LABEL__refreshandcheck
__LABEL42:
	ld a, (_button1)
	sub 1
	sbc a, a
	push af
	ld de, 0
	ld hl, (_se)
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL44
	ld hl, 0
	ld (_se), hl
__LABEL44:
	ld de, 1
	ld hl, (_gv)
	call __EQ16
	or a
	jp z, __LABEL46
	call __LABEL__gameover
	jp __LABEL__scorewrite
__LABEL46:
	ld hl, (_timecount)
	inc hl
	ld (_timecount), hl
	jp __LABEL__gameloop
__LABEL__refreshandcheck:
	ld de, 0
	ld hl, (_sl)
	call __EQ16
	push af
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 1
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL48
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 2
	inc hl
	ld (hl), 0
	ld hl, (_px)
	ld (_ox), hl
	ld hl, (_py)
	ld (_oy), hl
	ld hl, 1
	ld (_sl), hl
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 7
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
	ret
__LABEL48:
	ld de, 1
	ld hl, (_sl)
	call __EQ16
	push af
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 2
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL50
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 1
	inc hl
	ld (hl), 0
	ld hl, (_px)
	ld (_ox), hl
	ld hl, (_py)
	ld (_oy), hl
	ld hl, 0
	ld (_sl), hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 90
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
	ret
__LABEL50:
	ld hl, (_px)
	ld de, (_ox)
	or a
	sbc hl, de
	call __ABS16
	ld (_dx), hl
	ld hl, (_py)
	ld de, (_oy)
	or a
	sbc hl, de
	call __ABS16
	ld (_dy), hl
	ld de, (_ox)
	ld hl, (_px)
	add hl, de
	sra h
	rr l
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	call __I32TOFREG
	ld hl, _mx
	call __STOREF
	ld de, (_oy)
	ld hl, (_py)
	add hl, de
	sra h
	rr l
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	call __I32TOFREG
	ld hl, _my
	call __STOREF
	ld de, 0
	ld hl, (_dx)
	call __EQ16
	push af
	ld de, 2
	ld hl, (_dy)
	call __EQ16
	ld h, a
	pop af
	call __AND8
	push af
	ld a, (_my)
	ld de, (_my + 1)
	ld bc, (_my + 3)
	call __FTOU32REG
	push hl
	ld a, (_mx)
	ld de, (_mx + 1)
	ld bc, (_mx + 3)
	call __FTOU32REG
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 1
	call __EQ16
	ld h, a
	pop af
	call __AND8
	push af
	ld de, 2
	ld hl, (_dx)
	call __EQ16
	push af
	ld de, 0
	ld hl, (_dy)
	call __EQ16
	ld h, a
	pop af
	call __AND8
	push af
	ld a, (_my)
	ld de, (_my + 1)
	ld bc, (_my + 3)
	call __FTOU32REG
	push hl
	ld a, (_mx)
	ld de, (_mx + 1)
	ld bc, (_mx + 3)
	call __FTOU32REG
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 1
	call __EQ16
	ld h, a
	pop af
	call __AND8
	ld h, a
	pop af
	or h
	ld (_zz), a
	ld de, 1
	ld hl, (_sl)
	call __EQ16
	push af
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 0
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL52
	ld a, (_zz)
	or a
	jp z, __LABEL54
	ld a, (_my)
	ld de, (_my + 1)
	ld bc, (_my + 3)
	call __FTOU32REG
	push hl
	ld a, (_mx)
	ld de, (_mx + 1)
	ld bc, (_mx + 3)
	call __FTOU32REG
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 0
	inc hl
	ld (hl), 0
	ld hl, (_oy)
	push hl
	ld hl, (_ox)
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 0
	inc hl
	ld (hl), 0
	ld hl, (_py)
	push hl
	ld hl, (_px)
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 1
	inc hl
	ld (hl), 0
	ld hl, 0
	ld (_sl), hl
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 0
	push hl
	ld hl, _my + 4
	call __FP_PUSH_REV
	ld a, 082h
	ld de, 00000h
	ld bc, 00000h
	call __MULF
	push bc
	push de
	push af
	ld hl, (_sy)
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	call __I32TOFREG
	call __ADDF
	push bc
	push de
	push af
	ld a, 086h
	ld de, 00000h
	ld bc, 00000h
	call __MULF
	push bc
	push de
	push af
	ld hl, _mx + 4
	call __FP_PUSH_REV
	ld a, 082h
	ld de, 00000h
	ld bc, 00000h
	call __MULF
	push bc
	push de
	push af
	ld hl, (_sx)
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	call __I32TOFREG
	call __ADDF
	call __ADDF
	call __FTOU32REG
	push hl
	call _mc1000vpokechr
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 0
	push hl
	ld hl, (_oy)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_ox)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 1
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
	ld hl, (_pa)
	dec hl
	ld (_pa), hl
	call __LABEL__checkpossiblemotions
	call __LABEL__displaytimer
__LABEL54:
__LABEL52:
	ret
__LABEL__gameover:
	ld hl, 0
	ld (_timecount), hl
	ld hl, 7
	push hl
	ld hl, (__LABEL__text01) + (32)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 598
	push hl
	call _mc1000vpprintpk
	ld hl, 5
	push hl
	ld hl, (__LABEL__text01) + (40)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 630
	push hl
	call _mc1000vpprintpk
__LABEL__gameoverdelay:
	ld hl, (_timecount)
	inc hl
	ld (_timecount), hl
	ld de, 200
	ld hl, (_timecount)
	call __LTI16
	or a
	jp nz, __LABEL__gameoverdelay
	ret
__LABEL__restartboard:
	ld hl, 0
	ld (_y), hl
	jp __LABEL57
__LABEL60:
	ld hl, 0
	ld (_x), hl
	jp __LABEL62
__LABEL65:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 1
	inc hl
	ld (hl), 0
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL62:
	ld hl, 6
	ld de, (_x)
	call __LTI16
	or a
	jp z, __LABEL65
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL57:
	ld hl, 6
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL60
	ld hl, 5
	ld (_y), hl
	jp __LABEL67
__LABEL70:
	ld hl, 5
	ld (_x), hl
	jp __LABEL72
__LABEL75:
	ld hl, (_y)
	ld de, 7
	call __MODI16
	push hl
	ld hl, (_x)
	ld de, 7
	call __MODI16
	push hl
	ld hl, _a
	call __ARRAY
	ld (hl), 3
	inc hl
	ld (hl), 0
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL72:
	ld hl, 8
	ld de, (_x)
	call __LTI16
	or a
	jp z, __LABEL75
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL67:
	ld hl, 8
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL70
	ld hl, 0
	ld (_a + 59), hl
	ret
__LABEL__arraytodisplay:
	ld hl, 0
	ld (_y), hl
	jp __LABEL77
__LABEL80:
	ld hl, 0
	ld (_x), hl
	jp __LABEL82
__LABEL85:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 3
	call __LTI16
	or a
	jp z, __LABEL88
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 5
	push hl
	ld hl, (_y)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_x)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
__LABEL88:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 3
	call __LTI16
	push af
	ld de, 1
	ld hl, (_pf)
	call __EQ16
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL90
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 0
	call __EQ16
	or a
	jp z, __LABEL92
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 5
	push hl
	ld hl, (_y)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_x)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
__LABEL92:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	ld de, 1
	call __EQ16
	or a
	jp z, __LABEL94
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 6
	push hl
	ld hl, (_y)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_x)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
__LABEL94:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ld hl, 1
	call __LTI16
	or a
	jp z, __LABEL96
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 7
	push hl
	ld hl, (_y)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	push hl
	ld hl, (_x)
	add hl, hl
	ex de, hl
	ld hl, (_sx)
	add hl, de
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _mc1000vpokechr
__LABEL96:
__LABEL90:
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL82:
	ld hl, 6
	ld de, (_x)
	call __LTI16
	or a
	jp z, __LABEL85
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL77:
	ld hl, 6
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL80
	ret
__LABEL__cursormotion:
	ld hl, 1
	ld (_je), hl
	ld a, 128
	call _mc1000keymapinv
	ld h, 4
	and h
	ld h, 4
	call __DIVU8_FAST
	push af
	ld a, 8
	call _mc1000keymapinv
	ld h, 4
	and h
	srl a
	ld h, a
	pop af
	or h
	push af
	ld a, 2
	call _mc1000keymapinv
	ld h, 1
	and h
	add a, a
	add a, a
	ld h, a
	pop af
	or h
	push af
	ld a, 16
	call _mc1000keymapinv
	ld h, 1
	and h
	ld h, 8
	call __MUL8_FAST
	ld h, a
	pop af
	or h
	ld (_stick), a
__LABEL__verifystickvalue:
__LABEL__erasecursor:
	ld hl, 3
	push hl
	ld hl, __LABEL__texterasecursor01
	push hl
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld de, -33
	add hl, de
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld hl, 46
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	dec hl
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld hl, 46
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	inc hl
	push hl
	call _mc1000vpokechr
	ld hl, 3
	push hl
	ld hl, (__LABEL__texterasecursor01) + (5)
	push hl
	ld hl, (__LABEL__charmap01) - (336)
	push hl
	ld hl, (_py)
	add hl, hl
	ex de, hl
	ld hl, (_sy)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_sx)
	add hl, de
	push hl
	ld hl, (_px)
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	ld de, 31
	add hl, de
	push hl
	call _mc1000vpprintpk
	xor a
	ld (_kprs), a
	sub 1
	sbc a, a
	push af
	ld a, (_stick)
	dec a
	sub 1
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL98
	ld a, 1
	ld (_kprs), a
	ld hl, (_py)
	dec hl
	ld (_py), hl
__LABEL98:
	ld a, (_kprs)
	sub 1
	sbc a, a
	push af
	ld a, (_stick)
	sub 2
	sub 1
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL100
	ld a, 1
	ld (_kprs), a
	ld hl, (_py)
	inc hl
	ld (_py), hl
__LABEL100:
	ld a, (_kprs)
	sub 1
	sbc a, a
	push af
	ld a, (_stick)
	sub 4
	sub 1
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL102
	ld a, 1
	ld (_kprs), a
	ld hl, (_px)
	dec hl
	ld (_px), hl
__LABEL102:
	ld a, (_kprs)
	sub 1
	sbc a, a
	push af
	ld a, (_stick)
	sub 8
	sub 1
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL104
	ld a, 1
	ld (_kprs), a
	ld hl, (_px)
	inc hl
	ld (_px), hl
__LABEL104:
	ld de, (_x1)
	ld hl, (_px)
	call __LTI16
	or a
	jp z, __LABEL106
	ld hl, (_x2)
	ld (_px), hl
__LABEL106:
	ld hl, (_x2)
	ld de, (_px)
	call __LTI16
	or a
	jp z, __LABEL108
	ld hl, (_x1)
	ld (_px), hl
__LABEL108:
	ld de, (_y1)
	ld hl, (_py)
	call __LTI16
	or a
	jp z, __LABEL110
	ld hl, (_y2)
	ld (_py), hl
__LABEL110:
	ld hl, (_y2)
	ld de, (_py)
	call __LTI16
	or a
	jp z, __LABEL112
	ld hl, (_y1)
	ld (_py), hl
__LABEL112:
	ld hl, 0
	ld (_y1), hl
	ld hl, 6
	ld (_y2), hl
	ld hl, 0
	ld (_x1), hl
	ld hl, 6
	ld (_x2), hl
	ld de, 2
	ld hl, (_px)
	call __LTI16
	or a
	jp z, __LABEL114
	ld hl, 0
	ld (_x1), hl
	ld hl, 6
	ld (_x2), hl
	ld hl, 2
	ld (_y1), hl
	ld hl, 4
	ld (_y2), hl
__LABEL114:
	ld hl, 4
	ld de, (_px)
	call __LTI16
	or a
	jp z, __LABEL116
	ld hl, 0
	ld (_x1), hl
	ld hl, 6
	ld (_x2), hl
	ld hl, 2
	ld (_y1), hl
	ld hl, 4
	ld (_y2), hl
__LABEL116:
	ld de, 2
	ld hl, (_py)
	call __LTI16
	or a
	jp z, __LABEL118
	ld hl, 0
	ld (_y1), hl
	ld hl, 6
	ld (_y2), hl
	ld hl, 2
	ld (_x1), hl
	ld hl, 4
	ld (_x2), hl
__LABEL118:
	ld hl, 4
	ld de, (_py)
	call __LTI16
	or a
	jp z, __LABEL120
	ld hl, 0
	ld (_y1), hl
	ld hl, 6
	ld (_y2), hl
	ld hl, 2
	ld (_x1), hl
	ld hl, 4
	ld (_x2), hl
__LABEL120:
	ld a, (_debug)
	dec a
	sub 1
	jp nc, __LABEL122
	ld hl, (__LABEL__textdebug03) - (__LABEL__textdebug02)
	push hl
	ld hl, __LABEL__textdebug02
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 33
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_px)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 36
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_py)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 41
	push hl
	call _mc1000vpokechr
__LABEL122:
	ld a, (_debug)
	dec a
	sub 1
	jp nc, __LABEL124
	ld hl, (__LABEL__textdebug04) - (__LABEL__textdebug03)
	push hl
	ld hl, __LABEL__textdebug03
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 65
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_sx)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 68
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_sy)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 73
	push hl
	call _mc1000vpokechr
__LABEL124:
	ld a, (_debug)
	dec a
	sub 1
	jp nc, __LABEL126
	ld hl, (__LABEL__textdebug05) - (__LABEL__textdebug04)
	push hl
	ld hl, __LABEL__textdebug04
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 97
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_ox)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 100
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_oy)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 105
	push hl
	call _mc1000vpokechr
__LABEL126:
	ld a, (_debug)
	dec a
	sub 1
	jp nc, __LABEL128
	ld hl, (__LABEL__textdebug06) - (__LABEL__textdebug05)
	push hl
	ld hl, __LABEL__textdebug05
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 43
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_x1)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 46
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_y1)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 51
	push hl
	call _mc1000vpokechr
__LABEL128:
	ld a, (_debug)
	dec a
	sub 1
	jp nc, __LABEL130
	ld hl, (__LABEL__textdebug07) - (__LABEL__textdebug06)
	push hl
	ld hl, __LABEL__textdebug06
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 75
	push hl
	call _mc1000vpprintpk
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_x2)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 78
	push hl
	call _mc1000vpokechr
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, (_y2)
	ld de, 10
	call __MODI16
	ld de, 48
	add hl, de
	push hl
	ld hl, 83
	push hl
	call _mc1000vpokechr
__LABEL130:
	ret
__LABEL__checkpossiblemotions:
	ld de, 1
	ld hl, (_ch)
	call __EQ16
	or a
	jp z, __LABEL132
	ret
__LABEL132:
	xor a
	ld (_ck), a
	ld hl, 7
	push hl
	ld hl, (__LABEL__text01) + (48)
	push hl
	ld hl, (__LABEL__charmap01) - (128)
	push hl
	ld hl, 630
	push hl
	call _mc1000vpprintpk
	ld hl, 2
	ld (_x), hl
	jp __LABEL133
__LABEL136:
	ld hl, 0
	ld (_y), hl
	jp __LABEL138
__LABEL141:
	ld hl, (_y)
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	add hl, hl
	push hl
	ld hl, (_y)
	inc hl
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_y)
	inc hl
	inc hl
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	pop hl
	add hl, de
	ld (_v1), hl
	ld hl, (_x)
	push hl
	ld hl, (_y)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	add hl, hl
	push hl
	ld hl, (_x)
	push hl
	ld hl, (_y)
	inc hl
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_x)
	push hl
	ld hl, (_y)
	inc hl
	inc hl
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	pop hl
	add hl, de
	ld (_v2), hl
	ld de, 3
	ld hl, (_v1)
	call __EQ16
	push af
	ld de, 6
	ld hl, (_v1)
	call __EQ16
	ld h, a
	pop af
	or h
	push af
	ld de, 3
	ld hl, (_v2)
	call __EQ16
	ld h, a
	pop af
	or h
	push af
	ld de, 6
	ld hl, (_v2)
	call __EQ16
	ld h, a
	pop af
	or h
	or a
	jp z, __LABEL144
	ld a, (_ck)
	inc a
	ld (_ck), a
__LABEL144:
	ld hl, (_y)
	inc hl
	ld (_y), hl
__LABEL138:
	ld hl, 4
	ld de, (_y)
	call __LTI16
	or a
	jp z, __LABEL141
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL133:
	ld hl, 4
	ld de, (_x)
	call __LTI16
	or a
	jp z, __LABEL136
	ld hl, 5
	ld (_x), hl
	jp __LABEL145
__LABEL148:
	ld hl, 2
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	add hl, hl
	push hl
	ld hl, 3
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, 4
	push hl
	ld hl, (_x)
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	pop hl
	add hl, de
	ld (_v1), hl
	ld hl, (_x)
	push hl
	ld hl, 2
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	add hl, hl
	push hl
	ld hl, (_x)
	push hl
	ld hl, 3
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	ex de, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld hl, (_x)
	push hl
	ld hl, 4
	push hl
	ld hl, _a
	call __ARRAY
	ld e, (hl)
	inc hl
	ld d, (hl)
	pop hl
	add hl, de
	ld (_v2), hl
	ld de, 3
	ld hl, (_v1)
	call __EQ16
	push af
	ld de, 6
	ld hl, (_v1)
	call __EQ16
	ld h, a
	pop af
	or h
	push af
	ld de, 3
	ld hl, (_v2)
	call __EQ16
	ld h, a
	pop af
	or h
	push af
	ld de, 6
	ld hl, (_v2)
	call __EQ16
	ld h, a
	pop af
	or h
	or a
	jp z, __LABEL151
	ld a, (_ck)
	inc a
	ld (_ck), a
__LABEL151:
	ld hl, (_x)
	inc hl
	ld (_x), hl
__LABEL145:
	ld hl, 8
	ld de, (_x)
	call __LTI16
	or a
	jp z, __LABEL148
	ld a, (_ck)
	sub 1
	jp nc, __LABEL153
	ld hl, 1
	ld (_gv), hl
__LABEL153:
	ret
__LABEL__displaytimer:
	ld hl, (_tt)
	ld (_qq), hl
	ld hl, (_timecount)
	ld (_tt), hl
	ret
__LABEL__scorewrite:
	ld hl, 0
	ld (_timecount), hl
	ld hl, (_tt)
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld hl, (_pa)
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld de, 1
	ld hl, 6464
	call __MUL32
	pop bc
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	ld (_tf), hl
__LABEL__textinput:
	ld de, 13
	ld hl, (_ka)
	call __EQ16
	or a
	jp nz, __LABEL__5200
__LABEL__5200:
	ld a, 10
	ld (_iq), a
	ld hl, 9
	ld (_i), hl
	jp __LABEL156
__LABEL159:
	ld hl, (_tf)
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	push de
	push hl
	ld hl, (_i)
	push hl
	ld hl, _t
	call __ARRAY
	call __ILOAD32
	call __LTI32
	or a
	jp z, __LABEL162
	ld a, (_iq)
	dec a
	ld (_iq), a
__LABEL162:
	ld hl, (_i)
	dec hl
	ld (_i), hl
__LABEL156:
	ld de, 0
	ld hl, (_i)
	call __LTI16
	or a
	jp z, __LABEL159
	ld a, 9
	ld hl, (_iq - 1)
	cp h
	jp c, __LABEL__scoretable
__LABEL__movingworstscoresdown:
__LABEL__5300:
	jp __LABEL__scoretable
__LABEL165:
	jp __LABEL165
	jp __LABEL__text99
__LABEL__textdectmp:
#line 434
		defb "12345678"
#line 435
__LABEL__text01:
__LABEL__text02:
#line 440
		defb "BY PAULO SILVA, '05,'18"
#line 441
__LABEL__text03:
#line 444
		defb "PUSH SPACE KEY"
#line 445
__LABEL__text04:
#line 448
		defb "00000000000101000000000010000000"
		defb "00011100000100010000000000000000"
		defb "00010001110101011011011010111000"
		defb "00011101010101010001010010101000"
		defb "00000101010101010111010010101000"
		defb "00011101110101011111010010111000"
#line 454
__LABEL__text05:
#line 457
		defb "HISCORESRESTTIME................."
#line 458
__LABEL__text06:
#line 461
		defb "NO MORE MOVES...WAIT............."
#line 462
__LABEL__text01a:
__LABEL__text07:
#line 466
		defb "012..."
#line 467
__LABEL__text08:
#line 470
		defb "TIMEREST"
#line 471
__LABEL__text09:
__LABEL__textbinout01:
#line 477
		defb "01234567"
#line 478
__LABEL__texthisc01:
#line 481
		defb "PORCOROSSO.."
		defb "TOTORO......"
		defb "CHIHIRO....."
		defb "HEIDI......."
		defb "KONAN......."
		defb "GHIBLI......"
		defb "MONONOKE...."
		defb "NAUSICAA...."
		defb "LUPIN......."
		defb "UNKNOWN....."
#line 491
__LABEL__texterasecursor01:
#line 494
		defb "........  "
		defb "ABCDEFGH"
#line 496
__LABEL__textdebug01:
#line 500
		defb "DEBUG"
#line 501
__LABEL__textdebug02:
#line 504
	defb "PX: ,PY: "
#line 505
__LABEL__textdebug03:
#line 508
	defb "SX: ,SY: "
#line 509
__LABEL__textdebug04:
#line 512
	defb "OX: ,OY: "
#line 513
__LABEL__textdebug05:
#line 516
	defb "X1:0,Y1:0"
#line 517
__LABEL__textdebug06:
#line 520
	defb "X2:0,Y2:0"
#line 521
__LABEL__textdebug07:
__LABEL__decuibuffer01:
#line 527
		defb "12345"
#line 528
__LABEL__text99:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	pop iy
	pop ix
	exx
	ei
	ret
__CALL_BACK__:
	DEFW 0
_aquariusfillram:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b,(ix+6)
		ld d,(ix+9)
		ld e,(ix+8)
		ld h,(ix+5)
		ld l,(ix+4)
aquariusfillramloop:
		ld (hl),b
		inc hl
		dec de
		ld a,d
		or e
		jr nz,aquariusfillramloop
#line 13
_aquariusfillram__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_minivadersldir32b:
	push ix
	ld ix, 0
	add ix, sp
#line 2
		ld d,(ix+5)
		ld e,(ix+4)
		ld h,(ix+7)
		ld l,(ix+6)
		ld b,(ix+9)
		ld c,(ix+8)
minivadersldir32bloop:
		ld a,(hl)
		ld (de),a
		inc hl
		ex de,hl
		push de
		ld de,32
		add hl,de
		pop de
		ex de,hl
		dec bc
		ld a,b
		or c
		jp nz,minivadersldir32bloop
#line 22
_minivadersldir32b__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000screen:
#line 1
		out($80),a
		ld($00F5),a
#line 3
_mc1000screen__leave:
	ret
_mc1000cls:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	push hl
	inc sp
	ld (ix-3), 0
	ld (ix-2), 2
	ld (ix-1), 0
	ld a, (245)
	ld h, 248
	and h
	sub 128
	sub 1
	jp nc, __LABEL168
	ld (ix-3), 0
	ld (ix-2), 4
__LABEL168:
	ld a, (245)
	ld h, 252
	and h
	sub 136
	sub 1
	jp nc, __LABEL170
	ld (ix-3), 0
	ld (ix-2), 8
__LABEL170:
	ld a, (245)
	ld h, 252
	and h
	sub 140
	sub 1
	jp nc, __LABEL172
	ld (ix-3), 0
	ld (ix-2), 6
__LABEL172:
	ld a, (245)
	ld h, 248
	and h
	sub 144
	sub 1
	jp nc, __LABEL174
	ld (ix-3), 0
	ld (ix-2), 12
__LABEL174:
	ld a, (245)
	ld h, 248
	and h
	sub 152
	sub 1
	jp nc, __LABEL176
	ld (ix-3), 0
	ld (ix-2), 24
__LABEL176:
	ld a, (245)
	ld h, 132
	and h
	sub 128
	sub 1
	jp nc, __LABEL178
	ld a, (ix+5)
	ld h, 3
	and h
	ld h, 85
	call __MUL8_FAST
	ld (ix-1), a
__LABEL178:
	ld a, (245)
	ld h, 132
	and h
	sub 132
	sub 1
	jp nc, __LABEL180
	ld a, (ix+5)
	ld h, 1
	and h
	ld h, 255
	call __MUL8_FAST
	ld (ix-1), a
__LABEL180:
	ld a, (245)
	ld h, 96
	and h
	sub 96
	sub 1
	jp nc, __LABEL182
	ld (ix-1), 0
	ld a, (ix+5)
	ld h, 7
	and h
	ld h, 4
	cp h
	jp nc, __LABEL184
	ld a, (ix+5)
	ld h, 3
	and h
	ld b, 6
__LABEL196:
	add a, a
	djnz __LABEL196
	ld h, a
	ld a, 63
	or h
	ld (ix-1), a
__LABEL184:
__LABEL182:
	ld a, (245)
	ld h, 96
	and h
	sub 64
	sub 1
	jp nc, __LABEL186
	ld (ix-1), 0
	ld a, (ix+5)
	ld h, 15
	and h
	ld h, 8
	cp h
	jp nc, __LABEL188
	ld a, (ix+5)
	ld h, 7
	and h
	add a, a
	add a, a
	add a, a
	add a, a
	ld h, a
	ld a, 15
	or h
	ld (ix-1), a
__LABEL188:
__LABEL186:
	ld a, (245)
	ld h, 252
	and h
	sub 1
	jp nc, __LABEL190
	ld a, (ix+5)
	ld h, 1
	and h
	ld b, 7
__LABEL197:
	add a, a
	djnz __LABEL197
	ld h, a
	ld a, 32
	or h
	ld (ix-1), a
__LABEL190:
	ld l, (ix-3)
	ld h, (ix-2)
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	push hl
	ld hl, 32768
	push hl
	call _aquariusfillram
_mc1000cls__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_mc1000vpokechr:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 768
	call __MODU16
	ld (ix+4), l
	ld (ix+5), h
	ld hl, 8
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld h, 0
	ld a, l
	and 31
	ld l, a
	ld de, 32768
	add hl, de
	ld de, 0
	push de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 256
	call __MUL32
	pop bc
	add hl, bc
	pop bc
	ex de, hl
	adc hl, bc
	ex de, hl
	push hl
	call _minivadersldir32b
_mc1000vpokechr__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000delayb:
#line 1
		ld b,h
		ld c,l
		call $C30E
#line 4
_mc1000delayb__leave:
	ret
_mc1000keymapinv:
#line 1
		di
		xor $FF
		ld b,a
		ld a,$0E
		out($20),a
		ld a,b
		out($60),a
		ld a,$0F
		out($20),a
		in a,($40)
		xor $FF
		ei
#line 13
_mc1000keymapinv__leave:
	ret
_mc1000vpprintpk:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_tflp1), hl
	jp __LABEL191
__LABEL194:
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	ld hl, (_tflp1)
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	ld l, a
	ld h, 0
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, (_tflp1)
	add hl, de
	push hl
	call _mc1000vpokechr
	ld hl, (_tflp1)
	inc hl
	ld (_tflp1), hl
__LABEL191:
	ld l, (ix+10)
	ld h, (ix+11)
	dec hl
	ld de, (_tflp1)
	or a
	sbc hl, de
	jp nc, __LABEL194
_mc1000vpprintpk__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "abs16.asm"
	; 16 bit signed integer abs value 
	; HL = value
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "abs16.asm"
	
#line 2863 "solitario.bas"
#line 1 "addf.asm"
#line 1 "stackf.asm"
	; -------------------------------------------------------------
	; Functions to manage FP-Stack of the ZX Spectrum ROM CALC
	; -------------------------------------------------------------
	
	
	__FPSTACK_PUSH EQU 2AB6h	; Stores an FP number into the ROM FP stack (A, ED CB)
	__FPSTACK_POP  EQU 2BF1h	; Pops an FP number out of the ROM FP stack (A, ED CB)
	
__FPSTACK_PUSH2: ; Pushes Current A ED CB registers and top of the stack on (SP + 4)
	                 ; Second argument to push into the stack calculator is popped out of the stack
	                 ; Since the caller routine also receives the parameters into the top of the stack
	                 ; four bytes must be removed from SP before pop them out
	
	    call __FPSTACK_PUSH ; Pushes A ED CB into the FP-STACK
	    exx
	    pop hl       ; Caller-Caller return addr
	    exx
	    pop hl       ; Caller return addr
	
	    pop af
	    pop de
	    pop bc
	
	    push hl      ; Caller return addr
	    exx
	    push hl      ; Caller-Caller return addr
	    exx
	 
	    jp __FPSTACK_PUSH
	
	
__FPSTACK_I16:	; Pushes 16 bits integer in HL into the FP ROM STACK
					; This format is specified in the ZX 48K Manual
					; You can push a 16 bit signed integer as
					; 0 SS LL HH 0, being SS the sign and LL HH the low
					; and High byte respectively
		ld a, h
		rla			; sign to Carry
		sbc	a, a	; 0 if positive, FF if negative
		ld e, a
		ld d, l
		ld c, h
		xor a
		ld b, a
		jp __FPSTACK_PUSH
#line 2 "addf.asm"
	
	; -------------------------------------------------------------
	; Floating point library using the FP ROM Calculator (ZX 48K)
	; All of them uses A EDCB registers as 1st paramter.
	; For binary operators, the 2n operator must be pushed into the
	; stack, in the order AF DE BC (F not used).
	;
	; Uses CALLEE convention
	; -------------------------------------------------------------
	
__ADDF:	; Addition
		call __FPSTACK_PUSH2
		
		; ------------- ROM ADD
		rst 28h
		defb 0fh	; ADD
		defb 38h;   ; END CALC
	
		jp __FPSTACK_POP
	
#line 2864 "solitario.bas"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 2865 "solitario.bas"
#line 1 "array.asm"
; vim: ts=4:et:sw=4:
	; Copyleft (K) by Jose M. Rodriguez de la Rosa
	;  (a.k.a. Boriel) 
;  http://www.boriel.com
	; -------------------------------------------------------------------
	; Simple array Index routine
	; Number of total indexes dimensions - 1 at beginning of memory
	; HL = Start of array memory (First two bytes contains N-1 dimensions)
	; Dimension values on the stack, (top of the stack, highest dimension)
	; E.g. A(2, 4) -> PUSH <4>; PUSH <2>
	
	; For any array of N dimension A(aN-1, ..., a1, a0)
	; and dimensions D[bN-1, ..., b1, b0], the offset is calculated as
	; O = [a0 + b0 * (a1 + b1 * (a2 + ... bN-2(aN-1)))]
; What I will do here is to calculate the following sequence:
	; ((aN-1 * bN-2) + aN-2) * bN-3 + ...
	
	
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 20 "array.asm"
	
#line 24 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
	
__ARRAY:
		PROC
	
		LOCAL LOOP
		LOCAL ARRAY_END
		LOCAL RET_ADDRESS ; Stores return address
	
		ex (sp), hl	; Return address in HL, array address in the stack
		ld (RET_ADDRESS + 1), hl ; Stores it for later
	
		exx
		pop hl		; Will use H'L' as the pointer
		ld c, (hl)	; Loads Number of dimensions from (hl)
		inc hl
		ld b, (hl)
		inc hl		; Ready
		exx
			
		ld hl, 0	; BC = Offset "accumulator"
	
#line 48 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
	
LOOP:
		pop bc		; Get next index (Ai) from the stack
	
#line 60 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
	
		add hl, bc	; Adds current index
	
		exx			; Checks if B'C' = 0
		ld a, b		; Which means we must exit (last element is not multiplied by anything)
		or c
		jr z, ARRAY_END		; if B'Ci == 0 we are done
	
		ld e, (hl)			; Loads next dimension into D'E'
		inc hl
		ld d, (hl)
		inc hl
		push de
		dec bc				; Decrements loop counter
		exx
		pop de				; DE = Max bound Number (i-th dimension)
	
#line 80 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
		;call __MUL16_FAST	; HL *= DE
	    call __FNMUL
#line 86 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
		jp LOOP
		
ARRAY_END:
		ld e, (hl)
		inc hl
		ld d, c			; C = 0 => DE = E = Element size
		push hl
		push de
		exx
	
#line 100 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
	    LOCAL ARRAY_SIZE_LOOP
	
	    ex de, hl
	    ld hl, 0
	    pop bc
	    ld b, c
ARRAY_SIZE_LOOP: 
	    add hl, de
	    djnz ARRAY_SIZE_LOOP
	
	    ;; Even faster
	    ;pop bc
	
	    ;ld d, h
	    ;ld e, l
	    
	    ;dec c
	    ;jp z, __ARRAY_FIN
	
	    ;add hl, hl
	    ;dec c
	    ;jp z, __ARRAY_FIN
	
	    ;add hl, hl
	    ;dec c
	    ;dec c
	    ;jp z, __ARRAY_FIN
	
	    ;add hl, de
    ;__ARRAY_FIN:    
#line 131 "/mnt/sda3/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/array.asm"
	
		pop de
		add hl, de  ; Adds element start
	
RET_ADDRESS:
		ld de, 0
		push de
		ret			; HL = (Start of Elements + Offset)
	
	    ;; Performs a faster multiply for little 16bit numbs
	    LOCAL __FNMUL, __FNMUL2
	
__FNMUL:
	    xor a
	    or d
	    jp nz, __MUL16_FAST
	
	    or e
	    ex de, hl
	    ret z
	
	    cp 33
	    jp nc, __MUL16_FAST
	
	    ld b, l
	    ld l, h  ; HL = 0
	
__FNMUL2:
	    add hl, de
	    djnz __FNMUL2
	    ret
	
		ENDP
		
#line 2866 "solitario.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
	
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 2867 "solitario.bas"
#line 1 "div8.asm"
				; --------------------------------
__DIVU8:	; 8 bit unsigned integer division 
				; Divides (Top of stack, High Byte) / A
		pop hl	; --------------------------------
		ex (sp), hl	; CALLEE
	
__DIVU8_FAST:	; Does A / H
		ld l, h
		ld h, a		; At this point do H / L
	
		ld b, 8
		xor a		; A = 0, Carry Flag = 0
		
__DIV8LOOP:
		sla	h		
		rla			
		cp	l		
		jr	c, __DIV8NOSUB
		sub	l		
		inc	h		
	
__DIV8NOSUB:	
		djnz __DIV8LOOP
	
		ld	l, a		; save remainder
		ld	a, h		; 
		
		ret			; a = Quotient, 
	
	
					; --------------------------------
__DIVI8:		; 8 bit signed integer division Divides (Top of stack) / A
		pop hl		; --------------------------------
		ex (sp), hl
	
__DIVI8_FAST:
		ld e, a		; store operands for later
		ld c, h
	
		or a		; negative?
		jp p, __DIV8A
		neg			; Make it positive
	
__DIV8A:
		ex af, af'
		ld a, h
		or a
		jp p, __DIV8B
		neg
		ld h, a		; make it positive
	
__DIV8B:
		ex af, af'
	
		call __DIVU8_FAST
	
		ld a, c
		xor l		; bit 7 of A = 1 if result is negative
	
		ld a, h		; Quotient
		ret p		; return if positive	
	
		neg
		ret
		
	
__MODU8:		; 8 bit module. REturns A mod (Top of stack) (unsigned operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODU8_FAST:	; __FASTCALL__ entry
		call __DIVU8_FAST
		ld a, l		; Remainder
	
		ret		; a = Modulus
	
	
__MODI8:		; 8 bit module. REturns A mod (Top of stack) (For singed operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODI8_FAST:	; __FASTCALL__ entry
		call __DIVI8_FAST
		ld a, l		; remainder
	
		ret		; a = Modulus
	
#line 2868 "solitario.bas"
#line 1 "eq16.asm"
__EQ16:	; Test if 16bit values HL == DE
		; Returns result in A: 0 = False, FF = True
			or a	; Reset carry flag
			sbc hl, de 
	
			ld a, h
			or l
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 2869 "solitario.bas"
#line 1 "eq32.asm"
__EQ32:	; Test if 32bit value HLDE equals top of the stack
		; Returns result in A: 0 = False, FF = True
			exx
			pop bc ; Return address
			exx
	
			or a	; Reset carry flag
			pop bc
			sbc hl, bc ; Low part
			ex de, hl 
			pop bc
			sbc hl, bc ; Hight part
	
			exx
			push bc ; CALLEE
			exx
	
			ld a, h
			or l
			or d
			or e   ; a = 0 only if HLDE = 0
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 2870 "solitario.bas"
#line 1 "ftou32reg.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "ftou32reg.asm"
	
__FTOU32REG:	; Converts a Float to (un)signed 32 bit integer (NOTE: It's ALWAYS 32 bit signed)
					; Input FP number in A EDCB (A exponent, EDCB mantissa)
				; Output: DEHL 32 bit number (signed)
		PROC
	
		LOCAL __IS_FLOAT
	
		or a
		jr nz, __IS_FLOAT 
		; Here if it is a ZX ROM Integer
	
		ld h, c
		ld l, d
	ld a, e	 ; Takes sign: FF = -, 0 = +
		ld de, 0
		inc a
		jp z, __NEG32	; Negates if negative
		ret
	
__IS_FLOAT:  ; Jumps here if it is a true floating point number
		ld h, e	
		push hl  ; Stores it for later (Contains Sign in H)
	
		push de
		push bc
	
		exx
		pop de   ; Loads mantissa into C'B' E'D' 
		pop bc	 ; 
	
		set 7, c ; Highest mantissa bit is always 1
		exx
	
		ld hl, 0 ; DEHL = 0
		ld d, h
		ld e, l
	
		;ld a, c  ; Get exponent
		sub 128  ; Exponent -= 128
		jr z, __FTOU32REG_END	; If it was <= 128, we are done (Integers must be > 128)
		jr c, __FTOU32REG_END	; It was decimal (0.xxx). We are done (return 0)
	
		ld b, a  ; Loop counter = exponent - 128
	
__FTOU32REG_LOOP:
		exx 	 ; Shift C'B' E'D' << 1, output bit stays in Carry
		sla d
		rl e
		rl b
		rl c
	
	    exx		 ; Shift DEHL << 1, inserting the carry on the right
		rl l
		rl h
		rl e
		rl d
	
		djnz __FTOU32REG_LOOP
	
__FTOU32REG_END:
		pop af   ; Take the sign bit
		or a	 ; Sets SGN bit to 1 if negative
		jp m, __NEG32 ; Negates DEHL
		
		ret
	
		ENDP
	
	
__FTOU8:	; Converts float in C ED LH to Unsigned byte in A
		call __FTOU32REG
		ld a, l
		ret
	
#line 2871 "solitario.bas"
#line 1 "iload32.asm"
	; __FASTCALL__ routine which
	; loads a 32 bits integer into DE,HL
	; stored at position pointed by POINTER HL
	; DE,HL <-- (HL)
	
__ILOAD32:
		ld e, (hl)	
		inc hl
		ld d, (hl)
		inc hl
		ld a, (hl)
		inc hl
		ld h, (hl)
		ld l, a
		ex de, hl
		ret
	
#line 2872 "solitario.bas"
#line 1 "lei16.asm"
	
#line 1 "lti8.asm"
	
__LTI8: ; Test 8 bit values A < H
        ; Returns result in A: 0 = False, !0 = True
	        sub h
	
__LTI:  ; Signed CMP
	        PROC
	        LOCAL __PE
	
	        ld a, 0  ; Sets default to false
__LTI2:
	        jp pe, __PE
	        ; Overflow flag NOT set
	        ret p
	        dec a ; TRUE
	
__PE:   ; Overflow set
	        ret m
	        dec a ; TRUE
	        ret
	        
	        ENDP
#line 3 "lei16.asm"
	
__LEI16: ; Test 8 bit values HL < DE
        ; Returns result in A: 0 = False, !0 = True
	        xor a
	        sbc hl, de
	        jp nz, __LTI2
	        dec a
	        ret
	
#line 2873 "solitario.bas"
#line 1 "lti16.asm"
	
	
	
__LTI16: ; Test 8 bit values HL < DE
        ; Returns result in A: 0 = False, !0 = True
	        xor a
	        sbc hl, de
	        jp __LTI2
	
#line 2874 "solitario.bas"
#line 1 "lti32.asm"
	
	
#line 1 "sub32.asm"
	; SUB32 
	; TOP of the stack - DEHL
	; Pops operand out of the stack (CALLEE)
	; and returns result in DEHL
	; Operands come reversed => So we swap then using EX (SP), HL
	
__SUB32:
		exx
		pop bc		; Return address
		exx
	
		ex (sp), hl
		pop bc
		or a 
		sbc hl, bc
	
		ex de, hl
		ex (sp), hl
		pop bc
		sbc hl, bc
		ex de, hl
	
		exx
		push bc		; Put return address
		exx
		ret
		
	
	
#line 4 "lti32.asm"
	
__LTI32: ; Test 32 bit values HLDE < Top of the stack
	    exx
	    pop de ; Preserves return address
	    exx
	
	    call __SUB32
	
	    exx
	    push de ; Restores return address
	    exx
	
	    ld a, 0
	    jp __LTI2 ; go for sign
	
#line 2875 "solitario.bas"
	
#line 1 "mul32.asm"
#line 1 "_mul32.asm"
	
; Ripped from: http://www.andreadrian.de/oldcpu/z80_number_cruncher.html#moztocid784223
	; Used with permission.
	; Multiplies 32x32 bit integer (DEHL x D'E'H'L')
	; 64bit result is returned in H'L'H L B'C'A C
	
	
__MUL32_64START:
			push hl
			exx
			ld b, h
			ld c, l		; BC = Low Part (A)
			pop hl		; HL = Load Part (B)
			ex de, hl	; DE = Low Part (B), HL = HightPart(A) (must be in B'C')
			push hl
	
			exx
			pop bc		; B'C' = HightPart(A)
			exx			; A = B'C'BC , B = D'E'DE
	
				; multiply routine 32 * 32bit = 64bit
				; h'l'hlb'c'ac = b'c'bc * d'e'de
				; needs register a, changes flags
				;
				; this routine was with tiny differences in the
				; sinclair zx81 rom for the mantissa multiply
	
__LMUL:
	        and     a               ; reset carry flag
	        sbc     hl,hl           ; result bits 32..47 = 0
	        exx
	        sbc     hl,hl           ; result bits 48..63 = 0
	        exx
	        ld      a,b             ; mpr is b'c'ac
	        ld      b,33            ; initialize loop counter
	        jp      __LMULSTART  
	
__LMULLOOP:
	        jr      nc,__LMULNOADD  ; JP is 2 cycles faster than JR. Since it's inside a LOOP
	                                ; it can save up to 33 * 2 = 66 cycles
	                                ; But JR if 3 cycles faster if JUMP not taken!
	        add     hl,de           ; result += mpd
	        exx
	        adc     hl,de
	        exx
	
__LMULNOADD:
	        exx
	        rr      h               ; right shift upper
	        rr      l               ; 32bit of result
	        exx
	        rr      h
	        rr      l
	
__LMULSTART:
	        exx
	        rr      b               ; right shift mpr/
	        rr      c               ; lower 32bit of result
	        exx
	        rra                     ; equivalent to rr a
	        rr      c
	        djnz    __LMULLOOP
	
			ret						; result in h'l'hlb'c'ac
	       
#line 2 "mul32.asm"
	
__MUL32:	; multiplies 32 bit un/signed integer.
				; First operand stored in DEHL, and 2nd onto stack
				; Lowest part of 2nd operand on top of the stack
				; returns the result in DE.HL
			exx
			pop hl	; Return ADDRESS
			pop de	; Low part
			ex (sp), hl ; CALLEE -> HL = High part
			ex de, hl
			call __MUL32_64START
	
__TO32BIT:  ; Converts H'L'HLB'C'AC to DEHL (Discards H'L'HL)
			exx
			push bc
			exx
			pop de
			ld h, a
			ld l, c
			ret
	
	
#line 2877 "solitario.bas"
#line 1 "mul8.asm"
__MUL8:		; Performs 8bit x 8bit multiplication
		PROC
	
		;LOCAL __MUL8A
		LOCAL __MUL8LOOP
		LOCAL __MUL8B
				; 1st operand (byte) in A, 2nd operand into the stack (AF)
		pop hl	; return address
		ex (sp), hl ; CALLE convention
	
;;__MUL8_FAST: ; __FASTCALL__ entry
	;;	ld e, a
	;;	ld d, 0
	;;	ld l, d
	;;	
	;;	sla h	
	;;	jr nc, __MUL8A
	;;	ld l, e
	;;
;;__MUL8A:
	;;
	;;	ld b, 7
;;__MUL8LOOP:
	;;	add hl, hl
	;;	jr nc, __MUL8B
	;;
	;;	add hl, de
	;;
;;__MUL8B:
	;;	djnz __MUL8LOOP
	;;
	;;	ld a, l ; result = A and HL  (Truncate to lower 8 bits)
	
__MUL8_FAST: ; __FASTCALL__ entry, a = a * h (8 bit mul) and Carry
	
	    ld b, 8
	    ld l, a
	    xor a
	
__MUL8LOOP:
	    add a, a ; a *= 2
	    sla l
	    jp nc, __MUL8B
	    add a, h
	
__MUL8B:
	    djnz __MUL8LOOP
		
		ret		; result = HL
		ENDP
	
#line 2878 "solitario.bas"
#line 1 "mulf.asm"
	
	
	; -------------------------------------------------------------
	; Floating point library using the FP ROM Calculator (ZX 48K)
	; All of them uses A EDCB registers as 1st paramter.
	; For binary operators, the 2n operator must be pushed into the
	; stack, in the order A DE BC.
	;
	; Uses CALLEE convention
	; -------------------------------------------------------------
	
__MULF:	; Multiplication
		call __FPSTACK_PUSH2
		
		; ------------- ROM MUL
		rst 28h
		defb 04h	; 
		defb 38h;   ; END CALC
	
		jp __FPSTACK_POP
	
#line 2879 "solitario.bas"
#line 1 "pushf.asm"
	
	; Routine to push Float pointed by HL 
	; Into the stack. Notice that the hl points to the last
	; byte of the FP number.
	; Uses H'L' B'C' and D'E' to preserve ABCDEHL registers
	
__FP_PUSH_REV:
	    push hl
	    exx
	    pop hl
	    pop bc ; Return Address
	    ld d, (hl)
	    dec hl
	    ld e, (hl)
	    dec hl
	    push de
	    ld d, (hl)
	    dec hl
	    ld e, (hl)
	    dec hl
	    push de
	    ld d, (hl)
	    push de
	    push bc ; Return Address
	    exx
	    ret
	
	
#line 2880 "solitario.bas"
#line 1 "storef.asm"
__PISTOREF:	; Indect Stores a float (A, E, D, C, B) at location stored in memory, pointed by (IX + HL)
			push de
			ex de, hl	; DE <- HL
			push ix
			pop hl		; HL <- IX
			add hl, de  ; HL <- IX + HL
			pop de
	
__ISTOREF:  ; Load address at hl, and stores A,E,D,C,B registers at that address. Modifies A' register
	        ex af, af'
			ld a, (hl)
			inc hl
			ld h, (hl)
			ld l, a     ; HL = (HL)
	        ex af, af'
	
__STOREF:	; Stores the given FP number in A EDCB at address HL
			ld (hl), a
			inc hl
			ld (hl), e
			inc hl
			ld (hl), d
			inc hl
			ld (hl), c
			inc hl
			ld (hl), b
			ret
			
#line 2881 "solitario.bas"
	
#line 1 "u32tofreg.asm"
	
__I8TOFREG:
		ld l, a
		rlca
		sbc a, a	; A = SGN(A)
		ld h, a
		ld e, a
		ld d, a
	
__I32TOFREG:	; Converts a 32bit signed integer (stored in DEHL)
					; to a Floating Point Number returned in (A ED CB)
	
		ld a, d
		or a		; Test sign
	
		jp p, __U32TOFREG	; It was positive, proceed as 32bit unsigned
	
		call __NEG32		; Convert it to positive
		call __U32TOFREG	; Convert it to Floating point
	
		set 7, e			; Put the sign bit (negative) in the 31bit of mantissa
		ret
	
__U8TOFREG:
					; Converts an unsigned 8 bit (A) to Floating point
		ld l, a
		ld h, 0
		ld e, h
		ld d, h
	
__U32TOFREG:	; Converts an unsigned 32 bit integer (DEHL)
					; to a Floating point number returned in A ED CB
	
	    PROC
	
	    LOCAL __U32TOFREG_END
	
		ld a, d
		or e
		or h
		or l
	    ld b, d
		ld c, e		; Returns 00 0000 0000 if ZERO
		ret z
	
		push de
		push hl
	
		exx
		pop de  ; Loads integer into B'C' D'E' 
		pop bc
		exx
	
		ld l, 128	; Exponent
		ld bc, 0	; DEBC = 0
		ld d, b
		ld e, c
	
__U32TOFREG_LOOP: ; Also an entry point for __F16TOFREG
		exx
		ld a, d 	; B'C'D'E' == 0 ?
		or e
		or b
		or c
		jp z, __U32TOFREG_END	; We are done
	
		srl b ; Shift B'C' D'E' >> 1, output bit stays in Carry
		rr c
		rr d
		rr e
		exx
	
		rr e ; Shift EDCB >> 1, inserting the carry on the left
		rr d
		rr c
		rr b
	
		inc l	; Increment exponent
		jp __U32TOFREG_LOOP
	
	
__U32TOFREG_END:
		exx
	    ld a, l     ; Puts the exponent in a
		res 7, e	; Sets the sign bit to 0 (positive)
	
		ret
	    ENDP
	
#line 2883 "solitario.bas"
	
ZXBASIC_USER_DATA:
_tadrv2:
	DEFB 00, 00
_ee0:
	DEFB 00, 00
_ee1:
	DEFB 00, 00
_i:
	DEFB 00, 00
_x:
	DEFB 00, 00
_y:
	DEFB 00, 00
_ch:
	DEFB 00, 00
_se:
	DEFB 00, 00
_sl:
	DEFB 00, 00
_pf:
	DEFB 00, 00
_gv:
	DEFB 00, 00
_pa:
	DEFB 00, 00
_je:
	DEFB 00, 00
_ka:
	DEFB 00, 00
_tf:
	DEFB 00, 00
_tt:
	DEFB 00, 00
_px:
	DEFB 00, 00
_py:
	DEFB 00, 00
_ox:
	DEFB 00, 00
_oy:
	DEFB 00, 00
_dx:
	DEFB 00, 00
_dy:
	DEFB 00, 00
_sx:
	DEFB 00, 00
_sy:
	DEFB 00, 00
_x1:
	DEFB 00, 00
_y1:
	DEFB 00, 00
_x2:
	DEFB 00, 00
_y2:
	DEFB 00, 00
_mx:
	DEFB 00, 00, 00, 00, 00
_my:
	DEFB 00, 00, 00, 00, 00
_timecount:
	DEFB 00, 00
_stick:
	DEFB 00
_button1:
	DEFB 00
_debug:
	DEFB 00
_tflp1:
	DEFB 00, 00
_eb0:
	DEFB 00
_kprs:
	DEFB 00
_qq:
	DEFB 00, 00
_zz:
	DEFB 00
_ck:
	DEFB 00
_v1:
	DEFB 00, 00
_v2:
	DEFB 00, 00
_iq:
	DEFB 00
_a:
	DEFW 0001h
	DEFW 0008h
	DEFB 02h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
_t:
	DEFW 0000h
	DEFB 04h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	DEFB 00h
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
