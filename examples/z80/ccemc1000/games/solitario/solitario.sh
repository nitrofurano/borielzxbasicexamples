rm solitario.bin solitario.asm mc1000header.bin mc1000tail.bin
zxb.py solitario.bas --asm --org=$((0x0200))
zxb.py solitario.bas --org=$((0x0200))
createmc1000header.py solitario.bin
cat mc1000header.bin > solitario.final.bin
cat solitario.bin >> solitario.final.bin
cat mc1000tail.bin >> solitario.final.bin
mv solitario.final.bin solitario.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py solitario.bin
zip solitario.bin.wav.zip solitario.bin.wav
#rm solitario.bin solitario.bin.wav

mame mc1000 -video opengl -ui_active -resolution0 754x448 -aspect 425:320 -ramsize 48k -cass solitario.bin.wav.zip

#mame mc1000 -video soft -w -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass solitario.bin.wav.zip
#mame sms -video opengl -ui_active -bp ~/.mame/roms -resolution0 754x448 -aspect 425:320 -skip_gameinfo -nowindow -cart1 $filesall&
#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -cass solitario.bin.wav
#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass solitario.bin.wav.zip

# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]
