'- writes a uinteger value (hex) inside 4 bytes of string in ram
sub hexuint2stradr(tdecv as uinteger,tadr as uinteger):
  t2decv=tdecv
  for i=0 to 3
    tbt=48+(t2decv mod 16)
    if tbt>57 then:tbt=tbt+7:end if
    poke tadr+3-i,tbt
    t2decv=int(t2decv/16)
    next
  end sub
