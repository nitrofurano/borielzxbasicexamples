' Knights & Demons
' original version: Manuel Gomez - Master System version: Paulo Silva

#include "library/mc1000screen.bas"
#include "library/mc1000cls.bas"
#include "library/mc1000vpokechr.bas"
#include "library/mc1000rnd.bas"
#include "library/minivadersldir32b.bas"
#include "library/knightsanddemons_charmap.bas"
'#include "library/charmap03_pm30n27e8.bas"
'#include "library/_sms_setz80stackpoint.bas"
'#include "library/smsvdp.bas"
'#include "library/smsfilvrm.bas"
'#include "library/smsldirvm.bas"
'#include "library/smsfilvrmtx.bas"
'#include "library/smsscroll.bas"
'#include "library/smsvpoke.bas"
'#include "library/smsrnd.bas"
'#include "library/smsjoypad.bas"
'#include "library/smspalette.bas"
'#include "library/smsdelay.bas"
'#include "library/smssound.bas"
'#include "library/knightanddemons_spritesheet_sms_6bit_tiles0x20to0x7F.bas"
'#include "library/knightanddemons_spritesheet_sms_6bit_palette.bas"

mc1000screen(152)
mc1000cls(2)

dim eex,eey,ee0,ee1,ee2,ee3,tq2 as uinteger
dim eee0,debug as uinteger
dim d,n,r,c,di,x,y as uinteger
dim su,ba,ca,t,p,mch,jprs,seed as uinteger
dim p1,p2,s1,s2,vl as integer
dim jopadb as ubyte
dim rseed as uinteger

rseed=50

sub mc1000writetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tloc2 as uinteger)
  for tq2=0 to tle2-1
    mc1000vpokechr((ty2*32)+tx2+tq2,peek(ttx2+tq2),tloc2)
    next
  end sub

mc1000vpokechr(5,34,@charmap01-256)

for eey=0 to 15
  for eex=0 to 15
    mc1000vpokechr(7*32+15+eey*32+eex,eey*16+eex,@charmap01-256)
    next:next

mc1000cls(2)

'-----------------------------------------------------------------------
'sub smsputchar(tx1 as uinteger,ty1 as uinteger,tch1 as ubyte, tat1 as ubyte)
'  smsvpoke($3800+(tx1*2)+(ty1*64),tch1)
'  smsvpoke($3801+(tx1*2)+(ty1*64),tat1)
'  end sub
'
'sub smswritetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tat2 as ubyte)
''- improve with ldirvm?
'  dim tq2 as uinteger at $C002
'  for tq2=0 to tle2-1
'    smsputchar(tx2+tq2,ty2,peek(ttx2+tq2),tat2)
'    next
'  end sub
'
'sub smswritedec(tx2 as uinteger,ty2 as uinteger,tvl as uinteger,tle2 as uinteger,tat2 as ubyte)
'  dim txlp as uinteger at $C002:dim tylp as uinteger at $C004
'  tylp=1
'  for txlp=tle2 to 1 step -1
'    smsputchar(tx2+txlp-1,ty2,48+(int(tvl/tylp) mod 10),tat2)
'    tylp=tylp*10
'    next
'  end sub
'
'sub smsputtile(txpos as uinteger,typos as uinteger,txsiz as uinteger,tysiz as uinteger,tadr as uinteger,tatr as ubyte):
''- improve with ldirvm?
'  dim txlp as uinteger at $C002:dim tylp as uinteger at $C004
'  for tylp=0 to tysiz-1
'    for txlp=0 to txsiz-1
'      smsputchar(txpos+txlp,typos+tylp,peek(tadr+(tylp*txsiz)+txlp),tatr)
'      next:next
'  end sub
'
'sub smsfilltile(txpos as uinteger,typos as uinteger,txsiz as uinteger,tysiz as uinteger,tvlr as ubyte,tatr as ubyte):
'  dim txlp as uinteger at $C002:dim tylp as uinteger at $C004
'  for tylp=0 to tysiz-1
'    for txlp=0 to txsiz-1
'      smsputchar(txpos+txlp,typos+tylp,tvlr,tatr)
'      next:next
'  end sub
'
'sub kndeplaysound():
''  '- replace with the equivalent on pv1000
''  'sound 7,62:sound 1,0:sound 0,254:sound 8,16:sound 13,9:sound 12,60:sound 11,0
''  out $F8,20:out $F9,21:out $FA,22
''  for eee0=1 to 2000:next
''  out $F8,63:out $F9,63:out $FA,63
'  smssoundio((1*128)+(0*32)+(0*16)+ %1100 ) '- cm=1, ch=0..3, kd=0, fr=0..15
'  smssoundio((0*128)+ %110101 )             '- cm=0, fr=0..63
'  smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
'  smssoundio((1*128)+(1*32)+(0*16)+ %1110 ) '- cm=1, ch=0..3, kd=0, fr=0..15
'  smssoundio((0*128)+ %101111 )             '- cm=0, fr=0..63
'  smssoundio((1*128)+(1*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
'  smssoundio((1*128)+(2*32)+(0*16)+ %1010 ) '- cm=1, ch=0..3, kd=0, fr=0..15
'  smssoundio((0*128)+ %101010 )             '- cm=0, fr=0..63
'  smssoundio((1*128)+(2*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
'  smsdelay(30000)
'  smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
'  smssoundio((1*128)+(1*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
'  smssoundio((1*128)+(2*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
'  end sub
'
'sub kndecls(tclvc as ubyte):
'  smsfilvrmtx($3800,tclvc,0,768)
''  smsfilltile(0,0,32,24,tclvc)
'  end sub

'-------


'- dim f(8,8) replaced to poke/peek area $C400..$BD3F - find where to put this on sms - $C400..$C43F?

start01:
'- pv1000 doesn't have to set screen mode, only one exists

''- mode 4,2
'smsvdp(0,%00000100):smsvdp(1,%11100010) '- smsvdp(1,$84)
'smsvdp(2,$ff):smsvdp(5,$ff):smsvdp(10,$ff)
'smsvdp(6,$fb) '- sprite patterns - $fb for $0000 (256 sprites available), $ff for $2000 (192 sprites available)
'smsvdp(7,$00) '- border colour (sprite palette)
'
'smsfilvrm($0000,0,$4000) '- replaces "clear vram"
'smsldirvm(32*32,@tiles01,96*32)
'
'for ee2=0 to 15
'  smspalette(ee2,peek(@palette01+ee2))
'  smspalette(ee2+16,peek(@palette01+ee2))
'  next
'
''ee3=0
''for ee2=0 to 767
''  smsvpoke($3800+(ee2*2),ee3 band $FF)
''  ee3=ee3+1
''  next

''- replace with delay?
''for ee2=0 to 50000:next
'smsdelay(10000)

seed=0
debug=1
'poke $C000,0

titulodeljuegoymenu:
''- there is no colour attribute area, no vpokes needed, pv1000 uses mostly rom for tiles
'''- only cls is needed

'''smsfilltile(0,0,32,24,$20) '-cls
''for ee2=0 to 767
''  smsvpoke($3800+(ee2*2),0)
''  next

'kndecls($20)

'smswritetext(8,1,@text01,@text02-@text01,0)   '- locate 8,1:print"KNIGHTS & DEMONS"
'smswritetext(4,2,@text02,@text03-@text02,0)   '- locate 4,2:print"A GAME BY KABUTO FACTORY"
'smswritetext(7,16,@text03b,19,0)              '- locate 2,18:print "MSX VERSION BY MANUEL GOMEZ"
'smswritetext(10,17,@text03b+19,13,0)
'smswritetext(9,19,@text03c,14,0)
'smswritetext(10,20,@text03c+14,12,0)
'smswritetext(5,22,@text04,@text05-@text04,0)  '- locate 4,20:print "(C) 2013 KABUTO FACTORY"
'smswritetext(5,8,@text05,@text06-@text05,0)   '- locate 6,8:print  "SELECT DIFFICULT LEVEL"
'smswritetext(9,10,@text06,@text07-@text06,0)  '- locate 9,10:print "1. EASY LEVEL"
'smswritetext(9,11,@text07,@text08-@text07,0)  '- locate 9,11:print "2. MEDIUM LEVEL"
'smswritetext(9,12,@text08,@text09-@text08,0)  '- locate 9,12:print "3. HARD LEVEL"

mc1000writetext(8,1,@text01,@text02-@text01,@charmap01-256)   '- locate 8,1:print"KNIGHTS & DEMONS"
mc1000writetext(4,2,@text02,@text03-@text02,@charmap01-256)   '- locate 4,2:print"A GAME BY KABUTO FACTORY"
mc1000writetext(7,16,@text03b,19,@charmap01-256)              '- locate 2,18:print "MSX VERSION BY MANUEL GOMEZ"
mc1000writetext(10,17,@text03b+19,13,@charmap01-256)
mc1000writetext(9,19,@text03c,14,@charmap01-256)
mc1000writetext(10,20,@text03c+14,12,@charmap01-256)
mc1000writetext(5,22,@text04,@text05-@text04,@charmap01-256)  '- locate 4,20:print "(C) 2013 KABUTO FACTORY"
mc1000writetext(5,8,@text05,@text06-@text05,@charmap01-256)   '- locate 6,8:print  "SELECT DIFFICULT LEVEL"
mc1000writetext(9,10,@text06,@text07-@text06,@charmap01-256)  '- locate 9,10:print "1. EASY LEVEL"
mc1000writetext(9,11,@text07,@text08-@text07,@charmap01-256)  '- locate 9,11:print "2. MEDIUM LEVEL"
mc1000writetext(9,12,@text08,@text09-@text08,@charmap01-256)  '- locate 9,12:print "3. HARD LEVEL"

di=1:jprs=0

do:loop

'---------------------------------------------------------------

selecciondenivel:
''- there is no keys 1,2 and 3 on sms, replacing it with joystick choosing
'
''IF INKEY$="1" then DI=1:goto variablesglobales
''IF INKEY$="2" then DI=2:goto variablesglobales
''IF INKEY$="3" then DI=3:goto variablesglobales
'
''- needs improvement
'
'joypadb=smsjoypad1() bor smsjoypad2()
'
'if ((joypadb band 5) <>0) and jprs=0 then:    '- y-
'  smswritetext(6,9+di,@finger01+2,2,0):jprs=1:di=di-1
'  end if
'if ((joypadb band 10) <>0) and jprs=0 then:    '- y+, select key
'  smswritetext(6,9+di,@finger01+2,2,0):jprs=1:di=di+1
'  end if
'if di<1 then:di=di+3:end if
'if di>3 then:di=di-3:end if
'
''poke $C000,0
''smsdelay(50)
''smswritedec(2,4,peek($C000),3,0)
''smswritedec(2,4,smsjoypad1(),3,0)
''smswritedec(2,5,smsjoypad2(),3,0)
'
'
'if (joypadb band 15) =0 then:
'  jprs=0:end if
'smswritetext(6,9+di,@finger01,2,0)
'if ((joypadb band 48)<>0) or peek($C000)<>0 then:                              '- buttons or start key
'  poke $C000,0:goto variablesglobales:end if
goto selecciondenivel



variablesglobales:
'kndecls($20)
x=1:y=1:su=0:ba=16

variablesfase:  '- used timer as t*10
if di=1 then ca=10:t=200*10:p=5:end if
if di=2 then ca=20:t=300*10:p=15:end if
if di=3 then ca=30:t=400*10:p=10:end if

255:
if ba<8 or ba>23 then: goto finaldeljuego:end if
gosub pintartablero
gosub pintarcursor         '- the cursor on the game start was missing #####

'---------------

bucleprincipal:
'joypadb=smsjoypad1() bor smsjoypad2()
'
'if ((joypadb band 4)<>0) and x>1 then: d=1:gosub pintarcursor:goto controldeltiempo:end if      'left- if stick(0)=7 and x>1 then d=1:gosub 7000:goto 360
'if ((joypadb band 8)<>0) and x<8 then: d=2:gosub pintarcursor:goto controldeltiempo:end if      'right- if stick(0)=3 and x<8 then d=2:gosub 7000:goto 360
'if ((joypadb band 1)<>0) and y>1 then: d=3:gosub pintarcursor:goto controldeltiempo:end if      'up- if stick(0)=1 and y>1 then d=3:gosub 7000:goto 360
'if ((joypadb band 2)<>0) and y<8 then: d=4:gosub pintarcursor:goto controldeltiempo:end if      'down- if stick(0)=5 and y<8 then d=4:gosub 7000:goto 360
'if ((joypadb band 16) <>0) then:  '- if strig(0)=-1 then d=4:gosub 7500:goto 360
'  d=0                      '- previously was d=4, cursor was moving down after cambiarfichas #####
'  gosub cambiarfichas
'  gosub pintarcursor       '- added pintarcursor, missing after cambiarfichas #####
'  goto controldeltiempo
'  end if
'if ((joypadb band 32)<>0) and p>0 then:  '- if inkey$="z" and p>0 then gosub 6500:p=p-1:goto 360
'  gosub utilizaunapica
'  d=0                      '- no idea about this
'  gosub pintarcursor       '- added pintarcursor, missing after utilizaunapica #####
'  p=p-1:goto controldeltiempo
'  end if
'if peek($C000)<>0 then:               '- start key
'  gosub pausa
'  end if

'---------------

controldeltiempo:
t=t-1

'---------------

controldeltiempodelay: '- there would be a better place for this, like at the loop's end?
for eee0=1 to 4000:next
'smsdelay(15000)

'---------------

condiciones:
'if t=0 then: goto gameover:end if
'if su=64 or su=128 then: goto pasadefase:end if
'smswritetext(25,8,@text09,@text10-@text09,0)  '- locate 25,8:print"PIKES"
'smswritedec(26,9,p,3,0)                       '- locate 26,9:print p
'smswritetext(2,8,@text10,@text11-@text10,0)   '- locate 2,8:print"TIME "
'smswritedec(2,9,int(t/10),3,0)                '- locate 2,9:print int(t)

goto bucleprincipal

'---------------

pasadefase:
if su=64 then:
'  smswritetext(11,9,@text11,@text12-@text11,0) '- LOCATE 11,9:PRINT"BOARD CLEAR"
'  smswritetext(7,11,@text12,@text13-@text12,0) '- LOCATE 7,11:PRINT"...ON THE GOOD SIDE"
  ba=ba-1:su=0:x=1:y=1:goto 6020
  end if
if su=128 then:
'  smswritetext(11,9,@text11,@text12-@text11,0) '- LOCATE 11,9:PRINT"BOARD CLEAR"
'  smswritetext(7,11,@text13,@text14-@text13,0) '- LOCATE 7,11:PRINT"...ON THE EVIL SIDE"
  ba=ba+1:su=0:x=1:y=1:goto 6020
  end if
6020:
'- wouldn't be better t=(di+1)*100 ?  '- used timer as t*10
if di=1 then: t=200*10:end if
if di=2 then: t=300*10:end if
if di=3 then: t=400*10:end if
'smsdelay(60000)
'smsdelay(60000)
goto 255 '- is the delay longer enough? '-##

'---------------

utilizaunapica:
'if peek($C400+y*8+x)=1 then: '- IF F(X,Y)=1 then
'  poke $C400+y*8+x,2         '-  F(X,Y)=2
'  su=su+1
'  goto 6510
'  end if
'if peek($C400+y*8+x)=2 then: '- IF F(X,Y)=2 then
'  poke $C400+y*8+x,1         '-  F(X,Y)=1
'  su=su-1
'  goto 6510
'  end if
6510:
'smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile06,0)    '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"xx":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"xx"
'for n=0 to 1000:next n								  ' delay for explosion
'if peek ($C400+y*8+x)=1 then:                         '- IF F(X,Y)=1 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile01,0)  '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"ab":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"cd"
'  kndeplaysound()
'  end if
'if peek ($C400+y*8+x)=2 then:                         '- IF F(X,Y)=2 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile02,0)  '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"hi":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"jk"
'  kndeplaysound()
'  end if
'smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile06,0)  '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"yy":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"yy"
'return

'---------------

pintarcursor:
'if peek($C400+y*8+x)=1 then:                          '- IF F(X,Y)=1 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile01,0)  '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"ab":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"cd"
'  end if
'if peek($C400+y*8+x)=2 then:                          '- IF F(X,Y)=2 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile02,0)  '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"hi":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"jk"
'  end if

if d=1 then:x=x-1:end if
if d=2 then:x=x+1:end if
if d=3 then:y=y-1:end if
if d=4 then:y=y+1:end if

''- replaced the cursor here
replacedthecursorhere:
''LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"yy":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"yy"
'if peek($C400+y*8+x)=1 then:
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile03,0)
'  end if
'if peek($C400+y*8+x)=2 then:
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile04,0)
'  end if
'return

'---------------

cambiarfichas:
minirutinadecambio:
'if peek($C400+y*8+x)=1 then:  '- IF F(X,Y)=1 then
'  poke $C400+y*8+x,2          '- F(X,Y)=2
'  su=su+1
'  goto 7508
'  end if
'if peek($C400+y*8+x)=2 then:  '- IF F(X,Y)=2 then
'  poke $C400+y*8+x,1          '- F(X,Y)=1
'  su=su-1
'  goto 7508
'  end if
7508:
'if y>1 then: goto 9101:end if
'if y>1 then: goto 9102:end if
7510:
'if y<8 then: goto 9103:end if
'if y<8 then: goto 9104:end if
7512:
'if x>1 then: goto 9105:end if
'if x>1 then: goto 9106:end if
7514:
'if x<8 then: goto 9107:end if
'if x<8 then: goto 9108:end if
7520:

'---------------

minirutinadepintado:
'smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile06,0)      '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"xx":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"xx"
'for n=0 to 1000:next n								  ' delay for explosion
'if peek($C400+y*8+x)=1 then:                            '- IF F(X,Y)=1 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile01,0)    '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"ab":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"cd"
'  end if
'if peek($C400+y*8+x)=2 then:                            '- IF F(X,Y)=2 then
'  smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile02,0)    '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"hi":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"jk"
'  end if
if y>1 then:
'  smsputtile((8+2*(x-1)),(3+2*((y-1)-1)),2,2,@tile06,0)    '- LOCATE (8+2*(X-1)),(3+2*((Y-1)-1)):PRINT"xx":LOCATE (8+2*(X-1)),(3+2*((Y-1)-1))+1:PRINT"xx"
'  for n=0 to 1000:next n								  ' delay for explosion
  end if
'if y>1 then: goto 9001:end if
'if y>1 then: goto 9002:end if
7535:
'if y<8 then:
'  smsputtile((8+2*(x-1)),(3+2*((y+1)-1)),2,2,@tile06,0)    '- LOCATE (8+2*(X-1)),(3+2*((Y+1)-1)):PRINT"xx":LOCATE (8+2*(X-1)),(3+2*((Y+1)-1))+1:PRINT"xx"
'  for n=0 to 1000:next n								  ' delay for explosion
'  end if
'if y<8 then: goto 9003:end if
'if y<8 then: goto 9004:end if
7540:
'if x>1 then:
'  smsputtile((8+2*((x-1)-1)),(3+2*(y-1)),2,2,@tile06,0)    '- LOCATE (8+2*((X-1)-1)),(3+2*(Y-1)):PRINT"xx":LOCATE (8+2*((X-1)-1)),(3+2*(Y-1))+1:PRINT"xx"
'  for n=0 to 1000:next n								  ' delay for explosion
'  end if
'if x>1 then: goto 9005:end if
'if x>1 then: goto 9006:end if
7545:
'if x<8 then
'  smsputtile((8+2*((x+1)-1)),(3+2*(y-1)),2,2,@tile06,0)    '- LOCATE (8+2*((X+1)-1)),(3+2*(Y-1)):PRINT"xx":LOCATE (8+2*((X+1)-1)),(3+2*(Y-1))+1:PRINT"xx"
'  for n=0 to 1000:next n								  ' delay for explosion
'  end if
'if x<8 then: goto 9007:end if
'if x<8 then: goto 9008:end if
7550:
'smsputtile((8+2*(x-1)),(3+2*(y-1)),2,2,@tile06,0)      'I think this is useless    '- LOCATE (8+2*(X-1)),(3+2*(Y-1)):PRINT"yy":LOCATE (8+2*(X-1)),(3+2*(Y-1))+1:PRINT"yy"
'return

'---------------

pintartablero:
'smsfilltile(0,0,32,24,$20,0)                 '- CLS
'kndecls($20)

'smswritetext(5,10,@text14,@text15-@text14,0)  '- LOCATE 5,10:PRINT"A NEW BATTLE BEGINS..."
'smsdelay(60000)
'smsdelay(60000)

for r=1 to 8:for c=1 to 8
'  poke $C400+r*8+c,1                           '- F(C,R)=1:SU=SU+1
  su=su+1
  next:next
for n=1 to ca
'  seed=smsrnd(seed):r=(seed band 7)+1  '- R=INT(RND(1)*8)+1
'  seed=smsrnd(seed):c=(seed band 7)+1  '- C=INT(RND(1)*8)+1
'  if peek($C400+r*8+c)=1 then:  '- IF F(C,R)=1 then
'    poke $C400+r*8+c,2          '- F(C,R)=2
'    su=su+1
'    goto 8010
'    end if
'  if peek($C400+r*8+c)=2 then:  '- IF F(C,R)=2 then
'    poke $C400+r*8+c,1          '-  F(C,R)=1
'    su=su-1
'    goto 8010
'    end if
8010:
  if r>1 then
'    if peek($C400+(r-1)*8+c)=1 then: '- IF R>1 then IF F(C,R-1)=1 then
'      poke $C400+(r-1)*8+c,2         '- F(C,R-1)=2
'      su=su+1
'      goto 8012
'      end if
    end if
  if r>1 then:
'    if peek($C400+(r-1)*8+c)=2 then: '- IF R>1 then IF F(C,R-1)=2 then
'      poke $C400+(r-1)*8+c,1         '- F(C,R-1)=1
'      su=su-1
'      goto 8012
'      end if
    end if
8012:
  if r<8 then:
'    if peek($C400+(r+1)*8+c)=1 then: '- IF R<8 then IF F(C,R+1)=1 then
'      poke $C400+(r+1)*8+c,2         '- F(C,R+1)=2
'      su=su+1
'      goto 8014
'      end if
    end if
  if r<8 then:
'    if peek($C400+(r+1)*8+c)=2 then: '- IF R<8 then IF F(C,R+1)=2 then
'      poke $C400+(r+1)*8+c,1         '- F(C,R+1)=1
'      su=su-1
'      goto 8014
'      end if
    end if
8014:
  if c>1 then:
'    if peek($C400+r*8+(c-1))=1 then: '- IF C>1 then IF F(C-1,R)=1 then
'      poke $C400+r*8+(c-1),2         '- F(C-1,R)=2
'      su=su+1
'      goto 8016
'      end if
    end if
  if c>1 then:
'    if peek($C400+r*8+(c-1))=2 then:  '- IF C>1 then IF F(C-1,R)=2 then
'      poke $C400+r*8+(c-1),1          '- F(C-1,R)=1
'      su=su-1
'      goto 8016
'      end if
    end if
8016:
  if c<8 then:
'    if peek($C400+r*8+(c+1))=1 then:  '- IF C<8 then IF F(C+1,R)=1 then
'      poke $C400+r*8+(c+1),2          '- F(C+1,R)=2
'      su=su+1
'      goto 8018
'      end if
    end if
  if c<8 then:
'    if peek ($C400+r*8+(c+1))=2 then:  '- IF C<8 then IF F(C+1,R)=2 then
'      poke $C400+r*8+(c+1),1           '- F(C+1,R)=1
'      su=su-1
'      goto 8018
'      end if
    end if
8018:
  next

''smsfilltile(0,0,32,24,$20,0)                '- CLS
'kndecls($20)

''smsfilltile(0,0,32,22,$7E,0)                 '- FOR N=0 TO 21:LOCATE 0,N:PRINT"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy":NEXT N
'smsfilvrmtx($3800,$7E,0,32*22)

'smswritetext(8,20,@text01,@text02-@text01,0) '- LOCATE 8,20:PRINT"KNIGHTS & DEMONS"
'smsfilltile(8,0,16,1,code("i"),0)            '- LOCATE 8,0:PRINT"pppppppppppppppp"
'smsfilltile(8,1,16,1,code("y"),0)            '- LOCATE 8,1:PRINT"qqqqqqqqqqqqqqqq"
'smsputtile(6,0,2,2,@tile01,0)                '- LOCATE 6,0:PRINT"ab":LOCATE 6,1:PRINT"cd"
'smsputtile(24,0,2,2,@tile02,0)               '- LOCATE 24,0:PRINT"hi":LOCATE 24,1:PRINT"jk"
'smsputtile(ba,0,1,2,@snakesheads01,0)        '- LOCATE BA,0:PRINT"e":LOCATE BA,1:PRINT"f"
for r=1 to 8
  for c=1 to 8
'    if peek($C400+r*8+c)=1 then:                          '- 'IF F(C,R)=1 then
'      smsputtile((8+2*(c-1)),(3+2*(r-1)),2,2,@tile01,0)  '-  LOCATE (8+2*(C-1)),(3+2*(R-1)):PRINT"ab":LOCATE (8+2*(C-1)),(3+2*(R-1))+1:PRINT"cd"
'      end if
'    if peek($C400+r*8+c)=2 then:                          '- 'IF F(C,R)=2 then
'      smsputtile((8+2*(c-1)),(3+2*(r-1)),2,2,@tile02,0)  '-  LOCATE (8+2*(C-1)),(3+2*(R-1)):PRINT"hi":LOCATE (8+2*(C-1)),(3+2*(R-1))+1:PRINT"jk"
'      end if
    next:next
'smsputtile((8+2*(c-1)),(3+2*(r-1)),2,2,@tile06,0)     'this was the explosion bug in the scoreboard
return

'---------------

pausa:  '- smsboot.bas at $0066 writes $FF at $C000 - it seems to affect some routines
'smswritetext(13,2,@text28,@text29-@text28,0)            '- LOCATE 13,2:PRINT"PAUSED"
'buclepausa1:
'if peek($C000)<>0 then:goto buclepausa1:end if
'smsfilltile(13,2,6,1,$7E,0)
return

'---------------

gameover:
'smswritetext(12,10,@text15,@text16-@text15,0)            '- LOCATE 12,10:PRINT"GAME OVER"
''for n=0 to 1000
''  for eee0=1 to 100:next  '- perhaps needs a longer delay '- ##
''  next n
'smsdelay(60000)
'smsdelay(60000)
goto titulodeljuegoymenu

'---------------
finaldeljuego:
if ba>23 then:
'  'smsfilltile(0,0,32,24,$20)                 '- CLS
'  kndecls($20)
'  smswritetext(2,8,@text16,@text17-@text16,0)   '- LOCATE 2,8:PRINT "THE EVIL LORD IS LOOKING FOR"
'  smswritetext(3,9,@text17,@text18-@text17,0)   '- LOCATE 3,9:PRINT "A PUPIL AND YOU'RE THE BEST"
'  smswritetext(12,10,@text18,@text19-@text18,0) '- LOCATE 12,10:PRINT "CANDIDATE"
'  smswritetext(4,13,@text19,@text20-@text19,0)  '- LOCATE 4,13:PRINT "WELCOME TO THE DARK SIDE"
'  smsdelay(60000):smsdelay(60000):smsdelay(60000):smsdelay(60000)
  goto titulodeljuegoymenu
  end if
if ba<8 and di=1 then:
'  'smsfilltile(0,0,32,24,$20)                  '- CLS
'  kndecls($20)
'  smswritetext(2,8,@text20,@text21-@text20,0)   '- LOCATE 2,8:PRINT "BAKELOR IS HAPPY WITH YOU..."
'  smswritetext(3,9,@text21,@text22-@text21,0)   '- LOCATE 3,9:PRINT "BUT THERE IS A LOT OF WORK "
'  smswritetext(12,10,@text22,@text23-@text22,0) '- LOCATE 12,10:PRINT "TO DO...."
'  smswritetext(4,13,@text23,@text24-@text23,0)  '- LOCATE 4,13:PRINT "TRY WITH A HARDER LEVEL."
'  smsdelay(60000):smsdelay(60000):smsdelay(60000):smsdelay(60000)
  goto titulodeljuegoymenu
  end if
if ba<8 and di=2 then:
'  'smsfilltile(0,0,32,24,$20)                 '- CLS
'  kndecls($20)
'  smswritetext(2,8,@text20,@text21-@text20,0)   '- LOCATE 2,8:PRINT "BAKELOR IS HAPPY WITH YOU..."
'  smswritetext(3,9,@text21,@text22-@text21,0)   '- LOCATE 3,9:PRINT "BUT THERE IS A LOT OF WORK "
'  smswritetext(12,10,@text22,@text23-@text22,0) '- LOCATE 12,10:PRINT "TO DO...."
'  smswritetext(4,13,@text23,@text24-@text23,0)  '- LOCATE 4,13:PRINT "TRY WITH A HARDER LEVEL."
'  smsdelay(60000):smsdelay(60000):smsdelay(60000):smsdelay(60000)
  goto titulodeljuegoymenu
  end if
if ba<8 and di=3 then:
'  'smsfilltile(0,0,32,24,$20)                 '- CLS
'  kndecls($20)
'  smswritetext(2,8,@text24,@text25-@text24,0)   '- LOCATE 2,8:PRINT "YOU ARE THE NEW LORD OF WAR."
'  smswritetext(3,9,@text25,@text26-@text25,0)   '- LOCATE 3,9:PRINT "YOU'VE MASTERED THE GAME..."
'  smswritetext(12,10,@text26,@text27-@text26,0) '- LOCATE 12,10:PRINT "         "
'  smswritetext(4,13,@text27,@text28-@text27,0)  '- LOCATE 4,13:PRINT "THANKS FOR PLAYING......"
'  smsdelay(60000):smsdelay(60000):smsdelay(60000):smsdelay(60000)
  goto titulodeljuegoymenu
  end if

'---------------
pintadodeifs:
9001:
'if peek ($C400+(y-1)*8+x)=1 then:                         '- IF F(X,Y-1)=1 then
'  smsputtile((8+2*(x-1)),(3+2*((y-1)-1)),2,2,@tile01,0)  '- LOCATE (8+2*(X-1)),(3+2*((Y-1)-1)):PRINT"ab":LOCATE (8+2*(X-1)),(3+2*((Y-1)-1))+1:PRINT"cd"
'  kndeplaysound()
'  goto 7535
'  end if
9002:
'if peek ($C400+(y-1)*8+x)=2 then:                         '- IF F(X,Y-1)=2 then
'  smsputtile((8+2*(x-1)),(3+2*((y-1)-1)),2,2,@tile02,0)  '- LOCATE (8+2*(X-1)),(3+2*((Y-1)-1)):PRINT"hi":LOCATE (8+2*(X-1)),(3+2*((Y-1)-1))+1:PRINT"jk"
'  kndeplaysound()
'  goto 7535
'  end if
9003:
'if peek ($C400+(y+1)*8+x)=1 then:                         '- IF F(X,Y+1)=1 then
'  smsputtile((8+2*(x-1)),(3+2*((y+1)-1)),2,2,@tile01,0)  '- LOCATE (8+2*(X-1)),(3+2*((Y+1)-1)):PRINT"ab":LOCATE (8+2*(X-1)),(3+2*((Y+1)-1))+1:PRINT"cd"
'  kndeplaysound()
'  goto 7540
'  end if
9004:
'if peek ($C400+(y+1)*8+x)=2 then:                         '- IF F(X,Y+1)=2 then
'  smsputtile((8+2*(x-1)),(3+2*((y+1)-1)),2,2,@tile02,0)  '- LOCATE (8+2*(X-1)),(3+2*((Y+1)-1)):PRINT"hi":LOCATE (8+2*(X-1)),(3+2*((Y+1)-1))+1:PRINT"jk"
'  kndeplaysound()
'  goto 7540
'  end if
9005:
'if peek ($C400+y*8+(x-1))=1 then:                           '- IF F(X-1,Y)=1 then
'  smsputtile((8+2*((x-1)-1)),(3+2*(y-1)),2,2,@tile01,0)  '- LOCATE (8+2*((X-1)-1)),(3+2*(Y-1)):PRINT"ab":LOCATE (8+2*((X-1)-1)),(3+2*(Y-1))+1:PRINT"cd"
'  kndeplaysound()
'  goto 7545
'  end if
9006:
'if peek ($C400+y*8+(x-1))=2 then:                           '- IF F(X-1,Y)=2 then
'  smsputtile((8+2*((x-1)-1)),(3+2*(y-1)),2,2,@tile02,0)  '- LOCATE (8+2*((X-1)-1)),(3+2*(Y-1)):PRINT"hi":LOCATE (8+2*((X-1)-1)),(3+2*(Y-1))+1:PRINT"jk"
'  kndeplaysound()
'  goto 7545
'  end if
9007:
'if peek ($C400+y*8+(x+1))=1 then:                           '- IF F(X+1,Y)=1 then
'  smsputtile((8+2*((x+1)-1)),(3+2*(y-1)),2,2,@tile01,0)  '- LOCATE (8+2*((X+1)-1)),(3+2*(Y-1)):PRINT"ab":LOCATE (8+2*((X+1)-1)),(3+2*(Y-1))+1:PRINT"cd"
'  kndeplaysound()
'  goto 7550
'  end if
9008:
'if peek ($C400+y*8+(x+1))=2 then:                           '- IF F(X+1,Y)=2 then
'  smsputtile((8+2*((x+1)-1)),(3+2*(y-1)),2,2,@tile02,0)  '- LOCATE (8+2*((X+1)-1)),(3+2*(Y-1)):PRINT"hi":LOCATE (8+2*((X+1)-1)),(3+2*(Y-1))+1:PRINT"jk"
'  kndeplaysound()
'  goto 7550
'  end if

'---------------

cambiodeifs:
9101:
'if peek($C400+(y-1)*8+x)=1 then:  '- 'IF F(X,Y-1)=1 then
'  poke $C400+(y-1)*8+x,2          '-  F(X,Y-1)=2
'  su=su+1
'  goto 7510
'  end if
9102:
'if peek($C400+(y-1)*8+x)=2 then:  '- 'IF F(X,Y-1)=2 then
'  poke $C400+(y-1)*8+x,1          '-  F(X,Y-1)=1
'  su=su-1
'  goto 7510
'  end if
9103:
'if peek($C400+(y+1)*8+x)=1 then:  '- 'IF F(X,Y+1)=1 then
'  poke $C400+(y+1)*8+x,2          '-  F(X,Y+1)=2
'  su=su+1
'  goto 7512
'  end if
9104:
'if peek($C400+(y+1)*8+x)=2 then:  '- 'IF F(X,Y+1)=2 then
'  poke $C400+(y+1)*8+x,1          '-  F(X,Y+1)=1
'  su=su-1
'  goto 7512
'  end if
9105:
'if peek($C400+y*8+(x-1))=1 then:  '- 'IF F(X-1,Y)=1 then
'  poke $C400+y*8+(x-1),2          '-  F(X-1,Y)=2
'  su=su+1
'  goto 7514
'  end if
9106:
'if peek($C400+y*8+(x-1))=2 then:  '- 'IF F(X-1,Y)=2 then
'  poke $C400+y*8+(x-1),1          '-  F(X-1,Y)=1
'  su=su-1
'  goto 7514
'  end if
9107:
'if peek($C400+y*8+(x+1))=1 then:  '- 'IF F(X+1,Y)=1 then
'  poke $C400+y*8+(x+1),2          '-  F(X+1,Y)=2
'  su=su+1
'  goto 7520
'  end if
9108:
'if peek($C400+y*8+(x+1))=2 then:  '- 'IF F(X+1,Y)=2 then
'  poke $C400+y*8+(x+1),1          '-  F(X+1,Y)=1
'  su=su-1
'  goto 7520
'  end if

'------------------ texts and tiles --------------------------------------------
'---- texts -----
text01:
asm
  defb "KNIGHTS & DEMONS"
  end asm
text02:
asm
  defb "A GAME BY KABUTO FACTORY"
  end asm
text03:
text03a:
asm
  ;defb "MSX VERSION BY MANUEL GOMEZ"
  defb "CASIO PV-1000 VERSION BY MANUEL GOMEZ"
  end asm
text03b:
asm
  defb "ORIGINAL VERSION BY MANUEL GOMEZ"
  end asm
text03c:
asm
  defb "SMS VERSION BY PAULO SILVA"
  end asm
text03d:
text04:
asm
  defb $7F
  defb " 2015 KABUTO FACTORY"
  end asm
text05:
asm
  defb "SELECT DIFFICULT LEVEL"
  end asm
text06:
asm
  defb "1. EASY LEVEL  "
  end asm
text07:
asm
  defb "2. MEDIUM LEVEL"
  end asm
text08:
asm
  defb "3. HARD LEVEL  "
  end asm
text09:
asm
  defb "PIKES"
  end asm
text10:
asm
  defb "TIME "
  end asm
text11:
asm
  defb "BOARD CLEAR"
  end asm
text12:
asm
  defb "...ON THE GOOD SIDE"
  end asm
text13:
asm
  defb "...ON THE EVIL SIDE"
  end asm
text14:
asm
  defb "A NEW BATTLE BEGINS..."
  end asm
text15:
asm
  defb "GAME OVER"
  end asm
text16:
asm
  defb "THE EVIL LORD IS LOOKING FOR"
  end asm
text17:
asm
  defb "A PUPIL AND YOU'RE THE BEST"
  end asm
text18:
asm
  defb "CANDIDATE"
  end asm
text19:
asm
  defb "WELCOME TO THE DARK SIDE"
  end asm
text20:
asm
  defb "BAKELOR IS HAPPY WITH YOU..."
  end asm
text21:
asm
  defb "BUT THERE IS A LOT OF WORK "
  end asm
text22:
asm
  defb "TO DO...."
  end asm
text23:
asm
  defb "TRY WITH A HARDER LEVEL."
  end asm
text24:
asm
  defb "YOU ARE THE NEW LORD OF WAR."
  end asm
text25:
asm
  defb "YOU'VE MASTERED THE GAME..."
  end asm
text26:
asm
  defb "         "
  end asm
text27:
asm
  defb "THANKS FOR PLAYING......"
  end asm
text28:
asm
  defb "PAUSED"
  end asm
text29:

'---- tiles -----
tile01:
asm
  defb "`apq"
  end asm
tile02:
asm
  defb "bcrs"
  end asm
tile03:
asm
  defb "detu"
  end asm
tile04:
asm
  defb "fgvw"
  end asm
tile05:
asm
  defb "hixy"
  end asm
tile06:
asm
  defb "jkz{"
  end asm
tile07:
finger01:
asm
  defb $6E,$6F,$20,$20
  end asm
snakesheads01:
asm
  defb "hx"
  end asm

'---- misc -----
tilechry01:
asm
  defb $7E,$7E,$7E,$7E
  end asm

'-------------------------------------------------------------------------------

'rseed=mc1000rnd(rseed) band 255
'p1=int(rseed*3)
'rseed=mc1000rnd(rseed) band 255
's1=int(rseed/8)+8

'rseed=mc1000rnd(rseed) band 255
'p2=int(rseed*3)
'rseed=mc1000rnd(rseed) band 255
's2=int(rseed/8)+8

'lp01:
'  rseed=mc1000rnd(rseed)
'  vl= (128+int(rseed/1.6))band 255

'  mc1000vpokechr(p1,vl,@charmap01)
'  p1=(p1+1)mod 768
'  s1=s1-1
'  if s1<1 then:
'    rseed=mc1000rnd(rseed) band 255
'    p1=int(rseed*3)
'    rseed=mc1000rnd(rseed) band 255
'    s1=int(rseed/8)+8
'    end if
'  mc1000vpokechr(p2,32,@charmap01)
'  p2=(p2+1)mod 768
'  s2=s2-1
'  if s2<1 then:
'    rseed=mc1000rnd(rseed) band 255
'    p2=int(rseed*3)
'    rseed=mc1000rnd(rseed) band 255
'    s2=int(rseed/8)+8
'    end if

 ' goto lp01
