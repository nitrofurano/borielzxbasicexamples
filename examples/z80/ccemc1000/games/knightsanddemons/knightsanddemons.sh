rm knightsanddemons.bin knightsanddemons.asm mc1000header.bin mc1000tail.bin
zxb.py knightsanddemons.bas --asm --org=$((0x0200))
zxb.py knightsanddemons.bas --org=$((0x0200))
createmc1000header.py knightsanddemons.bin
cat mc1000header.bin > knightsanddemons.final.bin
cat knightsanddemons.bin >> knightsanddemons.final.bin
cat mc1000tail.bin >> knightsanddemons.final.bin
mv knightsanddemons.final.bin knightsanddemons.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py knightsanddemons.bin
zip knightsanddemons.bin.wav.zip knightsanddemons.bin.wav
rm knightsanddemons.bin.wav

mame mc1000 -video soft -w -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass knightsanddemons.bin.wav.zip

#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass knightsanddemons.bin.wav.zip
# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]
