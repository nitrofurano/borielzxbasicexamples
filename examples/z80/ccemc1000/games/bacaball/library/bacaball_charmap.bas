goto charmap01end:
charmap01:
asm

  ;$11..$1F
  defb $00,$55,$55,$55,$55,$55,$55,$55
  defb $00,$44,$55,$55,$55,$55,$50,$50
  defb $55,$55,$55,$55,$45,$45,$44,$00
  defb $40,$40,$40,$41,$45,$41,$41,$01
  defb $40,$40,$40,$45,$41,$45,$44,$05
  defb $40,$40,$40,$45,$41,$45,$41,$05
  defb $40,$40,$40,$54,$54,$55,$44,$04
  defb $14,$14,$55,$55,$55,$55,$14,$14
  defb $00,$11,$55,$55,$55,$55,$05,$05
  defb $00,$55,$55,$55,$55,$55,$55,$55
  defb $55,$55,$55,$55,$51,$51,$11,$00
  defb $01,$01,$01,$11,$51,$11,$11,$10
  defb $01,$01,$01,$51,$11,$51,$41,$50
  defb $01,$01,$01,$51,$11,$51,$11,$50
  defb $01,$01,$01,$51,$51,$55,$11,$10

  ;$20..$2F
  defb $AA,$AA,$AA,$AA,$AA,$AA,$AA,$AA
  defb $AA,$A2,$A2,$A2,$A2,$AA,$A2,$AA
  defb $AA,$88,$88,$AA,$AA,$AA,$AA,$AA
  defb $AA,$88,$A2,$88,$88,$A2,$88,$AA
  defb $AA,$A2,$80,$8A,$80,$A8,$80,$A2
  defb $AA,$88,$A8,$A2,$A2,$8A,$88,$AA
  defb $AA,$80,$88,$A2,$8A,$88,$80,$AA
  defb $AA,$A8,$A2,$AA,$AA,$AA,$AA,$AA
  defb $AA,$A2,$8A,$8A,$8A,$8A,$A2,$AA
  defb $AA,$A2,$A8,$A8,$A8,$A8,$A2,$AA
  defb $AA,$AA,$88,$A2,$80,$A2,$88,$AA
  defb $AA,$AA,$A2,$A2,$80,$A2,$A2,$AA
  defb $AA,$AA,$AA,$AA,$AA,$AA,$A8,$A0
  defb $AA,$AA,$AA,$AA,$80,$AA,$AA,$AA
  defb $AA,$AA,$AA,$AA,$AA,$A2,$A2,$AA
  defb $AA,$A8,$A8,$A2,$A2,$8A,$8A,$AA

  ;$30..$3F
  defb $AA,$80,$88,$88,$88,$88,$80,$AA
  defb $AA,$82,$A2,$A2,$A2,$A2,$A2,$AA
  defb $AA,$80,$88,$A8,$80,$8A,$80,$AA
  defb $AA,$80,$88,$A0,$A8,$88,$80,$AA
  defb $AA,$88,$88,$88,$80,$A8,$A8,$AA
  defb $AA,$80,$8A,$80,$A8,$A8,$80,$AA
  defb $AA,$80,$8A,$80,$88,$88,$80,$AA
  defb $AA,$80,$A8,$A8,$80,$8A,$8A,$AA
  defb $AA,$80,$88,$A2,$88,$88,$80,$AA
  defb $AA,$80,$88,$88,$80,$A8,$80,$AA
  defb $AA,$AA,$A2,$A2,$AA,$A2,$A2,$AA
  defb $AA,$AA,$A2,$A2,$AA,$A2,$8A,$AA
  defb $AA,$AA,$A8,$A2,$8A,$A2,$A8,$AA
  defb $AA,$AA,$AA,$80,$AA,$80,$AA,$AA
  defb $AA,$AA,$8A,$A2,$A8,$A2,$8A,$AA
  defb $AA,$A2,$88,$A8,$A2,$AA,$A2,$AA

  ;$40..$4F
  defb $AA,$80,$88,$80,$80,$8A,$80,$AA
  defb $AA,$80,$88,$88,$80,$88,$88,$AA
  defb $AA,$80,$88,$82,$88,$88,$80,$AA
  defb $AA,$80,$88,$8A,$8A,$88,$80,$AA
  defb $AA,$82,$88,$88,$88,$88,$82,$AA
  defb $AA,$80,$8A,$80,$8A,$8A,$80,$AA
  defb $AA,$80,$8A,$82,$8A,$8A,$8A,$AA
  defb $AA,$80,$8A,$8A,$88,$88,$80,$AA
  defb $AA,$88,$88,$80,$88,$88,$88,$AA
  defb $AA,$80,$A2,$A2,$A2,$A2,$80,$AA
  defb $AA,$A0,$A8,$A8,$88,$88,$80,$AA
  defb $AA,$88,$88,$82,$88,$88,$88,$AA
  defb $AA,$8A,$8A,$8A,$8A,$8A,$80,$AA
  defb $AA,$88,$80,$80,$80,$88,$88,$AA
  defb $AA,$88,$80,$88,$88,$88,$88,$AA
  defb $AA,$80,$88,$88,$88,$88,$80,$AA

  ;$50..$5F
  defb $AA,$80,$88,$88,$80,$8A,$8A,$AA
  defb $AA,$80,$88,$88,$88,$80,$A0,$AA
  defb $AA,$80,$88,$88,$82,$88,$88,$AA
  defb $AA,$80,$8A,$80,$A8,$A8,$80,$AA
  defb $AA,$80,$A2,$A2,$A2,$A2,$A2,$AA
  defb $AA,$88,$88,$88,$88,$88,$80,$AA
  defb $AA,$88,$88,$88,$88,$80,$A2,$AA
  defb $AA,$88,$88,$80,$80,$80,$88,$AA
  defb $AA,$88,$88,$A2,$88,$88,$88,$AA
  defb $AA,$88,$88,$80,$A2,$A2,$A2,$AA
  defb $AA,$80,$A8,$A2,$8A,$8A,$80,$AA
  defb $AA,$82,$8A,$8A,$8A,$8A,$82,$AA
  defb $AA,$8A,$8A,$A2,$A2,$A8,$A8,$AA
  defb $AA,$A0,$A8,$A8,$A8,$A8,$A0,$AA
  defb $AA,$A2,$88,$AA,$AA,$AA,$AA,$AA
  defb $00,$28,$00,$20,$20,$00,$28,$00

  ;$60..$6F
  defb $00,$2A,$22,$22,$22,$22,$2A,$00
  defb $00,$28,$08,$08,$08,$08,$08,$00
  defb $00,$2A,$22,$02,$2A,$20,$2A,$00
  defb $00,$2A,$22,$0A,$02,$22,$2A,$00
  defb $00,$22,$22,$22,$2A,$02,$02,$00
  defb $00,$2A,$20,$2A,$02,$02,$2A,$00
  defb $00,$2A,$20,$2A,$22,$22,$2A,$00
  defb $00,$2A,$02,$02,$2A,$20,$20,$00
  defb $00,$2A,$22,$08,$22,$22,$2A,$00
  defb $00,$2A,$22,$22,$2A,$02,$2A,$00
  defb $00,$00,$00,$08,$00,$00,$08,$00
  defb $00,$80,$02,$20,$00,$80,$08,$00
  defb $AA,$BB,$FF,$FF,$FF,$FF,$AF,$AF
  defb $AA,$FF,$FF,$FF,$FF,$FF,$FF,$FF
  defb $AB,$AB,$AB,$AB,$AB,$AB,$AB,$AB
  defb $FF,$FF,$FF,$FF,$FB,$FB,$BB,$AA

  ;$70..$7A
  defb $00,$3F,$33,$33,$33,$33,$3F,$00
  defb $00,$3C,$0C,$0C,$0C,$0C,$0C,$00
  defb $00,$3F,$33,$03,$3F,$30,$3F,$00
  defb $00,$3F,$33,$0F,$03,$33,$3F,$00
  defb $00,$33,$33,$33,$3F,$03,$03,$00
  defb $00,$3F,$30,$3F,$03,$03,$3F,$00
  defb $00,$3F,$30,$3F,$33,$33,$3F,$00
  defb $00,$3F,$03,$03,$3F,$30,$30,$00
  defb $00,$3F,$33,$0C,$33,$33,$3F,$00
  defb $00,$3F,$33,$33,$3F,$03,$3F,$00
  defb $00,$00,$00,$0C,$00,$00,$0C,$00

end asm
charmap01end:
