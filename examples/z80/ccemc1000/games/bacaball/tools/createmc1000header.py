#!/usr/bin/python
# -*- coding: latin-1 -*-
# createmc1000header.py - 201208161346 - Paulo Silva (GPL licence)
# creates concatenation header and tail for mc1000 .bin base files used on mess emulator

import os,sys

finp_st=sys.argv[1]
finpln=os.path.getsize(finp_st)

fout_st="mc1000header.bin"
fout_fl=open(fout_st,"w")

fout_fl.write(chr(0x0D))
fout_fl.write(chr(0x00));fout_fl.write(chr(0x01))

finpln2=finpln+0x0200
finplnlm=finpln2 % 256;finpln2=int(finpln2/256)
finplnll=finpln2 % 256
fout_fl.write(chr(finplnlm))
fout_fl.write(chr(finplnll))

for i in range(0,6,1):
  fout_fl.write(chr(0xFF))
fout_fl.write(chr(0x00))
for i in range(0,249,1):
  fout_fl.write(chr(0xFF))
fout_fl.close()

fout_st="mc1000tail.bin"
fout_fl=open(fout_st,"w")
fout_fl.write(chr(0xFF))
fout_fl.close()

