#include "library/mc1000screen.bas"
#include "library/mc1000cls.bas"
#include "library/mc1000vpokechr.bas"
#include "library/mc1000rnd.bas"
#include "library/bacaball_charmap.bas"
#include "library/minivadersldir32b.bas"
#include "library/aquariusfillram.bas"
#include "library/mc1000keymapinv.bas"
#include "library/mc1000keymap.bas"
'#include "library/mc1000delayb.bas"

mc1000screen(152)
mc1000cls(2)

dim p1,p2,s1,s2,vl as integer
dim seed,rseed as uinteger
dim ee0,ee1,ee2,ee3,ee4,ee5,eee,eex,eey as uinteger
dim eb0 as ubyte

rseed=50:seed=50

sub mc1000writetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tloc2 as uinteger)
  for tq2=0 to tle2-1
    mc1000vpokechr((ty2*32)+tx2+tq2,peek(ttx2+tq2),tloc2)
    next
  end sub



'do:loop

'-----------------------------------------------------------------------

'- Bacaball - nov'11 - Paulo Silva (copyleft, gpl3) - rev 20111113200239
'- for the csscgc2011 competition, hosted by mojon twins

debug=1

'-----------------------------------------------
startup:
dim sq1,sq2,sq3,sq4 as uinteger
dim x1,y1,x2,y2,x3,y3,x4,y4 as float
dim xbp,ybp,xbd,ybd,spb as float
dim m1,m2,m3,m4 as ubyte
dim sc1,sc2,sc3,sc4,bown,govr,glim,gst as byte
dim kbdo,kbdf,kbds,kbdl as ubyte

if debug<>0 then:
mc1000vpokechr(25,34,@charmap01-136)
for eey=0 to 15
  for eex=0 to 15
     mc1000vpokechr(7*32+15+eey*32+eex,eey*16+eex,@charmap01-136)
     next:next
'for tq2=0 to 255:next
'mc1000delayb(60000)
end if

do
gosub title
gosub playstart
gosub playloop
loop

'-----------------------------------------------
title:
aquariusfillram($8000,2*$55,6144)   'bright 1:flash 0: inverse 0:border 0:ink 4:paper 3:cls
aquariusfillram($8000,0*$55,256*6)  'paper 4:for y=0 to 5:for x=0 to 31 step 4:print at y,x;chr$(32)+chr$(32)+chr$(32)+chr$(32):next:next

mc1000writetext(3,14,@text04,2,@charmap01-136+256):mc1000writetext(3,15,@text04+2,2,@charmap01-136+256)    'paper 3:ink 2:print at 14,3;"\K\M":print at 15,3;"\P\L"
mc1000writetext(19,13,@text04,2,@charmap01-136+256):mc1000writetext(19,14,@text04+2,2,@charmap01-136+256)  'paper 3:ink 2:print at 13,19;"\K\M":print at 14,19;"\P\L"
mc1000writetext(27,15,@text04,2,@charmap01-136+256):mc1000writetext(27,16,@text04+2,2,@charmap01-136+256)  'paper 3:ink 2:print at 15,27;"\K\M":print at 16,27;"\P\L"
mc1000writetext(21,21,@text04,2,@charmap01-136+256):mc1000writetext(21,22,@text04+2,2,@charmap01-136+256)  'paper 3:ink 2;print at 21,21;"\K\M":print at 22,21;"\P\L"

mc1000writetext(2,18,@text01,@text02-@text01,@charmap01-136)  'ink 4:print at 18,2;"BACABALL"
mc1000writetext(2,19,@text02,@text03-@text02,@charmap01-136)  'ink 4:print at 19,2;chr$(127);" PAULO SILVA, '11"
mc1000writetext(2,20,@text03,@text04-@text03,@charmap01-136)  'ink 4:print at 20,2;"PUSH ANY KEY"
'KMPL

'pause 0
waitkey01:
eb0=mc1000keymapinv(1) bor mc1000keymapinv(2)
eb0=(mc1000keymapinv(4) bor mc1000keymapinv(8)) bor eb0
eb0=mc1000keymapinv(16) bor mc1000keymapinv(32) bor eb0
eb0=(mc1000keymapinv(64) bor mc1000keymapinv(128)) bor eb0
if eb0=0 then:goto waitkey01:end if

'do:loop

return

'-----------------------------------------------
playstart:

spb=6:glim=5:gst=0
x1=4:y1=11:x2=15:y2=20:x3=15:y3=2:x4=26:y4=11
m1=0:m2=0:m3=1:m4=1:xbp=16:ybp=12
sc1=gst:sc2=gst:sc3=gst:sc4=gst:bown=0:govr=0

'xbd=((rnd*spb*2)-spb)/32
seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
'ybd=((rnd*spb*2)-spb)/32
seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32

'border 0:bright 1:paper 3:ink 0:flash 0:cls

'bright 1:paper 4:ink 5
'for y=0 to 23:for x=0 to 31 step 4:print at y,x;chr$(157)+chr$(157)+chr$(157)+chr$(157):next:next
aquariusfillram($8000,2*$55,6144)

drawsprites()
'inverse 0:ink 0:bright 1

return

'-----------------------------------------------
playloop:
lp01:
'  pause 1
'
'  if debug<>0 then: '- debug: pressed keys
'    ink 3:paper 4:bright 1:print at 23,0;sq1;",";sq2;",";sq3;",";sq4;
'    bright 1:paper 4:ink 5:print chr$(157)+chr$(157)+chr$(157)+chr$(157):
'    end if

if debug<>0 then
''eee=0
''do
mc1000vpokechr((10*32)+5,48+(int(eee/100) mod 10),@charmap01-136)
mc1000vpokechr((10*32)+6,48+(int(eee/10) mod 10),@charmap01-136)
mc1000vpokechr((10*32)+7,48+(eee mod 10),@charmap01-136)
eee=eee+1
ee5=1
for ee4=0 to 7
eb0=mc1000keymapinv(ee5)
mc1000vpokechr(((ee4+1)*32)+5,48+(int(eb0/100) mod 10),@charmap01-136)
mc1000vpokechr(((ee4+1)*32)+6,48+(int(eb0/10) mod 10),@charmap01-136)
mc1000vpokechr(((ee4+1)*32)+7,48+(eb0 mod 10),@charmap01-136)
ee5=ee5*2
next
eb0=mc1000keymapinv(1) bor mc1000keymapinv(2)
eb0=(mc1000keymapinv(4) bor mc1000keymapinv(8)) bor eb0
eb0=mc1000keymapinv(16) bor mc1000keymapinv(32) bor eb0
eb0=(mc1000keymapinv(64) bor mc1000keymapinv(128)) bor eb0
mc1000vpokechr((12*32)+5,48+(int(eb0/100) mod 10),@charmap01-136)
mc1000vpokechr((12*32)+6,48+(int(eb0/10) mod 10),@charmap01-136)
mc1000vpokechr((12*32)+7,48+(eb0 mod 10),@charmap01-136)
'loop
end if

'- score???
'  bright 1:paper 4
'  ink 3:print at 0,0;"1:";
'  ink 2:print sc1;
'  ink 5:print chr$(157);
'  ink 3:print "2:";
'  ink 2:print sc2;
'  ink 5:print chr$(157);
'  ink 3:print "3:";
'  ink 2:print sc3;
'  ink 5:print chr$(157);
'  ink 3:print "4:";
'  ink 2:print sc4;
'  ink 5:print chr$(157)+chr$(157)+chr$(157)+chr$(157)
'
'  bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
  xbp=xbp+xbd:ybp=ybp+ybd

'  if debug=0 then:bright 1:paper 4:ink 7:print at ybp,xbp;chr$(158):end if
'  if debug<>0 then:bright 1:paper 4:ink 7:print at ybp,xbp;chr$(128+16+16+bown):end if

'  sq1=((255-(in 64510)) band 2)/2 bor ((255-(in 65022)) band 2) bor ((255-(in 65022)) band 1)*4 bor ((255-(in 65022)) band 4)*2 :'- wsad
   sq1=(mc1000keymapinv(128) band 4)/4 bor (mc1000keymapinv(8) band 4)/2 bor (mc1000keymapinv(2) band 1)*4 bor (mc1000keymapinv(16) band 1)*8 :'- wsad

'  sq2=((255-(in 64510)) band 16)/16 bor ((255-(in 65022)) band 16)/8 bor ((255-(in 65022)) band 8)/2 bor ((255-(in 49150)) band 16)/2 :'- tgfh
   sq2=(mc1000keymapinv(16) band 4)/4 bor (mc1000keymapinv(128) band 1)*2 bor (mc1000keymapinv(64) band 1)*4 bor (mc1000keymapinv(1) band 2)*4

'  sq3=((255-(in 57342)) band 4)/4 bor ((255-(in 49150)) band 4)/2 bor ((255-(in 49150)) band 8)/2 bor ((255-(in 49150)) band 2)*4 :'- ikjl
   sq3=(mc1000keymapinv(2) band 2)/2 bor (mc1000keymapinv(8) band 2)/1 bor (mc1000keymapinv(4) band 2)*2 bor (mc1000keymapinv(16) band 2)*4

'  sq4=((255-(in 61438)) band 8)/8 bor ((255-(in 61438)) band 16)/8 bor ((255-(in 63486)) band 16)/4 bor ((255-(in 61438)) band 4)*2 :'-7658
'  sq4=(mc1000keymapinv(128) band 4)/4 bor (mc1000keymapinv(8) band 4)/2 bor (mc1000keymapinv(2) band 1)*4 bor (mc1000keymapinv(16) band 1)*8
   sq4=0

'7:4,3:4,1:1,4:1 - wsad
'4:4,7:1,6:1,0:2 - tgfh
'1:2,3:2,2:2,4:2 - ikjl
'??????????????????????

  if sq1<>0 then
'    bright 1:paper 4:ink 5:print at y1,x1;chr$(157)+chr$(157):print at y1+1,x1;chr$(157)+chr$(157)
    if ((sq1 band 4) <>0) and x1>0 then:x1=x1-.25:m1=1:end if
    if ((sq1 band 8) <>0) and x1<30 then:x1=x1+.25:m1=0:end if
    if ((sq1 band 1)<>0) and y1>0 then:y1=y1-.25:end if
    if ((sq1 band 2) <>0) and y1<22 then:y1=y1+.25:end if
    drawsprites()
    end if

  if sq2<>0 then
'    bright 1:paper 4:ink 5:print at y2,x2;chr$(157)+chr$(157):print at y2+1,x2;chr$(157)+chr$(157)
    if ((sq2 band 4) <>0) and x2>0 then:x2=x2-.25:m2=1:end if
    if ((sq2 band 8) <>0) and x2<30 then:x2=x2+.25:m2=0:end if
    if ((sq2 band 1)<>0) and y2>0 then:y2=y2-.25:end if
    if ((sq2 band 2) <>0) and y2<22 then:y2=y2+.25:end if
    drawsprites()
    end if

  if sq3<>0 then
'    bright 1:paper 4:ink 5:print at y3,x3;chr$(157)+chr$(157):print at y3+1,x3;chr$(157)+chr$(157)
    if ((sq3 band 4) <>0) and x3>0 then:x3=x3-.25:m3=1:end if
    if ((sq3 band 8) <>0) and x3<30 then:x3=x3+.25:m3=0:end if
    if ((sq3 band 1)<>0) and y3>0 then:y3=y3-.25:end if
    if ((sq3 band 2) <>0) and y3<22 then:y3=y3+.25:end if
    drawsprites()
    end if

  if sq4<>0 then
'    bright 1:paper 4:ink 5:print at y4,x4;chr$(157)+chr$(157):print at y4+1,x4;chr$(157)+chr$(157)
    if ((sq4 band 4) <>0) and x4>0 then:x4=x4-.25:m4=1:end if
    if ((sq4 band 8) <>0) and x4<30 then:x4=x4+.25:m4=0:end if
    if ((sq4 band 1)<>0) and y4>0 then:y4=y4-.25:end if
    if ((sq4 band 2) <>0) and y4<22 then:y4=y4+.25:end if
    drawsprites()
    end if

  if bown<>1 then:
  xt=int(x1)-int(xbp):yt=int(y1)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
'     xbd=(rnd*spb)/32
      seed=mc1000rnd(seed):xbd=(seed mod spb)/32
'     ybd=((rnd*spb*2)-spb)/32
      seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
      bown=1
      end if:end if:end if

  if bown<>4 then:
  xt=int(x4)-int(xbp):yt=int(y4)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
'     xbd=((rnd*spb)/32)*-1
      seed=mc1000rnd(seed):xbd=((seed mod spb)/32)*-1
'     ybd=((rnd*spb*2)-spb)/32
      seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
      bown=4
      end if:end if:end if

  if bown<>2 then:
  xt=int(x2)-int(xbp):yt=int(y2)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
'     xbd=((rnd*spb*2)-spb)/32
      seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
'     ybd=((rnd*spb)/32)*-1
      seed=mc1000rnd(seed):ybd=((seed mod spb)/32)*-1
      bown=2
      end if:end if:end if

  if bown<>3 then:
  xt=int(x3)-int(xbp):yt=int(y3)-int(ybp)
  if xt=0 or xt=1 then:
    if yt=0 or yt=1 then:
'     xbd=((rnd*spb*2)-spb)/32
      seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
'     ybd=(rnd*spb)/32
      seed=mc1000rnd(seed):ybd=(seed mod spb)/32
      bown=3
      end if:end if:end if

  if int(xbp)<0 then:
'    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    mc1000vpokechr(ybp*32+xbp,157,@charmap01-136)
    sc1=sc1-1
    if bown=2 then:sc2=sc2+1:end if
    if bown=3 then:sc3=sc3+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12
'   xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
    seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
    end if

  if int(ybp)>23 then:
'    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    mc1000vpokechr(ybp*32+xbp,157,@charmap01-136)
    sc2=sc2-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=3 then:sc3=sc3+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12
'   xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
    seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
    end if

  if int(ybp)<0 then:
'    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    mc1000vpokechr(ybp*32+xbp,157,@charmap01-136)
    sc3=sc3-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=2 then:sc2=sc2+1:end if
    if bown=4 then:sc4=sc4+1:end if
    bown=0:xbp=16:ybp=12
'   xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
    seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
    end if

  if int(xbp)>31 then:
'    bright 1:paper 4:ink 5:print at ybp,xbp;chr$(157)
    mc1000vpokechr(ybp*32+xbp,157,@charmap01-136)
    sc4=sc4-1
    if bown=1 then:sc1=sc1+1:end if
    if bown=2 then:sc2=sc2+1:end if
    if bown=3 then:sc3=sc3+1:end if
    bown=0:xbp=16:ybp=12
'   xbd=((rnd*spb*2)-spb)/32:ybd=((rnd*spb*2)-spb)/32
    seed=mc1000rnd(seed):xbd=(((seed mod spb)*2)-spb)/32
    seed=mc1000rnd(seed):ybd=(((seed mod spb)*2)-spb)/32
    end if

  if sc1<0 then:sc1=0:end if
  if sc2<0 then:sc2=0:end if
  if sc3<0 then:sc3=0:end if
  if sc4<0 then:sc4=0:end if

  if sc1>=glim then:govr=1:end if
  if sc2>=glim then:govr=1:end if
  if sc3>=glim then:govr=1:end if
  if sc4>=glim then:govr=1:end if

  if govr=0 then goto lp01:end if

  wnr=0
  if sc1>=glim then:wnr=1:end if
  if sc2>=glim then:wnr=2:end if
  if sc3>=glim then:wnr=3:end if
  if sc4>=glim then:wnr=4:end if

'  print at 11,10;"PLAYER ";wnr;" WINS"
' MIJAKMEL
' MIJBKMFL
' MIJCKMGL
' MIJDKMHL
'  pause 500

return

'-----------------------------------------------
sub drawsprites():
'  bright 1:paper 4:ink 6:
  if m1=0 then:
'   print at y1,x1;"\M\I":print at y1+1,x1;"\J\A":else:print at y1,x1;"\K\M":print at y1+1,x1;"\E\L"
    mc1000writetext(x1,y1  ,@text06  ,2,@charmap01-136+256)
    mc1000writetext(x1,y1+1,@text06+2,2,@charmap01-136+256)
  else
    mc1000writetext(x1,y1  ,@text06+4,2,@charmap01-136+256)
    mc1000writetext(x1,y1+1,@text06+6,2,@charmap01-136+256)
    end if
  if m2=0 then:
'   print at y2,x2;"\M\I":print at y2+1,x2;"\J\B":else:print at y2,x2;"\K\M":print at y2+1,x2;"\F\L":
    end if
  if m3=0 then:
'   print at y3,x3;"\M\I":print at y3+1,x3;"\J\C":else:print at y3,x3;"\K\M":print at y3+1,x3;"\G\L"
    end if
  if m4=0 then:
'   print at y4,x4;"\M\I":print at y4+1,x4;"\J\D":else:print at y4,x4;"\K\M":print at y4+1,x4;"\H\L"
    end if
  end sub

do:loop

text01:
asm
defb "BACABALL"
end asm
text02:
asm
defb " PAULO SILVA, '11"
end asm
text03:
asm
defb "PUSH ANY KEY"
end asm
text04:
asm
defb "LMNO"
defb "KMPL"
end asm
text05:
asm
defb "PLAYERWINS"
end asm
text06:
asm
defb "MIJAKMEL"
end asm
text07:
asm
defb "MIJBKMFL"
end asm
text08:
asm
defb "MIJCKMGL"
end asm
text09:
asm
defb "MIJDKMHL"
end asm
text10:

do:loop

''rseed=mc1000rnd(rseed) band 255
''p1=int(rseed*3)
''rseed=mc1000rnd(rseed) band 255
''s1=int(rseed/8)+8

''rseed=mc1000rnd(rseed) band 255
''p2=int(rseed*3)
''rseed=mc1000rnd(rseed) band 255
''s2=int(rseed/8)+8

''lp02:
''  rseed=mc1000rnd(rseed)
''  vl= (128+int(rseed/1.6))band 255
''
''  mc1000vpokechr(p1,vl,@charmap01)
''  p1=(p1+1)mod 768
''  s1=s1-1
''  if s1<1 then:
''    rseed=mc1000rnd(rseed) band 255
''    p1=int(rseed*3)
''    rseed=mc1000rnd(rseed) band 255
''    s1=int(rseed/8)+8
''    end if
''  mc1000vpokechr(p2,32,@charmap01)
''  p2=(p2+1)mod 768
''  s2=s2-1
''  if s2<1 then:
''    rseed=mc1000rnd(rseed) band 255
''    p2=int(rseed*3)
''    rseed=mc1000rnd(rseed) band 255
''    s2=int(rseed/8)+8
''    end if
''  goto lp02
