rm bacaball.bin bacaball.asm mc1000header.bin mc1000tail.bin
zxb.py bacaball.bas --asm --org=$((0x0200))
zxb.py bacaball.bas --org=$((0x0200))
createmc1000header.py bacaball.bin
cat mc1000header.bin > bacaball.final.bin
cat bacaball.bin >> bacaball.final.bin
cat mc1000tail.bin >> bacaball.final.bin
mv bacaball.final.bin bacaball.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py bacaball.bin
zip bacaball.bin.wav.zip bacaball.bin.wav
rm bacaball.bin.wav


mame mc1000 -video soft -w -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass bacaball.bin.wav.zip

#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass bacaball.bin.wav.zip

# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]


#mc1000.ic12
#SHA1: fd766f5ea4481ef7fd4df92cf7d8397cc2b5a6c4
#CRC32: 750c95f0
#mc1000.ic17
#SHA1: 9480270e67a5db2e7de8bc5c8b9e0bb210d4142b
#CRC32: 8e78d80d

#fd766f5ea4481ef7fd4df92cf7d8397cc2b5a6c4  mc1000.ic12
#9480270e67a5db2e7de8bc5c8b9e0bb210d4142b  mc1000.ic17
