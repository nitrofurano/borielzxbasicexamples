#include "library/mc1000screen.bas"
#include "library/mc1000cls.bas"
#include "library/mc1000vpokechr.bas"
#include "library/mc1000rnd.bas"
#include "library/memorama_charmap.bas"
#include "library/minivadersldir32b.bas"
#include "library/aquariusfillram.bas"

'- bugs:
'-   putchars - testar

dim tq2 as uinteger
sub mc1000writetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tloc2 as uinteger)
  for tq2=0 to tle2-1
    mc1000vpokechr((ty2*32)+tx2+tq2,peek(ttx2+tq2),tloc2)
    next
  end sub

dim tqx3,tqy3 as uinteger
sub putchars(tx3 as uinteger,ty3 as uinteger,txs as uinteger,tys as uinteger,tad as uinteger,tloc3 as uinteger)
  for tqy3=0 to tys-1
    for tqx3=0 to txs-1
      mc1000vpokechr( ((tqy3+ty3)*32)+tq3+tqx3+tx3,peek(tad+(tqy3*txs)+tqx3),tloc3)
      next:next
  end sub

mc1000screen(154)
mc1000cls(0)

dim cheat,je as ubyte
dim p1,p2,s1,s2,vl as integer
dim rseed as uinteger
rseed=50
dim ee0,ee1,ee2,ee3,eex,eey as uinteger

mc1000vpokechr(7,34,@charmap01-216)

for eey=0 to 15
  for eex=0 to 15
    mc1000vpokechr((7*32+15)+eey*32+eex,eey*16+eex,@charmap01-216)
    next:next

''do:loop

'-----------------------------------------------------------------------

''rseed=mc1000rnd(rseed) band 255
''p1=int(rseed*3)
''rseed=mc1000rnd(rseed) band 255
''s1=int(rseed/8)+8
''rseed=mc1000rnd(rseed) band 255
''p2=int(rseed*3)
''rseed=mc1000rnd(rseed) band 255
''s2=int(rseed/8)+8

''do
''  rseed=mc1000rnd(rseed)
''  vl= (128+int(rseed/1.6))band 255
''  mc1000vpokechr(p1,vl,@charmap01)
''  p1=(p1+1)mod 768
''  s1=s1-1
''  if s1<1 then:
''    rseed=mc1000rnd(rseed) band 255
''    p1=int(rseed*3)
''    rseed=mc1000rnd(rseed) band 255
''    s1=int(rseed/8)+8
''    end if
''  mc1000vpokechr(p2,32,@charmap01)
''  p2=(p2+1)mod 768
''  s2=s2-1
''  if s2<1 then:
''    rseed=mc1000rnd(rseed) band 255
''    p2=int(rseed*3)
''    rseed=mc1000rnd(rseed) band 255
''    s2=int(rseed/8)+8
''    end if
''  loop

'#include "library/putchar.bas"
''- #include <putchar.bas>
'
''- memorama - jan'06 - oct'11 - paulo silva - rev 20111009151725
''- for the csscgc2011 competition, hosted by mojon twins
''- based on msx version submitted to the msxdev karoshi's msx-basic competition at winter 2006
'
'poke uInteger 23606,@typeface-256:poke uInteger 23675,@patterns2
'
''>> game system startup <<
gamesystemstartup:
1000:
'dim h$(10)
'h$(0)="JUSPION":h$(1)="ANRIL":h$(2)="SATANGOSS":h$(3)="DAILEON":h$(4)="MACGAREN"
'h$(5)="MIYA":h$(6)="EDIN":h$(7)="CHIBA":h$(8)="JIBAN":h$(9)="BOOMERMAN"

dim t(10) as uinteger
dim u$(10)
dim g(4,4) as uinteger
dim f(4,4) as uinteger
dim i,x,y,ka as uinteger

cheat=0

'paper 7: ink 0: border 0: bright 1: cls
'aquariusfillram($8000,$00,6*1024)


'let p$=""
'for i=128 to 254
'  let p$=p$+chr$(i)
'  next
'let p$=p$+chr$(252)
'let xe$="ADFELMHINBGJKCOCKJGBNIHMLEFDAAAA"
'
'for i=0 to 9
'  t(i)=1800+i*1800
'  next
'
''- ??????
'for i=0 to 9
'  let u$(i)=chr$(int(t(i)/36000)mod 6+48)+chr$(int(t(i)/3600)mod 10+48)+chr$(39)
'  let u$(i)=u$(i)+chr$(int(t(i)/600)mod 6+48)+chr$(int(t(i)/60)mod 10+48)+chr$(34)
'  next

' >> game title <<
title:
2000:
'cls:poke uinteger 23672,0
mc1000writetext(3,12,@text01,@text02-@text01,@charmap01-216)  'print at 12,3;"MEMORAMA"
mc1000writetext(3,13,@text02,@text03-@text02,@charmap01-216)  'print at 13,3;"BY PAULO SILVA, '06, '11"
mc1000writetext(3,20,@text03,@text04-@text03,@charmap01-216)  'print at 20,3;"PUSH SPACE KEY"





putchars(2,2,4,4,@title,@charmap01-216)

putchars(3,3,4,4,@patterns9,@charmap01-216)


''for i=0 to 7
''  putchars(i*4,15,4,4,@patterns9+(i*16),@charmap01-216)
''  'paintData(i*4,15,4,4,@attributes+(i*16))
''  next


do:loop

2100:
'if inkey$=" "then goto 2200:end if
'if peek(uinteger,23672)<600 then goto 2100:end if
'goto 3000

2200:
'poke uinteger 23672,0
'for j=1 to 5
'  print at 20,3;"              ":pause 10
'  print at 20,3;"PUSH SPACE KEY":pause 10
'  next
'pause 150:randomize
'goto 4000

' >> score table <<
scoretable:
3000:
'cls
'poke uinteger 23672,0
'print at 9,4;"HISCORES:"

3020:
'let i=int(peek(uinteger,23672)/15)-1
'if i>=0 and i<10 then print at i+11,4;h$(i):print at i+11,18;u$(i):end if

'if inkey$<>"" or (peek(uinteger,23672))>=600 then goto 2000:end if
'goto 3020

' >> game play <<
gameplay:
4000:
'cls:poke uinteger 23672,0
'let px=3:let py=2
'let x1=3:let y1=2
'let x2=3:let y2=2
'let sl=0:let pa=8
'let je=1:let se=0

'- shuffling cards --
shufflingcards:
4050:
'print at 12,7; "SHUFFLING CARDS..."
'for x=0 to 3:for y=0 to 3:g(x,y)=0
'  let f(x,y)=0
'  next:next
'let ct=16

4054:
'for i=1 to 8:for j=1 to 2
4056:
'  let x=int((int(rnd*39))/10)
'  let y=int((int(rnd*39))/10)
'  if g(x,y)<>0 then goto 4056:end if
'  let g(x,y)=i
'  print at 14,7;"(";str$(16-ct);"/16) "
'  let ct=ct-1
'  next:next
'print at 12,7;"                  "

'-- displaying back of the cards --
displaybackcards:
4100:
'cls
'for y=0 to 3:for x=0 to 3:for i=0 to 3
'  print at y*5+i+3,x*5+2;ink 4;paper 6;bright 1;"\E\E\E\E"
'  next i
'  if cheat<>0 then: print at y*5+4+3,x*5+2;chr$(48+g(x,y));:end if
'  next:next

'- displaying some text and starting timer -
displaytext:
starttimer:
4200:
'print at 4,24;"TIME"
'print at 7,24;"PAIRS"
'poke uinteger 23672,0
'let tt=peek(uinteger,23672)

'- main game loop -
maingameloop:
4400:
''putsprite 0,((px*40)+38,(py*40)+42),1,0
'print at py*5+6,px*5+5;"\A\C":print at py*5+7,px*5+5;"\B\D"
'
'sq=((255-(in 64510)) band 2)/2 bor ((255-(in 65022)) band 2) bor ((255-(in 65022)) band 1)*4 bor ((255-(in 65022)) band 4)*2
'sq=sq bor ((255-(in 61438)) band 8)/8 bor ((255-(in 61438)) band 16)/8 bor ((255-(in 63486)) band 16)/4 bor ((255-(in 61438)) band 4)*2
'
'let qq=tt
'let tt=peek(uinteger,23672)
'if (int(qq/60))<>(int(tt/60)) then gosub 4900: end if
'if sq<>0 and je=1 then gosub 4500: end if
'if sq=0 then je=1:end if
'
''- obter substituto de strig, por i/o
''if strig(0)=-1 and se=0 then let se=1:gosub 4600:end if
''if strig(0)=0 then se=0:end if
'if inkey$=" " and se=0 then let se=1:gosub 4600:end if
'if inkey$<>" " then se=0:end if
'
'if inkey$=chr$(27) or tt>48000 then let hs=tt:let cn=0:gosub 4800:goto 5000:end if
'if pa<1 then hs=tt:cn=1:gosub 4800:goto 5000:end if
'goto 4400

' -- cursor motion --
cursormotion:
4500:
let je=0
'
'if f(px,py)=0 then
'  'print at py*5+5,px*5+4;"AA":print at py*5+6,px*5+4;"AA"
'  for i=1 to 3:print at py*5+i+3,px*5+2;ink 4;paper 6;bright 1;"\E\E\E\E":next i
'  end if
'if f(px,py)=1 then
'  'print at py*5+5,px*5+4;"BB":print at py*5+6,px*5+4;"BB"
'  putChars(px*5+2,py*5+3,4,4,@patterns+((g(px,py)-1)*128)):paintData(px*5+2,py*5+3,4,4,@attributes+((g(px,py)-1)*16))
'  end if
'if f(px,py)>1 then
'  for i=1 to 3:print at py*5+i+3,px*5+2;"    ":next i
'  end if
'
'  print at py*5+6,px*5+6;" "
'  print at py*5+7,px*5+5;"  "
'
'let px=px+(sq=4)-(sq=8)
'let py=py+(sq=1)-(sq=2)
'if px>3 then let px=0:end if
'if py>3 then let py=0:end if
'if px<0 then let px=3:end if
'if py<0 then let py=3:end if
'return

' - card checking -
cardchecking:
4600:
'let ck=0
'if ck=0 and sl=0 and f(px,py)=0 then:
'  let ck=1:let f(px,py)=1
'  let x1=px:let y1=py
'  let dx=px:let dy=py:gosub 4850
'  let sl=1
'  end if
'
'if ck=0 and sl=1 and f(px,py)=0 and (py*4+px)<>(y1*4+x1) then:
'  let ck=1:let f(px,py)=1
'  let x2=px:let y2=py
'  let dx=px:let dy=py:gosub 4850:gosub 4700
'  let sl=0
'  end if
'return

' - card parity checking -
cardparitychecking:
4700:
'if g(x2,y2)<>g(x1,y1) then:
'  gosub 4740
'else:
'  gosub 4720
'  end if
'return

' - removing cards -
removingcards:
4720:
'pause 120
'let f(x1,y1)=2:let f(x2,y2)=2:let pa=pa-1:let dx=x1:let dy=y1:gosub 4870
'let dx=x2:let dy=y2:gosub 4870:gosub 4900
'return

' - hiding cards -
hidingcards:
4740:
'pause 120
'let f(x1,y1)=0:let f(x2,y2)=0:let dx=x1:let dy=y1:gosub 4860
'let dx=x2:let dy=y2:gosub 4860
'return

' - displaying cards --
displayingcards:
4800:
'for y=0 to 3:for x=0 to 3
'  putChars(x*5+2,y*5+3,4,4,@patterns+((g(x,y)-1)*128)):paintData(x*5+2,y*5+3,4,4,@attributes+((g(x,y)-1)*16))
'  next x:next y
'return

' - display mean card - (input:dx, dy - using p$ and g(,) )
displaymeancard:
4850:
'putChars(dx*5+2,dy*5+3,4,4,@patterns+((g(dx,dy)-1)*128)):paintData(dx*5+2,dy*5+3,4,4,@attributes+((g(dx,dy)-1)*16))
'
'return

' - display back of card - (input:dx, dy)
displaybackofcard:
4860:
'for i=0 TO 3
'  print at dy*5+i+3,dx*5+2;ink 4;paper 6;bright 1;"\E\E\E\E"
'  next i
'  return

'- display card remotion - (input:dx, dy)
displaycardmotion:
4870:
'for i=0 to 3:print at dy*5+i+3,dx*5+2;"    ";:next i:return

' - display timer and pairs rest -
displaytimerpairs:
4900:
'let qq=tt:let tt=peek(uinteger,23672)
'let k$=chr$(int(tt/36000)mod 6+48)+chr$(int(tt/3600)mod 10+48)+chr$(39)
'let k$=k$+chr$(int(tt/600)mod 6+48)+chr$(int(tt/60)mod 10+48)+chr$(34)
'print at 5,24;k$
'print at 8,24;str$(pa)
'return

' >> score writing <<
scorewriting:
5000:
'cls:poke uinteger 23672,0:let l$=""
'if cn=0 then goto 3000:end if
'print at 9,4; "YOUR NAME:"

' - text input loop -  <--- it still has a bug - fix needed
textinputloop:
5100:
'let k$=inkey$
'let kl=len(k$)
'let ka=code(k$)
'rem if kl>0 then print at 10,4;hex$(ka);"   "
'if ka>31 and ka<96 and len(l$)<10 then let l$=l$+k$:end if
'if ka>95 and ka<127 and len(l$)<10 then let l$=l$+chr$(code(k$)-32):end if
'
'if (ka=8 or ka=29) and len(l$)>0 then:
'  let l$=l$(to len(l$)-1)
'  end if
'
'if ka=13 then goto 5200:end if
'print at 11,4; l$;"   "
'print at 20,4;"TIME:";29-int((peek(uinteger,23672))/60);"  "
''putsprite 0,(32+len(l$)*8,12*8),1,1 <-----
'print at 12,len(l$);"\A\C":print at 13,len(l$);"\B\D"
'
'if (peek(uinteger,23672))<1790 then goto 5100:end if

' - finding hiscore hole for current score -
findinghiscorehole:
5200:
'let iq=10:for i=9 to 0 step -1
'if tt<t(i) then let iq=iq-1:end if
'next
'if iq>9 then goto 3000:end if

' - moving worse scores down -
5300:
'for i=8 to iq step -1
'  let h$(i+1)=h$(i)
'  let u$(i+1)=u$(i)
'  let t(i+1)=t(i)
'  next
'let h$(iq)=l$: let t(iq)=tt
'let u$(iq)=chr$(int(t(iq)/36000)mod 6+48)+chr$(int(t(iq)/3600)mod 10+48)+chr$(39)
'let u$(iq)=u$(iq)+chr$(int(t(iq)/600)mod 6+48)+chr$(int(t(iq)/60)mod 10+48)+chr$(34)
'goto 3000


do:loop

patterns:
''asm
''  defb 000h,000h,000h,000h,000h,000h,007h,03Fh,07Fh,0F8h,0F0h,0F8h,0FFh,0FFh,0FFh,03Fh,00Fh,000h,000h,000h,000h,000h,01Fh,03Fh,07Eh,0F0h,0E0h,0E1h,0E3h,0FFh,0FFh,03Fh
''  defb 000h,001h,001h,001h,003h,003h,083h,0F3h,0FBh,07Fh,03Fh,01Fh,0FFh,0FCh,0FCh,0FCh,00Fh,003h,003h,001h,003h,01Fh,0FFh,0FFh,07Fh,0F3h,0E3h,0E3h,0C3h,083h,081h,000h
''  defb 0F8h,0FEh,0FFh,0CFh,0C7h,0C7h,0C7h,0C7h,0CEh,0FCh,0F8h,0E0h,0E0h,070h,07Fh,03Fh,07Fh,0FEh,0F7h,0E7h,0E3h,0F1h,0F9h,0FCh,09Eh,08Fh,087h,08Fh,09Fh,0FFh,0FFh,0FCh
''  defb 000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,0C0h,0F8h,0FCh,01Eh,00Fh,007h,087h,0EFh,0FFh,0FFh,0FEh,000h,000h,000h,000h,000h,000h,000h,000h
''  defb 000h,000h,000h,000h,000h,001h,003h,007h,007h,00Fh,01Fh,03Fh,07Fh,03Fh,03Fh,007h,000h,001h,003h,007h,00Fh,01Eh,03Eh,01Fh,00Fh,007h,007h,003h,001h,000h,000h,000h
''  defb 000h,000h,000h,0C0h,0E1h,0F1h,0F3h,0FBh,03Fh,01Fh,00Fh,006h,000h,0C0h,0C0h,0F0h,0F0h,0C0h,084h,00Ch,01Eh,03Eh,07Eh,076h,0E7h,0E7h,0C7h,0C7h,087h,007h,007h,007h
''  defb 078h,07Fh,0FFh,0FFh,0E7h,0C1h,0C3h,087h,087h,00Fh,00Fh,01Fh,03Fh,03Fh,03Fh,07Fh,000h,000h,010h,03Eh,03Fh,01Fh,01Ch,00Eh,00Eh,007h,007h,007h,003h,03Fh,0FFh,0E0h
''  defb 000h,000h,0E0h,0E0h,0E0h,0C0h,0C0h,080h,000h,03Ch,0FCh,0FCh,0FCh,0FEh,0FEh,08Eh,00Eh,00Eh,006h,006h,0C7h,0FFh,0FFh,00Fh,000h,000h,000h,000h,080h,080h,080h,000h
''  defb 000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,0FFh,0FFh,03Fh,01Fh,007h,003h,001h,000h,000h,001h,001h,001h,001h,001h,003h,003h,003h,000h
''  defb 001h,001h,003h,003h,007h,007h,007h,00Fh,01Fh,03Fh,07Fh,0F8h,0F8h,0F8h,0F0h,0F0h,000h,000h,000h,080h,080h,08Ch,09Eh,0BFh,0FFh,0F3h,0E1h,0C0h,080h,000h,000h,000h
''  defb 000h,000h,080h,080h,080h,080h,080h,080h,080h,080h,0C0h,0C0h,0FFh,0FFh,000h,001h,00Fh,03Fh,0FCh,0F0h,0F0h,0F0h,0F8h,0F8h,0F8h,0F8h,0F8h,0F8h,078h,078h,038h,010h
''  defb 000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,0FFh,0FCh,0F8h,0F0h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h
''  defb 000h,000h,000h,006h,006h,00Eh,00Eh,01Ch,01Fh,03Fh,03Fh,011h,000h,000h,000h,0E0h,0E0h,0E0h,0FFh,0FFh,0FCh,0E0h,060h,000h,000h,000h,000h,001h,001h,000h,000h,000h
''  defb 000h,001h,001h,000h,000h,000h,000h,000h,000h,080h,0E0h,0F0h,0F8h,07Ch,01Fh,00Fh,0FFh,0FFh,0FFh,0FFh,007h,006h,00Eh,01Ch,01Ch,01Ch,038h,0F8h,0F8h,0F8h,078h,030h
''  defb 060h,0FCh,0FFh,03Fh,03Fh,038h,038h,070h,070h,070h,070h,070h,060h,060h,0E0h,0E0h,0F0h,0F0h,0FCh,0FFh,09Fh,007h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h
''  defb 000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,082h,0E7h,0FFh,0FEh,01Eh,01Eh,01Ch,01Ch,000h,000h,000h,000h,000h
''  defb 00Eh,03Fh,07Fh,0FFh,0F8h,0F0h,0F0h,0F0h,0F8h,0FEh,03Fh,01Fh,00Fh,007h,007h,00Fh,03Fh,07Fh,0F0h,0E0h,0E0h,0E0h,0E0h,0E0h,0F0h,07Fh,03Fh,000h,000h,000h,000h,000h
''  defb 000h,0C0h,0E1h,0F3h,03Fh,01Fh,01Fh,00Eh,00Ch,000h,000h,000h,000h,080h,0E0h,0F0h,0F0h,000h,000h,000h,000h,00Ch,01Eh,03Eh,0FEh,0FEh,08Fh,007h,007h,007h,003h,000h
''  defb 03Ch,0FEh,0FFh,0C7h,083h,001h,001h,001h,003h,00Fh,00Fh,01Fh,01Fh,00Fh,000h,000h,000h,000h,000h,000h,008h,00Eh,00Fh,00Fh,00Fh,00Fh,007h,01Fh,0FFh,0FFh,0FEh,0FCh
''  defb 000h,000h,000h,080h,0C0h,0C0h,0C0h,0C0h,0C0h,0C0h,080h,080h,0F0h,0F8h,0FCh,01Fh,00Fh,007h,007h,007h,00Fh,00Fh,0FEh,0FCh,0F0h,000h,080h,080h,080h,080h,000h,000h
''  defb 000h,000h,000h,000h,000h,000h,000h,03Eh,0FFh,0FFh,0E7h,0C7h,0FFh,0FFh,0FFh,0FFh,03Dh,000h,000h,000h,000h,000h,01Ch,07Fh,07Fh,0E7h,0E7h,0EFh,0FFh,0FFh,07Fh,03Ch
''  defb 000h,000h,001h,001h,001h,001h,000h,000h,000h,080h,080h,080h,080h,080h,080h,0E0h,0F0h,07Eh,03Fh,01Fh,00Fh,03Fh,07Eh,0F0h,0C0h,080h,080h,080h,080h,000h,000h,000h
''  defb 0FEh,0FFh,0FFh,0F7h,0F3h,0F3h,0FFh,07Fh,070h,070h,061h,061h,0E1h,0E1h,0C1h,0C7h,0CFh,0FEh,0FCh,0F8h,0E0h,0E0h,0F8h,03Ch,01Fh,007h,001h,000h,000h,000h,000h,000h
''  defb 000h,080h,080h,080h,080h,080h,000h,000h,07Ch,0FEh,0FEh,0E3h,0F3h,0FFh,0FFh,0FEh,0FCh,000h,000h,000h,000h,000h,000h,000h,0FEh,0FFh,0FFh,0FBh,0F3h,0F3h,07Fh,03Ch
''  defb 006h,006h,00Eh,00Eh,01Eh,03Fh,03Fh,07Fh,07Fh,0F1h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,003h,001h,000h,000h,000h,000h,000h,000h
''  defb 000h,001h,000h,000h,000h,000h,000h,000h,0C0h,0E0h,0F8h,07Ch,03Fh,01Fh,00Fh,007h,003h,001h,001h,001h,003h,007h,007h,00Fh,0FEh,0FCh,07Ch,03Eh,00Eh,006h,002h,000h
''  defb 007h,0FFh,0FEh,07Ch,07Ch,038h,038h,070h,070h,070h,0F0h,0F0h,0F3h,0FFh,0FFh,0FFh,0FEh,0FCh,0F8h,0F8h,0F8h,0FCh,0BCh,01Ch,01Ch,00Fh,00Fh,00Fh,00Fh,01Eh,01Ch,018h
''  defb 000h,000h,000h,000h,000h,000h,000h,080h,0C0h,0E0h,0F0h,0F8h,0FCh,0FEh,08Fh,001h,000h,000h,000h,000h,000h,000h,000h,000h,000h,080h,0E0h,0C0h,080h,000h,000h,000h
''  defb 000h,000h,000h,003h,00Fh,03Fh,01Fh,007h,07Fh,0FFh,0FFh,0FFh,0E7h,0F0h,0F0h,070h,071h,07Fh,03Fh,03Fh,03Fh,01Eh,01Ch,000h,000h,000h,000h,000h,000h,000h,000h,000h
''  defb 000h,020h,0F0h,0F0h,0F8h,0F9h,0F9h,0F9h,0FFh,0FFh,0FFh,0FFh,0FFh,01Fh,01Fh,03Fh,0FFh,0FFh,0FFh,0DFh,09Fh,01Fh,03Fh,03Dh,039h,071h,071h,070h,0E1h,0FFh,0FFh,0F0h
''  defb 030h,0F8h,0FEh,0FFh,0FFh,0E3h,0E1h,0E3h,0CFh,0FFh,0FCh,0F8h,0FCh,0FFh,0FFh,0FFh,01Fh,00Fh,007h,0C1h,0C0h,0C0h,0C0h,0E0h,0E0h,0E0h,0F0h,0F0h,0F0h,0F0h,0E0h,000h
''  defb 000h,000h,000h,000h,0C0h,0E0h,0E0h,0C0h,080h,000h,000h,000h,000h,0E0h,0E0h,0FEh,0FFh,08Fh,0FEh,0FEh,07Eh,03Ch,00Ch,000h,000h,000h,000h,000h,000h,000h,000h,000h
''  end asm

attributes:
''asm
''  defb 01Ah,01Ah,032h,01Ah,032h,032h,01Ah,01Ah,01Ah,01Ah,01Ah,032h,032h,01Ah,032h,01Ah,034h,034h,00Ch,034h,034h,00Ch,00Ch,034h,034h,00Ch,00Ch,00Ch,034h,034h,00Ch,034h
''  defb 04Ah,04Ah,04Ah,04Ah,04Ah,062h,062h,04Ah,04Ah,062h,04Ah,04Ah,04Ah,04Ah,04Ah,04Ah,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h,069h
''  defb 05Ah,072h,05Ah,072h,072h,05Ah,05Ah,072h,05Ah,05Ah,05Ah,05Ah,072h,072h,05Ah,072h,048h,048h,038h,048h,038h,048h,048h,038h,048h,048h,048h,048h,038h,048h,048h,038h
''  defb 056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,056h,021h,021h,011h,021h,011h,011h,021h,021h,021h,021h,021h,021h,021h,011h,021h,021h
''  end asm

typeface:
''asm
''  defb 000h,000h,000h,000h,000h,000h,000h,000h,00Eh,00Ch,00Ch,00Ch,000h,01Ch,00Ch,000h,000h,06Eh,06Eh,02Ch,00Ch,000h,000h,000h,000h,050h,07Ch,0FCh,0D8h,07Ch,0E8h,020h
''  defb 008h,03Ch,038h,01Ch,00Eh,00Ah,03Ah,01Ch,000h,032h,076h,054h,078h,012h,035h,0E7h,030h,068h,078h,030h,072h,05Ch,036h,000h,000h,038h,018h,030h,000h,000h,000h,000h
''  defb 004h,00Ch,018h,018h,018h,018h,01Eh,00Ch,010h,018h,008h,00Ch,00Ch,00Ch,00Ch,018h,000h,018h,01Ah,03Eh,01Ch,03Eh,008h,000h,000h,000h,018h,01Eh,03Eh,038h,008h,000h
''  defb 000h,000h,000h,000h,000h,01Ch,00Ch,018h,000h,000h,000h,000h,07Ch,030h,000h,000h,000h,000h,000h,000h,000h,01Ch,01Ch,000h,006h,006h,00Ch,00Ch,018h,018h,030h,000h
''  defb 000h,000h,03Ch,03Eh,03Ah,03Eh,01Ch,000h,000h,00Ch,01Ch,014h,004h,01Fh,000h,000h,000h,01Eh,016h,002h,006h,03Fh,01Fh,000h,018h,03Eh,00Eh,00Ch,006h,01Eh,01Ch,000h
''  defb 000h,008h,01Bh,033h,03Fh,03Eh,006h,006h,000h,000h,07Ch,07Ch,070h,038h,00Ch,078h,000h,000h,00Eh,018h,01Ch,016h,01Eh,00Ch,00Ch,07Eh,006h,00Ch,004h,00Ch,008h,000h
''  defb 038h,068h,078h,078h,01Ch,024h,03Ch,018h,000h,038h,0ECh,0FCh,074h,004h,03Ch,01Ch,000h,000h,008h,018h,000h,018h,018h,000h,000h,000h,018h,018h,000h,00Ch,01Ch,038h
''  defb 000h,007h,00Eh,03Ch,038h,01Ch,00Ch,004h,000h,000h,03Eh,010h,000h,03Fh,000h,000h,000h,030h,038h,01Ch,00Eh,01Ch,038h,030h,03Ch,03Eh,01Eh,006h,01Ch,000h,00Ch,00Ch
''  defb 000h,03Eh,026h,02Eh,02Eh,020h,032h,01Eh,000h,000h,01Ch,01Eh,013h,03Fh,027h,060h,000h,000h,03Eh,013h,01Eh,01Fh,017h,03Eh,000h,000h,01Ch,034h,030h,01Fh,00Fh,000h
''  defb 000h,000h,03Eh,036h,016h,01Ch,078h,000h,000h,01Eh,018h,01Ch,01Ch,018h,01Fh,01Eh,000h,03Eh,038h,03Eh,03Eh,030h,030h,030h,000h,018h,038h,060h,06Ch,06Eh,064h,03Ch
''  defb 000h,006h,067h,06Eh,07Eh,066h,066h,004h,000h,03Ch,018h,018h,018h,038h,03Eh,000h,000h,003h,00Fh,022h,026h,03Eh,01Ch,000h,010h,019h,01Fh,00Eh,00Ch,01Fh,018h,000h
''  defb 020h,020h,020h,060h,060h,06Eh,07Ch,000h,000h,030h,036h,03Fh,02Fh,02Bh,063h,060h,003h,01Bh,01Bh,01Fh,017h,013h,010h,030h,000h,01Eh,036h,032h,026h,03Eh,01Ch,000h
''  defb 00Eh,01Eh,012h,01Bh,01Eh,018h,018h,010h,018h,03Ch,066h,062h,02Ah,03Eh,01Fh,001h,000h,01Ch,016h,01Eh,018h,01Eh,017h,000h,000h,038h,030h,038h,00Ch,00Ch,038h,000h
''  defb 000h,00Eh,07Eh,018h,018h,018h,018h,000h,000h,003h,033h,033h,03Bh,01Fh,00Eh,000h,033h,033h,01Eh,01Eh,00Eh,00Ch,000h,000h,000h,021h,02Bh,03Bh,01Fh,01Eh,018h,000h
''  defb 076h,074h,03Ch,01Ch,01Eh,036h,000h,000h,000h,042h,076h,03Eh,01Eh,00Ch,00Ch,000h,07Ch,03Eh,006h,00Ch,018h,036h,03Eh,000h,000h,01Eh,018h,018h,018h,01Eh,01Fh,000h
''  defb 040h,070h,070h,018h,00Ch,006h,002h,000h,01Eh,01Eh,002h,002h,002h,01Eh,01Eh,000h,004h,00Eh,01Fh,01Bh,031h,000h,000h,000h,000h,000h,000h,000h,000h,000h,07Eh,000h
''  end asm

patterns2:
''asm
''  defb 0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0E7h,0C3h,081h,0FFh,0FEh,0FCh,0F8h,0F8h,0FCh,0FEh,0FFh,0FFh,0FEh,0FCh,0F8h,0F0h,0E0h,0C0h,080h
''  defb 0BBh,035h,0AAh,0BBh,0BBh,053h,0AAh,0BBh
''  end asm

patterns9:
asm
  defb $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6A,$6B,$6C,$6D,$6E,$6F
  defb 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
end asm

''- compilable on Boriel's zx-basic compiler - http://www.boriel.com/software/the-zx-basic-compiler/?lang=en
''- commandline used: zxb.py -tBa memorama_zxbasic_20111010003859.bas
''- this code uses a library named putchar.bas from http://www.boriel.com/wiki/en/index.php/ZX_BASIC:Putchars.bas

text01:
asm
defb "MEMORAMA"
end asm
text02:
asm
defb "BY PAULO SILVA, '06, '11"
end asm
text03:
asm
defb "PUSH SPACE KEY"
end asm
text04:



