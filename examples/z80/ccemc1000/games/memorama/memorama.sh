rm memorama.bin memorama.asm mc1000header.bin mc1000tail.bin
zxb.py memorama.bas --asm --org=$((0x0200))
zxb.py memorama.bas --org=$((0x0200))
createmc1000header.py memorama.bin
cat mc1000header.bin > memorama.final.bin
cat memorama.bin >> memorama.final.bin
cat mc1000tail.bin >> memorama.final.bin
mv memorama.final.bin memorama.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py memorama.bin
zip memorama.bin.wav.zip memorama.bin.wav
rm memorama.bin.wav
mame mc1000 -video soft -w -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass memorama.bin.wav.zip

#mame mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -ramsize 48k -cass memorama.bin.wav.zip

# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]
