'--------------------------------
'- $00F5 is the system variable address storing the display mode  
'- screen modes available: (as from http://mc-1000.wikispaces.com/Modos+de+v%C3%ADdeo )
'- +1=disable for writing
'- hex = dec = %GSSMMHPW -   size - kind                  - res   - colours             
'- $00 = 0   = %00000000 -  512b - text                  - 32x16 - gk,g
'- $02 = 2   = %00000010 -  512b - text                  - 32x16 - rk,ry
'- $40 = 64  = %01000000 -  512b - semigraphics 4        - 64x32 - k(*8),g,b,w,m,y,r,gc,ry
'- $60 = 96  = %01100000 -  512b - semigraphics 6        - 64x48 - k(*4),g,y,b,r
'- $62 = 98  = %01100010 -  512b - semigraphics 6        - 64x48 - k(*4),w,gc,m,ry
'- $80 = 128 = %10000000 - 1024b - color graphics 1      - 64x64 - g,y,b,r
'- $82 = 130 = %10000010 - 1024b - color graphics 1      - 64x64 - w,c,m,ry
'- $84 = 132 = %10000100 - 1024b - resolution graphics 1 - 128x64 - gk,g
'- $86 = 134 = %10000110 - 1024b - resolution graphics 1 - 128x64 - k,w
'- $88 = 136 = %10001000 - 2048b - color graphics 2      - 128x64 - g,y,b,r
'- $8A = 138 = %10001010 - 2048b - color graphics 2      - 128x64 - w,c,m,ry
'- $8C = 140 = %10001100 - 1536b - resolution graphics 2 - 128x96 - gk,g
'- $8E = 142 = %10001110 - 1536b - resolution graphics 2 - 128x96 - k,w
'- $90 = 144 = %10010000 - 3072b - color graphics 3      - 128x96 - g,y,b,r
'- $92 = 146 = %10010010 - 3072b - color graphics 3      - 128x96 - w,c,m,ry
'- $94 = 148 = %10010100 - 3072b - resolution graphics 3 - 128x192 - gk,g
'- $96 = 150 = %10010110 - 3072b - resolution graphics 3 - 128x192 - k,w
'- $98 = 152 = %10011000 - 6144b - color graphics 6      - 128x192 - g,y,b,r
'- $9A = 154 = %10011010 - 6144b - color graphics 6      - 128x192 - w,c,m,ry
'- $9C = 156 = %10011100 - 6144b - resolution graphics 6 - 256x192 - gk,g
'- $9E = 158 = %10011110 - 6144b - resolution graphics 6 - 256x192 - k,w
'--------------------------------
'- #86 - 140 - pmode0:screen1,0 - GkG    - 128x96
'- #88 - 142 - pmode0:screen1,1 - KW     - 128x96
'- #90 - 144 - pmode1:screen1,0 - BRGY   - 128x96
'- #92 - 146 - pmode1:screen1,1 - MRyGcW - 128x96
'- #94 - 148 - pmode2:screen1,0 - GkG    - 128x192
'- #96 - 150 - pmode2:screen1,1 - KW     - 128x192
'- #98 - 152 - pmode3:screen1,0 - BRGY   - 128x192
'- #9A - 154 - pmode3:screen1,1 - MRyGcW - 128x192
'- #9E - 156 - pmode4:screen1,0 - GkG    - 256x192
'- #9E - 158 - pmode4:screen1,1 - KW     - 256x192


