sub aquariusldir(tvram as uinteger, tram as uinteger,tlen as uinteger):
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld h,(ix+7)
    ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)
    ldir
    end asm
  end sub
