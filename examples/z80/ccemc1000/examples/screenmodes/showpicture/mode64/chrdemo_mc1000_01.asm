	org 512
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
	jp __LABEL__picbin01end
__LABEL__picbin01:
#line 6
		incbin"library/lena_64x32.png.bin"
#line 7
__LABEL__picbin01end:
	ld a, 64
	call _mc1000screen
	ld hl, 512
	push hl
	ld hl, __LABEL__picbin01
	push hl
	ld hl, 32768
	push hl
	call _aquariusldir
__LABEL__loop01:
	jp __LABEL__loop01
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_mc1000screen:
#line 1
		out ($80),a
		ld ($00F5),a
#line 3
_mc1000screen__leave:
	ret
_aquariusldir:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d, (ix+5)
		ld e, (ix+4)
		ld h, (ix+7)
		ld l, (ix+6)
		ld b, (ix+9)
		ld c, (ix+8)
		ldir
#line 8
_aquariusldir__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
	
ZXBASIC_USER_DATA:
_p1:
	DEFB 00, 00
_p2:
	DEFB 00, 00
_s1:
	DEFB 00, 00
_s2:
	DEFB 00, 00
_vl:
	DEFB 00, 00
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
