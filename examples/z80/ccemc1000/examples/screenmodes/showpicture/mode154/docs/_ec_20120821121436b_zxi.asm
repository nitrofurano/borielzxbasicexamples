        col32: equ $80
        modbuf: equ $00f5
        vram: equ $8000

        ld d,a ; d = color.
        ld a,(modbuf)
        ld e,a ; e = video mode.
        bit 7,e
        jr nz,grmode
        nongr:
        ld bc,511
        bit 6,e
        jr nz,sgmode
        alpha:
        ; Mode: Alphanumeric.
        ; Fill with spaces.
        ld a,$20
        jr fill
        sgmode:
        bit 5,e
        ld a,d
        jr nz,sg6
        sg4:
        ; Mode: Semigraphics Four.
        ; Fill with 2x2 colored block.
        ; Byte: 0-[C-C-C]-1-1-1-1, CCC=color (0~7)
        and 7
        rlca
        rlca
        rlca
        rlca
        or $0f
        jr fill
        sg6:
        ; Mode: Semigraphics Six.
        ; Fill with 3x2 colored block.
        ; Byte: [C-C]-1-1-1-1-1-1, CC=color (0~3)
        and 3
        rrca
        rrca
        or $3f
        jr fill
        grmode:
        ; Get fill size from table.
        ld a,e
        rrca
        and $0e
        ld c,a
        ld b,0
        ld hl,gmsztb
        add hl,bc
        ld c,(hl)
        inc hl
        ld b,(hl)
        ; Byte calculation differs among
        ; Color Graphics and Resolution Graphics.
        bit 2,e
        ld a,d
        jr nz,rgn
        cgn:
        ; Mode: Color Graphics n.
        ; Byte: [C-C]-[C-C]-[C-C]-[C-C], CC=color (0~3)
        and $3
        ld d,a
        rlca
        rlca
        or d
        ld d,a
        rlca
        rlca
        rlca
        rlca
        or d
        jr fill
        rgn:
        ; Mode: Resolution Graphics n.
        ; Byte: $00 if color=0, or $ff if color=1.
        rra
        ld a,0
        sbc a,0
        fill:
        ld d,a
        ld a,e
        and $fe
        out (col32),a
        ld hl,vram
        ld (hl),d
        ld d,h
        ld e,l
        inc de
        ldir
        ret
        gmsztb:
        dw 1023 ; Color Graphics 1
        dw 1023 ; Resolution Graphics 1
        dw 2047 ; Color Graphics 2
        dw 1535 ; Resolution Graphics 2
        dw 3071 ; Color Graphics 3
        dw 3071 ; Resolution Graphics 3
        dw 6143 ; Color Graphics 6
        dw 6143 ; Resolution Graphics 6

