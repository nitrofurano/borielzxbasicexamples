rm chrdemo_mc1000_01.bin chrdemo_mc1000_01.asm mc1000header.bin mc1000tail.bin
zxb.py chrdemo_mc1000_01.bas --asm --org=$((0x0200))
zxb.py chrdemo_mc1000_01.bas --org=$((0x0200))
createmc1000header.py chrdemo_mc1000_01.bin
cat mc1000header.bin > chrdemo_mc1000_01.final.bin
cat chrdemo_mc1000_01.bin >> chrdemo_mc1000_01.final.bin
cat mc1000tail.bin >> chrdemo_mc1000_01.final.bin
mv chrdemo_mc1000_01.final.bin chrdemo_mc1000_01.bin
rm mc1000header.bin mc1000tail.bin
mc1000bin2wav.py chrdemo_mc1000_01.bin
mess mc1000 -video soft -w -bp ~/.mess/roms -resolution0 640x480 -skip_gameinfo -cass chrdemo_mc1000_01.bin.wav

# tload, [enter], [insert], [tab], <tape control>, <play>, [tab]
