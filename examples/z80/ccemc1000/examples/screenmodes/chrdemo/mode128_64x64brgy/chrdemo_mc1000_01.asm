	org 512
	; Defines HEAP SIZE
ZXBASIC_HEAP_SIZE EQU 4768
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
	call __MEM_INIT
	jp __LABEL__end01
__LABEL__charmap01:
#line 2
		defb 0BCh,0FCh,0EEh,0ABh,0AAh,0EAh,0ABh,0B8h
		defb 001h,0FFh,03Fh,0BFh,0ABh,0FEh,0AEh,00Ah
		defb 0B8h,0CCh,0FEh,0BCh,0F8h,08Fh,0EEh,0CAh
		defb 0CFh,00Dh,0D0h,00Dh,081h,0F0h,0BBh,0BCh
		defb 0D5h,0D5h,015h,0F1h,0E0h,0EEh,0FFh,000h
		defb 040h,000h,000h,000h,010h,0B3h,0FFh,0FFh
		defb 000h,000h,000h,010h,001h,0B0h,0FDh,0F3h
		defb 000h,000h,000h,001h,055h,004h,003h,0FFh
		defb 005h,005h,005h,055h,055h,055h,0FFh,0F0h
		defb 055h,054h,054h,055h,055h,055h,0FCh,003h
		defb 000h,000h,000h,050h,050h,054h,004h,001h
		defb 000h,000h,000h,011h,015h,055h,055h,050h
		defb 011h,044h,044h,055h,055h,055h,055h,000h
		defb 015h,055h,045h,055h,055h,055h,055h,015h
		defb 04Fh,04Ch,040h,040h,010h,000h,000h,00Ch
		defb 0AAh,0F2h,0F2h,0C3h,0FFh,003h,007h,000h
		defb 00Dh,0C0h,0F1h,0C0h,0F0h,034h,000h,003h
		defb 0FFh,00Fh,00Fh,00Ch,0C0h,0C0h,0C0h,000h
		defb 083h,08Fh,0FFh,0FFh,00Ch,00Ch,000h,0CCh
		defb 0C1h,000h,000h,030h,000h,000h,004h,000h
		defb 010h,000h,000h,03Ch,0C0h,0C0h,0C0h,0C0h
		defb 003h,003h,003h,003h,000h,003h,003h,033h
		defb 015h,015h,055h,015h,015h,015h,055h,015h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,055h,055h,055h,055h,055h,055h,051h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 03Ch,028h,028h,03Ch,03Ch,000h,03Ch,000h
		defb 02Fh,02Fh,02Fh,000h,000h,000h,000h,000h
		defb 02Fh,02Fh,0EAh,02Fh,0EAh,02Fh,02Fh,000h
		defb 03Ch,02Bh,0F0h,028h,00Fh,0E8h,03Ch,000h
		defb 000h,0F2h,0FFh,008h,03Ch,022h,0F2h,000h
		defb 038h,02Fh,038h,02Eh,0FBh,0FFh,02Eh,000h
		defb 008h,008h,03Ch,000h,000h,000h,000h,000h
		defb 008h,03Ch,020h,020h,020h,03Ch,008h,000h
		defb 020h,03Ch,008h,008h,008h,03Ch,020h,000h
		defb 000h,0FFh,028h,0AAh,028h,0FFh,000h,000h
		defb 000h,03Ch,03Ch,0EBh,03Ch,03Ch,000h,000h
		defb 000h,000h,000h,000h,03Ch,00Ch,030h,000h
		defb 000h,000h,000h,0EBh,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,03Ch,03Ch,000h
		defb 002h,00Fh,008h,03Ch,020h,0F0h,080h,000h
		defb 038h,03Fh,0F2h,0F2h,0F2h,023h,038h,000h
		defb 008h,038h,008h,008h,008h,008h,02Ah,000h
		defb 02Bh,0F2h,00Eh,03Bh,028h,0E0h,0EAh,000h
		defb 02Ah,00Fh,008h,03Bh,002h,0F2h,02Bh,000h
		defb 00Bh,03Bh,02Fh,0FFh,0EAh,00Fh,00Fh,000h
		defb 0EBh,0F0h,0EBh,002h,002h,0F2h,02Bh,000h
		defb 038h,020h,0F0h,0EBh,0F2h,0F2h,02Bh,000h
		defb 0EAh,0F2h,00Fh,008h,03Ch,03Ch,03Ch,000h
		defb 028h,0F3h,0E3h,028h,0CAh,0C2h,02Bh,000h
		defb 02Bh,0F2h,0F2h,02Ah,002h,00Fh,028h,000h
		defb 000h,000h,03Ch,03Ch,000h,03Ch,03Ch,000h
		defb 000h,03Ch,03Ch,000h,02Ch,03Ch,020h,000h
		defb 008h,03Ch,020h,0F0h,020h,03Ch,008h,000h
		defb 000h,000h,0EBh,000h,0EBh,000h,000h,000h
		defb 020h,03Ch,008h,00Fh,008h,03Ch,020h,000h
		defb 0E8h,08Fh,00Fh,038h,020h,000h,020h,000h
		defb 02Bh,0F2h,0FAh,0FAh,0FAh,0F0h,028h,000h
		defb 038h,02Fh,0F2h,0F2h,0EAh,0F2h,0F2h,000h
		defb 0EBh,0F2h,0F2h,0EBh,0F2h,0F2h,0EBh,000h
		defb 03Bh,022h,0F0h,0F0h,0F0h,022h,03Bh,000h
		defb 0E8h,0FFh,0F2h,0F2h,0F2h,0FFh,0E8h,000h
		defb 02Ah,020h,020h,02Bh,020h,020h,02Ah,000h
		defb 0EAh,0F0h,0F0h,0EBh,0F0h,0F0h,0F0h,000h
		defb 03Ah,020h,0F0h,0FEh,0F2h,022h,03Ah,000h
		defb 0F2h,0F2h,0F2h,0EAh,0F2h,0F2h,0F2h,000h
		defb 02Ah,008h,008h,008h,008h,008h,02Ah,000h
		defb 002h,002h,002h,002h,002h,0F2h,02Bh,000h
		defb 0F2h,0FFh,0F8h,0ECh,0E8h,0FBh,0FEh,000h
		defb 020h,020h,020h,020h,020h,020h,02Ah,000h
		defb 0F2h,0EEh,0EAh,0EAh,0FEh,0F2h,0F2h,000h
		defb 0F2h,0E2h,0EEh,0EAh,0FAh,0FEh,0F2h,000h
		defb 02Bh,0F2h,0F2h,0F2h,0F2h,0F2h,02Bh,000h
		defb 0EBh,0F2h,0F2h,0F2h,0EBh,0F0h,0F0h,000h
		defb 02Bh,0F2h,0F2h,0F2h,0FAh,0FFh,02Bh,000h
		defb 0EBh,0F2h,0F2h,0FEh,0E8h,0FBh,0FEh,000h
		defb 028h,0FFh,0F0h,02Bh,002h,0F2h,02Bh,000h
		defb 02Ah,008h,008h,008h,008h,008h,008h,000h
		defb 0F2h,0F2h,0F2h,0F2h,0F2h,0F2h,02Bh,000h
		defb 0F2h,0F2h,0F2h,0EEh,02Bh,038h,00Ch,000h
		defb 0F2h,0F2h,0FEh,0EAh,0EAh,0EEh,0F2h,000h
		defb 0F2h,0EEh,02Bh,038h,02Bh,0EEh,0F2h,000h
		defb 022h,022h,022h,03Bh,008h,008h,008h,000h
		defb 0EAh,00Eh,00Bh,038h,02Ch,0E0h,0EAh,000h
		defb 000h,000h,000h,028h,028h,000h,000h,000h
		defb 000h,000h,000h,0EAh,0EAh,000h,000h,000h
		defb 000h,000h,000h,000h,000h,03Ch,03Ch,000h
		defb 03Ch,028h,028h,03Ch,03Ch,000h,03Ch,000h
		defb 028h,0C3h,0FBh,0F3h,0F3h,0FBh,0C3h,028h
		defb 08Ch,0CCh,0BFh,08Ch,0B8h,0ECh,0EBh,0EBh
		defb 0FFh,00Eh,033h,000h,0CCh,00Ch,033h,030h
		defb 0BAh,00Eh,03Eh,00Eh,0CEh,0C2h,032h,03Fh
		defb 0BDh,0F0h,0F0h,0BCh,08Ch,0CFh,0FFh,0BFh
		defb 000h,000h,000h,003h,001h,000h,000h,000h
		defb 00Fh,000h,030h,000h,010h,000h,000h,000h
		defb 003h,0F3h,000h,000h,000h,000h,030h,010h
		defb 0FCh,0FFh,000h,001h,001h,010h,000h,010h
		defb 00Fh,000h,000h,010h,011h,000h,040h,011h
		defb 0AFh,03Ch,03Fh,000h,010h,010h,0D0h,0DDh
		defb 0CEh,0FFh,0C0h,000h,010h,000h,004h,000h
		defb 0AEh,0F3h,000h,000h,000h,000h,000h,001h
		defb 0B0h,0F0h,000h,001h,001h,000h,000h,000h
		defb 015h,005h,000h,014h,015h,005h,005h,005h
		defb 03Ch,040h,030h,030h,030h,030h,030h,0F0h
		defb 003h,000h,000h,010h,000h,0CCh,0FCh,030h
		defb 013h,003h,043h,003h,0C3h,003h,003h,003h
		defb 0CFh,00Fh,0CFh,0C0h,00Ch,0CFh,00Ch,00Eh
		defb 000h,003h,00Ch,000h,000h,0C0h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,003h,000h
		defb 001h,000h,000h,031h,030h,000h,000h,000h
		defb 00Fh,003h,003h,003h,003h,003h,003h,003h
		defb 015h,055h,055h,015h,015h,015h,055h,015h
		defb 055h,055h,055h,055h,055h,055h,055h,055h
		defb 055h,051h,051h,05Fh,051h,053h,051h,05Ch
		defb 011h,040h,004h,0C0h,011h,0FFh,015h,0D1h
		defb 011h,000h,044h,000h,011h,0C0h,045h,01Ch
		defb 011h,000h,044h,000h,011h,000h,055h,000h
		defb 011h,000h,044h,000h,011h,000h,055h,050h
		defb 015h,005h,045h,001h,011h,000h,054h,000h
		defb 055h,055h,054h,000h,011h,000h,044h,000h
		defb 05Dh,00Ch,032h,000h,011h,000h,045h,000h
		defb 0EBh,0EFh,02Fh,00Ch,0CCh,0CCh,033h,000h
		defb 0FBh,0FFh,00Fh,01Bh,018h,00Ch,034h,031h
		defb 0BEh,0AEh,0FBh,0FBh,03Dh,00Dh,071h,001h
		defb 0C8h,0CCh,0CCh,00Ch,00Ch,00Ch,00Fh,00Fh
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 010h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,031h,0F1h,0F1h,001h,005h,001h
		defb 010h,000h,040h,00Fh,03Fh,040h,040h,000h
		defb 011h,000h,040h,000h,000h,000h,000h,000h
		defb 000h,000h,040h,000h,000h,000h,000h,000h
		defb 001h,000h,000h,000h,000h,000h,000h,000h
		defb 010h,000h,000h,011h,001h,000h,000h,000h
		defb 0C5h,0C0h,0C0h,003h,003h,0CFh,00Fh,00Fh
		defb 0F0h,0F0h,0F0h,0FFh,0F0h,0F0h,0F0h,0F0h
		defb 030h,031h,000h,0C1h,000h,000h,040h,040h
		defb 003h,003h,003h,000h,0F3h,0F0h,030h,000h
		defb 00Fh,0C0h,0C3h,0C0h,000h,000h,000h,000h
		defb 0FCh,04Fh,044h,000h,000h,000h,000h,000h
		defb 003h,0C0h,003h,00Fh,0C3h,003h,028h,013h
		defb 001h,000h,000h,000h,001h,000h,030h,030h
		defb 003h,003h,0C3h,0C3h,0C3h,0C3h,0C3h,003h
		defb 015h,015h,055h,014h,001h,015h,015h,005h
		defb 055h,015h,055h,015h,055h,055h,055h,055h
		defb 050h,054h,05Fh,001h,041h,041h,051h,041h
		defb 0DDh,0F0h,033h,033h,055h,055h,050h,050h
		defb 00Eh,04Eh,003h,0F0h,03Ch,00Ch,01Ch,030h
		defb 0AAh,0AAh,0AAh,0AAh,0AFh,000h,000h,03Bh
		defb 0BEh,0AAh,0B2h,0B0h,0F0h,000h,0D0h,090h
		defb 0AAh,0EAh,0EAh,038h,00Dh,014h,004h,030h
		defb 0FEh,002h,0AEh,0AFh,0FFh,0BFh,0F3h,0F7h
		defb 0AAh,0AAh,02Ah,0BEh,0BFh,0ABh,0B3h,0BFh
		defb 0CFh,0C3h,0EFh,0ABh,0A8h,0A8h,0E3h,0ACh
		defb 031h,034h,074h,001h,03Dh,03Ch,070h,031h
		defb 011h,052h,042h,01Eh,01Eh,042h,047h,013h
		defb 0CFh,08Fh,0BFh,08Fh,0CCh,0CFh,08Fh,08Ch
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 000h,000h,000h,000h,000h,000h,000h,000h
		defb 0CFh,0CFh,00Fh,0CFh,0FFh,0FFh,03Fh,0FFh
		defb 0F0h,0F0h,0C0h,0F0h,0C0h,0F0h,0F5h,0F0h
		defb 000h,000h,000h,000h,013h,003h,040h,0CCh
		defb 000h,030h,03Ch,0C0h,000h,000h,0CFh,000h
		defb 000h,000h,030h,000h,000h,00Ch,030h,030h
		defb 001h,011h,004h,000h,000h,005h,004h,011h
		defb 010h,040h,050h,013h,010h,043h,007h,000h
		defb 000h,0C0h,000h,000h,030h,030h,007h,00Eh
		defb 003h,000h,000h,00Ch,030h,0F0h,0C0h,003h
		defb 088h,088h,030h,0FCh,03Ch,08Eh,003h,0CFh
		defb 08Bh,03Ch,000h,000h,0CAh,003h,003h,000h
		defb 080h,001h,03Bh,004h,0BCh,0ACh,0AAh,0ABh
		defb 05Dh,03Fh,002h,00Eh,0C2h,08Ch,0CCh,0B0h
		defb 030h,040h,070h,000h,090h,001h,004h,000h
		defb 0CFh,00Ch,000h,000h,001h,000h,043h,003h
		defb 081h,001h,045h,0F1h,0E1h,0B0h,0ECh,0BCh
		defb 000h,03Ch,044h,03Ch,031h,0C0h,0FCh,000h
		defb 0FFh,03Fh,007h,000h,011h,003h,004h,000h
		defb 0B0h,0F7h,0A7h,003h,00Fh,0C3h,0C3h,0C3h
		defb 0AAh,0AAh,0AAh,0AAh,0AAh,0FAh,0AAh,0AAh
		defb 031h,074h,064h,021h,031h,000h,0F0h,0B0h
		defb 012h,002h,04Eh,00Ah,02Ah,07Ah,00Ah,00Bh
		defb 08Ch,0BCh,0BCh,08Ch,08Fh,08Fh,0CFh,0CCh
		defb 000h,000h,0F0h,0FFh,03Fh,03Fh,03Fh,03Fh
		defb 000h,000h,000h,0FFh,0FFh,0FCh,0FCh,0C0h
		defb 000h,000h,000h,000h,0FFh,0EAh,03Fh,0FFh
		defb 000h,000h,000h,000h,0F0h,0AFh,0FAh,0FFh
		defb 000h,000h,000h,000h,000h,0FFh,0AAh,0FFh
		defb 000h,000h,000h,000h,000h,0F0h,0BFh,0FAh
		defb 000h,000h,000h,000h,000h,000h,0FFh,0ABh
		defb 000h,000h,000h,000h,000h,000h,0F0h,0FFh
		defb 000h,000h,000h,000h,000h,000h,000h,0FCh
		defb 03Fh,03Fh,03Fh,03Fh,0FFh,0FFh,0FFh,0FFh
		defb 0F0h,0F0h,0F4h,0F0h,0F0h,0C0h,0F0h,0C0h
		defb 000h,000h,040h,000h,000h,000h,000h,000h
		defb 000h,040h,000h,010h,001h,000h,000h,000h
		defb 000h,03Ch,02Bh,03Ah,02Ah,02Ah,02Ah,02Ah
		defb 010h,000h,004h,0BFh,0AAh,0AAh,0AAh,0AAh
		defb 000h,004h,007h,00Eh,0ABh,0ACh,0ACh,0B0h
		defb 00Eh,0ABh,0EBh,0EFh,0FFh,030h,000h,03Ch
		defb 013h,000h,004h,000h,010h,000h,004h,000h
		defb 0CFh,0DFh,030h,000h,010h,000h,000h,000h
		defb 00Fh,0C0h,000h,000h,000h,000h,004h,000h
		defb 0AFh,0FFh,000h,000h,000h,000h,000h,000h
		defb 0CCh,0A0h,000h,000h,000h,000h,040h,000h
		defb 011h,000h,000h,000h,000h,000h,004h,000h
		defb 010h,000h,007h,003h,010h,000h,000h,000h
		defb 0F0h,0EDh,004h,0C1h,001h,000h,040h,000h
		defb 001h,00Ch,004h,000h,001h,000h,000h,000h
		defb 010h,003h,043h,003h,000h,003h,004h,000h
		defb 0C5h,0C5h,005h,0C0h,0D1h,001h,0C5h,00Ch
		defb 0BEh,0BEh,0B2h,0F2h,0F3h,0FFh,0FCh,0C3h
		defb 0F0h,0B0h,0F0h,0EBh,0F0h,0F0h,0B0h,0B0h
		defb 00Fh,00Fh,0FFh,0FFh,03Ch,03Ch,028h,02Bh
		defb 08Ch,0CCh,0CFh,0CCh,08Ch,08Ch,0FCh,08Fh
		defb 00Fh,003h,000h,000h,000h,000h,000h,000h
		defb 0FCh,0FFh,030h,000h,000h,000h,000h,000h
		defb 00Fh,0FFh,0EAh,02Ah,000h,03Fh,010h,000h
		defb 0FFh,003h,0AFh,0FFh,0FFh,0FCh,000h,001h
		defb 0FEh,0FFh,0F0h,0EAh,00Fh,043h,043h,000h
		defb 0AFh,0FFh,003h,0FFh,0FAh,0ABh,0F0h,003h
		defb 0FFh,0FFh,0F0h,000h,0ABh,03Fh,030h,0F0h
		defb 0FFh,0FFh,0FFh,00Fh,0FCh,0FCh,000h,030h
		defb 0FCh,0FCh,0CCh,000h,00Ch,000h,000h,000h
		defb 0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh
		defb 0C0h,0C0h,0C0h,0F3h,0FBh,0AFh,0BCh,0FCh
		defb 000h,000h,000h,0C0h,000h,000h,000h,010h
		defb 000h,000h,000h,000h,000h,00Fh,002h,003h
		defb 0AAh,0AEh,0EAh,0EAh,01Fh,000h,0B0h,0B0h
		defb 0AAh,0AAh,0AAh,0AAh,0FEh,02Bh,00Eh,03Eh
		defb 0B1h,0F0h,0F0h,0C0h,0C0h,0C0h,0C4h,000h
		defb 00Ch,03Ch,00Ch,0CFh,0CFh,00Fh,00Fh,00Fh
		defb 011h,000h,000h,000h,010h,000h,000h,000h
		defb 000h,000h,000h,000h,001h,000h,044h,010h
		defb 001h,000h,044h,000h,011h,000h,040h,000h
		defb 011h,000h,040h,000h,011h,000h,040h,000h
		defb 011h,000h,000h,000h,011h,000h,040h,000h
		defb 001h,000h,040h,000h,010h,000h,004h,000h
		defb 000h,000h,044h,000h,001h,000h,040h,000h
		defb 010h,000h,040h,000h,001h,000h,040h,000h
		defb 011h,000h,000h,010h,011h,000h,040h,000h
		defb 011h,000h,004h,000h,011h,000h,044h,000h
		defb 01Ch,00Eh,04Fh,00Bh,00Ah,03Bh,07Bh,03Bh
#line 258
__LABEL__end01:
	ld a, 128
	call _mc1000screen
	ld a, 2
	push af
	call _mc1000cls
	ld hl, 50
	ld (_rseed), hl
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 3
	call __MUL16_FAST
	ld de, 0
	ld (_p1), hl
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 8
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld (_s1), hl
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 3
	call __MUL16_FAST
	ld de, 0
	ld (_p2), hl
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 8
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld (_s2), hl
__LABEL__lp01:
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	ld (_rseed), hl
	ex de, hl
	ld hl, 0
	push de
	push hl
	ld de, 1
	ld hl, 39321
	call __DIVF16
	ex de, hl
	ld a, h
	add a, a
	sbc a, a
	ld e, a
	ld d, a
	ld bc, 0
	push bc
	ld bc, 128
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	push hl
	push de
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_vl), hl
	ld hl, __LABEL__charmap01
	push hl
	ld hl, (_vl)
	push hl
	ld hl, (_p1)
	push hl
	call _mc1000vpokechr64wed
	ld hl, (_p1)
	inc hl
	ld h, 0
	ld a, l
	and 127
	ld l, a
	ld (_p1), hl
	ld hl, (_s1)
	dec hl
	ld (_s1), hl
	ld de, 1
	ld hl, (_s1)
	call __LTI16
	or a
	jp z, __LABEL1
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 3
	call __MUL16_FAST
	ld de, 0
	ld (_p1), hl
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 8
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld (_s1), hl
__LABEL1:
	ld hl, __LABEL__charmap01
	push hl
	ld hl, 32
	push hl
	ld hl, (_p2)
	push hl
	call _mc1000vpokechr64wed
	ld hl, (_p2)
	inc hl
	ld h, 0
	ld a, l
	and 127
	ld l, a
	ld (_p2), hl
	ld hl, (_s2)
	dec hl
	ld (_s2), hl
	ld de, 1
	ld hl, (_s2)
	call __LTI16
	or a
	jp z, __LABEL3
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 3
	call __MUL16_FAST
	ld de, 0
	ld (_p2), hl
	ld hl, (_rseed)
	push hl
	call _mc1000rnd
	call __FTOU32REG
	push de
	push hl
	ld de, 0
	ld hl, 255
	call __BAND32
	ld (_rseed), hl
	ld de, 8
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 8
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld (_s2), hl
__LABEL3:
	jp __LABEL__lp01
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_mc1000screen:
#line 1
		out ($80),a
		ld ($00F5),a
#line 3
_mc1000screen__leave:
	ret
_mc1000cls:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	push hl
	push hl
	inc sp
	ld (ix-3), 0
	ld (ix-2), 2
	ld (ix-1), 0
	ld a, (245)
	push af
	ld h, 248
	pop af
	and h
	sub 128
	sub 1
	jp nc, __LABEL5
	ld (ix-3), 0
	ld (ix-2), 4
__LABEL5:
	ld a, (245)
	push af
	ld h, 252
	pop af
	and h
	sub 136
	sub 1
	jp nc, __LABEL7
	ld (ix-3), 0
	ld (ix-2), 8
__LABEL7:
	ld a, (245)
	push af
	ld h, 252
	pop af
	and h
	sub 140
	sub 1
	jp nc, __LABEL9
	ld (ix-3), 0
	ld (ix-2), 6
__LABEL9:
	ld a, (245)
	push af
	ld h, 248
	pop af
	and h
	sub 144
	sub 1
	jp nc, __LABEL11
	ld (ix-3), 0
	ld (ix-2), 12
__LABEL11:
	ld a, (245)
	push af
	ld h, 248
	pop af
	and h
	sub 152
	sub 1
	jp nc, __LABEL13
	ld (ix-3), 0
	ld (ix-2), 24
__LABEL13:
	ld a, (245)
	push af
	ld h, 132
	pop af
	and h
	sub 128
	sub 1
	jp nc, __LABEL15
	ld a, (ix+5)
	push af
	ld h, 3
	pop af
	and h
	ld h, 85
	call __MUL8_FAST
	ld (ix-1), a
__LABEL15:
	ld a, (245)
	push af
	ld h, 132
	pop af
	and h
	sub 132
	sub 1
	jp nc, __LABEL17
	ld a, (ix+5)
	push af
	ld h, 1
	pop af
	and h
	ld h, 255
	call __MUL8_FAST
	ld (ix-1), a
__LABEL17:
	ld a, (245)
	push af
	ld h, 96
	pop af
	and h
	sub 96
	sub 1
	jp nc, __LABEL19
	ld (ix-1), 0
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	push af
	ld h, 4
	pop af
	cp h
	jp nc, __LABEL21
	ld a, (ix+5)
	push af
	ld h, 3
	pop af
	and h
	ld b, 6
__LABEL58:
	add a, a
	djnz __LABEL58
	ld h, a
	ld a, 63
	or h
	ld (ix-1), a
__LABEL21:
__LABEL19:
	ld a, (245)
	push af
	ld h, 96
	pop af
	and h
	sub 64
	sub 1
	jp nc, __LABEL23
	ld (ix-1), 0
	ld a, (ix+5)
	push af
	ld h, 15
	pop af
	and h
	push af
	ld h, 8
	pop af
	cp h
	jp nc, __LABEL25
	ld a, (ix+5)
	push af
	ld h, 7
	pop af
	and h
	add a, a
	add a, a
	add a, a
	add a, a
	ld h, a
	ld a, 15
	or h
	ld (ix-1), a
__LABEL25:
__LABEL23:
	ld a, (245)
	push af
	ld h, 252
	pop af
	and h
	sub 1
	jp nc, __LABEL27
	ld a, (ix+5)
	push af
	ld h, 1
	pop af
	and h
	ld b, 7
__LABEL59:
	add a, a
	djnz __LABEL59
	ld h, a
	ld a, 32
	or h
	ld (ix-1), a
__LABEL27:
	ld (ix-5), 0
	ld (ix-4), 0
	jp __LABEL28
__LABEL31:
	ld l, (ix-5)
	ld h, (ix-4)
	ld de, 32768
	add hl, de
	push hl
	ld a, (ix-1)
	pop hl
	ld (hl), a
__LABEL32:
	ld l, (ix-5)
	ld h, (ix-4)
	inc hl
	ld (ix-5), l
	ld (ix-4), h
__LABEL28:
	ld l, (ix-5)
	ld h, (ix-4)
	push hl
	ld l, (ix-3)
	ld h, (ix-2)
	dec hl
	pop de
	or a
	sbc hl, de
	jp nc, __LABEL31
__LABEL30:
_mc1000cls__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_mc1000vpokechr:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	inc sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 768
	call __MODU16
	ld (ix+4), l
	ld (ix+5), h
	ld (ix-1), 0
	jp __LABEL33
__LABEL36:
	ld a, (ix-1)
	ld h, 32
	call __MUL8_FAST
	ld l, a
	ld h, 0
	ld de, 32768
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld h, 0
	ld a, l
	and 31
	ld l, a
	ex de, hl
	pop hl
	add hl, de
	ld de, 0
	push de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 32
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 256
	call __MUL32
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
__LABEL37:
	ld a, (ix-1)
	inc a
	ld (ix-1), a
__LABEL33:
	ld a, (ix-1)
	push af
	ld a, 7
	pop hl
	cp h
	jp nc, __LABEL36
__LABEL35:
_mc1000vpokechr__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000vpokechr64wed:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	inc sp
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 768
	call __MODU16
	ld (ix+4), l
	ld (ix+5), h
	ld (ix-1), 0
	jp __LABEL38
__LABEL41:
	ld a, (ix-1)
	ld h, 16
	call __MUL8_FAST
	ld l, a
	ld h, 0
	ld de, 32768
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld h, 0
	ld a, l
	and 15
	ld l, a
	ex de, hl
	pop hl
	add hl, de
	ld de, 0
	push de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ld de, 16
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 128
	call __MUL32
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
__LABEL42:
	ld a, (ix-1)
	inc a
	ld (ix-1), a
__LABEL38:
	ld a, (ix-1)
	push af
	ld a, 7
	pop hl
	cp h
	jp nc, __LABEL41
__LABEL40:
_mc1000vpokechr64wed__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000vpcls:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	push hl
	inc sp
	ld hl, 0
	ld (_tadrv2), hl
	jp __LABEL43
__LABEL46:
	ld (ix-1), 0
	jp __LABEL48
__LABEL51:
	ld a, (ix-1)
	ld h, 32
	call __MUL8_FAST
	ld l, a
	ld h, 0
	ld de, 32768
	add hl, de
	push hl
	ld hl, (_tadrv2)
	ld h, 0
	ld a, l
	and 31
	ld l, a
	ex de, hl
	pop hl
	add hl, de
	ld de, 0
	push de
	push hl
	ld hl, (_tadrv2)
	ld de, 32
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 256
	call __MUL32
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	add hl, hl
	add hl, hl
	add hl, hl
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld a, (ix-1)
	ld l, a
	ld h, 0
	ex de, hl
	pop hl
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
__LABEL52:
	ld a, (ix-1)
	inc a
	ld (ix-1), a
__LABEL48:
	ld a, (ix-1)
	push af
	ld a, 7
	pop hl
	cp h
	jp nc, __LABEL51
__LABEL50:
__LABEL47:
	ld hl, (_tadrv2)
	inc hl
	ld (_tadrv2), hl
__LABEL43:
	ld hl, 767
	ld de, (_tadrv2)
	or a
	sbc hl, de
	jp nc, __LABEL46
__LABEL45:
_mc1000vpcls__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000vpprint:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_tadrv2), hl
	jp __LABEL53
__LABEL56:
	ld l, (ix+6)
	ld h, (ix+7)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld hl, (_tadrv2)
	push hl
	ld hl, (_tadrv2)
	push hl
	xor a
	call __STRSLICE
	ld a, 1
	call VAL
	call __FTOU32REG
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	ld hl, (_tadrv2)
	add hl, de
	push hl
	call _mc1000vpokechr
__LABEL57:
	ld hl, (_tadrv2)
	inc hl
	ld (_tadrv2), hl
__LABEL53:
	ld l, (ix+8)
	ld h, (ix+9)
	call __STRLEN
	ld de, (_tadrv2)
	or a
	sbc hl, de
	jp nc, __LABEL56
__LABEL55:
_mc1000vpprint__leave:
	ex af, af'
	exx
	ld l, (ix+8)
	ld h, (ix+9)
	call __MEM_FREE
	ex af, af'
	exx
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_mc1000rnd:
	push ix
	ld ix, 0
	add ix, sp
	ld l, (ix+4)
	ld h, (ix+5)
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld de, 221
	call __MUL16_FAST
	ld de, 53
	add hl, de
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ld de, 0
	call __U32TOFREG
_mc1000rnd__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 1142 "chrdemo_mc1000_01.bas"
#line 1 "mul32.asm"
#line 1 "_mul32.asm"
	
; Ripped from: http://www.andreadrian.de/oldcpu/z80_number_cruncher.html#moztocid784223
	; Used with permission.
	; Multiplies 32x32 bit integer (DEHL x D'E'H'L')
	; 64bit result is returned in H'L'H L B'C'A C
	
	
__MUL32_64START:
			push hl
			exx
			ld b, h
			ld c, l		; BC = Low Part (A)
			pop hl		; HL = Load Part (B)
			ex de, hl	; DE = Low Part (B), HL = HightPart(A) (must be in B'C')
			push hl
	
			exx
			pop bc		; B'C' = HightPart(A)
			exx			; A = B'C'BC , B = D'E'DE
	
				; multiply routine 32 * 32bit = 64bit
				; h'l'hlb'c'ac = b'c'bc * d'e'de
				; needs register a, changes flags
				;
				; this routine was with tiny differences in the
				; sinclair zx81 rom for the mantissa multiply
	
__LMUL:
	        and     a               ; reset carry flag
	        sbc     hl,hl           ; result bits 32..47 = 0
	        exx
	        sbc     hl,hl           ; result bits 48..63 = 0
	        exx
	        ld      a,b             ; mpr is b'c'ac
	        ld      b,33            ; initialize loop counter
	        jp      __LMULSTART  
	
__LMULLOOP:
	        jr      nc,__LMULNOADD  ; JP is 2 cycles faster than JR. Since it's inside a LOOP
	                                ; it can save up to 33 * 2 = 66 cycles
	                                ; But JR if 3 cycles faster if JUMP not taken!
	        add     hl,de           ; result += mpd
	        exx
	        adc     hl,de
	        exx
	
__LMULNOADD:
	        exx
	        rr      h               ; right shift upper
	        rr      l               ; 32bit of result
	        exx
	        rr      h
	        rr      l
	
__LMULSTART:
	        exx
	        rr      b               ; right shift mpr/
	        rr      c               ; lower 32bit of result
	        exx
	        rra                     ; equivalent to rr a
	        rr      c
	        djnz    __LMULLOOP
	
			ret						; result in h'l'hlb'c'ac
	       
#line 2 "mul32.asm"
	
__MUL32:	; multiplies 32 bit un/signed integer.
				; First operand stored in DEHL, and 2nd onto stack
				; Lowest part of 2nd operand on top of the stack
				; returns the result in DE.HL
			exx
			pop hl	; Return ADDRESS
			pop de	; Low part
			ex (sp), hl ; CALLEE -> HL = High part
			ex de, hl
			call __MUL32_64START
	
__TO32BIT:  ; Converts H'L'HLB'C'AC to DEHL (Discards H'L'HL)
			exx
			push bc
			exx
			pop de
			ld h, a
			ld l, c
			ret
	
	
#line 1143 "chrdemo_mc1000_01.bas"
#line 1 "free.asm"
; vim: ts=4:et:sw=4:
	; Copyleft (K) by Jose M. Rodriguez de la Rosa
	;  (a.k.a. Boriel) 
;  http://www.boriel.com
	;
	; This ASM library is licensed under the BSD license
	; you can use it for any purpose (even for commercial
	; closed source programs).
	;
	; Please read the BSD license on the internet
	
	; ----- IMPLEMENTATION NOTES ------
	; The heap is implemented as a linked list of free blocks.
	
; Each free block contains this info:
	; 
	; +----------------+ <-- HEAP START 
	; | Size (2 bytes) |
	; |        0       | <-- Size = 0 => DUMMY HEADER BLOCK
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   | <-- If Size > 4, then this contains (size - 4) bytes
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+   |
	;   <Allocated>        | <-- This zone is in use (Already allocated)
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Next (2 bytes) |--> NULL => END OF LIST
	; |    0 = NULL    |
	; +----------------+
	; | <free bytes...>|
	; | (0 if Size = 4)|
	; +----------------+
	
	
	; When a block is FREED, the previous and next pointers are examined to see
	; if we can defragment the heap. If the block to be breed is just next to the
	; previous, or to the next (or both) they will be converted into a single
	; block (so defragmented).
	
	
	;   MEMORY MANAGER
	;
	; This library must be initialized calling __MEM_INIT with 
	; HL = BLOCK Start & DE = Length.
	
	; An init directive is useful for initialization routines.
	; They will be added automatically if needed.
	
#line 1 "heapinit.asm"
; vim: ts=4:et:sw=4:
	; Copyleft (K) by Jose M. Rodriguez de la Rosa
	;  (a.k.a. Boriel) 
;  http://www.boriel.com
	;
	; This ASM library is licensed under the BSD license
	; you can use it for any purpose (even for commercial
	; closed source programs).
	;
	; Please read the BSD license on the internet
	
	; ----- IMPLEMENTATION NOTES ------
	; The heap is implemented as a linked list of free blocks.
	
; Each free block contains this info:
	; 
	; +----------------+ <-- HEAP START 
	; | Size (2 bytes) |
	; |        0       | <-- Size = 0 => DUMMY HEADER BLOCK
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   | <-- If Size > 4, then this contains (size - 4) bytes
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+   |
	;   <Allocated>        | <-- This zone is in use (Already allocated)
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Next (2 bytes) |--> NULL => END OF LIST
	; |    0 = NULL    |
	; +----------------+
	; | <free bytes...>|
	; | (0 if Size = 4)|
	; +----------------+
	
	
	; When a block is FREED, the previous and next pointers are examined to see
	; if we can defragment the heap. If the block to be breed is just next to the
	; previous, or to the next (or both) they will be converted into a single
	; block (so defragmented).
	
	
	;   MEMORY MANAGER
	;
	; This library must be initialized calling __MEM_INIT with 
	; HL = BLOCK Start & DE = Length.
	
	; An init directive is useful for initialization routines.
	; They will be added automatically if needed.
	
	
	
	
	; ---------------------------------------------------------------------
	;  __MEM_INIT must be called to initalize this library with the
	; standard parameters
	; ---------------------------------------------------------------------
__MEM_INIT: ; Initializes the library using (RAMTOP) as start, and
	        ld hl, ZXBASIC_MEM_HEAP  ; Change this with other address of heap start
	        ld de, ZXBASIC_HEAP_SIZE ; Change this with your size
	
	; ---------------------------------------------------------------------
	;  __MEM_INIT2 initalizes this library 
; Parameters:
;   HL : Memory address of 1st byte of the memory heap
;   DE : Length in bytes of the Memory Heap
	; ---------------------------------------------------------------------
__MEM_INIT2:     
	        ; HL as TOP            
	        PROC
	
	        dec de
	        dec de
	        dec de
	        dec de        ; DE = length - 4; HL = start
	        ; This is done, because we require 4 bytes for the empty dummy-header block
	
	        xor a
	        ld (hl), a
	        inc hl
        ld (hl), a ; First "free" block is a header: size=0, Pointer=&(Block) + 4
	        inc hl
	
	        ld b, h
	        ld c, l
	        inc bc
	        inc bc      ; BC = starts of next block
	
	        ld (hl), c
	        inc hl
	        ld (hl), b
	        inc hl      ; Pointer to next block
	
	        ld (hl), e
	        inc hl
	        ld (hl), d
	        inc hl      ; Block size (should be length - 4 at start); This block contains all the available memory
	
	        ld (hl), a ; NULL (0000h) ; No more blocks (a list with a single block)
	        inc hl
	        ld (hl), a
	
	        ld a, 201
	        ld (__MEM_INIT), a; "Pokes" with a RET so ensure this routine is not called again
	        ret
	
	        ENDP
	
#line 69 "free.asm"
	
	; ---------------------------------------------------------------------
	; MEM_FREE
	;  Frees a block of memory
	;
; Parameters:
	;  HL = Pointer to the block to be freed. If HL is NULL (0) nothing
	;  is done
	; ---------------------------------------------------------------------
	
MEM_FREE:
__MEM_FREE: ; Frees the block pointed by HL
	            ; HL DE BC & AF modified
	        PROC
	
	        LOCAL __MEM_LOOP2
	        LOCAL __MEM_LINK_PREV
	        LOCAL __MEM_JOIN_TEST
	        LOCAL __MEM_BLOCK_JOIN
	
	        ld a, h
	        or l
	        ret z       ; Return if NULL pointer
	
	        dec hl
	        dec hl
	        ld b, h
	        ld c, l    ; BC = Block pointer
	
	        ld hl, ZXBASIC_MEM_HEAP  ; This label point to the heap start
	
__MEM_LOOP2:
	        inc hl
	        inc hl     ; Next block ptr
	
	        ld e, (hl)
	        inc hl
	        ld d, (hl) ; Block next ptr
	        ex de, hl  ; DE = &(block->next); HL = block->next
	
	        ld a, h    ; HL == NULL?
	        or l
	        jp z, __MEM_LINK_PREV; if so, link with previous
	
	        or a       ; Clear carry flag
	        sbc hl, bc ; Carry if BC > HL => This block if before
	        add hl, bc ; Restores HL, preserving Carry flag
	        jp c, __MEM_LOOP2 ; This block is before. Keep searching PASS the block
	
	;------ At this point current HL is PAST BC, so we must link (DE) with BC, and HL in BC->next
	
__MEM_LINK_PREV:    ; Link (DE) with BC, and BC->next with HL
	        ex de, hl
	        push hl
	        dec hl
	
	        ld (hl), c
	        inc hl
	        ld (hl), b ; (DE) <- BC
	
	        ld h, b    ; HL <- BC (Free block ptr)
	        ld l, c
	        inc hl     ; Skip block length (2 bytes)
	        inc hl
	        ld (hl), e ; Block->next = DE
	        inc hl
	        ld (hl), d
	        ; --- LINKED ; HL = &(BC->next) + 2
	
	        call __MEM_JOIN_TEST
	        pop hl
	
__MEM_JOIN_TEST:   ; Checks for fragmented contiguous blocks and joins them
	                   ; hl = Ptr to current block + 2
	        ld d, (hl)
	        dec hl
	        ld e, (hl)
	        dec hl     
	        ld b, (hl) ; Loads block length into BC
	        dec hl
	        ld c, (hl) ;
	        
	        push hl    ; Saves it for later
	        add hl, bc ; Adds its length. If HL == DE now, it must be joined
	        or a
	        sbc hl, de ; If Z, then HL == DE => We must join
	        pop hl
	        ret nz
	
__MEM_BLOCK_JOIN:  ; Joins current block (pointed by HL) with next one (pointed by DE). HL->length already in BC
	        push hl    ; Saves it for later
	        ex de, hl
	        
	        ld e, (hl) ; DE -> block->next->length
	        inc hl
	        ld d, (hl)
	        inc hl
	
	        ex de, hl  ; DE = &(block->next)
	        add hl, bc ; HL = Total Length
	
	        ld b, h
	        ld c, l    ; BC = Total Length
	
	        ex de, hl
	        ld e, (hl)
	        inc hl
	        ld d, (hl) ; DE = block->next
	
	        pop hl     ; Recovers Pointer to block
	        ld (hl), c
	        inc hl
	        ld (hl), b ; Length Saved
	        inc hl
	        ld (hl), e
	        inc hl
	        ld (hl), d ; Next saved
	        ret
	
	        ENDP
	
#line 1144 "chrdemo_mc1000_01.bas"
#line 1 "u32tofreg.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "u32tofreg.asm"
__I8TOFREG:
		ld l, a
		rlca
		sbc a, a	; A = SGN(A)
		ld h, a
		ld e, a
		ld d, a
	
__I32TOFREG:	; Converts a 32bit signed integer (stored in DEHL)
					; to a Floating Point Number returned in (A ED CB)
	
		ld a, d
		or a		; Test sign
	
		jp p, __U32TOFREG	; It was positive, proceed as 32bit unsigned
	
		call __NEG32		; Convert it to positive
		call __U32TOFREG	; Convert it to Floating point
	
		set 7, e			; Put the sign bit (negative) in the 31bit of mantissa
		ret
	
__U8TOFREG:
					; Converts an unsigned 8 bit (A) to Floating point
		ld l, a
		ld h, 0
		ld e, h
		ld d, h
	
__U32TOFREG:	; Converts an unsigned 32 bit integer (DEHL)
					; to a Floating point number returned in A ED CB
	
	    PROC
	
	    LOCAL __U32TOFREG_END
	
		ld a, d
		or e
		or h
		or l
	    ld b, d
		ld c, e		; Returns 00 0000 0000 if ZERO
		ret z
	
		push de
		push hl
	
		exx
		pop de  ; Loads integer into B'C' D'E' 
		pop bc
		exx
	
		ld l, 128	; Exponent
		ld bc, 0	; DEBC = 0
		ld d, b
		ld e, c
	
__U32TOFREG_LOOP: ; Also an entry point for __F16TOFREG
		exx
		ld a, d 	; B'C'D'E' == 0 ?
		or e
		or b
		or c
		jp z, __U32TOFREG_END	; We are done
	
		srl b ; Shift B'C' D'E' >> 1, output bit stays in Carry
		rr c
		rr d
		rr e
		exx
	
		rr e ; Shift EDCB >> 1, inserting the carry on the left
		rr d
		rr c
		rr b
	
		inc l	; Increment exponent
		jp __U32TOFREG_LOOP
	
	
__U32TOFREG_END:
		exx
	    ld a, l     ; Puts the exponent in a
		res 7, e	; Sets the sign bit to 0 (positive)
	
		ret
	    ENDP
	
#line 1145 "chrdemo_mc1000_01.bas"
#line 1 "strslice.asm"
	; String slicing library
	; HL = Str pointer
	; DE = String start
	; BC = String character end
	; A register => 0 => the HL pointer wont' be freed from the HEAP
	; e.g. a$(5 TO 10) => HL = a$; DE = 5; BC = 10
	
	; This implements a$(X to Y) being X and Y first and 
	; last characters respectively. If X > Y, NULL is returned
	
	; Otherwise returns a pointer to a$ FROM X to Y (starting from 0)
	; if Y > len(a$), then a$ will be padded with spaces (reallocating
	; it in dynamic memory if needed). Returns pointer (HL) to resulting 
	; string. NULL (0) if no memory for padding.
	;
	
#line 1 "strlen.asm"
	; Returns len if a string
	; If a string is NULL, its len is also 0
	; Result returned in HL
	
__STRLEN:	; Direct FASTCALL entry
			ld a, h
			or l
			ret z
	
			ld a, (hl)
			inc hl
			ld h, (hl)  ; LEN(str) in HL
			ld l, a
			ret
	
	
#line 18 "strslice.asm"
#line 1 "alloc.asm"
; vim: ts=4:et:sw=4:
	; Copyleft (K) by Jose M. Rodriguez de la Rosa
	;  (a.k.a. Boriel) 
;  http://www.boriel.com
	;
	; This ASM library is licensed under the BSD license
	; you can use it for any purpose (even for commercial
	; closed source programs).
	;
	; Please read the BSD license on the internet
	
	; ----- IMPLEMENTATION NOTES ------
	; The heap is implemented as a linked list of free blocks.
	
; Each free block contains this info:
	; 
	; +----------------+ <-- HEAP START 
	; | Size (2 bytes) |
	; |        0       | <-- Size = 0 => DUMMY HEADER BLOCK
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   | <-- If Size > 4, then this contains (size - 4) bytes
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+   |
	;   <Allocated>        | <-- This zone is in use (Already allocated)
	; +----------------+ <-+ 
	; | Size (2 bytes) |
	; +----------------+
	; | Next (2 bytes) |---+
	; +----------------+   |
	; | <free bytes...>|   |
	; | (0 if Size = 4)|   |
	; +----------------+ <-+ 
	; | Next (2 bytes) |--> NULL => END OF LIST
	; |    0 = NULL    |
	; +----------------+
	; | <free bytes...>|
	; | (0 if Size = 4)|
	; +----------------+
	
	
	; When a block is FREED, the previous and next pointers are examined to see
	; if we can defragment the heap. If the block to be freed is just next to the
	; previous, or to the next (or both) they will be converted into a single
	; block (so defragmented).
	
	
	;   MEMORY MANAGER
	;
	; This library must be initialized calling __MEM_INIT with 
	; HL = BLOCK Start & DE = Length.
	
	; An init directive is useful for initialization routines.
	; They will be added automatically if needed.
	
#line 1 "error.asm"
	; Simple error control routines
; vim:ts=4:et:
	
	ERR_NR    EQU    23610    ; Error code system variable
	
	
	; Error code definitions (as in ZX spectrum manual)
	
; Set error code with:
	;    ld a, ERROR_CODE
	;    ld (ERR_NR), a
	
	
	ERROR_Ok                EQU    -1
	ERROR_SubscriptWrong    EQU     2
	ERROR_OutOfMemory       EQU     3
	ERROR_OutOfScreen       EQU     4
	ERROR_NumberTooBig      EQU     5
	ERROR_InvalidArg        EQU     9
	ERROR_IntOutOfRange     EQU    10
	ERROR_InvalidFileName   EQU    14 
	ERROR_InvalidColour     EQU    19
	ERROR_BreakIntoProgram  EQU    20
	ERROR_TapeLoadingErr    EQU    26
	
	
	; Raises error using RST #8
__ERROR:
	    ld (__ERROR_CODE), a
	    rst 8
__ERROR_CODE:
	    nop
	    ret
	
	; Sets the error system variable, but keeps running.
	; Usually this instruction if followed by the END intermediate instruction.
__STOP:
	    ld (ERR_NR), a
	    ret
#line 69 "alloc.asm"
	
	
	
	; ---------------------------------------------------------------------
	; MEM_ALLOC
	;  Allocates a block of memory in the heap.
	;
	; Parameters
	;  BC = Length of requested memory block
	;
; Returns:
	;  HL = Pointer to the allocated block in memory. Returns 0 (NULL)
	;       if the block could not be allocated (out of memory)
	; ---------------------------------------------------------------------
	
MEM_ALLOC:
__MEM_ALLOC: ; Returns the 1st free block found of the given length (in BC)
	        PROC
	
	        LOCAL __MEM_LOOP
	        LOCAL __MEM_DONE
	        LOCAL __MEM_SUBTRACT
	        LOCAL __MEM_START
	        LOCAL TEMP, TEMP0
	
	TEMP EQU TEMP0 + 1
	
	        ld hl, 0
	        ld (TEMP), hl
	
__MEM_START:
	        ld hl, ZXBASIC_MEM_HEAP  ; This label point to the heap start
	        inc bc
	        inc bc  ; BC = BC + 2 ; block size needs 2 extra bytes for hidden pointer
	        
__MEM_LOOP:  ; Loads lengh at (HL, HL+). If Lenght >= BC, jump to __MEM_DONE
	        ld a, h ;  HL = NULL (No memory available?)
	        or l
#line 111 "/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/alloc.asm"
	        ret z ; NULL
#line 113 "/opt/zxbasiccompiler/zxbasiccompiler_zxspectrum/library-asm/alloc.asm"
	        ; HL = Pointer to Free block
	        ld e, (hl)
	        inc hl
	        ld d, (hl)
	        inc hl          ; DE = Block Length
	        
	        push hl         ; HL = *pointer to -> next block
	        ex de, hl
	        or a            ; CF = 0
	        sbc hl, bc      ; FREE >= BC (Length)  (HL = BlockLength - Length)
	        jp nc, __MEM_DONE
	        pop hl
	        ld (TEMP), hl
	
	        ex de, hl
	        ld e, (hl)
	        inc hl
	        ld d, (hl)
	        ex de, hl
	        jp __MEM_LOOP
	        
__MEM_DONE:  ; A free block has been found. 
	             ; Check if at least 4 bytes remains free (HL >= 4)
	        push hl
	        exx  ; exx to preserve bc
	        pop hl
	        ld bc, 4
	        or a
	        sbc hl, bc
	        exx
	        jp nc, __MEM_SUBTRACT
	        ; At this point...
	        ; less than 4 bytes remains free. So we return this block entirely
	        ; We must link the previous block with the next to this one
	        ; (DE) => Pointer to next block
	        ; (TEMP) => &(previous->next)
	        pop hl     ; Discard current block pointer
	        push de
	        ex de, hl  ; DE = Previous block pointer; (HL) = Next block pointer
	        ld a, (hl)
	        inc hl
	        ld h, (hl)
	        ld l, a    ; HL = (HL)
	        ex de, hl  ; HL = Previous block pointer; DE = Next block pointer
TEMP0:
	        ld hl, 0   ; Pre-previous block pointer
	
	        ld (hl), e
	        inc hl
	        ld (hl), d ; LINKED
	        pop hl ; Returning block.
	        
	        ret
	
__MEM_SUBTRACT:
	        ; At this point we have to store HL value (Length - BC) into (DE - 2)
	        ex de, hl
	        dec hl
	        ld (hl), d
	        dec hl
	        ld (hl), e ; Store new block length
	        
	        add hl, de ; New length + DE => free-block start
	        pop de     ; Remove previous HL off the stack
	
	        ld (hl), c ; Store length on its 1st word
	        inc hl
	        ld (hl), b
	        inc hl     ; Return hl
	        ret
	            
	        ENDP
	
	
#line 19 "strslice.asm"
	
	
__STRSLICE:			; Callee entry
		pop hl			; Return ADDRESS
		pop bc			; Last char pos
		pop de			; 1st char pos
		ex (sp), hl		; CALLEE. -> String start
	
__STRSLICE_FAST:	; __FASTCALL__ Entry
		PROC
	
		LOCAL __CONT
		LOCAL __EMPTY
		LOCAL __FREE_ON_EXIT
	
		push hl			; Stores original HL pointer to be recovered on exit
		ex af, af'		; Saves A register for later
	
		push hl
		call __STRLEN
		inc bc			; Last character position + 1 (string starts from 0)	
		or a
		sbc hl, bc		; Compares length with last char position
		jr nc, __CONT	; If Carry => We must copy to end of string
		add hl, bc		; Restore back original LEN(a$) in HL
		ld b, h
		ld c, l			; Copy to the end of str
		ccf				; Clears Carry flag for next subtraction
	
__CONT:
		ld h, b	
		ld l, c			; HL = Last char position to copy (1 for char 0, 2 for char 1, etc)
		sbc hl, de		; HL = LEN(a$) - DE => Number of chars to copy
		jr z, __EMPTY	; 0 Chars to copy => Return HL = 0 (NULL STR)
		jr c, __EMPTY	; If Carry => Nothing to return (NULL STR)
	
		ld b, h
		ld c, l			; BC = Number of chars to copy
		inc bc
		inc bc			; +2 bytes for string length number
	
		push bc
		push de
		call __MEM_ALLOC
		pop de
		pop bc
		ld a, h
		or l
		jr z, __EMPTY	; Return if NULL (no memory)
	
		dec bc
		dec bc			; Number of chars to copy (Len of slice)
	
		ld (hl), c
		inc hl
		ld (hl), b
		inc hl			; Stores new string length
	
		ex (sp), hl		; Pointer to A$ now in HL; Pointer to new string chars in Stack
		inc hl
		inc hl			; Skip string length
		add hl, de		; Were to start from A$
		pop de			; Start of new string chars
		push de			; Stores it again
		ldir			; Copies BC chars
		pop de
		dec de
		dec de			; Points to String LEN start
		ex de, hl		; Returns it in HL
		jr __FREE_ON_EXIT
	
__EMPTY:			; Return NULL (empty) string
		pop hl
		ld hl, 0		; Return NULL
	
	
__FREE_ON_EXIT:
		ex af, af'		; Recover original A register
		ex (sp), hl		; Original HL pointer
	
		or a
		call nz, __MEM_FREE
	
		pop hl			; Recover result
		ret	
		
		ENDP	
	
#line 1146 "chrdemo_mc1000_01.bas"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 1147 "chrdemo_mc1000_01.bas"
#line 1 "mul8.asm"
__MUL8:		; Performs 8bit x 8bit multiplication
		PROC
	
		;LOCAL __MUL8A
		LOCAL __MUL8LOOP
		LOCAL __MUL8B
				; 1st operand (byte) in A, 2nd operand into the stack (AF)
		pop hl	; return address
		ex (sp), hl ; CALLE convention
	
;;__MUL8_FAST: ; __FASTCALL__ entry
	;;	ld e, a
	;;	ld d, 0
	;;	ld l, d
	;;	
	;;	sla h	
	;;	jr nc, __MUL8A
	;;	ld l, e
	;;
;;__MUL8A:
	;;
	;;	ld b, 7
;;__MUL8LOOP:
	;;	add hl, hl
	;;	jr nc, __MUL8B
	;;
	;;	add hl, de
	;;
;;__MUL8B:
	;;	djnz __MUL8LOOP
	;;
	;;	ld a, l ; result = A and HL  (Truncate to lower 8 bits)
	
__MUL8_FAST: ; __FASTCALL__ entry, a = a * h (8 bit mul) and Carry
	
	    ld b, 8
	    ld l, a
	    xor a
	
__MUL8LOOP:
	    add a, a ; a *= 2
	    sla l
	    jp nc, __MUL8B
	    add a, h
	
__MUL8B:
	    djnz __MUL8LOOP
		
		ret		; result = HL
		ENDP
	
#line 1148 "chrdemo_mc1000_01.bas"
#line 1 "band32.asm"
	; FASTCALL bitwise and 32 version.
	; Performs 32bit and 32bit and returns the bitwise
	; result in DE,HL
	; First operand in DE,HL 2nd operand into the stack
	
__BAND32:
	    ld b, h
	    ld c, l ; BC <- HL
	
	    pop hl  ; Return address
	    ex (sp), hl ; HL <- Lower part of 2nd Operand
	
		ld a, b
	    and h
	    ld b, a
	
	    ld a, c
	    and l
	    ld c, a ; BC <- BC & HL
	
		pop hl  ; Return dddress
	    ex (sp), hl ; HL <- High part of 2nd Operand
	
	    ld a, d
	    and h
	    ld d, a
	
	    ld a, e
	    and l
	    ld e, a ; DE <- DE & HL
	
	    ld h, b
	    ld l, c ; HL <- BC  ; Always return DE,HL pair regs
	
	    ret
	
#line 1149 "chrdemo_mc1000_01.bas"
#line 1 "val.asm"
	
#line 1 "stackf.asm"
	; -------------------------------------------------------------
	; Functions to manage FP-Stack of the ZX Spectrum ROM CALC
	; -------------------------------------------------------------
	
	
	__FPSTACK_PUSH EQU 2AB6h	; Stores an FP number into the ROM FP stack (A, ED CB)
	__FPSTACK_POP  EQU 2BF1h	; Pops an FP number out of the ROM FP stack (A, ED CB)
	
__FPSTACK_PUSH2: ; Pushes Current A ED CB registers and top of the stack on (SP + 4)
	                 ; Second argument to push into the stack calculator is popped out of the stack
	                 ; Since the caller routine also receives the parameters into the top of the stack
	                 ; four bytes must be removed from SP before pop them out
	
	    call __FPSTACK_PUSH ; Pushes A ED CB into the FP-STACK
	    exx
	    pop hl       ; Caller-Caller return addr
	    exx
	    pop hl       ; Caller return addr
	
	    pop af
	    pop de
	    pop bc
	
	    push hl      ; Caller return addr
	    exx
	    push hl      ; Caller-Caller return addr
	    exx
	 
	    jp __FPSTACK_PUSH
	
	
__FPSTACK_I16:	; Pushes 16 bits integer in HL into the FP ROM STACK
					; This format is specified in the ZX 48K Manual
					; You can push a 16 bit signed integer as
					; 0 SS LL HH 0, being SS the sign and LL HH the low
					; and High byte respectively
		ld a, h
		rla			; sign to Carry
		sbc	a, a	; 0 if positive, FF if negative
		ld e, a
		ld d, l
		ld c, h
		xor a
		ld b, a
		jp __FPSTACK_PUSH
#line 3 "val.asm"
	
	
VAL: ; Computes VAL(a$) using ROM FP-CALC
		 ; HL = address of a$
		 ; Returns FP number in C ED LH registers
		 ; A Register = 1 => Free a$ on return
		
		PROC
	
		LOCAL STK_STO_S
		LOCAL __RET_ZERO
		LOCAL ERR_SP
		LOCAL STKBOT
		LOCAL RECLAIM1
	    LOCAL CH_ADD
		LOCAL __VAL_ERROR
		LOCAL __VAL_EMPTY
	    LOCAL SET_MIN
	
	RECLAIM1	EQU 6629
	STKBOT		EQU 23651
	ERR_SP		EQU 23613
	CH_ADD      EQU 23645
	STK_STO_S	EQU	2AB2h
	SET_MIN     EQU 16B0h
	
	    ld d, a ; Preserves A register in DE
		ld a, h
		or l
		jr z, __RET_ZERO ; NULL STRING => Return 0 
	
	    push de ; Saves A Register (now in D)
		push hl	; Not null string. Save its address for later
	
		ld c, (hl)
		inc hl
		ld b, (hl)
		inc hl
	
		ld a, b
		or c
		jr z, __VAL_EMPTY ; Jumps VAL_EMPTY on empty string
	
		ex de, hl ; DE = String start
	
	    ld hl, (CH_ADD)
	    push hl
	
		ld hl, (STKBOT)
		push hl
	
		ld hl, (ERR_SP)
		push hl
	
	    ;; Now put our error handler on ERR_SP
		ld hl, __VAL_ERROR
		push hl
		ld hl, 0
		add hl, sp
		ld (ERR_SP), hl
	
		call STK_STO_S ; Enter it on the stack
	
		ld b, 1Dh ; "VAL"
		rst 28h	; ROM CALC
		defb 1Dh ; VAL
		defb 38h ; END CALC
	
		pop hl 	; Discards our current error handler
		pop hl
		ld (ERR_SP), hl	; Restores ERR_SP
	
		pop de	         ; old STKBOT
		ld hl, (STKBOT)  ; current SKTBOT
		call	RECLAIM1 ; Recover unused space
	
	    pop hl  ; Discards old CH_ADD value
		pop hl 	; String pointer
		pop af	; Deletion flag
		or a
		call nz, __MEM_FREE	; Frees string content before returning
	
	    ld a, ERROR_Ok      ; Sets OK in the result
	    ld (ERR_NR), a
	
		jp __FPSTACK_POP	; Recovers result and return from there
	
__VAL_ERROR:	; Jumps here on ERROR
		pop hl
		ld (ERR_SP), hl ; Restores ERR_SP
	
		ld hl, (STKBOT)  ; current SKTBOT
		pop de	; old STKBOT
	    pop hl
	    ld (CH_ADD), hl  ; Recovers old CH_ADD
	
	    call 16B0h       ; Resets temporary areas after an error
	
__VAL_EMPTY:	; Jumps here on empty string
		pop hl      ; Recovers initial string address
	pop af      ; String flag: If not 0 => it's temporary
		or a
		call nz, __MEM_FREE ; Frees "" string
	
__RET_ZERO:	; Returns 0 Floating point on error
		ld a, ERROR_Ok
		ld (ERR_NR), a
	
		xor a
		ld b, a
		ld c, a
		ld d, b
		ld e, c
		ret
	
		ENDP
	
#line 1150 "chrdemo_mc1000_01.bas"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 1151 "chrdemo_mc1000_01.bas"
	
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 1153 "chrdemo_mc1000_01.bas"
#line 1 "ftou32reg.asm"
	
	
__FTOU32REG:	; Converts a Float to (un)signed 32 bit integer (NOTE: It's ALWAYS 32 bit signed)
					; Input FP number in A EDCB (A exponent, EDCB mantissa)
				; Output: DEHL 32 bit number (signed)
		PROC
	
		LOCAL __IS_FLOAT
	
		or a
		jr nz, __IS_FLOAT 
		; Here if it is a ZX ROM Integer
	
		ld h, c
		ld l, d
	ld a, e	 ; Takes sign: FF = -, 0 = +
		ld de, 0
		inc a
		jp z, __NEG32	; Negates if negative
		ret
	
__IS_FLOAT:  ; Jumps here if it is a true floating point number
		ld h, e	
		push hl  ; Stores it for later (Contains Sign in H)
	
		push de
		push bc
	
		exx
		pop de   ; Loads mantissa into C'B' E'D' 
		pop bc	 ; 
	
		set 7, c ; Highest mantissa bit is always 1
		exx
	
		ld hl, 0 ; DEHL = 0
		ld d, h
		ld e, l
	
		;ld a, c  ; Get exponent
		sub 128  ; Exponent -= 128
		jr z, __FTOU32REG_END	; If it was <= 128, we are done (Integers must be > 128)
		jr c, __FTOU32REG_END	; It was decimal (0.xxx). We are done (return 0)
	
		ld b, a  ; Loop counter = exponent - 128
	
__FTOU32REG_LOOP:
		exx 	 ; Shift C'B' E'D' << 1, output bit stays in Carry
		sla d
		rl e
		rl b
		rl c
	
	    exx		 ; Shift DEHL << 1, inserting the carry on the right
		rl l
		rl h
		rl e
		rl d
	
		djnz __FTOU32REG_LOOP
	
__FTOU32REG_END:
		pop af   ; Take the sign bit
		or a	 ; Sets SGN bit to 1 if negative
		jp m, __NEG32 ; Negates DEHL
		
		ret
	
		ENDP
	
	
__FTOU8:	; Converts float in C ED LH to Unsigned byte in A
		call __FTOU32REG
		ld a, l
		ret
	
#line 1154 "chrdemo_mc1000_01.bas"
#line 1 "lti16.asm"
	
#line 1 "lti8.asm"
	
__LTI8: ; Test 8 bit values A < H
        ; Returns result in A: 0 = False, !0 = True
	        sub h
	
__LTI:  ; Signed CMP
	        PROC
	        LOCAL __PE
	
	        ld a, 0  ; Sets default to false
__LTI2:
	        jp pe, __PE
	        ; Overflow flag NOT set
	        ret p
	        dec a ; TRUE
	
__PE:   ; Overflow set
	        ret m
	        dec a ; TRUE
	        ret
	        
	        ENDP
#line 3 "lti16.asm"
	
__LTI16: ; Test 8 bit values HL < DE
        ; Returns result in A: 0 = False, !0 = True
	        xor a
	        sbc hl, de
	        jp __LTI2
	
#line 1155 "chrdemo_mc1000_01.bas"
#line 1 "divf16.asm"
#line 1 "div32.asm"
	
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 2 "divf16.asm"
	
	
__DIVF16:	; 16.16 Fixed point Division (signed)
		
	            ; DE.HL = Dividend, Stack Top = Divisor
	            ; A = Dividend, B = Divisor => A / B
	                exx
	                pop hl   ; return address
	                pop de   ; low part
	                ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
					ex de, hl   ; D'E'.H'L' Dividend
	
__DIVF16START: ; FAST Entry: DEHL => Dividend, D'E'H'L' => Divisor
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with D'E'.H'L'
			ex af, af'
			xor d
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, d ; Negative?
			call nz, __NEG32
			exx		 ; Now we have DE.HL => Dividend
	
			ld b, 16
	
__SHIFTALOOP:		; Tries to shift Dividend to the left
	        bit 7, d
			jp nz, __SHIFTB
	        add hl, hl
			ex de, hl
	        adc hl, hl
			ex de, hl
	        djnz __SHIFTALOOP
	        jp __DOF16_DIVRDY
	
__SHIFTB:       ; Cannot shift Dividend more to the left, try to shift Divisor to the right
	        ld a, b
	        exx
	        ld b, a
	                ; Divisor is in DEHL
__SHIFTBLOOP:
	        bit 1, l
			jp nz, __DOF16_DIVIDE
	        sra d
	        rr e
	        rr h
	        rr l
	        djnz __SHIFTBLOOP
	
__DOF16_DIVIDE:
	        ld a, b
	        exx
	        ld b, a
	
__DOF16_DIVRDY:
			exx
			ex de, hl
	        push bc
			call __DIVU32START
			pop bc
	
			xor a
			or b
			jp z, __ENDF16DIV
	
__SHIFTCLOOP:
			add hl, hl	; Shift DECIMAL PART << 1
			ex de, hl		
			adc hl, hl  ; Shift INTEGER PART << 1 Plus Carry
			ex de, hl		
			djnz __SHIFTCLOOP
	
__ENDF16DIV: 	   ; Put the sign on the result
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
			jp __NEG32 ; Negates DEHL and returns from there
	
#line 1156 "chrdemo_mc1000_01.bas"
	
ZXBASIC_USER_DATA:
_tadrv2:
	DEFB 00, 00
_p1:
	DEFB 00, 00
_p2:
	DEFB 00, 00
_s1:
	DEFB 00, 00
_s2:
	DEFB 00, 00
_vl:
	DEFB 00, 00
_rseed:
	DEFB 00, 00
ZXBASIC_MEM_HEAP:
	; Defines DATA END
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP + ZXBASIC_HEAP_SIZE
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
