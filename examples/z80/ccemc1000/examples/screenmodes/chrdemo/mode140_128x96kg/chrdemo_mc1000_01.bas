#include "library/mc1000screen.zxi"
#include "library/mc1000cls.zxi"
#include "library/mc1000vpokechr.zxi"
#include "library/charmap03_pm30n27e8.zxi"
#include "library/mc1000rnd.zxi"

mc1000screen(140)
mc1000cls(2)

dim p1,p2,s1,s2,vl as integer

dim rseed as uinteger
rseed=50

rseed=mc1000rnd(rseed) band 255
p1=int(rseed*3)
rseed=mc1000rnd(rseed) band 255
s1=int(rseed/8)+8

rseed=mc1000rnd(rseed) band 255
p2=int(rseed*3)
rseed=mc1000rnd(rseed) band 255
s2=int(rseed/8)+8

lp01:
  rseed=mc1000rnd(rseed)
  vl= (128+int(rseed/1.6))band 255

  mc1000vpokechr64wed(p1,vl,@charmap01)
  p1=(p1+1)mod 192
  s1=s1-1
  if s1<1 then: 
    rseed=mc1000rnd(rseed) band 255
    p1=int(rseed*3)
    rseed=mc1000rnd(rseed) band 255
    s1=int(rseed/8)+8
    end if
  mc1000vpokechr64wed(p2,32,@charmap01)
  p2=(p2+1)mod 192
  s2=s2-1
  if s2<1 then:
    rseed=mc1000rnd(rseed) band 255
    p2=int(rseed*3)
    rseed=mc1000rnd(rseed) band 255
    s2=int(rseed/8)+8
    end if

  goto lp01


