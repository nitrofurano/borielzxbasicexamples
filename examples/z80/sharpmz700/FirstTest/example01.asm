	org 4608
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		ld sp,$CFFF
#line 1
	ld hl, 0
	ld (_seed), hl
	ld hl, 1000
	push hl
	ld hl, 90
	push hl
	ld hl, 53248
	push hl
	call _aquariusfillbytes
	ld hl, 1000
	push hl
	ld hl, 21
	push hl
	ld hl, 55296
	push hl
	call _aquariusfillbytes
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL0
__LABEL3:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL5
__LABEL8:
	ld hl, (_ee1)
	ld de, 40
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_ee1)
	ld de, 40
	call __MUL16_FAST
	ld de, 55296
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	push hl
	ld a, 7
	pop hl
	ld (hl), a
	ld hl, (_ee1)
	ld de, 8
	add hl, de
	ld de, 40
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_ee1)
	ld de, 8
	add hl, de
	ld de, 40
	call __MUL16_FAST
	ld de, 55296
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	push hl
	ld a, 135
	pop hl
	ld (hl), a
__LABEL9:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL5:
	ld hl, 31
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
__LABEL4:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL0:
	ld hl, 7
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL10
__LABEL13:
	ld hl, 0
	ld (_ee2), hl
	jp __LABEL15
__LABEL18:
	ld hl, (_ee1)
	ld de, 40
	call __MUL16_FAST
	ld de, 53248
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld de, 32
	add hl, de
	push hl
	ld a, 71
	pop hl
	ld (hl), a
	ld hl, (_ee1)
	ld de, 40
	call __MUL16_FAST
	ld de, 55296
	add hl, de
	ex de, hl
	ld hl, (_ee2)
	add hl, de
	ld de, 32
	add hl, de
	push hl
	ld hl, (_ee2)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
__LABEL19:
	ld hl, (_ee2)
	inc hl
	ld (_ee2), hl
__LABEL15:
	ld hl, 7
	ld de, (_ee2)
	or a
	sbc hl, de
	jp nc, __LABEL18
__LABEL17:
__LABEL14:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL10:
	ld hl, 7
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL13
__LABEL12:
__LABEL20:
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53645), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53644), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53643), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 1
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53685), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53684), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53683), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 2
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53725), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53724), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53723), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 3
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53765), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53764), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53763), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 4
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53805), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53804), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53803), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 5
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53845), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53844), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53843), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 6
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53885), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53884), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53883), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 7
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53925), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53924), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53923), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 8
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (53965), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53964), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (53963), a
	ld a, (65520)
	push af
	ld h, 240
	pop af
	and h
	push af
	ld h, 9
	pop af
	or h
	ld (65520), a
	ld a, (65521)
	cpl
	ld l, a
	ld h, 0
	ld (_ee0), hl
	ld de, 10
	call __MODU16
	ld de, 32
	add hl, de
	ld a, l
	ld (54005), a
	ld hl, (_ee0)
	ld de, 10
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (54004), a
	ld hl, (_ee0)
	ld de, 100
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 32
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	ld (54003), a
	jp __LABEL20
__LABEL21:
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_smsrnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 19
_smsrnd__leave:
	ret
_smsdelay:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld b, (ix+5)
		ld c, (ix+4)
smsdelayloop:
		dec bc
		ld a,b
		or c
		jp nz,smsdelayloop
#line 8
_smsdelay__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_aquariusfillbytes:
	push ix
	ld ix, 0
	add ix, sp
#line 2
		ld b, (ix+6)
		ld d, (ix+9)
		ld e, (ix+8)
		ld h, (ix+5)
		ld l, (ix+4)
fillbytes:
		ld (hl),b
		inc hl
		dec de
		ld a,d
		or e
		jr nz,fillbytes
#line 14
_aquariusfillbytes__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 915 "example01.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 916 "example01.bas"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 917 "example01.bas"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 918 "example01.bas"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 919 "example01.bas"
	
ZXBASIC_USER_DATA:
	_ee0 EQU 51216
	_seed EQU 51218
	_ee1 EQU 51220
	_ee2 EQU 51222
	_ee3 EQU 51224
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
