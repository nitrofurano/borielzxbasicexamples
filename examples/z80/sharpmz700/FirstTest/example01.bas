asm
  ld sp,$CFFF
  end asm

#include "library/smsrnd.bas"
#include "library/smsdelay.bas"
#include "library/aquariusfillbytes.bas"

dim ee0 as uinteger at $C810
dim seed as uinteger at $C812
dim ee1 as uinteger at $C814
dim ee2 as uinteger at $C816
dim ee3 as uinteger at $C818

seed=0

aquariusfillbytes($D000,64+26,1000)
aquariusfillbytes($D800,$15,1000)

for ee1=0 to 7
  for ee2=0 to 31
    poke $D000+ee1*40+ee2,(ee1*32+ee2)
    poke $D800+ee1*40+ee2,$07
    poke $D000+(ee1+8)*40+ee2,(ee1*32+ee2)
    poke $D800+(ee1+8)*40+ee2,$87
    next:next

for ee1=0 to 7
  for ee2=0 to 7
    poke $D000+ee1*40+ee2+32,64+7
    poke $D800+ee1*40+ee2+32,ee2*16+ee1
    next:next

do
  poke $FFF0,(peek ($FFF0)band $F0) bor 0
  ee0=peek($FFF1) bxor 255
  poke $D000+9*40+37,32+(ee0 mod 10)
  poke $D000+9*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+9*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 1
  ee0=peek($FFF1) bxor 255
  poke $D000+10*40+37,32+(ee0 mod 10)
  poke $D000+10*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+10*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 2
  ee0=peek($FFF1) bxor 255
  poke $D000+11*40+37,32+(ee0 mod 10)
  poke $D000+11*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+11*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 3
  ee0=peek($FFF1) bxor 255
  poke $D000+12*40+37,32+(ee0 mod 10)
  poke $D000+12*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+12*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 4
  ee0=peek($FFF1) bxor 255
  poke $D000+13*40+37,32+(ee0 mod 10)
  poke $D000+13*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+13*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 5
  ee0=peek($FFF1) bxor 255
  poke $D000+14*40+37,32+(ee0 mod 10)
  poke $D000+14*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+14*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 6
  ee0=peek($FFF1) bxor 255
  poke $D000+15*40+37,32+(ee0 mod 10)
  poke $D000+15*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+15*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 7
  ee0=peek($FFF1) bxor 255
  poke $D000+16*40+37,32+(ee0 mod 10)
  poke $D000+16*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+16*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 8
  ee0=peek($FFF1) bxor 255
  poke $D000+17*40+37,32+(ee0 mod 10)
  poke $D000+17*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+17*40+35,32+((int(ee0/100)) mod 10)

  poke $FFF0,(peek ($FFF0)band $F0) bor 9
  ee0=peek($FFF1) bxor 255
  poke $D000+18*40+37,32+(ee0 mod 10)
  poke $D000+18*40+36,32+((int(ee0/10)) mod 10)
  poke $D000+18*40+35,32+((int(ee0/100)) mod 10)

  loop


