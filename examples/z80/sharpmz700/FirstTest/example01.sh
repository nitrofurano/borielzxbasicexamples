dd bs=$((0x10000)) count=1 if=/dev/zero of=_dummy64k.bin
rm _header_.bin
echo -n -e '\x01' >> _header_.bin
echo -n 'example01' >> _header_.bin #filename doesn't fix in mz700 text enconding?
dd ibs=1 count=15 skip=$((0x0000)) if=_header_.bin of=_header1_.bin
echo -n -e '\x0D' >> _header1_.bin
cat _dummy64k.bin >> _header1_.bin
dd ibs=1 count=16 skip=$((0x0000)) if=_header1_.bin of=_header2_.bin
echo -n -e '\x00\x00' >> _header2_.bin
echo -n -e '\x00\x08' >> _header2_.bin  #size @ 0x0800
echo -n -e '\x00\x12' >> _header2_.bin  #start @ 0x1200
echo -n -e '\x00\x12' >> _header2_.bin  #exec @ 0x1200
cat _dummy64k.bin >> _header2_.bin
dd ibs=1 count=$((0x0080)) skip=$((0x0000)) if=_header2_.bin of=_header3_.bin
zxb.py example01.bas --asm --org=$((0x1200))
zxb.py example01.bas --org=$((0x1200))
cat example01.bin >> _header3_.bin
cat _dummy64k.bin >> _header3_.bin
dd ibs=1 count=$((0x0880)) skip=$((0x0000)) if=_header3_.bin of=_header4_.bin #0x0880 is 0x0800+0x0080
rm _header_.bin _header1_.bin _header2_.bin _header3_.bin _dummy64k.bin example01.bin
mv _header4_.bin example01.mzf

mame mz700j -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cass1 example01.mzf

# enter 'L' for loading


