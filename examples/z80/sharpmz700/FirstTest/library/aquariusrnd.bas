function fastcall aquariusrnd(tseed as uinteger) as uinteger:
  asm
    ld d,h
    ld e,l
    ld  a,e         ; w = w ^ ( w << 3 )
    add a,a
    add a,a
    add a,a
    xor e
    ld  e,a
    ld  a,h         ; t = x ^ (x << 1)
    add a,a
    xor h
    ld  d,a
    rra             ; t = t ^ (t >> 1) ^ w
    xor d
    xor e
    ld  h,l         ; y = z
    ld  l,a         ; w = t
    end asm
  end function
