function fastcall aquariuskeychk() as ubyte:
  asm
    xor a        ; set a to zero
    ld ($380b),a ; kwaddr
    ld ($380c),a
    ld ($380e),a ; lastkey
    ld ($380f),a ; scancnt
    call $1e80   ; call keychck
                 ; a holds result
    end asm
  end function

'- http://atariage.com/forums/topic/214096-aquarius-reading-the-keymap-io-port-in-assembly/
