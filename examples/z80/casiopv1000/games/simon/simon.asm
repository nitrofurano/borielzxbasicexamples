	org 105
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 4
		ld sp,$BFFF
#line 5
	ld a, 63
	ld bc, 248
	out (c), a
	ld a, 63
	ld bc, 249
	out (c), a
	ld a, 63
	ld bc, 250
	out (c), a
	ld hl, 1
	ld (_hisc), hl
	ld a, 1
	ld (_cnt), a
	call __LABEL__650
	call __LABEL__670
	ld a, 32
	push af
	ld hl, 24
	push hl
	ld hl, 32
	push hl
	ld hl, 0
	push hl
	ld hl, 0
	push hl
	call _pv1000filltile
__LABEL__10:
	ld a, 32
	push af
	ld hl, 4
	push hl
	ld hl, 8
	push hl
	ld hl, 10
	push hl
	ld hl, 12
	push hl
	call _pv1000filltile
	ld hl, (_seed)
	call _pv1000rnd
	ld (_seed), hl
	ld de, 255
	ld hl, (_seed)
	call __BAND16
	ld (_rlc), hl
	call __LABEL__600
	ld hl, 1
	ld (_i), hl
	jp __LABEL0
__LABEL3:
__LABEL4:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL0:
	ld hl, 25000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL3
__LABEL2:
__LABEL__20:
	ld a, 1
	ld (_cnt), a
__LABEL__100:
	ld hl, 1
	ld (_n), hl
	jp __LABEL5
__LABEL8:
	ld de, (_rlc)
	ld hl, (_n)
	add hl, de
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ex de, hl
	ld hl, __LABEL__urandomdata01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld h, 3
	pop af
	and h
	ld l, a
	ld h, 0
	ld (_a), hl
	ld de, 0
	ld hl, (_a)
	call __EQ16
	or a
	jp z, __LABEL11
	call __LABEL__500
__LABEL11:
	ld de, 1
	ld hl, (_a)
	call __EQ16
	or a
	jp z, __LABEL13
	call __LABEL__510
__LABEL13:
	ld de, 2
	ld hl, (_a)
	call __EQ16
	or a
	jp z, __LABEL15
	call __LABEL__520
__LABEL15:
	ld de, 3
	ld hl, (_a)
	call __EQ16
	or a
	jp z, __LABEL17
	call __LABEL__530
__LABEL17:
__LABEL9:
	ld hl, (_n)
	inc hl
	ld (_n), hl
__LABEL5:
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ld de, (_n)
	or a
	sbc hl, de
	jp nc, __LABEL8
__LABEL7:
	call __LABEL__600
	ld hl, 1
	ld (_n), hl
__LABEL__150:
	ld a, 4
	push af
	call _pv1000joystick
	push af
	ld h, 2
	pop af
	and h
	srl a
	ld l, a
	ld h, 0
	ld (_jacum), hl
	ld a, 2
	push af
	call _pv1000joystick
	push af
	ld h, 1
	pop af
	and h
	add a, a
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 4
	push af
	call _pv1000joystick
	push af
	ld h, 1
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 2
	push af
	call _pv1000joystick
	push af
	ld h, 2
	pop af
	and h
	add a, a
	add a, a
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 4
	push af
	call _pv1000joystick
	push af
	ld h, 8
	pop af
	and h
	ld h, 8
	call __DIVU8_FAST
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 2
	push af
	call _pv1000joystick
	push af
	ld h, 4
	pop af
	and h
	srl a
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 4
	push af
	call _pv1000joystick
	push af
	ld h, 4
	pop af
	and h
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld a, 2
	push af
	call _pv1000joystick
	push af
	ld h, 8
	pop af
	and h
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_jacum)
	call __BOR16
	ld (_jacum), hl
	ld de, 0
	ld hl, (_jacum)
	call __EQ16
	or a
	jp z, __LABEL19
	jp __LABEL__150
__LABEL19:
	ld de, 1
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL21
	xor a
	ld (_c), a
	call __LABEL__500
__LABEL21:
	ld de, 2
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL23
	ld a, 1
	ld (_c), a
	call __LABEL__510
__LABEL23:
	ld de, 4
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL25
	ld a, 2
	ld (_c), a
	call __LABEL__520
__LABEL25:
	ld de, 8
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL27
	ld a, 3
	ld (_c), a
	call __LABEL__530
__LABEL27:
__LABEL__175:
	ld de, 15
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	call __EQ16
	or a
	jp z, __LABEL29
	jp __LABEL__175
__LABEL29:
	ld de, (_rlc)
	ld hl, (_n)
	add hl, de
	push hl
	ld de, 255
	pop hl
	call __BAND16
	ex de, hl
	ld hl, __LABEL__urandomdata01
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	push af
	ld h, 3
	pop af
	and h
	ld l, a
	ld h, 0
	ld (_a), hl
	ld a, (_c)
	ld l, a
	ld h, 0
	push hl
	ld de, (_a)
	pop hl
	call __EQ16
	push af
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_n)
	or a
	sbc hl, de
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL31
	ld hl, (_n)
	inc hl
	ld (_n), hl
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_hisc)
	or a
	sbc hl, de
	jp nc, __LABEL33
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ld (_hisc), hl
__LABEL33:
	jp __LABEL__150
__LABEL31:
	ld a, (_c)
	ld l, a
	ld h, 0
	push hl
	ld de, (_a)
	pop hl
	call __EQ16
	push af
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_n)
	or a
	sbc hl, de
	ccf
	sbc a, a
	ld h, a
	pop af
	call __AND8
	or a
	jp z, __LABEL35
	ld a, (_cnt)
	inc a
	ld (_cnt), a
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_hisc)
	or a
	sbc hl, de
	jp nc, __LABEL37
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ld (_hisc), hl
__LABEL37:
	call __LABEL__400
	ld a, (_cnt)
	sub 251
	sub 1
	jp nc, __LABEL39
	jp __LABEL__10
__LABEL39:
	ld a, (_cnt)
	sub 251
	or a
	jp z, __LABEL41
	jp __LABEL__100
__LABEL41:
__LABEL35:
	jp __LABEL__flg01
__LABEL__text01:
#line 121
		defb "GAME OVER"
#line 122
__LABEL__text02:
#line 125
	defb "SCORE:"
#line 126
__LABEL__text03:
#line 129
		defb "TRY AGAIN?"
#line 130
__LABEL__text04:
#line 133
	defb "HISCORE:"
#line 134
__LABEL__text05:
	jp __LABEL__flg01
#line 141
		org $400
#line 142
__LABEL__charmap01:
#line 1
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00100100
		defb %00100100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00100100
		defb %00100100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00100100
		defb %00111100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00011000
		defb %00011111
		defb %00110100
		defb %01101100
		defb %01001000
		defb %00000000
		defb %00000000
		defb %00001001
		defb %00001011
		defb %00110110
		defb %00100100
		defb %01111110
		defb %01011000
		defb %00000000
		defb %00000000
		defb %00000110
		defb %00111001
		defb %00011111
		defb %00010110
		defb %11101000
		defb %01010000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00100100
		defb %01111110
		defb %01111000
		defb %00111100
		defb %00111110
		defb %01111100
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01011110
		defb %01011000
		defb %00011010
		defb %01011010
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00100001
		defb %00101010
		defb %00011100
		defb %10011010
		defb %01100100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00100000
		defb %01010110
		defb %01101000
		defb %00011000
		defb %00000110
		defb %00100110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010100
		defb %00001000
		defb %00010000
		defb %00111010
		defb %01101000
		defb %00000000
		defb %00000000
		defb %01100110
		defb %10110110
		defb %01111100
		defb %00011000
		defb %00000101
		defb %01000111
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000100
		defb %00100110
		defb %00011100
		defb %01111010
		defb %01000100
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00100110
		defb %00111000
		defb %01001000
		defb %01000110
		defb %00111010
		defb %00000000
		defb %00000000
		defb %00000100
		defb %00110100
		defb %00110100
		defb %01111010
		defb %11000100
		defb %01000110
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00100000
		defb %00111000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00011000
		defb %00110000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00101000
		defb %00101000
		defb %00000000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00001100
		defb %00011000
		defb %00010000
		defb %00010000
		defb %00011000
		defb %00000100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00001100
		defb %00000000
		defb %00010000
		defb %00010000
		defb %00011000
		defb %00000100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00110000
		defb %00011000
		defb %00001000
		defb %00001000
		defb %00011000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00110000
		defb %00000000
		defb %00001000
		defb %00001000
		defb %00011000
		defb %00100000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %01011010
		defb %01011010
		defb %00000000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00100100
		defb %01111110
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01011010
		defb %00000000
		defb %10000001
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01111110
		defb %01111110
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100110
		defb %01100110
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00111000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00111000
		defb %00010000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00101000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01111110
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01111110
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00100000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00110000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00101000
		defb %00011000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000110
		defb %00001000
		defb %00011000
		defb %00000000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000100
		defb %00001000
		defb %00010000
		defb %00110000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000100
		defb %00000110
		defb %00001100
		defb %00011000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000100
		defb %00100110
		defb %01100110
		defb %01100110
		defb %00100110
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00100100
		defb %01100110
		defb %01100110
		defb %01100100
		defb %00111000
		defb %00000000
		defb %00000000
		defb %00000100
		defb %00101010
		defb %01100110
		defb %01100110
		defb %00100110
		defb %00000100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00001000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00111000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %01001000
		defb %00011000
		defb %00011000
		defb %00100100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000100
		defb %00000110
		defb %00000110
		defb %00011100
		defb %00010000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00111000
		defb %01000110
		defb %00000110
		defb %00001000
		defb %00100000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %01000100
		defb %00001000
		defb %00000100
		defb %00010100
		defb %01010000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00100000
		defb %01000110
		defb %00011110
		defb %00011110
		defb %01000010
		defb %01111000
		defb %00000000
		defb %00000000
		defb %00011100
		defb %00000010
		defb %00000110
		defb %00000010
		defb %00000010
		defb %00111100
		defb %00000000
		defb %00000000
		defb %00100010
		defb %01000110
		defb %00011011
		defb %00000110
		defb %11000010
		defb %01000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00001000
		defb %00011100
		defb %00010100
		defb %01100100
		defb %00001100
		defb %00000100
		defb %00000000
		defb %00000000
		defb %00000100
		defb %00000100
		defb %00000100
		defb %00000100
		defb %01111110
		defb %00000100
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00011100
		defb %00100100
		defb %01100100
		defb %00110111
		defb %00000100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000010
		defb %00100000
		defb %01111110
		defb %00000010
		defb %01000010
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00011100
		defb %00100000
		defb %01100100
		defb %00000010
		defb %00000010
		defb %00100100
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000000
		defb %00011010
		defb %00000111
		defb %11000011
		defb %01011010
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %01100000
		defb %01011100
		defb %01011110
		defb %01000010
		defb %01111000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00100000
		defb %01000000
		defb %01000010
		defb %01000010
		defb %00111100
		defb %00000000
		defb %00000000
		defb %00001000
		defb %01110000
		defb %01011100
		defb %01100110
		defb %11100010
		defb %01000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %01000010
		defb %00000100
		defb %00000100
		defb %00001000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00111110
		defb %00000010
		defb %00000100
		defb %00000000
		defb %00001000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %01000001
		defb %01000010
		defb %00000100
		defb %00001100
		defb %00001000
		defb %00011000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00101100
		defb %01100110
		defb %00000100
		defb %00110100
		defb %01100110
		defb %00011100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100110
		defb %00110000
		defb %00001000
		defb %01100110
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00011100
		defb %00100100
		defb %00000100
		defb %00100100
		defb %01000010
		defb %00011100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00101000
		defb %01000110
		defb %01100110
		defb %00000010
		defb %00000100
		defb %01111000
		defb %00000000
		defb %00000000
		defb %00000100
		defb %01000010
		defb %01000010
		defb %00111010
		defb %00000110
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00111010
		defb %11000110
		defb %01100110
		defb %01000011
		defb %00000100
		defb %00101100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00100000
		defb %00100000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00110000
		defb %00010000
		defb %00010000
		defb %00110000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00101000
		defb %00101000
		defb %00011000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00011000
		defb %00010000
		defb %00111000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00110000
		defb %00000000
		defb %00111000
		defb %00010000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00110000
		defb %00011000
		defb %00000000
		defb %00010000
		defb %00101000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000110
		defb %01001000
		defb %01001000
		defb %00000110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00110000
		defb %00110000
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001100
		defb %01000000
		defb %01000000
		defb %00001100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01111110
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000001
		defb %10000001
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %01100000
		defb %00010010
		defb %00010010
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00001100
		defb %00001100
		defb %01000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00110000
		defb %00000010
		defb %00000010
		defb %00110000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00100100
		defb %00100110
		defb %00000110
		defb %00001000
		defb %00010000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01000110
		defb %00000100
		defb %00010000
		defb %00000000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00100100
		defb %01111000
		defb %00001010
		defb %00001000
		defb %00010000
		defb %00010000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00011100
		defb %01100010
		defb %01011100
		defb %01000010
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000010
		defb %01010100
		defb %01010100
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01111100
		defb %01111110
		defb %01011001
		defb %11001001
		defb %00011110
		defb %00011110
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00011000
		defb %00100000
		defb %01111110
		defb %00000110
		defb %11000111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00000100
		defb %00100100
		defb %01000010
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00111100
		defb %00111000
		defb %01111110
		defb %00000110
		defb %11000101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111000
		defb %01000110
		defb %01000110
		defb %01111100
		defb %01000010
		defb %11111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000100
		defb %01000100
		defb %01000010
		defb %01000010
		defb %01000100
		defb %00000000
		defb %00000000
		defb %10111000
		defb %11000110
		defb %01001010
		defb %01010100
		defb %01000011
		defb %10111010
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011100
		defb %01100000
		defb %01000000
		defb %01000000
		defb %01000000
		defb %00011110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %01000000
		defb %01000000
		defb %01000000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00011110
		defb %01100000
		defb %01000000
		defb %01000000
		defb %01100000
		defb %00011110
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111000
		defb %01000110
		defb %01000010
		defb %01000010
		defb %01000010
		defb %11111100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000110
		defb %01000010
		defb %01000010
		defb %01000010
		defb %01000000
		defb %00000000
		defb %00000000
		defb %10111000
		defb %11001000
		defb %01000010
		defb %01000010
		defb %01000010
		defb %10111100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %01111100
		defb %01100010
		defb %01100100
		defb %01111000
		defb %01100000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100010
		defb %01100000
		defb %01100100
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00011110
		defb %01100000
		defb %01100100
		defb %01100000
		defb %01100000
		defb %00011111
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %01111100
		defb %01100010
		defb %01100100
		defb %01111000
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100010
		defb %01100000
		defb %01100100
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00011110
		defb %01100000
		defb %01100100
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011100
		defb %01100000
		defb %01000000
		defb %01000111
		defb %01000010
		defb %00011110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %01000000
		defb %01000000
		defb %01000010
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00011110
		defb %01100000
		defb %01000000
		defb %01000111
		defb %01100010
		defb %00011110
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11100111
		defb %01000010
		defb %01000010
		defb %01111110
		defb %01000010
		defb %11100111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000010
		defb %01000010
		defb %01000010
		defb %01000010
		defb %01000010
		defb %00000000
		defb %00000000
		defb %10000001
		defb %11000011
		defb %01000010
		defb %01011010
		defb %01000010
		defb %10100101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00111100
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00111100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00100100
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00100100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00111100
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %01000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00010000
		defb %00100000
		defb %00100100
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %01000000
		defb 0,0,0,0,0,0,0,0
		defb %11100010
		defb %01000000
		defb %01100000
		defb %01011000
		defb %01011000
		defb %11100111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01001100
		defb %01010000
		defb %01110000
		defb %01001100
		defb %01000010
		defb %00000000
		defb %00000000
		defb %10000110
		defb %11000000
		defb %01100000
		defb %01001000
		defb %01010010
		defb %10100101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %01100000
		defb %00011111
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11000011
		defb %01100110
		defb %00100110
		defb %00000010
		defb %00010010
		defb %11010111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000110
		defb %01100010
		defb %00110010
		defb %00011010
		defb %00010010
		defb %00000000
		defb %00000000
		defb %10000000
		defb %11100011
		defb %10001110
		defb %11001010
		defb %11010010
		defb %11000101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000011
		defb %01100000
		defb %01000000
		defb %00011000
		defb %00000100
		defb %11000001
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00110000
		defb %00001000
		defb %00000110
		defb %00000010
		defb %00000000
		defb %00000000
		defb %10000011
		defb %11100011
		defb %10000011
		defb %11011011
		defb %11000101
		defb %11000001
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011000
		defb %01100110
		defb %01000010
		defb %01000010
		defb %01000010
		defb %00011100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000110
		defb %01000010
		defb %01000010
		defb %01000010
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01101000
		defb %01000010
		defb %01000010
		defb %01100010
		defb %00011100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %01111100
		defb %01100110
		defb %01100110
		defb %01101100
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01100010
		defb %01100010
		defb %01100000
		defb %01100000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %01100101
		defb %01100101
		defb %01011000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011000
		defb %01100110
		defb %01000010
		defb %01000010
		defb %01100010
		defb %00011100
		defb %01000111
		defb %00000000
		defb %00000000
		defb %00000110
		defb %01000010
		defb %01000010
		defb %01000010
		defb %00111000
		defb %00001100
		defb %00000000
		defb %00011000
		defb %01101000
		defb %01000010
		defb %01000010
		defb %01100010
		defb %00111100
		defb %01110111
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111000
		defb %01000110
		defb %01000110
		defb %01010000
		defb %01011000
		defb %11100111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000100
		defb %01000100
		defb %01111000
		defb %01001100
		defb %01000010
		defb %00000000
		defb %00000000
		defb %10111000
		defb %11001010
		defb %01000110
		defb %01011000
		defb %01010010
		defb %10100101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00111110
		defb %01100000
		defb %01110000
		defb %00100010
		defb %00000110
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000010
		defb %01100000
		defb %00011100
		defb %00000010
		defb %01000100
		defb %00000000
		defb %00000000
		defb %00011100
		defb %10100001
		defb %10010000
		defb %00000110
		defb %10000111
		defb %00111010
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %01111110
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00111100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %01100110
		defb %11011011
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00100100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11100011
		defb %01000000
		defb %01000000
		defb %01000000
		defb %01000010
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %01000000
		defb %01000000
		defb %01000000
		defb %00111100
		defb %00000000
		defb %00000000
		defb %10000010
		defb %11000011
		defb %01000011
		defb %01000011
		defb %11100011
		defb %01000010
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11000110
		defb %01100110
		defb %01100100
		defb %00001000
		defb %00010000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00100000
		defb %00100000
		defb %00011000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10100110
		defb %01001100
		defb %00001100
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11110111
		defb %01011000
		defb %01011010
		defb %01101110
		defb %01100100
		defb %00100100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01011000
		defb %01111000
		defb %01101010
		defb %00101100
		defb %00100100
		defb %00000000
		defb %00000000
		defb %10101000
		defb %11011011
		defb %01100011
		defb %00011110
		defb %01100100
		defb %00100100
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11100011
		defb %01100010
		defb %00000000
		defb %00001000
		defb %01000110
		defb %10000111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00110100
		defb %00011000
		defb %00100100
		defb %01000010
		defb %00000000
		defb %00000000
		defb %00000010
		defb %11100011
		defb %00000000
		defb %00001000
		defb %00000110
		defb %10000101
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11100011
		defb %01100010
		defb %00000000
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00110100
		defb %00011000
		defb %00011000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000011
		defb %10100011
		defb %00000000
		defb %00001000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00111110
		defb %01000110
		defb %00000000
		defb %00001000
		defb %00110000
		defb %01111110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000110
		defb %00001100
		defb %00011000
		defb %00010000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %01111000
		defb %01000100
		defb %00000000
		defb %00001000
		defb %00100000
		defb %01011111
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00011100
		defb %00000000
		defb %00000000
		defb %00011100
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00011100
		defb %00000000
		defb %00000000
		defb %00100000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00010000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %01100000
		defb %00010000
		defb %00011000
		defb %00000000
		defb %00000110
		defb %00000000
		defb %00000000
		defb %01000000
		defb %00100000
		defb %00010000
		defb %00001000
		defb %00001100
		defb %00000110
		defb %00000000
		defb %00000000
		defb %00100000
		defb %01100000
		defb %00110000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00111000
		defb %00000000
		defb %00000000
		defb %00111000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00111000
		defb %00000000
		defb %00000000
		defb %00000100
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %01100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00010000
		defb %00101100
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %01000000
		defb %01000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000011
		defb %00001111
		defb %00111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000011
		defb %00001111
		defb %00111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00100010
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000011
		defb %00001111
		defb %00111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000011
		defb %00001111
		defb %00111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00001111
		defb %00111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00100010
		defb %00100010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00001111
		defb %00111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %00000000
		defb %00001000
		defb %00100010
		defb %00100010
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00001111
		defb %00111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00001010
		defb %00111011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00001010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %00000010
		defb %00001111
		defb %00111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %00000010
		defb %00001010
		defb %00111011
		defb %10101010
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000010
		defb %00000000
		defb %00000010
		defb %00000010
		defb %00001010
		defb %00000000
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000110
		defb %00000111
		defb %00001011
		defb %00001111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb %10000000
		defb %10100000
		defb %00100000
		defb %10100000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %10110000
		defb %11110000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000011
		defb %00000010
		defb %00000100
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00000000
		defb %00000001
		defb %00000011
		defb %00000010
		defb %00000110
		defb %00000011
		defb %00001011
		defb %00001110
		defb %00000001
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000110
		defb %00000111
		defb %00001111
		defb %00001111
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10100000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11100000
		defb %10100000
		defb %10110000
		defb %11100000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %10110000
		defb %11110000
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000111
		defb %00000111
		defb %00001111
		defb %00001111
		defb %00000000
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000110
		defb %00000111
		defb %00001011
		defb %00001111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %11110000
		defb %11110000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %10110000
		defb %11110000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000111
		defb %00000111
		defb %00001111
		defb %00001111
		defb %00000001
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00000111
		defb %00000111
		defb %00001111
		defb %00001111
		defb %00000000
		defb %00000000
		defb %00000011
		defb %00000010
		defb %00000100
		defb %00000010
		defb %00000011
		defb %00001010
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %11110000
		defb %11110000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %11000000
		defb %11100000
		defb %11100000
		defb %11110000
		defb %11110000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10100000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %00111111
		defb %00001111
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00111111
		defb %00001111
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %00111111
		defb %00001111
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00111111
		defb %00001111
		defb %00000011
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %00111111
		defb %00001011
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00001000
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %00111111
		defb %00001011
		defb %00000011
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb %00001000
		defb %00001000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %00111111
		defb %00001111
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %00101010
		defb %00001011
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %00101010
		defb %00000011
		defb %00000010
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %00111111
		defb %00001011
		defb %00000011
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb %11101110
		defb %00101010
		defb %00001011
		defb %00000010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00001010
		defb %00100010
		defb %00101010
		defb %00001000
		defb %00101010
		defb %00100010
		defb %10101010
		defb %00011110
		defb %00011111
		defb %00111011
		defb %00111111
		defb %01101110
		defb %01111111
		defb %10111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %10101000
		defb %00100000
		defb %10101000
		defb %10001000
		defb %10101010
		defb %00100010
		defb %10101010
		defb %11101000
		defb %11111000
		defb %10111000
		defb %11111100
		defb %11101110
		defb %11111110
		defb %10111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00001100
		defb %00001010
		defb %00110011
		defb %00101010
		defb %01001100
		defb %00101010
		defb %00110011
		defb %10101010
		defb %00001110
		defb %00011011
		defb %00111011
		defb %00101110
		defb %01101110
		defb %00111011
		defb %10111011
		defb %11101110
		defb %00011110
		defb %00011111
		defb %00111011
		defb %00111111
		defb %01101110
		defb %01111111
		defb %11111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %11001000
		defb %10101000
		defb %00110000
		defb %10101000
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11101000
		defb %10111000
		defb %10111000
		defb %11101100
		defb %11101110
		defb %10111010
		defb %10111011
		defb %11101110
		defb %11101000
		defb %11111000
		defb %10111100
		defb %11111100
		defb %11101110
		defb %11111110
		defb %10111111
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00011111
		defb %00011111
		defb %00111111
		defb %00111111
		defb %01111111
		defb %01111111
		defb %11111111
		defb %11111111
		defb %00001110
		defb %00011111
		defb %00111011
		defb %00111111
		defb %01101110
		defb %01111111
		defb %10111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111000
		defb %11111000
		defb %11111100
		defb %11111100
		defb %11111110
		defb %11111110
		defb %11111111
		defb %11111111
		defb %11101000
		defb %11111000
		defb %10111000
		defb %11111100
		defb %11101110
		defb %11111110
		defb %10111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00011111
		defb %00011111
		defb %00111111
		defb %00111111
		defb %01111111
		defb %01111111
		defb %11111111
		defb %11111111
		defb %00011111
		defb %00011111
		defb %00111111
		defb %00111111
		defb %01111111
		defb %01111111
		defb %11111111
		defb %11111111
		defb %00001100
		defb %00001010
		defb %00110011
		defb %00101010
		defb %01001100
		defb %00101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %11111000
		defb %11111000
		defb %11111100
		defb %11111100
		defb %11111110
		defb %11111110
		defb %11111111
		defb %11111111
		defb %11111000
		defb %11111000
		defb %11111100
		defb %11111100
		defb %11111110
		defb %11111110
		defb %11111111
		defb %11111111
		defb %11001000
		defb %10101000
		defb %00110000
		defb %10101000
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %11000000
		defb %11110000
		defb %11111100
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %00000000
		defb %00100000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %11110000
		defb %11111100
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %00000000
		defb %00100000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11000000
		defb %11110000
		defb %11111100
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %11110000
		defb %11111100
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %11110000
		defb %10111000
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00100000
		defb %00100010
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %11110000
		defb %10111000
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %10000000
		defb %00100000
		defb %00100010
		defb 0,0,0,0,0,0,0,0
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11000000
		defb %11110000
		defb %10111000
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11000000
		defb %10100000
		defb %10111000
		defb %10101010
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %10100000
		defb %00110000
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %11110000
		defb %10111100
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11000000
		defb %10100000
		defb %10111000
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00101010
		defb %00100010
		defb %00101010
		defb %00001000
		defb %00101010
		defb %00000010
		defb %00001010
		defb %11101110
		defb %11111111
		defb %00111011
		defb %01111111
		defb %00101110
		defb %00111111
		defb %00011011
		defb %00011111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %10101010
		defb %00100010
		defb %10101010
		defb %10001000
		defb %10101000
		defb %00100000
		defb %10101000
		defb %11101110
		defb %11111111
		defb %10111010
		defb %11111110
		defb %11101100
		defb %11111100
		defb %10111000
		defb %11111000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110011
		defb %00101010
		defb %00001100
		defb %00101010
		defb %00010011
		defb %00001010
		defb %11101110
		defb %10111011
		defb %00111011
		defb %01101110
		defb %00101110
		defb %00111011
		defb %00011011
		defb %00001110
		defb %11101110
		defb %11111111
		defb %01111011
		defb %01111111
		defb %00111110
		defb %00111111
		defb %00011011
		defb %00011111
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110010
		defb %10101010
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10101000
		defb %11101110
		defb %10111011
		defb %10111010
		defb %11101110
		defb %11101100
		defb %10111000
		defb %10111000
		defb %11101000
		defb %11101111
		defb %11111111
		defb %10111110
		defb %11111110
		defb %11101100
		defb %11111100
		defb %10111000
		defb %11111000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %01111111
		defb %01111111
		defb %00111111
		defb %00111111
		defb %00011111
		defb %00011111
		defb %11101110
		defb %11111111
		defb %00111011
		defb %01111111
		defb %00101110
		defb %00111111
		defb %00011011
		defb %00011111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111110
		defb %11111110
		defb %11111100
		defb %11111100
		defb %11111000
		defb %11111000
		defb %11101110
		defb %11111111
		defb %10111010
		defb %11111110
		defb %11101100
		defb %11111100
		defb %10111000
		defb %11111000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %01111111
		defb %01111111
		defb %00111111
		defb %00111111
		defb %00011111
		defb %00011111
		defb %11111111
		defb %11111111
		defb %01111111
		defb %01111111
		defb %00111111
		defb %00111111
		defb %00011111
		defb %00011111
		defb %11001100
		defb %10101010
		defb %00110011
		defb %00101010
		defb %00001100
		defb %00101010
		defb %00010011
		defb %00001010
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111110
		defb %11111110
		defb %11111100
		defb %11111100
		defb %11111000
		defb %11111000
		defb %11111111
		defb %11111111
		defb %11111110
		defb %11111110
		defb %11111100
		defb %11111100
		defb %11111000
		defb %11111000
		defb %11001100
		defb %10101010
		defb %00110010
		defb %10101010
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10101000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111100
		defb %11110000
		defb %11000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111100
		defb %11110000
		defb %11000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111100
		defb %11110000
		defb %11000000
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111100
		defb %11110000
		defb %11000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111100
		defb %10110000
		defb %11000000
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb %10001000
		defb %10000000
		defb %00100000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111100
		defb %10110000
		defb %11000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %10001000
		defb %00100000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101111
		defb %11111100
		defb %10110000
		defb %11000000
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb %11101110
		defb %10101000
		defb %10110000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101000
		defb %00110000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111100
		defb %10110000
		defb %11000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %10101000
		defb %10110000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00001010
		defb %00000010
		defb %00000010
		defb %00000000
		defb %00000010
		defb %00000000
		defb %00000000
		defb %00001110
		defb %00001111
		defb %00000011
		defb %00000111
		defb %00000010
		defb %00000011
		defb %00000001
		defb %00000001
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %10100000
		defb %00100000
		defb %10100000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb %11100000
		defb %11110000
		defb %10100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00001100
		defb %00001010
		defb %00000011
		defb %00000010
		defb %00000000
		defb %00000010
		defb %00000001
		defb %00000000
		defb %00001110
		defb %00001011
		defb %00000011
		defb %00000110
		defb %00000010
		defb %00000011
		defb %00000001
		defb %00000000
		defb %00001110
		defb %00001111
		defb %00000111
		defb %00000111
		defb %00000010
		defb %00000011
		defb %00000001
		defb %00000001
		defb 0,0,0,0,0,0,0,0
		defb %11000000
		defb %10100000
		defb %00100000
		defb %10100000
		defb %11000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb %11100000
		defb %10110000
		defb %10100000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %11110000
		defb %11110000
		defb %10100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00001111
		defb %00001111
		defb %00000111
		defb %00000111
		defb %00000011
		defb %00000011
		defb %00000001
		defb %00000001
		defb %00001110
		defb %00001111
		defb %00000011
		defb %00000111
		defb %00000010
		defb %00000011
		defb %00000001
		defb %00000001
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %11100000
		defb %11110000
		defb %10100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00001111
		defb %00001111
		defb %00000111
		defb %00000111
		defb %00000011
		defb %00000011
		defb %00000001
		defb %00000001
		defb %00001111
		defb %00001111
		defb %00000111
		defb %00000111
		defb %00000011
		defb %00000011
		defb %00000001
		defb %00000001
		defb %00001100
		defb %00001010
		defb %00000011
		defb %00000010
		defb %00000000
		defb %00000010
		defb %00000001
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11100000
		defb %11000000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %10100000
		defb %00100000
		defb %10100000
		defb %11000000
		defb %10000000
		defb %00000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00011111
		defb %00111111
		defb %01111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000000
		defb %00001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11111000
		defb %11111100
		defb %11111110
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %00000000
		defb %00100000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00011111
		defb %00111111
		defb %01111111
		defb %11111111
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00101010
		defb %00110011
		defb %10101010
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11111000
		defb %11111100
		defb %11111110
		defb %11111111
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11001000
		defb %10101000
		defb %00110010
		defb %10101010
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11001000
		defb %10101000
		defb %00110010
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000011
		defb %00000011
		defb %00001111
		defb %00001110
		defb %00000111
		defb %00000011
		defb %00000001
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000010
		defb %00001000
		defb %00000000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %11000000
		defb %10100000
		defb %11110000
		defb %11100000
		defb %11100000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %00100000
		defb %00100000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00000010
		defb %00000011
		defb %00000000
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00001110
		defb %00000111
		defb %00000011
		defb %00000001
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001110
		defb %00000010
		defb %00000011
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11000000
		defb %10100000
		defb %00000000
		defb %10000000
		defb %10000000
		defb %11000000
		defb %10100000
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %10100000
		defb %10100000
		defb %11100000
		defb %10100000
		defb %10000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000010
		defb %00001010
		defb %00001000
		defb %00101010
		defb %00100010
		defb %10101010
		defb %00000001
		defb %00000011
		defb %00000011
		defb %00001111
		defb %00001110
		defb %00111111
		defb %00111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00101000
		defb %00100010
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00111100
		defb %00111010
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00101010
		defb %00110011
		defb %10101010
		defb %00000000
		defb %00000011
		defb %00000011
		defb %00001110
		defb %00001110
		defb %00111011
		defb %00111011
		defb %11101110
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00011110
		defb %00111111
		defb %01111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00101000
		defb %00110010
		defb %10101010
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00111000
		defb %00111010
		defb %11101110
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00111100
		defb %01111010
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00111100
		defb %01111110
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00111100
		defb %00111010
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11111000
		defb %11111100
		defb %11111110
		defb %11111111
		defb %10000000
		defb %11000000
		defb %10100000
		defb %11110000
		defb %11101000
		defb %11111100
		defb %10111010
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00111100
		defb %01111110
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00011000
		defb %00111100
		defb %01111110
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00001000
		defb %00101000
		defb %00110010
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11111000
		defb %11111100
		defb %11111110
		defb %11111111
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11111000
		defb %11111100
		defb %11111110
		defb %11111111
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11001000
		defb %10101000
		defb %00110010
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00001111
		defb %00000111
		defb %00000011
		defb %00000001
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000000
		defb %00001000
		defb %00000000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10000000
		defb %00000000
		defb %00100000
		defb %00000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000001
		defb %00000011
		defb %00000111
		defb %00001111
		defb %00001111
		defb %00000111
		defb %00000011
		defb %00000001
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00000010
		defb %00000011
		defb %00000000
		defb %00000000
		defb %00000010
		defb %00000011
		defb %00001010
		defb %00001100
		defb %00000010
		defb %00000011
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %10000000
		defb %11000000
		defb %11100000
		defb %11110000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11000000
		defb %10100000
		defb %00000000
		defb %10000000
		defb %10000000
		defb %10000000
		defb %00100000
		defb %10100000
		defb %11000000
		defb %10100000
		defb %00000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %00111111
		defb %00111011
		defb %00001111
		defb %00001110
		defb %00000011
		defb %00000011
		defb %00000000
		defb %10001000
		defb %00001000
		defb %00100010
		defb %00000010
		defb %00001000
		defb %00000000
		defb %00000010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111110
		defb %10111000
		defb %11111000
		defb %11100000
		defb %11100000
		defb %10000000
		defb %10000000
		defb %10001000
		defb %10001000
		defb %00100000
		defb %00100000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %00101010
		defb %00110011
		defb %00001010
		defb %00001100
		defb %00000010
		defb %00000011
		defb %00000000
		defb %11101110
		defb %00111111
		defb %00111011
		defb %00001111
		defb %00001110
		defb %00000011
		defb %00000011
		defb %00000000
		defb %11101110
		defb %00101010
		defb %00111011
		defb %00001010
		defb %00001110
		defb %00000010
		defb %00000011
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110000
		defb %10101000
		defb %11000000
		defb %10100000
		defb %00000000
		defb %10000000
		defb %11101111
		defb %11111110
		defb %10111100
		defb %11111000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %11101110
		defb %10101010
		defb %10111000
		defb %10101000
		defb %11100000
		defb %10100000
		defb %10000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00101010
		defb %00100010
		defb %00001010
		defb %00001000
		defb %00000010
		defb %00000010
		defb %00000000
		defb %11101110
		defb %01111111
		defb %00111011
		defb %00011111
		defb %00001110
		defb %00000111
		defb %00000011
		defb %00000001
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00101010
		defb %00100000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %01111110
		defb %00111000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %00101010
		defb %00110011
		defb %00001010
		defb %00001100
		defb %00000010
		defb %00000011
		defb %00000000
		defb %11101110
		defb %00111011
		defb %00111011
		defb %00001110
		defb %00001110
		defb %00000011
		defb %00000011
		defb %00000000
		defb %11101110
		defb %01111111
		defb %00111011
		defb %00011111
		defb %00001110
		defb %00000111
		defb %00000011
		defb %00000001
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %00101010
		defb %00110000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %00111010
		defb %00111000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101111
		defb %01111110
		defb %00111100
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %01111110
		defb %00111100
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %01111110
		defb %00111000
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111110
		defb %11111100
		defb %11111000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %11101110
		defb %11111110
		defb %10111000
		defb %11111000
		defb %11100000
		defb %11100000
		defb %10000000
		defb %10000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %01111110
		defb %00111100
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11111111
		defb %01111110
		defb %00111100
		defb %00011000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11001100
		defb %00101010
		defb %00110000
		defb %00001000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111110
		defb %11111100
		defb %11111000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %11111111
		defb %11111110
		defb %11111100
		defb %11111000
		defb %11110000
		defb %11100000
		defb %11000000
		defb %10000000
		defb %11001100
		defb %10101010
		defb %00110000
		defb %10101000
		defb %11000000
		defb %10100000
		defb %00000000
		defb %10000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb %10001000
		defb %00000000
		defb %00100010
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb %10001000
		defb %10001000
		defb %00100010
		defb %00100010
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb %11101110
		defb %10101010
		defb %10111011
		defb %10101010
		defb 0,0,0,0,0,0,0,0
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %10001000
		defb %10101010
		defb %00100010
		defb %10101010
		defb %10001000
		defb %10101010
		defb %00100010
		defb %10101010
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11101110
		defb %10111011
		defb %10111011
		defb %11101110
		defb %11101110
		defb %10111011
		defb %10111011
		defb %11101110
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %11101110
		defb %11111111
		defb %10111011
		defb %11111111
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb %00000000
		defb 0,0,0,0,0,0,0,0
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11111111
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
		defb %11001100
		defb %10101010
		defb %00110011
		defb %10101010
#line 4201
__LABEL__flg01:
	ld a, (_c)
	ld l, a
	ld h, 0
	push hl
	ld de, (_a)
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL43
	call __LABEL__700
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ex de, hl
	ld hl, (_hisc)
	or a
	sbc hl, de
	jp nc, __LABEL45
	ld a, (_cnt)
	ld l, a
	ld h, 0
	ld (_hisc), hl
__LABEL45:
	ld hl, __LABEL__text02 - __LABEL__text01
	push hl
	ld hl, __LABEL__text01
	push hl
	ld hl, 11
	push hl
	ld hl, 11
	push hl
	call _pv1000writetext
	ld hl, __LABEL__text04 - __LABEL__text03
	push hl
	ld hl, __LABEL__text03
	push hl
	ld hl, 12
	push hl
	ld hl, 11
	push hl
	call _pv1000writetext
	call __LABEL__670
	ld a, 0
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL46
__LABEL49:
__LABEL50:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL46:
	ld hl, 50000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL49
__LABEL48:
	ld a, 63
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL51
__LABEL54:
__LABEL55:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL51:
	ld hl, 25000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL54
__LABEL53:
__LABEL__210:
	ld de, 15
	ld hl, (_jacum)
	call __BAND16
	push hl
	ld de, 0
	pop hl
	or a
	sbc hl, de
	ld a, h
	or l
	or a
	jp z, __LABEL57
	ld de, (_shfc)
	ld hl, (_seed)
	add hl, de
	call _pv1000rnd
	ld (_seed), hl
	jp __LABEL__10
__LABEL57:
	ld hl, (_shfc)
	inc hl
	ld (_shfc), hl
	jp __LABEL__210
__LABEL43:
__LABEL__400:
	ld de, (_n)
	ld hl, (_hisc)
	or a
	sbc hl, de
	jp nc, __LABEL59
	ld hl, (_n)
	ld (_hisc), hl
__LABEL59:
	call __LABEL__670
	ld hl, 1
	ld (_i), hl
	jp __LABEL60
__LABEL63:
__LABEL64:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL60:
	ld hl, 20000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL63
__LABEL62:
	call __LABEL__600
	ret
__LABEL__500:
	ld hl, __LABEL__tileg1p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg1p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg1p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld a, 10
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL65
__LABEL68:
__LABEL69:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL65:
	ld hl, 15000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL68
__LABEL67:
	ld a, 63
	ld bc, 248
	out (c), a
	ld hl, __LABEL__tileg0p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg0p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg0p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, 1
	ld (_i), hl
	jp __LABEL70
__LABEL73:
__LABEL74:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL70:
	ld hl, 1000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL73
__LABEL72:
	ret
__LABEL__510:
	ld hl, __LABEL__tiler1p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler1p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 16
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler1p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld a, 12
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL75
__LABEL78:
__LABEL79:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL75:
	ld hl, 15000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL78
__LABEL77:
	ld a, 63
	ld bc, 248
	out (c), a
	ld hl, __LABEL__tiler0p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler0p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 16
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler0p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, 1
	ld (_i), hl
	jp __LABEL80
__LABEL83:
__LABEL84:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL80:
	ld hl, 1000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL83
__LABEL82:
	ret
__LABEL__520:
	ld hl, __LABEL__tiley1p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley1p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 6
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley1p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld a, 14
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL85
__LABEL88:
__LABEL89:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL85:
	ld hl, 15000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL88
__LABEL87:
	ld a, 63
	ld bc, 248
	out (c), a
	ld hl, __LABEL__tiley0p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley0p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 6
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley0p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, 1
	ld (_i), hl
	jp __LABEL90
__LABEL93:
__LABEL94:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL90:
	ld hl, 1000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL93
__LABEL92:
	ret
__LABEL__530:
	ld hl, __LABEL__tilec1p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec1p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec1p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld a, 16
	ld bc, 248
	out (c), a
	ld hl, 1
	ld (_i), hl
	jp __LABEL95
__LABEL98:
__LABEL99:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL95:
	ld hl, 15000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL98
__LABEL97:
	ld a, 63
	ld bc, 248
	out (c), a
	ld hl, __LABEL__tilec0p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec0p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec0p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld hl, 1
	ld (_i), hl
	jp __LABEL100
__LABEL103:
__LABEL104:
	ld hl, (_i)
	inc hl
	ld (_i), hl
__LABEL100:
	ld hl, 1000
	ld de, (_i)
	or a
	sbc hl, de
	jp nc, __LABEL103
__LABEL102:
	ret
__LABEL__600:
	ld hl, __LABEL__tileg0p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg0p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg0p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler0p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler0p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 16
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler0p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley0p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley0p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 6
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley0p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec0p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec0p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec0p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ret
__LABEL__650:
	ld a, 32
	push af
	ld hl, 24
	push hl
	ld hl, 32
	push hl
	ld hl, 0
	push hl
	ld hl, 0
	push hl
	call _pv1000filltile
	ret
__LABEL__670:
	ld hl, __LABEL__text03 - __LABEL__text02
	push hl
	ld hl, __LABEL__text02
	push hl
	ld hl, 22
	push hl
	ld hl, 20
	push hl
	call _pv1000writetext
	ld hl, 3
	push hl
	ld a, (_cnt)
	dec a
	ld l, a
	ld h, 0
	push hl
	ld hl, 22
	push hl
	ld hl, 26
	push hl
	call _pv1000writedec
	ld hl, __LABEL__text05 - __LABEL__text04
	push hl
	ld hl, __LABEL__text04
	push hl
	ld hl, 23
	push hl
	ld hl, 18
	push hl
	call _pv1000writetext
	ld hl, 3
	push hl
	ld hl, (_hisc)
	dec hl
	push hl
	ld hl, 23
	push hl
	ld hl, 26
	push hl
	call _pv1000writedec
	ret
__LABEL__700:
	ld hl, __LABEL__tileg1p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg1p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 2
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tileg1p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 3
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler1p0
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 10
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler1p1
	push hl
	ld a, 6
	push af
	ld hl, 8
	push hl
	ld hl, 16
	push hl
	ld hl, 12
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiler1p2
	push hl
	ld a, 4
	push af
	ld hl, 2
	push hl
	ld hl, 17
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley1p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley1p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 6
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tiley1p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 7
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec1p0
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 6
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec1p1
	push hl
	ld a, 8
	push af
	ld hl, 6
	push hl
	ld hl, 8
	push hl
	ld hl, 20
	push hl
	call _pv1000puttile
	ld hl, __LABEL__tilec1p2
	push hl
	ld a, 2
	push af
	ld hl, 4
	push hl
	ld hl, 16
	push hl
	ld hl, 21
	push hl
	call _pv1000puttile
	ret
__LABEL105:
	jp __LABEL105
__LABEL106:
__LABEL__tileg0p0:
#line 277
		defb $64,$65
		defb $C2,$C2
		defb $B4,$C2
		defb $20,$B4
#line 281
__LABEL__tileg0p1:
#line 284
		defb $64,$65,$C2,$C2,$C2,$C2,$84,$85
		defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
		defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
		defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
		defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
		defb $B4,$94,$95,$20,$20,$74,$75,$B5
#line 290
__LABEL__tileg0p2:
#line 293
		defb $84,$85
		defb $C2,$C2
		defb $C2,$B5
		defb $B5,$20
#line 297
__LABEL__tileg1p0:
#line 302
		defb $66,$67
		defb $C3,$C3
		defb $B6,$C3
		defb $20,$B6
#line 306
__LABEL__tileg1p1:
#line 309
		defb $66,$67,$C3,$C3,$C3,$C3,$86,$87
		defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
		defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
		defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
		defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
		defb $B6,$96,$97,$20,$20,$76,$77,$B7
#line 315
__LABEL__tileg1p2:
#line 319
		defb $86,$87
		defb $C3,$C3
		defb $C3,$B7
		defb $B7,$20
#line 323
__LABEL__tiler0p0:
#line 331
		defb $20,$A0
		defb $A0,$C0
		defb $C0,$C0
		defb $70,$71
#line 335
__LABEL__tiler0p1:
#line 338
		defb $A0,$80,$81,$20,$20,$60,$61,$A1
		defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
		defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
		defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
		defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
		defb $70,$71,$C0,$C0,$C0,$C0,$90,$91
#line 344
__LABEL__tiler0p2:
#line 347
		defb $A1,$20
		defb $C0,$A1
		defb $C0,$C0
		defb $90,$91
#line 351
__LABEL__tiler1p0:
#line 356
		defb $20,$A2
		defb $A2,$C1
		defb $C1,$C1
		defb $72,$73
#line 360
__LABEL__tiler1p1:
#line 363
		defb $A2,$82,$83,$20,$20,$62,$63,$A3
		defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
		defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
		defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
		defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
		defb $72,$73,$C1,$C1,$C1,$C1,$92,$93
#line 369
__LABEL__tiler1p2:
#line 372
		defb $A3,$20
		defb $C1,$A3
		defb $C1,$C1
		defb $92,$93
#line 376
__LABEL__tiley0p0:
#line 384
		defb $6C,$C6,$AD,$20
		defb $7C,$C6,$C6,$AD
#line 386
__LABEL__tiley0p1:
#line 389
		defb $6C,$C6,$C6,$C6,$C6,$AD
		defb $7C,$C6,$C6,$C6,$C6,$8D
		defb $C6,$C6,$C6,$C6,$C6,$9D
		defb $C6,$C6,$C6,$C6,$C6,$20
		defb $C6,$C6,$C6,$C6,$C6,$20
		defb $C6,$C6,$C6,$C6,$C6,$6D
		defb $8C,$C6,$C6,$C6,$C6,$7D
		defb $9C,$C6,$C6,$C6,$C6,$BD
#line 397
__LABEL__tiley0p2:
#line 400
		defb $8C,$C6,$C6,$BD
		defb $9C,$C6,$BD,$20
#line 402
__LABEL__tiley1p0:
#line 407
		defb $6E,$C7,$AF,$20
		defb $7E,$C7,$C7,$AF
#line 409
__LABEL__tiley1p1:
#line 412
		defb $6E,$C7,$C7,$C7,$C7,$AF
		defb $7E,$C7,$C7,$C7,$C7,$8F
		defb $C7,$C7,$C7,$C7,$C7,$9F
		defb $C7,$C7,$C7,$C7,$C7,$20
		defb $C7,$C7,$C7,$C7,$C7,$20
		defb $C7,$C7,$C7,$C7,$C7,$6F
		defb $8E,$C7,$C7,$C7,$C7,$7F
		defb $9E,$C7,$C7,$C7,$C7,$BF
#line 420
__LABEL__tiley1p2:
#line 423
		defb $8E,$C7,$C7,$BF
		defb $9E,$C7,$BF,$20
#line 425
__LABEL__tilec0p0:
#line 433
		defb $20,$A8,$C4,$69
		defb $A8,$C4,$C4,$79
#line 435
__LABEL__tilec0p1:
#line 438
		defb $A8,$C4,$C4,$C4,$C4,$69
		defb $88,$C4,$C4,$C4,$C4,$79
		defb $98,$C4,$C4,$C4,$C4,$C4
		defb $20,$C4,$C4,$C4,$C4,$C4
		defb $20,$C4,$C4,$C4,$C4,$C4
		defb $68,$C4,$C4,$C4,$C4,$C4
		defb $78,$C4,$C4,$C4,$C4,$89
		defb $B8,$C4,$C4,$C4,$C4,$99
#line 446
__LABEL__tilec0p2:
#line 449
		defb $B8,$C4,$C4,$89
		defb $20,$B8,$C4,$99
#line 451
__LABEL__tilec1p0:
#line 456
		defb $20,$AA,$C5,$6B
		defb $AA,$C5,$C5,$7B
#line 458
__LABEL__tilec1p1:
#line 461
		defb $AA,$C5,$C5,$C5,$C5,$6B
		defb $8A,$C5,$C5,$C5,$C5,$7B
		defb $9A,$C5,$C5,$C5,$C5,$C5
		defb $20,$C5,$C5,$C5,$C5,$C5
		defb $20,$C5,$C5,$C5,$C5,$C5
		defb $6A,$C5,$C5,$C5,$C5,$C5
		defb $7A,$C5,$C5,$C5,$C5,$8B
		defb $BA,$C5,$C5,$C5,$C5,$9B
#line 469
__LABEL__tilec1p2:
#line 472
		defb $BA,$C5,$C5,$8B
		defb $20,$BA,$C5,$9B
#line 474
__LABEL__urandomdata01:
#line 480
		defb 7,5,4,5,7,3,5,7,1,6,6,3,3,1,3,2
		defb 6,4,5,2,2,4,5,4,5,5,5,7,3,2,7,1
		defb 1,3,7,4,0,4,4,1,5,7,7,0,1,5,6,5
		defb 6,3,4,1,4,1,4,2,1,1,6,3,0,5,6,5
		defb 6,4,2,0,2,2,3,7,3,7,1,3,1,7,3,5
		defb 2,0,1,5,1,4,2,0,6,0,1,6,0,5,2,6
		defb 5,1,4,2,3,4,7,6,3,5,0,6,5,2,3,1
		defb 2,1,1,5,2,4,5,6,3,7,2,7,3,0,5,6
		defb 7,4,7,1,6,3,3,6,7,5,1,0,6,4,2,7
		defb 5,0,3,7,5,1,1,1,2,6,2,0,3,4,6,3
		defb 3,1,4,3,6,1,0,7,2,3,0,0,0,4,7,0
		defb 3,6,3,4,3,4,6,0,5,5,4,7,0,5,5,2
		defb 4,1,6,7,4,1,2,7,4,0,3,5,6,4,2,0
		defb 6,7,2,7,0,4,6,4,4,0,7,5,2,1,5,5
		defb 5,2,0,0,2,7,3,6,7,5,4,7,6,4,7,6
		defb 1,6,6,4,2,2,7,3,0,1,2,3,5,6,2,7
#line 496
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_pv1000rnd:
#line 2
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, pv1000rndloop
		inc  hl
pv1000rndloop:
		ret
#line 19
_pv1000rnd__leave:
	ret
_pv1000joystick:
	push ix
	ld ix, 0
	add ix, sp
	ld a, (ix+5)
	ld bc, 253
	out (c), a
	ld bc, 253
	in a, (c)
_pv1000joystick__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	ex (sp), hl
	exx
	ret
_pv1000writedec:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 1
	ld (_pv1000writedec_tylp), hl
	ld l, (ix+10)
	ld h, (ix+11)
	ld (_pv1000writedec_txlp), hl
	jp __LABEL107
__LABEL110:
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 47104
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	ld hl, (_pv1000writedec_txlp)
	add hl, de
	dec hl
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ld de, (_pv1000writedec_tylp)
	call __DIVU16
	ld de, 0
	push de
	push hl
	ld de, 0
	ld hl, 10
	call __SWAP32
	call __MODI32
	ld bc, 0
	push bc
	ld bc, 48
	push bc
	pop bc
	add hl, bc
	ex de, hl
	pop bc
	adc hl, bc
	ex de, hl
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_pv1000writedec_tylp)
	ld de, 10
	call __MUL16_FAST
	ld (_pv1000writedec_tylp), hl
__LABEL111:
	ld hl, (_pv1000writedec_txlp)
	dec hl
	ld (_pv1000writedec_txlp), hl
__LABEL107:
	ld de, 1
	ld hl, (_pv1000writedec_txlp)
	or a
	sbc hl, de
	jp nc, __LABEL110
__LABEL109:
_pv1000writedec__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pv1000writetext:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_pv1000writetext_txlp), hl
	jp __LABEL112
__LABEL115:
	ld l, (ix+6)
	ld h, (ix+7)
	ld de, 32
	call __MUL16_FAST
	ld de, 47104
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	ex de, hl
	ld hl, (_pv1000writetext_txlp)
	add hl, de
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	ld hl, (_pv1000writetext_txlp)
	add hl, de
	ld b, h
	ld c, l
	ld a, (bc)
	pop hl
	ld (hl), a
__LABEL116:
	ld hl, (_pv1000writetext_txlp)
	inc hl
	ld (_pv1000writetext_txlp), hl
__LABEL112:
	ld l, (ix+10)
	ld h, (ix+11)
	dec hl
	ld de, (_pv1000writetext_txlp)
	or a
	sbc hl, de
	jp nc, __LABEL115
__LABEL114:
_pv1000writetext__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pv1000ldir:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld d, (ix+5)
		ld e, (ix+4)
		ld h, (ix+7)
		ld l, (ix+6)
		ld b, (ix+9)
		ld c, (ix+8)
		ldir
#line 8
_pv1000ldir__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pv1000fillram:
	push ix
	ld ix, 0
	add ix, sp
#line 1
		ld l, (ix+4)
		ld h, (ix+5)
		ld b, (ix+9)
		ld c, (ix+8)
pv1000fillramloop:
		ld a, (ix+6)
		ld (hl),a
		dec bc
		inc hl
		ld a,b
		or c
		jp nz,pv1000fillramloop
#line 13
_pv1000fillram__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pv1000filltile:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_pv1000filltile_tylp), hl
	jp __LABEL117
__LABEL120:
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld a, (ix+13)
	ld l, a
	ld h, 0
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	ld hl, (_pv1000filltile_tylp)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ld de, 47104
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _pv1000fillram
__LABEL121:
	ld hl, (_pv1000filltile_tylp)
	inc hl
	ld (_pv1000filltile_tylp), hl
__LABEL117:
	ld l, (ix+10)
	ld h, (ix+11)
	dec hl
	ld de, (_pv1000filltile_tylp)
	or a
	sbc hl, de
	jp nc, __LABEL120
__LABEL119:
_pv1000filltile__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
_pv1000puttile:
	push ix
	ld ix, 0
	add ix, sp
	ld hl, 0
	ld (_pv1000puttile_tylp), hl
	jp __LABEL122
__LABEL125:
	ld l, (ix+8)
	ld h, (ix+9)
	push hl
	ld l, (ix+12)
	ld h, (ix+13)
	push hl
	ld l, (ix+8)
	ld h, (ix+9)
	ex de, hl
	ld hl, (_pv1000puttile_tylp)
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	push hl
	ld l, (ix+6)
	ld h, (ix+7)
	ex de, hl
	ld hl, (_pv1000puttile_tylp)
	add hl, de
	ld de, 32
	call __MUL16_FAST
	ld de, 47104
	add hl, de
	push hl
	ld l, (ix+4)
	ld h, (ix+5)
	ex de, hl
	pop hl
	add hl, de
	push hl
	call _pv1000ldir
__LABEL126:
	ld hl, (_pv1000puttile_tylp)
	inc hl
	ld (_pv1000puttile_tylp), hl
__LABEL122:
	ld a, (ix+11)
	dec a
	ld l, a
	ld h, 0
	ld de, (_pv1000puttile_tylp)
	or a
	sbc hl, de
	jp nc, __LABEL125
__LABEL124:
_pv1000puttile__leave:
	ld sp, ix
	pop ix
	exx
	pop hl
	pop bc
	pop bc
	pop bc
	pop bc
	ex (sp), hl
	exx
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 6103 "simon.bas"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 6104 "simon.bas"
#line 1 "div32.asm"
#line 1 "neg32.asm"
__ABS32:
		bit 7, d
		ret z
	
__NEG32: ; Negates DEHL (Two's complement)
		ld a, l
		cpl
		ld l, a
	
		ld a, h
		cpl
		ld h, a
	
		ld a, e
		cpl
		ld e, a
		
		ld a, d
		cpl
		ld d, a
	
		inc l
		ret nz
	
		inc h
		ret nz
	
		inc de
		ret
	
#line 2 "div32.asm"
	
				 ; ---------------------------------------------------------
__DIVU32:    ; 32 bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; OPERANDS P = Dividend, Q = Divisor => OPERATION => P / Q
				 ;
				 ; Changes A, BC DE HL B'C' D'E' H'L'
				 ; ---------------------------------------------------------
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVU32START: ; Performs D'E'H'L' / HLDE
	        ; Now switch to DIVIDEND = B'C'BC / DIVISOR = D'E'DE (A / B)
	        push de ; push Lowpart(Q)
			ex de, hl	; DE = HL
	        ld hl, 0
	        exx
	        ld b, h
	        ld c, l
	        pop hl
	        push de
	        ex de, hl
	        ld hl, 0        ; H'L'HL = 0
	        exx
	        pop bc          ; Pop HightPart(B) => B = B'C'BC
	        exx
	
	        ld a, 32 ; Loop count
	
__DIV32LOOP:
	        sll c  ; B'C'BC << 1 ; Output most left bit to carry
	        rl  b
	        exx
	        rl c
	        rl b
	        exx
	
	        adc hl, hl
	        exx
	        adc hl, hl
	        exx
	
	        sbc hl,de
	        exx
	        sbc hl,de
	        exx
	        jp nc, __DIV32NOADD	; use JP inside a loop for being faster
	
	        add hl, de
	        exx
	        adc hl, de
	        exx
	        dec bc
	
__DIV32NOADD:
	        dec a
	        jp nz, __DIV32LOOP	; use JP inside a loop for being faster
	        ; At this point, quotient is stored in B'C'BC and the reminder in H'L'HL
	
	        push hl
	        exx
	        pop de
	        ex de, hl ; D'E'H'L' = 32 bits modulus
	        push bc
	        exx
	        pop de    ; DE = B'C'
	        ld h, b
	        ld l, c   ; DEHL = quotient D'E'H'L' = Modulus
	
	        ret     ; DEHL = quotient, D'E'H'L' = Modulus
	
	
	
__MODU32:    ; 32 bit modulus for 32bit unsigned division
	             ; DEHL = Dividend, Stack Top = Divisor (DE, HL)
	
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
	        call __DIVU32START	; At return, modulus is at D'E'H'L'
	
__MODU32START:
	
			exx
			push de
			push hl
	
			exx 
			pop hl
			pop de
	
			ret
	
	
__DIVI32:    ; 32 bit signed division
	             ; DEHL = Dividend, Stack Top = Divisor
	             ; A = Dividend, B = Divisor => A / B
	        exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
__DIVI32START:
			exx
			ld a, d	 ; Save sign
			ex af, af'
			bit 7, d ; Negative?
			call nz, __NEG32 ; Negates DEHL
	
			exx		; Now works with H'L'D'E'
			ex af, af'
			xor h
			ex af, af'  ; Stores sign of the result for later
	
			bit 7, h ; Negative?
			ex de, hl ; HLDE = DEHL
			call nz, __NEG32
			ex de, hl 
	
			call __DIVU32START
			ex af, af' ; Recovers sign
			and 128	   ; positive?
			ret z
	
			jp __NEG32 ; Negates DEHL and returns from there
			
			
__MODI32:	; 32bits signed division modulus
			exx
	        pop hl   ; return address
	        pop de   ; low part
	        ex (sp), hl ; CALLEE Convention ; H'L'D'E' => Dividend
	
			call __DIVI32START
			jp __MODU32START		
	
#line 6105 "simon.bas"
#line 1 "and8.asm"
	; FASTCALL boolean and 8 version.
	; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
	; Performs 8bit and 8bit and returns the boolean
	
__AND8:
		or a
		ret z
		ld a, h
		ret 
	
#line 6106 "simon.bas"
#line 1 "div8.asm"
				; --------------------------------
__DIVU8:	; 8 bit unsigned integer division 
				; Divides (Top of stack, High Byte) / A
		pop hl	; --------------------------------
		ex (sp), hl	; CALLEE
	
__DIVU8_FAST:	; Does A / H
		ld l, h
		ld h, a		; At this point do H / L
	
		ld b, 8
		xor a		; A = 0, Carry Flag = 0
		
__DIV8LOOP:
		sla	h		
		rla			
		cp	l		
		jr	c, __DIV8NOSUB
		sub	l		
		inc	h		
	
__DIV8NOSUB:	
		djnz __DIV8LOOP
	
		ld	l, a		; save remainder
		ld	a, h		; 
		
		ret			; a = Quotient, 
	
	
					; --------------------------------
__DIVI8:		; 8 bit signed integer division Divides (Top of stack) / A
		pop hl		; --------------------------------
		ex (sp), hl
	
__DIVI8_FAST:
		ld e, a		; store operands for later
		ld c, h
	
		or a		; negative?
		jp p, __DIV8A
		neg			; Make it positive
	
__DIV8A:
		ex af, af'
		ld a, h
		or a
		jp p, __DIV8B
		neg
		ld h, a		; make it positive
	
__DIV8B:
		ex af, af'
	
		call __DIVU8_FAST
	
		ld a, c
		xor l		; bit 7 of A = 1 if result is negative
	
		ld a, h		; Quotient
		ret p		; return if positive	
	
		neg
		ret
		
	
__MODU8:		; 8 bit module. REturns A mod (Top of stack) (unsigned operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODU8_FAST:	; __FASTCALL__ entry
		call __DIVU8_FAST
		ld a, l		; Remainder
	
		ret		; a = Modulus
	
	
__MODI8:		; 8 bit module. REturns A mod (Top of stack) (For singed operands)
		pop hl
		ex (sp), hl	; CALLEE
	
__MODI8_FAST:	; __FASTCALL__ entry
		call __DIVI8_FAST
		ld a, l		; remainder
	
		ret		; a = Modulus
	
#line 6107 "simon.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 6108 "simon.bas"
#line 1 "eq16.asm"
__EQ16:	; Test if 16bit values HL == DE
		; Returns result in A: 0 = False, FF = True
			or a	; Reset carry flag
			sbc hl, de 
	
			ld a, h
			or l
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 6109 "simon.bas"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 6110 "simon.bas"
#line 1 "swap32.asm"
	; Exchanges current DE HL with the
	; ones in the stack
	
__SWAP32:
		pop bc ; Return address
	
		exx
		pop hl	; exx'
		pop de
	
		exx
		push de ; exx
		push hl
	
		exx		; exx '
		push de
		push hl
		
		exx		; exx
		pop hl
		pop de
	
		push bc
	
		ret
	
#line 6111 "simon.bas"
	
ZXBASIC_USER_DATA:
	_pv1000writedec_txlp EQU 48128
	_pv1000writedec_tylp EQU 48130
	_pv1000writetext_txlp EQU 48128
	_pv1000writetext_tylp EQU 48130
	_pv1000filltile_tylp EQU 48130
	_pv1000puttile_tylp EQU 48130
	_n EQU 48160
	_cnt EQU 48162
	_c EQU 48164
	_rlc EQU 48166
	_seed EQU 48192
	_jacum EQU 48194
	_i EQU 48196
	_a EQU 48198
	_shfc EQU 48224
	_hisc EQU 48226
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
