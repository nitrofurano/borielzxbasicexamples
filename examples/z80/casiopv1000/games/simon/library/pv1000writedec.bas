sub pv1000writedec(txpos as uinteger,typos as uinteger,tvl as uinteger,tlen as uinteger):
  dim txlp as uinteger at $BC00:dim tylp as uinteger at $BC02
  tylp=1
  for txlp=tlen to 1 step -1
    poke $B800+typos*32+txpos+txlp-1,48+(int(tvl/tylp) mod 10)
    tylp=tylp*10
    next
  end sub


