sub pv1000tilebank(tvl as ubyte):
  out $FF,(tvl band $3)*16
  ;- try bor/band with existing one
  end sub

76543210
||||||||
|||||||+-- border (blue?)
||||||+--- border (green?)
|||||+---- border (red?)
||||+----- 
|||+------ bank (which one?)
||+------- bank (which one?)
|+-------- 
+--------- 
