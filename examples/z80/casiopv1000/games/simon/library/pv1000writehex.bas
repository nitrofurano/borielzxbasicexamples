sub pv1000writehex(txpos as uinteger,typos as uinteger,tvl as uinteger,tlen as uinteger):
  dim txlp as uinteger at $BC00:dim tylp as uinteger at $BC02:dim tqlp as ubyte at $BC04
  tylp=1
  for txlp=tlen to 1 step -1
    tqlp=(int(tvl/tylp)) mod 16
    poke $B800+typos*32+txpos+txlp-1,48+tqlp+(int(tqlp/10)*7)
    tylp=tylp*16
    next
  end sub


