asm
  ld sp,$BFFF
  end asm

#include "library/smsrnd.bas"

function pv1000joystick(treg as ubyte) as ubyte:
  '- 0: select,start 
  '- 1: down,right
  '- 2: left,up
  '- 3: trg1,trg2
  out $FD,treg
  return in($FD)
  end function

dim seed as uinteger at $BC10
dim eee as uinteger at $BC12
dim ee0 as uinteger at $BC14
dim ee1 as uinteger at $BC16
dim eex as uinteger at $BC18
dim eey as uinteger at $BC1A

goto flg01:

asm
  org $400
  end asm
#include "library/bacaball_charmap_antigo_pv1000.bas"

flg01:

seed=0
eex=4
eey=4

for eee=$B800 to $BAFF
  seed=smsrnd(seed)
  poke eee,128+16+(seed band 7)
  next

for ee0=0 to 15
  for ee1=0 to 15
    poke $B800+(12+ee1)+((2+ee0)*32),(ee0*16)+ee1
    next:next

eee=0

lp09:
poke $B800+(eex band 31)+((eey mod 24)*32),eee
eex=eex+((pv1000joystick(2) band 2)/2)-(pv1000joystick(4) band 1)
eey=eey+(pv1000joystick(2) band 1)-((pv1000joystick(4) band 2)/2)
for ee0=1 to 2000:next
eee=eee+1
goto lp09:


'-------------------------------------------------------------------------------




