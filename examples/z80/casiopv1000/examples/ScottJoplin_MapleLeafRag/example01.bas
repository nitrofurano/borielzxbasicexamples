asm
  ld sp,$BFFF
  end asm

out $F8,63
out $F9,63
out $FA,63

#include "library/pv1000rnd.bas"
#include "library/pv1000fillram.bas"
#include "library/pv1000joystick.bas"
#include "library/pv1000border.bas"
#include "library/pv1000putchar.bas"
#include "library/pv1000writedec.bas"
#include "library/pv1000writebin.bas"
#include "library/pv1000writehex.bas"
#include "library/pv1000writetext.bas"

dim seed as uinteger at $BC20
dim eee as uinteger at $BC22
dim ee0 as uinteger at $BC24
dim ee1 as uinteger at $BC26
dim eex as uinteger at $BC40
dim eey as uinteger at $BC42

goto flg01:

asm
  org $200
  end asm
#include "library/sunresize8x8partwhalftone.bas"
#include "library/ScottJoplin_MapleLeafRag_vortextracker_fixneeded.bas"
#include "library/ScottJoplinPic.bas"

flg01:

seed=0:eex=4:eey=4

for eee=0 to 383
  poke $B800+(eee*2),16+int(peek(eee+@scottjoplinpic01)/16)
  poke $B801+(eee*2),16+int(peek(eee+@scottjoplinpic01)band 15)
  next

pv1000writetext(3,21,@text01,14)
pv1000writetext(3,22,@text01+14,16)

for eee=$BC20 to $BFEF
  seed=pv1000rnd(seed)
  poke eee,seed
  next

do

for ee0=0 to 655
  out $F8,peek(@mapleleafrag01+(ee0*3)+0)
  out $F9,peek(@mapleleafrag01+(ee0*3)+1)
  out $FA,peek(@mapleleafrag01+(ee0*3)+2)
  for ee1=1 to 5000:next
  next

loop

text01:
asm
  defb " Scott Joplin "
  defb " Maple Leaf Rag "
  end asm

