zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=example01.bin2_
zxb.py example01.bas --org=$((0x0069))
zxb.py example01.bas --asm --org=$((0x0069))
cat example01.bin >> example01.bin2_
#dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
dd bs=40000 count=1 if=/dev/urandom of=_dummybytes.bin
cat _dummybytes.bin >> example01.bin2_
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.bin2_ of=example01.bin2
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.bin2_ of=example01.bin2
rm smsboot.bin example01.bin2_ _dummybytes.bin example01.bin
mv example01.bin2 example01.bin

mame pv1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -cart1 example01.bin
#mess pv1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -sound none -cart1 example01.bin


