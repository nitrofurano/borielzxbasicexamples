function pv1000joystick(treg as ubyte) as ubyte:
  '- 0: select,start 
  '- 1: down,right
  '- 2: left,up
  '- 3: trg1,trg2
  out $FD,treg
  return in($FD)
  end function
