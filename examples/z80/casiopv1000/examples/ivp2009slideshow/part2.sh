zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=part2.bin2_
zxb.py part2.bas --org=$((0x0069))
zxb.py part2.bas --asm --org=$((0x0069))
cat part2.bin >> part2.bin2_
#dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
dd bs=40000 count=1 if=/dev/urandom of=_dummybytes.bin
cat _dummybytes.bin >> part2.bin2_
#dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=part2.bin2_ of=part2.bin2
dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=part2.bin2_ of=part2.bin2
rm smsboot.bin part2.bin2_ _dummybytes.bin part2.bin
mv part2.bin2 part2.bin
mame pv1000 -skip_gameinfo -aspect 28:17 -resolution0 560x432 -video soft -w -sound none -cart1 part2.bin



