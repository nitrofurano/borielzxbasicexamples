rm h7n9.asm h7n9.rom
zxb.py h7n9.bas --asm --org=$((0x7FF0))
zxb.py h7n9.bas --org=$((0x7FF0))
dd ibs=1 skip=$((0x0010)) if=h7n9.bin of=h7n9.rom
rm h7n9.bin

mame coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 h7n9.rom

