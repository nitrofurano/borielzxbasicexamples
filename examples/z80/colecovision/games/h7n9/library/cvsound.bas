sub cvsound(tcha as uinteger,ttyp as uinteger, tvlu as uinteger):
  '- not working
  asm
    di
    ld a,(ix+4) ;- channel
    and %00000011
    rl a
    ld d,a
    ld a,(ix+6) ;- type
    and %00000001
    or d
    rl a
    rl a
    rl a
    rl a
    or %10000000
    ld d,a
    ld c,(ix+9) ;- value
    ld b,(ix+8) ;- value
    ld a,c
    and %00001111
    or d
    xor %00001111
    ld d,a ;- byte 1 ready, stored at register d

    ld a,c
    and %11110000
    rr a
    rr a
    rr a
    rr a
    ld c,a
    ld a,b
    and %00000011
    rl a
    rl a
    rl a
    rl a
    or c
    xor %00111111
    and %00111111
    ld c,a ;- byte 2 ready
    ld a,c
    out ($ff),a
    nop
    nop
    ld a,d
    out ($ff),a

    ei
    end asm
  end sub
