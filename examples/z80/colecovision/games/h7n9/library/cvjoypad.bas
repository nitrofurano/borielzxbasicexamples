function fastcall cvjoypad1a() as ubyte '- $C0,$FC
  asm
    ld a,$FF
    out ($C0),a
    nop
    nop
    ;nop
    ;nop
    in a,($FC) 
    end asm
  end function

function fastcall cvjoypad1b() as ubyte '- $80,$FC
  asm
    ld a,$FF
    out ($80),a
    nop
    nop
    ;nop
    ;nop
    in  a,($FC) 
    end asm
  end function

function fastcall cvjoypad2a() as ubyte '- $C0,$FE
  asm
    ld a,$FF
    out ($C0),a
    nop
    nop
    ;nop
    ;nop
    in  a,($FE) 
    end asm
  end function

function fastcall cvjoypad2b() as ubyte '- $80,$FE
  asm
    ld a,$FF
    out ($80),a
    nop
    nop
    ;nop
    ;nop
    in  a,($FE) 
    end asm
  end function

'cvjoypad - ports $FC and $FE
'- $C0 an $80 enables pad or joystick?
'- cpl inverts all bits from register a

