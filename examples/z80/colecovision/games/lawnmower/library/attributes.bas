goto attributes01end:

attributes01:
attributes32ch:
asm
;groups of 32 characters
defb $12,$E1,$E1,$6E,$1A,$4A,$6A,$CA
end asm

attributes1ch:
asm
;first characters before $20
defb $2E,$2E,$2E,$2E
defb $12,$12,$12,$12
defb $63,$12,$62,$42
defb $1A,$1A,$6A,$4A
end asm

attributeshch:
asm
;lowrespictiles
defb $33,$33
defb $33,$CC
defb $33,$C9
defb $33,$9C
defb $33,$3C
defb $3C,$99
defb $CC,$96
defb $99,$66

defb $99,$69
defb $66,$9C
defb $69,$C3
defb $3C,$33
defb $99,$33
defb $9C,$33
defb $CC,$33
defb $96,$39

defb $9C,$99
defb $33,$C3
defb $99,$C6
defb $69,$C6
defb $39,$33
defb $99,$99
defb $C9,$3C
defb $C3,$CC

defb $9C,$9C
defb $99,$39
defb $CC,$9C
defb $C3,$9C
defb $99,$C9
defb $C9,$9C
defb $9C,$C6
defb $CC,$CC



defb $6C,$99
defb $9C,$C9
defb $39,$3C
defb $9C,$6C
defb $96,$C9
defb $CC,$69
defb $33,$99
defb $33,$96

defb $33,$66
defb $3C,$99
defb $CC,$66
defb $96,$3C
defb $9C,$69
defb $39,$C6
defb $99,$C3
defb $C6,$3C

defb $93,$69
defb $CC,$99
defb $33,$93
defb $99,$C6
defb $66,$66
defb $69,$66
defb $3C,$C9
defb $CC,$C9

defb $66,$99
defb $69,$69
defb $C3,$93
defb $66,$69
defb $69,$99
defb $C9,$C9
defb $99,$96
defb $93,$93



defb $66,$96
defb $93,$CC
defb $96,$66
defb $69,$9C
defb $9C,$66
defb $9C,$CC
defb $C9,$CC
defb $99,$CC

defb $C9,$99
defb $3C,$C3
defb $C3,$33
defb $DD,$DD
defb $DD,$DD
defb $DD,$DD
defb $DD,$DD
defb $DD,$DD

end asm

attributes01end:

