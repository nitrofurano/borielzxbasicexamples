rm lawnmower.asm lawnmower.rom
zxb.py lawnmower.bas --asm --org=$((0x7FF0))
zxb.py lawnmower.bas --org=$((0x7FF0))
dd ibs=1 skip=$((0x0010)) if=lawnmower.bin of=lawnmower.rom
rm lawnmower.bin
mame coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 lawnmower.rom
