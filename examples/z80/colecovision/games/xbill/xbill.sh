rm xbill.asm xbill.rom
zxb.py xbill.bas --asm --org=$((0x7FF0))
zxb.py xbill.bas --org=$((0x7FF0))
dd ibs=1 skip=$((0x0010)) if=xbill.bin of=xbill.rom
rm xbill.bin
mame coleco -video soft -w -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 xbill.rom
#mame coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 xbill.rom
