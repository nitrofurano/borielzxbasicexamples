#include "library/cvboot.bas"
#include "library/cvjoypad.bas"
#include "library/smsvdp.bas"
#include "library/smsfilvrm.bas"
#include "library/smsldirvm.bas"
#include "library/smsvpoke.bas"
#include "library/smsrnd.bas"
#include "library/sg1000putsprite.bas"
#include "library/3c1b_chr_bmap.bas"
#include "library/3c1b_chr_attr.bas"
#include "library/xbill_sprites.bas"
#include "library/titletiles.bas"
#include "library/titletileschr.bas"
#include "library/3c1b_chr_bmap_lowercase.bas"

dim xq as uinteger at $7010
dim yq as uinteger at $7012
dim sq as uinteger at $7014
dim ep as uinteger at $7016
dim cp as uinteger at $7018
dim xl as uinteger at $701A
dim yl as uinteger at $701C
dim a as uinteger at $701E
dim b as uinteger at $7020
dim c as uinteger at $7022
dim i as uinteger at $7024
dim x as uinteger at $7026
dim y as uinteger at $7028
dim d as uinteger at $702A
dim mv as uinteger at $702C
dim ym as uinteger at $702E
dim lc as uinteger at $7030
dim ad as uinteger at $7032
dim jst0 as ubyte at $7034
dim xdv as uinteger at $7036

dim seed as uinteger at $7038
dim eee as uinteger at $703A
dim ee0 as uinteger at $703C
dim ee1 as uinteger at $703E

dim xpbt as integer at $7040
dim ypbt as integer at $7042
dim xdbt as integer at $7044
dim ydbt as integer at $7046
dim cbt as ubyte at $7048
dim debug as ubyte at $7049

dim xpb0 as integer at $7050
dim ypb0 as integer at $7052
dim xpb1 as integer at $7054
dim ypb1 as integer at $7056
dim xpb2 as integer at $7058
dim ypb2 as integer at $705A
dim xpb3 as integer at $705C
dim ypb3 as integer at $705E
dim xdb0 as integer at $7060
dim ydb0 as integer at $7062
dim xdb1 as integer at $7064
dim ydb1 as integer at $7066
dim xdb2 as integer at $7068
dim ydb2 as integer at $706A
dim xdb3 as integer at $706C
dim ydb3 as integer at $706E
dim cb0 as ubyte at $7070
dim cb1 as ubyte at $7071
dim cb2 as ubyte at $7072
dim cb3 as ubyte at $7073

'dim zv1(5,5) as ubyte at $E080
debug=%10000011
debug=0

seed=51

seed=smsrnd(seed):xpb0=seed:seed=smsrnd(seed):ypb0=seed mod 384
seed=smsrnd(seed):xpb1=seed:seed=smsrnd(seed):ypb1=seed mod 384
seed=smsrnd(seed):xpb2=seed:seed=smsrnd(seed):ypb2=seed mod 384
seed=smsrnd(seed):xpb3=seed:seed=smsrnd(seed):ypb3=seed mod 384

seed=smsrnd(seed):xdb0=seed mod 5
seed=smsrnd(seed):ydb0=seed mod 5
seed=smsrnd(seed):xdb1=seed mod 5
seed=smsrnd(seed):ydb1=seed mod 5
seed=smsrnd(seed):xdb2=seed mod 5
seed=smsrnd(seed):ydb2=seed mod 5
seed=smsrnd(seed):xdb3=seed mod 5
seed=smsrnd(seed):ydb3=seed mod 5

'set screenmode - copiar de cmjn - includes tambem
'20 screen1:width32:keyoff:color 1,14,1:cls
'30 screen2,2:color1,14,1:poke&hfcaf,1

'msxcolor($1,$E,$1)
smsvdp(7,$11)  '- ink*16+border 

'msxscreen(2,0,0,0)
'msx16x16sprites() '- sets 16x16 sprite size, i have no idea how to do it otherwise?
smsvdp(0,%00000010):smsvdp(1,%11100010) '- screen2,2 (graphics1)
'- vdp entries for screen2 - base vram addresses
smsvdp(2,$06) '- $1800
smsvdp(3,$FF) '- $2000
smsvdp(4,$03) '- $0000
smsvdp(5,$36) '- $1B00
smsvdp(6,$07) '- $3800

smsfilvrm($1800,$20,768)

for eee=0 to 31
sg1000putsprite(eee,0,196,0,0)
next


'- corrigir
smsldirvm($0000,@chrbmap01,$800)
smsldirvm($0800,@chrbmap01,$800)
smsldirvm($1000,@chrbmap01,$800)
smsldirvm($2000,@chrattr01,$800)
smsldirvm($2800,@chrattr01,$800)
smsldirvm($3000,@chrattr01,$800)
smsldirvm($3800,@spr1,$800)

'-------------------------------------------------------------------------------
'title
'if debug band 4<>0 then
  smsldirvm($0300,@lowercase01,$100)
  smsldirvm($0B00,@lowercase01,$100)
  smsldirvm($1300,@lowercase01,$100)
  smsfilvrm($2300,$1F,$100)
  smsfilvrm($2B00,$1F,$100)
  smsfilvrm($3300,$1F,$100)
  smsldirvm($0400,@titletiles01,$400)
  smsldirvm($0C00,@titletiles01,$400)
  smsldirvm($1400,@titletiles01,$400)
  smsfilvrm($2400,$1F,$400)
  smsfilvrm($2C00,$1F,$400)
  smsfilvrm($3400,$1F,$400)
  smsfilvrm($2528,$5F,$2D8)
  smsfilvrm($2D28,$5F,$2D8)
  smsfilvrm($3528,$5F,$2D8)
  smsfilvrm($27D8,$1F,3)
  smsfilvrm($27E0,$1F,3)
  smsfilvrm($2FD8,$1F,3)
  smsfilvrm($2FE0,$1F,3)
  for eee=0 to 7
    smsldirvm($1845+eee*32,@titletileschr01+eee*22,22)
    next
  smsldirvm($1800+14*32+2,@texttitle01,@texttitle02-@texttitle01)
  smsldirvm($1800+15*32+4,@texttitle02,@texttitle03-@texttitle02)
  smsldirvm($1800+16*32+4,@texttitle03,@texttitle04-@texttitle03)
  smsldirvm($1800+17*32+2,@texttitle04,@texttitle05-@texttitle04)
  smsldirvm($1800+18*32+4,@texttitle05,@texttitle06-@texttitle05)
  smsldirvm($1800+22*32+2,@texttitle06,@texttitle07-@texttitle06)

''titleloop02:
''  jst0=(cvjoypad1a() band cvjoypad2a()) bxor $7F
''  smsvpoke($1800,48+int(jst0/100) mod 10)
''  smsvpoke($1801,48+int(jst0/10) mod 10)
''  smsvpoke($1802,48+jst0 mod 10)
''  goto titleloop02

titleloop01:
  jst0=(cvjoypad1a() band cvjoypad2a()) bxor $7F
  if (jst0 band 240)=0 then:goto titleloop01:end if

'cls
smsfilvrm($1800,$20,768)

'- corrigir
smsldirvm($0000,@chrbmap01,$800)
smsldirvm($0800,@chrbmap01,$800)
smsldirvm($1000,@chrbmap01,$800)
smsldirvm($2000,@chrattr01,$800)
smsldirvm($2800,@chrattr01,$800)
smsldirvm($3000,@chrattr01,$800)
smsldirvm($3800,@spr1,$800)

gosub 8000

ep=0:cp=0:xl=2:yl=7:gosub 9000
ep=1:cp=1:xl=10:yl=1:gosub 9000
ep=2:cp=2:xl=24:yl=5:gosub 9000
ep=6:cp=3:xl=12:yl=10:gosub 9000
ep=4:cp=4:xl=20:yl=14:gosub 9000
ep=5:cp=5:xl=7:yl=16:gosub 9000

xq=160:yq=96:sq=32

150:
x=0
do
  x=x+1
  xdv=int(x/4)
  jst0=(cvjoypad1a() band cvjoypad2a())   bxor $7F
  if (jst0 band 8)=0 then:xq=xq+2:end if '-right
  if (jst0 band 2)=0 then:xq=xq-2:end if '-left
  if (jst0 band 1)=0 then:yq=yq+2:end if '- down
  if (jst0 band 4)=0 then:yq=yq-2:end if '- up
  xq=xq band $FF:yq=yq band $FF
  
  sq=33:if (jst0 band 240)=0 then:sq=32:end if '- buttons
  
  c=xdv mod 4
  ''smsvpoke($1800,48+c)  'locate 0,0:print c

  'smsvpoke($1803,48+(int(jst0/100) mod 10))
  'smsvpoke($1804,48+(int(jst0/10) mod 10))
  'smsvpoke($1805,48+(jst0 mod 10))
  
  ym=128
  if c mod 2<>1 then:ym=127:end if
  d=c
  if c=0 then:d=2:end if
  
  sg1000putsprite(0,xq,yq,sq*4,1)
  
  ''sg1000putsprite(10,255-xdv,ym,0,1)
  ''sg1000putsprite(11,255-xdv,ym+16,d*4,1)


  cb0=1:if (abs(xq-((xpb0/2)band $FF))<16) and (abs(yq-4-((ypb0/2)band $FF))<20) then:
    cb0=8
    if (jst0 band 240)<>0 then:
      seed=smsrnd(seed):xpb0=seed:seed=smsrnd(seed):ypb0=seed mod 384
      seed=smsrnd(seed):xdb0=seed mod 5:seed=smsrnd(seed):ydb0=seed mod 5
      end if
    end if
  sg1000putsprite(20,xpb0/2,ypb0/2,((xdb0>2)*-8)*4,cb0)
  sg1000putsprite(21,xpb0/2,ypb0/2+16,(d+((xdb0>2)*-8))*4,cb0)
  xpb0=xpb0+xdb0+510:ypb0=ypb0+ydb0+510

  cb1=1:if (abs(xq-((xpb1/2)band $FF))<16) and (abs(yq-4-((ypb1/2)band $FF))<20) then:
    cb1=8
    if (jst0 band 240)<>0 then:
      seed=smsrnd(seed):xpb1=seed:seed=smsrnd(seed):ypb1=seed mod 384
      seed=smsrnd(seed):xdb1=seed mod 5:seed=smsrnd(seed):ydb1=seed mod 5
      end if
    end if
  sg1000putsprite(22,xpb1/2,ypb1/2,((xdb1>2)*-8)*4,cb1)
  sg1000putsprite(23,xpb1/2,ypb1/2+16,(d+((xdb1>2)*-8))*4,cb1)
  xpb1=xpb1+xdb1+510:ypb1=ypb1+ydb1+510

  cb2=1:if (abs(xq-((xpb2/2)band $FF))<16) and (abs(yq-4-((ypb2/2)band $FF))<20) then:
    cb2=8
    if (jst0 band 240)<>0 then:
      seed=smsrnd(seed):xpb2=seed:seed=smsrnd(seed):ypb2=seed mod 384
      seed=smsrnd(seed):xdb2=seed mod 5:seed=smsrnd(seed):ydb2=seed mod 5
      end if
    end if
  sg1000putsprite(24,xpb2/2,ypb2/2,((xdb2>2)*-8)*4,cb2)
  sg1000putsprite(25,xpb2/2,ypb2/2+16,(d+((xdb2>2)*-8))*4,cb2)
  xpb2=xpb2+xdb2+510:ypb2=ypb2+ydb2+510

  cb3=1:if (abs(xq-((xpb3/2)band $FF))<16) and (abs(yq-4-((ypb3/2)band $FF))<20) then:
    cb3=8
    if (jst0 band 240)<>0 then:
      seed=smsrnd(seed):xpb3=seed:seed=smsrnd(seed):ypb3=seed mod 384
      seed=smsrnd(seed):xdb3=seed mod 5:seed=smsrnd(seed):ydb3=seed mod 5
      end if
    end if
  sg1000putsprite(26,xpb3/2,ypb3/2,((xdb3>2)*-8)*4,cb3)
  sg1000putsprite(27,xpb3/2,ypb3/2+16,(d+((xdb3>2)*-8))*4,cb3)
  xpb3=xpb3+xdb3+510:ypb3=ypb3+ydb3+510

  asm
    halt
    end asm
  loop

8000:
'locate 0,22:print"   bill: 2/12   system: 9/1/0      level:  2    score:    153";:return
smsldirvm($1800+22*32,@text01,@text02-@text01)
return

for i=0 to 255
  smsvpoke($1800+i,i)
  next

do:loop

9000:
for y=0 to 4
  ad=xl+(32*(yl+y))
  smsldirvm($1800+ad,@computers01+cp*35+y*7,7)
  next

9040:
for y=0 to 1
  ad=33+xl+(32*(yl+y))
  smsldirvm($1800+ad,@operatingsystems01+ep*6+y*3,3)
  next
return

do:loop

9100:
return

do:loop

computers01:
asm
defb $6e,$61,$61,$61,$6f,$70,$71,$80,$20,$20,$20,$8e,$8f,$90,$80,$20,$20,$20,$ae,$af,$b0,$ce,$cf,$d0,$d1,$d2,$d3,$e8,$ee,$ef,$f0,$f1,$f2,$f3,$20
defb $64,$65,$65,$65,$66,$20,$20,$a0,$20,$20,$20,$86,$87,$88,$a0,$20,$20,$20,$a6,$a7,$a8,$c4,$c5,$84,$85,$c6,$c7,$c8,$e4,$e5,$a4,$a5,$e6,$e7,$20
defb $75,$61,$61,$61,$76,$20,$20,$80,$20,$20,$20,$95,$96,$97,$80,$20,$20,$20,$b5,$b6,$b7,$91,$92,$93,$94,$72,$73,$74,$b1,$b2,$b2,$b4,$77,$78,$20
defb $60,$61,$61,$61,$62,$20,$20,$80,$20,$20,$20,$a9,$aa,$ab,$80,$20,$20,$20,$c9,$ca,$cb,$c0,$c1,$81,$82,$e9,$ea,$eb,$e0,$e1,$a1,$a2,$e2,$e3,$20
defb $67,$68,$68,$68,$6c,$20,$20,$8d,$20,$20,$20,$8c,$20,$20,$8d,$20,$20,$20,$ac,$ad,$20,$cc,$cd,$69,$69,$6a,$6b,$20,$ec,$ed,$6d,$89,$8a,$8b,$20
defb $60,$61,$61,$61,$62,$20,$20,$80,$20,$20,$20,$63,$83,$20,$80,$20,$20,$20,$63,$a3,$20,$c0,$c1,$81,$82,$c2,$c3,$20,$e0,$e1,$a1,$a2,$e2,$e3,$20
end asm

operatingsystems01:
asm
defb $1a,$1b,$1c,$1d,$1e,$1f
defb $7e,$7f,$9e,$9f,$be,$bf
defb $9b,$9c,$9d,$bb,$bc,$bd
defb $98,$99,$9a,$b8,$b9,$ba
defb $dd,$de,$df,$fd,$fe,$ff
defb $7b,$7c,$7d,$20,$79,$7a
defb $d4,$d5,$d6,$f4,$f5,$f6
defb $da,$db,$dc,$fa,$fb,$fc
defb $d7,$d8,$d9,$f7,$f8,$f9
end asm

text01:
asm
defb "   BILL: 2/12   SYSTEM: 9/1/0      LEVEL:  2    SCORE:    153"
end asm
text02:

textabout01:
asm
defb "XBill v2.1"
end asm
textabout02:
asm
defb "The Story:"
end asm
textabout03:
asm
defb "Yet aqain, the fate of the world rests in your hands!"
end asm
textabout04:
asm
defb "An evil computer cracker, known only by his handle 'Bill', has created the ultimate computer virus."
end asm
textabout05:
asm
defb "A virus so powerful that it has the power to transmute an ordinary computer into a toaster oven."
end asm
textabout06:
asm
defb "(oooh!)"
end asm
textabout07:
asm
defb "'Bill' has cloned himself into a billion-jillion micro Bills."
end asm
textabout08:
asm
defb "Their sole purpose is to deliver the nefarious virus, which has been cleverly diguised as a popular operating system."
end asm
textabout09:
asm
defb "As System Administrator/Exterminator, your job is to keep Bill from succeeding at his task."
end asm
textabout10:
asm
defb "[OK]"
end asm
textabout11:
asm
defb "XBill was created by Brian Wellington & Matias Duarte."
end asm
textabout12:

'01234567890123456789012345678901

texttitle01:
asm
defb "Original Version, 1994"
end asm
texttitle02:
asm
defb "Brian Wellington"
end asm
texttitle03:
asm
defb "Matias Duarte"
end asm
texttitle04:
asm
defb "MSX1 Version, 2018"
end asm
texttitle05:
asm
defb "Paulo Silva"
end asm
texttitle06:
asm
defb "Push any button"
end asm
texttitle07:


