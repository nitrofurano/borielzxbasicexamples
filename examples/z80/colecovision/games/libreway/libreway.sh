rm libreway.asm libreway.rom
zxb.py libreway.bas --asm --org=$((0x7FF0))
zxb.py libreway.bas --org=$((0x7FF0))
dd ibs=1 skip=$((0x0010)) if=libreway.bin of=libreway.rom
rm libreway.bin
mame coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 libreway.rom

