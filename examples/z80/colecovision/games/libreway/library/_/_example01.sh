rm example01.asm example01.sg
zxb.py example01.bas --asm --org=0x0000
zxb.py example01.bas --org=0x0000
cat example01.bin >> example01.tmr
filesize=$(stat -c%s "example01.tmr")
dd bs=40000 count=1 if=/dev/zero of=_dummybytes.bin
#if [ $filesize -le 32768 ]; then dd bs=32768 count=1 if=/dev/zero of=_dummybytes.bin ; fi
#if [ $filesize -le 16384 ]; then dd bs=16384 count=1 if=/dev/zero of=_dummybytes.bin ; fi
#if [ $filesize -le  8192 ]; then dd bs=8192 count=1 if=/dev/zero of=_dummybytes.bin ; fi
cat _dummybytes.bin >> example01.tmr
rm _dummybytes.bin example01.bin
if [ $filesize -le 32768 ]; then dd ibs=1 count=$((0x8000)) skip=$((0x0000)) if=example01.tmr of=example01.sg ; fi
if [ $filesize -le 16384 ]; then dd ibs=1 count=$((0x4000)) skip=$((0x0000)) if=example01.tmr of=example01.sg ; fi
#if [ $filesize -le  8192 ]; then dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=example01.tmr of=example01.sg ; fi
rm example01.tmr

mess sg1000 -video soft -w -bp ~/.mess/roms -resolution0 560x432 -cart1 example01.sg




