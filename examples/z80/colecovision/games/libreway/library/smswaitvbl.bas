sub smswaitvbl(ttm as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)

    jp smswaitvblstp2    
    smswaitvblloop:
    dec bc
    halt
    smswaitvblstp2:
    ld a,b
    or c
    jp nz,smswaitvblloop

    end asm
  end sub

'-  look for WaitForVBlankNoInt - ?????
