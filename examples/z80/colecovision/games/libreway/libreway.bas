'#include "library/_sms_setz80stackpoint.bas"
asm
  defw $aa55,0,$7000,0,0,startboot
  defw 0,0,0,0,0,0,0,0,0,0,0
  push af 
  in a,($bf) 
  pop af 
  ei 
  reti
  startboot:
  end asm

'#include "library/_sms_setz80stackpoint.bas"
#include "library/smsvdp.bas"
#include "library/smsfilvrm.bas"
#include "library/smsldirvm.bas"
#include "library/smsvpoke.bas"
#include "library/sg1000putsprite.bas"
#include "library/sg1000spritest.bas"
#include "library/smswaitvbl.bas"
#include "library/smsrnd.bas"
#include "library/librewayconvxwindows8x16font_charsetsprites.bas"
#include "library/smsjoypad.bas"
#include "library/cvjoypad.bas"

'- check which variables are not being used - sg1000 ram starts at $C000 (2kb mirrored along)
'- check which variables are not being used - sg1000 ram starts at $7000 (1kb mirrored along)
dim eee as uinteger at $7010
dim qq2 as uinteger at $7012
dim cpos as uinteger at $7014
dim eeseed as uinteger at $7016
dim ee1 as uinteger at $7018
dim ee2 as uinteger at $701A
dim xpos1 as uinteger at $7020
dim ypos1 as uinteger at $7022
dim xsid1 as uinteger at $7024
dim xfrm1 as uinteger at $7026
dim xpos2 as uinteger at $7028
dim ypos2 as uinteger at $702A
dim xsid2 as uinteger at $702C
dim xfrm2 as uinteger at $702E
dim spq1 as uinteger at $7030
dim spq2 as uinteger at $7032
dim gct1 as uinteger at $7034
dim gct2 as uinteger at $7036
dim ernd as uinteger at $7038
dim erndm as uinteger at $703A
dim frmpg as uinteger at $703C
dim edlim as uinteger at $703E
dim frspl as uinteger at $7040

edlim=16 '- goal limit
frspl=0 '- first play for title

'- area 10*4 (xpos,xspd, xdis) - $E080?

smsvdp(0,$00):smsvdp(1,$E2) '- screen1,2
smsvdp(2,$06):smsvdp(3,$80):smsvdp(4,$00)
smsvdp(5,$36):smsvdp(6,$07)

'msxscreen(1,2,0,0)
'smsvdp(0,%00000000):smsvdp(1,%01000010)
''- i dont know about these vdp entries, i have to fix it
'smsvdp(2,$06) '- $1800
'smsvdp(3,$FF) '- $2000
'smsvdp(4,$03) '- $0000
'smsvdp(5,$36) '- $1B00
'smsvdp(6,$07) '- $3800

' msxcolor(3,5,1) ----????
smsvdp(7,$31) 'smsvdp(7,F*16+4) '- ink*16+border - color(3,?,1)


'- mastergear needs this, i wonder why... :S 
smsfilvrm($0000,0,$4000)


'asm
'  ld bc,$E201
'  call $0047 ;WRTVDP - setting 16x16 sprites
'  end asm

'msxresetsprites() '- i have to see better what this does
'- smsfilvrm($0000,$00,$4000)

'----
'- vpokes

for eee=0 to 2047
  qq2=peek(@charset01+eee)
  smsvpoke(eee+$0000,qq2)
  smsvpoke(eee+$2000,$1F)
  next

smsvpoke($2000,$9E):smsvpoke($2001,$FE):smsvpoke($2002,$FE):smsvpoke($2003,$FE)
smsvpoke($2010,$6E):smsvpoke($2011,$4E):smsvpoke($2012,$1E):smsvpoke($2013,$1E)
smsvpoke($0009,$FF):smsvpoke($0010,$FF):smsvpoke($0012,$FF):smsvpoke($0049,$FF)

for eee=6144 to 6144+767
  smsvpoke(eee,0)
  next

for ee1=0 to 3
  for ee2=0 to 31
    smsvpoke($1800+ee2+(ee1*64),ee2+(ee1*32))
    smsvpoke($1820+ee2+(ee1*64),128+ee2+(ee1*32))
    next:next

for ee1=0 to 15
  for ee2=0 to 15
    smsvpoke($3800+ee2+(ee1*32),peek(@sprites01+ee2+(ee1*16)))
    next:next

'----
'- subs

sub drawbg():
  '-- draw backgorund
  for eee=0 to 767
    smsvpoke($1800+eee,0)
    next
  for ee2=0 to 31 step 2
    for ee1=2 to 10
      smsvpoke($1801+ee2+(ee1*64),9)
      next:next
  for ee2=0 to 31 step 2
    smsvpoke($1800+ee2+(1*64),1)
    smsvpoke($1801+ee2+(1*64),1)
    smsvpoke($1800+ee2+(11*64),1)
    smsvpoke($1801+ee2+(11*64),1)
    smsvpoke($1801+ee2+(6*64),2)
    next
  end sub

sub print2x1(txlc as uinteger, tylc as uinteger, tlad as uinteger, tlen as uinteger):
  dim tllp as uinteger at $700E
  for tllp=0 to tlen-1
    smsvpoke($1800+txlc+(tylc*32)+tllp,peek(tlad+tllp) )
    smsvpoke($1820+txlc+(tylc*32)+tllp,peek(tlad+tllp)+128 )
    next
  end sub

'-------------------------------------------------------------------------------

looptitle01:

if frspl=0 then
  drawbg()
  frspl=1
  end if

print2x1(2,12,@text01,8)
print2x1(2,17,@text02,23)
print2x1(2,20,@text03,12)

'- this delay makes no sense - replace with a key checker
'for ee1=0 to 500
'  asm
'    halt
'    end asm
'  next

loopkeyboard01:
eee=(smsjoypad1()band 15)bor(smsjoypad2()band 15)
if eee=0 then: goto loopkeyboard01:end if

'eee=$FF
'for ee1=0 to 8
'  'eee=eee band msxsnsmat(ee1)
'  next
'if eee=$FF then: goto loopkeyboard01:end if

'-------------------------------------------------------------------------------

drawbg()

'-------------------------------------------------------------------------------

xpos1=64:ypos1=174:xsid1=0:xfrm1=0
xpos2=192:ypos2=174:xsid2=4:xfrm2=0
cpos=12:ernd=0
gct1=0:gct2=0
frmpg=0

'poke $7080,128:poke $e081,1:poke $7082,45
'poke $7084, 28:poke $e085,2:poke $7086,76

for eee=0 to 39 step 4
  poke $7080+eee,ernd:ernd=smsrnd(ernd)
  poke $7081+eee,ernd band 7:ernd=smsrnd(ernd)
  poke $7082+eee,(ernd band 63)+16:ernd=smsrnd(ernd)
  poke $7083+eee,1+ernd mod 12:ernd=smsrnd(ernd)
  next

'-------------------------------------------------------------------------------

loopgameplay01:



'----
'- control

if (cvjoypad1a() band 2)=0 then:     'right
'if (smsjoypad1() band 8)<>0 then:
  xpos1=xpos1+2:xfrm1=xfrm1+1:xsid1=0
  end if

if (cvjoypad1a() band 8)=0 then:     'left
'if (smsjoypad1() band 4)<>0 then:
  xpos1=xpos1-2:xfrm1=xfrm1+1:xsid1=4
  end if

if (cvjoypad1a() band 4)=0 then:     'down
'if (smsjoypad1() band 2)<>0 then:
  ypos1=ypos1+2:xfrm1=xfrm1+1
  end if

if (cvjoypad1a() band 1)=0 then:     'up
'if (smsjoypad1() band 1)<>0 then:
  ypos1=ypos1-2:xfrm1=xfrm1+1
  end if



if (cvjoypad2a() band 2)=0 then:     'right
  xpos2=xpos2+2:xfrm2=xfrm2+1:xsid2=0
  end if

if (cvjoypad2a() band 8)=0 then:     'left
  xpos2=xpos2-2:xfrm2=xfrm2+1:xsid2=4
  end if

if (cvjoypad2a() band 4)=0 then:     'down
  ypos2=ypos2+2:xfrm2=xfrm2+1
  end if

if (cvjoypad2a() band 1)=0 then:     'up
  ypos2=ypos2-2:xfrm2=xfrm2+1
  end if


'----
'- ??? - what is cpos - deprecated colour in a test?
'if (msxsnsmat(8) band 1)=0 then:
'  cpos=(cpos+1)band 15
'  end if

'----
'- ???

xfrm1=xfrm1 mod 4:xfrm2=xfrm2 mod 4
spq1=(xfrm1+(xsid1)):spq2=(xfrm2+(xsid2))

'----
'- sprites
sg1000putsprite(0,xpos1,ypos1,spq1*4,6)
sg1000putsprite(1,xpos2,ypos2,spq2*4,4)

for eee=0 to 4
  sg1000putsprite(2+(eee*2),peek($7080+(eee*4)),15+(eee*16),(8+((frmpg/2) band 3))*4,peek($7083+(eee*4)))
  sg1000putsprite(3+(eee*2),peek($7080+(eee*4))+peek($7082+(eee*4)),15+(eee*16),(8+((frmpg/2) band 3))*4,peek($7083+(eee*4)))
  poke $7080+(eee*4),(peek($7080+(eee*4))+1+peek($7081+(eee*4)))
  next

for eee=5 to 9
  sg1000putsprite(2+(eee*2),peek($7080+(eee*4)),15+(eee*16),(12+((frmpg/2) band 3))*4,peek($7083+(eee*4)))
  sg1000putsprite(3+(eee*2),peek($7080+(eee*4))+peek($7082+(eee*4)),15+(eee*16),(12+((frmpg/2) band 3))*4,peek($7083+(eee*4)))
  poke $7080+(eee*4),(peek($7080+(eee*4))+254- peek($7081+(eee*4)) )
  next

'----
'- test - collision with "enemies"
eee=int((ypos1+0)/16)
if (abs (peek($7080+(eee*4))-xpos1)<8) or (abs (peek($7080+(eee*4))+peek($7082+(eee*4))-xpos1)<8) then
  ypos1=ypos1+16
  end if

eee=int((ypos2+0)/16)
if (abs (peek($7080+(eee*4))-xpos2)<8) or (abs (peek($7080+(eee*4))+peek($7082+(eee*4))-xpos2)<8) then
  ypos2=ypos2+16
  end if

'----

ernd=smsrnd(ernd) bxor xpos1
ernd=smsrnd(ernd) bxor xpos2
erndm=(ernd band 3)*2

'----
'- goal scoring

if ypos1<1 then:
  smsvpoke($1800+gct1,128+erndm)
  smsvpoke($1820+gct1,129+erndm)
  gct1=gct1+1
  ypos1=176
  end if

if ypos2<1 then:
  smsvpoke($1800+31-gct2,128+8+erndm)
  smsvpoke($1820+31-gct2,128+9+erndm)
  gct2=gct2+1
  ypos2=176
  end if

'----
'- hitting other borders

if ypos1>176 then:ypos1=176:end if
if ypos2>176 then:ypos2=176:end if
if xpos1<2 then:xpos1=2:end if
if xpos2<2 then:xpos2=2:end if
if xpos1>246 then:xpos1=246:end if
if xpos2>246 then:xpos2=246:end if

'----
'- ???

ernd=smsrnd(ernd):eee=ernd:ernd=smsrnd(ernd)
if (ernd band 255)<64 then
  ernd=smsrnd(ernd):eee=ernd:ernd=smsrnd(ernd)
  poke $e081+(ernd band $3C),eee band 7
  end if

frmpg=frmpg+1
asm
  halt
  end asm

'----
'- goal limit

if (gct1<edlim) and (gct2<edlim) then:
  goto loopgameplay01
  end if

'----
'- game over delay
for ee1=0 to 100
  asm
    halt
    end asm
  next

goto looptitle01

'-------------------------------------------------------------------------------

text01:
asm
  defb "Libreway"
  end asm
text02:
asm
  defb "Paulo Silva, jan-jun'14"
  end asm
text03:
asm
  defb "Push any key"
  end asm
text04:

'-------------------------------------------------------------------------------

