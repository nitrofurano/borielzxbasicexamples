rm numericmaze.asm numericmaze.rom
zxb.py numericmaze.bas --asm --$((org=0x7FF0))
zxb.py numericmaze.bas --org=$((0x7FF0))
dd ibs=1 skip=$((0x0010)) if=numericmaze.bin of=numericmaze.rom
rm numericmaze.bin
mame coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 numericmaze.rom

