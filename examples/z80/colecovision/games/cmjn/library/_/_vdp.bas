asm

  ld hl,vdpinitial
  ld b,vdpinitialend-vdpinitial
  ld c,$bf
  otir

  vdpinitial:
    defb $00,$80
    defb $40,$81
    defb $0f,$82
    defb $00,$83
    defb $01,$84
    defb $01,$85
    defb $04,$86
    defb $0E,$87
  vdpinitialend:

  end asm

