

asm
  clearvram:
    ld hl,$4000
  setvdpaddress:
    ld a,l
    out ($bf),a
    ld a,h
    out ($bf),a
  clearvramcont:
    ld bc,$4000    
    ld a,$23     
  clearvramloop:
    out ($be),a 
    dec c
    jr nz,clearvramloop
    dec b
    jr nz,clearvramloop
  end asm
