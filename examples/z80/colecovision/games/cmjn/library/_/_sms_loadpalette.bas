asm
  ; Load palette
  ; 1. Set VRAM write address to CRAM (palette) address 0 (for palette index 0)
  ; by outputting $c000 ORed with $0000
  ld a,$00
  out ($bf),a
  ld a,$c0
  out ($bf),a
  ; 2. Output colour data
  ld hl,PaletteData
  ld b,(PaletteDataEnd-PaletteData)
  ld c,$be
  otir

  jp PaletteDataEnd

  PaletteData:

  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877
  defw $0457
  defw $0877

  ;defb %00011111
  ;defb %00100000

  ;defb $00
  ;defb $3f

  PaletteDataEnd:

  end asm
