rm poleposition.asm poleposition.rom
zxb.py poleposition.bas --asm --org=0x7FF0
zxb.py poleposition.bas --org=0x7FF0
dd ibs=1 skip=$((0x0010)) if=poleposition.bin of=poleposition.rom
rm poleposition.bin

mess coleco -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 poleposition.rom

