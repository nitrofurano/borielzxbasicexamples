goto railstripes01end:

railstripes01:

asm
  defb %00100100
  defb %10011100
  defb %01100110
  defb %01000100
  defb %00000100
  defb %10101110
  defb %01100100
  defb %00000100
  defb %10111100
  defb %00001110
  defb %11000000
  defb %00100000
  defb %11100000
  defb %11011000
  defb %10000000
  defb %01000000
  defb %00111000
  defb %00000110
  defb %11111110
  defb %11100110
  defb %11011110
  defb %10100110
  defb %10011110
  defb %10000010
  defb %01110010
  defb %01101010
  defb %01000010
  defb %00110110
  defb %00101100
  defb %00100100
  defb %00010100
  defb %00001000
  defb %00000000
  defb %11110000
  defb %11101000
  defb %11100100
  defb %11010000
  defb %11001010
  defb %11000010
  defb %10111110
  defb %10110010
  defb %10101010
  defb %10100110
  defb %10100000
  defb %10011000
  defb %10010100
  defb %10010000
  defb %10001010
  defb %10000110
  defb %10000010
  defb %01111000
  defb %01110100
  defb %01110010
  defb %01101110
  defb %01101000
  defb %01100100
  defb %01100010
  defb %01011110
  defb %01011000
  defb %01010100
  defb %01010010
  defb %01001110
  defb %01001000
  defb %01000100
  defb %01000010
  defb %01000000
  defb %00111100
  defb %00111010
  defb %00110100
  defb %00110010
  defb %00110000
  defb %00101100
  defb %00101010
  defb %00101000
  defb %00100110
  defb %00100100
  defb %00100010
  defb %00100000
  defb %00011110
  defb %00011100
  defb %00011010
  defb %00011000
  defb %00010110
  defb %00010100
  defb %00010010
  defb %00010000
  defb %00001110
  defb %00001100
  defb %00001010
  defb %00001000
  defb %00001000
  defb %00000110
  defb %00000100
  defb %00000010
  defb %00000001
  defb %00000000


  end asm



asm
  defb %10010010
  defb %01001110
  defb %00110011
  defb %10100010
  defb %00000010
  defb %01010111
  defb %10110010
  defb %00000010
  defb %01011110
  defb %00000111
  defb %11100000
  defb %10010000
  defb %01110000
  defb %01101100
  defb %01000000
  defb %00100000
  defb %00011100
  defb %00000011
  defb %11111111
  defb %11110011
  defb %11101111
  defb %11010011
  defb %11001111
  defb %11000001
  defb %10111001
  defb %10110101
  defb %10100001
  defb %10011011
  defb %10010110
  defb %10010010
  defb %10001010
  defb %10000100
  defb %10000000
  defb %01111000
  defb %01110100
  defb %01110010
  defb %01101000
  defb %01100101
  defb %01100001
  defb %01011111
  defb %01011001
  defb %01010101
  defb %01010011
  defb %01010000
  defb %01001100
  defb %01001010
  defb %01001000
  defb %01000101
  defb %01000011
  defb %01000001
  defb %00111100
  defb %00111010
  defb %00111001
  defb %00110111
  defb %00110100
  defb %00110010
  defb %00110001
  defb %00101111
  defb %00101100
  defb %00101010
  defb %00101001
  defb %00100111
  defb %00100100
  defb %00100010
  defb %00100001
  defb %00100000
  defb %00011110
  defb %00011101
  defb %00011010
  defb %00011001
  defb %00011000
  defb %00010110
  defb %00010101
  defb %00010100
  defb %00010011
  defb %00010010
  defb %00010001
  defb %00010000
  defb %00001111
  defb %00001110
  defb %00001101
  defb %00001100
  defb %00001011
  defb %00001010
  defb %00001001
  defb %00001000
  defb %00000111
  defb %00000110
  defb %00000101
  defb %00000100
  defb %00000100
  defb %00000011
  defb %00000010
  defb %00000001
  defb %00000000
  defb %00000000
  end asm

'asm
'  defb $10,$0C,$08,$00,$F8,$F0,$E8,$E0
'  defb $D8,$D0,$CC,$C8,$C0,$BC,$B8,$B0
'  defb $AC,$A8,$A4,$A0,$9C,$98,$96,$93
'  defb $90,$8C,$88,$84,$80,$7D,$7A,$78
'  defb $76,$73,$70,$6D,$6B,$68,$64,$60
'  defb $5D,$5A,$58,$56,$53,$50,$4E,$4C
'  defb $4A,$48,$46,$44,$42,$40,$3D,$3A
'  defb $38,$36,$34,$33,$31,$30,$2F,$2D
'  defb $2B,$29,$28,$26,$24,$22,$20,$1E
'  defb $1D,$1B,$1A,$18,$17,$15,$14,$12
'  defb $11,$10,$0F,$0E,$0D,$0B,$0A,$09
'  defb $08,$06,$05,$04,$03,$02,$01,$00
'  end asm

railstripes01end:

