asm

;-------------------------------
;--- zx81sys.asm ---
;system variables
;Origin of a ZX81 file is always 16393
org 16393
; System variables live here
VERSN:    defb 0
E_PPC:    defw 2
D_FILE:   defw Display
DF_CC:    defw Display+1                  ; First character of display
VARS:     defw Variables
DEST:     defw 0
E_LINE:   defw BasicEnd 
CH_ADD:   defw BasicEnd+4                 ; Simulate SAVE "X"
X_PTR:    defw 0
STKBOT:   defw BasicEnd+5
STKEND:   defw BasicEnd+5                 ; Empty stack
BREG:     defb 0
MEM:      defw MEMBOT
UNUSED1:  defb 0
DF_SZ:    defb 2
S_TOP:    defw $0002                      ; Top program line number
LAST_K:   defw $fdbf
DEBOUN:   defb 15
MARGIN:   defb 55
NXTLIN:   defw Line2                      ; Next line address
OLDPPC:   defw 0
FLAGX:    defb 0
STRLEN:   defw 0
T_ADDR:   defw $0c8d
SEED:     defw 0
FRAMES:   defw $f5a3
COORDS:   defw 0
PR_CC:    defb $bc
S_POSN:   defw $1821
CDFLAG:   defb $40
PRBUFF:   defb 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$76 ; 32 Spaces + Newline
MEMBOT:   defb 0,0,0,0,0,0,0,0,0,0,$84,$20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ; 30 zeros
UNUNSED2: defw 0
; End of system variables

;-------------------------------
;--- line1.asm ---

;Line 1
;this is the REM statement...
Line1:
  defb $00,$00                    ; Line 1
  defw Line1End-Line1Text         ; Line 1 length
Line1Text:
  defb $ea                        ; REM             

;-------------------------------

end asm

