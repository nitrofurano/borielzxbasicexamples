function fastcall zx81keyboard(ttm as ubyte) as ubyte:
  asm
    xor $FF
    in a,($FE)
    xor $FF
    and %11111
    end asm
  end function

'- input: 1,2,4,8,16,32,64,128
'- output: 5 bits from the pressed keys
