sub fastcall zx81delay(ttm as uinteger):
  asm
    ld b,h
    ld c,l
    ;ld b,(ix+5)
    ;ld c,(ix+4)

    push bc
    pacmandelayloop:
    dec bc
    ld a,b
    or c
    jp nz,pacmandelayloop
    pop bc

    end asm
  end sub


