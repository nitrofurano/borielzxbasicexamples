sub fastcall zx81ldirvarsys()
  asm
    ld de,($4060)
    ld hl,($4062)
    ld bc,($4064)
    ;ld d,(ix+5)
    ;ld e,(ix+4)
    ;ld h,(ix+7)
    ;ld l,(ix+6)
    ;ld b,(ix+9)
    ;ld c,(ix+8)
    ldir
    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
