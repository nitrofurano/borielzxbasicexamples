sub fastcall zx81push(ttm as uinteger):
  asm
    push hl
    end asm
  end sub

'- input: 1,2,4,8,16,32,64,128
'- output: 5 bits from the pressed keys
