asm

;-------------------------------
;--- zx81sys.asm ---
;system variables
;Origin of a ZX81 file is always 16393
org 16393   ;$4009
; System variables live here
VERSN:    defb 0          ;$4009
E_PPC:    defw 2          ;$400A
D_FILE:   defw Display    ;$400C
DF_CC:    defw Display+1  ;$400E - First character of display
VARS:     defw Variables  ;$4010
DEST:     defw 0          ;$4012
E_LINE:   defw BasicEnd   ;$4014
CH_ADD:   defw BasicEnd+4 ;$4016 - Simulate SAVE "X"
X_PTR:    defw 0          ;$4018
STKBOT:   defw BasicEnd+5 ;$401A
STKEND:   defw BasicEnd+5 ;$401C - Empty stack
BREG:     defb 0          ;$401E
MEM:      defw MEMBOT     ;$401F
UNUSED1:  defb 0          ;$4021
DF_SZ:    defb 2          ;$4022
S_TOP:    defw $0002      ;$4023 - Top program line number
LAST_K:   defw $fdbf      ;$4025
DEBOUN:   defb 15         ;$4027
MARGIN:   defb 55         ;$4028
NXTLIN:   defw Line2      ;$4029 - Next line address
OLDPPC:   defw 0          ;$402A
FLAGX:    defb 0          ;$402C
STRLEN:   defw 0          ;$402D
T_ADDR:   defw $0c8d      ;$402F
SEED:     defw 0          ;$4031
FRAMES:   defw $f5a3      ;$4033
COORDS:   defw 0          ;$4035
PR_CC:    defb $bc        ;$4037
S_POSN:   defw $1821      ;$4038
CDFLAG:   defb $40        ;$403A                            
PRBUFF:   defb 0,0,0,0,0                       ;$403B..$403F
          defb 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ;$4040..$404F
          defb 0,0,0,0,0,0,0,0                 ;$4050..$4057
          defb 0,0,0,$76 ; 32 Spaces + Newline ;$4058..$405B
MEMBOT:   defb 0,0,0,0                         ;$405C..$405F
          defb 0,0,0,0,0,0,$84,$20             ;$4060..$4067
          defb 0,0,0,0,0,0,0,0                 ;$4068..$406F
          defb 0,0,0,0,0,0,0,0,0,0 ; 30 zeros  ;$4070..$4079
UNUNSED2: defw 0          ;$407A
; End of system variables

;-------------------------------
;--- line1.asm ---

;Line 1
;this is the REM statement...
Line1:
  defb $00,$00                    ; Line 1
  defw Line1End-Line1Text         ; Line 1 length
Line1Text:
  defb $ea                        ; REM             

;-------------------------------

end asm

