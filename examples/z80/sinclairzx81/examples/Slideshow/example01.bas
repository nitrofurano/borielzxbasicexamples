#include "library/zx81header.bas"

'asm
'  ld sp, codeend+8000
'  end asm

#include "library/zx81delay.bas"
#include "library/zx81rnd.bas"
#include "library/zx81keyboard.bas"
'#include "library/pacmanldir.bas"
#include "library/zx81push.bas"
#include "library/zx81ldirvarsys.bas"


goto pictures01end:
pictures01:
asm
  incbin "library_pics/gnu.png.bin"
  incbin "library_pics/tux.png.bin"
  incbin "library_pics/debian.png.bin"
  incbin "library_pics/lena.png.bin"
  incbin "library_pics/zx81.png.bin"
  incbin "library_pics/pcb.png.bin"
  incbin "library_pics/4thinternational.png.bin"
  incbin "library_pics/mewebcam.png.bin"
  end asm
pictures01end:

dim eee as uinteger at $403C
dim seed as uinteger at $4031
dim ee0 as uinteger at $403E
dim ee1 as uinteger at $4040
dim ee2 as uinteger at $4042
dim ee3 as uinteger at $4044
dim ee4 as uinteger at $4046
dim ee5 as uinteger at $4048
dim eex as uinteger at $404A
dim eey as uinteger at $404C

dim t0 as uinteger at $4060
dim t1 as uinteger at $4062
dim t2 as uinteger at $4064


do
'for eee=0 to 9

while eee=ee5
  seed=zx81rnd(seed)
  eee=seed mod 8
  end while
ee5=eee
  for ee0=0 to 23
    ee1=ee0*33
    ee2=eee*768+ee0*32
    t0=peek(uinteger,16396)+1+ee1
    t1=@pictures01+ee2
    t2=32
    zx81ldirvarsys()
    next
  zx81delay(50000)
loop

do:loop


for ee1=0 to 23
  for ee0=0 to 31
    seed=zx81rnd(seed)
    ee2=seed mod 11
    ee5=seed band 128
    ee2=ee2 bor ee5
    ee3=ee1*33+ee0
    poke (peek(uinteger,16396)+1+ee3),ee2
    zx81delay(100)
    next:next

  for ee1=0 to 7
    for ee0=0 to 15
      ee3=ee1*33+ee0
      ee4=ee1*16+ee0+64
      'seed=zx81rnd(seed)
      ee2=ee4 band %10111111
      poke (peek(uinteger,16396)+1+330+15+165+ee3),ee2
      zx81delay(100)
      next:next

eex=8
eey=8
eee=0
do
  ee5=int(eee/10000):poke (peek(uinteger,16396)+34+1),28+(ee5 mod 10)
  ee5=int(eee/1000):poke (peek(uinteger,16396)+34+2),28+(ee5 mod 10)
  ee5=int(eee/100):poke (peek(uinteger,16396)+34+3),28+(ee5 mod 10)
  ee5=int(eee/10):poke (peek(uinteger,16396)+34+4),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+34+5),28+(eee mod 10)



  ee1=zx81keyboard(1)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+33+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+33+31),28+(ee1 mod 10)

  ee1=zx81keyboard(2)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+66+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+66+31),28+(ee1 mod 10)

  eex=eex-(ee1 band 1)+((ee1 band 4)/4)
  eey=eey+((ee1 band 2)/2)

  ee1=zx81keyboard(4)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+99+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+99+31),28+(ee1 mod 10)

  eey=eey-((ee1 band 2)/2)

  ee1=zx81keyboard(8)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+132+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+132+31),28+(ee1 mod 10)

  ee1=zx81keyboard(16)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+165+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+165+31),28+(ee1 mod 10)

  ee1=zx81keyboard(32)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+198+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+198+31),28+(ee1 mod 10)

  ee1=zx81keyboard(64)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+231+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+231+31),28+(ee1 mod 10)

  ee1=zx81keyboard(128)
  ee5=int(ee1/10):poke (peek(uinteger,16396)+264+30),28+(ee5 mod 10)
  poke (peek(uinteger,16396)+264+31),28+(ee1 mod 10)

  ee5=eey*33+eex
  poke (peek(uinteger,16396)+1+ee5),eee band %10111111


  zx81delay(1000)
  eee=eee+1
  loop

asm
codeend:
end asm

#include "library/zx81tail.bas"

