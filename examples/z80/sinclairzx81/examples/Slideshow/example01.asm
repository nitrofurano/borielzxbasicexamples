	org 0
__START_PROGRAM:
	di
	push ix
	push iy
	exx
	push hl
	exx
	ld hl, 0
	add hl, sp
	ld (__CALL_BACK__), hl
	ei
#line 0
		org 16393
VERSN:
		defb 0
E_PPC:
		defw 2
D_FILE:
		defw Display
DF_CC:
		defw Display+1
VARS:
		defw Variables
DEST:
		defw 0
E_LINE:
		defw BasicEnd
CH_ADD:
		defw BasicEnd+4
X_PTR:
		defw 0
STKBOT:
		defw BasicEnd+5
STKEND:
		defw BasicEnd+5
BREG:
		defb 0
MEM:
		defw MEMBOT
UNUSED1:
		defb 0
DF_SZ:
		defb 2
S_TOP:
		defw $0002
LAST_K:
		defw $fdbf
DEBOUN:
		defb 15
MARGIN:
		defb 55
NXTLIN:
		defw Line2
OLDPPC:
		defw 0
FLAGX:
		defb 0
STRLEN:
		defw 0
T_ADDR:
		defw $0c8d
SEED:
		defw 0
FRAMES:
		defw $f5a3
COORDS:
		defw 0
PR_CC:
		defb $bc
S_POSN:
		defw $1821
CDFLAG:
		defb $40
PRBUFF:
		defb 0,0,0,0,0
		defb 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		defb 0,0,0,0,0,0,0,0
		defb 0,0,0,$76
MEMBOT:
		defb 0,0,0,0
		defb 0,0,0,0,0,0,$84,$20
		defb 0,0,0,0,0,0,0,0
		defb 0,0,0,0,0,0,0,0,0,0
UNUNSED2:
		defw 0
Line1:
		defb $00,$00
		defw Line1End-Line1Text
Line1Text:
		defb $ea
#line 78
	jp __LABEL__pictures01end
__LABEL__pictures01:
#line 16
		incbin "library_pics/gnu.png.bin"
		incbin "library_pics/tux.png.bin"
		incbin "library_pics/debian.png.bin"
		incbin "library_pics/lena.png.bin"
		incbin "library_pics/zx81.png.bin"
		incbin "library_pics/pcb.png.bin"
		incbin "library_pics/4thinternational.png.bin"
		incbin "library_pics/mewebcam.png.bin"
#line 24
__LABEL__pictures01end:
__LABEL0:
__LABEL2:
	ld de, (_ee5)
	ld hl, (_eee)
	call __EQ16
	or a
	jp z, __LABEL3
	ld hl, (_seed)
	call _zx81rnd
	ld (_seed), hl
	ld h, 0
	ld a, l
	and 7
	ld l, a
	ld (_eee), hl
	jp __LABEL2
__LABEL3:
	ld hl, (_eee)
	ld (_ee5), hl
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL4
__LABEL7:
	ld hl, (_ee0)
	ld de, 33
	call __MUL16_FAST
	ld (_ee1), hl
	ld hl, (_eee)
	ld de, 768
	call __MUL16_FAST
	push hl
	ld hl, (_ee0)
	ld de, 32
	call __MUL16_FAST
	ex de, hl
	pop hl
	add hl, de
	ld (_ee2), hl
	ld hl, (16396)
	inc hl
	ex de, hl
	ld hl, (_ee1)
	add hl, de
	ld (_t0), hl
	ld de, __LABEL__pictures01
	ld hl, (_ee2)
	add hl, de
	ld (_t1), hl
	ld hl, 32
	ld (_t2), hl
	call _zx81ldirvarsys
__LABEL8:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL4:
	ld hl, 23
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL7
__LABEL6:
	ld hl, 50000
	call _zx81delay
	jp __LABEL0
__LABEL1:
__LABEL9:
	jp __LABEL9
__LABEL10:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL11
__LABEL14:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL16
__LABEL19:
	ld hl, (_seed)
	call _zx81rnd
	ld (_seed), hl
	ld de, 11
	call __MODU16
	ld (_ee2), hl
	ld de, 128
	ld hl, (_seed)
	call __BAND16
	ld (_ee5), hl
	ld de, (_ee5)
	ld hl, (_ee2)
	call __BOR16
	ld (_ee2), hl
	ld hl, (_ee1)
	ld de, 33
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee0)
	add hl, de
	ld (_ee3), hl
	ld hl, (16396)
	inc hl
	ex de, hl
	ld hl, (_ee3)
	add hl, de
	push hl
	ld hl, (_ee2)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 100
	call _zx81delay
__LABEL20:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL16:
	ld hl, 31
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL19
__LABEL18:
__LABEL15:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL11:
	ld hl, 23
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL14
__LABEL13:
	ld hl, 0
	ld (_ee1), hl
	jp __LABEL21
__LABEL24:
	ld hl, 0
	ld (_ee0), hl
	jp __LABEL26
__LABEL29:
	ld hl, (_ee1)
	ld de, 33
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee0)
	add hl, de
	ld (_ee3), hl
	ld hl, (_ee1)
	ld de, 16
	call __MUL16_FAST
	ex de, hl
	ld hl, (_ee0)
	add hl, de
	ld de, 64
	add hl, de
	ld (_ee4), hl
	ld de, 191
	ld hl, (_ee4)
	call __BAND16
	ld (_ee2), hl
	ld hl, (16396)
	inc hl
	ld de, 330
	add hl, de
	ld de, 15
	add hl, de
	ld de, 165
	add hl, de
	ex de, hl
	ld hl, (_ee3)
	add hl, de
	push hl
	ld hl, (_ee2)
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 100
	call _zx81delay
__LABEL30:
	ld hl, (_ee0)
	inc hl
	ld (_ee0), hl
__LABEL26:
	ld hl, 15
	ld de, (_ee0)
	or a
	sbc hl, de
	jp nc, __LABEL29
__LABEL28:
__LABEL25:
	ld hl, (_ee1)
	inc hl
	ld (_ee1), hl
__LABEL21:
	ld hl, 7
	ld de, (_ee1)
	or a
	sbc hl, de
	jp nc, __LABEL24
__LABEL23:
	ld hl, 8
	ld (_eex), hl
	ld hl, 8
	ld (_eey), hl
	ld hl, 0
	ld (_eee), hl
__LABEL31:
	ld hl, (_eee)
	ld de, 10000
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 34
	add hl, de
	inc hl
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_eee)
	ld de, 1000
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 34
	add hl, de
	inc hl
	inc hl
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_eee)
	ld de, 100
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 34
	add hl, de
	inc hl
	inc hl
	inc hl
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_eee)
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 34
	add hl, de
	ld de, 4
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 34
	add hl, de
	ld de, 5
	add hl, de
	push hl
	ld hl, (_eee)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 1
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 33
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 33
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 2
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 66
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 66
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld de, 1
	ld hl, (_ee1)
	call __BAND16
	ex de, hl
	ld hl, (_eex)
	or a
	sbc hl, de
	push hl
	ld de, 4
	ld hl, (_ee1)
	call __BAND16
	ld de, 4
	call __DIVU16
	ex de, hl
	pop hl
	add hl, de
	ld (_eex), hl
	ld de, 2
	ld hl, (_ee1)
	call __BAND16
	srl h
	rr l
	ex de, hl
	ld hl, (_eey)
	add hl, de
	ld (_eey), hl
	ld a, 4
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 99
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 99
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld de, 2
	ld hl, (_ee1)
	call __BAND16
	srl h
	rr l
	ex de, hl
	ld hl, (_eey)
	or a
	sbc hl, de
	ld (_eey), hl
	ld a, 8
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 132
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 132
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 16
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 165
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 165
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 32
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 198
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 198
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 64
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 231
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 231
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld a, 128
	call _zx81keyboard
	ld l, a
	ld h, 0
	ld (_ee1), hl
	ld de, 10
	call __DIVU16
	ld de, 0
	ld (_ee5), hl
	ld hl, (16396)
	ld de, 264
	add hl, de
	ld de, 30
	add hl, de
	push hl
	ld hl, (_ee5)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (16396)
	ld de, 264
	add hl, de
	ld de, 31
	add hl, de
	push hl
	ld hl, (_ee1)
	ld de, 10
	call __MODU16
	ld de, 28
	add hl, de
	ld a, l
	pop hl
	ld (hl), a
	ld hl, (_eey)
	ld de, 33
	call __MUL16_FAST
	ex de, hl
	ld hl, (_eex)
	add hl, de
	ld (_ee5), hl
	ld hl, (16396)
	inc hl
	ex de, hl
	ld hl, (_ee5)
	add hl, de
	push hl
	ld de, 191
	ld hl, (_eee)
	call __BAND16
	ld a, l
	pop hl
	ld (hl), a
	ld hl, 1000
	call _zx81delay
	ld hl, (_eee)
	inc hl
	ld (_eee), hl
	jp __LABEL31
__LABEL32:
#line 144
codeend:
#line 145
#line 0
		defb $76
Line1End:
Line2:
		defb $00,$0a
		defw Line2End-Line2Text
Line2Text:
		defb $F9,$D4
		defb $1D,$22,$21,$1D,$20
		defb $7E
		defb $8F,$01,$04,$00,$00
		defb $76
Line2End:
Display:
		defb $76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
		defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$76
Variables:
VariablesEnd:
		defb $80
BasicEnd:
#line 42
	ld hl, 0
	ld b, h
	ld c, l
__END_PROGRAM:
	di
	ld hl, (__CALL_BACK__)
	ld sp, hl
	exx
	pop hl
	exx
	pop iy
	pop ix
	ei
	ret
__CALL_BACK__:
	DEFW 0
_zx81delay:
#line 1
		ld b,h
		ld c,l
		push bc
pacmandelayloop:
		dec bc
		ld a,b
		or c
		jp nz,pacmandelayloop
		pop bc
#line 10
_zx81delay__leave:
	ret
_zx81rnd:
#line 1
		ld  d, h
		ld  e, l
		ld  a, d
		ld  h, e
		ld  l, 253
		or  a
		sbc  hl, de
		sbc  a, 0
		sbc  hl, de
		ld  d, 0
		sbc  a, d
		ld  e, a
		sbc  hl, de
		jr  nc, smsrndloop
		inc  hl
smsrndloop:
		ret
#line 18
_zx81rnd__leave:
	ret
_zx81keyboard:
#line 1
		xor $FF
		in a, ($FE)
		xor $FF
		and %11111
#line 5
_zx81keyboard__leave:
	ret
_zx81push:
#line 1
		push hl
#line 2
_zx81push__leave:
	ret
_zx81ldirvarsys:
#line 1
		ld de, ($4060)
		ld hl, ($4062)
		ld bc, ($4064)
		ldir
#line 5
_zx81ldirvarsys__leave:
	ret
#line 1 "mul16.asm"
__MUL16:	; Mutiplies HL with the last value stored into de stack
				; Works for both signed and unsigned
	
			PROC
	
			LOCAL __MUL16LOOP
	        LOCAL __MUL16NOADD
			
			ex de, hl
			pop hl		; Return address
			ex (sp), hl ; CALLEE caller convention
	
;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
	;;		ld c, h
	;;		ld a, l	 ; C,A => 1st Operand
	;;
	;;		ld hl, 0 ; Accumulator
	;;		ld b, 16
	;;
;;__MUL16LOOP:
	;;		sra c	; C,A >> 1  (Arithmetic)
	;;		rra
	;;
	;;		jr nc, __MUL16NOADD
	;;		add hl, de
	;;
;;__MUL16NOADD:
	;;		sla e
	;;		rl d
	;;			
	;;		djnz __MUL16LOOP
	
__MUL16_FAST:
	        ld b, 16
	        ld a, d
	        ld c, e
	        ex de, hl
	        ld hl, 0
	
__MUL16LOOP:
	        add hl, hl  ; hl << 1
	        sla c
	        rla         ; a,c << 1
	        jp nc, __MUL16NOADD
	        add hl, de
	
__MUL16NOADD:
	        djnz __MUL16LOOP
	
			ret	; Result in hl (16 lower bits)
	
			ENDP
	
#line 873 "example01.bas"
#line 1 "div16.asm"
	; 16 bit division and modulo functions 
	; for both signed and unsigned values
	
#line 1 "neg16.asm"
	; Negates HL value (16 bit)
__ABS16:
		bit 7, h
		ret z
	
__NEGHL:
		ld a, l			; HL = -HL
		cpl
		ld l, a
		ld a, h
		cpl
		ld h, a
		inc hl
		ret
	
#line 5 "div16.asm"
	
__DIVU16:    ; 16 bit unsigned division
	             ; HL = Dividend, Stack Top = Divisor
	
		;   -- OBSOLETE ; Now uses FASTCALL convention
		;   ex de, hl
	    ;	pop hl      ; Return address
	    ;	ex (sp), hl ; CALLEE Convention
	
__DIVU16_FAST:
	    ld a, h
	    ld c, l
	    ld hl, 0
	    ld b, 16
	
__DIV16LOOP:
	    sll c
	    rla
	    adc hl,hl
	    sbc hl,de
	    jr  nc, __DIV16NOADD
	    add hl,de
	    dec c
	
__DIV16NOADD:
	    djnz __DIV16LOOP
	
	    ex de, hl
	    ld h, a
	    ld l, c
	
	    ret     ; HL = quotient, DE = Mudulus
	
	
	
__MODU16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVU16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
	
__DIVI16:	; 16 bit signed division
		;	--- The following is OBSOLETE ---
		;	ex de, hl
		;	pop hl
		;	ex (sp), hl 	; CALLEE Convention
	
__DIVI16_FAST:
		ld a, d
		xor h
		ex af, af'		; BIT 7 of a contains result
	
		bit 7, d		; DE is negative?
		jr z, __DIVI16A	
	
		ld a, e			; DE = -DE
		cpl
		ld e, a
		ld a, d
		cpl
		ld d, a
		inc de
	
__DIVI16A:
		bit 7, h		; HL is negative?
		call nz, __NEGHL
	
__DIVI16B:
		call __DIVU16_FAST
		ex af, af'
	
		or a	
		ret p	; return if positive
	    jp __NEGHL
	
		
__MODI16:    ; 16 bit modulus
	             ; HL = Dividend, Stack Top = Divisor
	
	    ;ex de, hl
	    ;pop hl
	    ;ex (sp), hl ; CALLEE Convention
	
	    call __DIVI16_FAST
	    ex de, hl	; hl = reminder (modulus)
					; de = quotient
	
	    ret
	
#line 874 "example01.bas"
#line 1 "eq16.asm"
__EQ16:	; Test if 16bit values HL == DE
		; Returns result in A: 0 = False, FF = True
			or a	; Reset carry flag
			sbc hl, de 
	
			ld a, h
			or l
			sub 1  ; sets carry flag only if a = 0
			sbc a, a
			
			ret
	
#line 875 "example01.bas"
#line 1 "band16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise and16 version.
	; result in hl 
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
	
__BAND16:
		ld a, h
		and d
	    ld h, a
	
	    ld a, l
	    and e
	    ld l, a
	
	    ret 
	
#line 876 "example01.bas"
#line 1 "bor16.asm"
; vim:ts=4:et:
	; FASTCALL bitwise or 16 version.
	; result in HL
; __FASTCALL__ version (operands: A, H)
	; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
	
__BOR16:
		ld a, h
		or d
	    ld h, a
	
	    ld a, l
	    or e
	    ld l, a
	
	    ret 
	
#line 877 "example01.bas"
	
ZXBASIC_USER_DATA:
	_eee EQU 16444
	_seed EQU 16433
	_ee0 EQU 16446
	_ee1 EQU 16448
	_ee2 EQU 16450
	_ee3 EQU 16452
	_ee4 EQU 16454
	_ee5 EQU 16456
	_eex EQU 16458
	_eey EQU 16460
	_t0 EQU 16480
	_t1 EQU 16482
	_t2 EQU 16484
	; Defines DATA END --> HEAP size is 0
ZXBASIC_USER_DATA_END EQU ZXBASIC_MEM_HEAP
	; Defines USER DATA Length in bytes
ZXBASIC_USER_DATA_LEN EQU ZXBASIC_USER_DATA_END - ZXBASIC_USER_DATA
	END
