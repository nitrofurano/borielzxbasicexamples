sub fillram40b(tvram as uinteger,tvle as uinteger,tlen as uinteger):
'- testar....
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld b,(ix+9)
    ld c,(ix+8)
    fillram40bloop:
    ld a,(ix+6)    ;- Get data byte
    ld (de),a      ;- funciona?
    ex de,hl
    push de
    ld de,40
    add hl,de
    pop de
    ex de,hl
    dec bc
    ld a,b
    or c
    jp nz,fillram40bloop
    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
