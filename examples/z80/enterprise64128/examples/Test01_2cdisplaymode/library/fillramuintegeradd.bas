sub fillramuintegeradd(tvram as uinteger,tvle as uinteger,tlen as uinteger,tinc as uinteger):
'- testar....
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ld b,(ix+9)
    ld c,(ix+8)
    fillramuintegeraddloop:
    ld a,(ix+6)    ;- Get data byte
    ld (de),a      ;- funciona?
    inc de
    ld (de),a      ;- funciona?
    dec de    
    ex de,hl
    push de
    ld d,(ix+11)
    ld e,(ix+10)
    add hl,de
    pop de
    ex de,hl
    dec bc
    ld a,b
    or c
    jp nz,fillramuintegeraddloop
    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
