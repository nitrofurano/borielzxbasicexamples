sub enterpriseloadfile(tfn1 as uinteger,tad1 as uinteger,tle1 as uinteger):
  asm
    xor a             ;select channel 0 for open, it could be anything, except reserved channal numbers
    ;ld de,filename
    ld d,(ix+5)
    ld e,(ix+4)
    rst $30           ;exos 1
    defb 1
    jp nz,error       ;if open failed
    ;xor a            ;not needed to set the channel number, because exos 1 returned 0
    ;ld bc,filelength ;file length should be placed here, this number can bigger than file length, file will be loaded and exos call gives back $e4, or $e6 error code, i do not remember
    ld b,(ix+9)
    ld c,(ix+8)
    ;ld de,targetaddress ;address where the file to be loaded
    ld d,(ix+7)
    ld e,(ix+6)
    rst $30           ;exos 6 ;read file
    defb 6
    xor a
    rst $30           ;exos 3 ;close channel 0
    defb 3
    end asm
  end sub

