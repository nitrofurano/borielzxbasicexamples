zxb.py example01.bas --org=$((0x0100))
zxb.py example01.bas --asm --org=$((0x0100))

#- _header1a_.bin - 2 bytes
rm _header1a_.bin
touch _header1a_.bin
echo 0:0005 | xxd -r >> _header1a_.bin

#- _header1c_.bin - 12 bytes
rm _header1c_.bin
touch _header1c_.bin
echo 0:000000000000000000000000 | xxd -r >> _header1c_.bin

#- _header1b_endian1_.bin - file size, excluding file header
fsize1=`du -b "example01.bin" | cut -f1`
printf "0: %.4x" $fsize1 | xxd -r -g0 > _header1b_endian1_.bin
#- _header1b_endian2_.bin - file size, excluding file header
dd ibs=1 count=1 skip=0 if=_header1b_endian1_.bin of=_header1b_endian2_b_.bin
dd ibs=1 count=1 skip=1 if=_header1b_endian1_.bin of=_header1b_endian2_a_.bin
cat _header1b_endian2_a_.bin _header1b_endian2_b_.bin > _header1b_endian2_.bin
rm _header1b_endian2_a_.bin _header1b_endian2_b_.bin

rm example01.ldr
cat _header1a_.bin _header1b_endian2_.bin _header1c_.bin example01.bin > example01.ldr
rm _header1a_.bin _header1b_endian1_.bin _header1b_endian2_.bin _header1c_.bin example01.bin

#- note: this part runs ep128emu emulator, and it’s supposed that
# the virtual disk (alt+f) is mounted at /opt/emulators/ep128emu/virtualdisk/
rm /opt/emulators/ep128emu/virtualdisk/example01.ldr
cp example01.ldr /opt/emulators/ep128emu/virtualdisk/
/opt/emulators/ep128emu/ep128emu_linux64 -ep128 -no-opengl

