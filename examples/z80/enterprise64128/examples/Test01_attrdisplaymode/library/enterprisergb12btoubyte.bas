'- not tested yet
function fastcall enterprisergbtoubyte(trgb1 as uinteger) as ubyte:
  asm
    ;ld h,(ix+5)
    ;ld l,(ix+4)
    
    ld a,h
    and %00000010
    rl a
    rl a
    ld b,a
    ld a,l
    and %00000100
    or b
    rl a
    ld b,a
    ld a,l
    and %00100000
    or b
    rl a
    ld b,a
    ld a,h
    and %00000100
    or b
    rl a
    ld b,a
    
    ld a,l
    and %10000000
    rr a
    rr a
    rr a
    ld c,a
    ld a,h
    and %00001000
    or c
    rr a
    ld c,a
    ld a,l
    and %01000000
    or c
    rr a
    ld c,a
    ld a,l
    and %00001000
    or c
    rr a
    or b
        
    end asm
  end function
    
    '-------------------------
    
    '....RRR.GGG.BB..
    '3210321032103210
    
    '....RRR.
    '....321.
    'GGG.BB..
    '321.32..
    
    'RRRGGGBB
    '32132132
    
    'GRBGRBGR
    '11222333
    
    
    '-------------------------
    
    '....RRR.GGG.BB..
    '3210321032103210
    
    'GRBGRBGR
    '11222333
    
    'G3 >6
    'R3 >3
    'G2 >2
    'B3 >1
    
    'R1 <5
    'B2 <3
    'G1 <2
    'R2 <1
    
    
