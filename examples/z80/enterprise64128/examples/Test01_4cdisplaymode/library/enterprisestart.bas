asm
startpr:
  ld sp,$0100
  ld hl,error        ;- store error and soft reset routine
  ld ($BFF8),hl
  ld bc,$011B
  ld d,$00
  ;------
  ;exos 16           ;- set border to 0 via EXOS
  rst $30
  defb 16
  ;------
  call vid           ;- get a video segment
  out ($B3),a        ;- page into page 3
  call memaddr       ;- get Nick address of the video segment
  ld (vlpb+$04),hl   ;- store Nick address of video memory to the LPT
  ld a,h
  ld hl,vlpb         ;- copy LPT into video memory (now it's z80 address is $FF00)
  ld de,$FF00
  ld bc,$0070
  ldir
  or $3F             ;- this part activate copied LPT, 1st calculate value for port($83,$82)
  rrca               ;- Address of LPT/16 or $C000 is written to ports $83,$82
  rrca
  rrca
  rrca
  or $C0
  ld c,a
  ld a,$F0
  out ($82),a        ;- low value of LPT/16 or $C000
  ld a,c
  out ($83),a        ;- high value of LPT/16 or $C000
  jp fillen

;------------------------------------------
  ;- after this 2 port writes Nick starts to display the new screen, which is a
  ;- 320x200 screen with 16 colours and it can be used $C000-$FE7F by changing
  ;- VLPB+1 you can change colours: $F2 256 colour, $D2 16 colour, $B2
  ;- 4 colour, $92 2 colour mode
vid:
  ld hl,endprg       ;- request a Video RAM from EXOS
  ld (hl),0
req_seg:
  ;------
  ;exos 24
  rst $30
  defb 24
  ;------
  jr z,whatsup
  cp $7F
  jp nz,error
whatsup:
  ex af,af'
  ld a,c
  cp $FC
  jr nc,dontask
  inc hl
  ld (hl),c
  jr req_seg
dontask:
  ex af,af'
  push bc
  push af
back:
  ld c,(hl)
  ;------
  ;exos 25
  rst $30
  defb 25
  ;------
  dec hl
  jr z,back
  pop af
  pop bc
  or a
  jp nz,error
  ld a,c
  ret
memaddr:
  ld hl,$0000
  rra
  rr h
  rra
  rr h
  ret
error:
  di
  ld sp,$0100
  ld a,$FF
  out ($B2),a
  ld c,$40
  ;------
  ;exos 0
  rst $30
  defb 0
  ;------
  ld a,$01
  out ($B3),a
  ld a,$06
  jp $C00D
vlpb:
  ;- number of lines, 100h-38h (?)
  defb $38
  ;- http://ep.lgb.hu/doc/Nick.html#4
  ;- $D2: 
  ; b7: video interrupt will occur after this line, 
  ; b6,b5: now 16 colour, 
  ; b4: if it is 0 then Nick takes graphics data from the
  ;    same place for each scan line defined in this LPB, 
  ; b3,b2,b1: 001 - PIXEL mode
  ;defb $B2
  ;-----iccvmmmr
  ;defb %10110010
  ;defb %10111000
  ;defb $92
  ;defb %10010010 ;$92
  defb %10110010 ;$??
  ;- $0B: left margin at 0bh
  defb $0B
  ;- $33: right margin at 33h
  defb $33
  ;- video address1
  defb $00,$00    ;- defw $0000 instead?
  ;- video address2
  defb $00,$00    ;- defw $0000 instead?
  ;- colour 1-8
  ;defb $00,$DF,$09,$04,$34,$3F,$00,$66

  defb %00111000
  defb %00000111

  defb %00010000 ;g?
  defb %00001000 ;r
  defb %00000100 ;b
  defb %00000010 ;g
  defb %00000001 ;r
  
  defb %00000010

  ;- video lpb 200
vsync:
  defb $D2,$02,$3F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;- sync  46
  defb $FD,$00,$3F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;-        3
  defb $FE,$00,$06,$3F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;-        2
  defb $FF,$00,$3F,$20,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;-        1
  defb $F0,$02,$06,$3F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;-       16
  defb $D4,$03,$3F,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00   ;-       44
endprg:
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  defb $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
fillen:

ld sp,$0100
di

end asm
