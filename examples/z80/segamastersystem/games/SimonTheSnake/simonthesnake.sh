dd bs=65536 count=1 if=/dev/zero of=_dummy64k.bin
zxb.py library/smsboot.bas --org=1024
dd ibs=1 count=$((0x0069)) skip=$((0x0000)) if=smsboot.bin of=simonthesnake.sm_
zxb.py simonthesnake.bas --org=$((0x0069))
zxb.py simonthesnake.bas --asm --org=$((0x0069))
cat simonthesnake.bin >> simonthesnake.sm_
cat _dummy64k.bin >> simonthesnake.sm_
dd ibs=1 count=$((0x2000)) skip=$((0x0000)) if=simonthesnake.sm_ of=simonthesnake.sms
rm smsboot.bin simonthesnake.sm_ _dummy64k.bin simonthesnake.bin
smshead8k simonthesnake.sms
mame sg1000m3 -video soft -w -bp ~/.mess/roms -resolution0 754x448 -aspect 41:32 -skip_gameinfo -cart1 simonthesnake.sms
#mess sg1000m3 -video soft -w -bp ~/.mess/roms -aspect 41:23 -resolution0 536x448 -skip_gameinfo -cart1 simonthesnake.sms
