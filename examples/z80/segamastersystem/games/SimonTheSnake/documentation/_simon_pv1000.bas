'- code based on: Simon - Angeles Barba, Grupo Move - Revista MicroHobby issue 5 - (1985?)
'- original game (now public domain?) from Ralph H. Baer and Howard J. Morrison - (1978?)
'- pv1000 version: Paulo Silva - Jan'2015

asm
  ld sp,$BFFF
  end asm

out $F8,63:out $F9,63:out $FA,63

#include "library/pv1000rnd.bas"
#include "library/pv1000joystick.bas"
#include "library/pv1000writedec.bas"
#include "library/pv1000writetext.bas"
#include "library/pv1000ldir.bas"
#include "library/pv1000fillram.bas"
#include "library/pv1000filltile_f.bas"
#include "library/pv1000puttile_f.bas"

dim n as uinteger at $BC20
dim cnt as ubyte at $BC22
dim c as ubyte at $BC24
dim rlc as uinteger at $BC26
dim seed as uinteger at $BC40
dim jacum as uinteger at $BC42
dim i as uinteger at $BC44
dim a as uinteger at $BC46
dim shfc as uinteger at $BC60
dim hisc as uinteger at $BC62

hisc=1:cnt=1
gosub 650  '- cls2
gosub 670  '- printscore
pv1000filltile(0,0,32,24,$20)

10:
pv1000filltile(12,10,8,4,$20)
seed=pv1000rnd(seed)
rlc=seed band 255
gosub 600 '- cls1
for i=1 to 25000:next

20:
cnt=1

'----------------------------
'- teste:
'pv1000filltile(0,0,32,24,$41)
'----------------------------

'- game loop
'- computer turn

100: '- gameloopcomputerturn
for n=1 to cnt
  a=peek(@urandomdata01+((rlc+n) band 255))band 3
  if a=0 then:gosub 500:end if
  if a=1 then:gosub 510:end if
  if a=2 then:gosub 520:end if
  if a=3 then:gosub 530:end if
  'pv1000writedec(3,5,a,3)
  next

gosub 600 '- cls1

'- player turn
n=1

'- checkjoystick
150:
'-joystick 1
jacum=          ((pv1000joystick(4) band 2)/2) '- y-
jacum=jacum bor ((pv1000joystick(2) band 1)*2) '- y+
jacum=jacum bor ((pv1000joystick(4) band 1)*4) '- x-
jacum=jacum bor ((pv1000joystick(2) band 2)*4) '- x+
'-joystick 2
jacum=jacum bor ((pv1000joystick(4) band 8)/8) '- y-
jacum=jacum bor ((pv1000joystick(2) band 4)/2) '- y+
jacum=jacum bor ((pv1000joystick(4) band 4)*1) '- x-
jacum=jacum bor ((pv1000joystick(2) band 8)*1) '- x+

'----------------------------
'- teste:
'pv1000writedec(3,0,jacum,3)
'pv1000writedec(3,1,pv1000joystick(2),3)
'pv1000writedec(3,2,pv1000joystick(4),3)
'----------------------------

if jacum=0 then:
  goto 150 '- checkjoystick
  end if

if (jacum band 1)<>0 then:c=0:gosub 500:end if '- y-
if (jacum band 2)<>0 then:c=1:gosub 510:end if '- y+
if (jacum band 4)<>0 then:c=2:gosub 520:end if '- x-
if (jacum band 8)<>0 then:c=3:gosub 530:end if '- x+

175:
if (jacum band 15)=0 then:goto 175:end if

a=peek(@urandomdata01+((rlc+n) band 255))band 3
if c=a and n<cnt then:
  let n=n+1
  if hisc<cnt then:hisc=cnt:end if
  goto 150
  end if
if c=a and n>=cnt then:
  cnt=cnt+1
  if hisc<cnt then:hisc=cnt:end if
  gosub 400
  if cnt=251 then:goto 10:end if
  if cnt<>251 then:goto 100:end if
  end if





'- i had to move this here, cartridge memory arrangement optimization
'- score could be inside hiscore....
goto flg01:
text01:
asm
  defb "GAME OVER"
  end asm
text02:
asm
  defb "SCORE:"
  end asm
text03:
asm
  defb "TRY AGAIN?"
  end asm
text04:
asm
  defb "HISCORE:"
  end asm
text05:



'?- 0x03F3 (still 12 bytes free)
'--- important - this allocates the tiles bitmap -----
goto flg01:
asm
  org $400
  end asm
#include "library/tiles_01d.bas"
flg01:
'-----------------------------------------------------
'?- 0x0400





if c<>a then:
  gosub 700 '- error
  if hisc<cnt then:hisc=cnt:end if
  pv1000writetext(11,11,@text01,@text02-@text01) '- print at 14,10;"game over"
  pv1000writetext(11,12,@text03,@text04-@text03) '- print at 16,10;"try again?"
  gosub 670 '- printscore
  out $F8,0:for i=1 to 50000:next:out $F8,63  'beep .5,-36
  for i=1 to 25000:next
210:
  if (jacum band 15)<>0 then:seed=pv1000rnd(shfc+seed):goto 10:end if
  shfc=shfc+1
  goto 210
  end if

400:
if hisc<n then:hisc=n:end if
gosub 670
for i=1 to 20000:next 'pause 200?
gosub 600
return 

'---------------------------------
'- green - up
500:
pv1000puttile(10,3,2,4,@tileg1p0)
pv1000puttile(12,2,8,6,@tileg1p1)
pv1000puttile(20,3,2,4,@tileg1p2)
out $F8,10:for i=1 to 15000:next:out $F8,63
pv1000puttile(10,3,2,4,@tileg0p0)
pv1000puttile(12,2,8,6,@tileg0p1)
pv1000puttile(20,3,2,4,@tileg0p2)
for i=1 to 1000:next
return

'- red - down
510:
pv1000puttile(10,17,2,4,@tiler1p0)
pv1000puttile(12,16,8,6,@tiler1p1)
pv1000puttile(20,17,2,4,@tiler1p2)
out $F8,12:for i=1 to 15000:next:out $F8,63
pv1000puttile(10,17,2,4,@tiler0p0)
pv1000puttile(12,16,8,6,@tiler0p1)
pv1000puttile(20,17,2,4,@tiler0p2)
for i=1 to 1000:next
return

'- yellow - left
520:
pv1000puttile(7,6,4,2,@tiley1p0)
pv1000puttile(6,8,6,8,@tiley1p1)
pv1000puttile(7,16,4,2,@tiley1p2)
out $F8,14:for i=1 to 15000:next:out $F8,63
pv1000puttile(7,6,4,2,@tiley0p0)
pv1000puttile(6,8,6,8,@tiley0p1)
pv1000puttile(7,16,4,2,@tiley0p2)
for i=1 to 1000:next
return

'- cyan - right
530:
pv1000puttile(21,6,4,2,@tilec1p0)
pv1000puttile(20,8,6,8,@tilec1p1)
pv1000puttile(21,16,4,2,@tilec1p2)
out $F8,16:for i=1 to 15000:next:out $F8,63
pv1000puttile(21,6,4,2,@tilec0p0)
pv1000puttile(20,8,6,8,@tilec0p1)
pv1000puttile(21,16,4,2,@tilec0p2)
for i=1 to 1000:next
return

'---------------------------------
'- cls1
600:
pv1000puttile(10,3,2,4,@tileg0p0)
pv1000puttile(12,2,8,6,@tileg0p1)
pv1000puttile(20,3,2,4,@tileg0p2)
pv1000puttile(10,17,2,4,@tiler0p0)
pv1000puttile(12,16,8,6,@tiler0p1)
pv1000puttile(20,17,2,4,@tiler0p2)
pv1000puttile(7,6,4,2,@tiley0p0)
pv1000puttile(6,8,6,8,@tiley0p1)
pv1000puttile(7,16,4,2,@tiley0p2)
pv1000puttile(21,6,4,2,@tilec0p0)
pv1000puttile(20,8,6,8,@tilec0p1)
pv1000puttile(21,16,4,2,@tilec0p2)
return

'- cls2
650:
pv1000filltile(0,0,32,24,$20)
return

'---------------------------------
'- print score
670:
pv1000writetext(20,22,@text02,@text03-@text02) '- print at 15,10;"score:";cnt-1;
pv1000writedec(26,22,cnt-1,3)
pv1000writetext(18,23,@text04,@text05-@text04) '- print at 15,10;"hiscore:";cnt-1;
pv1000writedec(26,23,hisc-1,3)
return

'- error
700:
pv1000puttile(10,3,2,4,@tileg1p0)
pv1000puttile(12,2,8,6,@tileg1p1)
pv1000puttile(20,3,2,4,@tileg1p2)
pv1000puttile(10,17,2,4,@tiler1p0)
pv1000puttile(12,16,8,6,@tiler1p1)
pv1000puttile(20,17,2,4,@tiler1p2)
pv1000puttile(7,6,4,2,@tiley1p0)
pv1000puttile(6,8,6,8,@tiley1p1)
pv1000puttile(7,16,4,2,@tiley1p2)
pv1000puttile(21,6,4,2,@tilec1p0)
pv1000puttile(20,8,6,8,@tilec1p1)
pv1000puttile(21,16,4,2,@tilec1p2)
return

do:loop

'----------------------------------------
'- green

'- green off
tileg0p0:
asm
  defb $64,$65
  defb $C2,$C2
  defb $B4,$C2
  defb $20,$B4
  end asm
tileg0p1:
asm
  defb $64,$65,$C2,$C2,$C2,$C2,$84,$85
  defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
  defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
  defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
  defb $C2,$C2,$C2,$C2,$C2,$C2,$C2,$C2
  defb $B4,$94,$95,$20,$20,$74,$75,$B5
  end asm
tileg0p2:
asm
  defb $84,$85
  defb $C2,$C2
  defb $C2,$B5
  defb $B5,$20
  end asm

'- green on
tileg1p0:
asm
  defb $66,$67
  defb $C3,$C3
  defb $B6,$C3
  defb $20,$B6
  end asm
tileg1p1:
asm
  defb $66,$67,$C3,$C3,$C3,$C3,$86,$87
  defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
  defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3

  defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
  defb $C3,$C3,$C3,$C3,$C3,$C3,$C3,$C3
  defb $B6,$96,$97,$20,$20,$76,$77,$B7
  end asm
tileg1p2:
asm
  defb $86,$87
  defb $C3,$C3
  defb $C3,$B7
  defb $B7,$20
  end asm

'----------------------------------------
'- red

'- red off
tiler0p0:
asm
  defb $20,$A0
  defb $A0,$C0
  defb $C0,$C0
  defb $70,$71
  end asm
tiler0p1:
asm
  defb $A0,$80,$81,$20,$20,$60,$61,$A1
  defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
  defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
  defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
  defb $C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0
  defb $70,$71,$C0,$C0,$C0,$C0,$90,$91
  end asm
tiler0p2:
asm
  defb $A1,$20
  defb $C0,$A1
  defb $C0,$C0
  defb $90,$91
  end asm

'- red on
tiler1p0:
asm
  defb $20,$A2
  defb $A2,$C1
  defb $C1,$C1
  defb $72,$73
  end asm
tiler1p1:
asm
  defb $A2,$82,$83,$20,$20,$62,$63,$A3
  defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
  defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
  defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
  defb $C1,$C1,$C1,$C1,$C1,$C1,$C1,$C1
  defb $72,$73,$C1,$C1,$C1,$C1,$92,$93
  end asm
tiler1p2:
asm
  defb $A3,$20
  defb $C1,$A3
  defb $C1,$C1
  defb $92,$93
  end asm

'----------------------------------------
'- yellow

'- yellow off
tiley0p0:
asm
  defb $6C,$C6,$AD,$20
  defb $7C,$C6,$C6,$AD
  end asm
tiley0p1:
asm
  defb $6C,$C6,$C6,$C6,$C6,$AD
  defb $7C,$C6,$C6,$C6,$C6,$8D
  defb $C6,$C6,$C6,$C6,$C6,$9D
  defb $C6,$C6,$C6,$C6,$C6,$20
  defb $C6,$C6,$C6,$C6,$C6,$20
  defb $C6,$C6,$C6,$C6,$C6,$6D
  defb $8C,$C6,$C6,$C6,$C6,$7D
  defb $9C,$C6,$C6,$C6,$C6,$BD
  end asm
tiley0p2:
asm
  defb $8C,$C6,$C6,$BD
  defb $9C,$C6,$BD,$20
  end asm

'- yellow on
tiley1p0:
asm
  defb $6E,$C7,$AF,$20
  defb $7E,$C7,$C7,$AF
  end asm
tiley1p1:
asm
  defb $6E,$C7,$C7,$C7,$C7,$AF
  defb $7E,$C7,$C7,$C7,$C7,$8F
  defb $C7,$C7,$C7,$C7,$C7,$9F
  defb $C7,$C7,$C7,$C7,$C7,$20
  defb $C7,$C7,$C7,$C7,$C7,$20
  defb $C7,$C7,$C7,$C7,$C7,$6F
  defb $8E,$C7,$C7,$C7,$C7,$7F
  defb $9E,$C7,$C7,$C7,$C7,$BF
  end asm
tiley1p2:
asm
  defb $8E,$C7,$C7,$BF
  defb $9E,$C7,$BF,$20
  end asm

'----------------------------------------
'- cyan

'- cyan off
tilec0p0:
asm
  defb $20,$A8,$C4,$69
  defb $A8,$C4,$C4,$79
  end asm
tilec0p1:
asm
  defb $A8,$C4,$C4,$C4,$C4,$69
  defb $88,$C4,$C4,$C4,$C4,$79
  defb $98,$C4,$C4,$C4,$C4,$C4
  defb $20,$C4,$C4,$C4,$C4,$C4
  defb $20,$C4,$C4,$C4,$C4,$C4
  defb $68,$C4,$C4,$C4,$C4,$C4
  defb $78,$C4,$C4,$C4,$C4,$89
  defb $B8,$C4,$C4,$C4,$C4,$99
  end asm
tilec0p2:
asm
  defb $B8,$C4,$C4,$89
  defb $20,$B8,$C4,$99
  end asm

'- cyan on
tilec1p0:
asm
  defb $20,$AA,$C5,$6B
  defb $AA,$C5,$C5,$7B
  end asm
tilec1p1:
asm
  defb $AA,$C5,$C5,$C5,$C5,$6B
  defb $8A,$C5,$C5,$C5,$C5,$7B
  defb $9A,$C5,$C5,$C5,$C5,$C5
  defb $20,$C5,$C5,$C5,$C5,$C5
  defb $20,$C5,$C5,$C5,$C5,$C5
  defb $6A,$C5,$C5,$C5,$C5,$C5
  defb $7A,$C5,$C5,$C5,$C5,$8B
  defb $BA,$C5,$C5,$C5,$C5,$9B
  end asm
tilec1p2:
asm
  defb $BA,$C5,$C5,$8B
  defb $20,$BA,$C5,$9B
  end asm

'----------------------------------------

urandomdata01:
asm
  defb 7,5,4,5,7,3,5,7,1,6,6,3,3,1,3,2
  defb 6,4,5,2,2,4,5,4,5,5,5,7,3,2,7,1
  defb 1,3,7,4,0,4,4,1,5,7,7,0,1,5,6,5
  defb 6,3,4,1,4,1,4,2,1,1,6,3,0,5,6,5
  defb 6,4,2,0,2,2,3,7,3,7,1,3,1,7,3,5
  defb 2,0,1,5,1,4,2,0,6,0,1,6,0,5,2,6
  defb 5,1,4,2,3,4,7,6,3,5,0,6,5,2,3,1
  defb 2,1,1,5,2,4,5,6,3,7,2,7,3,0,5,6
  defb 7,4,7,1,6,3,3,6,7,5,1,0,6,4,2,7
  defb 5,0,3,7,5,1,1,1,2,6,2,0,3,4,6,3
  defb 3,1,4,3,6,1,0,7,2,3,0,0,0,4,7,0
  defb 3,6,3,4,3,4,6,0,5,5,4,7,0,5,5,2
  defb 4,1,6,7,4,1,2,7,4,0,3,5,6,4,2,0
  defb 6,7,2,7,0,4,6,4,4,0,7,5,2,1,5,5
  defb 5,2,0,0,2,7,3,6,7,5,4,7,6,4,7,6
  defb 1,6,6,4,2,2,7,3,0,1,2,3,5,6,2,7
  end asm

'----------------------------------------

'- r:#F01
'- g:#0C5
'- y:#FC0
'- c:#06C

