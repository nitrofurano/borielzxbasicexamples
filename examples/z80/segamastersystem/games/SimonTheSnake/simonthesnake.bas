#include "library/_sms_setz80stackpoint.bas"
#include "library/smsvdp.bas"
#include "library/smsfilvrm.bas"
#include "library/smsfilvrmtx.bas"
#include "library/smsldirvm.bas"
#include "library/smsscroll.bas"
#include "library/smsvpoke.bas"
#include "library/smsrnd.bas"
#include "library/smsjoypad.bas"
#include "library/smspalette.bas"
#include "library/smsdelay.bas"
#include "library/smssound.bas"
#include "library/01_arrowpatterns4bit.bas"
#include "library/01_charmap2bit.bas"
#include "library/01_palette.bas"

dim ee1 as uinteger at $C010
dim ee2 as uinteger at $C012
dim ee3 as uinteger at $C014
dim seed as uinteger at $C016
dim shfc as uinteger at $C018
dim hisc as uinteger at $C01A
dim xcr as uinteger at $C01C
dim ycr as uinteger at $C01E
dim cnt as uinteger at $C020
dim rlc as uinteger at $C022
dim jacum as uinteger at $C024
dim n as uinteger at $C026
dim c as uinteger at $C028
dim a as uinteger at $C02A
dim i as uinteger at $C02C
dim cntb as uinteger at $C02E

'-------------------------------------------------------------------------------

seed=%10101010

sub smsputchar(tx1 as uinteger,ty1 as uinteger,tch1 as ubyte, tat1 as ubyte)
  smsvpoke($3800+(tx1*2)+(ty1*64),tch1)
  smsvpoke($3801+(tx1*2)+(ty1*64),tat1)
  end sub

sub smswritetext(tx2 as uinteger,ty2 as uinteger,ttx2 as uinteger,tle2 as uinteger,tat2 as ubyte)
  dim tq2 as uinteger at $C000
  for tq2=0 to tle2-1
    smsputchar(tx2+tq2,ty2,peek(ttx2+tq2),tat2)
    next
  end sub

sub smswritedec(tx2 as uinteger,ty2 as uinteger,tvl as uinteger,tle2 as uinteger,tat2 as ubyte)
  dim txlp as uinteger at $C000:dim tylp as uinteger at $C002
  tylp=1
  for txlp=tle2 to 1 step -1
    smsputchar(tx2+txlp-1,ty2,48+(int(tvl/tylp) mod 10),tat2)
    tylp=tylp*10
    next
  end sub

'sub pv1000writebin(txpos as uinteger,typos as uinteger,tvl as uinteger,tlen as uinteger):
'  dim txlp as uinteger at $BC00:dim tylp as uinteger at $BC02
'  tylp=1
'  for txlp=tlen to 1 step -1
'    poke $B800+typos*32+txpos+txlp-1,48+(int(tvl/tylp) mod 2)
'    tylp=tylp*2
'    next
'  end sub

'sub pv1000writehex(txpos as uinteger,typos as uinteger,tvl as uinteger,tlen as uinteger):
'  dim txlp as uinteger at $BC00:dim tylp as uinteger at $BC02:dim tqlp as ubyte at $BC04
'  tylp=1
'  for txlp=tlen to 1 step -1
'    tqlp=(int(tvl/tylp)) mod 16
'    poke $B800+typos*32+txpos+txlp-1,48+tqlp+(int(tqlp/10)*7)
'    tylp=tylp*16
'    next
'  end sub

'-------------------------------------------------------------------------------

start01:

'- mode 4,2
smsvdp(0,%00000100):smsvdp(1,%11100010) '- smsvdp(1,$84)
smsvdp(2,$ff):smsvdp(5,$ff):smsvdp(10,$ff)
smsvdp(6,$fb) '- sprite patterns - $fb for $0000 (256 sprites available), $ff for $2000 (192 sprites available)
smsvdp(7,$00) '- border colour (sprite palette)

smsfilvrm($0000,0,$4000) '- replaces "clear vram"
'smsldirvm($0800,@charset01,2048)

'- palette
for ee1=0 to 15
  ee2=peek(@palette01+ee1)
  smspalette(ee1,ee2)
  smspalette(16+ee1,ee2 bxor %11111111)
  next

smspalette(16,%00000000)
smspalette(17,%00110011)

'- transfer characters
for ee1=0 to 64*8
  for ee2=0 to 1
    ee3=peek(@charmap2bit+(ee1*2)+ee2)
    smsvpoke(($20*32)+(ee1*4)+ee2,ee3)
    next:next

for ee1=0 to 6*8
  for ee2=0 to 3
    ee3=peek(@arrowpatterns4bit+(ee1*4)+ee2)
    smsvpoke(($80*32)+(ee1*4)+ee2,ee3)
    next:next

'-------------------------------------------------------------------------------

mainloop01:
gosub title01
gosub gameplay01
goto mainloop01

'-------------------------------------------------------------------------------

title01:

for ee1=17 to 31
  smspalette(ee1,seed)
  next

gosub clsr

smswritetext(2,19,@text01b,@text01bend-@text01b,0)
smswritetext(2,21,@text01c,@text01cend-@text01c,0)
smswritetext(2,22,@text01d,@text01dend-@text01d,0)

ee1=0:cntb=1

do
  ee1=ee1+1
  if ee1>64 then:
    if (cntb mod 8)=0 then:
      seed=smsrnd(seed)
      end if
    smspalette(17+(cntb mod 8),seed)
    cntb=cntb+1
    ee1=0
    end if
  if (smsjoypad1() bor smsjoypad2())<>0 then:return:end if
  smsdelay(500)
  loop

'-------------------------------------------------------------------------------

cnt=1
seed=smsrnd(seed)
rlc=seed

gameplay01:

cnt=1 '-????? bug...

gameloop:
computerturn:
xcr=16:ycr=12
gosub clsk

for n=1 to cnt
  a=(peek(@urandomdata01+((rlc+n) band 255)))band 3
  if a=0 then:gosub 500:end if '-right
  if a=1 then:gosub 510:end if '-down
  if a=2 then:gosub 520:end if '-left
  if a=3 then:gosub 530:end if '-up

  'smswritedec(2,1,n,5,0)
  'smswritedec(2,2,cnt,5,0)
  'smswritedec(2,4,a,1,0)
  'smswritedec(2,6,xcr,2,0)
  'smswritedec(2,7,ycr,2,0)

  next
  smsdelay(30000)

playerturn:
xcr=16:ycr=12
gosub clsk

n=1

150:
checkjoystick:
jacum=(smsjoypad1() bor smsjoypad2())band 15

if jacum=0 then:
  goto checkjoystick
  end if

if (jacum band 8)<>0 then:c=0:gosub 500:end if '- x+
if (jacum band 2)<>0 then:c=1:gosub 510:end if '- y+
if (jacum band 4)<>0 then:c=2:gosub 520:end if '- x-
if (jacum band 1)<>0 then:c=3:gosub 530:end if '- y-

175:
loop1:
if (jacum band 15)=0 then:goto loop1:end if

a=(peek(@urandomdata01+((rlc+n) band 255)))band 3
if c=a and n<cnt then:
  let n=n+1
  if hisc<cnt then:hisc=cnt:end if
  goto checkjoystick
  end if
if c=a and n>=cnt then:
  cnt=cnt+1
  if hisc<cnt then:hisc=cnt:end if
  gosub 400 '- ?
  if cnt=251 then:goto 10:end if
  if cnt<>251 then:goto gameloop:end if
  end if

smsdelay(15000)
'-?????
smssoundio((1*128)+(0*32)+(0*16)+ %1000 ) '- cm=1, ch=0..3, kd=0, fr=0..15
smssoundio((0*128)+ %111111 )             '- cm=0, fr=0..63
smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(60000)
smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(15000)

return

do:loop

400:
return

10:
do:loop

'-------------------------------------------------------------------------------

500: '- right
xcr=xcr+1
smsputchar(xcr,ycr,32,0)
smsdelay(15000)
smsputchar(xcr,ycr,128,0)
smssoundio((1*128)+(0*32)+(0*16)+ %1100 ) '- cm=1, ch=0..3, kd=0, fr=0..15
smssoundio((0*128)+ %110101 )             '- cm=0, fr=0..63
smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(30000)
smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(15000)
return

510: '- down
ycr=ycr+1
smsputchar(xcr,ycr,32,0)
smsdelay(15000)
smsputchar(xcr,ycr,129,0)
smssoundio((1*128)+(0*32)+(0*16)+ %1110 ) '- cm=1, ch=0..3, kd=0, fr=0..15
smssoundio((0*128)+ %101111 )             '- cm=0, fr=0..63
smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(30000)
smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(15000)
return

520: '- left
xcr=xcr-1
smsputchar(xcr,ycr,32,0)
smsdelay(15000)
smsputchar(xcr,ycr,130,0)
smssoundio((1*128)+(0*32)+(0*16)+ %1010 ) '- cm=1, ch=0..3, kd=0, fr=0..15
smssoundio((0*128)+ %101010 )             '- cm=0, fr=0..63
smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(30000)
smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(15000)
return

530: '- up
ycr=ycr-1
smsputchar(xcr,ycr,32,0)
smsdelay(15000)
smsputchar(xcr,ycr,131,0)
smssoundio((1*128)+(0*32)+(0*16)+ %1100 ) '- cm=1, ch=0..3, kd=0, fr=0..15
smssoundio((0*128)+ %100101 )             '- cm=0, fr=0..63
smssoundio((1*128)+(0*32)+(1*16)+0)       '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(30000)
smssoundio((1*128)+(0*32)+(1*16)+15)      '- cm=1, ch=0..3, kd=1, vl=0..15
smsdelay(15000)
return

clsk:
smsfilvrmtx($3800,code(" "),0,768)
return

clsr:
smsfilvrmtx($3800,132,8,768)
return

'-------------------------------------------------------------------------------

do:loop

text01a:
asm
  defb "HELLO WORLD!!!!!"
  end asm
text01aend:
text01b:
asm
  defb " SIMON THE SNAKE "
  end asm
text01bend:
text01c:
asm
  defb " PAULO SILVA, 2015 "
  end asm
text01cend:
text01d:
asm
  defb " PUSH ANY BUTTON "
  end asm
text01dend:

urandomdata01:
asm
  defb 3,0,0,1,2,1,0,1,1,2,3,2,3,2,1,1
  defb 0,1,2,2,2,3,0,3,2,2,1,2,3,3,0,0
  defb 0,3,0,3,2,2,1,2,3,3,0,0,0,0,3,2
  defb 2,3,0,0,0,1,1,0,0,3,2,3,0,0,1,1
  defb 1,2,1,0,0,0,3,2,0,3,3,1,0,1,2,1
  defb 2,2,1,0,0,0,3,0,1,1,2,2,2,1,0,1
  defb 2,2,1,2,3,3,0,3,2,3,3,2,1,1,1,2
  defb 2,1,0,0,1,1,2,3,2,2,3,2,1,2,3,2
  defb 1,1,0,1,2,2,3,2,3,0,3,3,2,1,2,3
  defb 3,3,2,3,0,3,0,3,2,2,1,2,3,3,3,0
  defb 1,0,0,3,2,3,0,0,1,0,3,3,2,2,2,2
  defb 1,2,3,3,0,0,0,3,0,1,0,3,3,2,3,0
  defb 0,1,1,0,3,0,1,1,2,2,1,0,0,0,3,0
  defb 1,0,3,3,2,2,3,0,0,0,1,1,1,0,3,0
  defb 1,1,2,1,0,0,3,3,0,1,1,1,0,1,1,0
  defb 1,2,1,0,1,2,1,0,0,3,0,1,1,2,2,2
  end asm

'-------------------------------------------------------------------------------

'sn76489notes:
'asm
'  defw   0,  0,  0,  0,  0,  0,  0,  0,  0,   0,  0,  0 ;- o0
'  defw   0,  0,  0,  0,  0,  0,  0,  0,  0,   0,  0,  0 ;- o1
'  defw   0,  0,  0,  0,  0,  0,  0,  0,  0,1016,960,908 ;- o2
'  defw 860,810,766,721,682,642,604,570,540, 508,480,454 ;- o3
'  defw 430,405,383,360,341,321,302,285,270, 254,240,227 ;- o4
'  defw 215,202,191,180,170,160,151,142,135, 127,120,113 ;- o5
'  defw 107,101, 95, 90, 85, 80, 75, 71, 67,  63, 60, 56 ;- o6
'  defw  53, 50, 47, 45, 42, 40, 37, 35, 33,  31, 30, 28 ;- o7
'  defw  26, 25, 23, 22, 21, 20, 19, 17, 16,  15, 15, 14 ;- o8
'  end asm 

'1016 - 111111 1000 - 000000 0111
'860 - 110101 1100 - 001010 0011
'766 - 101111 1110 - 010000 0001
'682 - 101010 1010 - 010101 0101
'604 - 100101 1100 - 011010 0011


