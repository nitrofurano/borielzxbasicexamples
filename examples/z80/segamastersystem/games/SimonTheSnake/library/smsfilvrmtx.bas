sub smsfilvrmtx(tvram as uinteger,tvl as uinteger,tat as uinteger,tlen as uinteger):
  asm
    ld l,(ix+4) ;- tvram
    ld a,l
    out ($bf),a
    ld h,(ix+5) ;- tvram
    ld a,h
    or $40
    out ($bf),a

    ld b,(ix+11) ;- tlen
    ld c,(ix+10) ;- tlen

    smsfilvrmtxloop:
    ld a,(ix+6) ;- tvl
    out ($be),a
    ld a,(ix+8) ;- tat
    out ($be),a
    dec bc
    ld a,b
    or c
    jp nz,smsfilvrmtxloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'FILVRM (based on msx bios)
'Address  : #0056
'Function : fill VRAM with value
'Input    : 
'           HL - start address
'           A  - data byte
'           BC - length of the area to be written
'Registers: AF, BC
