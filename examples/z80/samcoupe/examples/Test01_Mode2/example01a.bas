#include "library/samcoupebootmode2.bas"
#include "library/1_Vdg_mc6847_b_2a.bas"
#include "library/samcoupernd.bas"
#include "library/samcoupepaletteout.bas"
#include "library/samcoupedelay.bas"
#include "library/samcoupehexcolour.bas"
#include "library/samcoupeborder.bas"

'-------------------------------------------------------------------------------

asm
  ld bc,$04F8
  ld a,$3F
  out (c),a
  end asm

dim i as uinteger at $4010
dim seed1 as uinteger at $4012
dim seed2 as uinteger at $4014
dim i0 as uinteger at $4016
dim i1 as uinteger at $4018
dim i2 as uinteger at $401A
dim i3 as uinteger at $401C
dim seed3 as uinteger at $401E

sub putchar8x12(txpo1 as uinteger,typo1 as uinteger,tch1 as uinteger,tatt1 as ubyte,tadr1 as uinteger):
  dim tlp1 as uinteger at $4000
  dim tpv1 as uinteger at $4002
  for tlp1=0 to 11
    tpv1=(txpo1)+(tlp1*32)+(typo1*384)
    poke $C000+tpv1,peek(tadr1+((tch1 mod 80)*12)+tlp1)
    poke $E000+tpv1,tatt1
    next

  end sub

sub putcharmc6842alike(txpo3 as uinteger,typo3 as uinteger,tch3 as uinteger,tadr3 as uinteger):
  tch3=tch3 band 255
  if tch3<128 then
    putchar8x12(txpo3,typo3,tch3 band 63,peek(@putcharmc6842alikedefb+int(tch3/64)),tadr3)
  else
    putchar8x12(txpo3,typo3,64+(tch3 band 15),peek(2+@putcharmc6842alikedefb+int((tch3-128)/16)),tadr3)
    end if
  goto putcharmc6842alikeend
  putcharmc6842alikedefb:
  asm
    defb %00001010,%00010001
    defb $04,$05,$06,$07
    defb $44,$45,$46,$47
    end asm
  putcharmc6842alikeend:
  end sub

samcoupeborder(4)


seed1=43

'for i4=0 to 0

for i=0 to 15
  samcoupepaletteout(i,samcoupehexcolour(peek(uinteger,(32*0)+@palette01+(i*2))))
  next

samcoupeborder(0)

'for i0=0 to 15
'  for i1=0 to 31
'    seed1=samcoupernd(seed1):seed2=seed1
'    seed1=samcoupernd(seed1)
'    putchar8x12(i1,i0,seed1 mod 80,seed2 mod 128,@charset01)
'    'samcoupedelay(5000)
'    next:next

for i0=0 to 15
  for i1=0 to 31
    seed1=samcoupernd(seed1)
    putcharmc6842alike(i1,i0,seed1,@charset01)
    'samcoupedelay(5000)
    next:next

do
  seed1=samcoupernd(seed1):seed2=seed1
  seed1=samcoupernd(seed1):seed3=seed1
  seed1=samcoupernd(seed1)
  putcharmc6842alike(seed1 band 31,seed2 band 15,seed3 band 255,@charset01)
  loop

'next

do:loop

'samcoupeborder(0)
'for i=0 to 15
'  samcoupepaletteout(i,samcoupehexcolour(peek(uinteger,(32*7)+@palette01+(i*2))))
'  next
'samcoupeborder(7)
'loop01:
'goto loop01

'-------------------------------------------------------------------------------

palette01:
asm
  ;-coco1
  defw $000,$050,$0F0,$050
  defw $0F0,$FF5,$00F,$F00
  defw $000,$050,$0F0,$050
  defw $FFF,$5FA,$F0F,$FA0
  end asm

'  ;-msx1
'  defw $000,$000,$2D2,$6F6
'  defw $22F,$36F,$B22,$4DF
'  defw $F22,$F66,$DD2,$DD9
'  defw $292,$D4B,$BBB,$FFF
'  ;-gods-atarist
'  defw $000,$005,$500,$055
'  defw $555,$55A,$A50,$A55
'  defw $5AF,$AA5,$AAA,$AAF
'  defw $FAA,$FF0,$AFF,$FFF
'  ;-goemonkonamimsx
'  defw $000,$FB9,$000,$F00
'  defw $FB9,$B60,$FFF,$620
'  defw $FB0,$B64,$940,$049
'  defw $999,$092,$FFF,$042
'  ;-sg1000mk3
'  defw $000,$000,$0A0,$0F0
'  defw $005,$00F,$500,$0FF
'  defw $A00,$F00,$550,$FF0
'  defw $050,$F0F,$555,$FFF
'  ;-mojontwinsulaplus
'  defw $000,$40F,$B20,$94A
'  defw $460,$26F,$B90,$BBF
'  defw $000,$04F,$D40,$B6A
'  defw $6D0,$0BF,$DB5,$FFF
'  ;-sgi4dwm
'  defw $000,$F00,$0F0,$FF0
'  defw $00F,$F0F,$0FF,$FFF
'  defw $555,$C88,$8C8,$884
'  defw $88C,$848,$488,$AAA
'  ;-apple2c16
'  defw $000,$724,$437,$E3F
'  defw $054,$888,$19F,$BBF
'  defw $440,$E60,$888,$FAB
'  defw $1C0,$BC8,$8DB,$FFF
'  ;-c64
'  defw $000,$FFF,$A00,$AFF
'  defw $A5A,$0A5,$00A,$FF5
'  defw $FA5,$550,$F55,$555
'  defw $555,$AF5,$0AF,$AAA
'  end asm

'- .sbt file: old versions of Simcoupe needs:
'- boot1:load1:call32768

