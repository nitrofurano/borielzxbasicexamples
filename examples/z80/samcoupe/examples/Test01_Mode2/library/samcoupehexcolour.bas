function samcoupehexcolour(hxcv as uinteger) as ubyte:
  dim tmpv as uinteger
  tmpv=          (hxcv band 4)/4
  tmpv=tmpv bor ((hxcv band 64)/16)
  tmpv=tmpv bor ((hxcv band 8)*2)
  tmpv=tmpv bor ((hxcv band 128)/2)
  tmpv=tmpv bor ((hxcv band 32)/4)
  hxcv=int(hxcv/256)
  tmpv=tmpv bor ((hxcv band 8)*4)
  tmpv=tmpv bor ((hxcv band 4)/2)
  return tmpv
  end function
