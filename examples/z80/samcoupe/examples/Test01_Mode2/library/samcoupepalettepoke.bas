sub samcoupepalettepoke(tid as ubyte, tvl as ubyte):
  poke $55D8+tid,tvl
  poke $55EC+tid,tvl
  end sub

sub samcoupepalettepokeflash(tid as ubyte, tvl1 as ubyte, tvl2 as ubyte):
  poke $55D8+tid,tvl1
  poke $55EC+tid,tvl2
  end sub
