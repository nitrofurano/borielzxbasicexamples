sub minivadersfillram32b(tvram as uinteger, tvl as uinteger,tlen as uinteger):
'- testar....
  asm
    ld d,(ix+5)
    ld e,(ix+4)
    ;ld h,(ix+7)
    ;ld l,(ix+6)
    ld b,(ix+9)
    ld c,(ix+8)

    ;apagar
    ;call $005C
    ;ld a,e
    ;out ($bf),a
    ;ld a,$78     ;- ld a,$38|$40 
    ;ld a,d
    ;or $40
    ;out ($bf),a
    ; 2. Output tilemap data
    ;ld hl,Message
    ;ld bc,MessageEnd-Message  ; Counter for number of bytes to write

    minivadersldir32bloop:
    'ld a,(hl)    ; Get data byte

    ld a,(ix+7)
    ld (de),a    ; funciona?
    
    inc hl       ; Point to next letter

    ;add de,32    ; funciona?
    ex de,hl
    push de
    ld de,32
    add hl,de
    pop de
    ex de,hl
    
    dec bc
    ld a,b
    or c
    jp nz,minivadersldir32bloop

    end asm
  end sub

'-------------------------------------------------------------------------------
'- LDIRVM (from msx bios)
'- Address  : #005C
'- Function : Block transfer to VRAM from memory
'- Input    : DE - Start address of VRAM
'-            HL - Start address of memory
'-            BC - blocklength
'- Registers: All
