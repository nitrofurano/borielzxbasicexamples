asm
  di
  end asm

asm
  ld a,1            ;modes 1 to 4 should be 0-3
  dec a
  rrca              ;rotate them round to
  rrca              ;bits 7,6 and 5.
  rrca
  ld b,a            ;and save in b.
  in a,($fc)        ;get current screen page.
  and $1f           ;strip off the mode bits
  or b              ;and replace with new mode.
  out ($fc),a       ;set new screen mode.
  in  a,($fb)       ; page at &8000, page+1 at &c000
  inc a             ; put screen at HMPR page+1
  and %00011111     ; keep only page bits
  or  1*32          ; merge in mode 1 bits ((mode-1)*32? not working?)
  out ($fc),a       ; set video mode+page to look at &c000
  end asm

asm
  ld a,7
  out ($FE),a
  ld a,6
  out ($FE),a
  ld a,5
  out ($FE),a
  end asm
