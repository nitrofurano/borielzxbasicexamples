sub samcoupedelay(ttm as uinteger):
  asm
    ld b,(ix+5)
    ld c,(ix+4)
    samcoupedelayloop:
    dec bc
    ld a,b
    or c
    jp nz,samcoupedelayloop
    end asm
  end sub
