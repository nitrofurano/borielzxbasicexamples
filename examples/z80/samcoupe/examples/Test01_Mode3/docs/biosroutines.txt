
$0103 - JSVIN
$0109 - WKROOM
$010C - MKRBIG
$010F - CALBAS
$0112 - SETSTRM - a=channel
$0115 - POMSG
$0118 - EXPT1NUM
$011B - EXPTSTR
$011E - EXPTEXPR
$0121 - GETINT
$0124 - STKFETCH
$0127 - STKSTORE
$012A - SBUFFET
$012D - FARLDIR - Copies (PAGCOUNT=#5B83) 16k pages + (MODCOUNT=#5B84) bytes from page A, offset HL to page C, offset DE. Paging on entry/exit unimportant and unchanged.
$0130 - FARLDDR
$0133 - JPUT - Puts a block of data onto a mode 3 or 4 screen.
$0136 - JGRAB - Stores a block of data from a mode 3/4 screen to a buffer.
$0139 - JPLOT - b (or hl, if mode=3 and fatpix=0)=x, c=y, 
$013C - JDRAW - b (or hl, if mode=3 and fatpix=0)=x, c=y, d= direction, e=direction
$0142 - JCIRCLE - Draw circle at C,B radius A
$0145 - JFILL - Modes 3,4 only
$0148 - JBLITZ 
$014B - JROLL
$014E - CLSBL (cls)  - a=0 (entire screen)
$0151 - CLSLOWER
$015A - JMODE - a= screen mode    <----- !!!!!
$0163 - RECLAIM2
$0166 - KBFLUSH
$0169 - READKEY
$016C - KYIP2
$0187 - GRCOMP


