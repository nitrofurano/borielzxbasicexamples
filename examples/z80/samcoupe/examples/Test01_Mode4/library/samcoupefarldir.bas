sub samcoupefarldir (tsadr as uinteger,tspag as ubyte,tdadr as uinteger,tdpag as ubyte,tsiz as uinteger,tnpag as ubyte):
  asm
    ;unsure how fine it works, not tested yet
    ld hl,$5B84
    ld b,(ix+13)
    ld (hl),b
    inc hl
    ld b,(ix+12)
    ld (hl),b
    ld hl,$5B83
    ld b,(ix+15)
    ld (hl),b
    ld h,(ix+5)
    ld l,(ix+4)
    ld a,(ix+7)
    ld d,(ix+9)
    ld e,(ix+8)
    ld c,(ix+11)
    call $012D
    end asm 
  end sub

'$012D - FARLDIR - Copies (PAGCOUNT=#5B83) 16k pages + (MODCOUNT=#5B84) bytes from page A, offset HL to page C, offset DE. Paging on entry/exit unimportant and unchanged.
