function fastcall samcoupernd(trds as uinteger) as uinteger:
  asm
    ;ld  hl, (zb_randomSeed)
    ld  d, h
    ld  e, l
    ld  a, d
    ld  h, e
    ld  l, 253
    or  a
    sbc  hl, de
    sbc  a, 0
    sbc  hl, de
    ld  d, 0
    sbc  a, d
    ld  e, a
    sbc  hl, de
    jr  nc, smsrndloop
    inc  hl
    smsrndloop:
    ;ld  (zb_randomSeed), hl
    ret
    end asm
  end function
