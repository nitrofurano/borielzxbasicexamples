asm
  di
  end asm

asm
  in  a,($fb)       ; page at &8000, page+1 at &C000
  inc a             ; put screen at HMPR page+1
  and %00011111     ; keep only page bits
  or  3*32          ; merge in mode 1 bits ((mode-1)*32? not working?) <-!!!!!!
  out ($fc),a       ; set video mode+page to look at &c000
  end asm

'-------------------------------------------------------------------------------

#include "library/samcoupernd.bas"
#include "library/samcoupepaletteout.bas"
#include "library/samcoupedelay.bas"
#include "library/samcoupehexcolour.bas"
#include "library/samcoupeborder.bas"
#include "library/b4r0f0e0_charmappobynews01.zxi"
#include "library/pacmanldir.bas"
#include "library/pacmanfillram.bas"

asm
ld sp,$40FF
end asm

pacmanfillram($8000,0,$8000)

'-------------------------------------------------------------------------------

asm
  ld bc,$04F8
  ld a,$3F
  out (c),a
  end asm

dim i1 as uinteger at $4010
dim i2 as uinteger at $4012
dim i3 as uinteger at $4014
dim i4 as uinteger at $4016
dim ee0 as uinteger at $4018
dim ee1 as uinteger at $401A
dim ee2 as uinteger at $401C
dim ee3 as uinteger at $401E
dim i as uinteger at $4020
dim seed1 as uinteger at $4022

samcoupeborder(4)

for i=0 to 15
  samcoupepaletteout(i,samcoupehexcolour(peek(uinteger,(32*0)+@palette01+(i*2))))
  next

seed1=43

samcoupeborder(4)

asm
  in  a,($fb)  
  inc a        
  out ($fb),a  
  end asm

do

''for i1=0 to 15
''  for i2=0 to 31
''    seed1=samcoupernd(seed1)
''    for i3=0 to 7
''      for i4=0 to 3
''        i=(i4)+(i2*4)+(i3*128)+(i1*1024)
''        ee0=peek(@charmap01+(seed1 mod 64)*32+i3*4+i4)
''        'poke $9C00+i,ee0 '$C000
''        poke $C000+i,ee0
''        next:next
''    samcoupedelay(500)
''    next:next


for i1=0 to 23
  for i2=0 to 31
    seed1=samcoupernd(seed1)
    for i3=0 to 7
      for i4=0 to 3
        i=(i4)+(i2*4)+(i3*128)+(i1*1024)
        ee0=peek(@charmap01+(seed1 mod 64)*32+i3*4+i4)
        'poke $9C00+i,ee0 '$C000
        poke $8000+i,ee0
        next:next
    samcoupedelay(500)
    next:next

'asm
'  in  a,($fb)  
'  dec a        
'  out ($fb),a  
'  end asm

seed1=samcoupernd(seed1)
samcoupeborder(seed1 band 15)

seed1=samcoupernd(seed1)
for i=0 to 15
  samcoupepaletteout(i,samcoupehexcolour(peek(uinteger,((seed1 mod 8)*32)+@palette01+(i*2))))
  next


loop

samcoupeborder(0)


samcoupeborder(7)


'-------------------------------------------------------------------------------


palette01:
asm
  ;-god-atarist
  defw $000,$005,$500,$055
  defw $555,$55A,$A50,$A55
  defw $5AF,$AA5,$AAA,$AAF
  defw $FAA,$FF0,$AFF,$FFF
  ;-goemonkonamimsx
  defw $000,$FB9,$000,$F00
  defw $FB9,$B60,$FFF,$620
  defw $FB0,$B64,$940,$049
  defw $999,$092,$FFF,$042
  ;-sg1000mk3
  defw $000,$000,$0A0,$0F0
  defw $005,$00F,$500,$0FF
  defw $A00,$F00,$550,$FF0
  defw $050,$F0F,$555,$FFF
  ;-mojontwinsulaplus
  defw $000,$40F,$B20,$94A
  defw $460,$26F,$B90,$BBF
  defw $000,$04F,$D40,$B6A
  defw $6D0,$0BF,$DB5,$FFF
  ;-sgi4dwm
  defw $000,$F00,$0F0,$FF0
  defw $00F,$F0F,$0FF,$FFF
  defw $555,$C88,$8C8,$884
  defw $88C,$848,$488,$AAA
  ;-apple2c16
  defw $000,$724,$437,$E3F
  defw $054,$888,$19F,$BBF
  defw $440,$E60,$888,$FAB
  defw $1C0,$BC8,$8DB,$FFF
  ;-msx1
  defw $000,$000,$2D2,$6F6
  defw $22F,$36F,$B22,$4DF
  defw $F22,$F66,$DD2,$DD9
  defw $292,$D4B,$BBB,$FFF
  ;-c64
  defw $000,$FFF,$A00,$AFF
  defw $A5A,$0A5,$00A,$FF5
  defw $FA5,$550,$F55,$555
  defw $555,$AF5,$0AF,$AAA
  end asm


'- .sbt file: old versions of Simcoupe needs:
'- boot1:load1:call32768


chartest01:
chartest01a:
asm
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00
defb $00,$00,$00,$00

defb $00,$FF,$F0,$00
defb $0F,$F0,$FF,$00
defb $FF,$00,$0F,$F0
defb $FF,$00,$0F,$F0
defb $FF,$FF,$FF,$F0
defb $FF,$00,$0F,$F0
defb $FF,$00,$0F,$F0
defb $00,$00,$00,$00

defb $FF,$FF,$FF,$00
defb $FF,$00,$0F,$F0
defb $FF,$00,$0F,$F0
defb $FF,$FF,$FF,$00
defb $FF,$00,$0F,$F0
defb $FF,$00,$0F,$F0
defb $FF,$FF,$FF,$00
defb $00,$00,$00,$00

defb $00,$11,$11,$00
defb $01,$10,$01,$10
defb $11,$00,$00,$00
defb $11,$00,$00,$00
defb $11,$00,$00,$00
defb $01,$10,$01,$10
defb $00,$11,$11,$00
defb $00,$00,$00,$00

end asm

chartest01b:
asm
defb %00111000
defb %01101100
defb %11000110
defb %11000110
defb %11111110
defb %11000110
defb %11000110
defb %00000000
end asm

'asm
'  in  a,($fc)
'  and %11111110
'  or  %00000001
'  out ($fc),a
'  end asm

'asm
'  in  a,($fb)
'  and %00011110
'  or  %00000001
'  out ($fb),a
'  end asm


