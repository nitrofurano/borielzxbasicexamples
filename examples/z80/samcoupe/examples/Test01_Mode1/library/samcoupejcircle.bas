sub samcoupejcircle(txpo as uinteger, typo as ubyte, trad as ubyte):
  asm
    ld h,(ix+5)
    ld l,(ix+4)
    ld c,h
    ld b,(ix+7)
    ld a,(ix+9)
    ld d,a   ;-+--- patchwork on a SamCoupe rom bug... :S 
    ld e,a   ; |
    push de  ; |  
    pop af   ;-+
    ld ($4000),ix
    call $0142
    ld ix,($4000)
    end asm
  end sub
'$0142 - JCIRCLE - Draw circle at C,B radius A
